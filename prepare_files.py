import glob
import yaml
import io
import os
import shutil

from bs4 import BeautifulSoup

def delete_unnecessary_folders():
    folder_paths = ["assets/page-data/404.html","assets/learn"]

    for folder_path in folder_paths:
        if os.path.exists(folder_path):
            try:
                shutil.rmtree(folder_path)
                print(f"{folder_path} is removed!")
            except OSError as e:
                print("Error: %s - %s." % (e.filename, e.strerror))

def add_assets_to_config():
    asset_files = glob.glob("assets/**/*.*",recursive=True)
    asset_files = [asset_file.replace("\\","/") for asset_file in asset_files]
    asset_files = ["/".join(asset_file.split("/")[:-1])+"/" for asset_file in asset_files] # Adding folder instead of all files.
    asset_files = list(set(asset_files))

    with open("pubspec.yaml", "r") as stream:
        try:
            pubspec_dict = yaml.safe_load(stream)
            pubspec_dict["flutter"]["assets"] = asset_files

        except yaml.YAMLError as exc:
            print(exc)

    with io.open('pubspec.yaml', 'w', encoding='utf8') as outfile:
        yaml.dump(pubspec_dict, outfile, default_flow_style=False, allow_unicode=True,sort_keys=False,indent=4)

    print("pubspec.yaml file is re-created with assets!")

def inject_javascript():
    html = open("assets/index.html").read()
    soup = BeautifulSoup(html,parser="lxml",features="lxml")
    if len(soup.select("#url-observer")) == 0:
        new_tag = soup.new_tag('script', id='url-observer')
        new_tag.string = """ window.addEventListener(
                    "load",
                    function () {
                        let oldHref = document.location.href,
                            bodyDOM = document.querySelector("body")
                        const observer = new MutationObserver(function (mutations) {
                            if (oldHref != document.location.href) {
                                oldHref = document.location.href
                                console.warn(oldHref)
                                window.requestAnimationFrame(function () {
                                    let tmp = document.querySelector("body")
                                    if (tmp != bodyDOM) {
                                        bodyDOM = tmp
                                        observer.observe(bodyDOM, config)
                                    }
                                })
                            }
                        })
                        const config = {
                            childList: true,
                            subtree: true
                        }
                        observer.observe(bodyDOM, config)
                    },
                    true
                )
        """
        soup.body.insert(-1, new_tag)
        with open("assets/index.html", "w") as file:
            file.write(str(soup))
    else:
        print("Javascript already injected!")

def main():
    delete_unnecessary_folders()
    add_assets_to_config()
    inject_javascript()

if __name__ == "__main__":
    main()