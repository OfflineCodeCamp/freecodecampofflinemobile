// ignore_for_file: unnecessary_new

import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'in_app_localhost_server.dart';
import 'package:url_launcher/url_launcher.dart';

final InAppLocalhostServerMy localhostServer = new InAppLocalhostServerMy();
var currentUrl = "http://localhost:8080/";

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // start the localhost server
  await localhostServer.start();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: Color(0xFF0a0a23), // navigation bar color
    statusBarColor: Color(0xFF0a0a23), // status bar color
  ));
  runApp(MaterialApp(home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  InAppWebViewController? webViewController;
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        javaScriptEnabled: true,
        useOnLoadResource: true,
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    Future<bool> _onWillPop(BuildContext context) async {
      print("currentUrl is: $currentUrl");
      if (currentUrl == "http://localhost:8080/") {
        return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text('Are you sure?'),
            content: const Text('Do you want to exit an App'),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('No'),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text('Yes'),
              ),
            ],
          ),
        ));
      }
      webViewController?.goBack();
      return false;
    }

    return WillPopScope(
        onWillPop: () => _onWillPop(context),
        child: Scaffold(
          body: Container(
              margin: EdgeInsets.only(top: statusBarHeight),
              child: Column(children: <Widget>[
                Expanded(
                  child: InAppWebView(
                    initialUrlRequest: URLRequest(
                        url: Uri.parse("http://localhost:8080/index.html")),
                    initialOptions: options,
                    onWebViewCreated: (controller) {
                      webViewController = controller;
                    },
                    onLoadStart: (controller, url) {},
                    onLoadStop: (controller, url) {},
                    onLoadResource: (controller, resource) {},
                    onConsoleMessage: (controller, consoleMessage) {
                      print(consoleMessage);
                      if (consoleMessage.messageLevel == 2) {
                        var url = consoleMessage.message;
                        currentUrl = url;
                        print("CURRENT URL::" + currentUrl);
                      }
                    },
                    shouldOverrideUrlLoading:
                        (controller, navigationAction) async {
                      var uri = navigationAction.request.url!;

                      print("EXTERNAL URL:" + uri.toString());
                      var url = uri.toString();
                      await canLaunch(url)
                          ? await launch(url)
                          : throw 'Could not launch $url';

                      return NavigationActionPolicy.CANCEL;
                    },
                  ),
                )
              ])),
        ));
  }
}
