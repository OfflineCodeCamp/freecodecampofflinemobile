(self["webpackChunk_freecodecamp_client"] = self["webpackChunk_freecodecamp_client"] || []).push([["chai"],{

/***/ "./node_modules/assertion-error/index.js":
/*!***********************************************!*\
  !*** ./node_modules/assertion-error/index.js ***!
  \***********************************************/
/***/ ((module) => {

/*!
 * assertion-error
 * Copyright(c) 2013 Jake Luer <jake@qualiancy.com>
 * MIT Licensed
 */

/*!
 * Return a function that will copy properties from
 * one object to another excluding any originally
 * listed. Returned function will create a new `{}`.
 *
 * @param {String} excluded properties ...
 * @return {Function}
 */

function exclude () {
  var excludes = [].slice.call(arguments);

  function excludeProps (res, obj) {
    Object.keys(obj).forEach(function (key) {
      if (!~excludes.indexOf(key)) res[key] = obj[key];
    });
  }

  return function extendExclude () {
    var args = [].slice.call(arguments)
      , i = 0
      , res = {};

    for (; i < args.length; i++) {
      excludeProps(res, args[i]);
    }

    return res;
  };
};

/*!
 * Primary Exports
 */

module.exports = AssertionError;

/**
 * ### AssertionError
 *
 * An extension of the JavaScript `Error` constructor for
 * assertion and validation scenarios.
 *
 * @param {String} message
 * @param {Object} properties to include (optional)
 * @param {callee} start stack function (optional)
 */

function AssertionError (message, _props, ssf) {
  var extend = exclude('name', 'message', 'stack', 'constructor', 'toJSON')
    , props = extend(_props || {});

  // default values
  this.message = message || 'Unspecified AssertionError';
  this.showDiff = false;

  // copy from properties
  for (var key in props) {
    this[key] = props[key];
  }

  // capture stack trace
  ssf = ssf || AssertionError;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, ssf);
  } else {
    try {
      throw new Error();
    } catch(e) {
      this.stack = e.stack;
    }
  }
}

/*!
 * Inherit from Error.prototype
 */

AssertionError.prototype = Object.create(Error.prototype);

/*!
 * Statically set name
 */

AssertionError.prototype.name = 'AssertionError';

/*!
 * Ensure correct constructor
 */

AssertionError.prototype.constructor = AssertionError;

/**
 * Allow errors to be converted to JSON for static transfer.
 *
 * @param {Boolean} include stack (default: `true`)
 * @return {Object} object that can be `JSON.stringify`
 */

AssertionError.prototype.toJSON = function (stack) {
  var extend = exclude('constructor', 'toJSON', 'stack')
    , props = extend({ name: this.name }, this);

  // include stack if exists and not turned off
  if (false !== stack && this.stack) {
    props.stack = this.stack;
  }

  return props;
};


/***/ }),

/***/ "./node_modules/chai/index.js":
/*!************************************!*\
  !*** ./node_modules/chai/index.js ***!
  \************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! ./lib/chai */ "./node_modules/chai/lib/chai.js");


/***/ }),

/***/ "./node_modules/chai/lib/chai.js":
/*!***************************************!*\
  !*** ./node_modules/chai/lib/chai.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/*!
 * chai
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var used = [];

/*!
 * Chai version
 */

exports.version = '4.3.3';

/*!
 * Assertion Error
 */

exports.AssertionError = __webpack_require__(/*! assertion-error */ "./node_modules/assertion-error/index.js");

/*!
 * Utils for plugins (not exported)
 */

var util = __webpack_require__(/*! ./chai/utils */ "./node_modules/chai/lib/chai/utils/index.js");

/**
 * # .use(function)
 *
 * Provides a way to extend the internals of Chai.
 *
 * @param {Function}
 * @returns {this} for chaining
 * @api public
 */

exports.use = function (fn) {
  if (!~used.indexOf(fn)) {
    fn(exports, util);
    used.push(fn);
  }

  return exports;
};

/*!
 * Utility Functions
 */

exports.util = util;

/*!
 * Configuration
 */

var config = __webpack_require__(/*! ./chai/config */ "./node_modules/chai/lib/chai/config.js");
exports.config = config;

/*!
 * Primary `Assertion` prototype
 */

var assertion = __webpack_require__(/*! ./chai/assertion */ "./node_modules/chai/lib/chai/assertion.js");
exports.use(assertion);

/*!
 * Core Assertions
 */

var core = __webpack_require__(/*! ./chai/core/assertions */ "./node_modules/chai/lib/chai/core/assertions.js");
exports.use(core);

/*!
 * Expect interface
 */

var expect = __webpack_require__(/*! ./chai/interface/expect */ "./node_modules/chai/lib/chai/interface/expect.js");
exports.use(expect);

/*!
 * Should interface
 */

var should = __webpack_require__(/*! ./chai/interface/should */ "./node_modules/chai/lib/chai/interface/should.js");
exports.use(should);

/*!
 * Assert interface
 */

var assert = __webpack_require__(/*! ./chai/interface/assert */ "./node_modules/chai/lib/chai/interface/assert.js");
exports.use(assert);


/***/ }),

/***/ "./node_modules/chai/lib/chai/assertion.js":
/*!*************************************************!*\
  !*** ./node_modules/chai/lib/chai/assertion.js ***!
  \*************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * chai
 * http://chaijs.com
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var config = __webpack_require__(/*! ./config */ "./node_modules/chai/lib/chai/config.js");

module.exports = function (_chai, util) {
  /*!
   * Module dependencies.
   */

  var AssertionError = _chai.AssertionError
    , flag = util.flag;

  /*!
   * Module export.
   */

  _chai.Assertion = Assertion;

  /*!
   * Assertion Constructor
   *
   * Creates object for chaining.
   *
   * `Assertion` objects contain metadata in the form of flags. Three flags can
   * be assigned during instantiation by passing arguments to this constructor:
   *
   * - `object`: This flag contains the target of the assertion. For example, in
   *   the assertion `expect(numKittens).to.equal(7);`, the `object` flag will
   *   contain `numKittens` so that the `equal` assertion can reference it when
   *   needed.
   *
   * - `message`: This flag contains an optional custom error message to be
   *   prepended to the error message that's generated by the assertion when it
   *   fails.
   *
   * - `ssfi`: This flag stands for "start stack function indicator". It
   *   contains a function reference that serves as the starting point for
   *   removing frames from the stack trace of the error that's created by the
   *   assertion when it fails. The goal is to provide a cleaner stack trace to
   *   end users by removing Chai's internal functions. Note that it only works
   *   in environments that support `Error.captureStackTrace`, and only when
   *   `Chai.config.includeStack` hasn't been set to `false`.
   *
   * - `lockSsfi`: This flag controls whether or not the given `ssfi` flag
   *   should retain its current value, even as assertions are chained off of
   *   this object. This is usually set to `true` when creating a new assertion
   *   from within another assertion. It's also temporarily set to `true` before
   *   an overwritten assertion gets called by the overwriting assertion.
   *
   * @param {Mixed} obj target of the assertion
   * @param {String} msg (optional) custom error message
   * @param {Function} ssfi (optional) starting point for removing stack frames
   * @param {Boolean} lockSsfi (optional) whether or not the ssfi flag is locked
   * @api private
   */

  function Assertion (obj, msg, ssfi, lockSsfi) {
    flag(this, 'ssfi', ssfi || Assertion);
    flag(this, 'lockSsfi', lockSsfi);
    flag(this, 'object', obj);
    flag(this, 'message', msg);

    return util.proxify(this);
  }

  Object.defineProperty(Assertion, 'includeStack', {
    get: function() {
      console.warn('Assertion.includeStack is deprecated, use chai.config.includeStack instead.');
      return config.includeStack;
    },
    set: function(value) {
      console.warn('Assertion.includeStack is deprecated, use chai.config.includeStack instead.');
      config.includeStack = value;
    }
  });

  Object.defineProperty(Assertion, 'showDiff', {
    get: function() {
      console.warn('Assertion.showDiff is deprecated, use chai.config.showDiff instead.');
      return config.showDiff;
    },
    set: function(value) {
      console.warn('Assertion.showDiff is deprecated, use chai.config.showDiff instead.');
      config.showDiff = value;
    }
  });

  Assertion.addProperty = function (name, fn) {
    util.addProperty(this.prototype, name, fn);
  };

  Assertion.addMethod = function (name, fn) {
    util.addMethod(this.prototype, name, fn);
  };

  Assertion.addChainableMethod = function (name, fn, chainingBehavior) {
    util.addChainableMethod(this.prototype, name, fn, chainingBehavior);
  };

  Assertion.overwriteProperty = function (name, fn) {
    util.overwriteProperty(this.prototype, name, fn);
  };

  Assertion.overwriteMethod = function (name, fn) {
    util.overwriteMethod(this.prototype, name, fn);
  };

  Assertion.overwriteChainableMethod = function (name, fn, chainingBehavior) {
    util.overwriteChainableMethod(this.prototype, name, fn, chainingBehavior);
  };

  /**
   * ### .assert(expression, message, negateMessage, expected, actual, showDiff)
   *
   * Executes an expression and check expectations. Throws AssertionError for reporting if test doesn't pass.
   *
   * @name assert
   * @param {Philosophical} expression to be tested
   * @param {String|Function} message or function that returns message to display if expression fails
   * @param {String|Function} negatedMessage or function that returns negatedMessage to display if negated expression fails
   * @param {Mixed} expected value (remember to check for negation)
   * @param {Mixed} actual (optional) will default to `this.obj`
   * @param {Boolean} showDiff (optional) when set to `true`, assert will display a diff in addition to the message if expression fails
   * @api private
   */

  Assertion.prototype.assert = function (expr, msg, negateMsg, expected, _actual, showDiff) {
    var ok = util.test(this, arguments);
    if (false !== showDiff) showDiff = true;
    if (undefined === expected && undefined === _actual) showDiff = false;
    if (true !== config.showDiff) showDiff = false;

    if (!ok) {
      msg = util.getMessage(this, arguments);
      var actual = util.getActual(this, arguments);
      var assertionErrorObjectProperties = {
          actual: actual
        , expected: expected
        , showDiff: showDiff
      };

      var operator = util.getOperator(this, arguments);
      if (operator) {
        assertionErrorObjectProperties.operator = operator;
      }

      throw new AssertionError(
        msg,
        assertionErrorObjectProperties,
        (config.includeStack) ? this.assert : flag(this, 'ssfi'));
    }
  };

  /*!
   * ### ._obj
   *
   * Quick reference to stored `actual` value for plugin developers.
   *
   * @api private
   */

  Object.defineProperty(Assertion.prototype, '_obj',
    { get: function () {
        return flag(this, 'object');
      }
    , set: function (val) {
        flag(this, 'object', val);
      }
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/config.js":
/*!**********************************************!*\
  !*** ./node_modules/chai/lib/chai/config.js ***!
  \**********************************************/
/***/ ((module) => {

module.exports = {

  /**
   * ### config.includeStack
   *
   * User configurable property, influences whether stack trace
   * is included in Assertion error message. Default of false
   * suppresses stack trace in the error message.
   *
   *     chai.config.includeStack = true;  // enable stack on error
   *
   * @param {Boolean}
   * @api public
   */

  includeStack: false,

  /**
   * ### config.showDiff
   *
   * User configurable property, influences whether or not
   * the `showDiff` flag should be included in the thrown
   * AssertionErrors. `false` will always be `false`; `true`
   * will be true when the assertion has requested a diff
   * be shown.
   *
   * @param {Boolean}
   * @api public
   */

  showDiff: true,

  /**
   * ### config.truncateThreshold
   *
   * User configurable property, sets length threshold for actual and
   * expected values in assertion errors. If this threshold is exceeded, for
   * example for large data structures, the value is replaced with something
   * like `[ Array(3) ]` or `{ Object (prop1, prop2) }`.
   *
   * Set it to zero if you want to disable truncating altogether.
   *
   * This is especially userful when doing assertions on arrays: having this
   * set to a reasonable large value makes the failure messages readily
   * inspectable.
   *
   *     chai.config.truncateThreshold = 0;  // disable truncating
   *
   * @param {Number}
   * @api public
   */

  truncateThreshold: 40,

  /**
   * ### config.useProxy
   *
   * User configurable property, defines if chai will use a Proxy to throw
   * an error when a non-existent property is read, which protects users
   * from typos when using property-based assertions.
   *
   * Set it to false if you want to disable this feature.
   *
   *     chai.config.useProxy = false;  // disable use of Proxy
   *
   * This feature is automatically disabled regardless of this config value
   * in environments that don't support proxies.
   *
   * @param {Boolean}
   * @api public
   */

  useProxy: true,

  /**
   * ### config.proxyExcludedKeys
   *
   * User configurable property, defines which properties should be ignored
   * instead of throwing an error if they do not exist on the assertion.
   * This is only applied if the environment Chai is running in supports proxies and
   * if the `useProxy` configuration setting is enabled.
   * By default, `then` and `inspect` will not throw an error if they do not exist on the
   * assertion object because the `.inspect` property is read by `util.inspect` (for example, when
   * using `console.log` on the assertion object) and `.then` is necessary for promise type-checking.
   *
   *     // By default these keys will not throw an error if they do not exist on the assertion object
   *     chai.config.proxyExcludedKeys = ['then', 'inspect'];
   *
   * @param {Array}
   * @api public
   */

  proxyExcludedKeys: ['then', 'catch', 'inspect', 'toJSON']
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/core/assertions.js":
/*!*******************************************************!*\
  !*** ./node_modules/chai/lib/chai/core/assertions.js ***!
  \*******************************************************/
/***/ ((module) => {

/*!
 * chai
 * http://chaijs.com
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

module.exports = function (chai, _) {
  var Assertion = chai.Assertion
    , AssertionError = chai.AssertionError
    , flag = _.flag;

  /**
   * ### Language Chains
   *
   * The following are provided as chainable getters to improve the readability
   * of your assertions.
   *
   * **Chains**
   *
   * - to
   * - be
   * - been
   * - is
   * - that
   * - which
   * - and
   * - has
   * - have
   * - with
   * - at
   * - of
   * - same
   * - but
   * - does
   * - still
   * - also
   *
   * @name language chains
   * @namespace BDD
   * @api public
   */

  [ 'to', 'be', 'been', 'is'
  , 'and', 'has', 'have', 'with'
  , 'that', 'which', 'at', 'of'
  , 'same', 'but', 'does', 'still', "also" ].forEach(function (chain) {
    Assertion.addProperty(chain);
  });

  /**
   * ### .not
   *
   * Negates all assertions that follow in the chain.
   *
   *     expect(function () {}).to.not.throw();
   *     expect({a: 1}).to.not.have.property('b');
   *     expect([1, 2]).to.be.an('array').that.does.not.include(3);
   *
   * Just because you can negate any assertion with `.not` doesn't mean you
   * should. With great power comes great responsibility. It's often best to
   * assert that the one expected output was produced, rather than asserting
   * that one of countless unexpected outputs wasn't produced. See individual
   * assertions for specific guidance.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.not.equal(1); // Not recommended
   *
   * @name not
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('not', function () {
    flag(this, 'negate', true);
  });

  /**
   * ### .deep
   *
   * Causes all `.equal`, `.include`, `.members`, `.keys`, and `.property`
   * assertions that follow in the chain to use deep equality instead of strict
   * (`===`) equality. See the `deep-eql` project page for info on the deep
   * equality algorithm: https://github.com/chaijs/deep-eql.
   *
   *     // Target object deeply (but not strictly) equals `{a: 1}`
   *     expect({a: 1}).to.deep.equal({a: 1});
   *     expect({a: 1}).to.not.equal({a: 1});
   *
   *     // Target array deeply (but not strictly) includes `{a: 1}`
   *     expect([{a: 1}]).to.deep.include({a: 1});
   *     expect([{a: 1}]).to.not.include({a: 1});
   *
   *     // Target object deeply (but not strictly) includes `x: {a: 1}`
   *     expect({x: {a: 1}}).to.deep.include({x: {a: 1}});
   *     expect({x: {a: 1}}).to.not.include({x: {a: 1}});
   *
   *     // Target array deeply (but not strictly) has member `{a: 1}`
   *     expect([{a: 1}]).to.have.deep.members([{a: 1}]);
   *     expect([{a: 1}]).to.not.have.members([{a: 1}]);
   *
   *     // Target set deeply (but not strictly) has key `{a: 1}`
   *     expect(new Set([{a: 1}])).to.have.deep.keys([{a: 1}]);
   *     expect(new Set([{a: 1}])).to.not.have.keys([{a: 1}]);
   *
   *     // Target object deeply (but not strictly) has property `x: {a: 1}`
   *     expect({x: {a: 1}}).to.have.deep.property('x', {a: 1});
   *     expect({x: {a: 1}}).to.not.have.property('x', {a: 1});
   *
   * @name deep
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('deep', function () {
    flag(this, 'deep', true);
  });

  /**
   * ### .nested
   *
   * Enables dot- and bracket-notation in all `.property` and `.include`
   * assertions that follow in the chain.
   *
   *     expect({a: {b: ['x', 'y']}}).to.have.nested.property('a.b[1]');
   *     expect({a: {b: ['x', 'y']}}).to.nested.include({'a.b[1]': 'y'});
   *
   * If `.` or `[]` are part of an actual property name, they can be escaped by
   * adding two backslashes before them.
   *
   *     expect({'.a': {'[b]': 'x'}}).to.have.nested.property('\\.a.\\[b\\]');
   *     expect({'.a': {'[b]': 'x'}}).to.nested.include({'\\.a.\\[b\\]': 'x'});
   *
   * `.nested` cannot be combined with `.own`.
   *
   * @name nested
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('nested', function () {
    flag(this, 'nested', true);
  });

  /**
   * ### .own
   *
   * Causes all `.property` and `.include` assertions that follow in the chain
   * to ignore inherited properties.
   *
   *     Object.prototype.b = 2;
   *
   *     expect({a: 1}).to.have.own.property('a');
   *     expect({a: 1}).to.have.property('b');
   *     expect({a: 1}).to.not.have.own.property('b');
   *
   *     expect({a: 1}).to.own.include({a: 1});
   *     expect({a: 1}).to.include({b: 2}).but.not.own.include({b: 2});
   *
   * `.own` cannot be combined with `.nested`.
   *
   * @name own
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('own', function () {
    flag(this, 'own', true);
  });

  /**
   * ### .ordered
   *
   * Causes all `.members` assertions that follow in the chain to require that
   * members be in the same order.
   *
   *     expect([1, 2]).to.have.ordered.members([1, 2])
   *       .but.not.have.ordered.members([2, 1]);
   *
   * When `.include` and `.ordered` are combined, the ordering begins at the
   * start of both arrays.
   *
   *     expect([1, 2, 3]).to.include.ordered.members([1, 2])
   *       .but.not.include.ordered.members([2, 3]);
   *
   * @name ordered
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('ordered', function () {
    flag(this, 'ordered', true);
  });

  /**
   * ### .any
   *
   * Causes all `.keys` assertions that follow in the chain to only require that
   * the target have at least one of the given keys. This is the opposite of
   * `.all`, which requires that the target have all of the given keys.
   *
   *     expect({a: 1, b: 2}).to.not.have.any.keys('c', 'd');
   *
   * See the `.keys` doc for guidance on when to use `.any` or `.all`.
   *
   * @name any
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('any', function () {
    flag(this, 'any', true);
    flag(this, 'all', false);
  });

  /**
   * ### .all
   *
   * Causes all `.keys` assertions that follow in the chain to require that the
   * target have all of the given keys. This is the opposite of `.any`, which
   * only requires that the target have at least one of the given keys.
   *
   *     expect({a: 1, b: 2}).to.have.all.keys('a', 'b');
   *
   * Note that `.all` is used by default when neither `.all` nor `.any` are
   * added earlier in the chain. However, it's often best to add `.all` anyway
   * because it improves readability.
   *
   * See the `.keys` doc for guidance on when to use `.any` or `.all`.
   *
   * @name all
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('all', function () {
    flag(this, 'all', true);
    flag(this, 'any', false);
  });

  /**
   * ### .a(type[, msg])
   *
   * Asserts that the target's type is equal to the given string `type`. Types
   * are case insensitive. See the `type-detect` project page for info on the
   * type detection algorithm: https://github.com/chaijs/type-detect.
   *
   *     expect('foo').to.be.a('string');
   *     expect({a: 1}).to.be.an('object');
   *     expect(null).to.be.a('null');
   *     expect(undefined).to.be.an('undefined');
   *     expect(new Error).to.be.an('error');
   *     expect(Promise.resolve()).to.be.a('promise');
   *     expect(new Float32Array).to.be.a('float32array');
   *     expect(Symbol()).to.be.a('symbol');
   *
   * `.a` supports objects that have a custom type set via `Symbol.toStringTag`.
   *
   *     var myObj = {
   *       [Symbol.toStringTag]: 'myCustomType'
   *     };
   *
   *     expect(myObj).to.be.a('myCustomType').but.not.an('object');
   *
   * It's often best to use `.a` to check a target's type before making more
   * assertions on the same target. That way, you avoid unexpected behavior from
   * any assertion that does different things based on the target's type.
   *
   *     expect([1, 2, 3]).to.be.an('array').that.includes(2);
   *     expect([]).to.be.an('array').that.is.empty;
   *
   * Add `.not` earlier in the chain to negate `.a`. However, it's often best to
   * assert that the target is the expected type, rather than asserting that it
   * isn't one of many unexpected types.
   *
   *     expect('foo').to.be.a('string'); // Recommended
   *     expect('foo').to.not.be.an('array'); // Not recommended
   *
   * `.a` accepts an optional `msg` argument which is a custom error message to
   * show when the assertion fails. The message can also be given as the second
   * argument to `expect`.
   *
   *     expect(1).to.be.a('string', 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.be.a('string');
   *
   * `.a` can also be used as a language chain to improve the readability of
   * your assertions.
   *
   *     expect({b: 2}).to.have.a.property('b');
   *
   * The alias `.an` can be used interchangeably with `.a`.
   *
   * @name a
   * @alias an
   * @param {String} type
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function an (type, msg) {
    if (msg) flag(this, 'message', msg);
    type = type.toLowerCase();
    var obj = flag(this, 'object')
      , article = ~[ 'a', 'e', 'i', 'o', 'u' ].indexOf(type.charAt(0)) ? 'an ' : 'a ';

    this.assert(
        type === _.type(obj).toLowerCase()
      , 'expected #{this} to be ' + article + type
      , 'expected #{this} not to be ' + article + type
    );
  }

  Assertion.addChainableMethod('an', an);
  Assertion.addChainableMethod('a', an);

  /**
   * ### .include(val[, msg])
   *
   * When the target is a string, `.include` asserts that the given string `val`
   * is a substring of the target.
   *
   *     expect('foobar').to.include('foo');
   *
   * When the target is an array, `.include` asserts that the given `val` is a
   * member of the target.
   *
   *     expect([1, 2, 3]).to.include(2);
   *
   * When the target is an object, `.include` asserts that the given object
   * `val`'s properties are a subset of the target's properties.
   *
   *     expect({a: 1, b: 2, c: 3}).to.include({a: 1, b: 2});
   *
   * When the target is a Set or WeakSet, `.include` asserts that the given `val` is a
   * member of the target. SameValueZero equality algorithm is used.
   *
   *     expect(new Set([1, 2])).to.include(2);
   *
   * When the target is a Map, `.include` asserts that the given `val` is one of
   * the values of the target. SameValueZero equality algorithm is used.
   *
   *     expect(new Map([['a', 1], ['b', 2]])).to.include(2);
   *
   * Because `.include` does different things based on the target's type, it's
   * important to check the target's type before using `.include`. See the `.a`
   * doc for info on testing a target's type.
   *
   *     expect([1, 2, 3]).to.be.an('array').that.includes(2);
   *
   * By default, strict (`===`) equality is used to compare array members and
   * object properties. Add `.deep` earlier in the chain to use deep equality
   * instead (WeakSet targets are not supported). See the `deep-eql` project
   * page for info on the deep equality algorithm: https://github.com/chaijs/deep-eql.
   *
   *     // Target array deeply (but not strictly) includes `{a: 1}`
   *     expect([{a: 1}]).to.deep.include({a: 1});
   *     expect([{a: 1}]).to.not.include({a: 1});
   *
   *     // Target object deeply (but not strictly) includes `x: {a: 1}`
   *     expect({x: {a: 1}}).to.deep.include({x: {a: 1}});
   *     expect({x: {a: 1}}).to.not.include({x: {a: 1}});
   *
   * By default, all of the target's properties are searched when working with
   * objects. This includes properties that are inherited and/or non-enumerable.
   * Add `.own` earlier in the chain to exclude the target's inherited
   * properties from the search.
   *
   *     Object.prototype.b = 2;
   *
   *     expect({a: 1}).to.own.include({a: 1});
   *     expect({a: 1}).to.include({b: 2}).but.not.own.include({b: 2});
   *
   * Note that a target object is always only searched for `val`'s own
   * enumerable properties.
   *
   * `.deep` and `.own` can be combined.
   *
   *     expect({a: {b: 2}}).to.deep.own.include({a: {b: 2}});
   *
   * Add `.nested` earlier in the chain to enable dot- and bracket-notation when
   * referencing nested properties.
   *
   *     expect({a: {b: ['x', 'y']}}).to.nested.include({'a.b[1]': 'y'});
   *
   * If `.` or `[]` are part of an actual property name, they can be escaped by
   * adding two backslashes before them.
   *
   *     expect({'.a': {'[b]': 2}}).to.nested.include({'\\.a.\\[b\\]': 2});
   *
   * `.deep` and `.nested` can be combined.
   *
   *     expect({a: {b: [{c: 3}]}}).to.deep.nested.include({'a.b[0]': {c: 3}});
   *
   * `.own` and `.nested` cannot be combined.
   *
   * Add `.not` earlier in the chain to negate `.include`.
   *
   *     expect('foobar').to.not.include('taco');
   *     expect([1, 2, 3]).to.not.include(4);
   *
   * However, it's dangerous to negate `.include` when the target is an object.
   * The problem is that it creates uncertain expectations by asserting that the
   * target object doesn't have all of `val`'s key/value pairs but may or may
   * not have some of them. It's often best to identify the exact output that's
   * expected, and then write an assertion that only accepts that exact output.
   *
   * When the target object isn't even expected to have `val`'s keys, it's
   * often best to assert exactly that.
   *
   *     expect({c: 3}).to.not.have.any.keys('a', 'b'); // Recommended
   *     expect({c: 3}).to.not.include({a: 1, b: 2}); // Not recommended
   *
   * When the target object is expected to have `val`'s keys, it's often best to
   * assert that each of the properties has its expected value, rather than
   * asserting that each property doesn't have one of many unexpected values.
   *
   *     expect({a: 3, b: 4}).to.include({a: 3, b: 4}); // Recommended
   *     expect({a: 3, b: 4}).to.not.include({a: 1, b: 2}); // Not recommended
   *
   * `.include` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect([1, 2, 3]).to.include(4, 'nooo why fail??');
   *     expect([1, 2, 3], 'nooo why fail??').to.include(4);
   *
   * `.include` can also be used as a language chain, causing all `.members` and
   * `.keys` assertions that follow in the chain to require the target to be a
   * superset of the expected set, rather than an identical set. Note that
   * `.members` ignores duplicates in the subset when `.include` is added.
   *
   *     // Target object's keys are a superset of ['a', 'b'] but not identical
   *     expect({a: 1, b: 2, c: 3}).to.include.all.keys('a', 'b');
   *     expect({a: 1, b: 2, c: 3}).to.not.have.all.keys('a', 'b');
   *
   *     // Target array is a superset of [1, 2] but not identical
   *     expect([1, 2, 3]).to.include.members([1, 2]);
   *     expect([1, 2, 3]).to.not.have.members([1, 2]);
   *
   *     // Duplicates in the subset are ignored
   *     expect([1, 2, 3]).to.include.members([1, 2, 2, 2]);
   *
   * Note that adding `.any` earlier in the chain causes the `.keys` assertion
   * to ignore `.include`.
   *
   *     // Both assertions are identical
   *     expect({a: 1}).to.include.any.keys('a', 'b');
   *     expect({a: 1}).to.have.any.keys('a', 'b');
   *
   * The aliases `.includes`, `.contain`, and `.contains` can be used
   * interchangeably with `.include`.
   *
   * @name include
   * @alias contain
   * @alias includes
   * @alias contains
   * @param {Mixed} val
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function SameValueZero(a, b) {
    return (_.isNaN(a) && _.isNaN(b)) || a === b;
  }

  function includeChainingBehavior () {
    flag(this, 'contains', true);
  }

  function include (val, msg) {
    if (msg) flag(this, 'message', msg);

    var obj = flag(this, 'object')
      , objType = _.type(obj).toLowerCase()
      , flagMsg = flag(this, 'message')
      , negate = flag(this, 'negate')
      , ssfi = flag(this, 'ssfi')
      , isDeep = flag(this, 'deep')
      , descriptor = isDeep ? 'deep ' : '';

    flagMsg = flagMsg ? flagMsg + ': ' : '';

    var included = false;

    switch (objType) {
      case 'string':
        included = obj.indexOf(val) !== -1;
        break;

      case 'weakset':
        if (isDeep) {
          throw new AssertionError(
            flagMsg + 'unable to use .deep.include with WeakSet',
            undefined,
            ssfi
          );
        }

        included = obj.has(val);
        break;

      case 'map':
        var isEql = isDeep ? _.eql : SameValueZero;
        obj.forEach(function (item) {
          included = included || isEql(item, val);
        });
        break;

      case 'set':
        if (isDeep) {
          obj.forEach(function (item) {
            included = included || _.eql(item, val);
          });
        } else {
          included = obj.has(val);
        }
        break;

      case 'array':
        if (isDeep) {
          included = obj.some(function (item) {
            return _.eql(item, val);
          })
        } else {
          included = obj.indexOf(val) !== -1;
        }
        break;

      default:
        // This block is for asserting a subset of properties in an object.
        // `_.expectTypes` isn't used here because `.include` should work with
        // objects with a custom `@@toStringTag`.
        if (val !== Object(val)) {
          throw new AssertionError(
            flagMsg + 'the given combination of arguments ('
            + objType + ' and '
            + _.type(val).toLowerCase() + ')'
            + ' is invalid for this assertion. '
            + 'You can use an array, a map, an object, a set, a string, '
            + 'or a weakset instead of a '
            + _.type(val).toLowerCase(),
            undefined,
            ssfi
          );
        }

        var props = Object.keys(val)
          , firstErr = null
          , numErrs = 0;

        props.forEach(function (prop) {
          var propAssertion = new Assertion(obj);
          _.transferFlags(this, propAssertion, true);
          flag(propAssertion, 'lockSsfi', true);

          if (!negate || props.length === 1) {
            propAssertion.property(prop, val[prop]);
            return;
          }

          try {
            propAssertion.property(prop, val[prop]);
          } catch (err) {
            if (!_.checkError.compatibleConstructor(err, AssertionError)) {
              throw err;
            }
            if (firstErr === null) firstErr = err;
            numErrs++;
          }
        }, this);

        // When validating .not.include with multiple properties, we only want
        // to throw an assertion error if all of the properties are included,
        // in which case we throw the first property assertion error that we
        // encountered.
        if (negate && props.length > 1 && numErrs === props.length) {
          throw firstErr;
        }
        return;
    }

    // Assert inclusion in collection or substring in a string.
    this.assert(
      included
      , 'expected #{this} to ' + descriptor + 'include ' + _.inspect(val)
      , 'expected #{this} to not ' + descriptor + 'include ' + _.inspect(val));
  }

  Assertion.addChainableMethod('include', include, includeChainingBehavior);
  Assertion.addChainableMethod('contain', include, includeChainingBehavior);
  Assertion.addChainableMethod('contains', include, includeChainingBehavior);
  Assertion.addChainableMethod('includes', include, includeChainingBehavior);

  /**
   * ### .ok
   *
   * Asserts that the target is a truthy value (considered `true` in boolean context).
   * However, it's often best to assert that the target is strictly (`===`) or
   * deeply equal to its expected value.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.be.ok; // Not recommended
   *
   *     expect(true).to.be.true; // Recommended
   *     expect(true).to.be.ok; // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.ok`.
   *
   *     expect(0).to.equal(0); // Recommended
   *     expect(0).to.not.be.ok; // Not recommended
   *
   *     expect(false).to.be.false; // Recommended
   *     expect(false).to.not.be.ok; // Not recommended
   *
   *     expect(null).to.be.null; // Recommended
   *     expect(null).to.not.be.ok; // Not recommended
   *
   *     expect(undefined).to.be.undefined; // Recommended
   *     expect(undefined).to.not.be.ok; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(false, 'nooo why fail??').to.be.ok;
   *
   * @name ok
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('ok', function () {
    this.assert(
        flag(this, 'object')
      , 'expected #{this} to be truthy'
      , 'expected #{this} to be falsy');
  });

  /**
   * ### .true
   *
   * Asserts that the target is strictly (`===`) equal to `true`.
   *
   *     expect(true).to.be.true;
   *
   * Add `.not` earlier in the chain to negate `.true`. However, it's often best
   * to assert that the target is equal to its expected value, rather than not
   * equal to `true`.
   *
   *     expect(false).to.be.false; // Recommended
   *     expect(false).to.not.be.true; // Not recommended
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.true; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(false, 'nooo why fail??').to.be.true;
   *
   * @name true
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('true', function () {
    this.assert(
        true === flag(this, 'object')
      , 'expected #{this} to be true'
      , 'expected #{this} to be false'
      , flag(this, 'negate') ? false : true
    );
  });

  /**
   * ### .false
   *
   * Asserts that the target is strictly (`===`) equal to `false`.
   *
   *     expect(false).to.be.false;
   *
   * Add `.not` earlier in the chain to negate `.false`. However, it's often
   * best to assert that the target is equal to its expected value, rather than
   * not equal to `false`.
   *
   *     expect(true).to.be.true; // Recommended
   *     expect(true).to.not.be.false; // Not recommended
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.false; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(true, 'nooo why fail??').to.be.false;
   *
   * @name false
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('false', function () {
    this.assert(
        false === flag(this, 'object')
      , 'expected #{this} to be false'
      , 'expected #{this} to be true'
      , flag(this, 'negate') ? true : false
    );
  });

  /**
   * ### .null
   *
   * Asserts that the target is strictly (`===`) equal to `null`.
   *
   *     expect(null).to.be.null;
   *
   * Add `.not` earlier in the chain to negate `.null`. However, it's often best
   * to assert that the target is equal to its expected value, rather than not
   * equal to `null`.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.null; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(42, 'nooo why fail??').to.be.null;
   *
   * @name null
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('null', function () {
    this.assert(
        null === flag(this, 'object')
      , 'expected #{this} to be null'
      , 'expected #{this} not to be null'
    );
  });

  /**
   * ### .undefined
   *
   * Asserts that the target is strictly (`===`) equal to `undefined`.
   *
   *     expect(undefined).to.be.undefined;
   *
   * Add `.not` earlier in the chain to negate `.undefined`. However, it's often
   * best to assert that the target is equal to its expected value, rather than
   * not equal to `undefined`.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.undefined; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(42, 'nooo why fail??').to.be.undefined;
   *
   * @name undefined
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('undefined', function () {
    this.assert(
        undefined === flag(this, 'object')
      , 'expected #{this} to be undefined'
      , 'expected #{this} not to be undefined'
    );
  });

  /**
   * ### .NaN
   *
   * Asserts that the target is exactly `NaN`.
   *
   *     expect(NaN).to.be.NaN;
   *
   * Add `.not` earlier in the chain to negate `.NaN`. However, it's often best
   * to assert that the target is equal to its expected value, rather than not
   * equal to `NaN`.
   *
   *     expect('foo').to.equal('foo'); // Recommended
   *     expect('foo').to.not.be.NaN; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(42, 'nooo why fail??').to.be.NaN;
   *
   * @name NaN
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('NaN', function () {
    this.assert(
        _.isNaN(flag(this, 'object'))
        , 'expected #{this} to be NaN'
        , 'expected #{this} not to be NaN'
    );
  });

  /**
   * ### .exist
   *
   * Asserts that the target is not strictly (`===`) equal to either `null` or
   * `undefined`. However, it's often best to assert that the target is equal to
   * its expected value.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.exist; // Not recommended
   *
   *     expect(0).to.equal(0); // Recommended
   *     expect(0).to.exist; // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.exist`.
   *
   *     expect(null).to.be.null; // Recommended
   *     expect(null).to.not.exist; // Not recommended
   *
   *     expect(undefined).to.be.undefined; // Recommended
   *     expect(undefined).to.not.exist; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(null, 'nooo why fail??').to.exist;
   *
   * The alias `.exists` can be used interchangeably with `.exist`.
   *
   * @name exist
   * @alias exists
   * @namespace BDD
   * @api public
   */

  function assertExist () {
    var val = flag(this, 'object');
    this.assert(
        val !== null && val !== undefined
      , 'expected #{this} to exist'
      , 'expected #{this} to not exist'
    );
  }

  Assertion.addProperty('exist', assertExist);
  Assertion.addProperty('exists', assertExist);

  /**
   * ### .empty
   *
   * When the target is a string or array, `.empty` asserts that the target's
   * `length` property is strictly (`===`) equal to `0`.
   *
   *     expect([]).to.be.empty;
   *     expect('').to.be.empty;
   *
   * When the target is a map or set, `.empty` asserts that the target's `size`
   * property is strictly equal to `0`.
   *
   *     expect(new Set()).to.be.empty;
   *     expect(new Map()).to.be.empty;
   *
   * When the target is a non-function object, `.empty` asserts that the target
   * doesn't have any own enumerable properties. Properties with Symbol-based
   * keys are excluded from the count.
   *
   *     expect({}).to.be.empty;
   *
   * Because `.empty` does different things based on the target's type, it's
   * important to check the target's type before using `.empty`. See the `.a`
   * doc for info on testing a target's type.
   *
   *     expect([]).to.be.an('array').that.is.empty;
   *
   * Add `.not` earlier in the chain to negate `.empty`. However, it's often
   * best to assert that the target contains its expected number of values,
   * rather than asserting that it's not empty.
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3); // Recommended
   *     expect([1, 2, 3]).to.not.be.empty; // Not recommended
   *
   *     expect(new Set([1, 2, 3])).to.have.property('size', 3); // Recommended
   *     expect(new Set([1, 2, 3])).to.not.be.empty; // Not recommended
   *
   *     expect(Object.keys({a: 1})).to.have.lengthOf(1); // Recommended
   *     expect({a: 1}).to.not.be.empty; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect([1, 2, 3], 'nooo why fail??').to.be.empty;
   *
   * @name empty
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('empty', function () {
    var val = flag(this, 'object')
      , ssfi = flag(this, 'ssfi')
      , flagMsg = flag(this, 'message')
      , itemsCount;

    flagMsg = flagMsg ? flagMsg + ': ' : '';

    switch (_.type(val).toLowerCase()) {
      case 'array':
      case 'string':
        itemsCount = val.length;
        break;
      case 'map':
      case 'set':
        itemsCount = val.size;
        break;
      case 'weakmap':
      case 'weakset':
        throw new AssertionError(
          flagMsg + '.empty was passed a weak collection',
          undefined,
          ssfi
        );
      case 'function':
        var msg = flagMsg + '.empty was passed a function ' + _.getName(val);
        throw new AssertionError(msg.trim(), undefined, ssfi);
      default:
        if (val !== Object(val)) {
          throw new AssertionError(
            flagMsg + '.empty was passed non-string primitive ' + _.inspect(val),
            undefined,
            ssfi
          );
        }
        itemsCount = Object.keys(val).length;
    }

    this.assert(
        0 === itemsCount
      , 'expected #{this} to be empty'
      , 'expected #{this} not to be empty'
    );
  });

  /**
   * ### .arguments
   *
   * Asserts that the target is an `arguments` object.
   *
   *     function test () {
   *       expect(arguments).to.be.arguments;
   *     }
   *
   *     test();
   *
   * Add `.not` earlier in the chain to negate `.arguments`. However, it's often
   * best to assert which type the target is expected to be, rather than
   * asserting that it’s not an `arguments` object.
   *
   *     expect('foo').to.be.a('string'); // Recommended
   *     expect('foo').to.not.be.arguments; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect({}, 'nooo why fail??').to.be.arguments;
   *
   * The alias `.Arguments` can be used interchangeably with `.arguments`.
   *
   * @name arguments
   * @alias Arguments
   * @namespace BDD
   * @api public
   */

  function checkArguments () {
    var obj = flag(this, 'object')
      , type = _.type(obj);
    this.assert(
        'Arguments' === type
      , 'expected #{this} to be arguments but got ' + type
      , 'expected #{this} to not be arguments'
    );
  }

  Assertion.addProperty('arguments', checkArguments);
  Assertion.addProperty('Arguments', checkArguments);

  /**
   * ### .equal(val[, msg])
   *
   * Asserts that the target is strictly (`===`) equal to the given `val`.
   *
   *     expect(1).to.equal(1);
   *     expect('foo').to.equal('foo');
   *
   * Add `.deep` earlier in the chain to use deep equality instead. See the
   * `deep-eql` project page for info on the deep equality algorithm:
   * https://github.com/chaijs/deep-eql.
   *
   *     // Target object deeply (but not strictly) equals `{a: 1}`
   *     expect({a: 1}).to.deep.equal({a: 1});
   *     expect({a: 1}).to.not.equal({a: 1});
   *
   *     // Target array deeply (but not strictly) equals `[1, 2]`
   *     expect([1, 2]).to.deep.equal([1, 2]);
   *     expect([1, 2]).to.not.equal([1, 2]);
   *
   * Add `.not` earlier in the chain to negate `.equal`. However, it's often
   * best to assert that the target is equal to its expected value, rather than
   * not equal to one of countless unexpected values.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.equal(2); // Not recommended
   *
   * `.equal` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(1).to.equal(2, 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.equal(2);
   *
   * The aliases `.equals` and `eq` can be used interchangeably with `.equal`.
   *
   * @name equal
   * @alias equals
   * @alias eq
   * @param {Mixed} val
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertEqual (val, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object');
    if (flag(this, 'deep')) {
      var prevLockSsfi = flag(this, 'lockSsfi');
      flag(this, 'lockSsfi', true);
      this.eql(val);
      flag(this, 'lockSsfi', prevLockSsfi);
    } else {
      this.assert(
          val === obj
        , 'expected #{this} to equal #{exp}'
        , 'expected #{this} to not equal #{exp}'
        , val
        , this._obj
        , true
      );
    }
  }

  Assertion.addMethod('equal', assertEqual);
  Assertion.addMethod('equals', assertEqual);
  Assertion.addMethod('eq', assertEqual);

  /**
   * ### .eql(obj[, msg])
   *
   * Asserts that the target is deeply equal to the given `obj`. See the
   * `deep-eql` project page for info on the deep equality algorithm:
   * https://github.com/chaijs/deep-eql.
   *
   *     // Target object is deeply (but not strictly) equal to {a: 1}
   *     expect({a: 1}).to.eql({a: 1}).but.not.equal({a: 1});
   *
   *     // Target array is deeply (but not strictly) equal to [1, 2]
   *     expect([1, 2]).to.eql([1, 2]).but.not.equal([1, 2]);
   *
   * Add `.not` earlier in the chain to negate `.eql`. However, it's often best
   * to assert that the target is deeply equal to its expected value, rather
   * than not deeply equal to one of countless unexpected values.
   *
   *     expect({a: 1}).to.eql({a: 1}); // Recommended
   *     expect({a: 1}).to.not.eql({b: 2}); // Not recommended
   *
   * `.eql` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect({a: 1}).to.eql({b: 2}, 'nooo why fail??');
   *     expect({a: 1}, 'nooo why fail??').to.eql({b: 2});
   *
   * The alias `.eqls` can be used interchangeably with `.eql`.
   *
   * The `.deep.equal` assertion is almost identical to `.eql` but with one
   * difference: `.deep.equal` causes deep equality comparisons to also be used
   * for any other assertions that follow in the chain.
   *
   * @name eql
   * @alias eqls
   * @param {Mixed} obj
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertEql(obj, msg) {
    if (msg) flag(this, 'message', msg);
    this.assert(
        _.eql(obj, flag(this, 'object'))
      , 'expected #{this} to deeply equal #{exp}'
      , 'expected #{this} to not deeply equal #{exp}'
      , obj
      , this._obj
      , true
    );
  }

  Assertion.addMethod('eql', assertEql);
  Assertion.addMethod('eqls', assertEql);

  /**
   * ### .above(n[, msg])
   *
   * Asserts that the target is a number or a date greater than the given number or date `n` respectively.
   * However, it's often best to assert that the target is equal to its expected
   * value.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.be.above(1); // Not recommended
   *
   * Add `.lengthOf` earlier in the chain to assert that the target's `length`
   * or `size` is greater than the given number `n`.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.have.lengthOf.above(2); // Not recommended
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3); // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf.above(2); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.above`.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(1).to.not.be.above(2); // Not recommended
   *
   * `.above` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(1).to.be.above(2, 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.be.above(2);
   *
   * The aliases `.gt` and `.greaterThan` can be used interchangeably with
   * `.above`.
   *
   * @name above
   * @alias gt
   * @alias greaterThan
   * @param {Number} n
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertAbove (n, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , doLength = flag(this, 'doLength')
      , flagMsg = flag(this, 'message')
      , msgPrefix = ((flagMsg) ? flagMsg + ': ' : '')
      , ssfi = flag(this, 'ssfi')
      , objType = _.type(obj).toLowerCase()
      , nType = _.type(n).toLowerCase()
      , errorMessage
      , shouldThrow = true;

    if (doLength && objType !== 'map' && objType !== 'set') {
      new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
    }

    if (!doLength && (objType === 'date' && nType !== 'date')) {
      errorMessage = msgPrefix + 'the argument to above must be a date';
    } else if (nType !== 'number' && (doLength || objType === 'number')) {
      errorMessage = msgPrefix + 'the argument to above must be a number';
    } else if (!doLength && (objType !== 'date' && objType !== 'number')) {
      var printObj = (objType === 'string') ? "'" + obj + "'" : obj;
      errorMessage = msgPrefix + 'expected ' + printObj + ' to be a number or a date';
    } else {
      shouldThrow = false;
    }

    if (shouldThrow) {
      throw new AssertionError(errorMessage, undefined, ssfi);
    }

    if (doLength) {
      var descriptor = 'length'
        , itemsCount;
      if (objType === 'map' || objType === 'set') {
        descriptor = 'size';
        itemsCount = obj.size;
      } else {
        itemsCount = obj.length;
      }
      this.assert(
          itemsCount > n
        , 'expected #{this} to have a ' + descriptor + ' above #{exp} but got #{act}'
        , 'expected #{this} to not have a ' + descriptor + ' above #{exp}'
        , n
        , itemsCount
      );
    } else {
      this.assert(
          obj > n
        , 'expected #{this} to be above #{exp}'
        , 'expected #{this} to be at most #{exp}'
        , n
      );
    }
  }

  Assertion.addMethod('above', assertAbove);
  Assertion.addMethod('gt', assertAbove);
  Assertion.addMethod('greaterThan', assertAbove);

  /**
   * ### .least(n[, msg])
   *
   * Asserts that the target is a number or a date greater than or equal to the given
   * number or date `n` respectively. However, it's often best to assert that the target is equal to
   * its expected value.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.be.at.least(1); // Not recommended
   *     expect(2).to.be.at.least(2); // Not recommended
   *
   * Add `.lengthOf` earlier in the chain to assert that the target's `length`
   * or `size` is greater than or equal to the given number `n`.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.have.lengthOf.at.least(2); // Not recommended
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3); // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf.at.least(2); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.least`.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.at.least(2); // Not recommended
   *
   * `.least` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(1).to.be.at.least(2, 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.be.at.least(2);
   *
   * The aliases `.gte` and `.greaterThanOrEqual` can be used interchangeably with
   * `.least`.
   *
   * @name least
   * @alias gte
   * @alias greaterThanOrEqual
   * @param {Number} n
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertLeast (n, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , doLength = flag(this, 'doLength')
      , flagMsg = flag(this, 'message')
      , msgPrefix = ((flagMsg) ? flagMsg + ': ' : '')
      , ssfi = flag(this, 'ssfi')
      , objType = _.type(obj).toLowerCase()
      , nType = _.type(n).toLowerCase()
      , errorMessage
      , shouldThrow = true;

    if (doLength && objType !== 'map' && objType !== 'set') {
      new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
    }

    if (!doLength && (objType === 'date' && nType !== 'date')) {
      errorMessage = msgPrefix + 'the argument to least must be a date';
    } else if (nType !== 'number' && (doLength || objType === 'number')) {
      errorMessage = msgPrefix + 'the argument to least must be a number';
    } else if (!doLength && (objType !== 'date' && objType !== 'number')) {
      var printObj = (objType === 'string') ? "'" + obj + "'" : obj;
      errorMessage = msgPrefix + 'expected ' + printObj + ' to be a number or a date';
    } else {
      shouldThrow = false;
    }

    if (shouldThrow) {
      throw new AssertionError(errorMessage, undefined, ssfi);
    }

    if (doLength) {
      var descriptor = 'length'
        , itemsCount;
      if (objType === 'map' || objType === 'set') {
        descriptor = 'size';
        itemsCount = obj.size;
      } else {
        itemsCount = obj.length;
      }
      this.assert(
          itemsCount >= n
        , 'expected #{this} to have a ' + descriptor + ' at least #{exp} but got #{act}'
        , 'expected #{this} to have a ' + descriptor + ' below #{exp}'
        , n
        , itemsCount
      );
    } else {
      this.assert(
          obj >= n
        , 'expected #{this} to be at least #{exp}'
        , 'expected #{this} to be below #{exp}'
        , n
      );
    }
  }

  Assertion.addMethod('least', assertLeast);
  Assertion.addMethod('gte', assertLeast);
  Assertion.addMethod('greaterThanOrEqual', assertLeast);

  /**
   * ### .below(n[, msg])
   *
   * Asserts that the target is a number or a date less than the given number or date `n` respectively.
   * However, it's often best to assert that the target is equal to its expected
   * value.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.be.below(2); // Not recommended
   *
   * Add `.lengthOf` earlier in the chain to assert that the target's `length`
   * or `size` is less than the given number `n`.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.have.lengthOf.below(4); // Not recommended
   *
   *     expect([1, 2, 3]).to.have.length(3); // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf.below(4); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.below`.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.not.be.below(1); // Not recommended
   *
   * `.below` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(2).to.be.below(1, 'nooo why fail??');
   *     expect(2, 'nooo why fail??').to.be.below(1);
   *
   * The aliases `.lt` and `.lessThan` can be used interchangeably with
   * `.below`.
   *
   * @name below
   * @alias lt
   * @alias lessThan
   * @param {Number} n
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertBelow (n, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , doLength = flag(this, 'doLength')
      , flagMsg = flag(this, 'message')
      , msgPrefix = ((flagMsg) ? flagMsg + ': ' : '')
      , ssfi = flag(this, 'ssfi')
      , objType = _.type(obj).toLowerCase()
      , nType = _.type(n).toLowerCase()
      , errorMessage
      , shouldThrow = true;

    if (doLength && objType !== 'map' && objType !== 'set') {
      new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
    }

    if (!doLength && (objType === 'date' && nType !== 'date')) {
      errorMessage = msgPrefix + 'the argument to below must be a date';
    } else if (nType !== 'number' && (doLength || objType === 'number')) {
      errorMessage = msgPrefix + 'the argument to below must be a number';
    } else if (!doLength && (objType !== 'date' && objType !== 'number')) {
      var printObj = (objType === 'string') ? "'" + obj + "'" : obj;
      errorMessage = msgPrefix + 'expected ' + printObj + ' to be a number or a date';
    } else {
      shouldThrow = false;
    }

    if (shouldThrow) {
      throw new AssertionError(errorMessage, undefined, ssfi);
    }

    if (doLength) {
      var descriptor = 'length'
        , itemsCount;
      if (objType === 'map' || objType === 'set') {
        descriptor = 'size';
        itemsCount = obj.size;
      } else {
        itemsCount = obj.length;
      }
      this.assert(
          itemsCount < n
        , 'expected #{this} to have a ' + descriptor + ' below #{exp} but got #{act}'
        , 'expected #{this} to not have a ' + descriptor + ' below #{exp}'
        , n
        , itemsCount
      );
    } else {
      this.assert(
          obj < n
        , 'expected #{this} to be below #{exp}'
        , 'expected #{this} to be at least #{exp}'
        , n
      );
    }
  }

  Assertion.addMethod('below', assertBelow);
  Assertion.addMethod('lt', assertBelow);
  Assertion.addMethod('lessThan', assertBelow);

  /**
   * ### .most(n[, msg])
   *
   * Asserts that the target is a number or a date less than or equal to the given number
   * or date `n` respectively. However, it's often best to assert that the target is equal to its
   * expected value.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.be.at.most(2); // Not recommended
   *     expect(1).to.be.at.most(1); // Not recommended
   *
   * Add `.lengthOf` earlier in the chain to assert that the target's `length`
   * or `size` is less than or equal to the given number `n`.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.have.lengthOf.at.most(4); // Not recommended
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3); // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf.at.most(4); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.most`.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.not.be.at.most(1); // Not recommended
   *
   * `.most` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(2).to.be.at.most(1, 'nooo why fail??');
   *     expect(2, 'nooo why fail??').to.be.at.most(1);
   *
   * The aliases `.lte` and `.lessThanOrEqual` can be used interchangeably with
   * `.most`.
   *
   * @name most
   * @alias lte
   * @alias lessThanOrEqual
   * @param {Number} n
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertMost (n, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , doLength = flag(this, 'doLength')
      , flagMsg = flag(this, 'message')
      , msgPrefix = ((flagMsg) ? flagMsg + ': ' : '')
      , ssfi = flag(this, 'ssfi')
      , objType = _.type(obj).toLowerCase()
      , nType = _.type(n).toLowerCase()
      , errorMessage
      , shouldThrow = true;

    if (doLength && objType !== 'map' && objType !== 'set') {
      new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
    }

    if (!doLength && (objType === 'date' && nType !== 'date')) {
      errorMessage = msgPrefix + 'the argument to most must be a date';
    } else if (nType !== 'number' && (doLength || objType === 'number')) {
      errorMessage = msgPrefix + 'the argument to most must be a number';
    } else if (!doLength && (objType !== 'date' && objType !== 'number')) {
      var printObj = (objType === 'string') ? "'" + obj + "'" : obj;
      errorMessage = msgPrefix + 'expected ' + printObj + ' to be a number or a date';
    } else {
      shouldThrow = false;
    }

    if (shouldThrow) {
      throw new AssertionError(errorMessage, undefined, ssfi);
    }

    if (doLength) {
      var descriptor = 'length'
        , itemsCount;
      if (objType === 'map' || objType === 'set') {
        descriptor = 'size';
        itemsCount = obj.size;
      } else {
        itemsCount = obj.length;
      }
      this.assert(
          itemsCount <= n
        , 'expected #{this} to have a ' + descriptor + ' at most #{exp} but got #{act}'
        , 'expected #{this} to have a ' + descriptor + ' above #{exp}'
        , n
        , itemsCount
      );
    } else {
      this.assert(
          obj <= n
        , 'expected #{this} to be at most #{exp}'
        , 'expected #{this} to be above #{exp}'
        , n
      );
    }
  }

  Assertion.addMethod('most', assertMost);
  Assertion.addMethod('lte', assertMost);
  Assertion.addMethod('lessThanOrEqual', assertMost);

  /**
   * ### .within(start, finish[, msg])
   *
   * Asserts that the target is a number or a date greater than or equal to the given
   * number or date `start`, and less than or equal to the given number or date `finish` respectively.
   * However, it's often best to assert that the target is equal to its expected
   * value.
   *
   *     expect(2).to.equal(2); // Recommended
   *     expect(2).to.be.within(1, 3); // Not recommended
   *     expect(2).to.be.within(2, 3); // Not recommended
   *     expect(2).to.be.within(1, 2); // Not recommended
   *
   * Add `.lengthOf` earlier in the chain to assert that the target's `length`
   * or `size` is greater than or equal to the given number `start`, and less
   * than or equal to the given number `finish`.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.have.lengthOf.within(2, 4); // Not recommended
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3); // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf.within(2, 4); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.within`.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.within(2, 4); // Not recommended
   *
   * `.within` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect(4).to.be.within(1, 3, 'nooo why fail??');
   *     expect(4, 'nooo why fail??').to.be.within(1, 3);
   *
   * @name within
   * @param {Number} start lower bound inclusive
   * @param {Number} finish upper bound inclusive
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  Assertion.addMethod('within', function (start, finish, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , doLength = flag(this, 'doLength')
      , flagMsg = flag(this, 'message')
      , msgPrefix = ((flagMsg) ? flagMsg + ': ' : '')
      , ssfi = flag(this, 'ssfi')
      , objType = _.type(obj).toLowerCase()
      , startType = _.type(start).toLowerCase()
      , finishType = _.type(finish).toLowerCase()
      , errorMessage
      , shouldThrow = true
      , range = (startType === 'date' && finishType === 'date')
          ? start.toUTCString() + '..' + finish.toUTCString()
          : start + '..' + finish;

    if (doLength && objType !== 'map' && objType !== 'set') {
      new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
    }

    if (!doLength && (objType === 'date' && (startType !== 'date' || finishType !== 'date'))) {
      errorMessage = msgPrefix + 'the arguments to within must be dates';
    } else if ((startType !== 'number' || finishType !== 'number') && (doLength || objType === 'number')) {
      errorMessage = msgPrefix + 'the arguments to within must be numbers';
    } else if (!doLength && (objType !== 'date' && objType !== 'number')) {
      var printObj = (objType === 'string') ? "'" + obj + "'" : obj;
      errorMessage = msgPrefix + 'expected ' + printObj + ' to be a number or a date';
    } else {
      shouldThrow = false;
    }

    if (shouldThrow) {
      throw new AssertionError(errorMessage, undefined, ssfi);
    }

    if (doLength) {
      var descriptor = 'length'
        , itemsCount;
      if (objType === 'map' || objType === 'set') {
        descriptor = 'size';
        itemsCount = obj.size;
      } else {
        itemsCount = obj.length;
      }
      this.assert(
          itemsCount >= start && itemsCount <= finish
        , 'expected #{this} to have a ' + descriptor + ' within ' + range
        , 'expected #{this} to not have a ' + descriptor + ' within ' + range
      );
    } else {
      this.assert(
          obj >= start && obj <= finish
        , 'expected #{this} to be within ' + range
        , 'expected #{this} to not be within ' + range
      );
    }
  });

  /**
   * ### .instanceof(constructor[, msg])
   *
   * Asserts that the target is an instance of the given `constructor`.
   *
   *     function Cat () { }
   *
   *     expect(new Cat()).to.be.an.instanceof(Cat);
   *     expect([1, 2]).to.be.an.instanceof(Array);
   *
   * Add `.not` earlier in the chain to negate `.instanceof`.
   *
   *     expect({a: 1}).to.not.be.an.instanceof(Array);
   *
   * `.instanceof` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect(1).to.be.an.instanceof(Array, 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.be.an.instanceof(Array);
   *
   * Due to limitations in ES5, `.instanceof` may not always work as expected
   * when using a transpiler such as Babel or TypeScript. In particular, it may
   * produce unexpected results when subclassing built-in object such as
   * `Array`, `Error`, and `Map`. See your transpiler's docs for details:
   *
   * - ([Babel](https://babeljs.io/docs/usage/caveats/#classes))
   * - ([TypeScript](https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work))
   *
   * The alias `.instanceOf` can be used interchangeably with `.instanceof`.
   *
   * @name instanceof
   * @param {Constructor} constructor
   * @param {String} msg _optional_
   * @alias instanceOf
   * @namespace BDD
   * @api public
   */

  function assertInstanceOf (constructor, msg) {
    if (msg) flag(this, 'message', msg);

    var target = flag(this, 'object')
    var ssfi = flag(this, 'ssfi');
    var flagMsg = flag(this, 'message');

    try {
      var isInstanceOf = target instanceof constructor;
    } catch (err) {
      if (err instanceof TypeError) {
        flagMsg = flagMsg ? flagMsg + ': ' : '';
        throw new AssertionError(
          flagMsg + 'The instanceof assertion needs a constructor but '
            + _.type(constructor) + ' was given.',
          undefined,
          ssfi
        );
      }
      throw err;
    }

    var name = _.getName(constructor);
    if (name === null) {
      name = 'an unnamed constructor';
    }

    this.assert(
        isInstanceOf
      , 'expected #{this} to be an instance of ' + name
      , 'expected #{this} to not be an instance of ' + name
    );
  };

  Assertion.addMethod('instanceof', assertInstanceOf);
  Assertion.addMethod('instanceOf', assertInstanceOf);

  /**
   * ### .property(name[, val[, msg]])
   *
   * Asserts that the target has a property with the given key `name`.
   *
   *     expect({a: 1}).to.have.property('a');
   *
   * When `val` is provided, `.property` also asserts that the property's value
   * is equal to the given `val`.
   *
   *     expect({a: 1}).to.have.property('a', 1);
   *
   * By default, strict (`===`) equality is used. Add `.deep` earlier in the
   * chain to use deep equality instead. See the `deep-eql` project page for
   * info on the deep equality algorithm: https://github.com/chaijs/deep-eql.
   *
   *     // Target object deeply (but not strictly) has property `x: {a: 1}`
   *     expect({x: {a: 1}}).to.have.deep.property('x', {a: 1});
   *     expect({x: {a: 1}}).to.not.have.property('x', {a: 1});
   *
   * The target's enumerable and non-enumerable properties are always included
   * in the search. By default, both own and inherited properties are included.
   * Add `.own` earlier in the chain to exclude inherited properties from the
   * search.
   *
   *     Object.prototype.b = 2;
   *
   *     expect({a: 1}).to.have.own.property('a');
   *     expect({a: 1}).to.have.own.property('a', 1);
   *     expect({a: 1}).to.have.property('b');
   *     expect({a: 1}).to.not.have.own.property('b');
   *
   * `.deep` and `.own` can be combined.
   *
   *     expect({x: {a: 1}}).to.have.deep.own.property('x', {a: 1});
   *
   * Add `.nested` earlier in the chain to enable dot- and bracket-notation when
   * referencing nested properties.
   *
   *     expect({a: {b: ['x', 'y']}}).to.have.nested.property('a.b[1]');
   *     expect({a: {b: ['x', 'y']}}).to.have.nested.property('a.b[1]', 'y');
   *
   * If `.` or `[]` are part of an actual property name, they can be escaped by
   * adding two backslashes before them.
   *
   *     expect({'.a': {'[b]': 'x'}}).to.have.nested.property('\\.a.\\[b\\]');
   *
   * `.deep` and `.nested` can be combined.
   *
   *     expect({a: {b: [{c: 3}]}})
   *       .to.have.deep.nested.property('a.b[0]', {c: 3});
   *
   * `.own` and `.nested` cannot be combined.
   *
   * Add `.not` earlier in the chain to negate `.property`.
   *
   *     expect({a: 1}).to.not.have.property('b');
   *
   * However, it's dangerous to negate `.property` when providing `val`. The
   * problem is that it creates uncertain expectations by asserting that the
   * target either doesn't have a property with the given key `name`, or that it
   * does have a property with the given key `name` but its value isn't equal to
   * the given `val`. It's often best to identify the exact output that's
   * expected, and then write an assertion that only accepts that exact output.
   *
   * When the target isn't expected to have a property with the given key
   * `name`, it's often best to assert exactly that.
   *
   *     expect({b: 2}).to.not.have.property('a'); // Recommended
   *     expect({b: 2}).to.not.have.property('a', 1); // Not recommended
   *
   * When the target is expected to have a property with the given key `name`,
   * it's often best to assert that the property has its expected value, rather
   * than asserting that it doesn't have one of many unexpected values.
   *
   *     expect({a: 3}).to.have.property('a', 3); // Recommended
   *     expect({a: 3}).to.not.have.property('a', 1); // Not recommended
   *
   * `.property` changes the target of any assertions that follow in the chain
   * to be the value of the property from the original target object.
   *
   *     expect({a: 1}).to.have.property('a').that.is.a('number');
   *
   * `.property` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`. When not providing `val`, only use the
   * second form.
   *
   *     // Recommended
   *     expect({a: 1}).to.have.property('a', 2, 'nooo why fail??');
   *     expect({a: 1}, 'nooo why fail??').to.have.property('a', 2);
   *     expect({a: 1}, 'nooo why fail??').to.have.property('b');
   *
   *     // Not recommended
   *     expect({a: 1}).to.have.property('b', undefined, 'nooo why fail??');
   *
   * The above assertion isn't the same thing as not providing `val`. Instead,
   * it's asserting that the target object has a `b` property that's equal to
   * `undefined`.
   *
   * The assertions `.ownProperty` and `.haveOwnProperty` can be used
   * interchangeably with `.own.property`.
   *
   * @name property
   * @param {String} name
   * @param {Mixed} val (optional)
   * @param {String} msg _optional_
   * @returns value of property for chaining
   * @namespace BDD
   * @api public
   */

  function assertProperty (name, val, msg) {
    if (msg) flag(this, 'message', msg);

    var isNested = flag(this, 'nested')
      , isOwn = flag(this, 'own')
      , flagMsg = flag(this, 'message')
      , obj = flag(this, 'object')
      , ssfi = flag(this, 'ssfi')
      , nameType = typeof name;

    flagMsg = flagMsg ? flagMsg + ': ' : '';

    if (isNested) {
      if (nameType !== 'string') {
        throw new AssertionError(
          flagMsg + 'the argument to property must be a string when using nested syntax',
          undefined,
          ssfi
        );
      }
    } else {
      if (nameType !== 'string' && nameType !== 'number' && nameType !== 'symbol') {
        throw new AssertionError(
          flagMsg + 'the argument to property must be a string, number, or symbol',
          undefined,
          ssfi
        );
      }
    }

    if (isNested && isOwn) {
      throw new AssertionError(
        flagMsg + 'The "nested" and "own" flags cannot be combined.',
        undefined,
        ssfi
      );
    }

    if (obj === null || obj === undefined) {
      throw new AssertionError(
        flagMsg + 'Target cannot be null or undefined.',
        undefined,
        ssfi
      );
    }

    var isDeep = flag(this, 'deep')
      , negate = flag(this, 'negate')
      , pathInfo = isNested ? _.getPathInfo(obj, name) : null
      , value = isNested ? pathInfo.value : obj[name];

    var descriptor = '';
    if (isDeep) descriptor += 'deep ';
    if (isOwn) descriptor += 'own ';
    if (isNested) descriptor += 'nested ';
    descriptor += 'property ';

    var hasProperty;
    if (isOwn) hasProperty = Object.prototype.hasOwnProperty.call(obj, name);
    else if (isNested) hasProperty = pathInfo.exists;
    else hasProperty = _.hasProperty(obj, name);

    // When performing a negated assertion for both name and val, merely having
    // a property with the given name isn't enough to cause the assertion to
    // fail. It must both have a property with the given name, and the value of
    // that property must equal the given val. Therefore, skip this assertion in
    // favor of the next.
    if (!negate || arguments.length === 1) {
      this.assert(
          hasProperty
        , 'expected #{this} to have ' + descriptor + _.inspect(name)
        , 'expected #{this} to not have ' + descriptor + _.inspect(name));
    }

    if (arguments.length > 1) {
      this.assert(
          hasProperty && (isDeep ? _.eql(val, value) : val === value)
        , 'expected #{this} to have ' + descriptor + _.inspect(name) + ' of #{exp}, but got #{act}'
        , 'expected #{this} to not have ' + descriptor + _.inspect(name) + ' of #{act}'
        , val
        , value
      );
    }

    flag(this, 'object', value);
  }

  Assertion.addMethod('property', assertProperty);

  function assertOwnProperty (name, value, msg) {
    flag(this, 'own', true);
    assertProperty.apply(this, arguments);
  }

  Assertion.addMethod('ownProperty', assertOwnProperty);
  Assertion.addMethod('haveOwnProperty', assertOwnProperty);

  /**
   * ### .ownPropertyDescriptor(name[, descriptor[, msg]])
   *
   * Asserts that the target has its own property descriptor with the given key
   * `name`. Enumerable and non-enumerable properties are included in the
   * search.
   *
   *     expect({a: 1}).to.have.ownPropertyDescriptor('a');
   *
   * When `descriptor` is provided, `.ownPropertyDescriptor` also asserts that
   * the property's descriptor is deeply equal to the given `descriptor`. See
   * the `deep-eql` project page for info on the deep equality algorithm:
   * https://github.com/chaijs/deep-eql.
   *
   *     expect({a: 1}).to.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 1,
   *     });
   *
   * Add `.not` earlier in the chain to negate `.ownPropertyDescriptor`.
   *
   *     expect({a: 1}).to.not.have.ownPropertyDescriptor('b');
   *
   * However, it's dangerous to negate `.ownPropertyDescriptor` when providing
   * a `descriptor`. The problem is that it creates uncertain expectations by
   * asserting that the target either doesn't have a property descriptor with
   * the given key `name`, or that it does have a property descriptor with the
   * given key `name` but it’s not deeply equal to the given `descriptor`. It's
   * often best to identify the exact output that's expected, and then write an
   * assertion that only accepts that exact output.
   *
   * When the target isn't expected to have a property descriptor with the given
   * key `name`, it's often best to assert exactly that.
   *
   *     // Recommended
   *     expect({b: 2}).to.not.have.ownPropertyDescriptor('a');
   *
   *     // Not recommended
   *     expect({b: 2}).to.not.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 1,
   *     });
   *
   * When the target is expected to have a property descriptor with the given
   * key `name`, it's often best to assert that the property has its expected
   * descriptor, rather than asserting that it doesn't have one of many
   * unexpected descriptors.
   *
   *     // Recommended
   *     expect({a: 3}).to.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 3,
   *     });
   *
   *     // Not recommended
   *     expect({a: 3}).to.not.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 1,
   *     });
   *
   * `.ownPropertyDescriptor` changes the target of any assertions that follow
   * in the chain to be the value of the property descriptor from the original
   * target object.
   *
   *     expect({a: 1}).to.have.ownPropertyDescriptor('a')
   *       .that.has.property('enumerable', true);
   *
   * `.ownPropertyDescriptor` accepts an optional `msg` argument which is a
   * custom error message to show when the assertion fails. The message can also
   * be given as the second argument to `expect`. When not providing
   * `descriptor`, only use the second form.
   *
   *     // Recommended
   *     expect({a: 1}).to.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 2,
   *     }, 'nooo why fail??');
   *
   *     // Recommended
   *     expect({a: 1}, 'nooo why fail??').to.have.ownPropertyDescriptor('a', {
   *       configurable: true,
   *       enumerable: true,
   *       writable: true,
   *       value: 2,
   *     });
   *
   *     // Recommended
   *     expect({a: 1}, 'nooo why fail??').to.have.ownPropertyDescriptor('b');
   *
   *     // Not recommended
   *     expect({a: 1})
   *       .to.have.ownPropertyDescriptor('b', undefined, 'nooo why fail??');
   *
   * The above assertion isn't the same thing as not providing `descriptor`.
   * Instead, it's asserting that the target object has a `b` property
   * descriptor that's deeply equal to `undefined`.
   *
   * The alias `.haveOwnPropertyDescriptor` can be used interchangeably with
   * `.ownPropertyDescriptor`.
   *
   * @name ownPropertyDescriptor
   * @alias haveOwnPropertyDescriptor
   * @param {String} name
   * @param {Object} descriptor _optional_
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertOwnPropertyDescriptor (name, descriptor, msg) {
    if (typeof descriptor === 'string') {
      msg = descriptor;
      descriptor = null;
    }
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object');
    var actualDescriptor = Object.getOwnPropertyDescriptor(Object(obj), name);
    if (actualDescriptor && descriptor) {
      this.assert(
          _.eql(descriptor, actualDescriptor)
        , 'expected the own property descriptor for ' + _.inspect(name) + ' on #{this} to match ' + _.inspect(descriptor) + ', got ' + _.inspect(actualDescriptor)
        , 'expected the own property descriptor for ' + _.inspect(name) + ' on #{this} to not match ' + _.inspect(descriptor)
        , descriptor
        , actualDescriptor
        , true
      );
    } else {
      this.assert(
          actualDescriptor
        , 'expected #{this} to have an own property descriptor for ' + _.inspect(name)
        , 'expected #{this} to not have an own property descriptor for ' + _.inspect(name)
      );
    }
    flag(this, 'object', actualDescriptor);
  }

  Assertion.addMethod('ownPropertyDescriptor', assertOwnPropertyDescriptor);
  Assertion.addMethod('haveOwnPropertyDescriptor', assertOwnPropertyDescriptor);

  /**
   * ### .lengthOf(n[, msg])
   *
   * Asserts that the target's `length` or `size` is equal to the given number
   * `n`.
   *
   *     expect([1, 2, 3]).to.have.lengthOf(3);
   *     expect('foo').to.have.lengthOf(3);
   *     expect(new Set([1, 2, 3])).to.have.lengthOf(3);
   *     expect(new Map([['a', 1], ['b', 2], ['c', 3]])).to.have.lengthOf(3);
   *
   * Add `.not` earlier in the chain to negate `.lengthOf`. However, it's often
   * best to assert that the target's `length` property is equal to its expected
   * value, rather than not equal to one of many unexpected values.
   *
   *     expect('foo').to.have.lengthOf(3); // Recommended
   *     expect('foo').to.not.have.lengthOf(4); // Not recommended
   *
   * `.lengthOf` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect([1, 2, 3]).to.have.lengthOf(2, 'nooo why fail??');
   *     expect([1, 2, 3], 'nooo why fail??').to.have.lengthOf(2);
   *
   * `.lengthOf` can also be used as a language chain, causing all `.above`,
   * `.below`, `.least`, `.most`, and `.within` assertions that follow in the
   * chain to use the target's `length` property as the target. However, it's
   * often best to assert that the target's `length` property is equal to its
   * expected length, rather than asserting that its `length` property falls
   * within some range of values.
   *
   *     // Recommended
   *     expect([1, 2, 3]).to.have.lengthOf(3);
   *
   *     // Not recommended
   *     expect([1, 2, 3]).to.have.lengthOf.above(2);
   *     expect([1, 2, 3]).to.have.lengthOf.below(4);
   *     expect([1, 2, 3]).to.have.lengthOf.at.least(3);
   *     expect([1, 2, 3]).to.have.lengthOf.at.most(3);
   *     expect([1, 2, 3]).to.have.lengthOf.within(2,4);
   *
   * Due to a compatibility issue, the alias `.length` can't be chained directly
   * off of an uninvoked method such as `.a`. Therefore, `.length` can't be used
   * interchangeably with `.lengthOf` in every situation. It's recommended to
   * always use `.lengthOf` instead of `.length`.
   *
   *     expect([1, 2, 3]).to.have.a.length(3); // incompatible; throws error
   *     expect([1, 2, 3]).to.have.a.lengthOf(3);  // passes as expected
   *
   * @name lengthOf
   * @alias length
   * @param {Number} n
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertLengthChain () {
    flag(this, 'doLength', true);
  }

  function assertLength (n, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , objType = _.type(obj).toLowerCase()
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi')
      , descriptor = 'length'
      , itemsCount;

    switch (objType) {
      case 'map':
      case 'set':
        descriptor = 'size';
        itemsCount = obj.size;
        break;
      default:
        new Assertion(obj, flagMsg, ssfi, true).to.have.property('length');
        itemsCount = obj.length;
    }

    this.assert(
        itemsCount == n
      , 'expected #{this} to have a ' + descriptor + ' of #{exp} but got #{act}'
      , 'expected #{this} to not have a ' + descriptor + ' of #{act}'
      , n
      , itemsCount
    );
  }

  Assertion.addChainableMethod('length', assertLength, assertLengthChain);
  Assertion.addChainableMethod('lengthOf', assertLength, assertLengthChain);

  /**
   * ### .match(re[, msg])
   *
   * Asserts that the target matches the given regular expression `re`.
   *
   *     expect('foobar').to.match(/^foo/);
   *
   * Add `.not` earlier in the chain to negate `.match`.
   *
   *     expect('foobar').to.not.match(/taco/);
   *
   * `.match` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect('foobar').to.match(/taco/, 'nooo why fail??');
   *     expect('foobar', 'nooo why fail??').to.match(/taco/);
   *
   * The alias `.matches` can be used interchangeably with `.match`.
   *
   * @name match
   * @alias matches
   * @param {RegExp} re
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */
  function assertMatch(re, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object');
    this.assert(
        re.exec(obj)
      , 'expected #{this} to match ' + re
      , 'expected #{this} not to match ' + re
    );
  }

  Assertion.addMethod('match', assertMatch);
  Assertion.addMethod('matches', assertMatch);

  /**
   * ### .string(str[, msg])
   *
   * Asserts that the target string contains the given substring `str`.
   *
   *     expect('foobar').to.have.string('bar');
   *
   * Add `.not` earlier in the chain to negate `.string`.
   *
   *     expect('foobar').to.not.have.string('taco');
   *
   * `.string` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect('foobar').to.have.string('taco', 'nooo why fail??');
   *     expect('foobar', 'nooo why fail??').to.have.string('taco');
   *
   * @name string
   * @param {String} str
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  Assertion.addMethod('string', function (str, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');
    new Assertion(obj, flagMsg, ssfi, true).is.a('string');

    this.assert(
        ~obj.indexOf(str)
      , 'expected #{this} to contain ' + _.inspect(str)
      , 'expected #{this} to not contain ' + _.inspect(str)
    );
  });

  /**
   * ### .keys(key1[, key2[, ...]])
   *
   * Asserts that the target object, array, map, or set has the given keys. Only
   * the target's own inherited properties are included in the search.
   *
   * When the target is an object or array, keys can be provided as one or more
   * string arguments, a single array argument, or a single object argument. In
   * the latter case, only the keys in the given object matter; the values are
   * ignored.
   *
   *     expect({a: 1, b: 2}).to.have.all.keys('a', 'b');
   *     expect(['x', 'y']).to.have.all.keys(0, 1);
   *
   *     expect({a: 1, b: 2}).to.have.all.keys(['a', 'b']);
   *     expect(['x', 'y']).to.have.all.keys([0, 1]);
   *
   *     expect({a: 1, b: 2}).to.have.all.keys({a: 4, b: 5}); // ignore 4 and 5
   *     expect(['x', 'y']).to.have.all.keys({0: 4, 1: 5}); // ignore 4 and 5
   *
   * When the target is a map or set, each key must be provided as a separate
   * argument.
   *
   *     expect(new Map([['a', 1], ['b', 2]])).to.have.all.keys('a', 'b');
   *     expect(new Set(['a', 'b'])).to.have.all.keys('a', 'b');
   *
   * Because `.keys` does different things based on the target's type, it's
   * important to check the target's type before using `.keys`. See the `.a` doc
   * for info on testing a target's type.
   *
   *     expect({a: 1, b: 2}).to.be.an('object').that.has.all.keys('a', 'b');
   *
   * By default, strict (`===`) equality is used to compare keys of maps and
   * sets. Add `.deep` earlier in the chain to use deep equality instead. See
   * the `deep-eql` project page for info on the deep equality algorithm:
   * https://github.com/chaijs/deep-eql.
   *
   *     // Target set deeply (but not strictly) has key `{a: 1}`
   *     expect(new Set([{a: 1}])).to.have.all.deep.keys([{a: 1}]);
   *     expect(new Set([{a: 1}])).to.not.have.all.keys([{a: 1}]);
   *
   * By default, the target must have all of the given keys and no more. Add
   * `.any` earlier in the chain to only require that the target have at least
   * one of the given keys. Also, add `.not` earlier in the chain to negate
   * `.keys`. It's often best to add `.any` when negating `.keys`, and to use
   * `.all` when asserting `.keys` without negation.
   *
   * When negating `.keys`, `.any` is preferred because `.not.any.keys` asserts
   * exactly what's expected of the output, whereas `.not.all.keys` creates
   * uncertain expectations.
   *
   *     // Recommended; asserts that target doesn't have any of the given keys
   *     expect({a: 1, b: 2}).to.not.have.any.keys('c', 'd');
   *
   *     // Not recommended; asserts that target doesn't have all of the given
   *     // keys but may or may not have some of them
   *     expect({a: 1, b: 2}).to.not.have.all.keys('c', 'd');
   *
   * When asserting `.keys` without negation, `.all` is preferred because
   * `.all.keys` asserts exactly what's expected of the output, whereas
   * `.any.keys` creates uncertain expectations.
   *
   *     // Recommended; asserts that target has all the given keys
   *     expect({a: 1, b: 2}).to.have.all.keys('a', 'b');
   *
   *     // Not recommended; asserts that target has at least one of the given
   *     // keys but may or may not have more of them
   *     expect({a: 1, b: 2}).to.have.any.keys('a', 'b');
   *
   * Note that `.all` is used by default when neither `.all` nor `.any` appear
   * earlier in the chain. However, it's often best to add `.all` anyway because
   * it improves readability.
   *
   *     // Both assertions are identical
   *     expect({a: 1, b: 2}).to.have.all.keys('a', 'b'); // Recommended
   *     expect({a: 1, b: 2}).to.have.keys('a', 'b'); // Not recommended
   *
   * Add `.include` earlier in the chain to require that the target's keys be a
   * superset of the expected keys, rather than identical sets.
   *
   *     // Target object's keys are a superset of ['a', 'b'] but not identical
   *     expect({a: 1, b: 2, c: 3}).to.include.all.keys('a', 'b');
   *     expect({a: 1, b: 2, c: 3}).to.not.have.all.keys('a', 'b');
   *
   * However, if `.any` and `.include` are combined, only the `.any` takes
   * effect. The `.include` is ignored in this case.
   *
   *     // Both assertions are identical
   *     expect({a: 1}).to.have.any.keys('a', 'b');
   *     expect({a: 1}).to.include.any.keys('a', 'b');
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect({a: 1}, 'nooo why fail??').to.have.key('b');
   *
   * The alias `.key` can be used interchangeably with `.keys`.
   *
   * @name keys
   * @alias key
   * @param {...String|Array|Object} keys
   * @namespace BDD
   * @api public
   */

  function assertKeys (keys) {
    var obj = flag(this, 'object')
      , objType = _.type(obj)
      , keysType = _.type(keys)
      , ssfi = flag(this, 'ssfi')
      , isDeep = flag(this, 'deep')
      , str
      , deepStr = ''
      , actual
      , ok = true
      , flagMsg = flag(this, 'message');

    flagMsg = flagMsg ? flagMsg + ': ' : '';
    var mixedArgsMsg = flagMsg + 'when testing keys against an object or an array you must give a single Array|Object|String argument or multiple String arguments';

    if (objType === 'Map' || objType === 'Set') {
      deepStr = isDeep ? 'deeply ' : '';
      actual = [];

      // Map and Set '.keys' aren't supported in IE 11. Therefore, use .forEach.
      obj.forEach(function (val, key) { actual.push(key) });

      if (keysType !== 'Array') {
        keys = Array.prototype.slice.call(arguments);
      }
    } else {
      actual = _.getOwnEnumerableProperties(obj);

      switch (keysType) {
        case 'Array':
          if (arguments.length > 1) {
            throw new AssertionError(mixedArgsMsg, undefined, ssfi);
          }
          break;
        case 'Object':
          if (arguments.length > 1) {
            throw new AssertionError(mixedArgsMsg, undefined, ssfi);
          }
          keys = Object.keys(keys);
          break;
        default:
          keys = Array.prototype.slice.call(arguments);
      }

      // Only stringify non-Symbols because Symbols would become "Symbol()"
      keys = keys.map(function (val) {
        return typeof val === 'symbol' ? val : String(val);
      });
    }

    if (!keys.length) {
      throw new AssertionError(flagMsg + 'keys required', undefined, ssfi);
    }

    var len = keys.length
      , any = flag(this, 'any')
      , all = flag(this, 'all')
      , expected = keys;

    if (!any && !all) {
      all = true;
    }

    // Has any
    if (any) {
      ok = expected.some(function(expectedKey) {
        return actual.some(function(actualKey) {
          if (isDeep) {
            return _.eql(expectedKey, actualKey);
          } else {
            return expectedKey === actualKey;
          }
        });
      });
    }

    // Has all
    if (all) {
      ok = expected.every(function(expectedKey) {
        return actual.some(function(actualKey) {
          if (isDeep) {
            return _.eql(expectedKey, actualKey);
          } else {
            return expectedKey === actualKey;
          }
        });
      });

      if (!flag(this, 'contains')) {
        ok = ok && keys.length == actual.length;
      }
    }

    // Key string
    if (len > 1) {
      keys = keys.map(function(key) {
        return _.inspect(key);
      });
      var last = keys.pop();
      if (all) {
        str = keys.join(', ') + ', and ' + last;
      }
      if (any) {
        str = keys.join(', ') + ', or ' + last;
      }
    } else {
      str = _.inspect(keys[0]);
    }

    // Form
    str = (len > 1 ? 'keys ' : 'key ') + str;

    // Have / include
    str = (flag(this, 'contains') ? 'contain ' : 'have ') + str;

    // Assertion
    this.assert(
        ok
      , 'expected #{this} to ' + deepStr + str
      , 'expected #{this} to not ' + deepStr + str
      , expected.slice(0).sort(_.compareByInspect)
      , actual.sort(_.compareByInspect)
      , true
    );
  }

  Assertion.addMethod('keys', assertKeys);
  Assertion.addMethod('key', assertKeys);

  /**
   * ### .throw([errorLike], [errMsgMatcher], [msg])
   *
   * When no arguments are provided, `.throw` invokes the target function and
   * asserts that an error is thrown.
   *
   *     var badFn = function () { throw new TypeError('Illegal salmon!'); };
   *
   *     expect(badFn).to.throw();
   *
   * When one argument is provided, and it's an error constructor, `.throw`
   * invokes the target function and asserts that an error is thrown that's an
   * instance of that error constructor.
   *
   *     var badFn = function () { throw new TypeError('Illegal salmon!'); };
   *
   *     expect(badFn).to.throw(TypeError);
   *
   * When one argument is provided, and it's an error instance, `.throw` invokes
   * the target function and asserts that an error is thrown that's strictly
   * (`===`) equal to that error instance.
   *
   *     var err = new TypeError('Illegal salmon!');
   *     var badFn = function () { throw err; };
   *
   *     expect(badFn).to.throw(err);
   *
   * When one argument is provided, and it's a string, `.throw` invokes the
   * target function and asserts that an error is thrown with a message that
   * contains that string.
   *
   *     var badFn = function () { throw new TypeError('Illegal salmon!'); };
   *
   *     expect(badFn).to.throw('salmon');
   *
   * When one argument is provided, and it's a regular expression, `.throw`
   * invokes the target function and asserts that an error is thrown with a
   * message that matches that regular expression.
   *
   *     var badFn = function () { throw new TypeError('Illegal salmon!'); };
   *
   *     expect(badFn).to.throw(/salmon/);
   *
   * When two arguments are provided, and the first is an error instance or
   * constructor, and the second is a string or regular expression, `.throw`
   * invokes the function and asserts that an error is thrown that fulfills both
   * conditions as described above.
   *
   *     var err = new TypeError('Illegal salmon!');
   *     var badFn = function () { throw err; };
   *
   *     expect(badFn).to.throw(TypeError, 'salmon');
   *     expect(badFn).to.throw(TypeError, /salmon/);
   *     expect(badFn).to.throw(err, 'salmon');
   *     expect(badFn).to.throw(err, /salmon/);
   *
   * Add `.not` earlier in the chain to negate `.throw`.
   *
   *     var goodFn = function () {};
   *
   *     expect(goodFn).to.not.throw();
   *
   * However, it's dangerous to negate `.throw` when providing any arguments.
   * The problem is that it creates uncertain expectations by asserting that the
   * target either doesn't throw an error, or that it throws an error but of a
   * different type than the given type, or that it throws an error of the given
   * type but with a message that doesn't include the given string. It's often
   * best to identify the exact output that's expected, and then write an
   * assertion that only accepts that exact output.
   *
   * When the target isn't expected to throw an error, it's often best to assert
   * exactly that.
   *
   *     var goodFn = function () {};
   *
   *     expect(goodFn).to.not.throw(); // Recommended
   *     expect(goodFn).to.not.throw(ReferenceError, 'x'); // Not recommended
   *
   * When the target is expected to throw an error, it's often best to assert
   * that the error is of its expected type, and has a message that includes an
   * expected string, rather than asserting that it doesn't have one of many
   * unexpected types, and doesn't have a message that includes some string.
   *
   *     var badFn = function () { throw new TypeError('Illegal salmon!'); };
   *
   *     expect(badFn).to.throw(TypeError, 'salmon'); // Recommended
   *     expect(badFn).to.not.throw(ReferenceError, 'x'); // Not recommended
   *
   * `.throw` changes the target of any assertions that follow in the chain to
   * be the error object that's thrown.
   *
   *     var err = new TypeError('Illegal salmon!');
   *     err.code = 42;
   *     var badFn = function () { throw err; };
   *
   *     expect(badFn).to.throw(TypeError).with.property('code', 42);
   *
   * `.throw` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`. When not providing two arguments, always use
   * the second form.
   *
   *     var goodFn = function () {};
   *
   *     expect(goodFn).to.throw(TypeError, 'x', 'nooo why fail??');
   *     expect(goodFn, 'nooo why fail??').to.throw();
   *
   * Due to limitations in ES5, `.throw` may not always work as expected when
   * using a transpiler such as Babel or TypeScript. In particular, it may
   * produce unexpected results when subclassing the built-in `Error` object and
   * then passing the subclassed constructor to `.throw`. See your transpiler's
   * docs for details:
   *
   * - ([Babel](https://babeljs.io/docs/usage/caveats/#classes))
   * - ([TypeScript](https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work))
   *
   * Beware of some common mistakes when using the `throw` assertion. One common
   * mistake is to accidentally invoke the function yourself instead of letting
   * the `throw` assertion invoke the function for you. For example, when
   * testing if a function named `fn` throws, provide `fn` instead of `fn()` as
   * the target for the assertion.
   *
   *     expect(fn).to.throw();     // Good! Tests `fn` as desired
   *     expect(fn()).to.throw();   // Bad! Tests result of `fn()`, not `fn`
   *
   * If you need to assert that your function `fn` throws when passed certain
   * arguments, then wrap a call to `fn` inside of another function.
   *
   *     expect(function () { fn(42); }).to.throw();  // Function expression
   *     expect(() => fn(42)).to.throw();             // ES6 arrow function
   *
   * Another common mistake is to provide an object method (or any stand-alone
   * function that relies on `this`) as the target of the assertion. Doing so is
   * problematic because the `this` context will be lost when the function is
   * invoked by `.throw`; there's no way for it to know what `this` is supposed
   * to be. There are two ways around this problem. One solution is to wrap the
   * method or function call inside of another function. Another solution is to
   * use `bind`.
   *
   *     expect(function () { cat.meow(); }).to.throw();  // Function expression
   *     expect(() => cat.meow()).to.throw();             // ES6 arrow function
   *     expect(cat.meow.bind(cat)).to.throw();           // Bind
   *
   * Finally, it's worth mentioning that it's a best practice in JavaScript to
   * only throw `Error` and derivatives of `Error` such as `ReferenceError`,
   * `TypeError`, and user-defined objects that extend `Error`. No other type of
   * value will generate a stack trace when initialized. With that said, the
   * `throw` assertion does technically support any type of value being thrown,
   * not just `Error` and its derivatives.
   *
   * The aliases `.throws` and `.Throw` can be used interchangeably with
   * `.throw`.
   *
   * @name throw
   * @alias throws
   * @alias Throw
   * @param {Error|ErrorConstructor} errorLike
   * @param {String|RegExp} errMsgMatcher error message
   * @param {String} msg _optional_
   * @see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Error_types
   * @returns error for chaining (null if no error)
   * @namespace BDD
   * @api public
   */

  function assertThrows (errorLike, errMsgMatcher, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , ssfi = flag(this, 'ssfi')
      , flagMsg = flag(this, 'message')
      , negate = flag(this, 'negate') || false;
    new Assertion(obj, flagMsg, ssfi, true).is.a('function');

    if (errorLike instanceof RegExp || typeof errorLike === 'string') {
      errMsgMatcher = errorLike;
      errorLike = null;
    }

    var caughtErr;
    try {
      obj();
    } catch (err) {
      caughtErr = err;
    }

    // If we have the negate flag enabled and at least one valid argument it means we do expect an error
    // but we want it to match a given set of criteria
    var everyArgIsUndefined = errorLike === undefined && errMsgMatcher === undefined;

    // If we've got the negate flag enabled and both args, we should only fail if both aren't compatible
    // See Issue #551 and PR #683@GitHub
    var everyArgIsDefined = Boolean(errorLike && errMsgMatcher);
    var errorLikeFail = false;
    var errMsgMatcherFail = false;

    // Checking if error was thrown
    if (everyArgIsUndefined || !everyArgIsUndefined && !negate) {
      // We need this to display results correctly according to their types
      var errorLikeString = 'an error';
      if (errorLike instanceof Error) {
        errorLikeString = '#{exp}';
      } else if (errorLike) {
        errorLikeString = _.checkError.getConstructorName(errorLike);
      }

      this.assert(
          caughtErr
        , 'expected #{this} to throw ' + errorLikeString
        , 'expected #{this} to not throw an error but #{act} was thrown'
        , errorLike && errorLike.toString()
        , (caughtErr instanceof Error ?
            caughtErr.toString() : (typeof caughtErr === 'string' ? caughtErr : caughtErr &&
                                    _.checkError.getConstructorName(caughtErr)))
      );
    }

    if (errorLike && caughtErr) {
      // We should compare instances only if `errorLike` is an instance of `Error`
      if (errorLike instanceof Error) {
        var isCompatibleInstance = _.checkError.compatibleInstance(caughtErr, errorLike);

        if (isCompatibleInstance === negate) {
          // These checks were created to ensure we won't fail too soon when we've got both args and a negate
          // See Issue #551 and PR #683@GitHub
          if (everyArgIsDefined && negate) {
            errorLikeFail = true;
          } else {
            this.assert(
                negate
              , 'expected #{this} to throw #{exp} but #{act} was thrown'
              , 'expected #{this} to not throw #{exp}' + (caughtErr && !negate ? ' but #{act} was thrown' : '')
              , errorLike.toString()
              , caughtErr.toString()
            );
          }
        }
      }

      var isCompatibleConstructor = _.checkError.compatibleConstructor(caughtErr, errorLike);
      if (isCompatibleConstructor === negate) {
        if (everyArgIsDefined && negate) {
            errorLikeFail = true;
        } else {
          this.assert(
              negate
            , 'expected #{this} to throw #{exp} but #{act} was thrown'
            , 'expected #{this} to not throw #{exp}' + (caughtErr ? ' but #{act} was thrown' : '')
            , (errorLike instanceof Error ? errorLike.toString() : errorLike && _.checkError.getConstructorName(errorLike))
            , (caughtErr instanceof Error ? caughtErr.toString() : caughtErr && _.checkError.getConstructorName(caughtErr))
          );
        }
      }
    }

    if (caughtErr && errMsgMatcher !== undefined && errMsgMatcher !== null) {
      // Here we check compatible messages
      var placeholder = 'including';
      if (errMsgMatcher instanceof RegExp) {
        placeholder = 'matching'
      }

      var isCompatibleMessage = _.checkError.compatibleMessage(caughtErr, errMsgMatcher);
      if (isCompatibleMessage === negate) {
        if (everyArgIsDefined && negate) {
            errMsgMatcherFail = true;
        } else {
          this.assert(
            negate
            , 'expected #{this} to throw error ' + placeholder + ' #{exp} but got #{act}'
            , 'expected #{this} to throw error not ' + placeholder + ' #{exp}'
            ,  errMsgMatcher
            ,  _.checkError.getMessage(caughtErr)
          );
        }
      }
    }

    // If both assertions failed and both should've matched we throw an error
    if (errorLikeFail && errMsgMatcherFail) {
      this.assert(
        negate
        , 'expected #{this} to throw #{exp} but #{act} was thrown'
        , 'expected #{this} to not throw #{exp}' + (caughtErr ? ' but #{act} was thrown' : '')
        , (errorLike instanceof Error ? errorLike.toString() : errorLike && _.checkError.getConstructorName(errorLike))
        , (caughtErr instanceof Error ? caughtErr.toString() : caughtErr && _.checkError.getConstructorName(caughtErr))
      );
    }

    flag(this, 'object', caughtErr);
  };

  Assertion.addMethod('throw', assertThrows);
  Assertion.addMethod('throws', assertThrows);
  Assertion.addMethod('Throw', assertThrows);

  /**
   * ### .respondTo(method[, msg])
   *
   * When the target is a non-function object, `.respondTo` asserts that the
   * target has a method with the given name `method`. The method can be own or
   * inherited, and it can be enumerable or non-enumerable.
   *
   *     function Cat () {}
   *     Cat.prototype.meow = function () {};
   *
   *     expect(new Cat()).to.respondTo('meow');
   *
   * When the target is a function, `.respondTo` asserts that the target's
   * `prototype` property has a method with the given name `method`. Again, the
   * method can be own or inherited, and it can be enumerable or non-enumerable.
   *
   *     function Cat () {}
   *     Cat.prototype.meow = function () {};
   *
   *     expect(Cat).to.respondTo('meow');
   *
   * Add `.itself` earlier in the chain to force `.respondTo` to treat the
   * target as a non-function object, even if it's a function. Thus, it asserts
   * that the target has a method with the given name `method`, rather than
   * asserting that the target's `prototype` property has a method with the
   * given name `method`.
   *
   *     function Cat () {}
   *     Cat.prototype.meow = function () {};
   *     Cat.hiss = function () {};
   *
   *     expect(Cat).itself.to.respondTo('hiss').but.not.respondTo('meow');
   *
   * When not adding `.itself`, it's important to check the target's type before
   * using `.respondTo`. See the `.a` doc for info on checking a target's type.
   *
   *     function Cat () {}
   *     Cat.prototype.meow = function () {};
   *
   *     expect(new Cat()).to.be.an('object').that.respondsTo('meow');
   *
   * Add `.not` earlier in the chain to negate `.respondTo`.
   *
   *     function Dog () {}
   *     Dog.prototype.bark = function () {};
   *
   *     expect(new Dog()).to.not.respondTo('meow');
   *
   * `.respondTo` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect({}).to.respondTo('meow', 'nooo why fail??');
   *     expect({}, 'nooo why fail??').to.respondTo('meow');
   *
   * The alias `.respondsTo` can be used interchangeably with `.respondTo`.
   *
   * @name respondTo
   * @alias respondsTo
   * @param {String} method
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function respondTo (method, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , itself = flag(this, 'itself')
      , context = ('function' === typeof obj && !itself)
        ? obj.prototype[method]
        : obj[method];

    this.assert(
        'function' === typeof context
      , 'expected #{this} to respond to ' + _.inspect(method)
      , 'expected #{this} to not respond to ' + _.inspect(method)
    );
  }

  Assertion.addMethod('respondTo', respondTo);
  Assertion.addMethod('respondsTo', respondTo);

  /**
   * ### .itself
   *
   * Forces all `.respondTo` assertions that follow in the chain to behave as if
   * the target is a non-function object, even if it's a function. Thus, it
   * causes `.respondTo` to assert that the target has a method with the given
   * name, rather than asserting that the target's `prototype` property has a
   * method with the given name.
   *
   *     function Cat () {}
   *     Cat.prototype.meow = function () {};
   *     Cat.hiss = function () {};
   *
   *     expect(Cat).itself.to.respondTo('hiss').but.not.respondTo('meow');
   *
   * @name itself
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('itself', function () {
    flag(this, 'itself', true);
  });

  /**
   * ### .satisfy(matcher[, msg])
   *
   * Invokes the given `matcher` function with the target being passed as the
   * first argument, and asserts that the value returned is truthy.
   *
   *     expect(1).to.satisfy(function(num) {
   *       return num > 0;
   *     });
   *
   * Add `.not` earlier in the chain to negate `.satisfy`.
   *
   *     expect(1).to.not.satisfy(function(num) {
   *       return num > 2;
   *     });
   *
   * `.satisfy` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect(1).to.satisfy(function(num) {
   *       return num > 2;
   *     }, 'nooo why fail??');
   *
   *     expect(1, 'nooo why fail??').to.satisfy(function(num) {
   *       return num > 2;
   *     });
   *
   * The alias `.satisfies` can be used interchangeably with `.satisfy`.
   *
   * @name satisfy
   * @alias satisfies
   * @param {Function} matcher
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function satisfy (matcher, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object');
    var result = matcher(obj);
    this.assert(
        result
      , 'expected #{this} to satisfy ' + _.objDisplay(matcher)
      , 'expected #{this} to not satisfy' + _.objDisplay(matcher)
      , flag(this, 'negate') ? false : true
      , result
    );
  }

  Assertion.addMethod('satisfy', satisfy);
  Assertion.addMethod('satisfies', satisfy);

  /**
   * ### .closeTo(expected, delta[, msg])
   *
   * Asserts that the target is a number that's within a given +/- `delta` range
   * of the given number `expected`. However, it's often best to assert that the
   * target is equal to its expected value.
   *
   *     // Recommended
   *     expect(1.5).to.equal(1.5);
   *
   *     // Not recommended
   *     expect(1.5).to.be.closeTo(1, 0.5);
   *     expect(1.5).to.be.closeTo(2, 0.5);
   *     expect(1.5).to.be.closeTo(1, 1);
   *
   * Add `.not` earlier in the chain to negate `.closeTo`.
   *
   *     expect(1.5).to.equal(1.5); // Recommended
   *     expect(1.5).to.not.be.closeTo(3, 1); // Not recommended
   *
   * `.closeTo` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect(1.5).to.be.closeTo(3, 1, 'nooo why fail??');
   *     expect(1.5, 'nooo why fail??').to.be.closeTo(3, 1);
   *
   * The alias `.approximately` can be used interchangeably with `.closeTo`.
   *
   * @name closeTo
   * @alias approximately
   * @param {Number} expected
   * @param {Number} delta
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function closeTo(expected, delta, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');

    new Assertion(obj, flagMsg, ssfi, true).is.a('number');
    if (typeof expected !== 'number' || typeof delta !== 'number') {
      flagMsg = flagMsg ? flagMsg + ': ' : '';
      var deltaMessage = delta === undefined ? ", and a delta is required" : "";
      throw new AssertionError(
          flagMsg + 'the arguments to closeTo or approximately must be numbers' + deltaMessage,
          undefined,
          ssfi
      );
    }

    this.assert(
        Math.abs(obj - expected) <= delta
      , 'expected #{this} to be close to ' + expected + ' +/- ' + delta
      , 'expected #{this} not to be close to ' + expected + ' +/- ' + delta
    );
  }

  Assertion.addMethod('closeTo', closeTo);
  Assertion.addMethod('approximately', closeTo);

  // Note: Duplicates are ignored if testing for inclusion instead of sameness.
  function isSubsetOf(subset, superset, cmp, contains, ordered) {
    if (!contains) {
      if (subset.length !== superset.length) return false;
      superset = superset.slice();
    }

    return subset.every(function(elem, idx) {
      if (ordered) return cmp ? cmp(elem, superset[idx]) : elem === superset[idx];

      if (!cmp) {
        var matchIdx = superset.indexOf(elem);
        if (matchIdx === -1) return false;

        // Remove match from superset so not counted twice if duplicate in subset.
        if (!contains) superset.splice(matchIdx, 1);
        return true;
      }

      return superset.some(function(elem2, matchIdx) {
        if (!cmp(elem, elem2)) return false;

        // Remove match from superset so not counted twice if duplicate in subset.
        if (!contains) superset.splice(matchIdx, 1);
        return true;
      });
    });
  }

  /**
   * ### .members(set[, msg])
   *
   * Asserts that the target array has the same members as the given array
   * `set`.
   *
   *     expect([1, 2, 3]).to.have.members([2, 1, 3]);
   *     expect([1, 2, 2]).to.have.members([2, 1, 2]);
   *
   * By default, members are compared using strict (`===`) equality. Add `.deep`
   * earlier in the chain to use deep equality instead. See the `deep-eql`
   * project page for info on the deep equality algorithm:
   * https://github.com/chaijs/deep-eql.
   *
   *     // Target array deeply (but not strictly) has member `{a: 1}`
   *     expect([{a: 1}]).to.have.deep.members([{a: 1}]);
   *     expect([{a: 1}]).to.not.have.members([{a: 1}]);
   *
   * By default, order doesn't matter. Add `.ordered` earlier in the chain to
   * require that members appear in the same order.
   *
   *     expect([1, 2, 3]).to.have.ordered.members([1, 2, 3]);
   *     expect([1, 2, 3]).to.have.members([2, 1, 3])
   *       .but.not.ordered.members([2, 1, 3]);
   *
   * By default, both arrays must be the same size. Add `.include` earlier in
   * the chain to require that the target's members be a superset of the
   * expected members. Note that duplicates are ignored in the subset when
   * `.include` is added.
   *
   *     // Target array is a superset of [1, 2] but not identical
   *     expect([1, 2, 3]).to.include.members([1, 2]);
   *     expect([1, 2, 3]).to.not.have.members([1, 2]);
   *
   *     // Duplicates in the subset are ignored
   *     expect([1, 2, 3]).to.include.members([1, 2, 2, 2]);
   *
   * `.deep`, `.ordered`, and `.include` can all be combined. However, if
   * `.include` and `.ordered` are combined, the ordering begins at the start of
   * both arrays.
   *
   *     expect([{a: 1}, {b: 2}, {c: 3}])
   *       .to.include.deep.ordered.members([{a: 1}, {b: 2}])
   *       .but.not.include.deep.ordered.members([{b: 2}, {c: 3}]);
   *
   * Add `.not` earlier in the chain to negate `.members`. However, it's
   * dangerous to do so. The problem is that it creates uncertain expectations
   * by asserting that the target array doesn't have all of the same members as
   * the given array `set` but may or may not have some of them. It's often best
   * to identify the exact output that's expected, and then write an assertion
   * that only accepts that exact output.
   *
   *     expect([1, 2]).to.not.include(3).and.not.include(4); // Recommended
   *     expect([1, 2]).to.not.have.members([3, 4]); // Not recommended
   *
   * `.members` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`.
   *
   *     expect([1, 2]).to.have.members([1, 2, 3], 'nooo why fail??');
   *     expect([1, 2], 'nooo why fail??').to.have.members([1, 2, 3]);
   *
   * @name members
   * @param {Array} set
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  Assertion.addMethod('members', function (subset, msg) {
    if (msg) flag(this, 'message', msg);
    var obj = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');

    new Assertion(obj, flagMsg, ssfi, true).to.be.an('array');
    new Assertion(subset, flagMsg, ssfi, true).to.be.an('array');

    var contains = flag(this, 'contains');
    var ordered = flag(this, 'ordered');

    var subject, failMsg, failNegateMsg;

    if (contains) {
      subject = ordered ? 'an ordered superset' : 'a superset';
      failMsg = 'expected #{this} to be ' + subject + ' of #{exp}';
      failNegateMsg = 'expected #{this} to not be ' + subject + ' of #{exp}';
    } else {
      subject = ordered ? 'ordered members' : 'members';
      failMsg = 'expected #{this} to have the same ' + subject + ' as #{exp}';
      failNegateMsg = 'expected #{this} to not have the same ' + subject + ' as #{exp}';
    }

    var cmp = flag(this, 'deep') ? _.eql : undefined;

    this.assert(
        isSubsetOf(subset, obj, cmp, contains, ordered)
      , failMsg
      , failNegateMsg
      , subset
      , obj
      , true
    );
  });

  /**
   * ### .oneOf(list[, msg])
   *
   * Asserts that the target is a member of the given array `list`. However,
   * it's often best to assert that the target is equal to its expected value.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.be.oneOf([1, 2, 3]); // Not recommended
   *
   * Comparisons are performed using strict (`===`) equality.
   *
   * Add `.not` earlier in the chain to negate `.oneOf`.
   *
   *     expect(1).to.equal(1); // Recommended
   *     expect(1).to.not.be.oneOf([2, 3, 4]); // Not recommended
   *
   * It can also be chained with `.contain` or `.include`, which will work with
   * both arrays and strings:
   *
   *     expect('Today is sunny').to.contain.oneOf(['sunny', 'cloudy'])
   *     expect('Today is rainy').to.not.contain.oneOf(['sunny', 'cloudy'])
   *     expect([1,2,3]).to.contain.oneOf([3,4,5])
   *     expect([1,2,3]).to.not.contain.oneOf([4,5,6])
   *
   * `.oneOf` accepts an optional `msg` argument which is a custom error message
   * to show when the assertion fails. The message can also be given as the
   * second argument to `expect`.
   *
   *     expect(1).to.be.oneOf([2, 3, 4], 'nooo why fail??');
   *     expect(1, 'nooo why fail??').to.be.oneOf([2, 3, 4]);
   *
   * @name oneOf
   * @param {Array<*>} list
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function oneOf (list, msg) {
    if (msg) flag(this, 'message', msg);
    var expected = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi')
      , contains = flag(this, 'contains')
      , isDeep = flag(this, 'deep');
    new Assertion(list, flagMsg, ssfi, true).to.be.an('array');

    if (contains) {
      this.assert(
        list.some(function(possibility) { return expected.indexOf(possibility) > -1 })
        , 'expected #{this} to contain one of #{exp}'
        , 'expected #{this} to not contain one of #{exp}'
        , list
        , expected
      );
    } else {
      if (isDeep) {
        this.assert(
          list.some(function(possibility) { return _.eql(expected, possibility) })
          , 'expected #{this} to deeply equal one of #{exp}'
          , 'expected #{this} to deeply equal one of #{exp}'
          , list
          , expected
        );
      } else {
        this.assert(
          list.indexOf(expected) > -1
          , 'expected #{this} to be one of #{exp}'
          , 'expected #{this} to not be one of #{exp}'
          , list
          , expected
        );
      }
    }
  }

  Assertion.addMethod('oneOf', oneOf);

  /**
   * ### .change(subject[, prop[, msg]])
   *
   * When one argument is provided, `.change` asserts that the given function
   * `subject` returns a different value when it's invoked before the target
   * function compared to when it's invoked afterward. However, it's often best
   * to assert that `subject` is equal to its expected value.
   *
   *     var dots = ''
   *       , addDot = function () { dots += '.'; }
   *       , getDots = function () { return dots; };
   *
   *     // Recommended
   *     expect(getDots()).to.equal('');
   *     addDot();
   *     expect(getDots()).to.equal('.');
   *
   *     // Not recommended
   *     expect(addDot).to.change(getDots);
   *
   * When two arguments are provided, `.change` asserts that the value of the
   * given object `subject`'s `prop` property is different before invoking the
   * target function compared to afterward.
   *
   *     var myObj = {dots: ''}
   *       , addDot = function () { myObj.dots += '.'; };
   *
   *     // Recommended
   *     expect(myObj).to.have.property('dots', '');
   *     addDot();
   *     expect(myObj).to.have.property('dots', '.');
   *
   *     // Not recommended
   *     expect(addDot).to.change(myObj, 'dots');
   *
   * Strict (`===`) equality is used to compare before and after values.
   *
   * Add `.not` earlier in the chain to negate `.change`.
   *
   *     var dots = ''
   *       , noop = function () {}
   *       , getDots = function () { return dots; };
   *
   *     expect(noop).to.not.change(getDots);
   *
   *     var myObj = {dots: ''}
   *       , noop = function () {};
   *
   *     expect(noop).to.not.change(myObj, 'dots');
   *
   * `.change` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`. When not providing two arguments, always
   * use the second form.
   *
   *     var myObj = {dots: ''}
   *       , addDot = function () { myObj.dots += '.'; };
   *
   *     expect(addDot).to.not.change(myObj, 'dots', 'nooo why fail??');
   *
   *     var dots = ''
   *       , addDot = function () { dots += '.'; }
   *       , getDots = function () { return dots; };
   *
   *     expect(addDot, 'nooo why fail??').to.not.change(getDots);
   *
   * `.change` also causes all `.by` assertions that follow in the chain to
   * assert how much a numeric subject was increased or decreased by. However,
   * it's dangerous to use `.change.by`. The problem is that it creates
   * uncertain expectations by asserting that the subject either increases by
   * the given delta, or that it decreases by the given delta. It's often best
   * to identify the exact output that's expected, and then write an assertion
   * that only accepts that exact output.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; }
   *       , subtractTwo = function () { myObj.val -= 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(2); // Recommended
   *     expect(addTwo).to.change(myObj, 'val').by(2); // Not recommended
   *
   *     expect(subtractTwo).to.decrease(myObj, 'val').by(2); // Recommended
   *     expect(subtractTwo).to.change(myObj, 'val').by(2); // Not recommended
   *
   * The alias `.changes` can be used interchangeably with `.change`.
   *
   * @name change
   * @alias changes
   * @param {String} subject
   * @param {String} prop name _optional_
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertChanges (subject, prop, msg) {
    if (msg) flag(this, 'message', msg);
    var fn = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');
    new Assertion(fn, flagMsg, ssfi, true).is.a('function');

    var initial;
    if (!prop) {
      new Assertion(subject, flagMsg, ssfi, true).is.a('function');
      initial = subject();
    } else {
      new Assertion(subject, flagMsg, ssfi, true).to.have.property(prop);
      initial = subject[prop];
    }

    fn();

    var final = prop === undefined || prop === null ? subject() : subject[prop];
    var msgObj = prop === undefined || prop === null ? initial : '.' + prop;

    // This gets flagged because of the .by(delta) assertion
    flag(this, 'deltaMsgObj', msgObj);
    flag(this, 'initialDeltaValue', initial);
    flag(this, 'finalDeltaValue', final);
    flag(this, 'deltaBehavior', 'change');
    flag(this, 'realDelta', final !== initial);

    this.assert(
      initial !== final
      , 'expected ' + msgObj + ' to change'
      , 'expected ' + msgObj + ' to not change'
    );
  }

  Assertion.addMethod('change', assertChanges);
  Assertion.addMethod('changes', assertChanges);

  /**
   * ### .increase(subject[, prop[, msg]])
   *
   * When one argument is provided, `.increase` asserts that the given function
   * `subject` returns a greater number when it's invoked after invoking the
   * target function compared to when it's invoked beforehand. `.increase` also
   * causes all `.by` assertions that follow in the chain to assert how much
   * greater of a number is returned. It's often best to assert that the return
   * value increased by the expected amount, rather than asserting it increased
   * by any amount.
   *
   *     var val = 1
   *       , addTwo = function () { val += 2; }
   *       , getVal = function () { return val; };
   *
   *     expect(addTwo).to.increase(getVal).by(2); // Recommended
   *     expect(addTwo).to.increase(getVal); // Not recommended
   *
   * When two arguments are provided, `.increase` asserts that the value of the
   * given object `subject`'s `prop` property is greater after invoking the
   * target function compared to beforehand.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(2); // Recommended
   *     expect(addTwo).to.increase(myObj, 'val'); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.increase`. However, it's
   * dangerous to do so. The problem is that it creates uncertain expectations
   * by asserting that the subject either decreases, or that it stays the same.
   * It's often best to identify the exact output that's expected, and then
   * write an assertion that only accepts that exact output.
   *
   * When the subject is expected to decrease, it's often best to assert that it
   * decreased by the expected amount.
   *
   *     var myObj = {val: 1}
   *       , subtractTwo = function () { myObj.val -= 2; };
   *
   *     expect(subtractTwo).to.decrease(myObj, 'val').by(2); // Recommended
   *     expect(subtractTwo).to.not.increase(myObj, 'val'); // Not recommended
   *
   * When the subject is expected to stay the same, it's often best to assert
   * exactly that.
   *
   *     var myObj = {val: 1}
   *       , noop = function () {};
   *
   *     expect(noop).to.not.change(myObj, 'val'); // Recommended
   *     expect(noop).to.not.increase(myObj, 'val'); // Not recommended
   *
   * `.increase` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`. When not providing two arguments, always
   * use the second form.
   *
   *     var myObj = {val: 1}
   *       , noop = function () {};
   *
   *     expect(noop).to.increase(myObj, 'val', 'nooo why fail??');
   *
   *     var val = 1
   *       , noop = function () {}
   *       , getVal = function () { return val; };
   *
   *     expect(noop, 'nooo why fail??').to.increase(getVal);
   *
   * The alias `.increases` can be used interchangeably with `.increase`.
   *
   * @name increase
   * @alias increases
   * @param {String|Function} subject
   * @param {String} prop name _optional_
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertIncreases (subject, prop, msg) {
    if (msg) flag(this, 'message', msg);
    var fn = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');
    new Assertion(fn, flagMsg, ssfi, true).is.a('function');

    var initial;
    if (!prop) {
      new Assertion(subject, flagMsg, ssfi, true).is.a('function');
      initial = subject();
    } else {
      new Assertion(subject, flagMsg, ssfi, true).to.have.property(prop);
      initial = subject[prop];
    }

    // Make sure that the target is a number
    new Assertion(initial, flagMsg, ssfi, true).is.a('number');

    fn();

    var final = prop === undefined || prop === null ? subject() : subject[prop];
    var msgObj = prop === undefined || prop === null ? initial : '.' + prop;

    flag(this, 'deltaMsgObj', msgObj);
    flag(this, 'initialDeltaValue', initial);
    flag(this, 'finalDeltaValue', final);
    flag(this, 'deltaBehavior', 'increase');
    flag(this, 'realDelta', final - initial);

    this.assert(
      final - initial > 0
      , 'expected ' + msgObj + ' to increase'
      , 'expected ' + msgObj + ' to not increase'
    );
  }

  Assertion.addMethod('increase', assertIncreases);
  Assertion.addMethod('increases', assertIncreases);

  /**
   * ### .decrease(subject[, prop[, msg]])
   *
   * When one argument is provided, `.decrease` asserts that the given function
   * `subject` returns a lesser number when it's invoked after invoking the
   * target function compared to when it's invoked beforehand. `.decrease` also
   * causes all `.by` assertions that follow in the chain to assert how much
   * lesser of a number is returned. It's often best to assert that the return
   * value decreased by the expected amount, rather than asserting it decreased
   * by any amount.
   *
   *     var val = 1
   *       , subtractTwo = function () { val -= 2; }
   *       , getVal = function () { return val; };
   *
   *     expect(subtractTwo).to.decrease(getVal).by(2); // Recommended
   *     expect(subtractTwo).to.decrease(getVal); // Not recommended
   *
   * When two arguments are provided, `.decrease` asserts that the value of the
   * given object `subject`'s `prop` property is lesser after invoking the
   * target function compared to beforehand.
   *
   *     var myObj = {val: 1}
   *       , subtractTwo = function () { myObj.val -= 2; };
   *
   *     expect(subtractTwo).to.decrease(myObj, 'val').by(2); // Recommended
   *     expect(subtractTwo).to.decrease(myObj, 'val'); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.decrease`. However, it's
   * dangerous to do so. The problem is that it creates uncertain expectations
   * by asserting that the subject either increases, or that it stays the same.
   * It's often best to identify the exact output that's expected, and then
   * write an assertion that only accepts that exact output.
   *
   * When the subject is expected to increase, it's often best to assert that it
   * increased by the expected amount.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(2); // Recommended
   *     expect(addTwo).to.not.decrease(myObj, 'val'); // Not recommended
   *
   * When the subject is expected to stay the same, it's often best to assert
   * exactly that.
   *
   *     var myObj = {val: 1}
   *       , noop = function () {};
   *
   *     expect(noop).to.not.change(myObj, 'val'); // Recommended
   *     expect(noop).to.not.decrease(myObj, 'val'); // Not recommended
   *
   * `.decrease` accepts an optional `msg` argument which is a custom error
   * message to show when the assertion fails. The message can also be given as
   * the second argument to `expect`. When not providing two arguments, always
   * use the second form.
   *
   *     var myObj = {val: 1}
   *       , noop = function () {};
   *
   *     expect(noop).to.decrease(myObj, 'val', 'nooo why fail??');
   *
   *     var val = 1
   *       , noop = function () {}
   *       , getVal = function () { return val; };
   *
   *     expect(noop, 'nooo why fail??').to.decrease(getVal);
   *
   * The alias `.decreases` can be used interchangeably with `.decrease`.
   *
   * @name decrease
   * @alias decreases
   * @param {String|Function} subject
   * @param {String} prop name _optional_
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertDecreases (subject, prop, msg) {
    if (msg) flag(this, 'message', msg);
    var fn = flag(this, 'object')
      , flagMsg = flag(this, 'message')
      , ssfi = flag(this, 'ssfi');
    new Assertion(fn, flagMsg, ssfi, true).is.a('function');

    var initial;
    if (!prop) {
      new Assertion(subject, flagMsg, ssfi, true).is.a('function');
      initial = subject();
    } else {
      new Assertion(subject, flagMsg, ssfi, true).to.have.property(prop);
      initial = subject[prop];
    }

    // Make sure that the target is a number
    new Assertion(initial, flagMsg, ssfi, true).is.a('number');

    fn();

    var final = prop === undefined || prop === null ? subject() : subject[prop];
    var msgObj = prop === undefined || prop === null ? initial : '.' + prop;

    flag(this, 'deltaMsgObj', msgObj);
    flag(this, 'initialDeltaValue', initial);
    flag(this, 'finalDeltaValue', final);
    flag(this, 'deltaBehavior', 'decrease');
    flag(this, 'realDelta', initial - final);

    this.assert(
      final - initial < 0
      , 'expected ' + msgObj + ' to decrease'
      , 'expected ' + msgObj + ' to not decrease'
    );
  }

  Assertion.addMethod('decrease', assertDecreases);
  Assertion.addMethod('decreases', assertDecreases);

  /**
   * ### .by(delta[, msg])
   *
   * When following an `.increase` assertion in the chain, `.by` asserts that
   * the subject of the `.increase` assertion increased by the given `delta`.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(2);
   *
   * When following a `.decrease` assertion in the chain, `.by` asserts that the
   * subject of the `.decrease` assertion decreased by the given `delta`.
   *
   *     var myObj = {val: 1}
   *       , subtractTwo = function () { myObj.val -= 2; };
   *
   *     expect(subtractTwo).to.decrease(myObj, 'val').by(2);
   *
   * When following a `.change` assertion in the chain, `.by` asserts that the
   * subject of the `.change` assertion either increased or decreased by the
   * given `delta`. However, it's dangerous to use `.change.by`. The problem is
   * that it creates uncertain expectations. It's often best to identify the
   * exact output that's expected, and then write an assertion that only accepts
   * that exact output.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; }
   *       , subtractTwo = function () { myObj.val -= 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(2); // Recommended
   *     expect(addTwo).to.change(myObj, 'val').by(2); // Not recommended
   *
   *     expect(subtractTwo).to.decrease(myObj, 'val').by(2); // Recommended
   *     expect(subtractTwo).to.change(myObj, 'val').by(2); // Not recommended
   *
   * Add `.not` earlier in the chain to negate `.by`. However, it's often best
   * to assert that the subject changed by its expected delta, rather than
   * asserting that it didn't change by one of countless unexpected deltas.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; };
   *
   *     // Recommended
   *     expect(addTwo).to.increase(myObj, 'val').by(2);
   *
   *     // Not recommended
   *     expect(addTwo).to.increase(myObj, 'val').but.not.by(3);
   *
   * `.by` accepts an optional `msg` argument which is a custom error message to
   * show when the assertion fails. The message can also be given as the second
   * argument to `expect`.
   *
   *     var myObj = {val: 1}
   *       , addTwo = function () { myObj.val += 2; };
   *
   *     expect(addTwo).to.increase(myObj, 'val').by(3, 'nooo why fail??');
   *     expect(addTwo, 'nooo why fail??').to.increase(myObj, 'val').by(3);
   *
   * @name by
   * @param {Number} delta
   * @param {String} msg _optional_
   * @namespace BDD
   * @api public
   */

  function assertDelta(delta, msg) {
    if (msg) flag(this, 'message', msg);

    var msgObj = flag(this, 'deltaMsgObj');
    var initial = flag(this, 'initialDeltaValue');
    var final = flag(this, 'finalDeltaValue');
    var behavior = flag(this, 'deltaBehavior');
    var realDelta = flag(this, 'realDelta');

    var expression;
    if (behavior === 'change') {
      expression = Math.abs(final - initial) === Math.abs(delta);
    } else {
      expression = realDelta === Math.abs(delta);
    }

    this.assert(
      expression
      , 'expected ' + msgObj + ' to ' + behavior + ' by ' + delta
      , 'expected ' + msgObj + ' to not ' + behavior + ' by ' + delta
    );
  }

  Assertion.addMethod('by', assertDelta);

  /**
   * ### .extensible
   *
   * Asserts that the target is extensible, which means that new properties can
   * be added to it. Primitives are never extensible.
   *
   *     expect({a: 1}).to.be.extensible;
   *
   * Add `.not` earlier in the chain to negate `.extensible`.
   *
   *     var nonExtensibleObject = Object.preventExtensions({})
   *       , sealedObject = Object.seal({})
   *       , frozenObject = Object.freeze({});
   *
   *     expect(nonExtensibleObject).to.not.be.extensible;
   *     expect(sealedObject).to.not.be.extensible;
   *     expect(frozenObject).to.not.be.extensible;
   *     expect(1).to.not.be.extensible;
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect(1, 'nooo why fail??').to.be.extensible;
   *
   * @name extensible
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('extensible', function() {
    var obj = flag(this, 'object');

    // In ES5, if the argument to this method is a primitive, then it will cause a TypeError.
    // In ES6, a non-object argument will be treated as if it was a non-extensible ordinary object, simply return false.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible
    // The following provides ES6 behavior for ES5 environments.

    var isExtensible = obj === Object(obj) && Object.isExtensible(obj);

    this.assert(
      isExtensible
      , 'expected #{this} to be extensible'
      , 'expected #{this} to not be extensible'
    );
  });

  /**
   * ### .sealed
   *
   * Asserts that the target is sealed, which means that new properties can't be
   * added to it, and its existing properties can't be reconfigured or deleted.
   * However, it's possible that its existing properties can still be reassigned
   * to different values. Primitives are always sealed.
   *
   *     var sealedObject = Object.seal({});
   *     var frozenObject = Object.freeze({});
   *
   *     expect(sealedObject).to.be.sealed;
   *     expect(frozenObject).to.be.sealed;
   *     expect(1).to.be.sealed;
   *
   * Add `.not` earlier in the chain to negate `.sealed`.
   *
   *     expect({a: 1}).to.not.be.sealed;
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect({a: 1}, 'nooo why fail??').to.be.sealed;
   *
   * @name sealed
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('sealed', function() {
    var obj = flag(this, 'object');

    // In ES5, if the argument to this method is a primitive, then it will cause a TypeError.
    // In ES6, a non-object argument will be treated as if it was a sealed ordinary object, simply return true.
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isSealed
    // The following provides ES6 behavior for ES5 environments.

    var isSealed = obj === Object(obj) ? Object.isSealed(obj) : true;

    this.assert(
      isSealed
      , 'expected #{this} to be sealed'
      , 'expected #{this} to not be sealed'
    );
  });

  /**
   * ### .frozen
   *
   * Asserts that the target is frozen, which means that new properties can't be
   * added to it, and its existing properties can't be reassigned to different
   * values, reconfigured, or deleted. Primitives are always frozen.
   *
   *     var frozenObject = Object.freeze({});
   *
   *     expect(frozenObject).to.be.frozen;
   *     expect(1).to.be.frozen;
   *
   * Add `.not` earlier in the chain to negate `.frozen`.
   *
   *     expect({a: 1}).to.not.be.frozen;
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect({a: 1}, 'nooo why fail??').to.be.frozen;
   *
   * @name frozen
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('frozen', function() {
    var obj = flag(this, 'object');

    // In ES5, if the argument to this method is a primitive, then it will cause a TypeError.
    // In ES6, a non-object argument will be treated as if it was a frozen ordinary object, simply return true.
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/isFrozen
    // The following provides ES6 behavior for ES5 environments.

    var isFrozen = obj === Object(obj) ? Object.isFrozen(obj) : true;

    this.assert(
      isFrozen
      , 'expected #{this} to be frozen'
      , 'expected #{this} to not be frozen'
    );
  });

  /**
   * ### .finite
   *
   * Asserts that the target is a number, and isn't `NaN` or positive/negative
   * `Infinity`.
   *
   *     expect(1).to.be.finite;
   *
   * Add `.not` earlier in the chain to negate `.finite`. However, it's
   * dangerous to do so. The problem is that it creates uncertain expectations
   * by asserting that the subject either isn't a number, or that it's `NaN`, or
   * that it's positive `Infinity`, or that it's negative `Infinity`. It's often
   * best to identify the exact output that's expected, and then write an
   * assertion that only accepts that exact output.
   *
   * When the target isn't expected to be a number, it's often best to assert
   * that it's the expected type, rather than asserting that it isn't one of
   * many unexpected types.
   *
   *     expect('foo').to.be.a('string'); // Recommended
   *     expect('foo').to.not.be.finite; // Not recommended
   *
   * When the target is expected to be `NaN`, it's often best to assert exactly
   * that.
   *
   *     expect(NaN).to.be.NaN; // Recommended
   *     expect(NaN).to.not.be.finite; // Not recommended
   *
   * When the target is expected to be positive infinity, it's often best to
   * assert exactly that.
   *
   *     expect(Infinity).to.equal(Infinity); // Recommended
   *     expect(Infinity).to.not.be.finite; // Not recommended
   *
   * When the target is expected to be negative infinity, it's often best to
   * assert exactly that.
   *
   *     expect(-Infinity).to.equal(-Infinity); // Recommended
   *     expect(-Infinity).to.not.be.finite; // Not recommended
   *
   * A custom error message can be given as the second argument to `expect`.
   *
   *     expect('foo', 'nooo why fail??').to.be.finite;
   *
   * @name finite
   * @namespace BDD
   * @api public
   */

  Assertion.addProperty('finite', function(msg) {
    var obj = flag(this, 'object');

    this.assert(
        typeof obj === 'number' && isFinite(obj)
      , 'expected #{this} to be a finite number'
      , 'expected #{this} to not be a finite number'
    );
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/interface/assert.js":
/*!********************************************************!*\
  !*** ./node_modules/chai/lib/chai/interface/assert.js ***!
  \********************************************************/
/***/ ((module) => {

/*!
 * chai
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

module.exports = function (chai, util) {
  /*!
   * Chai dependencies.
   */

  var Assertion = chai.Assertion
    , flag = util.flag;

  /*!
   * Module export.
   */

  /**
   * ### assert(expression, message)
   *
   * Write your own test expressions.
   *
   *     assert('foo' !== 'bar', 'foo is not bar');
   *     assert(Array.isArray([]), 'empty arrays are arrays');
   *
   * @param {Mixed} expression to test for truthiness
   * @param {String} message to display on error
   * @name assert
   * @namespace Assert
   * @api public
   */

  var assert = chai.assert = function (express, errmsg) {
    var test = new Assertion(null, null, chai.assert, true);
    test.assert(
        express
      , errmsg
      , '[ negation message unavailable ]'
    );
  };

  /**
   * ### .fail([message])
   * ### .fail(actual, expected, [message], [operator])
   *
   * Throw a failure. Node.js `assert` module-compatible.
   *
   *     assert.fail();
   *     assert.fail("custom error message");
   *     assert.fail(1, 2);
   *     assert.fail(1, 2, "custom error message");
   *     assert.fail(1, 2, "custom error message", ">");
   *     assert.fail(1, 2, undefined, ">");
   *
   * @name fail
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @param {String} operator
   * @namespace Assert
   * @api public
   */

  assert.fail = function (actual, expected, message, operator) {
    if (arguments.length < 2) {
        // Comply with Node's fail([message]) interface

        message = actual;
        actual = undefined;
    }

    message = message || 'assert.fail()';
    throw new chai.AssertionError(message, {
        actual: actual
      , expected: expected
      , operator: operator
    }, assert.fail);
  };

  /**
   * ### .isOk(object, [message])
   *
   * Asserts that `object` is truthy.
   *
   *     assert.isOk('everything', 'everything is ok');
   *     assert.isOk(false, 'this will fail');
   *
   * @name isOk
   * @alias ok
   * @param {Mixed} object to test
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isOk = function (val, msg) {
    new Assertion(val, msg, assert.isOk, true).is.ok;
  };

  /**
   * ### .isNotOk(object, [message])
   *
   * Asserts that `object` is falsy.
   *
   *     assert.isNotOk('everything', 'this will fail');
   *     assert.isNotOk(false, 'this will pass');
   *
   * @name isNotOk
   * @alias notOk
   * @param {Mixed} object to test
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotOk = function (val, msg) {
    new Assertion(val, msg, assert.isNotOk, true).is.not.ok;
  };

  /**
   * ### .equal(actual, expected, [message])
   *
   * Asserts non-strict equality (`==`) of `actual` and `expected`.
   *
   *     assert.equal(3, '3', '== coerces values to strings');
   *
   * @name equal
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.equal = function (act, exp, msg) {
    var test = new Assertion(act, msg, assert.equal, true);

    test.assert(
        exp == flag(test, 'object')
      , 'expected #{this} to equal #{exp}'
      , 'expected #{this} to not equal #{act}'
      , exp
      , act
      , true
    );
  };

  /**
   * ### .notEqual(actual, expected, [message])
   *
   * Asserts non-strict inequality (`!=`) of `actual` and `expected`.
   *
   *     assert.notEqual(3, 4, 'these numbers are not equal');
   *
   * @name notEqual
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notEqual = function (act, exp, msg) {
    var test = new Assertion(act, msg, assert.notEqual, true);

    test.assert(
        exp != flag(test, 'object')
      , 'expected #{this} to not equal #{exp}'
      , 'expected #{this} to equal #{act}'
      , exp
      , act
      , true
    );
  };

  /**
   * ### .strictEqual(actual, expected, [message])
   *
   * Asserts strict equality (`===`) of `actual` and `expected`.
   *
   *     assert.strictEqual(true, true, 'these booleans are strictly equal');
   *
   * @name strictEqual
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.strictEqual = function (act, exp, msg) {
    new Assertion(act, msg, assert.strictEqual, true).to.equal(exp);
  };

  /**
   * ### .notStrictEqual(actual, expected, [message])
   *
   * Asserts strict inequality (`!==`) of `actual` and `expected`.
   *
   *     assert.notStrictEqual(3, '3', 'no coercion for strict equality');
   *
   * @name notStrictEqual
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notStrictEqual = function (act, exp, msg) {
    new Assertion(act, msg, assert.notStrictEqual, true).to.not.equal(exp);
  };

  /**
   * ### .deepEqual(actual, expected, [message])
   *
   * Asserts that `actual` is deeply equal to `expected`.
   *
   *     assert.deepEqual({ tea: 'green' }, { tea: 'green' });
   *
   * @name deepEqual
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @alias deepStrictEqual
   * @namespace Assert
   * @api public
   */

  assert.deepEqual = assert.deepStrictEqual = function (act, exp, msg) {
    new Assertion(act, msg, assert.deepEqual, true).to.eql(exp);
  };

  /**
   * ### .notDeepEqual(actual, expected, [message])
   *
   * Assert that `actual` is not deeply equal to `expected`.
   *
   *     assert.notDeepEqual({ tea: 'green' }, { tea: 'jasmine' });
   *
   * @name notDeepEqual
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepEqual = function (act, exp, msg) {
    new Assertion(act, msg, assert.notDeepEqual, true).to.not.eql(exp);
  };

   /**
   * ### .isAbove(valueToCheck, valueToBeAbove, [message])
   *
   * Asserts `valueToCheck` is strictly greater than (>) `valueToBeAbove`.
   *
   *     assert.isAbove(5, 2, '5 is strictly greater than 2');
   *
   * @name isAbove
   * @param {Mixed} valueToCheck
   * @param {Mixed} valueToBeAbove
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isAbove = function (val, abv, msg) {
    new Assertion(val, msg, assert.isAbove, true).to.be.above(abv);
  };

   /**
   * ### .isAtLeast(valueToCheck, valueToBeAtLeast, [message])
   *
   * Asserts `valueToCheck` is greater than or equal to (>=) `valueToBeAtLeast`.
   *
   *     assert.isAtLeast(5, 2, '5 is greater or equal to 2');
   *     assert.isAtLeast(3, 3, '3 is greater or equal to 3');
   *
   * @name isAtLeast
   * @param {Mixed} valueToCheck
   * @param {Mixed} valueToBeAtLeast
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isAtLeast = function (val, atlst, msg) {
    new Assertion(val, msg, assert.isAtLeast, true).to.be.least(atlst);
  };

   /**
   * ### .isBelow(valueToCheck, valueToBeBelow, [message])
   *
   * Asserts `valueToCheck` is strictly less than (<) `valueToBeBelow`.
   *
   *     assert.isBelow(3, 6, '3 is strictly less than 6');
   *
   * @name isBelow
   * @param {Mixed} valueToCheck
   * @param {Mixed} valueToBeBelow
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isBelow = function (val, blw, msg) {
    new Assertion(val, msg, assert.isBelow, true).to.be.below(blw);
  };

   /**
   * ### .isAtMost(valueToCheck, valueToBeAtMost, [message])
   *
   * Asserts `valueToCheck` is less than or equal to (<=) `valueToBeAtMost`.
   *
   *     assert.isAtMost(3, 6, '3 is less than or equal to 6');
   *     assert.isAtMost(4, 4, '4 is less than or equal to 4');
   *
   * @name isAtMost
   * @param {Mixed} valueToCheck
   * @param {Mixed} valueToBeAtMost
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isAtMost = function (val, atmst, msg) {
    new Assertion(val, msg, assert.isAtMost, true).to.be.most(atmst);
  };

  /**
   * ### .isTrue(value, [message])
   *
   * Asserts that `value` is true.
   *
   *     var teaServed = true;
   *     assert.isTrue(teaServed, 'the tea has been served');
   *
   * @name isTrue
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isTrue = function (val, msg) {
    new Assertion(val, msg, assert.isTrue, true).is['true'];
  };

  /**
   * ### .isNotTrue(value, [message])
   *
   * Asserts that `value` is not true.
   *
   *     var tea = 'tasty chai';
   *     assert.isNotTrue(tea, 'great, time for tea!');
   *
   * @name isNotTrue
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotTrue = function (val, msg) {
    new Assertion(val, msg, assert.isNotTrue, true).to.not.equal(true);
  };

  /**
   * ### .isFalse(value, [message])
   *
   * Asserts that `value` is false.
   *
   *     var teaServed = false;
   *     assert.isFalse(teaServed, 'no tea yet? hmm...');
   *
   * @name isFalse
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isFalse = function (val, msg) {
    new Assertion(val, msg, assert.isFalse, true).is['false'];
  };

  /**
   * ### .isNotFalse(value, [message])
   *
   * Asserts that `value` is not false.
   *
   *     var tea = 'tasty chai';
   *     assert.isNotFalse(tea, 'great, time for tea!');
   *
   * @name isNotFalse
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotFalse = function (val, msg) {
    new Assertion(val, msg, assert.isNotFalse, true).to.not.equal(false);
  };

  /**
   * ### .isNull(value, [message])
   *
   * Asserts that `value` is null.
   *
   *     assert.isNull(err, 'there was no error');
   *
   * @name isNull
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNull = function (val, msg) {
    new Assertion(val, msg, assert.isNull, true).to.equal(null);
  };

  /**
   * ### .isNotNull(value, [message])
   *
   * Asserts that `value` is not null.
   *
   *     var tea = 'tasty chai';
   *     assert.isNotNull(tea, 'great, time for tea!');
   *
   * @name isNotNull
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotNull = function (val, msg) {
    new Assertion(val, msg, assert.isNotNull, true).to.not.equal(null);
  };

  /**
   * ### .isNaN
   *
   * Asserts that value is NaN.
   *
   *     assert.isNaN(NaN, 'NaN is NaN');
   *
   * @name isNaN
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNaN = function (val, msg) {
    new Assertion(val, msg, assert.isNaN, true).to.be.NaN;
  };

  /**
   * ### .isNotNaN
   *
   * Asserts that value is not NaN.
   *
   *     assert.isNotNaN(4, '4 is not NaN');
   *
   * @name isNotNaN
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */
  assert.isNotNaN = function (val, msg) {
    new Assertion(val, msg, assert.isNotNaN, true).not.to.be.NaN;
  };

  /**
   * ### .exists
   *
   * Asserts that the target is neither `null` nor `undefined`.
   *
   *     var foo = 'hi';
   *
   *     assert.exists(foo, 'foo is neither `null` nor `undefined`');
   *
   * @name exists
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.exists = function (val, msg) {
    new Assertion(val, msg, assert.exists, true).to.exist;
  };

  /**
   * ### .notExists
   *
   * Asserts that the target is either `null` or `undefined`.
   *
   *     var bar = null
   *       , baz;
   *
   *     assert.notExists(bar);
   *     assert.notExists(baz, 'baz is either null or undefined');
   *
   * @name notExists
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notExists = function (val, msg) {
    new Assertion(val, msg, assert.notExists, true).to.not.exist;
  };

  /**
   * ### .isUndefined(value, [message])
   *
   * Asserts that `value` is `undefined`.
   *
   *     var tea;
   *     assert.isUndefined(tea, 'no tea defined');
   *
   * @name isUndefined
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isUndefined = function (val, msg) {
    new Assertion(val, msg, assert.isUndefined, true).to.equal(undefined);
  };

  /**
   * ### .isDefined(value, [message])
   *
   * Asserts that `value` is not `undefined`.
   *
   *     var tea = 'cup of chai';
   *     assert.isDefined(tea, 'tea has been defined');
   *
   * @name isDefined
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isDefined = function (val, msg) {
    new Assertion(val, msg, assert.isDefined, true).to.not.equal(undefined);
  };

  /**
   * ### .isFunction(value, [message])
   *
   * Asserts that `value` is a function.
   *
   *     function serveTea() { return 'cup of tea'; };
   *     assert.isFunction(serveTea, 'great, we can have tea now');
   *
   * @name isFunction
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isFunction = function (val, msg) {
    new Assertion(val, msg, assert.isFunction, true).to.be.a('function');
  };

  /**
   * ### .isNotFunction(value, [message])
   *
   * Asserts that `value` is _not_ a function.
   *
   *     var serveTea = [ 'heat', 'pour', 'sip' ];
   *     assert.isNotFunction(serveTea, 'great, we have listed the steps');
   *
   * @name isNotFunction
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotFunction = function (val, msg) {
    new Assertion(val, msg, assert.isNotFunction, true).to.not.be.a('function');
  };

  /**
   * ### .isObject(value, [message])
   *
   * Asserts that `value` is an object of type 'Object' (as revealed by `Object.prototype.toString`).
   * _The assertion does not match subclassed objects._
   *
   *     var selection = { name: 'Chai', serve: 'with spices' };
   *     assert.isObject(selection, 'tea selection is an object');
   *
   * @name isObject
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isObject = function (val, msg) {
    new Assertion(val, msg, assert.isObject, true).to.be.a('object');
  };

  /**
   * ### .isNotObject(value, [message])
   *
   * Asserts that `value` is _not_ an object of type 'Object' (as revealed by `Object.prototype.toString`).
   *
   *     var selection = 'chai'
   *     assert.isNotObject(selection, 'tea selection is not an object');
   *     assert.isNotObject(null, 'null is not an object');
   *
   * @name isNotObject
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotObject = function (val, msg) {
    new Assertion(val, msg, assert.isNotObject, true).to.not.be.a('object');
  };

  /**
   * ### .isArray(value, [message])
   *
   * Asserts that `value` is an array.
   *
   *     var menu = [ 'green', 'chai', 'oolong' ];
   *     assert.isArray(menu, 'what kind of tea do we want?');
   *
   * @name isArray
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isArray = function (val, msg) {
    new Assertion(val, msg, assert.isArray, true).to.be.an('array');
  };

  /**
   * ### .isNotArray(value, [message])
   *
   * Asserts that `value` is _not_ an array.
   *
   *     var menu = 'green|chai|oolong';
   *     assert.isNotArray(menu, 'what kind of tea do we want?');
   *
   * @name isNotArray
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotArray = function (val, msg) {
    new Assertion(val, msg, assert.isNotArray, true).to.not.be.an('array');
  };

  /**
   * ### .isString(value, [message])
   *
   * Asserts that `value` is a string.
   *
   *     var teaOrder = 'chai';
   *     assert.isString(teaOrder, 'order placed');
   *
   * @name isString
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isString = function (val, msg) {
    new Assertion(val, msg, assert.isString, true).to.be.a('string');
  };

  /**
   * ### .isNotString(value, [message])
   *
   * Asserts that `value` is _not_ a string.
   *
   *     var teaOrder = 4;
   *     assert.isNotString(teaOrder, 'order placed');
   *
   * @name isNotString
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotString = function (val, msg) {
    new Assertion(val, msg, assert.isNotString, true).to.not.be.a('string');
  };

  /**
   * ### .isNumber(value, [message])
   *
   * Asserts that `value` is a number.
   *
   *     var cups = 2;
   *     assert.isNumber(cups, 'how many cups');
   *
   * @name isNumber
   * @param {Number} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNumber = function (val, msg) {
    new Assertion(val, msg, assert.isNumber, true).to.be.a('number');
  };

  /**
   * ### .isNotNumber(value, [message])
   *
   * Asserts that `value` is _not_ a number.
   *
   *     var cups = '2 cups please';
   *     assert.isNotNumber(cups, 'how many cups');
   *
   * @name isNotNumber
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotNumber = function (val, msg) {
    new Assertion(val, msg, assert.isNotNumber, true).to.not.be.a('number');
  };

   /**
   * ### .isFinite(value, [message])
   *
   * Asserts that `value` is a finite number. Unlike `.isNumber`, this will fail for `NaN` and `Infinity`.
   *
   *     var cups = 2;
   *     assert.isFinite(cups, 'how many cups');
   *
   *     assert.isFinite(NaN); // throws
   *
   * @name isFinite
   * @param {Number} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isFinite = function (val, msg) {
    new Assertion(val, msg, assert.isFinite, true).to.be.finite;
  };

  /**
   * ### .isBoolean(value, [message])
   *
   * Asserts that `value` is a boolean.
   *
   *     var teaReady = true
   *       , teaServed = false;
   *
   *     assert.isBoolean(teaReady, 'is the tea ready');
   *     assert.isBoolean(teaServed, 'has tea been served');
   *
   * @name isBoolean
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isBoolean = function (val, msg) {
    new Assertion(val, msg, assert.isBoolean, true).to.be.a('boolean');
  };

  /**
   * ### .isNotBoolean(value, [message])
   *
   * Asserts that `value` is _not_ a boolean.
   *
   *     var teaReady = 'yep'
   *       , teaServed = 'nope';
   *
   *     assert.isNotBoolean(teaReady, 'is the tea ready');
   *     assert.isNotBoolean(teaServed, 'has tea been served');
   *
   * @name isNotBoolean
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.isNotBoolean = function (val, msg) {
    new Assertion(val, msg, assert.isNotBoolean, true).to.not.be.a('boolean');
  };

  /**
   * ### .typeOf(value, name, [message])
   *
   * Asserts that `value`'s type is `name`, as determined by
   * `Object.prototype.toString`.
   *
   *     assert.typeOf({ tea: 'chai' }, 'object', 'we have an object');
   *     assert.typeOf(['chai', 'jasmine'], 'array', 'we have an array');
   *     assert.typeOf('tea', 'string', 'we have a string');
   *     assert.typeOf(/tea/, 'regexp', 'we have a regular expression');
   *     assert.typeOf(null, 'null', 'we have a null');
   *     assert.typeOf(undefined, 'undefined', 'we have an undefined');
   *
   * @name typeOf
   * @param {Mixed} value
   * @param {String} name
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.typeOf = function (val, type, msg) {
    new Assertion(val, msg, assert.typeOf, true).to.be.a(type);
  };

  /**
   * ### .notTypeOf(value, name, [message])
   *
   * Asserts that `value`'s type is _not_ `name`, as determined by
   * `Object.prototype.toString`.
   *
   *     assert.notTypeOf('tea', 'number', 'strings are not numbers');
   *
   * @name notTypeOf
   * @param {Mixed} value
   * @param {String} typeof name
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notTypeOf = function (val, type, msg) {
    new Assertion(val, msg, assert.notTypeOf, true).to.not.be.a(type);
  };

  /**
   * ### .instanceOf(object, constructor, [message])
   *
   * Asserts that `value` is an instance of `constructor`.
   *
   *     var Tea = function (name) { this.name = name; }
   *       , chai = new Tea('chai');
   *
   *     assert.instanceOf(chai, Tea, 'chai is an instance of tea');
   *
   * @name instanceOf
   * @param {Object} object
   * @param {Constructor} constructor
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.instanceOf = function (val, type, msg) {
    new Assertion(val, msg, assert.instanceOf, true).to.be.instanceOf(type);
  };

  /**
   * ### .notInstanceOf(object, constructor, [message])
   *
   * Asserts `value` is not an instance of `constructor`.
   *
   *     var Tea = function (name) { this.name = name; }
   *       , chai = new String('chai');
   *
   *     assert.notInstanceOf(chai, Tea, 'chai is not an instance of tea');
   *
   * @name notInstanceOf
   * @param {Object} object
   * @param {Constructor} constructor
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notInstanceOf = function (val, type, msg) {
    new Assertion(val, msg, assert.notInstanceOf, true)
      .to.not.be.instanceOf(type);
  };

  /**
   * ### .include(haystack, needle, [message])
   *
   * Asserts that `haystack` includes `needle`. Can be used to assert the
   * inclusion of a value in an array, a substring in a string, or a subset of
   * properties in an object.
   *
   *     assert.include([1,2,3], 2, 'array contains value');
   *     assert.include('foobar', 'foo', 'string contains substring');
   *     assert.include({ foo: 'bar', hello: 'universe' }, { foo: 'bar' }, 'object contains property');
   *
   * Strict equality (===) is used. When asserting the inclusion of a value in
   * an array, the array is searched for an element that's strictly equal to the
   * given value. When asserting a subset of properties in an object, the object
   * is searched for the given property keys, checking that each one is present
   * and strictly equal to the given property value. For instance:
   *
   *     var obj1 = {a: 1}
   *       , obj2 = {b: 2};
   *     assert.include([obj1, obj2], obj1);
   *     assert.include({foo: obj1, bar: obj2}, {foo: obj1});
   *     assert.include({foo: obj1, bar: obj2}, {foo: obj1, bar: obj2});
   *
   * @name include
   * @param {Array|String} haystack
   * @param {Mixed} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.include = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.include, true).include(inc);
  };

  /**
   * ### .notInclude(haystack, needle, [message])
   *
   * Asserts that `haystack` does not include `needle`. Can be used to assert
   * the absence of a value in an array, a substring in a string, or a subset of
   * properties in an object.
   *
   *     assert.notInclude([1,2,3], 4, "array doesn't contain value");
   *     assert.notInclude('foobar', 'baz', "string doesn't contain substring");
   *     assert.notInclude({ foo: 'bar', hello: 'universe' }, { foo: 'baz' }, 'object doesn't contain property');
   *
   * Strict equality (===) is used. When asserting the absence of a value in an
   * array, the array is searched to confirm the absence of an element that's
   * strictly equal to the given value. When asserting a subset of properties in
   * an object, the object is searched to confirm that at least one of the given
   * property keys is either not present or not strictly equal to the given
   * property value. For instance:
   *
   *     var obj1 = {a: 1}
   *       , obj2 = {b: 2};
   *     assert.notInclude([obj1, obj2], {a: 1});
   *     assert.notInclude({foo: obj1, bar: obj2}, {foo: {a: 1}});
   *     assert.notInclude({foo: obj1, bar: obj2}, {foo: obj1, bar: {b: 2}});
   *
   * @name notInclude
   * @param {Array|String} haystack
   * @param {Mixed} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notInclude = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.notInclude, true).not.include(inc);
  };

  /**
   * ### .deepInclude(haystack, needle, [message])
   *
   * Asserts that `haystack` includes `needle`. Can be used to assert the
   * inclusion of a value in an array or a subset of properties in an object.
   * Deep equality is used.
   *
   *     var obj1 = {a: 1}
   *       , obj2 = {b: 2};
   *     assert.deepInclude([obj1, obj2], {a: 1});
   *     assert.deepInclude({foo: obj1, bar: obj2}, {foo: {a: 1}});
   *     assert.deepInclude({foo: obj1, bar: obj2}, {foo: {a: 1}, bar: {b: 2}});
   *
   * @name deepInclude
   * @param {Array|String} haystack
   * @param {Mixed} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.deepInclude = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.deepInclude, true).deep.include(inc);
  };

  /**
   * ### .notDeepInclude(haystack, needle, [message])
   *
   * Asserts that `haystack` does not include `needle`. Can be used to assert
   * the absence of a value in an array or a subset of properties in an object.
   * Deep equality is used.
   *
   *     var obj1 = {a: 1}
   *       , obj2 = {b: 2};
   *     assert.notDeepInclude([obj1, obj2], {a: 9});
   *     assert.notDeepInclude({foo: obj1, bar: obj2}, {foo: {a: 9}});
   *     assert.notDeepInclude({foo: obj1, bar: obj2}, {foo: {a: 1}, bar: {b: 9}});
   *
   * @name notDeepInclude
   * @param {Array|String} haystack
   * @param {Mixed} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepInclude = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.notDeepInclude, true).not.deep.include(inc);
  };

  /**
   * ### .nestedInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the inclusion of a subset of properties in an
   * object.
   * Enables the use of dot- and bracket-notation for referencing nested
   * properties.
   * '[]' and '.' in property names can be escaped using double backslashes.
   *
   *     assert.nestedInclude({'.a': {'b': 'x'}}, {'\\.a.[b]': 'x'});
   *     assert.nestedInclude({'a': {'[b]': 'x'}}, {'a.\\[b\\]': 'x'});
   *
   * @name nestedInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.nestedInclude = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.nestedInclude, true).nested.include(inc);
  };

  /**
   * ### .notNestedInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' does not include 'needle'.
   * Can be used to assert the absence of a subset of properties in an
   * object.
   * Enables the use of dot- and bracket-notation for referencing nested
   * properties.
   * '[]' and '.' in property names can be escaped using double backslashes.
   *
   *     assert.notNestedInclude({'.a': {'b': 'x'}}, {'\\.a.b': 'y'});
   *     assert.notNestedInclude({'a': {'[b]': 'x'}}, {'a.\\[b\\]': 'y'});
   *
   * @name notNestedInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notNestedInclude = function (exp, inc, msg) {
    new Assertion(exp, msg, assert.notNestedInclude, true)
      .not.nested.include(inc);
  };

  /**
   * ### .deepNestedInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the inclusion of a subset of properties in an
   * object while checking for deep equality.
   * Enables the use of dot- and bracket-notation for referencing nested
   * properties.
   * '[]' and '.' in property names can be escaped using double backslashes.
   *
   *     assert.deepNestedInclude({a: {b: [{x: 1}]}}, {'a.b[0]': {x: 1}});
   *     assert.deepNestedInclude({'.a': {'[b]': {x: 1}}}, {'\\.a.\\[b\\]': {x: 1}});
   *
   * @name deepNestedInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.deepNestedInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.deepNestedInclude, true)
      .deep.nested.include(inc);
  };

  /**
   * ### .notDeepNestedInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' does not include 'needle'.
   * Can be used to assert the absence of a subset of properties in an
   * object while checking for deep equality.
   * Enables the use of dot- and bracket-notation for referencing nested
   * properties.
   * '[]' and '.' in property names can be escaped using double backslashes.
   *
   *     assert.notDeepNestedInclude({a: {b: [{x: 1}]}}, {'a.b[0]': {y: 1}})
   *     assert.notDeepNestedInclude({'.a': {'[b]': {x: 1}}}, {'\\.a.\\[b\\]': {y: 2}});
   *
   * @name notDeepNestedInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepNestedInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.notDeepNestedInclude, true)
      .not.deep.nested.include(inc);
  };

  /**
   * ### .ownInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the inclusion of a subset of properties in an
   * object while ignoring inherited properties.
   *
   *     assert.ownInclude({ a: 1 }, { a: 1 });
   *
   * @name ownInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.ownInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.ownInclude, true).own.include(inc);
  };

  /**
   * ### .notOwnInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the absence of a subset of properties in an
   * object while ignoring inherited properties.
   *
   *     Object.prototype.b = 2;
   *
   *     assert.notOwnInclude({ a: 1 }, { b: 2 });
   *
   * @name notOwnInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notOwnInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.notOwnInclude, true).not.own.include(inc);
  };

  /**
   * ### .deepOwnInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the inclusion of a subset of properties in an
   * object while ignoring inherited properties and checking for deep equality.
   *
   *      assert.deepOwnInclude({a: {b: 2}}, {a: {b: 2}});
   *
   * @name deepOwnInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.deepOwnInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.deepOwnInclude, true)
      .deep.own.include(inc);
  };

   /**
   * ### .notDeepOwnInclude(haystack, needle, [message])
   *
   * Asserts that 'haystack' includes 'needle'.
   * Can be used to assert the absence of a subset of properties in an
   * object while ignoring inherited properties and checking for deep equality.
   *
   *      assert.notDeepOwnInclude({a: {b: 2}}, {a: {c: 3}});
   *
   * @name notDeepOwnInclude
   * @param {Object} haystack
   * @param {Object} needle
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepOwnInclude = function(exp, inc, msg) {
    new Assertion(exp, msg, assert.notDeepOwnInclude, true)
      .not.deep.own.include(inc);
  };

  /**
   * ### .match(value, regexp, [message])
   *
   * Asserts that `value` matches the regular expression `regexp`.
   *
   *     assert.match('foobar', /^foo/, 'regexp matches');
   *
   * @name match
   * @param {Mixed} value
   * @param {RegExp} regexp
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.match = function (exp, re, msg) {
    new Assertion(exp, msg, assert.match, true).to.match(re);
  };

  /**
   * ### .notMatch(value, regexp, [message])
   *
   * Asserts that `value` does not match the regular expression `regexp`.
   *
   *     assert.notMatch('foobar', /^foo/, 'regexp does not match');
   *
   * @name notMatch
   * @param {Mixed} value
   * @param {RegExp} regexp
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notMatch = function (exp, re, msg) {
    new Assertion(exp, msg, assert.notMatch, true).to.not.match(re);
  };

  /**
   * ### .property(object, property, [message])
   *
   * Asserts that `object` has a direct or inherited property named by
   * `property`.
   *
   *     assert.property({ tea: { green: 'matcha' }}, 'tea');
   *     assert.property({ tea: { green: 'matcha' }}, 'toString');
   *
   * @name property
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.property = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.property, true).to.have.property(prop);
  };

  /**
   * ### .notProperty(object, property, [message])
   *
   * Asserts that `object` does _not_ have a direct or inherited property named
   * by `property`.
   *
   *     assert.notProperty({ tea: { green: 'matcha' }}, 'coffee');
   *
   * @name notProperty
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notProperty = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.notProperty, true)
      .to.not.have.property(prop);
  };

  /**
   * ### .propertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a direct or inherited property named by
   * `property` with a value given by `value`. Uses a strict equality check
   * (===).
   *
   *     assert.propertyVal({ tea: 'is good' }, 'tea', 'is good');
   *
   * @name propertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.propertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.propertyVal, true)
      .to.have.property(prop, val);
  };

  /**
   * ### .notPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a direct or inherited property named
   * by `property` with value given by `value`. Uses a strict equality check
   * (===).
   *
   *     assert.notPropertyVal({ tea: 'is good' }, 'tea', 'is bad');
   *     assert.notPropertyVal({ tea: 'is good' }, 'coffee', 'is good');
   *
   * @name notPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.notPropertyVal, true)
      .to.not.have.property(prop, val);
  };

  /**
   * ### .deepPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a direct or inherited property named by
   * `property` with a value given by `value`. Uses a deep equality check.
   *
   *     assert.deepPropertyVal({ tea: { green: 'matcha' } }, 'tea', { green: 'matcha' });
   *
   * @name deepPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.deepPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.deepPropertyVal, true)
      .to.have.deep.property(prop, val);
  };

  /**
   * ### .notDeepPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a direct or inherited property named
   * by `property` with value given by `value`. Uses a deep equality check.
   *
   *     assert.notDeepPropertyVal({ tea: { green: 'matcha' } }, 'tea', { black: 'matcha' });
   *     assert.notDeepPropertyVal({ tea: { green: 'matcha' } }, 'tea', { green: 'oolong' });
   *     assert.notDeepPropertyVal({ tea: { green: 'matcha' } }, 'coffee', { green: 'matcha' });
   *
   * @name notDeepPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.notDeepPropertyVal, true)
      .to.not.have.deep.property(prop, val);
  };

  /**
   * ### .ownProperty(object, property, [message])
   *
   * Asserts that `object` has a direct property named by `property`. Inherited
   * properties aren't checked.
   *
   *     assert.ownProperty({ tea: { green: 'matcha' }}, 'tea');
   *
   * @name ownProperty
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @api public
   */

  assert.ownProperty = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.ownProperty, true)
      .to.have.own.property(prop);
  };

  /**
   * ### .notOwnProperty(object, property, [message])
   *
   * Asserts that `object` does _not_ have a direct property named by
   * `property`. Inherited properties aren't checked.
   *
   *     assert.notOwnProperty({ tea: { green: 'matcha' }}, 'coffee');
   *     assert.notOwnProperty({}, 'toString');
   *
   * @name notOwnProperty
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @api public
   */

  assert.notOwnProperty = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.notOwnProperty, true)
      .to.not.have.own.property(prop);
  };

  /**
   * ### .ownPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a direct property named by `property` and a value
   * equal to the provided `value`. Uses a strict equality check (===).
   * Inherited properties aren't checked.
   *
   *     assert.ownPropertyVal({ coffee: 'is good'}, 'coffee', 'is good');
   *
   * @name ownPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @api public
   */

  assert.ownPropertyVal = function (obj, prop, value, msg) {
    new Assertion(obj, msg, assert.ownPropertyVal, true)
      .to.have.own.property(prop, value);
  };

  /**
   * ### .notOwnPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a direct property named by `property`
   * with a value equal to the provided `value`. Uses a strict equality check
   * (===). Inherited properties aren't checked.
   *
   *     assert.notOwnPropertyVal({ tea: 'is better'}, 'tea', 'is worse');
   *     assert.notOwnPropertyVal({}, 'toString', Object.prototype.toString);
   *
   * @name notOwnPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @api public
   */

  assert.notOwnPropertyVal = function (obj, prop, value, msg) {
    new Assertion(obj, msg, assert.notOwnPropertyVal, true)
      .to.not.have.own.property(prop, value);
  };

  /**
   * ### .deepOwnPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a direct property named by `property` and a value
   * equal to the provided `value`. Uses a deep equality check. Inherited
   * properties aren't checked.
   *
   *     assert.deepOwnPropertyVal({ tea: { green: 'matcha' } }, 'tea', { green: 'matcha' });
   *
   * @name deepOwnPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @api public
   */

  assert.deepOwnPropertyVal = function (obj, prop, value, msg) {
    new Assertion(obj, msg, assert.deepOwnPropertyVal, true)
      .to.have.deep.own.property(prop, value);
  };

  /**
   * ### .notDeepOwnPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a direct property named by `property`
   * with a value equal to the provided `value`. Uses a deep equality check.
   * Inherited properties aren't checked.
   *
   *     assert.notDeepOwnPropertyVal({ tea: { green: 'matcha' } }, 'tea', { black: 'matcha' });
   *     assert.notDeepOwnPropertyVal({ tea: { green: 'matcha' } }, 'tea', { green: 'oolong' });
   *     assert.notDeepOwnPropertyVal({ tea: { green: 'matcha' } }, 'coffee', { green: 'matcha' });
   *     assert.notDeepOwnPropertyVal({}, 'toString', Object.prototype.toString);
   *
   * @name notDeepOwnPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @api public
   */

  assert.notDeepOwnPropertyVal = function (obj, prop, value, msg) {
    new Assertion(obj, msg, assert.notDeepOwnPropertyVal, true)
      .to.not.have.deep.own.property(prop, value);
  };

  /**
   * ### .nestedProperty(object, property, [message])
   *
   * Asserts that `object` has a direct or inherited property named by
   * `property`, which can be a string using dot- and bracket-notation for
   * nested reference.
   *
   *     assert.nestedProperty({ tea: { green: 'matcha' }}, 'tea.green');
   *
   * @name nestedProperty
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.nestedProperty = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.nestedProperty, true)
      .to.have.nested.property(prop);
  };

  /**
   * ### .notNestedProperty(object, property, [message])
   *
   * Asserts that `object` does _not_ have a property named by `property`, which
   * can be a string using dot- and bracket-notation for nested reference. The
   * property cannot exist on the object nor anywhere in its prototype chain.
   *
   *     assert.notNestedProperty({ tea: { green: 'matcha' }}, 'tea.oolong');
   *
   * @name notNestedProperty
   * @param {Object} object
   * @param {String} property
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notNestedProperty = function (obj, prop, msg) {
    new Assertion(obj, msg, assert.notNestedProperty, true)
      .to.not.have.nested.property(prop);
  };

  /**
   * ### .nestedPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a property named by `property` with value given
   * by `value`. `property` can use dot- and bracket-notation for nested
   * reference. Uses a strict equality check (===).
   *
   *     assert.nestedPropertyVal({ tea: { green: 'matcha' }}, 'tea.green', 'matcha');
   *
   * @name nestedPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.nestedPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.nestedPropertyVal, true)
      .to.have.nested.property(prop, val);
  };

  /**
   * ### .notNestedPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a property named by `property` with
   * value given by `value`. `property` can use dot- and bracket-notation for
   * nested reference. Uses a strict equality check (===).
   *
   *     assert.notNestedPropertyVal({ tea: { green: 'matcha' }}, 'tea.green', 'konacha');
   *     assert.notNestedPropertyVal({ tea: { green: 'matcha' }}, 'coffee.green', 'matcha');
   *
   * @name notNestedPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notNestedPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.notNestedPropertyVal, true)
      .to.not.have.nested.property(prop, val);
  };

  /**
   * ### .deepNestedPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` has a property named by `property` with a value given
   * by `value`. `property` can use dot- and bracket-notation for nested
   * reference. Uses a deep equality check.
   *
   *     assert.deepNestedPropertyVal({ tea: { green: { matcha: 'yum' } } }, 'tea.green', { matcha: 'yum' });
   *
   * @name deepNestedPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.deepNestedPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.deepNestedPropertyVal, true)
      .to.have.deep.nested.property(prop, val);
  };

  /**
   * ### .notDeepNestedPropertyVal(object, property, value, [message])
   *
   * Asserts that `object` does _not_ have a property named by `property` with
   * value given by `value`. `property` can use dot- and bracket-notation for
   * nested reference. Uses a deep equality check.
   *
   *     assert.notDeepNestedPropertyVal({ tea: { green: { matcha: 'yum' } } }, 'tea.green', { oolong: 'yum' });
   *     assert.notDeepNestedPropertyVal({ tea: { green: { matcha: 'yum' } } }, 'tea.green', { matcha: 'yuck' });
   *     assert.notDeepNestedPropertyVal({ tea: { green: { matcha: 'yum' } } }, 'tea.black', { matcha: 'yum' });
   *
   * @name notDeepNestedPropertyVal
   * @param {Object} object
   * @param {String} property
   * @param {Mixed} value
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notDeepNestedPropertyVal = function (obj, prop, val, msg) {
    new Assertion(obj, msg, assert.notDeepNestedPropertyVal, true)
      .to.not.have.deep.nested.property(prop, val);
  }

  /**
   * ### .lengthOf(object, length, [message])
   *
   * Asserts that `object` has a `length` or `size` with the expected value.
   *
   *     assert.lengthOf([1,2,3], 3, 'array has length of 3');
   *     assert.lengthOf('foobar', 6, 'string has length of 6');
   *     assert.lengthOf(new Set([1,2,3]), 3, 'set has size of 3');
   *     assert.lengthOf(new Map([['a',1],['b',2],['c',3]]), 3, 'map has size of 3');
   *
   * @name lengthOf
   * @param {Mixed} object
   * @param {Number} length
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.lengthOf = function (exp, len, msg) {
    new Assertion(exp, msg, assert.lengthOf, true).to.have.lengthOf(len);
  };

  /**
   * ### .hasAnyKeys(object, [keys], [message])
   *
   * Asserts that `object` has at least one of the `keys` provided.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.hasAnyKeys({foo: 1, bar: 2, baz: 3}, ['foo', 'iDontExist', 'baz']);
   *     assert.hasAnyKeys({foo: 1, bar: 2, baz: 3}, {foo: 30, iDontExist: 99, baz: 1337});
   *     assert.hasAnyKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{foo: 1}, 'key']);
   *     assert.hasAnyKeys(new Set([{foo: 'bar'}, 'anotherKey']), [{foo: 'bar'}, 'anotherKey']);
   *
   * @name hasAnyKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.hasAnyKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.hasAnyKeys, true).to.have.any.keys(keys);
  }

  /**
   * ### .hasAllKeys(object, [keys], [message])
   *
   * Asserts that `object` has all and only all of the `keys` provided.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.hasAllKeys({foo: 1, bar: 2, baz: 3}, ['foo', 'bar', 'baz']);
   *     assert.hasAllKeys({foo: 1, bar: 2, baz: 3}, {foo: 30, bar: 99, baz: 1337]);
   *     assert.hasAllKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{foo: 1}, 'key']);
   *     assert.hasAllKeys(new Set([{foo: 'bar'}, 'anotherKey'], [{foo: 'bar'}, 'anotherKey']);
   *
   * @name hasAllKeys
   * @param {Mixed} object
   * @param {String[]} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.hasAllKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.hasAllKeys, true).to.have.all.keys(keys);
  }

  /**
   * ### .containsAllKeys(object, [keys], [message])
   *
   * Asserts that `object` has all of the `keys` provided but may have more keys not listed.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.containsAllKeys({foo: 1, bar: 2, baz: 3}, ['foo', 'baz']);
   *     assert.containsAllKeys({foo: 1, bar: 2, baz: 3}, ['foo', 'bar', 'baz']);
   *     assert.containsAllKeys({foo: 1, bar: 2, baz: 3}, {foo: 30, baz: 1337});
   *     assert.containsAllKeys({foo: 1, bar: 2, baz: 3}, {foo: 30, bar: 99, baz: 1337});
   *     assert.containsAllKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{foo: 1}]);
   *     assert.containsAllKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{foo: 1}, 'key']);
   *     assert.containsAllKeys(new Set([{foo: 'bar'}, 'anotherKey'], [{foo: 'bar'}]);
   *     assert.containsAllKeys(new Set([{foo: 'bar'}, 'anotherKey'], [{foo: 'bar'}, 'anotherKey']);
   *
   * @name containsAllKeys
   * @param {Mixed} object
   * @param {String[]} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.containsAllKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.containsAllKeys, true)
      .to.contain.all.keys(keys);
  }

  /**
   * ### .doesNotHaveAnyKeys(object, [keys], [message])
   *
   * Asserts that `object` has none of the `keys` provided.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.doesNotHaveAnyKeys({foo: 1, bar: 2, baz: 3}, ['one', 'two', 'example']);
   *     assert.doesNotHaveAnyKeys({foo: 1, bar: 2, baz: 3}, {one: 1, two: 2, example: 'foo'});
   *     assert.doesNotHaveAnyKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{one: 'two'}, 'example']);
   *     assert.doesNotHaveAnyKeys(new Set([{foo: 'bar'}, 'anotherKey'], [{one: 'two'}, 'example']);
   *
   * @name doesNotHaveAnyKeys
   * @param {Mixed} object
   * @param {String[]} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.doesNotHaveAnyKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.doesNotHaveAnyKeys, true)
      .to.not.have.any.keys(keys);
  }

  /**
   * ### .doesNotHaveAllKeys(object, [keys], [message])
   *
   * Asserts that `object` does not have at least one of the `keys` provided.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.doesNotHaveAllKeys({foo: 1, bar: 2, baz: 3}, ['one', 'two', 'example']);
   *     assert.doesNotHaveAllKeys({foo: 1, bar: 2, baz: 3}, {one: 1, two: 2, example: 'foo'});
   *     assert.doesNotHaveAllKeys(new Map([[{foo: 1}, 'bar'], ['key', 'value']]), [{one: 'two'}, 'example']);
   *     assert.doesNotHaveAllKeys(new Set([{foo: 'bar'}, 'anotherKey'], [{one: 'two'}, 'example']);
   *
   * @name doesNotHaveAllKeys
   * @param {Mixed} object
   * @param {String[]} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.doesNotHaveAllKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.doesNotHaveAllKeys, true)
      .to.not.have.all.keys(keys);
  }

  /**
   * ### .hasAnyDeepKeys(object, [keys], [message])
   *
   * Asserts that `object` has at least one of the `keys` provided.
   * Since Sets and Maps can have objects as keys you can use this assertion to perform
   * a deep comparison.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.hasAnyDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [1, 2]]), {one: 'one'});
   *     assert.hasAnyDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [1, 2]]), [{one: 'one'}, {two: 'two'}]);
   *     assert.hasAnyDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [{two: 'two'}, 'valueTwo']]), [{one: 'one'}, {two: 'two'}]);
   *     assert.hasAnyDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), {one: 'one'});
   *     assert.hasAnyDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{one: 'one'}, {three: 'three'}]);
   *     assert.hasAnyDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{one: 'one'}, {two: 'two'}]);
   *
   * @name hasAnyDeepKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.hasAnyDeepKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.hasAnyDeepKeys, true)
      .to.have.any.deep.keys(keys);
  }

 /**
   * ### .hasAllDeepKeys(object, [keys], [message])
   *
   * Asserts that `object` has all and only all of the `keys` provided.
   * Since Sets and Maps can have objects as keys you can use this assertion to perform
   * a deep comparison.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.hasAllDeepKeys(new Map([[{one: 'one'}, 'valueOne']]), {one: 'one'});
   *     assert.hasAllDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [{two: 'two'}, 'valueTwo']]), [{one: 'one'}, {two: 'two'}]);
   *     assert.hasAllDeepKeys(new Set([{one: 'one'}]), {one: 'one'});
   *     assert.hasAllDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{one: 'one'}, {two: 'two'}]);
   *
   * @name hasAllDeepKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.hasAllDeepKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.hasAllDeepKeys, true)
      .to.have.all.deep.keys(keys);
  }

 /**
   * ### .containsAllDeepKeys(object, [keys], [message])
   *
   * Asserts that `object` contains all of the `keys` provided.
   * Since Sets and Maps can have objects as keys you can use this assertion to perform
   * a deep comparison.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.containsAllDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [1, 2]]), {one: 'one'});
   *     assert.containsAllDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [{two: 'two'}, 'valueTwo']]), [{one: 'one'}, {two: 'two'}]);
   *     assert.containsAllDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), {one: 'one'});
   *     assert.containsAllDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{one: 'one'}, {two: 'two'}]);
   *
   * @name containsAllDeepKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.containsAllDeepKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.containsAllDeepKeys, true)
      .to.contain.all.deep.keys(keys);
  }

 /**
   * ### .doesNotHaveAnyDeepKeys(object, [keys], [message])
   *
   * Asserts that `object` has none of the `keys` provided.
   * Since Sets and Maps can have objects as keys you can use this assertion to perform
   * a deep comparison.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.doesNotHaveAnyDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [1, 2]]), {thisDoesNot: 'exist'});
   *     assert.doesNotHaveAnyDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [{two: 'two'}, 'valueTwo']]), [{twenty: 'twenty'}, {fifty: 'fifty'}]);
   *     assert.doesNotHaveAnyDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), {twenty: 'twenty'});
   *     assert.doesNotHaveAnyDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{twenty: 'twenty'}, {fifty: 'fifty'}]);
   *
   * @name doesNotHaveAnyDeepKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.doesNotHaveAnyDeepKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.doesNotHaveAnyDeepKeys, true)
      .to.not.have.any.deep.keys(keys);
  }

 /**
   * ### .doesNotHaveAllDeepKeys(object, [keys], [message])
   *
   * Asserts that `object` does not have at least one of the `keys` provided.
   * Since Sets and Maps can have objects as keys you can use this assertion to perform
   * a deep comparison.
   * You can also provide a single object instead of a `keys` array and its keys
   * will be used as the expected set of keys.
   *
   *     assert.doesNotHaveAllDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [1, 2]]), {thisDoesNot: 'exist'});
   *     assert.doesNotHaveAllDeepKeys(new Map([[{one: 'one'}, 'valueOne'], [{two: 'two'}, 'valueTwo']]), [{twenty: 'twenty'}, {one: 'one'}]);
   *     assert.doesNotHaveAllDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), {twenty: 'twenty'});
   *     assert.doesNotHaveAllDeepKeys(new Set([{one: 'one'}, {two: 'two'}]), [{one: 'one'}, {fifty: 'fifty'}]);
   *
   * @name doesNotHaveAllDeepKeys
   * @param {Mixed} object
   * @param {Array|Object} keys
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.doesNotHaveAllDeepKeys = function (obj, keys, msg) {
    new Assertion(obj, msg, assert.doesNotHaveAllDeepKeys, true)
      .to.not.have.all.deep.keys(keys);
  }

 /**
   * ### .throws(fn, [errorLike/string/regexp], [string/regexp], [message])
   *
   * If `errorLike` is an `Error` constructor, asserts that `fn` will throw an error that is an
   * instance of `errorLike`.
   * If `errorLike` is an `Error` instance, asserts that the error thrown is the same
   * instance as `errorLike`.
   * If `errMsgMatcher` is provided, it also asserts that the error thrown will have a
   * message matching `errMsgMatcher`.
   *
   *     assert.throws(fn, 'Error thrown must have this msg');
   *     assert.throws(fn, /Error thrown must have a msg that matches this/);
   *     assert.throws(fn, ReferenceError);
   *     assert.throws(fn, errorInstance);
   *     assert.throws(fn, ReferenceError, 'Error thrown must be a ReferenceError and have this msg');
   *     assert.throws(fn, errorInstance, 'Error thrown must be the same errorInstance and have this msg');
   *     assert.throws(fn, ReferenceError, /Error thrown must be a ReferenceError and match this/);
   *     assert.throws(fn, errorInstance, /Error thrown must be the same errorInstance and match this/);
   *
   * @name throws
   * @alias throw
   * @alias Throw
   * @param {Function} fn
   * @param {ErrorConstructor|Error} errorLike
   * @param {RegExp|String} errMsgMatcher
   * @param {String} message
   * @see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Error_types
   * @namespace Assert
   * @api public
   */

  assert.throws = function (fn, errorLike, errMsgMatcher, msg) {
    if ('string' === typeof errorLike || errorLike instanceof RegExp) {
      errMsgMatcher = errorLike;
      errorLike = null;
    }

    var assertErr = new Assertion(fn, msg, assert.throws, true)
      .to.throw(errorLike, errMsgMatcher);
    return flag(assertErr, 'object');
  };

  /**
   * ### .doesNotThrow(fn, [errorLike/string/regexp], [string/regexp], [message])
   *
   * If `errorLike` is an `Error` constructor, asserts that `fn` will _not_ throw an error that is an
   * instance of `errorLike`.
   * If `errorLike` is an `Error` instance, asserts that the error thrown is _not_ the same
   * instance as `errorLike`.
   * If `errMsgMatcher` is provided, it also asserts that the error thrown will _not_ have a
   * message matching `errMsgMatcher`.
   *
   *     assert.doesNotThrow(fn, 'Any Error thrown must not have this message');
   *     assert.doesNotThrow(fn, /Any Error thrown must not match this/);
   *     assert.doesNotThrow(fn, Error);
   *     assert.doesNotThrow(fn, errorInstance);
   *     assert.doesNotThrow(fn, Error, 'Error must not have this message');
   *     assert.doesNotThrow(fn, errorInstance, 'Error must not have this message');
   *     assert.doesNotThrow(fn, Error, /Error must not match this/);
   *     assert.doesNotThrow(fn, errorInstance, /Error must not match this/);
   *
   * @name doesNotThrow
   * @param {Function} fn
   * @param {ErrorConstructor} errorLike
   * @param {RegExp|String} errMsgMatcher
   * @param {String} message
   * @see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Error_types
   * @namespace Assert
   * @api public
   */

  assert.doesNotThrow = function (fn, errorLike, errMsgMatcher, msg) {
    if ('string' === typeof errorLike || errorLike instanceof RegExp) {
      errMsgMatcher = errorLike;
      errorLike = null;
    }

    new Assertion(fn, msg, assert.doesNotThrow, true)
      .to.not.throw(errorLike, errMsgMatcher);
  };

  /**
   * ### .operator(val1, operator, val2, [message])
   *
   * Compares two values using `operator`.
   *
   *     assert.operator(1, '<', 2, 'everything is ok');
   *     assert.operator(1, '>', 2, 'this will fail');
   *
   * @name operator
   * @param {Mixed} val1
   * @param {String} operator
   * @param {Mixed} val2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.operator = function (val, operator, val2, msg) {
    var ok;
    switch(operator) {
      case '==':
        ok = val == val2;
        break;
      case '===':
        ok = val === val2;
        break;
      case '>':
        ok = val > val2;
        break;
      case '>=':
        ok = val >= val2;
        break;
      case '<':
        ok = val < val2;
        break;
      case '<=':
        ok = val <= val2;
        break;
      case '!=':
        ok = val != val2;
        break;
      case '!==':
        ok = val !== val2;
        break;
      default:
        msg = msg ? msg + ': ' : msg;
        throw new chai.AssertionError(
          msg + 'Invalid operator "' + operator + '"',
          undefined,
          assert.operator
        );
    }
    var test = new Assertion(ok, msg, assert.operator, true);
    test.assert(
        true === flag(test, 'object')
      , 'expected ' + util.inspect(val) + ' to be ' + operator + ' ' + util.inspect(val2)
      , 'expected ' + util.inspect(val) + ' to not be ' + operator + ' ' + util.inspect(val2) );
  };

  /**
   * ### .closeTo(actual, expected, delta, [message])
   *
   * Asserts that the target is equal `expected`, to within a +/- `delta` range.
   *
   *     assert.closeTo(1.5, 1, 0.5, 'numbers are close');
   *
   * @name closeTo
   * @param {Number} actual
   * @param {Number} expected
   * @param {Number} delta
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.closeTo = function (act, exp, delta, msg) {
    new Assertion(act, msg, assert.closeTo, true).to.be.closeTo(exp, delta);
  };

  /**
   * ### .approximately(actual, expected, delta, [message])
   *
   * Asserts that the target is equal `expected`, to within a +/- `delta` range.
   *
   *     assert.approximately(1.5, 1, 0.5, 'numbers are close');
   *
   * @name approximately
   * @param {Number} actual
   * @param {Number} expected
   * @param {Number} delta
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.approximately = function (act, exp, delta, msg) {
    new Assertion(act, msg, assert.approximately, true)
      .to.be.approximately(exp, delta);
  };

  /**
   * ### .sameMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` have the same members in any order. Uses a
   * strict equality check (===).
   *
   *     assert.sameMembers([ 1, 2, 3 ], [ 2, 1, 3 ], 'same members');
   *
   * @name sameMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.sameMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.sameMembers, true)
      .to.have.same.members(set2);
  }

  /**
   * ### .notSameMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` don't have the same members in any order.
   * Uses a strict equality check (===).
   *
   *     assert.notSameMembers([ 1, 2, 3 ], [ 5, 1, 3 ], 'not same members');
   *
   * @name notSameMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notSameMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.notSameMembers, true)
      .to.not.have.same.members(set2);
  }

  /**
   * ### .sameDeepMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` have the same members in any order. Uses a
   * deep equality check.
   *
   *     assert.sameDeepMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [{ b: 2 }, { a: 1 }, { c: 3 }], 'same deep members');
   *
   * @name sameDeepMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.sameDeepMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.sameDeepMembers, true)
      .to.have.same.deep.members(set2);
  }

  /**
   * ### .notSameDeepMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` don't have the same members in any order.
   * Uses a deep equality check.
   *
   *     assert.notSameDeepMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [{ b: 2 }, { a: 1 }, { f: 5 }], 'not same deep members');
   *
   * @name notSameDeepMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notSameDeepMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.notSameDeepMembers, true)
      .to.not.have.same.deep.members(set2);
  }

  /**
   * ### .sameOrderedMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` have the same members in the same order.
   * Uses a strict equality check (===).
   *
   *     assert.sameOrderedMembers([ 1, 2, 3 ], [ 1, 2, 3 ], 'same ordered members');
   *
   * @name sameOrderedMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.sameOrderedMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.sameOrderedMembers, true)
      .to.have.same.ordered.members(set2);
  }

  /**
   * ### .notSameOrderedMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` don't have the same members in the same
   * order. Uses a strict equality check (===).
   *
   *     assert.notSameOrderedMembers([ 1, 2, 3 ], [ 2, 1, 3 ], 'not same ordered members');
   *
   * @name notSameOrderedMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notSameOrderedMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.notSameOrderedMembers, true)
      .to.not.have.same.ordered.members(set2);
  }

  /**
   * ### .sameDeepOrderedMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` have the same members in the same order.
   * Uses a deep equality check.
   *
   *     assert.sameDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { a: 1 }, { b: 2 }, { c: 3 } ], 'same deep ordered members');
   *
   * @name sameDeepOrderedMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.sameDeepOrderedMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.sameDeepOrderedMembers, true)
      .to.have.same.deep.ordered.members(set2);
  }

  /**
   * ### .notSameDeepOrderedMembers(set1, set2, [message])
   *
   * Asserts that `set1` and `set2` don't have the same members in the same
   * order. Uses a deep equality check.
   *
   *     assert.notSameDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { a: 1 }, { b: 2 }, { z: 5 } ], 'not same deep ordered members');
   *     assert.notSameDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { b: 2 }, { a: 1 }, { c: 3 } ], 'not same deep ordered members');
   *
   * @name notSameDeepOrderedMembers
   * @param {Array} set1
   * @param {Array} set2
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notSameDeepOrderedMembers = function (set1, set2, msg) {
    new Assertion(set1, msg, assert.notSameDeepOrderedMembers, true)
      .to.not.have.same.deep.ordered.members(set2);
  }

  /**
   * ### .includeMembers(superset, subset, [message])
   *
   * Asserts that `subset` is included in `superset` in any order. Uses a
   * strict equality check (===). Duplicates are ignored.
   *
   *     assert.includeMembers([ 1, 2, 3 ], [ 2, 1, 2 ], 'include members');
   *
   * @name includeMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.includeMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.includeMembers, true)
      .to.include.members(subset);
  }

  /**
   * ### .notIncludeMembers(superset, subset, [message])
   *
   * Asserts that `subset` isn't included in `superset` in any order. Uses a
   * strict equality check (===). Duplicates are ignored.
   *
   *     assert.notIncludeMembers([ 1, 2, 3 ], [ 5, 1 ], 'not include members');
   *
   * @name notIncludeMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notIncludeMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.notIncludeMembers, true)
      .to.not.include.members(subset);
  }

  /**
   * ### .includeDeepMembers(superset, subset, [message])
   *
   * Asserts that `subset` is included in `superset` in any order. Uses a deep
   * equality check. Duplicates are ignored.
   *
   *     assert.includeDeepMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { b: 2 }, { a: 1 }, { b: 2 } ], 'include deep members');
   *
   * @name includeDeepMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.includeDeepMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.includeDeepMembers, true)
      .to.include.deep.members(subset);
  }

  /**
   * ### .notIncludeDeepMembers(superset, subset, [message])
   *
   * Asserts that `subset` isn't included in `superset` in any order. Uses a
   * deep equality check. Duplicates are ignored.
   *
   *     assert.notIncludeDeepMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { b: 2 }, { f: 5 } ], 'not include deep members');
   *
   * @name notIncludeDeepMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notIncludeDeepMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.notIncludeDeepMembers, true)
      .to.not.include.deep.members(subset);
  }

  /**
   * ### .includeOrderedMembers(superset, subset, [message])
   *
   * Asserts that `subset` is included in `superset` in the same order
   * beginning with the first element in `superset`. Uses a strict equality
   * check (===).
   *
   *     assert.includeOrderedMembers([ 1, 2, 3 ], [ 1, 2 ], 'include ordered members');
   *
   * @name includeOrderedMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.includeOrderedMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.includeOrderedMembers, true)
      .to.include.ordered.members(subset);
  }

  /**
   * ### .notIncludeOrderedMembers(superset, subset, [message])
   *
   * Asserts that `subset` isn't included in `superset` in the same order
   * beginning with the first element in `superset`. Uses a strict equality
   * check (===).
   *
   *     assert.notIncludeOrderedMembers([ 1, 2, 3 ], [ 2, 1 ], 'not include ordered members');
   *     assert.notIncludeOrderedMembers([ 1, 2, 3 ], [ 2, 3 ], 'not include ordered members');
   *
   * @name notIncludeOrderedMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notIncludeOrderedMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.notIncludeOrderedMembers, true)
      .to.not.include.ordered.members(subset);
  }

  /**
   * ### .includeDeepOrderedMembers(superset, subset, [message])
   *
   * Asserts that `subset` is included in `superset` in the same order
   * beginning with the first element in `superset`. Uses a deep equality
   * check.
   *
   *     assert.includeDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { a: 1 }, { b: 2 } ], 'include deep ordered members');
   *
   * @name includeDeepOrderedMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.includeDeepOrderedMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.includeDeepOrderedMembers, true)
      .to.include.deep.ordered.members(subset);
  }

  /**
   * ### .notIncludeDeepOrderedMembers(superset, subset, [message])
   *
   * Asserts that `subset` isn't included in `superset` in the same order
   * beginning with the first element in `superset`. Uses a deep equality
   * check.
   *
   *     assert.notIncludeDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { a: 1 }, { f: 5 } ], 'not include deep ordered members');
   *     assert.notIncludeDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { b: 2 }, { a: 1 } ], 'not include deep ordered members');
   *     assert.notIncludeDeepOrderedMembers([ { a: 1 }, { b: 2 }, { c: 3 } ], [ { b: 2 }, { c: 3 } ], 'not include deep ordered members');
   *
   * @name notIncludeDeepOrderedMembers
   * @param {Array} superset
   * @param {Array} subset
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.notIncludeDeepOrderedMembers = function (superset, subset, msg) {
    new Assertion(superset, msg, assert.notIncludeDeepOrderedMembers, true)
      .to.not.include.deep.ordered.members(subset);
  }

  /**
   * ### .oneOf(inList, list, [message])
   *
   * Asserts that non-object, non-array value `inList` appears in the flat array `list`.
   *
   *     assert.oneOf(1, [ 2, 1 ], 'Not found in list');
   *
   * @name oneOf
   * @param {*} inList
   * @param {Array<*>} list
   * @param {String} message
   * @namespace Assert
   * @api public
   */

  assert.oneOf = function (inList, list, msg) {
    new Assertion(inList, msg, assert.oneOf, true).to.be.oneOf(list);
  }

  /**
   * ### .changes(function, object, property, [message])
   *
   * Asserts that a function changes the value of a property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 22 };
   *     assert.changes(fn, obj, 'val');
   *
   * @name changes
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.changes = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.changes, true).to.change(obj, prop);
  }

   /**
   * ### .changesBy(function, object, property, delta, [message])
   *
   * Asserts that a function changes the value of a property by an amount (delta).
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val += 2 };
   *     assert.changesBy(fn, obj, 'val', 2);
   *
   * @name changesBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.changesBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.changesBy, true)
      .to.change(obj, prop).by(delta);
  }

   /**
   * ### .doesNotChange(function, object, property, [message])
   *
   * Asserts that a function does not change the value of a property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { console.log('foo'); };
   *     assert.doesNotChange(fn, obj, 'val');
   *
   * @name doesNotChange
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.doesNotChange = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.doesNotChange, true)
      .to.not.change(obj, prop);
  }

  /**
   * ### .changesButNotBy(function, object, property, delta, [message])
   *
   * Asserts that a function does not change the value of a property or of a function's return value by an amount (delta)
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val += 10 };
   *     assert.changesButNotBy(fn, obj, 'val', 5);
   *
   * @name changesButNotBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.changesButNotBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.changesButNotBy, true)
      .to.change(obj, prop).but.not.by(delta);
  }

  /**
   * ### .increases(function, object, property, [message])
   *
   * Asserts that a function increases a numeric object property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 13 };
   *     assert.increases(fn, obj, 'val');
   *
   * @name increases
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.increases = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.increases, true)
      .to.increase(obj, prop);
  }

  /**
   * ### .increasesBy(function, object, property, delta, [message])
   *
   * Asserts that a function increases a numeric object property or a function's return value by an amount (delta).
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val += 10 };
   *     assert.increasesBy(fn, obj, 'val', 10);
   *
   * @name increasesBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.increasesBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.increasesBy, true)
      .to.increase(obj, prop).by(delta);
  }

  /**
   * ### .doesNotIncrease(function, object, property, [message])
   *
   * Asserts that a function does not increase a numeric object property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 8 };
   *     assert.doesNotIncrease(fn, obj, 'val');
   *
   * @name doesNotIncrease
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.doesNotIncrease = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.doesNotIncrease, true)
      .to.not.increase(obj, prop);
  }

  /**
   * ### .increasesButNotBy(function, object, property, delta, [message])
   *
   * Asserts that a function does not increase a numeric object property or function's return value by an amount (delta).
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 15 };
   *     assert.increasesButNotBy(fn, obj, 'val', 10);
   *
   * @name increasesButNotBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.increasesButNotBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.increasesButNotBy, true)
      .to.increase(obj, prop).but.not.by(delta);
  }

  /**
   * ### .decreases(function, object, property, [message])
   *
   * Asserts that a function decreases a numeric object property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 5 };
   *     assert.decreases(fn, obj, 'val');
   *
   * @name decreases
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.decreases = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.decreases, true)
      .to.decrease(obj, prop);
  }

  /**
   * ### .decreasesBy(function, object, property, delta, [message])
   *
   * Asserts that a function decreases a numeric object property or a function's return value by an amount (delta)
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val -= 5 };
   *     assert.decreasesBy(fn, obj, 'val', 5);
   *
   * @name decreasesBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.decreasesBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.decreasesBy, true)
      .to.decrease(obj, prop).by(delta);
  }

  /**
   * ### .doesNotDecrease(function, object, property, [message])
   *
   * Asserts that a function does not decreases a numeric object property.
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 15 };
   *     assert.doesNotDecrease(fn, obj, 'val');
   *
   * @name doesNotDecrease
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.doesNotDecrease = function (fn, obj, prop, msg) {
    if (arguments.length === 3 && typeof obj === 'function') {
      msg = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.doesNotDecrease, true)
      .to.not.decrease(obj, prop);
  }

  /**
   * ### .doesNotDecreaseBy(function, object, property, delta, [message])
   *
   * Asserts that a function does not decreases a numeric object property or a function's return value by an amount (delta)
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 5 };
   *     assert.doesNotDecreaseBy(fn, obj, 'val', 1);
   *
   * @name doesNotDecreaseBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.doesNotDecreaseBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    return new Assertion(fn, msg, assert.doesNotDecreaseBy, true)
      .to.not.decrease(obj, prop).by(delta);
  }

  /**
   * ### .decreasesButNotBy(function, object, property, delta, [message])
   *
   * Asserts that a function does not decreases a numeric object property or a function's return value by an amount (delta)
   *
   *     var obj = { val: 10 };
   *     var fn = function() { obj.val = 5 };
   *     assert.decreasesButNotBy(fn, obj, 'val', 1);
   *
   * @name decreasesButNotBy
   * @param {Function} modifier function
   * @param {Object} object or getter function
   * @param {String} property name _optional_
   * @param {Number} change amount (delta)
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.decreasesButNotBy = function (fn, obj, prop, delta, msg) {
    if (arguments.length === 4 && typeof obj === 'function') {
      var tmpMsg = delta;
      delta = prop;
      msg = tmpMsg;
    } else if (arguments.length === 3) {
      delta = prop;
      prop = null;
    }

    new Assertion(fn, msg, assert.decreasesButNotBy, true)
      .to.decrease(obj, prop).but.not.by(delta);
  }

  /*!
   * ### .ifError(object)
   *
   * Asserts if value is not a false value, and throws if it is a true value.
   * This is added to allow for chai to be a drop-in replacement for Node's
   * assert class.
   *
   *     var err = new Error('I am a custom error');
   *     assert.ifError(err); // Rethrows err!
   *
   * @name ifError
   * @param {Object} object
   * @namespace Assert
   * @api public
   */

  assert.ifError = function (val) {
    if (val) {
      throw(val);
    }
  };

  /**
   * ### .isExtensible(object)
   *
   * Asserts that `object` is extensible (can have new properties added to it).
   *
   *     assert.isExtensible({});
   *
   * @name isExtensible
   * @alias extensible
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isExtensible = function (obj, msg) {
    new Assertion(obj, msg, assert.isExtensible, true).to.be.extensible;
  };

  /**
   * ### .isNotExtensible(object)
   *
   * Asserts that `object` is _not_ extensible.
   *
   *     var nonExtensibleObject = Object.preventExtensions({});
   *     var sealedObject = Object.seal({});
   *     var frozenObject = Object.freeze({});
   *
   *     assert.isNotExtensible(nonExtensibleObject);
   *     assert.isNotExtensible(sealedObject);
   *     assert.isNotExtensible(frozenObject);
   *
   * @name isNotExtensible
   * @alias notExtensible
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isNotExtensible = function (obj, msg) {
    new Assertion(obj, msg, assert.isNotExtensible, true).to.not.be.extensible;
  };

  /**
   * ### .isSealed(object)
   *
   * Asserts that `object` is sealed (cannot have new properties added to it
   * and its existing properties cannot be removed).
   *
   *     var sealedObject = Object.seal({});
   *     var frozenObject = Object.seal({});
   *
   *     assert.isSealed(sealedObject);
   *     assert.isSealed(frozenObject);
   *
   * @name isSealed
   * @alias sealed
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isSealed = function (obj, msg) {
    new Assertion(obj, msg, assert.isSealed, true).to.be.sealed;
  };

  /**
   * ### .isNotSealed(object)
   *
   * Asserts that `object` is _not_ sealed.
   *
   *     assert.isNotSealed({});
   *
   * @name isNotSealed
   * @alias notSealed
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isNotSealed = function (obj, msg) {
    new Assertion(obj, msg, assert.isNotSealed, true).to.not.be.sealed;
  };

  /**
   * ### .isFrozen(object)
   *
   * Asserts that `object` is frozen (cannot have new properties added to it
   * and its existing properties cannot be modified).
   *
   *     var frozenObject = Object.freeze({});
   *     assert.frozen(frozenObject);
   *
   * @name isFrozen
   * @alias frozen
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isFrozen = function (obj, msg) {
    new Assertion(obj, msg, assert.isFrozen, true).to.be.frozen;
  };

  /**
   * ### .isNotFrozen(object)
   *
   * Asserts that `object` is _not_ frozen.
   *
   *     assert.isNotFrozen({});
   *
   * @name isNotFrozen
   * @alias notFrozen
   * @param {Object} object
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isNotFrozen = function (obj, msg) {
    new Assertion(obj, msg, assert.isNotFrozen, true).to.not.be.frozen;
  };

  /**
   * ### .isEmpty(target)
   *
   * Asserts that the target does not contain any values.
   * For arrays and strings, it checks the `length` property.
   * For `Map` and `Set` instances, it checks the `size` property.
   * For non-function objects, it gets the count of own
   * enumerable string keys.
   *
   *     assert.isEmpty([]);
   *     assert.isEmpty('');
   *     assert.isEmpty(new Map);
   *     assert.isEmpty({});
   *
   * @name isEmpty
   * @alias empty
   * @param {Object|Array|String|Map|Set} target
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isEmpty = function(val, msg) {
    new Assertion(val, msg, assert.isEmpty, true).to.be.empty;
  };

  /**
   * ### .isNotEmpty(target)
   *
   * Asserts that the target contains values.
   * For arrays and strings, it checks the `length` property.
   * For `Map` and `Set` instances, it checks the `size` property.
   * For non-function objects, it gets the count of own
   * enumerable string keys.
   *
   *     assert.isNotEmpty([1, 2]);
   *     assert.isNotEmpty('34');
   *     assert.isNotEmpty(new Set([5, 6]));
   *     assert.isNotEmpty({ key: 7 });
   *
   * @name isNotEmpty
   * @alias notEmpty
   * @param {Object|Array|String|Map|Set} target
   * @param {String} message _optional_
   * @namespace Assert
   * @api public
   */

  assert.isNotEmpty = function(val, msg) {
    new Assertion(val, msg, assert.isNotEmpty, true).to.not.be.empty;
  };

  /*!
   * Aliases.
   */

  (function alias(name, as){
    assert[as] = assert[name];
    return alias;
  })
  ('isOk', 'ok')
  ('isNotOk', 'notOk')
  ('throws', 'throw')
  ('throws', 'Throw')
  ('isExtensible', 'extensible')
  ('isNotExtensible', 'notExtensible')
  ('isSealed', 'sealed')
  ('isNotSealed', 'notSealed')
  ('isFrozen', 'frozen')
  ('isNotFrozen', 'notFrozen')
  ('isEmpty', 'empty')
  ('isNotEmpty', 'notEmpty');
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/interface/expect.js":
/*!********************************************************!*\
  !*** ./node_modules/chai/lib/chai/interface/expect.js ***!
  \********************************************************/
/***/ ((module) => {

/*!
 * chai
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

module.exports = function (chai, util) {
  chai.expect = function (val, message) {
    return new chai.Assertion(val, message);
  };

  /**
   * ### .fail([message])
   * ### .fail(actual, expected, [message], [operator])
   *
   * Throw a failure.
   *
   *     expect.fail();
   *     expect.fail("custom error message");
   *     expect.fail(1, 2);
   *     expect.fail(1, 2, "custom error message");
   *     expect.fail(1, 2, "custom error message", ">");
   *     expect.fail(1, 2, undefined, ">");
   *
   * @name fail
   * @param {Mixed} actual
   * @param {Mixed} expected
   * @param {String} message
   * @param {String} operator
   * @namespace BDD
   * @api public
   */

  chai.expect.fail = function (actual, expected, message, operator) {
    if (arguments.length < 2) {
        message = actual;
        actual = undefined;
    }

    message = message || 'expect.fail()';
    throw new chai.AssertionError(message, {
        actual: actual
      , expected: expected
      , operator: operator
    }, chai.expect.fail);
  };
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/interface/should.js":
/*!********************************************************!*\
  !*** ./node_modules/chai/lib/chai/interface/should.js ***!
  \********************************************************/
/***/ ((module) => {

/*!
 * chai
 * Copyright(c) 2011-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

module.exports = function (chai, util) {
  var Assertion = chai.Assertion;

  function loadShould () {
    // explicitly define this method as function as to have it's name to include as `ssfi`
    function shouldGetter() {
      if (this instanceof String
          || this instanceof Number
          || this instanceof Boolean
          || typeof Symbol === 'function' && this instanceof Symbol
          || typeof BigInt === 'function' && this instanceof BigInt) {
        return new Assertion(this.valueOf(), null, shouldGetter);
      }
      return new Assertion(this, null, shouldGetter);
    }
    function shouldSetter(value) {
      // See https://github.com/chaijs/chai/issues/86: this makes
      // `whatever.should = someValue` actually set `someValue`, which is
      // especially useful for `global.should = require('chai').should()`.
      //
      // Note that we have to use [[DefineProperty]] instead of [[Put]]
      // since otherwise we would trigger this very setter!
      Object.defineProperty(this, 'should', {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    }
    // modify Object.prototype to have `should`
    Object.defineProperty(Object.prototype, 'should', {
      set: shouldSetter
      , get: shouldGetter
      , configurable: true
    });

    var should = {};

    /**
     * ### .fail([message])
     * ### .fail(actual, expected, [message], [operator])
     *
     * Throw a failure.
     *
     *     should.fail();
     *     should.fail("custom error message");
     *     should.fail(1, 2);
     *     should.fail(1, 2, "custom error message");
     *     should.fail(1, 2, "custom error message", ">");
     *     should.fail(1, 2, undefined, ">");
     *
     *
     * @name fail
     * @param {Mixed} actual
     * @param {Mixed} expected
     * @param {String} message
     * @param {String} operator
     * @namespace BDD
     * @api public
     */

    should.fail = function (actual, expected, message, operator) {
      if (arguments.length < 2) {
          message = actual;
          actual = undefined;
      }

      message = message || 'should.fail()';
      throw new chai.AssertionError(message, {
          actual: actual
        , expected: expected
        , operator: operator
      }, should.fail);
    };

    /**
     * ### .equal(actual, expected, [message])
     *
     * Asserts non-strict equality (`==`) of `actual` and `expected`.
     *
     *     should.equal(3, '3', '== coerces values to strings');
     *
     * @name equal
     * @param {Mixed} actual
     * @param {Mixed} expected
     * @param {String} message
     * @namespace Should
     * @api public
     */

    should.equal = function (val1, val2, msg) {
      new Assertion(val1, msg).to.equal(val2);
    };

    /**
     * ### .throw(function, [constructor/string/regexp], [string/regexp], [message])
     *
     * Asserts that `function` will throw an error that is an instance of
     * `constructor`, or alternately that it will throw an error with message
     * matching `regexp`.
     *
     *     should.throw(fn, 'function throws a reference error');
     *     should.throw(fn, /function throws a reference error/);
     *     should.throw(fn, ReferenceError);
     *     should.throw(fn, ReferenceError, 'function throws a reference error');
     *     should.throw(fn, ReferenceError, /function throws a reference error/);
     *
     * @name throw
     * @alias Throw
     * @param {Function} function
     * @param {ErrorConstructor} constructor
     * @param {RegExp} regexp
     * @param {String} message
     * @see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Error_types
     * @namespace Should
     * @api public
     */

    should.Throw = function (fn, errt, errs, msg) {
      new Assertion(fn, msg).to.Throw(errt, errs);
    };

    /**
     * ### .exist
     *
     * Asserts that the target is neither `null` nor `undefined`.
     *
     *     var foo = 'hi';
     *
     *     should.exist(foo, 'foo exists');
     *
     * @name exist
     * @namespace Should
     * @api public
     */

    should.exist = function (val, msg) {
      new Assertion(val, msg).to.exist;
    }

    // negation
    should.not = {}

    /**
     * ### .not.equal(actual, expected, [message])
     *
     * Asserts non-strict inequality (`!=`) of `actual` and `expected`.
     *
     *     should.not.equal(3, 4, 'these numbers are not equal');
     *
     * @name not.equal
     * @param {Mixed} actual
     * @param {Mixed} expected
     * @param {String} message
     * @namespace Should
     * @api public
     */

    should.not.equal = function (val1, val2, msg) {
      new Assertion(val1, msg).to.not.equal(val2);
    };

    /**
     * ### .throw(function, [constructor/regexp], [message])
     *
     * Asserts that `function` will _not_ throw an error that is an instance of
     * `constructor`, or alternately that it will not throw an error with message
     * matching `regexp`.
     *
     *     should.not.throw(fn, Error, 'function does not throw');
     *
     * @name not.throw
     * @alias not.Throw
     * @param {Function} function
     * @param {ErrorConstructor} constructor
     * @param {RegExp} regexp
     * @param {String} message
     * @see https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Error_types
     * @namespace Should
     * @api public
     */

    should.not.Throw = function (fn, errt, errs, msg) {
      new Assertion(fn, msg).to.not.Throw(errt, errs);
    };

    /**
     * ### .not.exist
     *
     * Asserts that the target is neither `null` nor `undefined`.
     *
     *     var bar = null;
     *
     *     should.not.exist(bar, 'bar does not exist');
     *
     * @name not.exist
     * @namespace Should
     * @api public
     */

    should.not.exist = function (val, msg) {
      new Assertion(val, msg).to.not.exist;
    }

    should['throw'] = should['Throw'];
    should.not['throw'] = should.not['Throw'];

    return should;
  };

  chai.should = loadShould;
  chai.Should = loadShould;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/addChainableMethod.js":
/*!****************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/addChainableMethod.js ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - addChainingMethod utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var addLengthGuard = __webpack_require__(/*! ./addLengthGuard */ "./node_modules/chai/lib/chai/utils/addLengthGuard.js");
var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var proxify = __webpack_require__(/*! ./proxify */ "./node_modules/chai/lib/chai/utils/proxify.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/*!
 * Module variables
 */

// Check whether `Object.setPrototypeOf` is supported
var canSetPrototype = typeof Object.setPrototypeOf === 'function';

// Without `Object.setPrototypeOf` support, this module will need to add properties to a function.
// However, some of functions' own props are not configurable and should be skipped.
var testFn = function() {};
var excludeNames = Object.getOwnPropertyNames(testFn).filter(function(name) {
  var propDesc = Object.getOwnPropertyDescriptor(testFn, name);

  // Note: PhantomJS 1.x includes `callee` as one of `testFn`'s own properties,
  // but then returns `undefined` as the property descriptor for `callee`. As a
  // workaround, we perform an otherwise unnecessary type-check for `propDesc`,
  // and then filter it out if it's not an object as it should be.
  if (typeof propDesc !== 'object')
    return true;

  return !propDesc.configurable;
});

// Cache `Function` properties
var call  = Function.prototype.call,
    apply = Function.prototype.apply;

/**
 * ### .addChainableMethod(ctx, name, method, chainingBehavior)
 *
 * Adds a method to an object, such that the method can also be chained.
 *
 *     utils.addChainableMethod(chai.Assertion.prototype, 'foo', function (str) {
 *       var obj = utils.flag(this, 'object');
 *       new chai.Assertion(obj).to.be.equal(str);
 *     });
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.addChainableMethod('foo', fn, chainingBehavior);
 *
 * The result can then be used as both a method assertion, executing both `method` and
 * `chainingBehavior`, or as a language chain, which only executes `chainingBehavior`.
 *
 *     expect(fooStr).to.be.foo('bar');
 *     expect(fooStr).to.be.foo.equal('foo');
 *
 * @param {Object} ctx object to which the method is added
 * @param {String} name of method to add
 * @param {Function} method function to be used for `name`, when called
 * @param {Function} chainingBehavior function to be called every time the property is accessed
 * @namespace Utils
 * @name addChainableMethod
 * @api public
 */

module.exports = function addChainableMethod(ctx, name, method, chainingBehavior) {
  if (typeof chainingBehavior !== 'function') {
    chainingBehavior = function () { };
  }

  var chainableBehavior = {
      method: method
    , chainingBehavior: chainingBehavior
  };

  // save the methods so we can overwrite them later, if we need to.
  if (!ctx.__methods) {
    ctx.__methods = {};
  }
  ctx.__methods[name] = chainableBehavior;

  Object.defineProperty(ctx, name,
    { get: function chainableMethodGetter() {
        chainableBehavior.chainingBehavior.call(this);

        var chainableMethodWrapper = function () {
          // Setting the `ssfi` flag to `chainableMethodWrapper` causes this
          // function to be the starting point for removing implementation
          // frames from the stack trace of a failed assertion.
          //
          // However, we only want to use this function as the starting point if
          // the `lockSsfi` flag isn't set.
          //
          // If the `lockSsfi` flag is set, then this assertion is being
          // invoked from inside of another assertion. In this case, the `ssfi`
          // flag has already been set by the outer assertion.
          //
          // Note that overwriting a chainable method merely replaces the saved
          // methods in `ctx.__methods` instead of completely replacing the
          // overwritten assertion. Therefore, an overwriting assertion won't
          // set the `ssfi` or `lockSsfi` flags.
          if (!flag(this, 'lockSsfi')) {
            flag(this, 'ssfi', chainableMethodWrapper);
          }

          var result = chainableBehavior.method.apply(this, arguments);
          if (result !== undefined) {
            return result;
          }

          var newAssertion = new chai.Assertion();
          transferFlags(this, newAssertion);
          return newAssertion;
        };

        addLengthGuard(chainableMethodWrapper, name, true);

        // Use `Object.setPrototypeOf` if available
        if (canSetPrototype) {
          // Inherit all properties from the object by replacing the `Function` prototype
          var prototype = Object.create(this);
          // Restore the `call` and `apply` methods from `Function`
          prototype.call = call;
          prototype.apply = apply;
          Object.setPrototypeOf(chainableMethodWrapper, prototype);
        }
        // Otherwise, redefine all properties (slow!)
        else {
          var asserterNames = Object.getOwnPropertyNames(ctx);
          asserterNames.forEach(function (asserterName) {
            if (excludeNames.indexOf(asserterName) !== -1) {
              return;
            }

            var pd = Object.getOwnPropertyDescriptor(ctx, asserterName);
            Object.defineProperty(chainableMethodWrapper, asserterName, pd);
          });
        }

        transferFlags(this, chainableMethodWrapper);
        return proxify(chainableMethodWrapper);
      }
    , configurable: true
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/addLengthGuard.js":
/*!************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/addLengthGuard.js ***!
  \************************************************************/
/***/ ((module) => {

var fnLengthDesc = Object.getOwnPropertyDescriptor(function () {}, 'length');

/*!
 * Chai - addLengthGuard utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .addLengthGuard(fn, assertionName, isChainable)
 *
 * Define `length` as a getter on the given uninvoked method assertion. The
 * getter acts as a guard against chaining `length` directly off of an uninvoked
 * method assertion, which is a problem because it references `function`'s
 * built-in `length` property instead of Chai's `length` assertion. When the
 * getter catches the user making this mistake, it throws an error with a
 * helpful message.
 *
 * There are two ways in which this mistake can be made. The first way is by
 * chaining the `length` assertion directly off of an uninvoked chainable
 * method. In this case, Chai suggests that the user use `lengthOf` instead. The
 * second way is by chaining the `length` assertion directly off of an uninvoked
 * non-chainable method. Non-chainable methods must be invoked prior to
 * chaining. In this case, Chai suggests that the user consult the docs for the
 * given assertion.
 *
 * If the `length` property of functions is unconfigurable, then return `fn`
 * without modification.
 *
 * Note that in ES6, the function's `length` property is configurable, so once
 * support for legacy environments is dropped, Chai's `length` property can
 * replace the built-in function's `length` property, and this length guard will
 * no longer be necessary. In the mean time, maintaining consistency across all
 * environments is the priority.
 *
 * @param {Function} fn
 * @param {String} assertionName
 * @param {Boolean} isChainable
 * @namespace Utils
 * @name addLengthGuard
 */

module.exports = function addLengthGuard (fn, assertionName, isChainable) {
  if (!fnLengthDesc.configurable) return fn;

  Object.defineProperty(fn, 'length', {
    get: function () {
      if (isChainable) {
        throw Error('Invalid Chai property: ' + assertionName + '.length. Due' +
          ' to a compatibility issue, "length" cannot directly follow "' +
          assertionName + '". Use "' + assertionName + '.lengthOf" instead.');
      }

      throw Error('Invalid Chai property: ' + assertionName + '.length. See' +
        ' docs for proper usage of "' + assertionName + '".');
    }
  });

  return fn;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/addMethod.js":
/*!*******************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/addMethod.js ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - addMethod utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var addLengthGuard = __webpack_require__(/*! ./addLengthGuard */ "./node_modules/chai/lib/chai/utils/addLengthGuard.js");
var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var proxify = __webpack_require__(/*! ./proxify */ "./node_modules/chai/lib/chai/utils/proxify.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/**
 * ### .addMethod(ctx, name, method)
 *
 * Adds a method to the prototype of an object.
 *
 *     utils.addMethod(chai.Assertion.prototype, 'foo', function (str) {
 *       var obj = utils.flag(this, 'object');
 *       new chai.Assertion(obj).to.be.equal(str);
 *     });
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.addMethod('foo', fn);
 *
 * Then can be used as any other assertion.
 *
 *     expect(fooStr).to.be.foo('bar');
 *
 * @param {Object} ctx object to which the method is added
 * @param {String} name of method to add
 * @param {Function} method function to be used for name
 * @namespace Utils
 * @name addMethod
 * @api public
 */

module.exports = function addMethod(ctx, name, method) {
  var methodWrapper = function () {
    // Setting the `ssfi` flag to `methodWrapper` causes this function to be the
    // starting point for removing implementation frames from the stack trace of
    // a failed assertion.
    //
    // However, we only want to use this function as the starting point if the
    // `lockSsfi` flag isn't set.
    //
    // If the `lockSsfi` flag is set, then either this assertion has been
    // overwritten by another assertion, or this assertion is being invoked from
    // inside of another assertion. In the first case, the `ssfi` flag has
    // already been set by the overwriting assertion. In the second case, the
    // `ssfi` flag has already been set by the outer assertion.
    if (!flag(this, 'lockSsfi')) {
      flag(this, 'ssfi', methodWrapper);
    }

    var result = method.apply(this, arguments);
    if (result !== undefined)
      return result;

    var newAssertion = new chai.Assertion();
    transferFlags(this, newAssertion);
    return newAssertion;
  };

  addLengthGuard(methodWrapper, name, false);
  ctx[name] = proxify(methodWrapper, name);
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/addProperty.js":
/*!*********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/addProperty.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - addProperty utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var isProxyEnabled = __webpack_require__(/*! ./isProxyEnabled */ "./node_modules/chai/lib/chai/utils/isProxyEnabled.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/**
 * ### .addProperty(ctx, name, getter)
 *
 * Adds a property to the prototype of an object.
 *
 *     utils.addProperty(chai.Assertion.prototype, 'foo', function () {
 *       var obj = utils.flag(this, 'object');
 *       new chai.Assertion(obj).to.be.instanceof(Foo);
 *     });
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.addProperty('foo', fn);
 *
 * Then can be used as any other assertion.
 *
 *     expect(myFoo).to.be.foo;
 *
 * @param {Object} ctx object to which the property is added
 * @param {String} name of property to add
 * @param {Function} getter function to be used for name
 * @namespace Utils
 * @name addProperty
 * @api public
 */

module.exports = function addProperty(ctx, name, getter) {
  getter = getter === undefined ? function () {} : getter;

  Object.defineProperty(ctx, name,
    { get: function propertyGetter() {
        // Setting the `ssfi` flag to `propertyGetter` causes this function to
        // be the starting point for removing implementation frames from the
        // stack trace of a failed assertion.
        //
        // However, we only want to use this function as the starting point if
        // the `lockSsfi` flag isn't set and proxy protection is disabled.
        //
        // If the `lockSsfi` flag is set, then either this assertion has been
        // overwritten by another assertion, or this assertion is being invoked
        // from inside of another assertion. In the first case, the `ssfi` flag
        // has already been set by the overwriting assertion. In the second
        // case, the `ssfi` flag has already been set by the outer assertion.
        //
        // If proxy protection is enabled, then the `ssfi` flag has already been
        // set by the proxy getter.
        if (!isProxyEnabled() && !flag(this, 'lockSsfi')) {
          flag(this, 'ssfi', propertyGetter);
        }

        var result = getter.call(this);
        if (result !== undefined)
          return result;

        var newAssertion = new chai.Assertion();
        transferFlags(this, newAssertion);
        return newAssertion;
      }
    , configurable: true
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/compareByInspect.js":
/*!**************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/compareByInspect.js ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - compareByInspect utility
 * Copyright(c) 2011-2016 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var inspect = __webpack_require__(/*! ./inspect */ "./node_modules/chai/lib/chai/utils/inspect.js");

/**
 * ### .compareByInspect(mixed, mixed)
 *
 * To be used as a compareFunction with Array.prototype.sort. Compares elements
 * using inspect instead of default behavior of using toString so that Symbols
 * and objects with irregular/missing toString can still be sorted without a
 * TypeError.
 *
 * @param {Mixed} first element to compare
 * @param {Mixed} second element to compare
 * @returns {Number} -1 if 'a' should come before 'b'; otherwise 1
 * @name compareByInspect
 * @namespace Utils
 * @api public
 */

module.exports = function compareByInspect(a, b) {
  return inspect(a) < inspect(b) ? -1 : 1;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/expectTypes.js":
/*!*********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/expectTypes.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - expectTypes utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .expectTypes(obj, types)
 *
 * Ensures that the object being tested against is of a valid type.
 *
 *     utils.expectTypes(this, ['array', 'object', 'string']);
 *
 * @param {Mixed} obj constructed Assertion
 * @param {Array} type A list of allowed types for this assertion
 * @namespace Utils
 * @name expectTypes
 * @api public
 */

var AssertionError = __webpack_require__(/*! assertion-error */ "./node_modules/assertion-error/index.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var type = __webpack_require__(/*! type-detect */ "./node_modules/type-detect/type-detect.js");

module.exports = function expectTypes(obj, types) {
  var flagMsg = flag(obj, 'message');
  var ssfi = flag(obj, 'ssfi');

  flagMsg = flagMsg ? flagMsg + ': ' : '';

  obj = flag(obj, 'object');
  types = types.map(function (t) { return t.toLowerCase(); });
  types.sort();

  // Transforms ['lorem', 'ipsum'] into 'a lorem, or an ipsum'
  var str = types.map(function (t, index) {
    var art = ~[ 'a', 'e', 'i', 'o', 'u' ].indexOf(t.charAt(0)) ? 'an' : 'a';
    var or = types.length > 1 && index === types.length - 1 ? 'or ' : '';
    return or + art + ' ' + t;
  }).join(', ');

  var objType = type(obj).toLowerCase();

  if (!types.some(function (expected) { return objType === expected; })) {
    throw new AssertionError(
      flagMsg + 'object tested must be ' + str + ', but ' + objType + ' given',
      undefined,
      ssfi
    );
  }
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/flag.js":
/*!**************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/flag.js ***!
  \**************************************************/
/***/ ((module) => {

/*!
 * Chai - flag utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .flag(object, key, [value])
 *
 * Get or set a flag value on an object. If a
 * value is provided it will be set, else it will
 * return the currently set value or `undefined` if
 * the value is not set.
 *
 *     utils.flag(this, 'foo', 'bar'); // setter
 *     utils.flag(this, 'foo'); // getter, returns `bar`
 *
 * @param {Object} object constructed Assertion
 * @param {String} key
 * @param {Mixed} value (optional)
 * @namespace Utils
 * @name flag
 * @api private
 */

module.exports = function flag(obj, key, value) {
  var flags = obj.__flags || (obj.__flags = Object.create(null));
  if (arguments.length === 3) {
    flags[key] = value;
  } else {
    return flags[key];
  }
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getActual.js":
/*!*******************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getActual.js ***!
  \*******************************************************/
/***/ ((module) => {

/*!
 * Chai - getActual utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .getActual(object, [actual])
 *
 * Returns the `actual` value for an Assertion.
 *
 * @param {Object} object (constructed Assertion)
 * @param {Arguments} chai.Assertion.prototype.assert arguments
 * @namespace Utils
 * @name getActual
 */

module.exports = function getActual(obj, args) {
  return args.length > 4 ? args[4] : obj._obj;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getEnumerableProperties.js":
/*!*********************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getEnumerableProperties.js ***!
  \*********************************************************************/
/***/ ((module) => {

/*!
 * Chai - getEnumerableProperties utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .getEnumerableProperties(object)
 *
 * This allows the retrieval of enumerable property names of an object,
 * inherited or not.
 *
 * @param {Object} object
 * @returns {Array}
 * @namespace Utils
 * @name getEnumerableProperties
 * @api public
 */

module.exports = function getEnumerableProperties(object) {
  var result = [];
  for (var name in object) {
    result.push(name);
  }
  return result;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getMessage.js":
/*!********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getMessage.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - message composition utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js")
  , getActual = __webpack_require__(/*! ./getActual */ "./node_modules/chai/lib/chai/utils/getActual.js")
  , objDisplay = __webpack_require__(/*! ./objDisplay */ "./node_modules/chai/lib/chai/utils/objDisplay.js");

/**
 * ### .getMessage(object, message, negateMessage)
 *
 * Construct the error message based on flags
 * and template tags. Template tags will return
 * a stringified inspection of the object referenced.
 *
 * Message template tags:
 * - `#{this}` current asserted object
 * - `#{act}` actual value
 * - `#{exp}` expected value
 *
 * @param {Object} object (constructed Assertion)
 * @param {Arguments} chai.Assertion.prototype.assert arguments
 * @namespace Utils
 * @name getMessage
 * @api public
 */

module.exports = function getMessage(obj, args) {
  var negate = flag(obj, 'negate')
    , val = flag(obj, 'object')
    , expected = args[3]
    , actual = getActual(obj, args)
    , msg = negate ? args[2] : args[1]
    , flagMsg = flag(obj, 'message');

  if(typeof msg === "function") msg = msg();
  msg = msg || '';
  msg = msg
    .replace(/#\{this\}/g, function () { return objDisplay(val); })
    .replace(/#\{act\}/g, function () { return objDisplay(actual); })
    .replace(/#\{exp\}/g, function () { return objDisplay(expected); });

  return flagMsg ? flagMsg + ': ' + msg : msg;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getOperator.js":
/*!*********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getOperator.js ***!
  \*********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var type = __webpack_require__(/*! type-detect */ "./node_modules/type-detect/type-detect.js");

var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");

function isObjectType(obj) {
  var objectType = type(obj);
  var objectTypes = ['Array', 'Object', 'function'];

  return objectTypes.indexOf(objectType) !== -1;
}

/**
 * ### .getOperator(message)
 *
 * Extract the operator from error message.
 * Operator defined is based on below link
 * https://nodejs.org/api/assert.html#assert_assert.
 *
 * Returns the `operator` or `undefined` value for an Assertion.
 *
 * @param {Object} object (constructed Assertion)
 * @param {Arguments} chai.Assertion.prototype.assert arguments
 * @namespace Utils
 * @name getOperator
 * @api public
 */

module.exports = function getOperator(obj, args) {
  var operator = flag(obj, 'operator');
  var negate = flag(obj, 'negate');
  var expected = args[3];
  var msg = negate ? args[2] : args[1];

  if (operator) {
    return operator;
  }

  if (typeof msg === 'function') msg = msg();

  msg = msg || '';
  if (!msg) {
    return undefined;
  }

  if (/\shave\s/.test(msg)) {
    return undefined;
  }

  var isObject = isObjectType(expected);
  if (/\snot\s/.test(msg)) {
    return isObject ? 'notDeepStrictEqual' : 'notStrictEqual';
  }

  return isObject ? 'deepStrictEqual' : 'strictEqual';
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getOwnEnumerableProperties.js":
/*!************************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getOwnEnumerableProperties.js ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - getOwnEnumerableProperties utility
 * Copyright(c) 2011-2016 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var getOwnEnumerablePropertySymbols = __webpack_require__(/*! ./getOwnEnumerablePropertySymbols */ "./node_modules/chai/lib/chai/utils/getOwnEnumerablePropertySymbols.js");

/**
 * ### .getOwnEnumerableProperties(object)
 *
 * This allows the retrieval of directly-owned enumerable property names and
 * symbols of an object. This function is necessary because Object.keys only
 * returns enumerable property names, not enumerable property symbols.
 *
 * @param {Object} object
 * @returns {Array}
 * @namespace Utils
 * @name getOwnEnumerableProperties
 * @api public
 */

module.exports = function getOwnEnumerableProperties(obj) {
  return Object.keys(obj).concat(getOwnEnumerablePropertySymbols(obj));
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getOwnEnumerablePropertySymbols.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getOwnEnumerablePropertySymbols.js ***!
  \*****************************************************************************/
/***/ ((module) => {

/*!
 * Chai - getOwnEnumerablePropertySymbols utility
 * Copyright(c) 2011-2016 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .getOwnEnumerablePropertySymbols(object)
 *
 * This allows the retrieval of directly-owned enumerable property symbols of an
 * object. This function is necessary because Object.getOwnPropertySymbols
 * returns both enumerable and non-enumerable property symbols.
 *
 * @param {Object} object
 * @returns {Array}
 * @namespace Utils
 * @name getOwnEnumerablePropertySymbols
 * @api public
 */

module.exports = function getOwnEnumerablePropertySymbols(obj) {
  if (typeof Object.getOwnPropertySymbols !== 'function') return [];

  return Object.getOwnPropertySymbols(obj).filter(function (sym) {
    return Object.getOwnPropertyDescriptor(obj, sym).enumerable;
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/getProperties.js":
/*!***********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/getProperties.js ***!
  \***********************************************************/
/***/ ((module) => {

/*!
 * Chai - getProperties utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .getProperties(object)
 *
 * This allows the retrieval of property names of an object, enumerable or not,
 * inherited or not.
 *
 * @param {Object} object
 * @returns {Array}
 * @namespace Utils
 * @name getProperties
 * @api public
 */

module.exports = function getProperties(object) {
  var result = Object.getOwnPropertyNames(object);

  function addProperty(property) {
    if (result.indexOf(property) === -1) {
      result.push(property);
    }
  }

  var proto = Object.getPrototypeOf(object);
  while (proto !== null) {
    Object.getOwnPropertyNames(proto).forEach(addProperty);
    proto = Object.getPrototypeOf(proto);
  }

  return result;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/index.js":
/*!***************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/index.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

/*!
 * chai
 * Copyright(c) 2011 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Dependencies that are used for multiple exports are required here only once
 */

var pathval = __webpack_require__(/*! pathval */ "./node_modules/pathval/index.js");

/*!
 * test utility
 */

exports.test = __webpack_require__(/*! ./test */ "./node_modules/chai/lib/chai/utils/test.js");

/*!
 * type utility
 */

exports.type = __webpack_require__(/*! type-detect */ "./node_modules/type-detect/type-detect.js");

/*!
 * expectTypes utility
 */
exports.expectTypes = __webpack_require__(/*! ./expectTypes */ "./node_modules/chai/lib/chai/utils/expectTypes.js");

/*!
 * message utility
 */

exports.getMessage = __webpack_require__(/*! ./getMessage */ "./node_modules/chai/lib/chai/utils/getMessage.js");

/*!
 * actual utility
 */

exports.getActual = __webpack_require__(/*! ./getActual */ "./node_modules/chai/lib/chai/utils/getActual.js");

/*!
 * Inspect util
 */

exports.inspect = __webpack_require__(/*! ./inspect */ "./node_modules/chai/lib/chai/utils/inspect.js");

/*!
 * Object Display util
 */

exports.objDisplay = __webpack_require__(/*! ./objDisplay */ "./node_modules/chai/lib/chai/utils/objDisplay.js");

/*!
 * Flag utility
 */

exports.flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");

/*!
 * Flag transferring utility
 */

exports.transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/*!
 * Deep equal utility
 */

exports.eql = __webpack_require__(/*! deep-eql */ "./node_modules/deep-eql/index.js");

/*!
 * Deep path info
 */

exports.getPathInfo = pathval.getPathInfo;

/*!
 * Check if a property exists
 */

exports.hasProperty = pathval.hasProperty;

/*!
 * Function name
 */

exports.getName = __webpack_require__(/*! get-func-name */ "./node_modules/get-func-name/index.js");

/*!
 * add Property
 */

exports.addProperty = __webpack_require__(/*! ./addProperty */ "./node_modules/chai/lib/chai/utils/addProperty.js");

/*!
 * add Method
 */

exports.addMethod = __webpack_require__(/*! ./addMethod */ "./node_modules/chai/lib/chai/utils/addMethod.js");

/*!
 * overwrite Property
 */

exports.overwriteProperty = __webpack_require__(/*! ./overwriteProperty */ "./node_modules/chai/lib/chai/utils/overwriteProperty.js");

/*!
 * overwrite Method
 */

exports.overwriteMethod = __webpack_require__(/*! ./overwriteMethod */ "./node_modules/chai/lib/chai/utils/overwriteMethod.js");

/*!
 * Add a chainable method
 */

exports.addChainableMethod = __webpack_require__(/*! ./addChainableMethod */ "./node_modules/chai/lib/chai/utils/addChainableMethod.js");

/*!
 * Overwrite chainable method
 */

exports.overwriteChainableMethod = __webpack_require__(/*! ./overwriteChainableMethod */ "./node_modules/chai/lib/chai/utils/overwriteChainableMethod.js");

/*!
 * Compare by inspect method
 */

exports.compareByInspect = __webpack_require__(/*! ./compareByInspect */ "./node_modules/chai/lib/chai/utils/compareByInspect.js");

/*!
 * Get own enumerable property symbols method
 */

exports.getOwnEnumerablePropertySymbols = __webpack_require__(/*! ./getOwnEnumerablePropertySymbols */ "./node_modules/chai/lib/chai/utils/getOwnEnumerablePropertySymbols.js");

/*!
 * Get own enumerable properties method
 */

exports.getOwnEnumerableProperties = __webpack_require__(/*! ./getOwnEnumerableProperties */ "./node_modules/chai/lib/chai/utils/getOwnEnumerableProperties.js");

/*!
 * Checks error against a given set of criteria
 */

exports.checkError = __webpack_require__(/*! check-error */ "./node_modules/check-error/index.js");

/*!
 * Proxify util
 */

exports.proxify = __webpack_require__(/*! ./proxify */ "./node_modules/chai/lib/chai/utils/proxify.js");

/*!
 * addLengthGuard util
 */

exports.addLengthGuard = __webpack_require__(/*! ./addLengthGuard */ "./node_modules/chai/lib/chai/utils/addLengthGuard.js");

/*!
 * isProxyEnabled helper
 */

exports.isProxyEnabled = __webpack_require__(/*! ./isProxyEnabled */ "./node_modules/chai/lib/chai/utils/isProxyEnabled.js");

/*!
 * isNaN method
 */

exports.isNaN = __webpack_require__(/*! ./isNaN */ "./node_modules/chai/lib/chai/utils/isNaN.js");

/*!
 * getOperator method
 */

exports.getOperator = __webpack_require__(/*! ./getOperator */ "./node_modules/chai/lib/chai/utils/getOperator.js");

/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/inspect.js":
/*!*****************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/inspect.js ***!
  \*****************************************************/
/***/ ((module, exports, __webpack_require__) => {

// This is (almost) directly from Node.js utils
// https://github.com/joyent/node/blob/f8c335d0caf47f16d31413f89aa28eda3878e3aa/lib/util.js

var getName = __webpack_require__(/*! get-func-name */ "./node_modules/get-func-name/index.js");
var getProperties = __webpack_require__(/*! ./getProperties */ "./node_modules/chai/lib/chai/utils/getProperties.js");
var getEnumerableProperties = __webpack_require__(/*! ./getEnumerableProperties */ "./node_modules/chai/lib/chai/utils/getEnumerableProperties.js");
var config = __webpack_require__(/*! ../config */ "./node_modules/chai/lib/chai/config.js");

module.exports = inspect;

/**
 * ### .inspect(obj, [showHidden], [depth], [colors])
 *
 * Echoes the value of a value. Tries to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Boolean} showHidden Flag that shows hidden (not enumerable)
 *    properties of objects. Default is false.
 * @param {Number} depth Depth in which to descend in object. Default is 2.
 * @param {Boolean} colors Flag to turn on ANSI escape codes to color the
 *    output. Default is false (no coloring).
 * @namespace Utils
 * @name inspect
 */
function inspect(obj, showHidden, depth, colors) {
  var ctx = {
    showHidden: showHidden,
    seen: [],
    stylize: function (str) { return str; }
  };
  return formatValue(ctx, obj, (typeof depth === 'undefined' ? 2 : depth));
}

// Returns true if object is a DOM element.
var isDOMElement = function (object) {
  if (typeof HTMLElement === 'object') {
    return object instanceof HTMLElement;
  } else {
    return object &&
      typeof object === 'object' &&
      'nodeType' in object &&
      object.nodeType === 1 &&
      typeof object.nodeName === 'string';
  }
};

function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (value && typeof value.inspect === 'function' &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (typeof ret !== 'string') {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // If this is a DOM element, try to get the outer HTML.
  if (isDOMElement(value)) {
    if ('outerHTML' in value) {
      return value.outerHTML;
      // This value does not have an outerHTML attribute,
      //   it could still be an XML element
    } else {
      // Attempt to serialize it
      try {
        if (document.xmlVersion) {
          var xmlSerializer = new XMLSerializer();
          return xmlSerializer.serializeToString(value);
        } else {
          // Firefox 11- do not support outerHTML
          //   It does, however, support innerHTML
          //   Use the following to render the element
          var ns = "http://www.w3.org/1999/xhtml";
          var container = document.createElementNS(ns, '_');

          container.appendChild(value.cloneNode(false));
          var html = container.innerHTML
            .replace('><', '>' + value.innerHTML + '<');
          container.innerHTML = '';
          return html;
        }
      } catch (err) {
        // This could be a non-native DOM implementation,
        //   continue with the normal flow:
        //   printing the element as if it is an object.
      }
    }
  }

  // Look up the keys of the object.
  var visibleKeys = getEnumerableProperties(value);
  var keys = ctx.showHidden ? getProperties(value) : visibleKeys;

  var name, nameSuffix;

  // Some type of object without properties can be shortcut.
  // In IE, errors have a single `stack` property, or if they are vanilla `Error`,
  // a `stack` plus `description` property; ignore those for consistency.
  if (keys.length === 0 || (isError(value) && (
      (keys.length === 1 && keys[0] === 'stack') ||
      (keys.length === 2 && keys[0] === 'description' && keys[1] === 'stack')
     ))) {
    if (typeof value === 'function') {
      name = getName(value);
      nameSuffix = name ? ': ' + name : '';
      return ctx.stylize('[Function' + nameSuffix + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toUTCString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = ''
    , array = false
    , typedArray = false
    , braces = ['{', '}'];

  if (isTypedArray(value)) {
    typedArray = true;
    braces = ['[', ']'];
  }

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (typeof value === 'function') {
    name = getName(value);
    nameSuffix = name ? ': ' + name : '';
    base = ' [Function' + nameSuffix + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    return formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else if (typedArray) {
    return formatTypedArray(value);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}

function formatPrimitive(ctx, value) {
  switch (typeof value) {
    case 'undefined':
      return ctx.stylize('undefined', 'undefined');

    case 'string':
      var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                               .replace(/'/g, "\\'")
                                               .replace(/\\"/g, '"') + '\'';
      return ctx.stylize(simple, 'string');

    case 'number':
      if (value === 0 && (1/value) === -Infinity) {
        return ctx.stylize('-0', 'number');
      }
      return ctx.stylize('' + value, 'number');

    case 'boolean':
      return ctx.stylize('' + value, 'boolean');

    case 'symbol':
      return ctx.stylize(value.toString(), 'symbol');

    case 'bigint':
      return ctx.stylize(value.toString() + 'n', 'bigint');
  }
  // For some reason typeof null is "object", so special case here.
  if (value === null) {
    return ctx.stylize('null', 'null');
  }
}

function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}

function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (Object.prototype.hasOwnProperty.call(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }

  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}

function formatTypedArray(value) {
  var str = '[ ';

  for (var i = 0; i < value.length; ++i) {
    if (str.length >= config.truncateThreshold - 7) {
      str += '...';
      break;
    }
    str += value[i] + ', ';
  }
  str += ' ]';

  // Removing trailing `, ` if the array was not truncated
  if (str.indexOf(',  ]') !== -1) {
    str = str.replace(',  ]', ' ]');
  }

  return str;
}

function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name;
  var propDescriptor = Object.getOwnPropertyDescriptor(value, key);
  var str;

  if (propDescriptor) {
    if (propDescriptor.get) {
      if (propDescriptor.set) {
        str = ctx.stylize('[Getter/Setter]', 'special');
      } else {
        str = ctx.stylize('[Getter]', 'special');
      }
    } else {
      if (propDescriptor.set) {
        str = ctx.stylize('[Setter]', 'special');
      }
    }
  }
  if (visibleKeys.indexOf(key) < 0) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(value[key]) < 0) {
      if (recurseTimes === null) {
        str = formatValue(ctx, value[key], null);
      } else {
        str = formatValue(ctx, value[key], recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (typeof name === 'undefined') {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}

function reduceToSingleString(output, base, braces) {
  var length = output.reduce(function(prev, cur) {
    return prev + cur.length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}

function isTypedArray(ar) {
  // Unfortunately there's no way to check if an object is a TypedArray
  // We have to check if it's one of these types
  return (typeof ar === 'object' && /\w+Array]$/.test(objectToString(ar)));
}

function isArray(ar) {
  return Array.isArray(ar) ||
         (typeof ar === 'object' && objectToString(ar) === '[object Array]');
}

function isRegExp(re) {
  return typeof re === 'object' && objectToString(re) === '[object RegExp]';
}

function isDate(d) {
  return typeof d === 'object' && objectToString(d) === '[object Date]';
}

function isError(e) {
  return typeof e === 'object' && objectToString(e) === '[object Error]';
}

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/isNaN.js":
/*!***************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/isNaN.js ***!
  \***************************************************/
/***/ ((module) => {

/*!
 * Chai - isNaN utility
 * Copyright(c) 2012-2015 Sakthipriyan Vairamani <thechargingvolcano@gmail.com>
 * MIT Licensed
 */

/**
 * ### .isNaN(value)
 *
 * Checks if the given value is NaN or not.
 *
 *     utils.isNaN(NaN); // true
 *
 * @param {Value} The value which has to be checked if it is NaN
 * @name isNaN
 * @api private
 */

function isNaN(value) {
  // Refer http://www.ecma-international.org/ecma-262/6.0/#sec-isnan-number
  // section's NOTE.
  return value !== value;
}

// If ECMAScript 6's Number.isNaN is present, prefer that.
module.exports = Number.isNaN || isNaN;


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/isProxyEnabled.js":
/*!************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/isProxyEnabled.js ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var config = __webpack_require__(/*! ../config */ "./node_modules/chai/lib/chai/config.js");

/*!
 * Chai - isProxyEnabled helper
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .isProxyEnabled()
 *
 * Helper function to check if Chai's proxy protection feature is enabled. If
 * proxies are unsupported or disabled via the user's Chai config, then return
 * false. Otherwise, return true.
 *
 * @namespace Utils
 * @name isProxyEnabled
 */

module.exports = function isProxyEnabled() {
  return config.useProxy &&
    typeof Proxy !== 'undefined' &&
    typeof Reflect !== 'undefined';
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/objDisplay.js":
/*!********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/objDisplay.js ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - flag utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var inspect = __webpack_require__(/*! ./inspect */ "./node_modules/chai/lib/chai/utils/inspect.js");
var config = __webpack_require__(/*! ../config */ "./node_modules/chai/lib/chai/config.js");

/**
 * ### .objDisplay(object)
 *
 * Determines if an object or an array matches
 * criteria to be inspected in-line for error
 * messages or should be truncated.
 *
 * @param {Mixed} javascript object to inspect
 * @name objDisplay
 * @namespace Utils
 * @api public
 */

module.exports = function objDisplay(obj) {
  var str = inspect(obj)
    , type = Object.prototype.toString.call(obj);

  if (config.truncateThreshold && str.length >= config.truncateThreshold) {
    if (type === '[object Function]') {
      return !obj.name || obj.name === ''
        ? '[Function]'
        : '[Function: ' + obj.name + ']';
    } else if (type === '[object Array]') {
      return '[ Array(' + obj.length + ') ]';
    } else if (type === '[object Object]') {
      var keys = Object.keys(obj)
        , kstr = keys.length > 2
          ? keys.splice(0, 2).join(', ') + ', ...'
          : keys.join(', ');
      return '{ Object (' + kstr + ') }';
    } else {
      return str;
    }
  } else {
    return str;
  }
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/overwriteChainableMethod.js":
/*!**********************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/overwriteChainableMethod.js ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - overwriteChainableMethod utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/**
 * ### .overwriteChainableMethod(ctx, name, method, chainingBehavior)
 *
 * Overwrites an already existing chainable method
 * and provides access to the previous function or
 * property.  Must return functions to be used for
 * name.
 *
 *     utils.overwriteChainableMethod(chai.Assertion.prototype, 'lengthOf',
 *       function (_super) {
 *       }
 *     , function (_super) {
 *       }
 *     );
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.overwriteChainableMethod('foo', fn, fn);
 *
 * Then can be used as any other assertion.
 *
 *     expect(myFoo).to.have.lengthOf(3);
 *     expect(myFoo).to.have.lengthOf.above(3);
 *
 * @param {Object} ctx object whose method / property is to be overwritten
 * @param {String} name of method / property to overwrite
 * @param {Function} method function that returns a function to be used for name
 * @param {Function} chainingBehavior function that returns a function to be used for property
 * @namespace Utils
 * @name overwriteChainableMethod
 * @api public
 */

module.exports = function overwriteChainableMethod(ctx, name, method, chainingBehavior) {
  var chainableBehavior = ctx.__methods[name];

  var _chainingBehavior = chainableBehavior.chainingBehavior;
  chainableBehavior.chainingBehavior = function overwritingChainableMethodGetter() {
    var result = chainingBehavior(_chainingBehavior).call(this);
    if (result !== undefined) {
      return result;
    }

    var newAssertion = new chai.Assertion();
    transferFlags(this, newAssertion);
    return newAssertion;
  };

  var _method = chainableBehavior.method;
  chainableBehavior.method = function overwritingChainableMethodWrapper() {
    var result = method(_method).apply(this, arguments);
    if (result !== undefined) {
      return result;
    }

    var newAssertion = new chai.Assertion();
    transferFlags(this, newAssertion);
    return newAssertion;
  };
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/overwriteMethod.js":
/*!*************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/overwriteMethod.js ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - overwriteMethod utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var addLengthGuard = __webpack_require__(/*! ./addLengthGuard */ "./node_modules/chai/lib/chai/utils/addLengthGuard.js");
var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var proxify = __webpack_require__(/*! ./proxify */ "./node_modules/chai/lib/chai/utils/proxify.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/**
 * ### .overwriteMethod(ctx, name, fn)
 *
 * Overwrites an already existing method and provides
 * access to previous function. Must return function
 * to be used for name.
 *
 *     utils.overwriteMethod(chai.Assertion.prototype, 'equal', function (_super) {
 *       return function (str) {
 *         var obj = utils.flag(this, 'object');
 *         if (obj instanceof Foo) {
 *           new chai.Assertion(obj.value).to.equal(str);
 *         } else {
 *           _super.apply(this, arguments);
 *         }
 *       }
 *     });
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.overwriteMethod('foo', fn);
 *
 * Then can be used as any other assertion.
 *
 *     expect(myFoo).to.equal('bar');
 *
 * @param {Object} ctx object whose method is to be overwritten
 * @param {String} name of method to overwrite
 * @param {Function} method function that returns a function to be used for name
 * @namespace Utils
 * @name overwriteMethod
 * @api public
 */

module.exports = function overwriteMethod(ctx, name, method) {
  var _method = ctx[name]
    , _super = function () {
      throw new Error(name + ' is not a function');
    };

  if (_method && 'function' === typeof _method)
    _super = _method;

  var overwritingMethodWrapper = function () {
    // Setting the `ssfi` flag to `overwritingMethodWrapper` causes this
    // function to be the starting point for removing implementation frames from
    // the stack trace of a failed assertion.
    //
    // However, we only want to use this function as the starting point if the
    // `lockSsfi` flag isn't set.
    //
    // If the `lockSsfi` flag is set, then either this assertion has been
    // overwritten by another assertion, or this assertion is being invoked from
    // inside of another assertion. In the first case, the `ssfi` flag has
    // already been set by the overwriting assertion. In the second case, the
    // `ssfi` flag has already been set by the outer assertion.
    if (!flag(this, 'lockSsfi')) {
      flag(this, 'ssfi', overwritingMethodWrapper);
    }

    // Setting the `lockSsfi` flag to `true` prevents the overwritten assertion
    // from changing the `ssfi` flag. By this point, the `ssfi` flag is already
    // set to the correct starting point for this assertion.
    var origLockSsfi = flag(this, 'lockSsfi');
    flag(this, 'lockSsfi', true);
    var result = method(_super).apply(this, arguments);
    flag(this, 'lockSsfi', origLockSsfi);

    if (result !== undefined) {
      return result;
    }

    var newAssertion = new chai.Assertion();
    transferFlags(this, newAssertion);
    return newAssertion;
  }

  addLengthGuard(overwritingMethodWrapper, name, false);
  ctx[name] = proxify(overwritingMethodWrapper, name);
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/overwriteProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/overwriteProperty.js ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - overwriteProperty utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var chai = __webpack_require__(/*! ../../chai */ "./node_modules/chai/lib/chai.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var isProxyEnabled = __webpack_require__(/*! ./isProxyEnabled */ "./node_modules/chai/lib/chai/utils/isProxyEnabled.js");
var transferFlags = __webpack_require__(/*! ./transferFlags */ "./node_modules/chai/lib/chai/utils/transferFlags.js");

/**
 * ### .overwriteProperty(ctx, name, fn)
 *
 * Overwrites an already existing property getter and provides
 * access to previous value. Must return function to use as getter.
 *
 *     utils.overwriteProperty(chai.Assertion.prototype, 'ok', function (_super) {
 *       return function () {
 *         var obj = utils.flag(this, 'object');
 *         if (obj instanceof Foo) {
 *           new chai.Assertion(obj.name).to.equal('bar');
 *         } else {
 *           _super.call(this);
 *         }
 *       }
 *     });
 *
 *
 * Can also be accessed directly from `chai.Assertion`.
 *
 *     chai.Assertion.overwriteProperty('foo', fn);
 *
 * Then can be used as any other assertion.
 *
 *     expect(myFoo).to.be.ok;
 *
 * @param {Object} ctx object whose property is to be overwritten
 * @param {String} name of property to overwrite
 * @param {Function} getter function that returns a getter function to be used for name
 * @namespace Utils
 * @name overwriteProperty
 * @api public
 */

module.exports = function overwriteProperty(ctx, name, getter) {
  var _get = Object.getOwnPropertyDescriptor(ctx, name)
    , _super = function () {};

  if (_get && 'function' === typeof _get.get)
    _super = _get.get

  Object.defineProperty(ctx, name,
    { get: function overwritingPropertyGetter() {
        // Setting the `ssfi` flag to `overwritingPropertyGetter` causes this
        // function to be the starting point for removing implementation frames
        // from the stack trace of a failed assertion.
        //
        // However, we only want to use this function as the starting point if
        // the `lockSsfi` flag isn't set and proxy protection is disabled.
        //
        // If the `lockSsfi` flag is set, then either this assertion has been
        // overwritten by another assertion, or this assertion is being invoked
        // from inside of another assertion. In the first case, the `ssfi` flag
        // has already been set by the overwriting assertion. In the second
        // case, the `ssfi` flag has already been set by the outer assertion.
        //
        // If proxy protection is enabled, then the `ssfi` flag has already been
        // set by the proxy getter.
        if (!isProxyEnabled() && !flag(this, 'lockSsfi')) {
          flag(this, 'ssfi', overwritingPropertyGetter);
        }

        // Setting the `lockSsfi` flag to `true` prevents the overwritten
        // assertion from changing the `ssfi` flag. By this point, the `ssfi`
        // flag is already set to the correct starting point for this assertion.
        var origLockSsfi = flag(this, 'lockSsfi');
        flag(this, 'lockSsfi', true);
        var result = getter(_super).call(this);
        flag(this, 'lockSsfi', origLockSsfi);

        if (result !== undefined) {
          return result;
        }

        var newAssertion = new chai.Assertion();
        transferFlags(this, newAssertion);
        return newAssertion;
      }
    , configurable: true
  });
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/proxify.js":
/*!*****************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/proxify.js ***!
  \*****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var config = __webpack_require__(/*! ../config */ "./node_modules/chai/lib/chai/config.js");
var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");
var getProperties = __webpack_require__(/*! ./getProperties */ "./node_modules/chai/lib/chai/utils/getProperties.js");
var isProxyEnabled = __webpack_require__(/*! ./isProxyEnabled */ "./node_modules/chai/lib/chai/utils/isProxyEnabled.js");

/*!
 * Chai - proxify utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .proxify(object)
 *
 * Return a proxy of given object that throws an error when a non-existent
 * property is read. By default, the root cause is assumed to be a misspelled
 * property, and thus an attempt is made to offer a reasonable suggestion from
 * the list of existing properties. However, if a nonChainableMethodName is
 * provided, then the root cause is instead a failure to invoke a non-chainable
 * method prior to reading the non-existent property.
 *
 * If proxies are unsupported or disabled via the user's Chai config, then
 * return object without modification.
 *
 * @param {Object} obj
 * @param {String} nonChainableMethodName
 * @namespace Utils
 * @name proxify
 */

var builtins = ['__flags', '__methods', '_obj', 'assert'];

module.exports = function proxify(obj, nonChainableMethodName) {
  if (!isProxyEnabled()) return obj;

  return new Proxy(obj, {
    get: function proxyGetter(target, property) {
      // This check is here because we should not throw errors on Symbol properties
      // such as `Symbol.toStringTag`.
      // The values for which an error should be thrown can be configured using
      // the `config.proxyExcludedKeys` setting.
      if (typeof property === 'string' &&
          config.proxyExcludedKeys.indexOf(property) === -1 &&
          !Reflect.has(target, property)) {
        // Special message for invalid property access of non-chainable methods.
        if (nonChainableMethodName) {
          throw Error('Invalid Chai property: ' + nonChainableMethodName + '.' +
            property + '. See docs for proper usage of "' +
            nonChainableMethodName + '".');
        }

        // If the property is reasonably close to an existing Chai property,
        // suggest that property to the user. Only suggest properties with a
        // distance less than 4.
        var suggestion = null;
        var suggestionDistance = 4;
        getProperties(target).forEach(function(prop) {
          if (
            !Object.prototype.hasOwnProperty(prop) &&
            builtins.indexOf(prop) === -1
          ) {
            var dist = stringDistanceCapped(
              property,
              prop,
              suggestionDistance
            );
            if (dist < suggestionDistance) {
              suggestion = prop;
              suggestionDistance = dist;
            }
          }
        });

        if (suggestion !== null) {
          throw Error('Invalid Chai property: ' + property +
            '. Did you mean "' + suggestion + '"?');
        } else {
          throw Error('Invalid Chai property: ' + property);
        }
      }

      // Use this proxy getter as the starting point for removing implementation
      // frames from the stack trace of a failed assertion. For property
      // assertions, this prevents the proxy getter from showing up in the stack
      // trace since it's invoked before the property getter. For method and
      // chainable method assertions, this flag will end up getting changed to
      // the method wrapper, which is good since this frame will no longer be in
      // the stack once the method is invoked. Note that Chai builtin assertion
      // properties such as `__flags` are skipped since this is only meant to
      // capture the starting point of an assertion. This step is also skipped
      // if the `lockSsfi` flag is set, thus indicating that this assertion is
      // being called from within another assertion. In that case, the `ssfi`
      // flag is already set to the outer assertion's starting point.
      if (builtins.indexOf(property) === -1 && !flag(target, 'lockSsfi')) {
        flag(target, 'ssfi', proxyGetter);
      }

      return Reflect.get(target, property);
    }
  });
};

/**
 * # stringDistanceCapped(strA, strB, cap)
 * Return the Levenshtein distance between two strings, but no more than cap.
 * @param {string} strA
 * @param {string} strB
 * @param {number} number
 * @return {number} min(string distance between strA and strB, cap)
 * @api private
 */

function stringDistanceCapped(strA, strB, cap) {
  if (Math.abs(strA.length - strB.length) >= cap) {
    return cap;
  }

  var memo = [];
  // `memo` is a two-dimensional array containing distances.
  // memo[i][j] is the distance between strA.slice(0, i) and
  // strB.slice(0, j).
  for (var i = 0; i <= strA.length; i++) {
    memo[i] = Array(strB.length + 1).fill(0);
    memo[i][0] = i;
  }
  for (var j = 0; j < strB.length; j++) {
    memo[0][j] = j;
  }

  for (var i = 1; i <= strA.length; i++) {
    var ch = strA.charCodeAt(i - 1);
    for (var j = 1; j <= strB.length; j++) {
      if (Math.abs(i - j) >= cap) {
        memo[i][j] = cap;
        continue;
      }
      memo[i][j] = Math.min(
        memo[i - 1][j] + 1,
        memo[i][j - 1] + 1,
        memo[i - 1][j - 1] +
          (ch === strB.charCodeAt(j - 1) ? 0 : 1)
      );
    }
  }

  return memo[strA.length][strB.length];
}


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/test.js":
/*!**************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/test.js ***!
  \**************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*!
 * Chai - test utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/*!
 * Module dependencies
 */

var flag = __webpack_require__(/*! ./flag */ "./node_modules/chai/lib/chai/utils/flag.js");

/**
 * ### .test(object, expression)
 *
 * Test and object for expression.
 *
 * @param {Object} object (constructed Assertion)
 * @param {Arguments} chai.Assertion.prototype.assert arguments
 * @namespace Utils
 * @name test
 */

module.exports = function test(obj, args) {
  var negate = flag(obj, 'negate')
    , expr = args[0];
  return negate ? !expr : expr;
};


/***/ }),

/***/ "./node_modules/chai/lib/chai/utils/transferFlags.js":
/*!***********************************************************!*\
  !*** ./node_modules/chai/lib/chai/utils/transferFlags.js ***!
  \***********************************************************/
/***/ ((module) => {

/*!
 * Chai - transferFlags utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .transferFlags(assertion, object, includeAll = true)
 *
 * Transfer all the flags for `assertion` to `object`. If
 * `includeAll` is set to `false`, then the base Chai
 * assertion flags (namely `object`, `ssfi`, `lockSsfi`,
 * and `message`) will not be transferred.
 *
 *
 *     var newAssertion = new Assertion();
 *     utils.transferFlags(assertion, newAssertion);
 *
 *     var anotherAssertion = new Assertion(myObj);
 *     utils.transferFlags(assertion, anotherAssertion, false);
 *
 * @param {Assertion} assertion the assertion to transfer the flags from
 * @param {Object} object the object to transfer the flags to; usually a new assertion
 * @param {Boolean} includeAll
 * @namespace Utils
 * @name transferFlags
 * @api private
 */

module.exports = function transferFlags(assertion, object, includeAll) {
  var flags = assertion.__flags || (assertion.__flags = Object.create(null));

  if (!object.__flags) {
    object.__flags = Object.create(null);
  }

  includeAll = arguments.length === 3 ? includeAll : true;

  for (var flag in flags) {
    if (includeAll ||
        (flag !== 'object' && flag !== 'ssfi' && flag !== 'lockSsfi' && flag != 'message')) {
      object.__flags[flag] = flags[flag];
    }
  }
};


/***/ }),

/***/ "./node_modules/check-error/index.js":
/*!*******************************************!*\
  !*** ./node_modules/check-error/index.js ***!
  \*******************************************/
/***/ ((module) => {

"use strict";


/* !
 * Chai - checkError utility
 * Copyright(c) 2012-2016 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .checkError
 *
 * Checks that an error conforms to a given set of criteria and/or retrieves information about it.
 *
 * @api public
 */

/**
 * ### .compatibleInstance(thrown, errorLike)
 *
 * Checks if two instances are compatible (strict equal).
 * Returns false if errorLike is not an instance of Error, because instances
 * can only be compatible if they're both error instances.
 *
 * @name compatibleInstance
 * @param {Error} thrown error
 * @param {Error|ErrorConstructor} errorLike object to compare against
 * @namespace Utils
 * @api public
 */

function compatibleInstance(thrown, errorLike) {
  return errorLike instanceof Error && thrown === errorLike;
}

/**
 * ### .compatibleConstructor(thrown, errorLike)
 *
 * Checks if two constructors are compatible.
 * This function can receive either an error constructor or
 * an error instance as the `errorLike` argument.
 * Constructors are compatible if they're the same or if one is
 * an instance of another.
 *
 * @name compatibleConstructor
 * @param {Error} thrown error
 * @param {Error|ErrorConstructor} errorLike object to compare against
 * @namespace Utils
 * @api public
 */

function compatibleConstructor(thrown, errorLike) {
  if (errorLike instanceof Error) {
    // If `errorLike` is an instance of any error we compare their constructors
    return thrown.constructor === errorLike.constructor || thrown instanceof errorLike.constructor;
  } else if (errorLike.prototype instanceof Error || errorLike === Error) {
    // If `errorLike` is a constructor that inherits from Error, we compare `thrown` to `errorLike` directly
    return thrown.constructor === errorLike || thrown instanceof errorLike;
  }

  return false;
}

/**
 * ### .compatibleMessage(thrown, errMatcher)
 *
 * Checks if an error's message is compatible with a matcher (String or RegExp).
 * If the message contains the String or passes the RegExp test,
 * it is considered compatible.
 *
 * @name compatibleMessage
 * @param {Error} thrown error
 * @param {String|RegExp} errMatcher to look for into the message
 * @namespace Utils
 * @api public
 */

function compatibleMessage(thrown, errMatcher) {
  var comparisonString = typeof thrown === 'string' ? thrown : thrown.message;
  if (errMatcher instanceof RegExp) {
    return errMatcher.test(comparisonString);
  } else if (typeof errMatcher === 'string') {
    return comparisonString.indexOf(errMatcher) !== -1; // eslint-disable-line no-magic-numbers
  }

  return false;
}

/**
 * ### .getFunctionName(constructorFn)
 *
 * Returns the name of a function.
 * This also includes a polyfill function if `constructorFn.name` is not defined.
 *
 * @name getFunctionName
 * @param {Function} constructorFn
 * @namespace Utils
 * @api private
 */

var functionNameMatch = /\s*function(?:\s|\s*\/\*[^(?:*\/)]+\*\/\s*)*([^\(\/]+)/;
function getFunctionName(constructorFn) {
  var name = '';
  if (typeof constructorFn.name === 'undefined') {
    // Here we run a polyfill if constructorFn.name is not defined
    var match = String(constructorFn).match(functionNameMatch);
    if (match) {
      name = match[1];
    }
  } else {
    name = constructorFn.name;
  }

  return name;
}

/**
 * ### .getConstructorName(errorLike)
 *
 * Gets the constructor name for an Error instance or constructor itself.
 *
 * @name getConstructorName
 * @param {Error|ErrorConstructor} errorLike
 * @namespace Utils
 * @api public
 */

function getConstructorName(errorLike) {
  var constructorName = errorLike;
  if (errorLike instanceof Error) {
    constructorName = getFunctionName(errorLike.constructor);
  } else if (typeof errorLike === 'function') {
    // If `err` is not an instance of Error it is an error constructor itself or another function.
    // If we've got a common function we get its name, otherwise we may need to create a new instance
    // of the error just in case it's a poorly-constructed error. Please see chaijs/chai/issues/45 to know more.
    constructorName = getFunctionName(errorLike).trim() ||
        getFunctionName(new errorLike()); // eslint-disable-line new-cap
  }

  return constructorName;
}

/**
 * ### .getMessage(errorLike)
 *
 * Gets the error message from an error.
 * If `err` is a String itself, we return it.
 * If the error has no message, we return an empty string.
 *
 * @name getMessage
 * @param {Error|String} errorLike
 * @namespace Utils
 * @api public
 */

function getMessage(errorLike) {
  var msg = '';
  if (errorLike && errorLike.message) {
    msg = errorLike.message;
  } else if (typeof errorLike === 'string') {
    msg = errorLike;
  }

  return msg;
}

module.exports = {
  compatibleInstance: compatibleInstance,
  compatibleConstructor: compatibleConstructor,
  compatibleMessage: compatibleMessage,
  getMessage: getMessage,
  getConstructorName: getConstructorName,
};


/***/ }),

/***/ "./node_modules/deep-eql/index.js":
/*!****************************************!*\
  !*** ./node_modules/deep-eql/index.js ***!
  \****************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

"use strict";

/* globals Symbol: false, Uint8Array: false, WeakMap: false */
/*!
 * deep-eql
 * Copyright(c) 2013 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

var type = __webpack_require__(/*! type-detect */ "./node_modules/type-detect/type-detect.js");
function FakeMap() {
  this._key = 'chai/deep-eql__' + Math.random() + Date.now();
}

FakeMap.prototype = {
  get: function getMap(key) {
    return key[this._key];
  },
  set: function setMap(key, value) {
    if (Object.isExtensible(key)) {
      Object.defineProperty(key, this._key, {
        value: value,
        configurable: true,
      });
    }
  },
};

var MemoizeMap = typeof WeakMap === 'function' ? WeakMap : FakeMap;
/*!
 * Check to see if the MemoizeMap has recorded a result of the two operands
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {MemoizeMap} memoizeMap
 * @returns {Boolean|null} result
*/
function memoizeCompare(leftHandOperand, rightHandOperand, memoizeMap) {
  // Technically, WeakMap keys can *only* be objects, not primitives.
  if (!memoizeMap || isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    return null;
  }
  var leftHandMap = memoizeMap.get(leftHandOperand);
  if (leftHandMap) {
    var result = leftHandMap.get(rightHandOperand);
    if (typeof result === 'boolean') {
      return result;
    }
  }
  return null;
}

/*!
 * Set the result of the equality into the MemoizeMap
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {MemoizeMap} memoizeMap
 * @param {Boolean} result
*/
function memoizeSet(leftHandOperand, rightHandOperand, memoizeMap, result) {
  // Technically, WeakMap keys can *only* be objects, not primitives.
  if (!memoizeMap || isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    return;
  }
  var leftHandMap = memoizeMap.get(leftHandOperand);
  if (leftHandMap) {
    leftHandMap.set(rightHandOperand, result);
  } else {
    leftHandMap = new MemoizeMap();
    leftHandMap.set(rightHandOperand, result);
    memoizeMap.set(leftHandOperand, leftHandMap);
  }
}

/*!
 * Primary Export
 */

module.exports = deepEqual;
module.exports.MemoizeMap = MemoizeMap;

/**
 * Assert deeply nested sameValue equality between two objects of any type.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (optional) Additional options
 * @param {Array} [options.comparator] (optional) Override default algorithm, determining custom equality.
 * @param {Array} [options.memoize] (optional) Provide a custom memoization object which will cache the results of
    complex objects for a speed boost. By passing `false` you can disable memoization, but this will cause circular
    references to blow the stack.
 * @return {Boolean} equal match
 */
function deepEqual(leftHandOperand, rightHandOperand, options) {
  // If we have a comparator, we can't assume anything; so bail to its check first.
  if (options && options.comparator) {
    return extensiveDeepEqual(leftHandOperand, rightHandOperand, options);
  }

  var simpleResult = simpleEqual(leftHandOperand, rightHandOperand);
  if (simpleResult !== null) {
    return simpleResult;
  }

  // Deeper comparisons are pushed through to a larger function
  return extensiveDeepEqual(leftHandOperand, rightHandOperand, options);
}

/**
 * Many comparisons can be canceled out early via simple equality or primitive checks.
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @return {Boolean|null} equal match
 */
function simpleEqual(leftHandOperand, rightHandOperand) {
  // Equal references (except for Numbers) can be returned early
  if (leftHandOperand === rightHandOperand) {
    // Handle +-0 cases
    return leftHandOperand !== 0 || 1 / leftHandOperand === 1 / rightHandOperand;
  }

  // handle NaN cases
  if (
    leftHandOperand !== leftHandOperand && // eslint-disable-line no-self-compare
    rightHandOperand !== rightHandOperand // eslint-disable-line no-self-compare
  ) {
    return true;
  }

  // Anything that is not an 'object', i.e. symbols, functions, booleans, numbers,
  // strings, and undefined, can be compared by reference.
  if (isPrimitive(leftHandOperand) || isPrimitive(rightHandOperand)) {
    // Easy out b/c it would have passed the first equality check
    return false;
  }
  return null;
}

/*!
 * The main logic of the `deepEqual` function.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (optional) Additional options
 * @param {Array} [options.comparator] (optional) Override default algorithm, determining custom equality.
 * @param {Array} [options.memoize] (optional) Provide a custom memoization object which will cache the results of
    complex objects for a speed boost. By passing `false` you can disable memoization, but this will cause circular
    references to blow the stack.
 * @return {Boolean} equal match
*/
function extensiveDeepEqual(leftHandOperand, rightHandOperand, options) {
  options = options || {};
  options.memoize = options.memoize === false ? false : options.memoize || new MemoizeMap();
  var comparator = options && options.comparator;

  // Check if a memoized result exists.
  var memoizeResultLeft = memoizeCompare(leftHandOperand, rightHandOperand, options.memoize);
  if (memoizeResultLeft !== null) {
    return memoizeResultLeft;
  }
  var memoizeResultRight = memoizeCompare(rightHandOperand, leftHandOperand, options.memoize);
  if (memoizeResultRight !== null) {
    return memoizeResultRight;
  }

  // If a comparator is present, use it.
  if (comparator) {
    var comparatorResult = comparator(leftHandOperand, rightHandOperand);
    // Comparators may return null, in which case we want to go back to default behavior.
    if (comparatorResult === false || comparatorResult === true) {
      memoizeSet(leftHandOperand, rightHandOperand, options.memoize, comparatorResult);
      return comparatorResult;
    }
    // To allow comparators to override *any* behavior, we ran them first. Since it didn't decide
    // what to do, we need to make sure to return the basic tests first before we move on.
    var simpleResult = simpleEqual(leftHandOperand, rightHandOperand);
    if (simpleResult !== null) {
      // Don't memoize this, it takes longer to set/retrieve than to just compare.
      return simpleResult;
    }
  }

  var leftHandType = type(leftHandOperand);
  if (leftHandType !== type(rightHandOperand)) {
    memoizeSet(leftHandOperand, rightHandOperand, options.memoize, false);
    return false;
  }

  // Temporarily set the operands in the memoize object to prevent blowing the stack
  memoizeSet(leftHandOperand, rightHandOperand, options.memoize, true);

  var result = extensiveDeepEqualByType(leftHandOperand, rightHandOperand, leftHandType, options);
  memoizeSet(leftHandOperand, rightHandOperand, options.memoize, result);
  return result;
}

function extensiveDeepEqualByType(leftHandOperand, rightHandOperand, leftHandType, options) {
  switch (leftHandType) {
    case 'String':
    case 'Number':
    case 'Boolean':
    case 'Date':
      // If these types are their instance types (e.g. `new Number`) then re-deepEqual against their values
      return deepEqual(leftHandOperand.valueOf(), rightHandOperand.valueOf());
    case 'Promise':
    case 'Symbol':
    case 'function':
    case 'WeakMap':
    case 'WeakSet':
    case 'Error':
      return leftHandOperand === rightHandOperand;
    case 'Arguments':
    case 'Int8Array':
    case 'Uint8Array':
    case 'Uint8ClampedArray':
    case 'Int16Array':
    case 'Uint16Array':
    case 'Int32Array':
    case 'Uint32Array':
    case 'Float32Array':
    case 'Float64Array':
    case 'Array':
      return iterableEqual(leftHandOperand, rightHandOperand, options);
    case 'RegExp':
      return regexpEqual(leftHandOperand, rightHandOperand);
    case 'Generator':
      return generatorEqual(leftHandOperand, rightHandOperand, options);
    case 'DataView':
      return iterableEqual(new Uint8Array(leftHandOperand.buffer), new Uint8Array(rightHandOperand.buffer), options);
    case 'ArrayBuffer':
      return iterableEqual(new Uint8Array(leftHandOperand), new Uint8Array(rightHandOperand), options);
    case 'Set':
      return entriesEqual(leftHandOperand, rightHandOperand, options);
    case 'Map':
      return entriesEqual(leftHandOperand, rightHandOperand, options);
    default:
      return objectEqual(leftHandOperand, rightHandOperand, options);
  }
}

/*!
 * Compare two Regular Expressions for equality.
 *
 * @param {RegExp} leftHandOperand
 * @param {RegExp} rightHandOperand
 * @return {Boolean} result
 */

function regexpEqual(leftHandOperand, rightHandOperand) {
  return leftHandOperand.toString() === rightHandOperand.toString();
}

/*!
 * Compare two Sets/Maps for equality. Faster than other equality functions.
 *
 * @param {Set} leftHandOperand
 * @param {Set} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function entriesEqual(leftHandOperand, rightHandOperand, options) {
  // IE11 doesn't support Set#entries or Set#@@iterator, so we need manually populate using Set#forEach
  if (leftHandOperand.size !== rightHandOperand.size) {
    return false;
  }
  if (leftHandOperand.size === 0) {
    return true;
  }
  var leftHandItems = [];
  var rightHandItems = [];
  leftHandOperand.forEach(function gatherEntries(key, value) {
    leftHandItems.push([ key, value ]);
  });
  rightHandOperand.forEach(function gatherEntries(key, value) {
    rightHandItems.push([ key, value ]);
  });
  return iterableEqual(leftHandItems.sort(), rightHandItems.sort(), options);
}

/*!
 * Simple equality for flat iterable objects such as Arrays, TypedArrays or Node.js buffers.
 *
 * @param {Iterable} leftHandOperand
 * @param {Iterable} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function iterableEqual(leftHandOperand, rightHandOperand, options) {
  var length = leftHandOperand.length;
  if (length !== rightHandOperand.length) {
    return false;
  }
  if (length === 0) {
    return true;
  }
  var index = -1;
  while (++index < length) {
    if (deepEqual(leftHandOperand[index], rightHandOperand[index], options) === false) {
      return false;
    }
  }
  return true;
}

/*!
 * Simple equality for generator objects such as those returned by generator functions.
 *
 * @param {Iterable} leftHandOperand
 * @param {Iterable} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function generatorEqual(leftHandOperand, rightHandOperand, options) {
  return iterableEqual(getGeneratorEntries(leftHandOperand), getGeneratorEntries(rightHandOperand), options);
}

/*!
 * Determine if the given object has an @@iterator function.
 *
 * @param {Object} target
 * @return {Boolean} `true` if the object has an @@iterator function.
 */
function hasIteratorFunction(target) {
  return typeof Symbol !== 'undefined' &&
    typeof target === 'object' &&
    typeof Symbol.iterator !== 'undefined' &&
    typeof target[Symbol.iterator] === 'function';
}

/*!
 * Gets all iterator entries from the given Object. If the Object has no @@iterator function, returns an empty array.
 * This will consume the iterator - which could have side effects depending on the @@iterator implementation.
 *
 * @param {Object} target
 * @returns {Array} an array of entries from the @@iterator function
 */
function getIteratorEntries(target) {
  if (hasIteratorFunction(target)) {
    try {
      return getGeneratorEntries(target[Symbol.iterator]());
    } catch (iteratorError) {
      return [];
    }
  }
  return [];
}

/*!
 * Gets all entries from a Generator. This will consume the generator - which could have side effects.
 *
 * @param {Generator} target
 * @returns {Array} an array of entries from the Generator.
 */
function getGeneratorEntries(generator) {
  var generatorResult = generator.next();
  var accumulator = [ generatorResult.value ];
  while (generatorResult.done === false) {
    generatorResult = generator.next();
    accumulator.push(generatorResult.value);
  }
  return accumulator;
}

/*!
 * Gets all own and inherited enumerable keys from a target.
 *
 * @param {Object} target
 * @returns {Array} an array of own and inherited enumerable keys from the target.
 */
function getEnumerableKeys(target) {
  var keys = [];
  for (var key in target) {
    keys.push(key);
  }
  return keys;
}

/*!
 * Determines if two objects have matching values, given a set of keys. Defers to deepEqual for the equality check of
 * each key. If any value of the given key is not equal, the function will return false (early).
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Array} keys An array of keys to compare the values of leftHandOperand and rightHandOperand against
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */
function keysEqual(leftHandOperand, rightHandOperand, keys, options) {
  var length = keys.length;
  if (length === 0) {
    return true;
  }
  for (var i = 0; i < length; i += 1) {
    if (deepEqual(leftHandOperand[keys[i]], rightHandOperand[keys[i]], options) === false) {
      return false;
    }
  }
  return true;
}

/*!
 * Recursively check the equality of two Objects. Once basic sameness has been established it will defer to `deepEqual`
 * for each enumerable key in the object.
 *
 * @param {Mixed} leftHandOperand
 * @param {Mixed} rightHandOperand
 * @param {Object} [options] (Optional)
 * @return {Boolean} result
 */

function objectEqual(leftHandOperand, rightHandOperand, options) {
  var leftHandKeys = getEnumerableKeys(leftHandOperand);
  var rightHandKeys = getEnumerableKeys(rightHandOperand);
  if (leftHandKeys.length && leftHandKeys.length === rightHandKeys.length) {
    leftHandKeys.sort();
    rightHandKeys.sort();
    if (iterableEqual(leftHandKeys, rightHandKeys) === false) {
      return false;
    }
    return keysEqual(leftHandOperand, rightHandOperand, leftHandKeys, options);
  }

  var leftHandEntries = getIteratorEntries(leftHandOperand);
  var rightHandEntries = getIteratorEntries(rightHandOperand);
  if (leftHandEntries.length && leftHandEntries.length === rightHandEntries.length) {
    leftHandEntries.sort();
    rightHandEntries.sort();
    return iterableEqual(leftHandEntries, rightHandEntries, options);
  }

  if (leftHandKeys.length === 0 &&
      leftHandEntries.length === 0 &&
      rightHandKeys.length === 0 &&
      rightHandEntries.length === 0) {
    return true;
  }

  return false;
}

/*!
 * Returns true if the argument is a primitive.
 *
 * This intentionally returns true for all objects that can be compared by reference,
 * including functions and symbols.
 *
 * @param {Mixed} value
 * @return {Boolean} result
 */
function isPrimitive(value) {
  return value === null || typeof value !== 'object';
}


/***/ }),

/***/ "./node_modules/get-func-name/index.js":
/*!*********************************************!*\
  !*** ./node_modules/get-func-name/index.js ***!
  \*********************************************/
/***/ ((module) => {

"use strict";


/* !
 * Chai - getFuncName utility
 * Copyright(c) 2012-2016 Jake Luer <jake@alogicalparadox.com>
 * MIT Licensed
 */

/**
 * ### .getFuncName(constructorFn)
 *
 * Returns the name of a function.
 * When a non-function instance is passed, returns `null`.
 * This also includes a polyfill function if `aFunc.name` is not defined.
 *
 * @name getFuncName
 * @param {Function} funct
 * @namespace Utils
 * @api public
 */

var toString = Function.prototype.toString;
var functionNameMatch = /\s*function(?:\s|\s*\/\*[^(?:*\/)]+\*\/\s*)*([^\s\(\/]+)/;
function getFuncName(aFunc) {
  if (typeof aFunc !== 'function') {
    return null;
  }

  var name = '';
  if (typeof Function.prototype.name === 'undefined' && typeof aFunc.name === 'undefined') {
    // Here we run a polyfill if Function does not support the `name` property and if aFunc.name is not defined
    var match = toString.call(aFunc).match(functionNameMatch);
    if (match) {
      name = match[1];
    }
  } else {
    // If we've got a `name` property we just use it
    name = aFunc.name;
  }

  return name;
}

module.exports = getFuncName;


/***/ }),

/***/ "./node_modules/pathval/index.js":
/*!***************************************!*\
  !*** ./node_modules/pathval/index.js ***!
  \***************************************/
/***/ ((module) => {

"use strict";


/* !
 * Chai - pathval utility
 * Copyright(c) 2012-2014 Jake Luer <jake@alogicalparadox.com>
 * @see https://github.com/logicalparadox/filtr
 * MIT Licensed
 */

/**
 * ### .hasProperty(object, name)
 *
 * This allows checking whether an object has own
 * or inherited from prototype chain named property.
 *
 * Basically does the same thing as the `in`
 * operator but works properly with null/undefined values
 * and other primitives.
 *
 *     var obj = {
 *         arr: ['a', 'b', 'c']
 *       , str: 'Hello'
 *     }
 *
 * The following would be the results.
 *
 *     hasProperty(obj, 'str');  // true
 *     hasProperty(obj, 'constructor');  // true
 *     hasProperty(obj, 'bar');  // false
 *
 *     hasProperty(obj.str, 'length'); // true
 *     hasProperty(obj.str, 1);  // true
 *     hasProperty(obj.str, 5);  // false
 *
 *     hasProperty(obj.arr, 'length');  // true
 *     hasProperty(obj.arr, 2);  // true
 *     hasProperty(obj.arr, 3);  // false
 *
 * @param {Object} object
 * @param {String|Symbol} name
 * @returns {Boolean} whether it exists
 * @namespace Utils
 * @name hasProperty
 * @api public
 */

function hasProperty(obj, name) {
  if (typeof obj === 'undefined' || obj === null) {
    return false;
  }

  // The `in` operator does not work with primitives.
  return name in Object(obj);
}

/* !
 * ## parsePath(path)
 *
 * Helper function used to parse string object
 * paths. Use in conjunction with `internalGetPathValue`.
 *
 *      var parsed = parsePath('myobject.property.subprop');
 *
 * ### Paths:
 *
 * * Can be infinitely deep and nested.
 * * Arrays are also valid using the formal `myobject.document[3].property`.
 * * Literal dots and brackets (not delimiter) must be backslash-escaped.
 *
 * @param {String} path
 * @returns {Object} parsed
 * @api private
 */

function parsePath(path) {
  var str = path.replace(/([^\\])\[/g, '$1.[');
  var parts = str.match(/(\\\.|[^.]+?)+/g);
  return parts.map(function mapMatches(value) {
    if (
      value === 'constructor' ||
      value === '__proto__' ||
      value === 'prototype'
    ) {
      return {};
    }
    var regexp = /^\[(\d+)\]$/;
    var mArr = regexp.exec(value);
    var parsed = null;
    if (mArr) {
      parsed = { i: parseFloat(mArr[1]) };
    } else {
      parsed = { p: value.replace(/\\([.[\]])/g, '$1') };
    }

    return parsed;
  });
}

/* !
 * ## internalGetPathValue(obj, parsed[, pathDepth])
 *
 * Helper companion function for `.parsePath` that returns
 * the value located at the parsed address.
 *
 *      var value = getPathValue(obj, parsed);
 *
 * @param {Object} object to search against
 * @param {Object} parsed definition from `parsePath`.
 * @param {Number} depth (nesting level) of the property we want to retrieve
 * @returns {Object|Undefined} value
 * @api private
 */

function internalGetPathValue(obj, parsed, pathDepth) {
  var temporaryValue = obj;
  var res = null;
  pathDepth = typeof pathDepth === 'undefined' ? parsed.length : pathDepth;

  for (var i = 0; i < pathDepth; i++) {
    var part = parsed[i];
    if (temporaryValue) {
      if (typeof part.p === 'undefined') {
        temporaryValue = temporaryValue[part.i];
      } else {
        temporaryValue = temporaryValue[part.p];
      }

      if (i === pathDepth - 1) {
        res = temporaryValue;
      }
    }
  }

  return res;
}

/* !
 * ## internalSetPathValue(obj, value, parsed)
 *
 * Companion function for `parsePath` that sets
 * the value located at a parsed address.
 *
 *  internalSetPathValue(obj, 'value', parsed);
 *
 * @param {Object} object to search and define on
 * @param {*} value to use upon set
 * @param {Object} parsed definition from `parsePath`
 * @api private
 */

function internalSetPathValue(obj, val, parsed) {
  var tempObj = obj;
  var pathDepth = parsed.length;
  var part = null;
  // Here we iterate through every part of the path
  for (var i = 0; i < pathDepth; i++) {
    var propName = null;
    var propVal = null;
    part = parsed[i];

    // If it's the last part of the path, we set the 'propName' value with the property name
    if (i === pathDepth - 1) {
      propName = typeof part.p === 'undefined' ? part.i : part.p;
      // Now we set the property with the name held by 'propName' on object with the desired val
      tempObj[propName] = val;
    } else if (typeof part.p !== 'undefined' && tempObj[part.p]) {
      tempObj = tempObj[part.p];
    } else if (typeof part.i !== 'undefined' && tempObj[part.i]) {
      tempObj = tempObj[part.i];
    } else {
      // If the obj doesn't have the property we create one with that name to define it
      var next = parsed[i + 1];
      // Here we set the name of the property which will be defined
      propName = typeof part.p === 'undefined' ? part.i : part.p;
      // Here we decide if this property will be an array or a new object
      propVal = typeof next.p === 'undefined' ? [] : {};
      tempObj[propName] = propVal;
      tempObj = tempObj[propName];
    }
  }
}

/**
 * ### .getPathInfo(object, path)
 *
 * This allows the retrieval of property info in an
 * object given a string path.
 *
 * The path info consists of an object with the
 * following properties:
 *
 * * parent - The parent object of the property referenced by `path`
 * * name - The name of the final property, a number if it was an array indexer
 * * value - The value of the property, if it exists, otherwise `undefined`
 * * exists - Whether the property exists or not
 *
 * @param {Object} object
 * @param {String} path
 * @returns {Object} info
 * @namespace Utils
 * @name getPathInfo
 * @api public
 */

function getPathInfo(obj, path) {
  var parsed = parsePath(path);
  var last = parsed[parsed.length - 1];
  var info = {
    parent:
      parsed.length > 1 ?
        internalGetPathValue(obj, parsed, parsed.length - 1) :
        obj,
    name: last.p || last.i,
    value: internalGetPathValue(obj, parsed),
  };
  info.exists = hasProperty(info.parent, info.name);

  return info;
}

/**
 * ### .getPathValue(object, path)
 *
 * This allows the retrieval of values in an
 * object given a string path.
 *
 *     var obj = {
 *         prop1: {
 *             arr: ['a', 'b', 'c']
 *           , str: 'Hello'
 *         }
 *       , prop2: {
 *             arr: [ { nested: 'Universe' } ]
 *           , str: 'Hello again!'
 *         }
 *     }
 *
 * The following would be the results.
 *
 *     getPathValue(obj, 'prop1.str'); // Hello
 *     getPathValue(obj, 'prop1.att[2]'); // b
 *     getPathValue(obj, 'prop2.arr[0].nested'); // Universe
 *
 * @param {Object} object
 * @param {String} path
 * @returns {Object} value or `undefined`
 * @namespace Utils
 * @name getPathValue
 * @api public
 */

function getPathValue(obj, path) {
  var info = getPathInfo(obj, path);
  return info.value;
}

/**
 * ### .setPathValue(object, path, value)
 *
 * Define the value in an object at a given string path.
 *
 * ```js
 * var obj = {
 *     prop1: {
 *         arr: ['a', 'b', 'c']
 *       , str: 'Hello'
 *     }
 *   , prop2: {
 *         arr: [ { nested: 'Universe' } ]
 *       , str: 'Hello again!'
 *     }
 * };
 * ```
 *
 * The following would be acceptable.
 *
 * ```js
 * var properties = require('tea-properties');
 * properties.set(obj, 'prop1.str', 'Hello Universe!');
 * properties.set(obj, 'prop1.arr[2]', 'B');
 * properties.set(obj, 'prop2.arr[0].nested.value', { hello: 'universe' });
 * ```
 *
 * @param {Object} object
 * @param {String} path
 * @param {Mixed} value
 * @api private
 */

function setPathValue(obj, path, val) {
  var parsed = parsePath(path);
  internalSetPathValue(obj, val, parsed);
  return obj;
}

module.exports = {
  hasProperty: hasProperty,
  getPathInfo: getPathInfo,
  getPathValue: getPathValue,
  setPathValue: setPathValue,
};


/***/ }),

/***/ "./node_modules/type-detect/type-detect.js":
/*!*************************************************!*\
  !*** ./node_modules/type-detect/type-detect.js ***!
  \*************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

(function (global, factory) {
	 true ? module.exports = factory() :
	0;
}(this, (function () { 'use strict';

/* !
 * type-detect
 * Copyright(c) 2013 jake luer <jake@alogicalparadox.com>
 * MIT Licensed
 */
var promiseExists = typeof Promise === 'function';

/* eslint-disable no-undef */
var globalObject = typeof self === 'object' ? self : __webpack_require__.g; // eslint-disable-line id-blacklist

var symbolExists = typeof Symbol !== 'undefined';
var mapExists = typeof Map !== 'undefined';
var setExists = typeof Set !== 'undefined';
var weakMapExists = typeof WeakMap !== 'undefined';
var weakSetExists = typeof WeakSet !== 'undefined';
var dataViewExists = typeof DataView !== 'undefined';
var symbolIteratorExists = symbolExists && typeof Symbol.iterator !== 'undefined';
var symbolToStringTagExists = symbolExists && typeof Symbol.toStringTag !== 'undefined';
var setEntriesExists = setExists && typeof Set.prototype.entries === 'function';
var mapEntriesExists = mapExists && typeof Map.prototype.entries === 'function';
var setIteratorPrototype = setEntriesExists && Object.getPrototypeOf(new Set().entries());
var mapIteratorPrototype = mapEntriesExists && Object.getPrototypeOf(new Map().entries());
var arrayIteratorExists = symbolIteratorExists && typeof Array.prototype[Symbol.iterator] === 'function';
var arrayIteratorPrototype = arrayIteratorExists && Object.getPrototypeOf([][Symbol.iterator]());
var stringIteratorExists = symbolIteratorExists && typeof String.prototype[Symbol.iterator] === 'function';
var stringIteratorPrototype = stringIteratorExists && Object.getPrototypeOf(''[Symbol.iterator]());
var toStringLeftSliceLength = 8;
var toStringRightSliceLength = -1;
/**
 * ### typeOf (obj)
 *
 * Uses `Object.prototype.toString` to determine the type of an object,
 * normalising behaviour across engine versions & well optimised.
 *
 * @param {Mixed} object
 * @return {String} object type
 * @api public
 */
function typeDetect(obj) {
  /* ! Speed optimisation
   * Pre:
   *   string literal     x 3,039,035 ops/sec ±1.62% (78 runs sampled)
   *   boolean literal    x 1,424,138 ops/sec ±4.54% (75 runs sampled)
   *   number literal     x 1,653,153 ops/sec ±1.91% (82 runs sampled)
   *   undefined          x 9,978,660 ops/sec ±1.92% (75 runs sampled)
   *   function           x 2,556,769 ops/sec ±1.73% (77 runs sampled)
   * Post:
   *   string literal     x 38,564,796 ops/sec ±1.15% (79 runs sampled)
   *   boolean literal    x 31,148,940 ops/sec ±1.10% (79 runs sampled)
   *   number literal     x 32,679,330 ops/sec ±1.90% (78 runs sampled)
   *   undefined          x 32,363,368 ops/sec ±1.07% (82 runs sampled)
   *   function           x 31,296,870 ops/sec ±0.96% (83 runs sampled)
   */
  var typeofObj = typeof obj;
  if (typeofObj !== 'object') {
    return typeofObj;
  }

  /* ! Speed optimisation
   * Pre:
   *   null               x 28,645,765 ops/sec ±1.17% (82 runs sampled)
   * Post:
   *   null               x 36,428,962 ops/sec ±1.37% (84 runs sampled)
   */
  if (obj === null) {
    return 'null';
  }

  /* ! Spec Conformance
   * Test: `Object.prototype.toString.call(window)``
   *  - Node === "[object global]"
   *  - Chrome === "[object global]"
   *  - Firefox === "[object Window]"
   *  - PhantomJS === "[object Window]"
   *  - Safari === "[object Window]"
   *  - IE 11 === "[object Window]"
   *  - IE Edge === "[object Window]"
   * Test: `Object.prototype.toString.call(this)``
   *  - Chrome Worker === "[object global]"
   *  - Firefox Worker === "[object DedicatedWorkerGlobalScope]"
   *  - Safari Worker === "[object DedicatedWorkerGlobalScope]"
   *  - IE 11 Worker === "[object WorkerGlobalScope]"
   *  - IE Edge Worker === "[object WorkerGlobalScope]"
   */
  if (obj === globalObject) {
    return 'global';
  }

  /* ! Speed optimisation
   * Pre:
   *   array literal      x 2,888,352 ops/sec ±0.67% (82 runs sampled)
   * Post:
   *   array literal      x 22,479,650 ops/sec ±0.96% (81 runs sampled)
   */
  if (
    Array.isArray(obj) &&
    (symbolToStringTagExists === false || !(Symbol.toStringTag in obj))
  ) {
    return 'Array';
  }

  // Not caching existence of `window` and related properties due to potential
  // for `window` to be unset before tests in quasi-browser environments.
  if (typeof window === 'object' && window !== null) {
    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/multipage/browsers.html#location)
     * WhatWG HTML$7.7.3 - The `Location` interface
     * Test: `Object.prototype.toString.call(window.location)``
     *  - IE <=11 === "[object Object]"
     *  - IE Edge <=13 === "[object Object]"
     */
    if (typeof window.location === 'object' && obj === window.location) {
      return 'Location';
    }

    /* ! Spec Conformance
     * (https://html.spec.whatwg.org/#document)
     * WhatWG HTML$3.1.1 - The `Document` object
     * Note: Most browsers currently adher to the W3C DOM Level 2 spec
     *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-26809268)
     *       which suggests that browsers should use HTMLTableCellElement for
     *       both TD and TH elements. WhatWG separates these.
     *       WhatWG HTML states:
     *         > For historical reasons, Window objects must also have a
     *         > writable, configurable, non-enumerable property named
     *         > HTMLDocument whose value is the Document interface object.
     * Test: `Object.prototype.toString.call(document)``
     *  - Chrome === "[object HTMLDocument]"
     *  - Firefox === "[object HTMLDocument]"
     *  - Safari === "[object HTMLDocument]"
     *  - IE <=10 === "[object Document]"
     *  - IE 11 === "[object HTMLDocument]"
     *  - IE Edge <=13 === "[object HTMLDocument]"
     */
    if (typeof window.document === 'object' && obj === window.document) {
      return 'Document';
    }

    if (typeof window.navigator === 'object') {
      /* ! Spec Conformance
       * (https://html.spec.whatwg.org/multipage/webappapis.html#mimetypearray)
       * WhatWG HTML$8.6.1.5 - Plugins - Interface MimeTypeArray
       * Test: `Object.prototype.toString.call(navigator.mimeTypes)``
       *  - IE <=10 === "[object MSMimeTypesCollection]"
       */
      if (typeof window.navigator.mimeTypes === 'object' &&
          obj === window.navigator.mimeTypes) {
        return 'MimeTypeArray';
      }

      /* ! Spec Conformance
       * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
       * WhatWG HTML$8.6.1.5 - Plugins - Interface PluginArray
       * Test: `Object.prototype.toString.call(navigator.plugins)``
       *  - IE <=10 === "[object MSPluginsCollection]"
       */
      if (typeof window.navigator.plugins === 'object' &&
          obj === window.navigator.plugins) {
        return 'PluginArray';
      }
    }

    if ((typeof window.HTMLElement === 'function' ||
        typeof window.HTMLElement === 'object') &&
        obj instanceof window.HTMLElement) {
      /* ! Spec Conformance
      * (https://html.spec.whatwg.org/multipage/webappapis.html#pluginarray)
      * WhatWG HTML$4.4.4 - The `blockquote` element - Interface `HTMLQuoteElement`
      * Test: `Object.prototype.toString.call(document.createElement('blockquote'))``
      *  - IE <=10 === "[object HTMLBlockElement]"
      */
      if (obj.tagName === 'BLOCKQUOTE') {
        return 'HTMLQuoteElement';
      }

      /* ! Spec Conformance
       * (https://html.spec.whatwg.org/#htmltabledatacellelement)
       * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableDataCellElement`
       * Note: Most browsers currently adher to the W3C DOM Level 2 spec
       *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
       *       which suggests that browsers should use HTMLTableCellElement for
       *       both TD and TH elements. WhatWG separates these.
       * Test: Object.prototype.toString.call(document.createElement('td'))
       *  - Chrome === "[object HTMLTableCellElement]"
       *  - Firefox === "[object HTMLTableCellElement]"
       *  - Safari === "[object HTMLTableCellElement]"
       */
      if (obj.tagName === 'TD') {
        return 'HTMLTableDataCellElement';
      }

      /* ! Spec Conformance
       * (https://html.spec.whatwg.org/#htmltableheadercellelement)
       * WhatWG HTML$4.9.9 - The `td` element - Interface `HTMLTableHeaderCellElement`
       * Note: Most browsers currently adher to the W3C DOM Level 2 spec
       *       (https://www.w3.org/TR/DOM-Level-2-HTML/html.html#ID-82915075)
       *       which suggests that browsers should use HTMLTableCellElement for
       *       both TD and TH elements. WhatWG separates these.
       * Test: Object.prototype.toString.call(document.createElement('th'))
       *  - Chrome === "[object HTMLTableCellElement]"
       *  - Firefox === "[object HTMLTableCellElement]"
       *  - Safari === "[object HTMLTableCellElement]"
       */
      if (obj.tagName === 'TH') {
        return 'HTMLTableHeaderCellElement';
      }
    }
  }

  /* ! Speed optimisation
  * Pre:
  *   Float64Array       x 625,644 ops/sec ±1.58% (80 runs sampled)
  *   Float32Array       x 1,279,852 ops/sec ±2.91% (77 runs sampled)
  *   Uint32Array        x 1,178,185 ops/sec ±1.95% (83 runs sampled)
  *   Uint16Array        x 1,008,380 ops/sec ±2.25% (80 runs sampled)
  *   Uint8Array         x 1,128,040 ops/sec ±2.11% (81 runs sampled)
  *   Int32Array         x 1,170,119 ops/sec ±2.88% (80 runs sampled)
  *   Int16Array         x 1,176,348 ops/sec ±5.79% (86 runs sampled)
  *   Int8Array          x 1,058,707 ops/sec ±4.94% (77 runs sampled)
  *   Uint8ClampedArray  x 1,110,633 ops/sec ±4.20% (80 runs sampled)
  * Post:
  *   Float64Array       x 7,105,671 ops/sec ±13.47% (64 runs sampled)
  *   Float32Array       x 5,887,912 ops/sec ±1.46% (82 runs sampled)
  *   Uint32Array        x 6,491,661 ops/sec ±1.76% (79 runs sampled)
  *   Uint16Array        x 6,559,795 ops/sec ±1.67% (82 runs sampled)
  *   Uint8Array         x 6,463,966 ops/sec ±1.43% (85 runs sampled)
  *   Int32Array         x 5,641,841 ops/sec ±3.49% (81 runs sampled)
  *   Int16Array         x 6,583,511 ops/sec ±1.98% (80 runs sampled)
  *   Int8Array          x 6,606,078 ops/sec ±1.74% (81 runs sampled)
  *   Uint8ClampedArray  x 6,602,224 ops/sec ±1.77% (83 runs sampled)
  */
  var stringTag = (symbolToStringTagExists && obj[Symbol.toStringTag]);
  if (typeof stringTag === 'string') {
    return stringTag;
  }

  var objPrototype = Object.getPrototypeOf(obj);
  /* ! Speed optimisation
  * Pre:
  *   regex literal      x 1,772,385 ops/sec ±1.85% (77 runs sampled)
  *   regex constructor  x 2,143,634 ops/sec ±2.46% (78 runs sampled)
  * Post:
  *   regex literal      x 3,928,009 ops/sec ±0.65% (78 runs sampled)
  *   regex constructor  x 3,931,108 ops/sec ±0.58% (84 runs sampled)
  */
  if (objPrototype === RegExp.prototype) {
    return 'RegExp';
  }

  /* ! Speed optimisation
  * Pre:
  *   date               x 2,130,074 ops/sec ±4.42% (68 runs sampled)
  * Post:
  *   date               x 3,953,779 ops/sec ±1.35% (77 runs sampled)
  */
  if (objPrototype === Date.prototype) {
    return 'Date';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-promise.prototype-@@tostringtag)
   * ES6$25.4.5.4 - Promise.prototype[@@toStringTag] should be "Promise":
   * Test: `Object.prototype.toString.call(Promise.resolve())``
   *  - Chrome <=47 === "[object Object]"
   *  - Edge <=20 === "[object Object]"
   *  - Firefox 29-Latest === "[object Promise]"
   *  - Safari 7.1-Latest === "[object Promise]"
   */
  if (promiseExists && objPrototype === Promise.prototype) {
    return 'Promise';
  }

  /* ! Speed optimisation
  * Pre:
  *   set                x 2,222,186 ops/sec ±1.31% (82 runs sampled)
  * Post:
  *   set                x 4,545,879 ops/sec ±1.13% (83 runs sampled)
  */
  if (setExists && objPrototype === Set.prototype) {
    return 'Set';
  }

  /* ! Speed optimisation
  * Pre:
  *   map                x 2,396,842 ops/sec ±1.59% (81 runs sampled)
  * Post:
  *   map                x 4,183,945 ops/sec ±6.59% (82 runs sampled)
  */
  if (mapExists && objPrototype === Map.prototype) {
    return 'Map';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakset            x 1,323,220 ops/sec ±2.17% (76 runs sampled)
  * Post:
  *   weakset            x 4,237,510 ops/sec ±2.01% (77 runs sampled)
  */
  if (weakSetExists && objPrototype === WeakSet.prototype) {
    return 'WeakSet';
  }

  /* ! Speed optimisation
  * Pre:
  *   weakmap            x 1,500,260 ops/sec ±2.02% (78 runs sampled)
  * Post:
  *   weakmap            x 3,881,384 ops/sec ±1.45% (82 runs sampled)
  */
  if (weakMapExists && objPrototype === WeakMap.prototype) {
    return 'WeakMap';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-dataview.prototype-@@tostringtag)
   * ES6$24.2.4.21 - DataView.prototype[@@toStringTag] should be "DataView":
   * Test: `Object.prototype.toString.call(new DataView(new ArrayBuffer(1)))``
   *  - Edge <=13 === "[object Object]"
   */
  if (dataViewExists && objPrototype === DataView.prototype) {
    return 'DataView';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%mapiteratorprototype%-@@tostringtag)
   * ES6$23.1.5.2.2 - %MapIteratorPrototype%[@@toStringTag] should be "Map Iterator":
   * Test: `Object.prototype.toString.call(new Map().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (mapExists && objPrototype === mapIteratorPrototype) {
    return 'Map Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%setiteratorprototype%-@@tostringtag)
   * ES6$23.2.5.2.2 - %SetIteratorPrototype%[@@toStringTag] should be "Set Iterator":
   * Test: `Object.prototype.toString.call(new Set().entries())``
   *  - Edge <=13 === "[object Object]"
   */
  if (setExists && objPrototype === setIteratorPrototype) {
    return 'Set Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%arrayiteratorprototype%-@@tostringtag)
   * ES6$22.1.5.2.2 - %ArrayIteratorPrototype%[@@toStringTag] should be "Array Iterator":
   * Test: `Object.prototype.toString.call([][Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (arrayIteratorExists && objPrototype === arrayIteratorPrototype) {
    return 'Array Iterator';
  }

  /* ! Spec Conformance
   * (http://www.ecma-international.org/ecma-262/6.0/index.html#sec-%stringiteratorprototype%-@@tostringtag)
   * ES6$21.1.5.2.2 - %StringIteratorPrototype%[@@toStringTag] should be "String Iterator":
   * Test: `Object.prototype.toString.call(''[Symbol.iterator]())``
   *  - Edge <=13 === "[object Object]"
   */
  if (stringIteratorExists && objPrototype === stringIteratorPrototype) {
    return 'String Iterator';
  }

  /* ! Speed optimisation
  * Pre:
  *   object from null   x 2,424,320 ops/sec ±1.67% (76 runs sampled)
  * Post:
  *   object from null   x 5,838,000 ops/sec ±0.99% (84 runs sampled)
  */
  if (objPrototype === null) {
    return 'Object';
  }

  return Object
    .prototype
    .toString
    .call(obj)
    .slice(toStringLeftSliceLength, toStringRightSliceLength);
}

return typeDetect;

})));


/***/ }),

/***/ "./node_modules/chai/index.mjs":
/*!*************************************!*\
  !*** ./node_modules/chai/index.mjs ***!
  \*************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "expect": () => (/* binding */ expect),
/* harmony export */   "version": () => (/* binding */ version),
/* harmony export */   "Assertion": () => (/* binding */ Assertion),
/* harmony export */   "AssertionError": () => (/* binding */ AssertionError),
/* harmony export */   "util": () => (/* binding */ util),
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "use": () => (/* binding */ use),
/* harmony export */   "should": () => (/* binding */ should),
/* harmony export */   "assert": () => (/* binding */ assert),
/* harmony export */   "core": () => (/* binding */ core),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.js */ "./node_modules/chai/index.js");


const expect = _index_js__WEBPACK_IMPORTED_MODULE_0__.expect;
const version = _index_js__WEBPACK_IMPORTED_MODULE_0__.version;
const Assertion = _index_js__WEBPACK_IMPORTED_MODULE_0__.Assertion;
const AssertionError = _index_js__WEBPACK_IMPORTED_MODULE_0__.AssertionError;
const util = _index_js__WEBPACK_IMPORTED_MODULE_0__.util;
const config = _index_js__WEBPACK_IMPORTED_MODULE_0__.config;
const use = _index_js__WEBPACK_IMPORTED_MODULE_0__.use;
const should = _index_js__WEBPACK_IMPORTED_MODULE_0__.should;
const assert = _index_js__WEBPACK_IMPORTED_MODULE_0__.assert;
const core = _index_js__WEBPACK_IMPORTED_MODULE_0__.core;

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_index_js__WEBPACK_IMPORTED_MODULE_0__);


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhaS5hZDNiNTkzM2FlODBiNmY1ZmRhZC5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xEO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFlBQVk7QUFDWjs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLGlCQUFpQjtBQUM1QjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLFFBQVE7QUFDbkI7O0FBRUE7QUFDQTtBQUNBLGlDQUFpQzs7QUFFakM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCLFlBQVksUUFBUTtBQUNwQjs7QUFFQTtBQUNBO0FBQ0EsdUJBQXVCLGlCQUFpQjs7QUFFeEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7QUNuSEEseUZBQXNDOzs7Ozs7Ozs7OztBQ0F0QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxlQUFlOztBQUVmO0FBQ0E7QUFDQTs7QUFFQSw4R0FBbUQ7O0FBRW5EO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLG1CQUFPLENBQUMsaUVBQWM7O0FBRWpDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsYUFBYSxNQUFNO0FBQ25CO0FBQ0E7O0FBRUEsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFlBQVk7O0FBRVo7QUFDQTtBQUNBOztBQUVBLGFBQWEsbUJBQU8sQ0FBQyw2REFBZTtBQUNwQyxjQUFjOztBQUVkO0FBQ0E7QUFDQTs7QUFFQSxnQkFBZ0IsbUJBQU8sQ0FBQyxtRUFBa0I7QUFDMUM7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFdBQVcsbUJBQU8sQ0FBQywrRUFBd0I7QUFDM0M7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGFBQWEsbUJBQU8sQ0FBQyxpRkFBeUI7QUFDOUM7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGFBQWEsbUJBQU8sQ0FBQyxpRkFBeUI7QUFDOUM7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGFBQWEsbUJBQU8sQ0FBQyxpRkFBeUI7QUFDOUM7Ozs7Ozs7Ozs7O0FDM0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxhQUFhLG1CQUFPLENBQUMsd0RBQVU7O0FBRS9CO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRDtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsVUFBVTtBQUN2QixhQUFhLFNBQVM7QUFDdEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGVBQWU7QUFDNUIsYUFBYSxpQkFBaUI7QUFDOUIsYUFBYSxpQkFBaUI7QUFDOUIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFNBQVM7QUFDdEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7O0FDOUtBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDO0FBQzNDO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNEQUFzRDtBQUN0RDtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsdUJBQXVCO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDO0FBQzdDO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7O0FDN0ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QjtBQUM5QixpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLG9DQUFvQztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2REFBNkQsS0FBSztBQUNsRSxpQkFBaUIsS0FBSyxpQkFBaUIsS0FBSztBQUM1QyxpQkFBaUIsS0FBSyxnQkFBZ0IsS0FBSztBQUMzQztBQUNBLDhEQUE4RCxLQUFLO0FBQ25FLGtCQUFrQixLQUFLLG9CQUFvQixLQUFLO0FBQ2hELGtCQUFrQixLQUFLLG1CQUFtQixLQUFLO0FBQy9DO0FBQ0Esa0VBQWtFLEtBQUs7QUFDdkUsaUJBQWlCLElBQUksTUFBTSxtQkFBbUIsSUFBSSxNQUFNO0FBQ3hELGlCQUFpQixJQUFJLE1BQU0sa0JBQWtCLElBQUksTUFBTTtBQUN2RDtBQUNBLGdFQUFnRSxLQUFLO0FBQ3JFLGtCQUFrQixLQUFLLDBCQUEwQixLQUFLO0FBQ3RELGtCQUFrQixLQUFLLHlCQUF5QixLQUFLO0FBQ3JEO0FBQ0EsMkRBQTJELEtBQUs7QUFDaEUsMEJBQTBCLEtBQUssd0JBQXdCLEtBQUs7QUFDNUQsMEJBQTBCLEtBQUssdUJBQXVCLEtBQUs7QUFDM0Q7QUFDQSxzRUFBc0UsS0FBSztBQUMzRSxpQkFBaUIsSUFBSSxNQUFNLDhCQUE4QixLQUFLO0FBQzlELGlCQUFpQixJQUFJLE1BQU0sNkJBQTZCLEtBQUs7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSSxlQUFlO0FBQ3BDLGlCQUFpQixJQUFJLGVBQWUscUJBQXFCLGNBQWM7QUFDdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTyxZQUFZO0FBQ3BDLGlCQUFpQixPQUFPLFlBQVkscUJBQXFCLG9CQUFvQjtBQUM3RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEIsaUJBQWlCLEtBQUs7QUFDdEIsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQSxpQkFBaUIsS0FBSyxrQkFBa0IsS0FBSztBQUM3QyxpQkFBaUIsS0FBSyxjQUFjLEtBQUssdUJBQXVCLEtBQUs7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsV0FBVztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFdBQVc7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDO0FBQzFDLDhDQUE4QztBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixpQkFBaUIsY0FBYyxXQUFXO0FBQzNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOERBQThELEtBQUs7QUFDbkUsa0JBQWtCLEtBQUssb0JBQW9CLEtBQUs7QUFDaEQsa0JBQWtCLEtBQUssbUJBQW1CLEtBQUs7QUFDL0M7QUFDQSxrRUFBa0UsS0FBSztBQUN2RSxpQkFBaUIsSUFBSSxNQUFNLG1CQUFtQixJQUFJLE1BQU07QUFDeEQsaUJBQWlCLElBQUksTUFBTSxrQkFBa0IsSUFBSSxNQUFNO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSyxrQkFBa0IsS0FBSztBQUM3QyxpQkFBaUIsS0FBSyxjQUFjLEtBQUssdUJBQXVCLEtBQUs7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLElBQUksTUFBTSx1QkFBdUIsSUFBSSxNQUFNO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLElBQUksZUFBZSxxQkFBcUIsY0FBYztBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPLFVBQVUscUJBQXFCLGtCQUFrQjtBQUN6RTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSSxLQUFLLEtBQUssR0FBRywwQkFBMEIsV0FBVyxNQUFNO0FBQzdFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSyxrQ0FBa0M7QUFDeEQsaUJBQWlCLEtBQUssa0JBQWtCLFdBQVcsR0FBRztBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFdBQVcsY0FBYyxXQUFXLEdBQUc7QUFDeEQsaUJBQWlCLFdBQVcsa0JBQWtCLFdBQVcsR0FBRztBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLGlCQUFpQjtBQUNsQyxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLFVBQVU7QUFDVjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsVUFBVTtBQUNWO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7O0FBRVQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLDZCQUE2QjtBQUM3QjtBQUNBLGtDQUFrQztBQUNsQyxnQ0FBZ0M7QUFDaEM7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLGlDQUFpQztBQUNqQztBQUNBLG9DQUFvQztBQUNwQyxxQ0FBcUM7QUFDckM7QUFDQSxrQ0FBa0M7QUFDbEMsb0NBQW9DO0FBQ3BDO0FBQ0EsNENBQTRDO0FBQzVDLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUIsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DO0FBQ3BDLHVDQUF1QztBQUN2QztBQUNBLGdDQUFnQztBQUNoQyxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0M7QUFDbEMsdUNBQXVDO0FBQ3ZDO0FBQ0EsZ0NBQWdDO0FBQ2hDLG9DQUFvQztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLHdDQUF3QztBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0M7QUFDeEMsc0NBQXNDO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsTUFBTTtBQUM1QixzQkFBc0IsTUFBTTtBQUM1QjtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEMsNkJBQTZCO0FBQzdCO0FBQ0EsZ0NBQWdDO0FBQ2hDLDZCQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0M7QUFDbEMsb0NBQW9DO0FBQ3BDO0FBQ0EsNENBQTRDO0FBQzVDLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsTUFBTTtBQUMxQixvQkFBb0IsTUFBTTtBQUMxQjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRDtBQUNoRCw0Q0FBNEM7QUFDNUM7QUFDQSxpRUFBaUU7QUFDakUscURBQXFEO0FBQ3JEO0FBQ0EsNkJBQTZCLEtBQUssd0JBQXdCO0FBQzFELGlCQUFpQixLQUFLLG1CQUFtQjtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxvQkFBb0IsTUFBTTtBQUMxQixvQkFBb0IsTUFBTTtBQUMxQjtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDO0FBQzFDLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RCxLQUFLO0FBQ2xFLGlCQUFpQixLQUFLLGlCQUFpQixLQUFLO0FBQzVDLGlCQUFpQixLQUFLLGdCQUFnQixLQUFLO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxzQkFBc0IsTUFBTSxXQUFXLElBQUk7QUFDM0Msc0JBQXNCLE1BQU0sZUFBZSxJQUFJO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUU7QUFDakUsaUJBQWlCLEtBQUssVUFBVSxLQUFLLGlCQUFpQixLQUFLO0FBQzNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSyxVQUFVLEtBQUssR0FBRztBQUN4QyxpQkFBaUIsS0FBSyxjQUFjLEtBQUssR0FBRztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUssVUFBVSxLQUFLO0FBQ3JDLGlCQUFpQixLQUFLLDZCQUE2QixLQUFLO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU0sa0JBQWtCLElBQUk7QUFDaEQsb0JBQW9CLE1BQU0sc0JBQXNCLElBQUk7QUFDcEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QyxrREFBa0Q7QUFDbEQ7QUFDQSxnREFBZ0Q7QUFDaEQsc0RBQXNEO0FBQ3REO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyx1Q0FBdUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsTUFBTSxxQ0FBcUMsS0FBSyxVQUFVLElBQUk7QUFDcEYsc0JBQXNCLE1BQU0seUNBQXlDLElBQUk7QUFDekU7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxzQkFBc0IsTUFBTSxjQUFjLElBQUk7QUFDOUMsc0JBQXNCLE1BQU0sZ0JBQWdCLElBQUk7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLHNDQUFzQztBQUN0QyxzQ0FBc0M7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEM7QUFDNUMscURBQXFEO0FBQ3JEO0FBQ0EsZ0RBQWdEO0FBQ2hELHlEQUF5RDtBQUN6RDtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEMsMENBQTBDO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQU0sd0NBQXdDLEtBQUssVUFBVSxJQUFJO0FBQ3ZGLHNCQUFzQixNQUFNLHFDQUFxQyxJQUFJO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0Esc0JBQXNCLE1BQU0saUJBQWlCLElBQUk7QUFDakQsc0JBQXNCLE1BQU0sY0FBYyxJQUFJO0FBQzlDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEM7QUFDNUMsa0RBQWtEO0FBQ2xEO0FBQ0EsOENBQThDO0FBQzlDLHNEQUFzRDtBQUN0RDtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEMsdUNBQXVDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQU0scUNBQXFDLEtBQUssVUFBVSxJQUFJO0FBQ3BGLHNCQUFzQixNQUFNLHlDQUF5QyxJQUFJO0FBQ3pFO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0Esc0JBQXNCLE1BQU0sY0FBYyxJQUFJO0FBQzlDLHNCQUFzQixNQUFNLGlCQUFpQixJQUFJO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyxxQ0FBcUM7QUFDckMscUNBQXFDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDO0FBQzVDLG9EQUFvRDtBQUNwRDtBQUNBLGdEQUFnRDtBQUNoRCx3REFBd0Q7QUFDeEQ7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixNQUFNLHVDQUF1QyxLQUFLLFVBQVUsSUFBSTtBQUN0RixzQkFBc0IsTUFBTSxxQ0FBcUMsSUFBSTtBQUNyRTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLHNCQUFzQixNQUFNLGdCQUFnQixJQUFJO0FBQ2hELHNCQUFzQixNQUFNLGNBQWMsSUFBSTtBQUM5QztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQztBQUNoQyx1Q0FBdUM7QUFDdkMsdUNBQXVDO0FBQ3ZDLHVDQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDO0FBQzVDLHNEQUFzRDtBQUN0RDtBQUNBLGdEQUFnRDtBQUNoRCwwREFBMEQ7QUFDMUQ7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQU07QUFDNUIsc0JBQXNCLE1BQU07QUFDNUI7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLHNCQUFzQixNQUFNO0FBQzVCLHNCQUFzQixNQUFNO0FBQzVCO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxhQUFhO0FBQzFCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0VBQXNFLEtBQUs7QUFDM0UsaUJBQWlCLElBQUksTUFBTSw4QkFBOEIsS0FBSztBQUM5RCxpQkFBaUIsSUFBSSxNQUFNLDZCQUE2QixLQUFLO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QixpQkFBaUIsS0FBSztBQUN0QixpQkFBaUIsS0FBSztBQUN0QixpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSSxNQUFNLGtDQUFrQyxLQUFLO0FBQ2xFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLElBQUksZUFBZTtBQUNwQyxpQkFBaUIsSUFBSSxlQUFlO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU8sWUFBWTtBQUNwQztBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSSxLQUFLLEtBQUssR0FBRztBQUNsQyxvREFBb0QsS0FBSztBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLLDZCQUE2QjtBQUNuRCxpQkFBaUIsS0FBSyxnQ0FBZ0M7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLLDRCQUE0QjtBQUNsRCxpQkFBaUIsS0FBSyxnQ0FBZ0M7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCLGlCQUFpQixLQUFLO0FBQ3RCLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQU07QUFDNUIsc0JBQXNCLE1BQU07QUFDNUI7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQU0sa0RBQWtELElBQUksWUFBWSxJQUFJO0FBQ2xHLHNCQUFzQixNQUFNLHNEQUFzRCxJQUFJO0FBQ3RGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUZBQWlGLE1BQU07QUFDdkYsaUZBQWlGLE1BQU07QUFDdkY7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLHNCQUFzQixNQUFNO0FBQzVCLHNCQUFzQixNQUFNO0FBQzVCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNENBQTRDO0FBQzVDLGdEQUFnRDtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnREFBZ0QsaUJBQWlCO0FBQ2pFLG1EQUFtRDtBQUNuRDtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU0sa0NBQWtDLEtBQUssVUFBVSxJQUFJO0FBQy9FLG9CQUFvQixNQUFNLHNDQUFzQyxJQUFJO0FBQ3BFO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0IsTUFBTTtBQUMxQixvQkFBb0IsTUFBTTtBQUMxQjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0VBQWdFO0FBQ2hFO0FBQ0E7QUFDQSxpQkFBaUIsV0FBVztBQUM1QjtBQUNBO0FBQ0EsaUJBQWlCLFdBQVc7QUFDNUI7QUFDQTtBQUNBLGlCQUFpQixXQUFXLG9CQUFvQixXQUFXLEdBQUc7QUFDOUQsOENBQThDLFdBQVcsR0FBRztBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFdBQVc7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkRBQTJELEtBQUs7QUFDaEUsMEJBQTBCLEtBQUssNEJBQTRCLEtBQUs7QUFDaEUsMEJBQTBCLEtBQUssMkJBQTJCLEtBQUs7QUFDL0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QixpQkFBaUIsV0FBVztBQUM1QjtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBLGlCQUFpQixXQUFXO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsaUJBQWlCLFdBQVc7QUFDNUI7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQSxpQkFBaUIsV0FBVztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsV0FBVyw4QkFBOEI7QUFDMUQsaUJBQWlCLFdBQVcsMEJBQTBCO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsaUJBQWlCO0FBQ2xDLGlCQUFpQixpQkFBaUI7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLHdCQUF3QjtBQUNyQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHdDQUF3QyxrQkFBa0I7O0FBRTFEO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdDQUF3QztBQUN4QywyREFBMkQ7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0Esc0RBQXNEO0FBQ3RELDBEQUEwRDtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0M7QUFDcEMsb0NBQW9DO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLFNBQVMsZUFBZTtBQUN0RCxzREFBc0Q7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEI7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEIsYUFBYSxlQUFlO0FBQzFELDBEQUEwRDtBQUMxRCwwREFBMEQ7QUFDMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsd0JBQXdCO0FBQ3JDLGFBQWEsZUFBZTtBQUM1QixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLElBQUk7QUFDakMsUUFBUTtBQUNSO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHNCQUFzQixNQUFNO0FBQzVCLHNCQUFzQixNQUFNLDRCQUE0QixLQUFLO0FBQzdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQSw0QkFBNEIsTUFBTSxXQUFXLEtBQUssTUFBTSxLQUFLO0FBQzdELDRCQUE0QixNQUFNLGVBQWUsSUFBSSxvQ0FBb0MsS0FBSztBQUM5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBLDBCQUEwQixNQUFNLFdBQVcsS0FBSyxNQUFNLEtBQUs7QUFDM0QsMEJBQTBCLE1BQU0sZUFBZSxJQUFJLHlCQUF5QixLQUFLO0FBQ2pGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBLDBCQUEwQixNQUFNLHFDQUFxQyxLQUFLLFVBQVUsSUFBSTtBQUN4RiwwQkFBMEIsTUFBTSx5Q0FBeUMsSUFBSTtBQUM3RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsTUFBTSxXQUFXLEtBQUssTUFBTSxLQUFLO0FBQ3ZELHNCQUFzQixNQUFNLGVBQWUsSUFBSSx5QkFBeUIsS0FBSztBQUM3RTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxvQkFBb0IsTUFBTTtBQUMxQixvQkFBb0IsTUFBTTtBQUMxQjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxVQUFVO0FBQ3ZCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DO0FBQ3BDLDhDQUE4QztBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esb0JBQW9CLE1BQU07QUFDMUIsb0JBQW9CLE1BQU07QUFDMUI7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdFQUFnRSxLQUFLO0FBQ3JFLGtCQUFrQixLQUFLLDBCQUEwQixLQUFLO0FBQ3RELGtCQUFrQixLQUFLLHlCQUF5QixLQUFLO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUs7QUFDdkMsOENBQThDLEtBQUssR0FBRyxLQUFLO0FBQzNELG1EQUFtRCxLQUFLLEdBQUcsS0FBSztBQUNoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOERBQThEO0FBQzlELHFEQUFxRDtBQUNyRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsNEJBQTRCLE1BQU0sMkJBQTJCLElBQUk7QUFDakUsa0NBQWtDLE1BQU0sK0JBQStCLElBQUk7QUFDM0UsTUFBTTtBQUNOO0FBQ0EsNEJBQTRCLE1BQU0sc0NBQXNDLElBQUk7QUFDNUUsa0NBQWtDLE1BQU0sMENBQTBDLElBQUk7QUFDdEY7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLDJDQUEyQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDLCtDQUErQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxVQUFVO0FBQ3ZCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMENBQTBDLDJDQUEyQztBQUNyRixzQkFBc0IsTUFBTSxvQkFBb0IsSUFBSTtBQUNwRCxzQkFBc0IsTUFBTSx3QkFBd0IsSUFBSTtBQUN4RDtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBLDRDQUE0QyxxQ0FBcUM7QUFDakYsd0JBQXdCLE1BQU0seUJBQXlCLElBQUk7QUFDM0Qsd0JBQXdCLE1BQU0seUJBQXlCLElBQUk7QUFDM0Q7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQSx3QkFBd0IsTUFBTSxlQUFlLElBQUk7QUFDakQsd0JBQXdCLE1BQU0sbUJBQW1CLElBQUk7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQztBQUNwQyxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsb0NBQW9DO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLG9DQUFvQztBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQztBQUNwQyxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEMseUNBQXlDO0FBQ3pDO0FBQ0EseURBQXlEO0FBQ3pELHVEQUF1RDtBQUN2RDtBQUNBLDhEQUE4RDtBQUM5RCw0REFBNEQ7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQztBQUNwQyxvQ0FBb0M7QUFDcEM7QUFDQSxtREFBbUQ7QUFDbkQsNkNBQTZDO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIsb0NBQW9DO0FBQ3BDO0FBQ0EseURBQXlEO0FBQ3pELG1EQUFtRDtBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0Qix5Q0FBeUM7QUFDekM7QUFDQSw4REFBOEQ7QUFDOUQsNERBQTREO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCO0FBQ0E7QUFDQSxtREFBbUQ7QUFDbkQscURBQXFEO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlCQUFpQjtBQUM5QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QztBQUN6QyxvQ0FBb0M7QUFDcEM7QUFDQSx3REFBd0Q7QUFDeEQsa0RBQWtEO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEIseUNBQXlDO0FBQ3pDO0FBQ0EsOERBQThEO0FBQzlELHdEQUF3RDtBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEM7QUFDQSx5REFBeUQ7QUFDekQsdURBQXVEO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCO0FBQ0E7QUFDQSxtREFBbUQ7QUFDbkQscURBQXFEO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlCQUFpQjtBQUM5QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCO0FBQ3RCLHlDQUF5QztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEMseUNBQXlDO0FBQ3pDO0FBQ0EseURBQXlEO0FBQ3pELHVEQUF1RDtBQUN2RDtBQUNBLDhEQUE4RDtBQUM5RCw0REFBNEQ7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQjtBQUN0QixvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0EsOERBQThEO0FBQzlELDBDQUEwQztBQUMxQyw0Q0FBNEM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxvQkFBb0IsTUFBTTtBQUMxQixvQkFBb0IsTUFBTTtBQUMxQjtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQztBQUMxQyw0Q0FBNEM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBDQUEwQztBQUMxQyx5Q0FBeUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEMsdUNBQXVDO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDO0FBQzlDLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBLGdEQUFnRDtBQUNoRCw2Q0FBNkM7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNO0FBQzFCLG9CQUFvQixNQUFNO0FBQzFCO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7OztBQzV3SEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNLFdBQVcsSUFBSTtBQUN6QyxvQkFBb0IsTUFBTSxlQUFlLElBQUk7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9CQUFvQixNQUFNLGVBQWUsSUFBSTtBQUM3QyxvQkFBb0IsTUFBTSxXQUFXLElBQUk7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLGNBQWMsSUFBSSxjQUFjO0FBQzVEO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixjQUFjLElBQUksZ0JBQWdCO0FBQ2pFO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLGFBQWE7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQztBQUNyQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsYUFBYTtBQUMxQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxhQUFhO0FBQzFCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLCtCQUErQixJQUFJLFlBQVk7QUFDekU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIscUJBQXFCO0FBQ3JCO0FBQ0EseUJBQXlCLHFCQUFxQixHQUFHLFVBQVU7QUFDM0QseUJBQXlCLHFCQUFxQixHQUFHLHFCQUFxQjtBQUN0RTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsK0JBQStCLElBQUksWUFBWTtBQUM1RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLHFCQUFxQjtBQUNyQiwwQ0FBMEMsS0FBSztBQUMvQyw0QkFBNEIscUJBQXFCLEdBQUcsTUFBTSxNQUFNO0FBQ2hFLDRCQUE0QixxQkFBcUIsR0FBRyxpQkFBaUIsTUFBTTtBQUMzRTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixxQkFBcUI7QUFDckIsMkNBQTJDLEtBQUs7QUFDaEQsNkJBQTZCLHFCQUFxQixHQUFHLE1BQU0sTUFBTTtBQUNqRSw2QkFBNkIscUJBQXFCLEdBQUcsTUFBTSxLQUFLLFFBQVEsTUFBTTtBQUM5RTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixxQkFBcUI7QUFDckIsOENBQThDLEtBQUs7QUFDbkQsZ0NBQWdDLHFCQUFxQixHQUFHLE1BQU0sTUFBTTtBQUNwRSxnQ0FBZ0MscUJBQXFCLEdBQUcsTUFBTSxLQUFLLFFBQVEsTUFBTTtBQUNqRjtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixPQUFPLFVBQVUsR0FBRyxnQkFBZ0I7QUFDbkUsK0JBQStCLE1BQU0sWUFBWSxHQUFHLGlCQUFpQjtBQUNyRTtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQyxPQUFPLFVBQVUsR0FBRyxjQUFjO0FBQ3BFLGtDQUFrQyxNQUFNLFlBQVksR0FBRyxpQkFBaUI7QUFDeEU7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyxJQUFJLEtBQUssS0FBSyxHQUFHLEdBQUcsV0FBVyxNQUFNO0FBQ3hFLG1DQUFtQyxPQUFPLFFBQVEsT0FBTyxHQUFHLGlCQUFpQixNQUFNO0FBQ25GO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsSUFBSSxLQUFLLEtBQUssR0FBRyxHQUFHLFdBQVcsTUFBTTtBQUMzRSxzQ0FBc0MsT0FBTyxRQUFRLE9BQU8sR0FBRyxpQkFBaUIsTUFBTTtBQUN0RjtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLE1BQU0sSUFBSSxNQUFNO0FBQzdDO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDQUFnQyxNQUFNLElBQUksTUFBTTtBQUNoRDtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU07QUFDeEQ7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxJQUFJLE1BQU0sR0FBRyxJQUFJLE1BQU07QUFDM0Q7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsT0FBTyxrQkFBa0I7QUFDcEQsMkJBQTJCLE9BQU8sa0JBQWtCO0FBQ3BEO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QixPQUFPLGtCQUFrQjtBQUN2RDtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLGdCQUFnQjtBQUM5QztBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxnQkFBZ0I7QUFDakQsaUNBQWlDLGdCQUFnQjtBQUNqRDtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsT0FBTyxtQkFBbUIsV0FBVyxpQkFBaUI7QUFDeEY7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLE9BQU8sbUJBQW1CLFdBQVcsaUJBQWlCO0FBQzNGLHFDQUFxQyxPQUFPLG1CQUFtQixXQUFXLGlCQUFpQjtBQUMzRixxQ0FBcUMsT0FBTyxtQkFBbUIsY0FBYyxpQkFBaUI7QUFDOUY7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLE9BQU8sa0JBQWtCO0FBQ3ZEO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxPQUFPLGtCQUFrQjtBQUMxRCxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsa0JBQWtCO0FBQ25EO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxpQkFBaUI7QUFDckQsb0NBQW9DO0FBQ3BDO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFDQUFxQyxPQUFPLG1CQUFtQixXQUFXLGlCQUFpQjtBQUMzRjtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsT0FBTyxtQkFBbUIsV0FBVyxpQkFBaUI7QUFDOUYsd0NBQXdDLE9BQU8sbUJBQW1CLFdBQVcsaUJBQWlCO0FBQzlGLHdDQUF3QyxPQUFPLG1CQUFtQixjQUFjLGlCQUFpQjtBQUNqRyx3Q0FBd0M7QUFDeEM7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDLE9BQU8sa0JBQWtCO0FBQzFEO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsT0FBTyxrQkFBa0I7QUFDN0Q7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxPQUFPLGtCQUFrQjtBQUM3RDtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxPQUFPLGtCQUFrQjtBQUNoRSx1Q0FBdUMsT0FBTyxrQkFBa0I7QUFDaEU7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsT0FBTyxTQUFTLG1CQUFtQixpQkFBaUIsZUFBZTtBQUMzRztBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyxPQUFPLFNBQVMsbUJBQW1CLGlCQUFpQixlQUFlO0FBQzlHLDJDQUEyQyxPQUFPLFNBQVMsbUJBQW1CLGlCQUFpQixnQkFBZ0I7QUFDL0csMkNBQTJDLE9BQU8sU0FBUyxtQkFBbUIsaUJBQWlCLGVBQWU7QUFDOUc7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25ELDRCQUE0Qix1QkFBdUIsR0FBRyxtQ0FBbUM7QUFDekYsc0NBQXNDLE9BQU8sZ0NBQWdDLE9BQU87QUFDcEYscUNBQXFDLFdBQVcsb0JBQW9CLFdBQVc7QUFDL0U7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLGNBQWM7QUFDM0IsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25ELDRCQUE0Qix1QkFBdUIsR0FBRztBQUN0RCxzQ0FBc0MsT0FBTyxnQ0FBZ0MsT0FBTztBQUNwRixxQ0FBcUMsV0FBVyxtQkFBbUIsV0FBVztBQUM5RTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyx1QkFBdUI7QUFDeEQsaUNBQWlDLHVCQUF1QjtBQUN4RCxpQ0FBaUMsdUJBQXVCLEdBQUcsbUJBQW1CO0FBQzlFLGlDQUFpQyx1QkFBdUIsR0FBRyw0QkFBNEI7QUFDdkYsMkNBQTJDLE9BQU8sZ0NBQWdDLE9BQU87QUFDekYsMkNBQTJDLE9BQU8sZ0NBQWdDLE9BQU87QUFDekYsMENBQTBDLFdBQVcsbUJBQW1CLFdBQVc7QUFDbkYsMENBQTBDLFdBQVcsbUJBQW1CLFdBQVc7QUFDbkY7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyx1QkFBdUI7QUFDM0Qsb0NBQW9DLHVCQUF1QixHQUFHLCtCQUErQjtBQUM3Riw4Q0FBOEMsT0FBTyxnQ0FBZ0MsV0FBVztBQUNoRyw2Q0FBNkMsV0FBVyxtQkFBbUIsV0FBVztBQUN0RjtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DLHVCQUF1QjtBQUMzRCxvQ0FBb0MsdUJBQXVCLEdBQUcsK0JBQStCO0FBQzdGLDhDQUE4QyxPQUFPLGdDQUFnQyxXQUFXO0FBQ2hHLDZDQUE2QyxXQUFXLG1CQUFtQixXQUFXO0FBQ3RGO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxVQUFVO0FBQ3ZCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLFdBQVcsMEJBQTBCLFdBQVc7QUFDMUYsMENBQTBDLFdBQVcsMkJBQTJCLFdBQVcsR0FBRyxXQUFXO0FBQ3pHLDBDQUEwQyxXQUFXLGlCQUFpQixXQUFXLG1CQUFtQixXQUFXLEdBQUcsV0FBVztBQUM3SCx5Q0FBeUMsV0FBVyxHQUFHLFdBQVcsS0FBSyxXQUFXO0FBQ2xGLHlDQUF5QyxXQUFXLEdBQUcsV0FBVyxNQUFNLFdBQVcsR0FBRyxlQUFlO0FBQ3JHLHlDQUF5QyxXQUFXLEdBQUcsV0FBVyxNQUFNLFdBQVcsR0FBRyxXQUFXO0FBQ2pHO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxjQUFjO0FBQzNCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLFdBQVcsa0JBQWtCLFdBQVc7QUFDbEYsMENBQTBDLFdBQVcsaUJBQWlCLFdBQVcsbUJBQW1CLFdBQVcsR0FBRyxXQUFXO0FBQzdILHlDQUF5QyxXQUFXLEtBQUssV0FBVztBQUNwRSx5Q0FBeUMsV0FBVyxHQUFHLFdBQVcsTUFBTSxXQUFXLEdBQUcsV0FBVztBQUNqRztBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsY0FBYztBQUMzQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxXQUFXLDBCQUEwQixXQUFXO0FBQy9GLCtDQUErQyxXQUFXLGlCQUFpQixXQUFXLG1CQUFtQixXQUFXLEdBQUcsV0FBVztBQUNsSSw4Q0FBOEMsV0FBVyxHQUFHLFdBQVcsS0FBSyxXQUFXO0FBQ3ZGLDhDQUE4QyxXQUFXLEdBQUcsV0FBVyxNQUFNLFdBQVcsR0FBRyxXQUFXO0FBQ3RHO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxjQUFjO0FBQzNCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELFdBQVcsMEJBQTBCLHFCQUFxQjtBQUM1RyxrREFBa0QsV0FBVyxpQkFBaUIsV0FBVyxtQkFBbUIsaUJBQWlCLEdBQUcsZUFBZTtBQUMvSSxpREFBaUQsV0FBVyxHQUFHLFdBQVcsS0FBSyxpQkFBaUI7QUFDaEcsaURBQWlELFdBQVcsR0FBRyxXQUFXLE1BQU0saUJBQWlCLEdBQUcsZUFBZTtBQUNuSDtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsY0FBYztBQUMzQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCxXQUFXLDBCQUEwQixxQkFBcUI7QUFDNUcsa0RBQWtELFdBQVcsaUJBQWlCLFdBQVcsbUJBQW1CLGlCQUFpQixHQUFHLFdBQVc7QUFDM0ksaURBQWlELFdBQVcsR0FBRyxXQUFXLEtBQUssaUJBQWlCO0FBQ2hHLGlEQUFpRCxXQUFXLEdBQUcsV0FBVyxNQUFNLFdBQVcsR0FBRyxlQUFlO0FBQzdHO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxjQUFjO0FBQzNCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSx3QkFBd0I7QUFDckMsYUFBYSxlQUFlO0FBQzVCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxrQkFBa0I7QUFDL0IsYUFBYSxlQUFlO0FBQzVCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DLE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxNQUFNLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTTtBQUMvRjtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sTUFBTSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU07QUFDbEc7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sT0FBTyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU87QUFDeEc7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsTUFBTSxJQUFJLE1BQU0sSUFBSSxPQUFPLE9BQU8sTUFBTSxJQUFJLE1BQU0sSUFBSSxPQUFPO0FBQzNHLDhDQUE4QyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sT0FBTyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU87QUFDM0c7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sT0FBTyxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU87QUFDcEc7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEMsTUFBTSxJQUFJLE1BQU0sSUFBSSxPQUFPLE9BQU8sTUFBTSxJQUFJLE9BQU87QUFDN0Y7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDLE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxPQUFPLE1BQU0sSUFBSSxPQUFPO0FBQ2pHO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpREFBaUQsTUFBTSxJQUFJLE1BQU0sSUFBSSxPQUFPLE9BQU8sTUFBTSxJQUFJLE9BQU87QUFDcEcsaURBQWlELE1BQU0sSUFBSSxNQUFNLElBQUksT0FBTyxPQUFPLE1BQU0sSUFBSSxPQUFPO0FBQ3BHLGlEQUFpRCxNQUFNLElBQUksTUFBTSxJQUFJLE9BQU8sT0FBTyxNQUFNLElBQUksT0FBTztBQUNwRztBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEdBQUc7QUFDaEIsYUFBYSxVQUFVO0FBQ3ZCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0EsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4REFBOEQ7QUFDOUQsMENBQTBDO0FBQzFDLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEM7QUFDMUMsMENBQTBDO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QjtBQUM5QjtBQUNBO0FBQ0E7QUFDQSxhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBLGFBQWEsNkJBQTZCO0FBQzFDLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixRQUFRO0FBQ3JDO0FBQ0E7QUFDQTtBQUNBLGFBQWEsNkJBQTZCO0FBQzFDLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDeGlHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOzs7Ozs7Ozs7OztBQzlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLFFBQVE7QUFDdkIsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxRQUFRO0FBQ3ZCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxVQUFVO0FBQ3pCLGVBQWUsa0JBQWtCO0FBQ2pDLGVBQWUsUUFBUTtBQUN2QixlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFVBQVU7QUFDekIsZUFBZSxrQkFBa0I7QUFDakMsZUFBZSxRQUFRO0FBQ3ZCLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDMU5BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHFCQUFxQixtQkFBTyxDQUFDLDhFQUFrQjtBQUMvQyxXQUFXLG1CQUFPLENBQUMsbURBQVk7QUFDL0IsV0FBVyxtQkFBTyxDQUFDLDBEQUFRO0FBQzNCLGNBQWMsbUJBQU8sQ0FBQyxnRUFBVztBQUNqQyxvQkFBb0IsbUJBQU8sQ0FBQyw0RUFBaUI7O0FBRTdDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLFVBQVU7QUFDckIsV0FBVyxVQUFVO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxNQUFNO0FBQ047O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBQVc7QUFDWDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7Ozs7QUN2SkEsaUVBQWlFOztBQUVqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQixXQUFXLFFBQVE7QUFDbkIsV0FBVyxTQUFTO0FBQ3BCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7Ozs7Ozs7Ozs7QUMzREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxxQkFBcUIsbUJBQU8sQ0FBQyw4RUFBa0I7QUFDL0MsV0FBVyxtQkFBTyxDQUFDLG1EQUFZO0FBQy9CLFdBQVcsbUJBQU8sQ0FBQywwREFBUTtBQUMzQixjQUFjLG1CQUFPLENBQUMsZ0VBQVc7QUFDakMsb0JBQW9CLG1CQUFPLENBQUMsNEVBQWlCOztBQUU3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLFFBQVE7QUFDbkIsV0FBVyxVQUFVO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNuRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLG1CQUFPLENBQUMsbURBQVk7QUFDL0IsV0FBVyxtQkFBTyxDQUFDLDBEQUFRO0FBQzNCLHFCQUFxQixtQkFBTyxDQUFDLDhFQUFrQjtBQUMvQyxvQkFBb0IsbUJBQU8sQ0FBQyw0RUFBaUI7O0FBRTdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLFVBQVU7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpREFBaUQ7O0FBRWpEO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7Ozs7Ozs7Ozs7QUN2RUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLGdFQUFXOztBQUVqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVEsa0NBQWtDO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUJBQXFCLG1CQUFPLENBQUMsZ0VBQWlCO0FBQzlDLFdBQVcsbUJBQU8sQ0FBQywwREFBUTtBQUMzQixXQUFXLG1CQUFPLENBQUMsOERBQWE7O0FBRWhDO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLG1DQUFtQyx5QkFBeUI7QUFDNUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7O0FBRUEsd0NBQXdDLDhCQUE4QjtBQUN0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNsREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDLGdDQUFnQztBQUNoQztBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLFFBQVE7QUFDbkIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsV0FBVztBQUN0QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7Ozs7OztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFdBQVcsbUJBQU8sQ0FBQywwREFBUTtBQUMzQixnQkFBZ0IsbUJBQU8sQ0FBQyxvRUFBYTtBQUNyQyxpQkFBaUIsbUJBQU8sQ0FBQyxzRUFBYzs7QUFFdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsS0FBSztBQUNiLFFBQVEsSUFBSTtBQUNaLFFBQVEsSUFBSTtBQUNaO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsV0FBVztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsTUFBTSxrQkFBa0IseUJBQXlCO0FBQ2xFLGlCQUFpQixLQUFLLGtCQUFrQiw0QkFBNEI7QUFDcEUsaUJBQWlCLEtBQUssa0JBQWtCLDhCQUE4Qjs7QUFFdEU7QUFDQTs7Ozs7Ozs7Ozs7QUNqREEsV0FBVyxtQkFBTyxDQUFDLDhEQUFhOztBQUVoQyxXQUFXLG1CQUFPLENBQUMsMERBQVE7O0FBRTNCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLFdBQVc7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7O0FDdERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHNDQUFzQyxtQkFBTyxDQUFDLGdIQUFtQzs7QUFFakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUM1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLGdEQUFTOztBQUUvQjtBQUNBO0FBQ0E7O0FBRUEsOEZBQWdDOztBQUVoQztBQUNBO0FBQ0E7O0FBRUEsa0dBQXFDOztBQUVyQztBQUNBO0FBQ0E7QUFDQSxtSEFBOEM7O0FBRTlDO0FBQ0E7QUFDQTs7QUFFQSxnSEFBNEM7O0FBRTVDO0FBQ0E7QUFDQTs7QUFFQSw2R0FBMEM7O0FBRTFDO0FBQ0E7QUFDQTs7QUFFQSx1R0FBc0M7O0FBRXRDO0FBQ0E7QUFDQTs7QUFFQSxnSEFBNEM7O0FBRTVDO0FBQ0E7QUFDQTs7QUFFQSw4RkFBZ0M7O0FBRWhDO0FBQ0E7QUFDQTs7QUFFQSx5SEFBa0Q7O0FBRWxEO0FBQ0E7QUFDQTs7QUFFQSxxRkFBaUM7O0FBRWpDO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUI7O0FBRW5CO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUI7O0FBRW5CO0FBQ0E7QUFDQTs7QUFFQSxtR0FBMEM7O0FBRTFDO0FBQ0E7QUFDQTs7QUFFQSxtSEFBOEM7O0FBRTlDO0FBQ0E7QUFDQTs7QUFFQSw2R0FBMEM7O0FBRTFDO0FBQ0E7QUFDQTs7QUFFQSxxSUFBMEQ7O0FBRTFEO0FBQ0E7QUFDQTs7QUFFQSwrSEFBc0Q7O0FBRXREO0FBQ0E7QUFDQTs7QUFFQSx3SUFBNEQ7O0FBRTVEO0FBQ0E7QUFDQTs7QUFFQSwwSkFBd0U7O0FBRXhFO0FBQ0E7QUFDQTs7QUFFQSxrSUFBd0Q7O0FBRXhEO0FBQ0E7QUFDQTs7QUFFQSwrS0FBc0Y7O0FBRXRGO0FBQ0E7QUFDQTs7QUFFQSxnS0FBNEU7O0FBRTVFO0FBQ0E7QUFDQTs7QUFFQSxrR0FBMkM7O0FBRTNDO0FBQ0E7QUFDQTs7QUFFQSx1R0FBc0M7O0FBRXRDO0FBQ0E7QUFDQTs7QUFFQSw0SEFBb0Q7O0FBRXBEO0FBQ0E7QUFDQTs7QUFFQSw0SEFBb0Q7O0FBRXBEO0FBQ0E7QUFDQTs7QUFFQSxpR0FBa0M7O0FBRWxDO0FBQ0E7QUFDQTs7QUFFQSxtSEFBOEM7Ozs7Ozs7Ozs7QUNqTDlDO0FBQ0E7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLDREQUFlO0FBQ3JDLG9CQUFvQixtQkFBTyxDQUFDLDRFQUFpQjtBQUM3Qyw4QkFBOEIsbUJBQU8sQ0FBQyxnR0FBMkI7QUFDakUsYUFBYSxtQkFBTyxDQUFDLHlEQUFXOztBQUVoQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxTQUFTO0FBQ3BCO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsU0FBUztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsNENBQTRDO0FBQzVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixLQUFLOztBQUV2QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9DQUFvQyxPQUFPO0FBQzNDO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsa0JBQWtCLGtCQUFrQjtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWCxVQUFVO0FBQ1Y7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDMVhBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7OztBQ3pCQSxhQUFhLG1CQUFPLENBQUMseURBQVc7O0FBRWhDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLGdFQUFXO0FBQ2pDLGFBQWEsbUJBQU8sQ0FBQyx5REFBVzs7QUFFaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IsdUJBQXVCO0FBQ3ZDLE1BQU07QUFDTjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUNqREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLG1CQUFPLENBQUMsbURBQVk7QUFDL0Isb0JBQW9CLG1CQUFPLENBQUMsNEVBQWlCOztBQUU3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxRQUFRO0FBQ25CLFdBQVcsVUFBVTtBQUNyQixXQUFXLFVBQVU7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDcEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUJBQXFCLG1CQUFPLENBQUMsOEVBQWtCO0FBQy9DLFdBQVcsbUJBQU8sQ0FBQyxtREFBWTtBQUMvQixXQUFXLG1CQUFPLENBQUMsMERBQVE7QUFDM0IsY0FBYyxtQkFBTyxDQUFDLGdFQUFXO0FBQ2pDLG9CQUFvQixtQkFBTyxDQUFDLDRFQUFpQjs7QUFFN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxRQUFRO0FBQ25CLFdBQVcsVUFBVTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUMzRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLG1CQUFPLENBQUMsbURBQVk7QUFDL0IsV0FBVyxtQkFBTyxDQUFDLDBEQUFRO0FBQzNCLHFCQUFxQixtQkFBTyxDQUFDLDhFQUFrQjtBQUMvQyxvQkFBb0IsbUJBQU8sQ0FBQyw0RUFBaUI7O0FBRTdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLFVBQVU7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7Ozs7Ozs7OztBQzNGQSxhQUFhLG1CQUFPLENBQUMseURBQVc7QUFDaEMsV0FBVyxtQkFBTyxDQUFDLDBEQUFRO0FBQzNCLG9CQUFvQixtQkFBTyxDQUFDLDRFQUFpQjtBQUM3QyxxQkFBcUIsbUJBQU8sQ0FBQyw4RUFBa0I7O0FBRS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxRQUFRO0FBQ25CO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLFFBQVE7QUFDbkIsV0FBVyxRQUFRO0FBQ25CLFlBQVksUUFBUTtBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLGtCQUFrQjtBQUNwQztBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsaUJBQWlCO0FBQ25DO0FBQ0E7O0FBRUEsa0JBQWtCLGtCQUFrQjtBQUNwQztBQUNBLG9CQUFvQixrQkFBa0I7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7O0FDbEpBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFdBQVcsbUJBQU8sQ0FBQywwREFBUTs7QUFFM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixXQUFXLFdBQVc7QUFDdEI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7O0FDM0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxXQUFXO0FBQ3RCLFdBQVcsUUFBUSw0Q0FBNEM7QUFDL0QsV0FBVyxTQUFTO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUM1Q2E7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsd0JBQXdCO0FBQ25DO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyx3QkFBd0I7QUFDbkM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxlQUFlO0FBQzFCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSix3REFBd0Q7QUFDeEQ7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLHdCQUF3QjtBQUNuQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEM7QUFDMUM7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxjQUFjO0FBQ3pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQzNLYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxXQUFXLG1CQUFPLENBQUMsOERBQWE7QUFDaEM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFlBQVk7QUFDdkIsYUFBYSxjQUFjO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsWUFBWTtBQUN2QixXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EseUJBQXlCOztBQUV6QjtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsUUFBUTtBQUNuQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQSxZQUFZLFNBQVM7QUFDckI7QUFDQTtBQUNBLHdEQUF3RDtBQUN4RDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFlBQVksY0FBYztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxRQUFRO0FBQ25CLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBLFlBQVksU0FBUztBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixZQUFZLFNBQVM7QUFDckI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsS0FBSztBQUNoQixXQUFXLEtBQUs7QUFDaEIsV0FBVyxRQUFRO0FBQ25CLFlBQVksU0FBUztBQUNyQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQixXQUFXLFVBQVU7QUFDckIsV0FBVyxRQUFRO0FBQ25CLFlBQVksU0FBUztBQUNyQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFVBQVU7QUFDckIsV0FBVyxVQUFVO0FBQ3JCLFdBQVcsUUFBUTtBQUNuQixZQUFZLFNBQVM7QUFDckI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixZQUFZLFNBQVM7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFdBQVc7QUFDdEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsUUFBUTtBQUNuQixZQUFZLFNBQVM7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLFlBQVk7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLFFBQVE7QUFDbkIsWUFBWSxTQUFTO0FBQ3JCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFlBQVksU0FBUztBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDdGNhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7QUMzQ2E7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakMseUNBQXlDO0FBQ3pDLGlDQUFpQztBQUNqQztBQUNBLHVDQUF1QztBQUN2QyxpQ0FBaUM7QUFDakMsaUNBQWlDO0FBQ2pDO0FBQ0Esd0NBQXdDO0FBQ3hDLGlDQUFpQztBQUNqQyxpQ0FBaUM7QUFDakM7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxlQUFlO0FBQzFCLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixhQUFhLFFBQVE7QUFDckI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsTUFBTTtBQUNOLGlCQUFpQjtBQUNqQjs7QUFFQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLFFBQVE7QUFDbkIsYUFBYSxrQkFBa0I7QUFDL0I7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxrQkFBa0IsZUFBZTtBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsR0FBRztBQUNkLFdBQVcsUUFBUTtBQUNuQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsZUFBZTtBQUNqQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBLE1BQU07QUFDTjtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFFBQVE7QUFDbkIsV0FBVyxRQUFRO0FBQ25CLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IscUJBQXFCO0FBQzdDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QywwQ0FBMEM7QUFDMUMsaURBQWlEO0FBQ2pEO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLHFCQUFxQjtBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0RBQXNELG1CQUFtQjtBQUN6RTtBQUNBO0FBQ0EsV0FBVyxRQUFRO0FBQ25CLFdBQVcsUUFBUTtBQUNuQixXQUFXLE9BQU87QUFDbEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7QUM1U0E7QUFDQSxDQUFDLEtBQTREO0FBQzdELENBQUMsQ0FDK0I7QUFDaEMsQ0FBQyxzQkFBc0I7O0FBRXZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFEQUFxRCxxQkFBTSxFQUFFOztBQUU3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWSxRQUFRO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25ZNkI7O0FBRXZCLGVBQWUsNkNBQVc7QUFDMUIsZ0JBQWdCLDhDQUFZO0FBQzVCLGtCQUFrQixnREFBYztBQUNoQyx1QkFBdUIscURBQW1CO0FBQzFDLGFBQWEsMkNBQVM7QUFDdEIsZUFBZSw2Q0FBVztBQUMxQixZQUFZLDBDQUFRO0FBQ3BCLGVBQWUsNkNBQVc7QUFDMUIsZUFBZSw2Q0FBVztBQUMxQixhQUFhLDJDQUFTOztBQUU3QixpRUFBZSxzQ0FBSSxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvYXNzZXJ0aW9uLWVycm9yL2luZGV4LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvaW5kZXguanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL2Fzc2VydGlvbi5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL2NvbmZpZy5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL2NvcmUvYXNzZXJ0aW9ucy5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL2ludGVyZmFjZS9hc3NlcnQuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS9pbnRlcmZhY2UvZXhwZWN0LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvaW50ZXJmYWNlL3Nob3VsZC5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL3V0aWxzL2FkZENoYWluYWJsZU1ldGhvZC5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL3V0aWxzL2FkZExlbmd0aEd1YXJkLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvYWRkTWV0aG9kLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvYWRkUHJvcGVydHkuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9jb21wYXJlQnlJbnNwZWN0LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvZXhwZWN0VHlwZXMuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9mbGFnLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvZ2V0QWN0dWFsLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvZ2V0RW51bWVyYWJsZVByb3BlcnRpZXMuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9nZXRNZXNzYWdlLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvZ2V0T3BlcmF0b3IuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9nZXRPd25FbnVtZXJhYmxlUHJvcGVydGllcy5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL3V0aWxzL2dldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9nZXRQcm9wZXJ0aWVzLmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9pbnNwZWN0LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvaXNOYU4uanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9pc1Byb3h5RW5hYmxlZC5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL3V0aWxzL29iakRpc3BsYXkuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9vdmVyd3JpdGVDaGFpbmFibGVNZXRob2QuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9vdmVyd3JpdGVNZXRob2QuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy9vdmVyd3JpdGVQcm9wZXJ0eS5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGFpL2xpYi9jaGFpL3V0aWxzL3Byb3hpZnkuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9saWIvY2hhaS91dGlscy90ZXN0LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL2NoYWkvbGliL2NoYWkvdXRpbHMvdHJhbnNmZXJGbGFncy5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9jaGVjay1lcnJvci9pbmRleC5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9kZWVwLWVxbC9pbmRleC5qcyIsIndlYnBhY2s6Ly9AZnJlZWNvZGVjYW1wL2NsaWVudC8uL25vZGVfbW9kdWxlcy9nZXQtZnVuYy1uYW1lL2luZGV4LmpzIiwid2VicGFjazovL0BmcmVlY29kZWNhbXAvY2xpZW50Ly4vbm9kZV9tb2R1bGVzL3BhdGh2YWwvaW5kZXguanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvdHlwZS1kZXRlY3QvdHlwZS1kZXRlY3QuanMiLCJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9ub2RlX21vZHVsZXMvY2hhaS9pbmRleC5tanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBhc3NlcnRpb24tZXJyb3JcbiAqIENvcHlyaWdodChjKSAyMDEzIEpha2UgTHVlciA8amFrZUBxdWFsaWFuY3kuY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyohXG4gKiBSZXR1cm4gYSBmdW5jdGlvbiB0aGF0IHdpbGwgY29weSBwcm9wZXJ0aWVzIGZyb21cbiAqIG9uZSBvYmplY3QgdG8gYW5vdGhlciBleGNsdWRpbmcgYW55IG9yaWdpbmFsbHlcbiAqIGxpc3RlZC4gUmV0dXJuZWQgZnVuY3Rpb24gd2lsbCBjcmVhdGUgYSBuZXcgYHt9YC5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gZXhjbHVkZWQgcHJvcGVydGllcyAuLi5cbiAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICovXG5cbmZ1bmN0aW9uIGV4Y2x1ZGUgKCkge1xuICB2YXIgZXhjbHVkZXMgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cyk7XG5cbiAgZnVuY3Rpb24gZXhjbHVkZVByb3BzIChyZXMsIG9iaikge1xuICAgIE9iamVjdC5rZXlzKG9iaikuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICBpZiAoIX5leGNsdWRlcy5pbmRleE9mKGtleSkpIHJlc1trZXldID0gb2JqW2tleV07XG4gICAgfSk7XG4gIH1cblxuICByZXR1cm4gZnVuY3Rpb24gZXh0ZW5kRXhjbHVkZSAoKSB7XG4gICAgdmFyIGFyZ3MgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cylcbiAgICAgICwgaSA9IDBcbiAgICAgICwgcmVzID0ge307XG5cbiAgICBmb3IgKDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICAgIGV4Y2x1ZGVQcm9wcyhyZXMsIGFyZ3NbaV0pO1xuICAgIH1cblxuICAgIHJldHVybiByZXM7XG4gIH07XG59O1xuXG4vKiFcbiAqIFByaW1hcnkgRXhwb3J0c1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gQXNzZXJ0aW9uRXJyb3I7XG5cbi8qKlxuICogIyMjIEFzc2VydGlvbkVycm9yXG4gKlxuICogQW4gZXh0ZW5zaW9uIG9mIHRoZSBKYXZhU2NyaXB0IGBFcnJvcmAgY29uc3RydWN0b3IgZm9yXG4gKiBhc3NlcnRpb24gYW5kIHZhbGlkYXRpb24gc2NlbmFyaW9zLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gKiBAcGFyYW0ge09iamVjdH0gcHJvcGVydGllcyB0byBpbmNsdWRlIChvcHRpb25hbClcbiAqIEBwYXJhbSB7Y2FsbGVlfSBzdGFydCBzdGFjayBmdW5jdGlvbiAob3B0aW9uYWwpXG4gKi9cblxuZnVuY3Rpb24gQXNzZXJ0aW9uRXJyb3IgKG1lc3NhZ2UsIF9wcm9wcywgc3NmKSB7XG4gIHZhciBleHRlbmQgPSBleGNsdWRlKCduYW1lJywgJ21lc3NhZ2UnLCAnc3RhY2snLCAnY29uc3RydWN0b3InLCAndG9KU09OJylcbiAgICAsIHByb3BzID0gZXh0ZW5kKF9wcm9wcyB8fCB7fSk7XG5cbiAgLy8gZGVmYXVsdCB2YWx1ZXNcbiAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZSB8fCAnVW5zcGVjaWZpZWQgQXNzZXJ0aW9uRXJyb3InO1xuICB0aGlzLnNob3dEaWZmID0gZmFsc2U7XG5cbiAgLy8gY29weSBmcm9tIHByb3BlcnRpZXNcbiAgZm9yICh2YXIga2V5IGluIHByb3BzKSB7XG4gICAgdGhpc1trZXldID0gcHJvcHNba2V5XTtcbiAgfVxuXG4gIC8vIGNhcHR1cmUgc3RhY2sgdHJhY2VcbiAgc3NmID0gc3NmIHx8IEFzc2VydGlvbkVycm9yO1xuICBpZiAoRXJyb3IuY2FwdHVyZVN0YWNrVHJhY2UpIHtcbiAgICBFcnJvci5jYXB0dXJlU3RhY2tUcmFjZSh0aGlzLCBzc2YpO1xuICB9IGVsc2Uge1xuICAgIHRyeSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoKTtcbiAgICB9IGNhdGNoKGUpIHtcbiAgICAgIHRoaXMuc3RhY2sgPSBlLnN0YWNrO1xuICAgIH1cbiAgfVxufVxuXG4vKiFcbiAqIEluaGVyaXQgZnJvbSBFcnJvci5wcm90b3R5cGVcbiAqL1xuXG5Bc3NlcnRpb25FcnJvci5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEVycm9yLnByb3RvdHlwZSk7XG5cbi8qIVxuICogU3RhdGljYWxseSBzZXQgbmFtZVxuICovXG5cbkFzc2VydGlvbkVycm9yLnByb3RvdHlwZS5uYW1lID0gJ0Fzc2VydGlvbkVycm9yJztcblxuLyohXG4gKiBFbnN1cmUgY29ycmVjdCBjb25zdHJ1Y3RvclxuICovXG5cbkFzc2VydGlvbkVycm9yLnByb3RvdHlwZS5jb25zdHJ1Y3RvciA9IEFzc2VydGlvbkVycm9yO1xuXG4vKipcbiAqIEFsbG93IGVycm9ycyB0byBiZSBjb252ZXJ0ZWQgdG8gSlNPTiBmb3Igc3RhdGljIHRyYW5zZmVyLlxuICpcbiAqIEBwYXJhbSB7Qm9vbGVhbn0gaW5jbHVkZSBzdGFjayAoZGVmYXVsdDogYHRydWVgKVxuICogQHJldHVybiB7T2JqZWN0fSBvYmplY3QgdGhhdCBjYW4gYmUgYEpTT04uc3RyaW5naWZ5YFxuICovXG5cbkFzc2VydGlvbkVycm9yLnByb3RvdHlwZS50b0pTT04gPSBmdW5jdGlvbiAoc3RhY2spIHtcbiAgdmFyIGV4dGVuZCA9IGV4Y2x1ZGUoJ2NvbnN0cnVjdG9yJywgJ3RvSlNPTicsICdzdGFjaycpXG4gICAgLCBwcm9wcyA9IGV4dGVuZCh7IG5hbWU6IHRoaXMubmFtZSB9LCB0aGlzKTtcblxuICAvLyBpbmNsdWRlIHN0YWNrIGlmIGV4aXN0cyBhbmQgbm90IHR1cm5lZCBvZmZcbiAgaWYgKGZhbHNlICE9PSBzdGFjayAmJiB0aGlzLnN0YWNrKSB7XG4gICAgcHJvcHMuc3RhY2sgPSB0aGlzLnN0YWNrO1xuICB9XG5cbiAgcmV0dXJuIHByb3BzO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9saWIvY2hhaScpO1xuIiwiLyohXG4gKiBjaGFpXG4gKiBDb3B5cmlnaHQoYykgMjAxMS0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxudmFyIHVzZWQgPSBbXTtcblxuLyohXG4gKiBDaGFpIHZlcnNpb25cbiAqL1xuXG5leHBvcnRzLnZlcnNpb24gPSAnNC4zLjMnO1xuXG4vKiFcbiAqIEFzc2VydGlvbiBFcnJvclxuICovXG5cbmV4cG9ydHMuQXNzZXJ0aW9uRXJyb3IgPSByZXF1aXJlKCdhc3NlcnRpb24tZXJyb3InKTtcblxuLyohXG4gKiBVdGlscyBmb3IgcGx1Z2lucyAobm90IGV4cG9ydGVkKVxuICovXG5cbnZhciB1dGlsID0gcmVxdWlyZSgnLi9jaGFpL3V0aWxzJyk7XG5cbi8qKlxuICogIyAudXNlKGZ1bmN0aW9uKVxuICpcbiAqIFByb3ZpZGVzIGEgd2F5IHRvIGV4dGVuZCB0aGUgaW50ZXJuYWxzIG9mIENoYWkuXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn1cbiAqIEByZXR1cm5zIHt0aGlzfSBmb3IgY2hhaW5pbmdcbiAqIEBhcGkgcHVibGljXG4gKi9cblxuZXhwb3J0cy51c2UgPSBmdW5jdGlvbiAoZm4pIHtcbiAgaWYgKCF+dXNlZC5pbmRleE9mKGZuKSkge1xuICAgIGZuKGV4cG9ydHMsIHV0aWwpO1xuICAgIHVzZWQucHVzaChmbik7XG4gIH1cblxuICByZXR1cm4gZXhwb3J0cztcbn07XG5cbi8qIVxuICogVXRpbGl0eSBGdW5jdGlvbnNcbiAqL1xuXG5leHBvcnRzLnV0aWwgPSB1dGlsO1xuXG4vKiFcbiAqIENvbmZpZ3VyYXRpb25cbiAqL1xuXG52YXIgY29uZmlnID0gcmVxdWlyZSgnLi9jaGFpL2NvbmZpZycpO1xuZXhwb3J0cy5jb25maWcgPSBjb25maWc7XG5cbi8qIVxuICogUHJpbWFyeSBgQXNzZXJ0aW9uYCBwcm90b3R5cGVcbiAqL1xuXG52YXIgYXNzZXJ0aW9uID0gcmVxdWlyZSgnLi9jaGFpL2Fzc2VydGlvbicpO1xuZXhwb3J0cy51c2UoYXNzZXJ0aW9uKTtcblxuLyohXG4gKiBDb3JlIEFzc2VydGlvbnNcbiAqL1xuXG52YXIgY29yZSA9IHJlcXVpcmUoJy4vY2hhaS9jb3JlL2Fzc2VydGlvbnMnKTtcbmV4cG9ydHMudXNlKGNvcmUpO1xuXG4vKiFcbiAqIEV4cGVjdCBpbnRlcmZhY2VcbiAqL1xuXG52YXIgZXhwZWN0ID0gcmVxdWlyZSgnLi9jaGFpL2ludGVyZmFjZS9leHBlY3QnKTtcbmV4cG9ydHMudXNlKGV4cGVjdCk7XG5cbi8qIVxuICogU2hvdWxkIGludGVyZmFjZVxuICovXG5cbnZhciBzaG91bGQgPSByZXF1aXJlKCcuL2NoYWkvaW50ZXJmYWNlL3Nob3VsZCcpO1xuZXhwb3J0cy51c2Uoc2hvdWxkKTtcblxuLyohXG4gKiBBc3NlcnQgaW50ZXJmYWNlXG4gKi9cblxudmFyIGFzc2VydCA9IHJlcXVpcmUoJy4vY2hhaS9pbnRlcmZhY2UvYXNzZXJ0Jyk7XG5leHBvcnRzLnVzZShhc3NlcnQpO1xuIiwiLyohXG4gKiBjaGFpXG4gKiBodHRwOi8vY2hhaWpzLmNvbVxuICogQ29weXJpZ2h0KGMpIDIwMTEtMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbnZhciBjb25maWcgPSByZXF1aXJlKCcuL2NvbmZpZycpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChfY2hhaSwgdXRpbCkge1xuICAvKiFcbiAgICogTW9kdWxlIGRlcGVuZGVuY2llcy5cbiAgICovXG5cbiAgdmFyIEFzc2VydGlvbkVycm9yID0gX2NoYWkuQXNzZXJ0aW9uRXJyb3JcbiAgICAsIGZsYWcgPSB1dGlsLmZsYWc7XG5cbiAgLyohXG4gICAqIE1vZHVsZSBleHBvcnQuXG4gICAqL1xuXG4gIF9jaGFpLkFzc2VydGlvbiA9IEFzc2VydGlvbjtcblxuICAvKiFcbiAgICogQXNzZXJ0aW9uIENvbnN0cnVjdG9yXG4gICAqXG4gICAqIENyZWF0ZXMgb2JqZWN0IGZvciBjaGFpbmluZy5cbiAgICpcbiAgICogYEFzc2VydGlvbmAgb2JqZWN0cyBjb250YWluIG1ldGFkYXRhIGluIHRoZSBmb3JtIG9mIGZsYWdzLiBUaHJlZSBmbGFncyBjYW5cbiAgICogYmUgYXNzaWduZWQgZHVyaW5nIGluc3RhbnRpYXRpb24gYnkgcGFzc2luZyBhcmd1bWVudHMgdG8gdGhpcyBjb25zdHJ1Y3RvcjpcbiAgICpcbiAgICogLSBgb2JqZWN0YDogVGhpcyBmbGFnIGNvbnRhaW5zIHRoZSB0YXJnZXQgb2YgdGhlIGFzc2VydGlvbi4gRm9yIGV4YW1wbGUsIGluXG4gICAqICAgdGhlIGFzc2VydGlvbiBgZXhwZWN0KG51bUtpdHRlbnMpLnRvLmVxdWFsKDcpO2AsIHRoZSBgb2JqZWN0YCBmbGFnIHdpbGxcbiAgICogICBjb250YWluIGBudW1LaXR0ZW5zYCBzbyB0aGF0IHRoZSBgZXF1YWxgIGFzc2VydGlvbiBjYW4gcmVmZXJlbmNlIGl0IHdoZW5cbiAgICogICBuZWVkZWQuXG4gICAqXG4gICAqIC0gYG1lc3NhZ2VgOiBUaGlzIGZsYWcgY29udGFpbnMgYW4gb3B0aW9uYWwgY3VzdG9tIGVycm9yIG1lc3NhZ2UgdG8gYmVcbiAgICogICBwcmVwZW5kZWQgdG8gdGhlIGVycm9yIG1lc3NhZ2UgdGhhdCdzIGdlbmVyYXRlZCBieSB0aGUgYXNzZXJ0aW9uIHdoZW4gaXRcbiAgICogICBmYWlscy5cbiAgICpcbiAgICogLSBgc3NmaWA6IFRoaXMgZmxhZyBzdGFuZHMgZm9yIFwic3RhcnQgc3RhY2sgZnVuY3Rpb24gaW5kaWNhdG9yXCIuIEl0XG4gICAqICAgY29udGFpbnMgYSBmdW5jdGlvbiByZWZlcmVuY2UgdGhhdCBzZXJ2ZXMgYXMgdGhlIHN0YXJ0aW5nIHBvaW50IGZvclxuICAgKiAgIHJlbW92aW5nIGZyYW1lcyBmcm9tIHRoZSBzdGFjayB0cmFjZSBvZiB0aGUgZXJyb3IgdGhhdCdzIGNyZWF0ZWQgYnkgdGhlXG4gICAqICAgYXNzZXJ0aW9uIHdoZW4gaXQgZmFpbHMuIFRoZSBnb2FsIGlzIHRvIHByb3ZpZGUgYSBjbGVhbmVyIHN0YWNrIHRyYWNlIHRvXG4gICAqICAgZW5kIHVzZXJzIGJ5IHJlbW92aW5nIENoYWkncyBpbnRlcm5hbCBmdW5jdGlvbnMuIE5vdGUgdGhhdCBpdCBvbmx5IHdvcmtzXG4gICAqICAgaW4gZW52aXJvbm1lbnRzIHRoYXQgc3VwcG9ydCBgRXJyb3IuY2FwdHVyZVN0YWNrVHJhY2VgLCBhbmQgb25seSB3aGVuXG4gICAqICAgYENoYWkuY29uZmlnLmluY2x1ZGVTdGFja2AgaGFzbid0IGJlZW4gc2V0IHRvIGBmYWxzZWAuXG4gICAqXG4gICAqIC0gYGxvY2tTc2ZpYDogVGhpcyBmbGFnIGNvbnRyb2xzIHdoZXRoZXIgb3Igbm90IHRoZSBnaXZlbiBgc3NmaWAgZmxhZ1xuICAgKiAgIHNob3VsZCByZXRhaW4gaXRzIGN1cnJlbnQgdmFsdWUsIGV2ZW4gYXMgYXNzZXJ0aW9ucyBhcmUgY2hhaW5lZCBvZmYgb2ZcbiAgICogICB0aGlzIG9iamVjdC4gVGhpcyBpcyB1c3VhbGx5IHNldCB0byBgdHJ1ZWAgd2hlbiBjcmVhdGluZyBhIG5ldyBhc3NlcnRpb25cbiAgICogICBmcm9tIHdpdGhpbiBhbm90aGVyIGFzc2VydGlvbi4gSXQncyBhbHNvIHRlbXBvcmFyaWx5IHNldCB0byBgdHJ1ZWAgYmVmb3JlXG4gICAqICAgYW4gb3ZlcndyaXR0ZW4gYXNzZXJ0aW9uIGdldHMgY2FsbGVkIGJ5IHRoZSBvdmVyd3JpdGluZyBhc3NlcnRpb24uXG4gICAqXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG9iaiB0YXJnZXQgb2YgdGhlIGFzc2VydGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIChvcHRpb25hbCkgY3VzdG9tIGVycm9yIG1lc3NhZ2VcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gc3NmaSAob3B0aW9uYWwpIHN0YXJ0aW5nIHBvaW50IGZvciByZW1vdmluZyBzdGFjayBmcmFtZXNcbiAgICogQHBhcmFtIHtCb29sZWFufSBsb2NrU3NmaSAob3B0aW9uYWwpIHdoZXRoZXIgb3Igbm90IHRoZSBzc2ZpIGZsYWcgaXMgbG9ja2VkXG4gICAqIEBhcGkgcHJpdmF0ZVxuICAgKi9cblxuICBmdW5jdGlvbiBBc3NlcnRpb24gKG9iaiwgbXNnLCBzc2ZpLCBsb2NrU3NmaSkge1xuICAgIGZsYWcodGhpcywgJ3NzZmknLCBzc2ZpIHx8IEFzc2VydGlvbik7XG4gICAgZmxhZyh0aGlzLCAnbG9ja1NzZmknLCBsb2NrU3NmaSk7XG4gICAgZmxhZyh0aGlzLCAnb2JqZWN0Jywgb2JqKTtcbiAgICBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcblxuICAgIHJldHVybiB1dGlsLnByb3hpZnkodGhpcyk7XG4gIH1cblxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkoQXNzZXJ0aW9uLCAnaW5jbHVkZVN0YWNrJywge1xuICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0Fzc2VydGlvbi5pbmNsdWRlU3RhY2sgaXMgZGVwcmVjYXRlZCwgdXNlIGNoYWkuY29uZmlnLmluY2x1ZGVTdGFjayBpbnN0ZWFkLicpO1xuICAgICAgcmV0dXJuIGNvbmZpZy5pbmNsdWRlU3RhY2s7XG4gICAgfSxcbiAgICBzZXQ6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0Fzc2VydGlvbi5pbmNsdWRlU3RhY2sgaXMgZGVwcmVjYXRlZCwgdXNlIGNoYWkuY29uZmlnLmluY2x1ZGVTdGFjayBpbnN0ZWFkLicpO1xuICAgICAgY29uZmlnLmluY2x1ZGVTdGFjayA9IHZhbHVlO1xuICAgIH1cbiAgfSk7XG5cbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KEFzc2VydGlvbiwgJ3Nob3dEaWZmJywge1xuICAgIGdldDogZnVuY3Rpb24oKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0Fzc2VydGlvbi5zaG93RGlmZiBpcyBkZXByZWNhdGVkLCB1c2UgY2hhaS5jb25maWcuc2hvd0RpZmYgaW5zdGVhZC4nKTtcbiAgICAgIHJldHVybiBjb25maWcuc2hvd0RpZmY7XG4gICAgfSxcbiAgICBzZXQ6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0Fzc2VydGlvbi5zaG93RGlmZiBpcyBkZXByZWNhdGVkLCB1c2UgY2hhaS5jb25maWcuc2hvd0RpZmYgaW5zdGVhZC4nKTtcbiAgICAgIGNvbmZpZy5zaG93RGlmZiA9IHZhbHVlO1xuICAgIH1cbiAgfSk7XG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5ID0gZnVuY3Rpb24gKG5hbWUsIGZuKSB7XG4gICAgdXRpbC5hZGRQcm9wZXJ0eSh0aGlzLnByb3RvdHlwZSwgbmFtZSwgZm4pO1xuICB9O1xuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QgPSBmdW5jdGlvbiAobmFtZSwgZm4pIHtcbiAgICB1dGlsLmFkZE1ldGhvZCh0aGlzLnByb3RvdHlwZSwgbmFtZSwgZm4pO1xuICB9O1xuXG4gIEFzc2VydGlvbi5hZGRDaGFpbmFibGVNZXRob2QgPSBmdW5jdGlvbiAobmFtZSwgZm4sIGNoYWluaW5nQmVoYXZpb3IpIHtcbiAgICB1dGlsLmFkZENoYWluYWJsZU1ldGhvZCh0aGlzLnByb3RvdHlwZSwgbmFtZSwgZm4sIGNoYWluaW5nQmVoYXZpb3IpO1xuICB9O1xuXG4gIEFzc2VydGlvbi5vdmVyd3JpdGVQcm9wZXJ0eSA9IGZ1bmN0aW9uIChuYW1lLCBmbikge1xuICAgIHV0aWwub3ZlcndyaXRlUHJvcGVydHkodGhpcy5wcm90b3R5cGUsIG5hbWUsIGZuKTtcbiAgfTtcblxuICBBc3NlcnRpb24ub3ZlcndyaXRlTWV0aG9kID0gZnVuY3Rpb24gKG5hbWUsIGZuKSB7XG4gICAgdXRpbC5vdmVyd3JpdGVNZXRob2QodGhpcy5wcm90b3R5cGUsIG5hbWUsIGZuKTtcbiAgfTtcblxuICBBc3NlcnRpb24ub3ZlcndyaXRlQ2hhaW5hYmxlTWV0aG9kID0gZnVuY3Rpb24gKG5hbWUsIGZuLCBjaGFpbmluZ0JlaGF2aW9yKSB7XG4gICAgdXRpbC5vdmVyd3JpdGVDaGFpbmFibGVNZXRob2QodGhpcy5wcm90b3R5cGUsIG5hbWUsIGZuLCBjaGFpbmluZ0JlaGF2aW9yKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5hc3NlcnQoZXhwcmVzc2lvbiwgbWVzc2FnZSwgbmVnYXRlTWVzc2FnZSwgZXhwZWN0ZWQsIGFjdHVhbCwgc2hvd0RpZmYpXG4gICAqXG4gICAqIEV4ZWN1dGVzIGFuIGV4cHJlc3Npb24gYW5kIGNoZWNrIGV4cGVjdGF0aW9ucy4gVGhyb3dzIEFzc2VydGlvbkVycm9yIGZvciByZXBvcnRpbmcgaWYgdGVzdCBkb2Vzbid0IHBhc3MuXG4gICAqXG4gICAqIEBuYW1lIGFzc2VydFxuICAgKiBAcGFyYW0ge1BoaWxvc29waGljYWx9IGV4cHJlc3Npb24gdG8gYmUgdGVzdGVkXG4gICAqIEBwYXJhbSB7U3RyaW5nfEZ1bmN0aW9ufSBtZXNzYWdlIG9yIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBtZXNzYWdlIHRvIGRpc3BsYXkgaWYgZXhwcmVzc2lvbiBmYWlsc1xuICAgKiBAcGFyYW0ge1N0cmluZ3xGdW5jdGlvbn0gbmVnYXRlZE1lc3NhZ2Ugb3IgZnVuY3Rpb24gdGhhdCByZXR1cm5zIG5lZ2F0ZWRNZXNzYWdlIHRvIGRpc3BsYXkgaWYgbmVnYXRlZCBleHByZXNzaW9uIGZhaWxzXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGV4cGVjdGVkIHZhbHVlIChyZW1lbWJlciB0byBjaGVjayBmb3IgbmVnYXRpb24pXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGFjdHVhbCAob3B0aW9uYWwpIHdpbGwgZGVmYXVsdCB0byBgdGhpcy5vYmpgXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gc2hvd0RpZmYgKG9wdGlvbmFsKSB3aGVuIHNldCB0byBgdHJ1ZWAsIGFzc2VydCB3aWxsIGRpc3BsYXkgYSBkaWZmIGluIGFkZGl0aW9uIHRvIHRoZSBtZXNzYWdlIGlmIGV4cHJlc3Npb24gZmFpbHNcbiAgICogQGFwaSBwcml2YXRlXG4gICAqL1xuXG4gIEFzc2VydGlvbi5wcm90b3R5cGUuYXNzZXJ0ID0gZnVuY3Rpb24gKGV4cHIsIG1zZywgbmVnYXRlTXNnLCBleHBlY3RlZCwgX2FjdHVhbCwgc2hvd0RpZmYpIHtcbiAgICB2YXIgb2sgPSB1dGlsLnRlc3QodGhpcywgYXJndW1lbnRzKTtcbiAgICBpZiAoZmFsc2UgIT09IHNob3dEaWZmKSBzaG93RGlmZiA9IHRydWU7XG4gICAgaWYgKHVuZGVmaW5lZCA9PT0gZXhwZWN0ZWQgJiYgdW5kZWZpbmVkID09PSBfYWN0dWFsKSBzaG93RGlmZiA9IGZhbHNlO1xuICAgIGlmICh0cnVlICE9PSBjb25maWcuc2hvd0RpZmYpIHNob3dEaWZmID0gZmFsc2U7XG5cbiAgICBpZiAoIW9rKSB7XG4gICAgICBtc2cgPSB1dGlsLmdldE1lc3NhZ2UodGhpcywgYXJndW1lbnRzKTtcbiAgICAgIHZhciBhY3R1YWwgPSB1dGlsLmdldEFjdHVhbCh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgdmFyIGFzc2VydGlvbkVycm9yT2JqZWN0UHJvcGVydGllcyA9IHtcbiAgICAgICAgICBhY3R1YWw6IGFjdHVhbFxuICAgICAgICAsIGV4cGVjdGVkOiBleHBlY3RlZFxuICAgICAgICAsIHNob3dEaWZmOiBzaG93RGlmZlxuICAgICAgfTtcblxuICAgICAgdmFyIG9wZXJhdG9yID0gdXRpbC5nZXRPcGVyYXRvcih0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgaWYgKG9wZXJhdG9yKSB7XG4gICAgICAgIGFzc2VydGlvbkVycm9yT2JqZWN0UHJvcGVydGllcy5vcGVyYXRvciA9IG9wZXJhdG9yO1xuICAgICAgfVxuXG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoXG4gICAgICAgIG1zZyxcbiAgICAgICAgYXNzZXJ0aW9uRXJyb3JPYmplY3RQcm9wZXJ0aWVzLFxuICAgICAgICAoY29uZmlnLmluY2x1ZGVTdGFjaykgPyB0aGlzLmFzc2VydCA6IGZsYWcodGhpcywgJ3NzZmknKSk7XG4gICAgfVxuICB9O1xuXG4gIC8qIVxuICAgKiAjIyMgLl9vYmpcbiAgICpcbiAgICogUXVpY2sgcmVmZXJlbmNlIHRvIHN0b3JlZCBgYWN0dWFsYCB2YWx1ZSBmb3IgcGx1Z2luIGRldmVsb3BlcnMuXG4gICAqXG4gICAqIEBhcGkgcHJpdmF0ZVxuICAgKi9cblxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkoQXNzZXJ0aW9uLnByb3RvdHlwZSwgJ19vYmonLFxuICAgIHsgZ2V0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBmbGFnKHRoaXMsICdvYmplY3QnKTtcbiAgICAgIH1cbiAgICAsIHNldDogZnVuY3Rpb24gKHZhbCkge1xuICAgICAgICBmbGFnKHRoaXMsICdvYmplY3QnLCB2YWwpO1xuICAgICAgfVxuICB9KTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHtcblxuICAvKipcbiAgICogIyMjIGNvbmZpZy5pbmNsdWRlU3RhY2tcbiAgICpcbiAgICogVXNlciBjb25maWd1cmFibGUgcHJvcGVydHksIGluZmx1ZW5jZXMgd2hldGhlciBzdGFjayB0cmFjZVxuICAgKiBpcyBpbmNsdWRlZCBpbiBBc3NlcnRpb24gZXJyb3IgbWVzc2FnZS4gRGVmYXVsdCBvZiBmYWxzZVxuICAgKiBzdXBwcmVzc2VzIHN0YWNrIHRyYWNlIGluIHRoZSBlcnJvciBtZXNzYWdlLlxuICAgKlxuICAgKiAgICAgY2hhaS5jb25maWcuaW5jbHVkZVN0YWNrID0gdHJ1ZTsgIC8vIGVuYWJsZSBzdGFjayBvbiBlcnJvclxuICAgKlxuICAgKiBAcGFyYW0ge0Jvb2xlYW59XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGluY2x1ZGVTdGFjazogZmFsc2UsXG5cbiAgLyoqXG4gICAqICMjIyBjb25maWcuc2hvd0RpZmZcbiAgICpcbiAgICogVXNlciBjb25maWd1cmFibGUgcHJvcGVydHksIGluZmx1ZW5jZXMgd2hldGhlciBvciBub3RcbiAgICogdGhlIGBzaG93RGlmZmAgZmxhZyBzaG91bGQgYmUgaW5jbHVkZWQgaW4gdGhlIHRocm93blxuICAgKiBBc3NlcnRpb25FcnJvcnMuIGBmYWxzZWAgd2lsbCBhbHdheXMgYmUgYGZhbHNlYDsgYHRydWVgXG4gICAqIHdpbGwgYmUgdHJ1ZSB3aGVuIHRoZSBhc3NlcnRpb24gaGFzIHJlcXVlc3RlZCBhIGRpZmZcbiAgICogYmUgc2hvd24uXG4gICAqXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn1cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgc2hvd0RpZmY6IHRydWUsXG5cbiAgLyoqXG4gICAqICMjIyBjb25maWcudHJ1bmNhdGVUaHJlc2hvbGRcbiAgICpcbiAgICogVXNlciBjb25maWd1cmFibGUgcHJvcGVydHksIHNldHMgbGVuZ3RoIHRocmVzaG9sZCBmb3IgYWN0dWFsIGFuZFxuICAgKiBleHBlY3RlZCB2YWx1ZXMgaW4gYXNzZXJ0aW9uIGVycm9ycy4gSWYgdGhpcyB0aHJlc2hvbGQgaXMgZXhjZWVkZWQsIGZvclxuICAgKiBleGFtcGxlIGZvciBsYXJnZSBkYXRhIHN0cnVjdHVyZXMsIHRoZSB2YWx1ZSBpcyByZXBsYWNlZCB3aXRoIHNvbWV0aGluZ1xuICAgKiBsaWtlIGBbIEFycmF5KDMpIF1gIG9yIGB7IE9iamVjdCAocHJvcDEsIHByb3AyKSB9YC5cbiAgICpcbiAgICogU2V0IGl0IHRvIHplcm8gaWYgeW91IHdhbnQgdG8gZGlzYWJsZSB0cnVuY2F0aW5nIGFsdG9nZXRoZXIuXG4gICAqXG4gICAqIFRoaXMgaXMgZXNwZWNpYWxseSB1c2VyZnVsIHdoZW4gZG9pbmcgYXNzZXJ0aW9ucyBvbiBhcnJheXM6IGhhdmluZyB0aGlzXG4gICAqIHNldCB0byBhIHJlYXNvbmFibGUgbGFyZ2UgdmFsdWUgbWFrZXMgdGhlIGZhaWx1cmUgbWVzc2FnZXMgcmVhZGlseVxuICAgKiBpbnNwZWN0YWJsZS5cbiAgICpcbiAgICogICAgIGNoYWkuY29uZmlnLnRydW5jYXRlVGhyZXNob2xkID0gMDsgIC8vIGRpc2FibGUgdHJ1bmNhdGluZ1xuICAgKlxuICAgKiBAcGFyYW0ge051bWJlcn1cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgdHJ1bmNhdGVUaHJlc2hvbGQ6IDQwLFxuXG4gIC8qKlxuICAgKiAjIyMgY29uZmlnLnVzZVByb3h5XG4gICAqXG4gICAqIFVzZXIgY29uZmlndXJhYmxlIHByb3BlcnR5LCBkZWZpbmVzIGlmIGNoYWkgd2lsbCB1c2UgYSBQcm94eSB0byB0aHJvd1xuICAgKiBhbiBlcnJvciB3aGVuIGEgbm9uLWV4aXN0ZW50IHByb3BlcnR5IGlzIHJlYWQsIHdoaWNoIHByb3RlY3RzIHVzZXJzXG4gICAqIGZyb20gdHlwb3Mgd2hlbiB1c2luZyBwcm9wZXJ0eS1iYXNlZCBhc3NlcnRpb25zLlxuICAgKlxuICAgKiBTZXQgaXQgdG8gZmFsc2UgaWYgeW91IHdhbnQgdG8gZGlzYWJsZSB0aGlzIGZlYXR1cmUuXG4gICAqXG4gICAqICAgICBjaGFpLmNvbmZpZy51c2VQcm94eSA9IGZhbHNlOyAgLy8gZGlzYWJsZSB1c2Ugb2YgUHJveHlcbiAgICpcbiAgICogVGhpcyBmZWF0dXJlIGlzIGF1dG9tYXRpY2FsbHkgZGlzYWJsZWQgcmVnYXJkbGVzcyBvZiB0aGlzIGNvbmZpZyB2YWx1ZVxuICAgKiBpbiBlbnZpcm9ubWVudHMgdGhhdCBkb24ndCBzdXBwb3J0IHByb3hpZXMuXG4gICAqXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn1cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgdXNlUHJveHk6IHRydWUsXG5cbiAgLyoqXG4gICAqICMjIyBjb25maWcucHJveHlFeGNsdWRlZEtleXNcbiAgICpcbiAgICogVXNlciBjb25maWd1cmFibGUgcHJvcGVydHksIGRlZmluZXMgd2hpY2ggcHJvcGVydGllcyBzaG91bGQgYmUgaWdub3JlZFxuICAgKiBpbnN0ZWFkIG9mIHRocm93aW5nIGFuIGVycm9yIGlmIHRoZXkgZG8gbm90IGV4aXN0IG9uIHRoZSBhc3NlcnRpb24uXG4gICAqIFRoaXMgaXMgb25seSBhcHBsaWVkIGlmIHRoZSBlbnZpcm9ubWVudCBDaGFpIGlzIHJ1bm5pbmcgaW4gc3VwcG9ydHMgcHJveGllcyBhbmRcbiAgICogaWYgdGhlIGB1c2VQcm94eWAgY29uZmlndXJhdGlvbiBzZXR0aW5nIGlzIGVuYWJsZWQuXG4gICAqIEJ5IGRlZmF1bHQsIGB0aGVuYCBhbmQgYGluc3BlY3RgIHdpbGwgbm90IHRocm93IGFuIGVycm9yIGlmIHRoZXkgZG8gbm90IGV4aXN0IG9uIHRoZVxuICAgKiBhc3NlcnRpb24gb2JqZWN0IGJlY2F1c2UgdGhlIGAuaW5zcGVjdGAgcHJvcGVydHkgaXMgcmVhZCBieSBgdXRpbC5pbnNwZWN0YCAoZm9yIGV4YW1wbGUsIHdoZW5cbiAgICogdXNpbmcgYGNvbnNvbGUubG9nYCBvbiB0aGUgYXNzZXJ0aW9uIG9iamVjdCkgYW5kIGAudGhlbmAgaXMgbmVjZXNzYXJ5IGZvciBwcm9taXNlIHR5cGUtY2hlY2tpbmcuXG4gICAqXG4gICAqICAgICAvLyBCeSBkZWZhdWx0IHRoZXNlIGtleXMgd2lsbCBub3QgdGhyb3cgYW4gZXJyb3IgaWYgdGhleSBkbyBub3QgZXhpc3Qgb24gdGhlIGFzc2VydGlvbiBvYmplY3RcbiAgICogICAgIGNoYWkuY29uZmlnLnByb3h5RXhjbHVkZWRLZXlzID0gWyd0aGVuJywgJ2luc3BlY3QnXTtcbiAgICpcbiAgICogQHBhcmFtIHtBcnJheX1cbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgcHJveHlFeGNsdWRlZEtleXM6IFsndGhlbicsICdjYXRjaCcsICdpbnNwZWN0JywgJ3RvSlNPTiddXG59O1xuIiwiLyohXG4gKiBjaGFpXG4gKiBodHRwOi8vY2hhaWpzLmNvbVxuICogQ29weXJpZ2h0KGMpIDIwMTEtMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNoYWksIF8pIHtcbiAgdmFyIEFzc2VydGlvbiA9IGNoYWkuQXNzZXJ0aW9uXG4gICAgLCBBc3NlcnRpb25FcnJvciA9IGNoYWkuQXNzZXJ0aW9uRXJyb3JcbiAgICAsIGZsYWcgPSBfLmZsYWc7XG5cbiAgLyoqXG4gICAqICMjIyBMYW5ndWFnZSBDaGFpbnNcbiAgICpcbiAgICogVGhlIGZvbGxvd2luZyBhcmUgcHJvdmlkZWQgYXMgY2hhaW5hYmxlIGdldHRlcnMgdG8gaW1wcm92ZSB0aGUgcmVhZGFiaWxpdHlcbiAgICogb2YgeW91ciBhc3NlcnRpb25zLlxuICAgKlxuICAgKiAqKkNoYWlucyoqXG4gICAqXG4gICAqIC0gdG9cbiAgICogLSBiZVxuICAgKiAtIGJlZW5cbiAgICogLSBpc1xuICAgKiAtIHRoYXRcbiAgICogLSB3aGljaFxuICAgKiAtIGFuZFxuICAgKiAtIGhhc1xuICAgKiAtIGhhdmVcbiAgICogLSB3aXRoXG4gICAqIC0gYXRcbiAgICogLSBvZlxuICAgKiAtIHNhbWVcbiAgICogLSBidXRcbiAgICogLSBkb2VzXG4gICAqIC0gc3RpbGxcbiAgICogLSBhbHNvXG4gICAqXG4gICAqIEBuYW1lIGxhbmd1YWdlIGNoYWluc1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBbICd0bycsICdiZScsICdiZWVuJywgJ2lzJ1xuICAsICdhbmQnLCAnaGFzJywgJ2hhdmUnLCAnd2l0aCdcbiAgLCAndGhhdCcsICd3aGljaCcsICdhdCcsICdvZidcbiAgLCAnc2FtZScsICdidXQnLCAnZG9lcycsICdzdGlsbCcsIFwiYWxzb1wiIF0uZm9yRWFjaChmdW5jdGlvbiAoY2hhaW4pIHtcbiAgICBBc3NlcnRpb24uYWRkUHJvcGVydHkoY2hhaW4pO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5ub3RcbiAgICpcbiAgICogTmVnYXRlcyBhbGwgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4uXG4gICAqXG4gICAqICAgICBleHBlY3QoZnVuY3Rpb24gKCkge30pLnRvLm5vdC50aHJvdygpO1xuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ubm90LmhhdmUucHJvcGVydHkoJ2InKTtcbiAgICogICAgIGV4cGVjdChbMSwgMl0pLnRvLmJlLmFuKCdhcnJheScpLnRoYXQuZG9lcy5ub3QuaW5jbHVkZSgzKTtcbiAgICpcbiAgICogSnVzdCBiZWNhdXNlIHlvdSBjYW4gbmVnYXRlIGFueSBhc3NlcnRpb24gd2l0aCBgLm5vdGAgZG9lc24ndCBtZWFuIHlvdVxuICAgKiBzaG91bGQuIFdpdGggZ3JlYXQgcG93ZXIgY29tZXMgZ3JlYXQgcmVzcG9uc2liaWxpdHkuIEl0J3Mgb2Z0ZW4gYmVzdCB0b1xuICAgKiBhc3NlcnQgdGhhdCB0aGUgb25lIGV4cGVjdGVkIG91dHB1dCB3YXMgcHJvZHVjZWQsIHJhdGhlciB0aGFuIGFzc2VydGluZ1xuICAgKiB0aGF0IG9uZSBvZiBjb3VudGxlc3MgdW5leHBlY3RlZCBvdXRwdXRzIHdhc24ndCBwcm9kdWNlZC4gU2VlIGluZGl2aWR1YWxcbiAgICogYXNzZXJ0aW9ucyBmb3Igc3BlY2lmaWMgZ3VpZGFuY2UuXG4gICAqXG4gICAqICAgICBleHBlY3QoMikudG8uZXF1YWwoMik7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMikudG8ubm90LmVxdWFsKDEpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQG5hbWUgbm90XG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnbm90JywgZnVuY3Rpb24gKCkge1xuICAgIGZsYWcodGhpcywgJ25lZ2F0ZScsIHRydWUpO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5kZWVwXG4gICAqXG4gICAqIENhdXNlcyBhbGwgYC5lcXVhbGAsIGAuaW5jbHVkZWAsIGAubWVtYmVyc2AsIGAua2V5c2AsIGFuZCBgLnByb3BlcnR5YFxuICAgKiBhc3NlcnRpb25zIHRoYXQgZm9sbG93IGluIHRoZSBjaGFpbiB0byB1c2UgZGVlcCBlcXVhbGl0eSBpbnN0ZWFkIG9mIHN0cmljdFxuICAgKiAoYD09PWApIGVxdWFsaXR5LiBTZWUgdGhlIGBkZWVwLWVxbGAgcHJvamVjdCBwYWdlIGZvciBpbmZvIG9uIHRoZSBkZWVwXG4gICAqIGVxdWFsaXR5IGFsZ29yaXRobTogaHR0cHM6Ly9naXRodWIuY29tL2NoYWlqcy9kZWVwLWVxbC5cbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBvYmplY3QgZGVlcGx5IChidXQgbm90IHN0cmljdGx5KSBlcXVhbHMgYHthOiAxfWBcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmRlZXAuZXF1YWwoe2E6IDF9KTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLm5vdC5lcXVhbCh7YTogMX0pO1xuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IGFycmF5IGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgaW5jbHVkZXMgYHthOiAxfWBcbiAgICogICAgIGV4cGVjdChbe2E6IDF9XSkudG8uZGVlcC5pbmNsdWRlKHthOiAxfSk7XG4gICAqICAgICBleHBlY3QoW3thOiAxfV0pLnRvLm5vdC5pbmNsdWRlKHthOiAxfSk7XG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgb2JqZWN0IGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgaW5jbHVkZXMgYHg6IHthOiAxfWBcbiAgICogICAgIGV4cGVjdCh7eDoge2E6IDF9fSkudG8uZGVlcC5pbmNsdWRlKHt4OiB7YTogMX19KTtcbiAgICogICAgIGV4cGVjdCh7eDoge2E6IDF9fSkudG8ubm90LmluY2x1ZGUoe3g6IHthOiAxfX0pO1xuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IGFycmF5IGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgaGFzIG1lbWJlciBge2E6IDF9YFxuICAgKiAgICAgZXhwZWN0KFt7YTogMX1dKS50by5oYXZlLmRlZXAubWVtYmVycyhbe2E6IDF9XSk7XG4gICAqICAgICBleHBlY3QoW3thOiAxfV0pLnRvLm5vdC5oYXZlLm1lbWJlcnMoW3thOiAxfV0pO1xuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IHNldCBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGhhcyBrZXkgYHthOiAxfWBcbiAgICogICAgIGV4cGVjdChuZXcgU2V0KFt7YTogMX1dKSkudG8uaGF2ZS5kZWVwLmtleXMoW3thOiAxfV0pO1xuICAgKiAgICAgZXhwZWN0KG5ldyBTZXQoW3thOiAxfV0pKS50by5ub3QuaGF2ZS5rZXlzKFt7YTogMX1dKTtcbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBvYmplY3QgZGVlcGx5IChidXQgbm90IHN0cmljdGx5KSBoYXMgcHJvcGVydHkgYHg6IHthOiAxfWBcbiAgICogICAgIGV4cGVjdCh7eDoge2E6IDF9fSkudG8uaGF2ZS5kZWVwLnByb3BlcnR5KCd4Jywge2E6IDF9KTtcbiAgICogICAgIGV4cGVjdCh7eDoge2E6IDF9fSkudG8ubm90LmhhdmUucHJvcGVydHkoJ3gnLCB7YTogMX0pO1xuICAgKlxuICAgKiBAbmFtZSBkZWVwXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnZGVlcCcsIGZ1bmN0aW9uICgpIHtcbiAgICBmbGFnKHRoaXMsICdkZWVwJywgdHJ1ZSk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLm5lc3RlZFxuICAgKlxuICAgKiBFbmFibGVzIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gaW4gYWxsIGAucHJvcGVydHlgIGFuZCBgLmluY2x1ZGVgXG4gICAqIGFzc2VydGlvbnMgdGhhdCBmb2xsb3cgaW4gdGhlIGNoYWluLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiB7YjogWyd4JywgJ3knXX19KS50by5oYXZlLm5lc3RlZC5wcm9wZXJ0eSgnYS5iWzFdJyk7XG4gICAqICAgICBleHBlY3Qoe2E6IHtiOiBbJ3gnLCAneSddfX0pLnRvLm5lc3RlZC5pbmNsdWRlKHsnYS5iWzFdJzogJ3knfSk7XG4gICAqXG4gICAqIElmIGAuYCBvciBgW11gIGFyZSBwYXJ0IG9mIGFuIGFjdHVhbCBwcm9wZXJ0eSBuYW1lLCB0aGV5IGNhbiBiZSBlc2NhcGVkIGJ5XG4gICAqIGFkZGluZyB0d28gYmFja3NsYXNoZXMgYmVmb3JlIHRoZW0uXG4gICAqXG4gICAqICAgICBleHBlY3QoeycuYSc6IHsnW2JdJzogJ3gnfX0pLnRvLmhhdmUubmVzdGVkLnByb3BlcnR5KCdcXFxcLmEuXFxcXFtiXFxcXF0nKTtcbiAgICogICAgIGV4cGVjdCh7Jy5hJzogeydbYl0nOiAneCd9fSkudG8ubmVzdGVkLmluY2x1ZGUoeydcXFxcLmEuXFxcXFtiXFxcXF0nOiAneCd9KTtcbiAgICpcbiAgICogYC5uZXN0ZWRgIGNhbm5vdCBiZSBjb21iaW5lZCB3aXRoIGAub3duYC5cbiAgICpcbiAgICogQG5hbWUgbmVzdGVkXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnbmVzdGVkJywgZnVuY3Rpb24gKCkge1xuICAgIGZsYWcodGhpcywgJ25lc3RlZCcsIHRydWUpO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5vd25cbiAgICpcbiAgICogQ2F1c2VzIGFsbCBgLnByb3BlcnR5YCBhbmQgYC5pbmNsdWRlYCBhc3NlcnRpb25zIHRoYXQgZm9sbG93IGluIHRoZSBjaGFpblxuICAgKiB0byBpZ25vcmUgaW5oZXJpdGVkIHByb3BlcnRpZXMuXG4gICAqXG4gICAqICAgICBPYmplY3QucHJvdG90eXBlLmIgPSAyO1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8uaGF2ZS5vd24ucHJvcGVydHkoJ2EnKTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUucHJvcGVydHkoJ2InKTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLm5vdC5oYXZlLm93bi5wcm9wZXJ0eSgnYicpO1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ub3duLmluY2x1ZGUoe2E6IDF9KTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmluY2x1ZGUoe2I6IDJ9KS5idXQubm90Lm93bi5pbmNsdWRlKHtiOiAyfSk7XG4gICAqXG4gICAqIGAub3duYCBjYW5ub3QgYmUgY29tYmluZWQgd2l0aCBgLm5lc3RlZGAuXG4gICAqXG4gICAqIEBuYW1lIG93blxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ293bicsIGZ1bmN0aW9uICgpIHtcbiAgICBmbGFnKHRoaXMsICdvd24nLCB0cnVlKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAub3JkZXJlZFxuICAgKlxuICAgKiBDYXVzZXMgYWxsIGAubWVtYmVyc2AgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG8gcmVxdWlyZSB0aGF0XG4gICAqIG1lbWJlcnMgYmUgaW4gdGhlIHNhbWUgb3JkZXIuXG4gICAqXG4gICAqICAgICBleHBlY3QoWzEsIDJdKS50by5oYXZlLm9yZGVyZWQubWVtYmVycyhbMSwgMl0pXG4gICAqICAgICAgIC5idXQubm90LmhhdmUub3JkZXJlZC5tZW1iZXJzKFsyLCAxXSk7XG4gICAqXG4gICAqIFdoZW4gYC5pbmNsdWRlYCBhbmQgYC5vcmRlcmVkYCBhcmUgY29tYmluZWQsIHRoZSBvcmRlcmluZyBiZWdpbnMgYXQgdGhlXG4gICAqIHN0YXJ0IG9mIGJvdGggYXJyYXlzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaW5jbHVkZS5vcmRlcmVkLm1lbWJlcnMoWzEsIDJdKVxuICAgKiAgICAgICAuYnV0Lm5vdC5pbmNsdWRlLm9yZGVyZWQubWVtYmVycyhbMiwgM10pO1xuICAgKlxuICAgKiBAbmFtZSBvcmRlcmVkXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnb3JkZXJlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICBmbGFnKHRoaXMsICdvcmRlcmVkJywgdHJ1ZSk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLmFueVxuICAgKlxuICAgKiBDYXVzZXMgYWxsIGAua2V5c2AgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG8gb25seSByZXF1aXJlIHRoYXRcbiAgICogdGhlIHRhcmdldCBoYXZlIGF0IGxlYXN0IG9uZSBvZiB0aGUgZ2l2ZW4ga2V5cy4gVGhpcyBpcyB0aGUgb3Bwb3NpdGUgb2ZcbiAgICogYC5hbGxgLCB3aGljaCByZXF1aXJlcyB0aGF0IHRoZSB0YXJnZXQgaGF2ZSBhbGwgb2YgdGhlIGdpdmVuIGtleXMuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDJ9KS50by5ub3QuaGF2ZS5hbnkua2V5cygnYycsICdkJyk7XG4gICAqXG4gICAqIFNlZSB0aGUgYC5rZXlzYCBkb2MgZm9yIGd1aWRhbmNlIG9uIHdoZW4gdG8gdXNlIGAuYW55YCBvciBgLmFsbGAuXG4gICAqXG4gICAqIEBuYW1lIGFueVxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2FueScsIGZ1bmN0aW9uICgpIHtcbiAgICBmbGFnKHRoaXMsICdhbnknLCB0cnVlKTtcbiAgICBmbGFnKHRoaXMsICdhbGwnLCBmYWxzZSk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLmFsbFxuICAgKlxuICAgKiBDYXVzZXMgYWxsIGAua2V5c2AgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG8gcmVxdWlyZSB0aGF0IHRoZVxuICAgKiB0YXJnZXQgaGF2ZSBhbGwgb2YgdGhlIGdpdmVuIGtleXMuIFRoaXMgaXMgdGhlIG9wcG9zaXRlIG9mIGAuYW55YCwgd2hpY2hcbiAgICogb25seSByZXF1aXJlcyB0aGF0IHRoZSB0YXJnZXQgaGF2ZSBhdCBsZWFzdCBvbmUgb2YgdGhlIGdpdmVuIGtleXMuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDJ9KS50by5oYXZlLmFsbC5rZXlzKCdhJywgJ2InKTtcbiAgICpcbiAgICogTm90ZSB0aGF0IGAuYWxsYCBpcyB1c2VkIGJ5IGRlZmF1bHQgd2hlbiBuZWl0aGVyIGAuYWxsYCBub3IgYC5hbnlgIGFyZVxuICAgKiBhZGRlZCBlYXJsaWVyIGluIHRoZSBjaGFpbi4gSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFkZCBgLmFsbGAgYW55d2F5XG4gICAqIGJlY2F1c2UgaXQgaW1wcm92ZXMgcmVhZGFiaWxpdHkuXG4gICAqXG4gICAqIFNlZSB0aGUgYC5rZXlzYCBkb2MgZm9yIGd1aWRhbmNlIG9uIHdoZW4gdG8gdXNlIGAuYW55YCBvciBgLmFsbGAuXG4gICAqXG4gICAqIEBuYW1lIGFsbFxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2FsbCcsIGZ1bmN0aW9uICgpIHtcbiAgICBmbGFnKHRoaXMsICdhbGwnLCB0cnVlKTtcbiAgICBmbGFnKHRoaXMsICdhbnknLCBmYWxzZSk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLmEodHlwZVssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0J3MgdHlwZSBpcyBlcXVhbCB0byB0aGUgZ2l2ZW4gc3RyaW5nIGB0eXBlYC4gVHlwZXNcbiAgICogYXJlIGNhc2UgaW5zZW5zaXRpdmUuIFNlZSB0aGUgYHR5cGUtZGV0ZWN0YCBwcm9qZWN0IHBhZ2UgZm9yIGluZm8gb24gdGhlXG4gICAqIHR5cGUgZGV0ZWN0aW9uIGFsZ29yaXRobTogaHR0cHM6Ly9naXRodWIuY29tL2NoYWlqcy90eXBlLWRldGVjdC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uYmUuYSgnc3RyaW5nJyk7XG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5iZS5hbignb2JqZWN0Jyk7XG4gICAqICAgICBleHBlY3QobnVsbCkudG8uYmUuYSgnbnVsbCcpO1xuICAgKiAgICAgZXhwZWN0KHVuZGVmaW5lZCkudG8uYmUuYW4oJ3VuZGVmaW5lZCcpO1xuICAgKiAgICAgZXhwZWN0KG5ldyBFcnJvcikudG8uYmUuYW4oJ2Vycm9yJyk7XG4gICAqICAgICBleHBlY3QoUHJvbWlzZS5yZXNvbHZlKCkpLnRvLmJlLmEoJ3Byb21pc2UnKTtcbiAgICogICAgIGV4cGVjdChuZXcgRmxvYXQzMkFycmF5KS50by5iZS5hKCdmbG9hdDMyYXJyYXknKTtcbiAgICogICAgIGV4cGVjdChTeW1ib2woKSkudG8uYmUuYSgnc3ltYm9sJyk7XG4gICAqXG4gICAqIGAuYWAgc3VwcG9ydHMgb2JqZWN0cyB0aGF0IGhhdmUgYSBjdXN0b20gdHlwZSBzZXQgdmlhIGBTeW1ib2wudG9TdHJpbmdUYWdgLlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge1xuICAgKiAgICAgICBbU3ltYm9sLnRvU3RyaW5nVGFnXTogJ215Q3VzdG9tVHlwZSdcbiAgICogICAgIH07XG4gICAqXG4gICAqICAgICBleHBlY3QobXlPYmopLnRvLmJlLmEoJ215Q3VzdG9tVHlwZScpLmJ1dC5ub3QuYW4oJ29iamVjdCcpO1xuICAgKlxuICAgKiBJdCdzIG9mdGVuIGJlc3QgdG8gdXNlIGAuYWAgdG8gY2hlY2sgYSB0YXJnZXQncyB0eXBlIGJlZm9yZSBtYWtpbmcgbW9yZVxuICAgKiBhc3NlcnRpb25zIG9uIHRoZSBzYW1lIHRhcmdldC4gVGhhdCB3YXksIHlvdSBhdm9pZCB1bmV4cGVjdGVkIGJlaGF2aW9yIGZyb21cbiAgICogYW55IGFzc2VydGlvbiB0aGF0IGRvZXMgZGlmZmVyZW50IHRoaW5ncyBiYXNlZCBvbiB0aGUgdGFyZ2V0J3MgdHlwZS5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmJlLmFuKCdhcnJheScpLnRoYXQuaW5jbHVkZXMoMik7XG4gICAqICAgICBleHBlY3QoW10pLnRvLmJlLmFuKCdhcnJheScpLnRoYXQuaXMuZW1wdHk7XG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuYWAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdCB0b1xuICAgKiBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIHRoZSBleHBlY3RlZCB0eXBlLCByYXRoZXIgdGhhbiBhc3NlcnRpbmcgdGhhdCBpdFxuICAgKiBpc24ndCBvbmUgb2YgbWFueSB1bmV4cGVjdGVkIHR5cGVzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5iZS5hKCdzdHJpbmcnKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8ubm90LmJlLmFuKCdhcnJheScpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogYC5hYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgdG9cbiAgICogc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmRcbiAgICogYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMSkudG8uYmUuYSgnc3RyaW5nJywgJ25vb28gd2h5IGZhaWw/PycpO1xuICAgKiAgICAgZXhwZWN0KDEsICdub29vIHdoeSBmYWlsPz8nKS50by5iZS5hKCdzdHJpbmcnKTtcbiAgICpcbiAgICogYC5hYCBjYW4gYWxzbyBiZSB1c2VkIGFzIGEgbGFuZ3VhZ2UgY2hhaW4gdG8gaW1wcm92ZSB0aGUgcmVhZGFiaWxpdHkgb2ZcbiAgICogeW91ciBhc3NlcnRpb25zLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHtiOiAyfSkudG8uaGF2ZS5hLnByb3BlcnR5KCdiJyk7XG4gICAqXG4gICAqIFRoZSBhbGlhcyBgLmFuYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLmFgLlxuICAgKlxuICAgKiBAbmFtZSBhXG4gICAqIEBhbGlhcyBhblxuICAgKiBAcGFyYW0ge1N0cmluZ30gdHlwZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gYW4gKHR5cGUsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHR5cGUgPSB0eXBlLnRvTG93ZXJDYXNlKCk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIGFydGljbGUgPSB+WyAnYScsICdlJywgJ2knLCAnbycsICd1JyBdLmluZGV4T2YodHlwZS5jaGFyQXQoMCkpID8gJ2FuICcgOiAnYSAnO1xuXG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIHR5cGUgPT09IF8udHlwZShvYmopLnRvTG93ZXJDYXNlKClcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgJyArIGFydGljbGUgKyB0eXBlXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IG5vdCB0byBiZSAnICsgYXJ0aWNsZSArIHR5cGVcbiAgICApO1xuICB9XG5cbiAgQXNzZXJ0aW9uLmFkZENoYWluYWJsZU1ldGhvZCgnYW4nLCBhbik7XG4gIEFzc2VydGlvbi5hZGRDaGFpbmFibGVNZXRob2QoJ2EnLCBhbik7XG5cbiAgLyoqXG4gICAqICMjIyAuaW5jbHVkZSh2YWxbLCBtc2ddKVxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgYSBzdHJpbmcsIGAuaW5jbHVkZWAgYXNzZXJ0cyB0aGF0IHRoZSBnaXZlbiBzdHJpbmcgYHZhbGBcbiAgICogaXMgYSBzdWJzdHJpbmcgb2YgdGhlIHRhcmdldC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vYmFyJykudG8uaW5jbHVkZSgnZm9vJyk7XG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpcyBhbiBhcnJheSwgYC5pbmNsdWRlYCBhc3NlcnRzIHRoYXQgdGhlIGdpdmVuIGB2YWxgIGlzIGFcbiAgICogbWVtYmVyIG9mIHRoZSB0YXJnZXQuXG4gICAqXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5pbmNsdWRlKDIpO1xuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgYW4gb2JqZWN0LCBgLmluY2x1ZGVgIGFzc2VydHMgdGhhdCB0aGUgZ2l2ZW4gb2JqZWN0XG4gICAqIGB2YWxgJ3MgcHJvcGVydGllcyBhcmUgYSBzdWJzZXQgb2YgdGhlIHRhcmdldCdzIHByb3BlcnRpZXMuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDIsIGM6IDN9KS50by5pbmNsdWRlKHthOiAxLCBiOiAyfSk7XG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpcyBhIFNldCBvciBXZWFrU2V0LCBgLmluY2x1ZGVgIGFzc2VydHMgdGhhdCB0aGUgZ2l2ZW4gYHZhbGAgaXMgYVxuICAgKiBtZW1iZXIgb2YgdGhlIHRhcmdldC4gU2FtZVZhbHVlWmVybyBlcXVhbGl0eSBhbGdvcml0aG0gaXMgdXNlZC5cbiAgICpcbiAgICogICAgIGV4cGVjdChuZXcgU2V0KFsxLCAyXSkpLnRvLmluY2x1ZGUoMik7XG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpcyBhIE1hcCwgYC5pbmNsdWRlYCBhc3NlcnRzIHRoYXQgdGhlIGdpdmVuIGB2YWxgIGlzIG9uZSBvZlxuICAgKiB0aGUgdmFsdWVzIG9mIHRoZSB0YXJnZXQuIFNhbWVWYWx1ZVplcm8gZXF1YWxpdHkgYWxnb3JpdGhtIGlzIHVzZWQuXG4gICAqXG4gICAqICAgICBleHBlY3QobmV3IE1hcChbWydhJywgMV0sIFsnYicsIDJdXSkpLnRvLmluY2x1ZGUoMik7XG4gICAqXG4gICAqIEJlY2F1c2UgYC5pbmNsdWRlYCBkb2VzIGRpZmZlcmVudCB0aGluZ3MgYmFzZWQgb24gdGhlIHRhcmdldCdzIHR5cGUsIGl0J3NcbiAgICogaW1wb3J0YW50IHRvIGNoZWNrIHRoZSB0YXJnZXQncyB0eXBlIGJlZm9yZSB1c2luZyBgLmluY2x1ZGVgLiBTZWUgdGhlIGAuYWBcbiAgICogZG9jIGZvciBpbmZvIG9uIHRlc3RpbmcgYSB0YXJnZXQncyB0eXBlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uYmUuYW4oJ2FycmF5JykudGhhdC5pbmNsdWRlcygyKTtcbiAgICpcbiAgICogQnkgZGVmYXVsdCwgc3RyaWN0IChgPT09YCkgZXF1YWxpdHkgaXMgdXNlZCB0byBjb21wYXJlIGFycmF5IG1lbWJlcnMgYW5kXG4gICAqIG9iamVjdCBwcm9wZXJ0aWVzLiBBZGQgYC5kZWVwYCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byB1c2UgZGVlcCBlcXVhbGl0eVxuICAgKiBpbnN0ZWFkIChXZWFrU2V0IHRhcmdldHMgYXJlIG5vdCBzdXBwb3J0ZWQpLiBTZWUgdGhlIGBkZWVwLWVxbGAgcHJvamVjdFxuICAgKiBwYWdlIGZvciBpbmZvIG9uIHRoZSBkZWVwIGVxdWFsaXR5IGFsZ29yaXRobTogaHR0cHM6Ly9naXRodWIuY29tL2NoYWlqcy9kZWVwLWVxbC5cbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBhcnJheSBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGluY2x1ZGVzIGB7YTogMX1gXG4gICAqICAgICBleHBlY3QoW3thOiAxfV0pLnRvLmRlZXAuaW5jbHVkZSh7YTogMX0pO1xuICAgKiAgICAgZXhwZWN0KFt7YTogMX1dKS50by5ub3QuaW5jbHVkZSh7YTogMX0pO1xuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IG9iamVjdCBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGluY2x1ZGVzIGB4OiB7YTogMX1gXG4gICAqICAgICBleHBlY3Qoe3g6IHthOiAxfX0pLnRvLmRlZXAuaW5jbHVkZSh7eDoge2E6IDF9fSk7XG4gICAqICAgICBleHBlY3Qoe3g6IHthOiAxfX0pLnRvLm5vdC5pbmNsdWRlKHt4OiB7YTogMX19KTtcbiAgICpcbiAgICogQnkgZGVmYXVsdCwgYWxsIG9mIHRoZSB0YXJnZXQncyBwcm9wZXJ0aWVzIGFyZSBzZWFyY2hlZCB3aGVuIHdvcmtpbmcgd2l0aFxuICAgKiBvYmplY3RzLiBUaGlzIGluY2x1ZGVzIHByb3BlcnRpZXMgdGhhdCBhcmUgaW5oZXJpdGVkIGFuZC9vciBub24tZW51bWVyYWJsZS5cbiAgICogQWRkIGAub3duYCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBleGNsdWRlIHRoZSB0YXJnZXQncyBpbmhlcml0ZWRcbiAgICogcHJvcGVydGllcyBmcm9tIHRoZSBzZWFyY2guXG4gICAqXG4gICAqICAgICBPYmplY3QucHJvdG90eXBlLmIgPSAyO1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ub3duLmluY2x1ZGUoe2E6IDF9KTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmluY2x1ZGUoe2I6IDJ9KS5idXQubm90Lm93bi5pbmNsdWRlKHtiOiAyfSk7XG4gICAqXG4gICAqIE5vdGUgdGhhdCBhIHRhcmdldCBvYmplY3QgaXMgYWx3YXlzIG9ubHkgc2VhcmNoZWQgZm9yIGB2YWxgJ3Mgb3duXG4gICAqIGVudW1lcmFibGUgcHJvcGVydGllcy5cbiAgICpcbiAgICogYC5kZWVwYCBhbmQgYC5vd25gIGNhbiBiZSBjb21iaW5lZC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YToge2I6IDJ9fSkudG8uZGVlcC5vd24uaW5jbHVkZSh7YToge2I6IDJ9fSk7XG4gICAqXG4gICAqIEFkZCBgLm5lc3RlZGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gZW5hYmxlIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gd2hlblxuICAgKiByZWZlcmVuY2luZyBuZXN0ZWQgcHJvcGVydGllcy5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YToge2I6IFsneCcsICd5J119fSkudG8ubmVzdGVkLmluY2x1ZGUoeydhLmJbMV0nOiAneSd9KTtcbiAgICpcbiAgICogSWYgYC5gIG9yIGBbXWAgYXJlIHBhcnQgb2YgYW4gYWN0dWFsIHByb3BlcnR5IG5hbWUsIHRoZXkgY2FuIGJlIGVzY2FwZWQgYnlcbiAgICogYWRkaW5nIHR3byBiYWNrc2xhc2hlcyBiZWZvcmUgdGhlbS5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7Jy5hJzogeydbYl0nOiAyfX0pLnRvLm5lc3RlZC5pbmNsdWRlKHsnXFxcXC5hLlxcXFxbYlxcXFxdJzogMn0pO1xuICAgKlxuICAgKiBgLmRlZXBgIGFuZCBgLm5lc3RlZGAgY2FuIGJlIGNvbWJpbmVkLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiB7YjogW3tjOiAzfV19fSkudG8uZGVlcC5uZXN0ZWQuaW5jbHVkZSh7J2EuYlswXSc6IHtjOiAzfX0pO1xuICAgKlxuICAgKiBgLm93bmAgYW5kIGAubmVzdGVkYCBjYW5ub3QgYmUgY29tYmluZWQuXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuaW5jbHVkZWAuXG4gICAqXG4gICAqICAgICBleHBlY3QoJ2Zvb2JhcicpLnRvLm5vdC5pbmNsdWRlKCd0YWNvJyk7XG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5ub3QuaW5jbHVkZSg0KTtcbiAgICpcbiAgICogSG93ZXZlciwgaXQncyBkYW5nZXJvdXMgdG8gbmVnYXRlIGAuaW5jbHVkZWAgd2hlbiB0aGUgdGFyZ2V0IGlzIGFuIG9iamVjdC5cbiAgICogVGhlIHByb2JsZW0gaXMgdGhhdCBpdCBjcmVhdGVzIHVuY2VydGFpbiBleHBlY3RhdGlvbnMgYnkgYXNzZXJ0aW5nIHRoYXQgdGhlXG4gICAqIHRhcmdldCBvYmplY3QgZG9lc24ndCBoYXZlIGFsbCBvZiBgdmFsYCdzIGtleS92YWx1ZSBwYWlycyBidXQgbWF5IG9yIG1heVxuICAgKiBub3QgaGF2ZSBzb21lIG9mIHRoZW0uIEl0J3Mgb2Z0ZW4gYmVzdCB0byBpZGVudGlmeSB0aGUgZXhhY3Qgb3V0cHV0IHRoYXQnc1xuICAgKiBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW4gYXNzZXJ0aW9uIHRoYXQgb25seSBhY2NlcHRzIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgb2JqZWN0IGlzbid0IGV2ZW4gZXhwZWN0ZWQgdG8gaGF2ZSBgdmFsYCdzIGtleXMsIGl0J3NcbiAgICogb2Z0ZW4gYmVzdCB0byBhc3NlcnQgZXhhY3RseSB0aGF0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHtjOiAzfSkudG8ubm90LmhhdmUuYW55LmtleXMoJ2EnLCAnYicpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHtjOiAzfSkudG8ubm90LmluY2x1ZGUoe2E6IDEsIGI6IDJ9KTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBvYmplY3QgaXMgZXhwZWN0ZWQgdG8gaGF2ZSBgdmFsYCdzIGtleXMsIGl0J3Mgb2Z0ZW4gYmVzdCB0b1xuICAgKiBhc3NlcnQgdGhhdCBlYWNoIG9mIHRoZSBwcm9wZXJ0aWVzIGhhcyBpdHMgZXhwZWN0ZWQgdmFsdWUsIHJhdGhlciB0aGFuXG4gICAqIGFzc2VydGluZyB0aGF0IGVhY2ggcHJvcGVydHkgZG9lc24ndCBoYXZlIG9uZSBvZiBtYW55IHVuZXhwZWN0ZWQgdmFsdWVzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAzLCBiOiA0fSkudG8uaW5jbHVkZSh7YTogMywgYjogNH0pOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAzLCBiOiA0fSkudG8ubm90LmluY2x1ZGUoe2E6IDEsIGI6IDJ9KTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAuaW5jbHVkZWAgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvclxuICAgKiBtZXNzYWdlIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhc1xuICAgKiB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaW5jbHVkZSg0LCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uaW5jbHVkZSg0KTtcbiAgICpcbiAgICogYC5pbmNsdWRlYCBjYW4gYWxzbyBiZSB1c2VkIGFzIGEgbGFuZ3VhZ2UgY2hhaW4sIGNhdXNpbmcgYWxsIGAubWVtYmVyc2AgYW5kXG4gICAqIGAua2V5c2AgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG8gcmVxdWlyZSB0aGUgdGFyZ2V0IHRvIGJlIGFcbiAgICogc3VwZXJzZXQgb2YgdGhlIGV4cGVjdGVkIHNldCwgcmF0aGVyIHRoYW4gYW4gaWRlbnRpY2FsIHNldC4gTm90ZSB0aGF0XG4gICAqIGAubWVtYmVyc2AgaWdub3JlcyBkdXBsaWNhdGVzIGluIHRoZSBzdWJzZXQgd2hlbiBgLmluY2x1ZGVgIGlzIGFkZGVkLlxuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IG9iamVjdCdzIGtleXMgYXJlIGEgc3VwZXJzZXQgb2YgWydhJywgJ2InXSBidXQgbm90IGlkZW50aWNhbFxuICAgKiAgICAgZXhwZWN0KHthOiAxLCBiOiAyLCBjOiAzfSkudG8uaW5jbHVkZS5hbGwua2V5cygnYScsICdiJyk7XG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDIsIGM6IDN9KS50by5ub3QuaGF2ZS5hbGwua2V5cygnYScsICdiJyk7XG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgYXJyYXkgaXMgYSBzdXBlcnNldCBvZiBbMSwgMl0gYnV0IG5vdCBpZGVudGljYWxcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmluY2x1ZGUubWVtYmVycyhbMSwgMl0pO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8ubm90LmhhdmUubWVtYmVycyhbMSwgMl0pO1xuICAgKlxuICAgKiAgICAgLy8gRHVwbGljYXRlcyBpbiB0aGUgc3Vic2V0IGFyZSBpZ25vcmVkXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5pbmNsdWRlLm1lbWJlcnMoWzEsIDIsIDIsIDJdKTtcbiAgICpcbiAgICogTm90ZSB0aGF0IGFkZGluZyBgLmFueWAgZWFybGllciBpbiB0aGUgY2hhaW4gY2F1c2VzIHRoZSBgLmtleXNgIGFzc2VydGlvblxuICAgKiB0byBpZ25vcmUgYC5pbmNsdWRlYC5cbiAgICpcbiAgICogICAgIC8vIEJvdGggYXNzZXJ0aW9ucyBhcmUgaWRlbnRpY2FsXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5pbmNsdWRlLmFueS5rZXlzKCdhJywgJ2InKTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUuYW55LmtleXMoJ2EnLCAnYicpO1xuICAgKlxuICAgKiBUaGUgYWxpYXNlcyBgLmluY2x1ZGVzYCwgYC5jb250YWluYCwgYW5kIGAuY29udGFpbnNgIGNhbiBiZSB1c2VkXG4gICAqIGludGVyY2hhbmdlYWJseSB3aXRoIGAuaW5jbHVkZWAuXG4gICAqXG4gICAqIEBuYW1lIGluY2x1ZGVcbiAgICogQGFsaWFzIGNvbnRhaW5cbiAgICogQGFsaWFzIGluY2x1ZGVzXG4gICAqIEBhbGlhcyBjb250YWluc1xuICAgKiBAcGFyYW0ge01peGVkfSB2YWxcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIFNhbWVWYWx1ZVplcm8oYSwgYikge1xuICAgIHJldHVybiAoXy5pc05hTihhKSAmJiBfLmlzTmFOKGIpKSB8fCBhID09PSBiO1xuICB9XG5cbiAgZnVuY3Rpb24gaW5jbHVkZUNoYWluaW5nQmVoYXZpb3IgKCkge1xuICAgIGZsYWcodGhpcywgJ2NvbnRhaW5zJywgdHJ1ZSk7XG4gIH1cblxuICBmdW5jdGlvbiBpbmNsdWRlICh2YWwsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuXG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIG9ialR5cGUgPSBfLnR5cGUob2JqKS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgbmVnYXRlID0gZmxhZyh0aGlzLCAnbmVnYXRlJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKVxuICAgICAgLCBpc0RlZXAgPSBmbGFnKHRoaXMsICdkZWVwJylcbiAgICAgICwgZGVzY3JpcHRvciA9IGlzRGVlcCA/ICdkZWVwICcgOiAnJztcblxuICAgIGZsYWdNc2cgPSBmbGFnTXNnID8gZmxhZ01zZyArICc6ICcgOiAnJztcblxuICAgIHZhciBpbmNsdWRlZCA9IGZhbHNlO1xuXG4gICAgc3dpdGNoIChvYmpUeXBlKSB7XG4gICAgICBjYXNlICdzdHJpbmcnOlxuICAgICAgICBpbmNsdWRlZCA9IG9iai5pbmRleE9mKHZhbCkgIT09IC0xO1xuICAgICAgICBicmVhaztcblxuICAgICAgY2FzZSAnd2Vha3NldCc6XG4gICAgICAgIGlmIChpc0RlZXApIHtcbiAgICAgICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoXG4gICAgICAgICAgICBmbGFnTXNnICsgJ3VuYWJsZSB0byB1c2UgLmRlZXAuaW5jbHVkZSB3aXRoIFdlYWtTZXQnLFxuICAgICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgICAgc3NmaVxuICAgICAgICAgICk7XG4gICAgICAgIH1cblxuICAgICAgICBpbmNsdWRlZCA9IG9iai5oYXModmFsKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ21hcCc6XG4gICAgICAgIHZhciBpc0VxbCA9IGlzRGVlcCA/IF8uZXFsIDogU2FtZVZhbHVlWmVybztcbiAgICAgICAgb2JqLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICBpbmNsdWRlZCA9IGluY2x1ZGVkIHx8IGlzRXFsKGl0ZW0sIHZhbCk7XG4gICAgICAgIH0pO1xuICAgICAgICBicmVhaztcblxuICAgICAgY2FzZSAnc2V0JzpcbiAgICAgICAgaWYgKGlzRGVlcCkge1xuICAgICAgICAgIG9iai5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICBpbmNsdWRlZCA9IGluY2x1ZGVkIHx8IF8uZXFsKGl0ZW0sIHZhbCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaW5jbHVkZWQgPSBvYmouaGFzKHZhbCk7XG4gICAgICAgIH1cbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgJ2FycmF5JzpcbiAgICAgICAgaWYgKGlzRGVlcCkge1xuICAgICAgICAgIGluY2x1ZGVkID0gb2JqLnNvbWUoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBfLmVxbChpdGVtLCB2YWwpO1xuICAgICAgICAgIH0pXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgaW5jbHVkZWQgPSBvYmouaW5kZXhPZih2YWwpICE9PSAtMTtcbiAgICAgICAgfVxuICAgICAgICBicmVhaztcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgLy8gVGhpcyBibG9jayBpcyBmb3IgYXNzZXJ0aW5nIGEgc3Vic2V0IG9mIHByb3BlcnRpZXMgaW4gYW4gb2JqZWN0LlxuICAgICAgICAvLyBgXy5leHBlY3RUeXBlc2AgaXNuJ3QgdXNlZCBoZXJlIGJlY2F1c2UgYC5pbmNsdWRlYCBzaG91bGQgd29yayB3aXRoXG4gICAgICAgIC8vIG9iamVjdHMgd2l0aCBhIGN1c3RvbSBgQEB0b1N0cmluZ1RhZ2AuXG4gICAgICAgIGlmICh2YWwgIT09IE9iamVjdCh2YWwpKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICAgICAgZmxhZ01zZyArICd0aGUgZ2l2ZW4gY29tYmluYXRpb24gb2YgYXJndW1lbnRzICgnXG4gICAgICAgICAgICArIG9ialR5cGUgKyAnIGFuZCAnXG4gICAgICAgICAgICArIF8udHlwZSh2YWwpLnRvTG93ZXJDYXNlKCkgKyAnKSdcbiAgICAgICAgICAgICsgJyBpcyBpbnZhbGlkIGZvciB0aGlzIGFzc2VydGlvbi4gJ1xuICAgICAgICAgICAgKyAnWW91IGNhbiB1c2UgYW4gYXJyYXksIGEgbWFwLCBhbiBvYmplY3QsIGEgc2V0LCBhIHN0cmluZywgJ1xuICAgICAgICAgICAgKyAnb3IgYSB3ZWFrc2V0IGluc3RlYWQgb2YgYSAnXG4gICAgICAgICAgICArIF8udHlwZSh2YWwpLnRvTG93ZXJDYXNlKCksXG4gICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICBzc2ZpXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBwcm9wcyA9IE9iamVjdC5rZXlzKHZhbClcbiAgICAgICAgICAsIGZpcnN0RXJyID0gbnVsbFxuICAgICAgICAgICwgbnVtRXJycyA9IDA7XG5cbiAgICAgICAgcHJvcHMuZm9yRWFjaChmdW5jdGlvbiAocHJvcCkge1xuICAgICAgICAgIHZhciBwcm9wQXNzZXJ0aW9uID0gbmV3IEFzc2VydGlvbihvYmopO1xuICAgICAgICAgIF8udHJhbnNmZXJGbGFncyh0aGlzLCBwcm9wQXNzZXJ0aW9uLCB0cnVlKTtcbiAgICAgICAgICBmbGFnKHByb3BBc3NlcnRpb24sICdsb2NrU3NmaScsIHRydWUpO1xuXG4gICAgICAgICAgaWYgKCFuZWdhdGUgfHwgcHJvcHMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICBwcm9wQXNzZXJ0aW9uLnByb3BlcnR5KHByb3AsIHZhbFtwcm9wXSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHByb3BBc3NlcnRpb24ucHJvcGVydHkocHJvcCwgdmFsW3Byb3BdKTtcbiAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIGlmICghXy5jaGVja0Vycm9yLmNvbXBhdGlibGVDb25zdHJ1Y3RvcihlcnIsIEFzc2VydGlvbkVycm9yKSkge1xuICAgICAgICAgICAgICB0aHJvdyBlcnI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZmlyc3RFcnIgPT09IG51bGwpIGZpcnN0RXJyID0gZXJyO1xuICAgICAgICAgICAgbnVtRXJycysrO1xuICAgICAgICAgIH1cbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgLy8gV2hlbiB2YWxpZGF0aW5nIC5ub3QuaW5jbHVkZSB3aXRoIG11bHRpcGxlIHByb3BlcnRpZXMsIHdlIG9ubHkgd2FudFxuICAgICAgICAvLyB0byB0aHJvdyBhbiBhc3NlcnRpb24gZXJyb3IgaWYgYWxsIG9mIHRoZSBwcm9wZXJ0aWVzIGFyZSBpbmNsdWRlZCxcbiAgICAgICAgLy8gaW4gd2hpY2ggY2FzZSB3ZSB0aHJvdyB0aGUgZmlyc3QgcHJvcGVydHkgYXNzZXJ0aW9uIGVycm9yIHRoYXQgd2VcbiAgICAgICAgLy8gZW5jb3VudGVyZWQuXG4gICAgICAgIGlmIChuZWdhdGUgJiYgcHJvcHMubGVuZ3RoID4gMSAmJiBudW1FcnJzID09PSBwcm9wcy5sZW5ndGgpIHtcbiAgICAgICAgICB0aHJvdyBmaXJzdEVycjtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgLy8gQXNzZXJ0IGluY2x1c2lvbiBpbiBjb2xsZWN0aW9uIG9yIHN1YnN0cmluZyBpbiBhIHN0cmluZy5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgIGluY2x1ZGVkXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvICcgKyBkZXNjcmlwdG9yICsgJ2luY2x1ZGUgJyArIF8uaW5zcGVjdCh2YWwpXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCAnICsgZGVzY3JpcHRvciArICdpbmNsdWRlICcgKyBfLmluc3BlY3QodmFsKSk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkQ2hhaW5hYmxlTWV0aG9kKCdpbmNsdWRlJywgaW5jbHVkZSwgaW5jbHVkZUNoYWluaW5nQmVoYXZpb3IpO1xuICBBc3NlcnRpb24uYWRkQ2hhaW5hYmxlTWV0aG9kKCdjb250YWluJywgaW5jbHVkZSwgaW5jbHVkZUNoYWluaW5nQmVoYXZpb3IpO1xuICBBc3NlcnRpb24uYWRkQ2hhaW5hYmxlTWV0aG9kKCdjb250YWlucycsIGluY2x1ZGUsIGluY2x1ZGVDaGFpbmluZ0JlaGF2aW9yKTtcbiAgQXNzZXJ0aW9uLmFkZENoYWluYWJsZU1ldGhvZCgnaW5jbHVkZXMnLCBpbmNsdWRlLCBpbmNsdWRlQ2hhaW5pbmdCZWhhdmlvcik7XG5cbiAgLyoqXG4gICAqICMjIyAub2tcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSB0cnV0aHkgdmFsdWUgKGNvbnNpZGVyZWQgYHRydWVgIGluIGJvb2xlYW4gY29udGV4dCkuXG4gICAqIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIHN0cmljdGx5IChgPT09YCkgb3JcbiAgICogZGVlcGx5IGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZS5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5lcXVhbCgxKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxKS50by5iZS5vazsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqICAgICBleHBlY3QodHJ1ZSkudG8uYmUudHJ1ZTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh0cnVlKS50by5iZS5vazsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAub2tgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDApLnRvLmVxdWFsKDApOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDApLnRvLm5vdC5iZS5vazsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqICAgICBleHBlY3QoZmFsc2UpLnRvLmJlLmZhbHNlOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGZhbHNlKS50by5ub3QuYmUub2s7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiAgICAgZXhwZWN0KG51bGwpLnRvLmJlLm51bGw7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QobnVsbCkudG8ubm90LmJlLm9rOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdCh1bmRlZmluZWQpLnRvLmJlLnVuZGVmaW5lZDsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh1bmRlZmluZWQpLnRvLm5vdC5iZS5vazsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgY2FuIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoZmFsc2UsICdub29vIHdoeSBmYWlsPz8nKS50by5iZS5vaztcbiAgICpcbiAgICogQG5hbWUgb2tcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdvaycsIGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgdHJ1dGh5J1xuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSBmYWxzeScpO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC50cnVlXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIHN0cmljdGx5IChgPT09YCkgZXF1YWwgdG8gYHRydWVgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHRydWUpLnRvLmJlLnRydWU7XG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAudHJ1ZWAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdFxuICAgKiB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZSwgcmF0aGVyIHRoYW4gbm90XG4gICAqIGVxdWFsIHRvIGB0cnVlYC5cbiAgICpcbiAgICogICAgIGV4cGVjdChmYWxzZSkudG8uYmUuZmFsc2U7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoZmFsc2UpLnRvLm5vdC5iZS50cnVlOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5lcXVhbCgxKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxKS50by5ub3QuYmUudHJ1ZTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgY2FuIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoZmFsc2UsICdub29vIHdoeSBmYWlsPz8nKS50by5iZS50cnVlO1xuICAgKlxuICAgKiBAbmFtZSB0cnVlXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgndHJ1ZScsIGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgdHJ1ZSA9PT0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgdHJ1ZSdcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgZmFsc2UnXG4gICAgICAsIGZsYWcodGhpcywgJ25lZ2F0ZScpID8gZmFsc2UgOiB0cnVlXG4gICAgKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAuZmFsc2VcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgc3RyaWN0bHkgKGA9PT1gKSBlcXVhbCB0byBgZmFsc2VgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KGZhbHNlKS50by5iZS5mYWxzZTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5mYWxzZWAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW5cbiAgICogYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZSwgcmF0aGVyIHRoYW5cbiAgICogbm90IGVxdWFsIHRvIGBmYWxzZWAuXG4gICAqXG4gICAqICAgICBleHBlY3QodHJ1ZSkudG8uYmUudHJ1ZTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh0cnVlKS50by5ub3QuYmUuZmFsc2U7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5iZS5mYWxzZTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgY2FuIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QodHJ1ZSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmZhbHNlO1xuICAgKlxuICAgKiBAbmFtZSBmYWxzZVxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2ZhbHNlJywgZnVuY3Rpb24gKCkge1xuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICBmYWxzZSA9PT0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgZmFsc2UnXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIHRydWUnXG4gICAgICAsIGZsYWcodGhpcywgJ25lZ2F0ZScpID8gdHJ1ZSA6IGZhbHNlXG4gICAgKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAubnVsbFxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgdGhlIHRhcmdldCBpcyBzdHJpY3RseSAoYD09PWApIGVxdWFsIHRvIGBudWxsYC5cbiAgICpcbiAgICogICAgIGV4cGVjdChudWxsKS50by5iZS5udWxsO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLm51bGxgLiBIb3dldmVyLCBpdCdzIG9mdGVuIGJlc3RcbiAgICogdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCBpcyBlcXVhbCB0byBpdHMgZXhwZWN0ZWQgdmFsdWUsIHJhdGhlciB0aGFuIG5vdFxuICAgKiBlcXVhbCB0byBgbnVsbGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMSkudG8uZXF1YWwoMSk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMSkudG8ubm90LmJlLm51bGw7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDQyLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUubnVsbDtcbiAgICpcbiAgICogQG5hbWUgbnVsbFxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ251bGwnLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIG51bGwgPT09IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIG51bGwnXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IG5vdCB0byBiZSBudWxsJ1xuICAgICk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLnVuZGVmaW5lZFxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgdGhlIHRhcmdldCBpcyBzdHJpY3RseSAoYD09PWApIGVxdWFsIHRvIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHVuZGVmaW5lZCkudG8uYmUudW5kZWZpbmVkO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLnVuZGVmaW5lZGAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW5cbiAgICogYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZSwgcmF0aGVyIHRoYW5cbiAgICogbm90IGVxdWFsIHRvIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5iZS51bmRlZmluZWQ7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDQyLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUudW5kZWZpbmVkO1xuICAgKlxuICAgKiBAbmFtZSB1bmRlZmluZWRcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCd1bmRlZmluZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIHVuZGVmaW5lZCA9PT0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgdW5kZWZpbmVkJ1xuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSBub3QgdG8gYmUgdW5kZWZpbmVkJ1xuICAgICk7XG4gIH0pO1xuXG4gIC8qKlxuICAgKiAjIyMgLk5hTlxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgdGhlIHRhcmdldCBpcyBleGFjdGx5IGBOYU5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KE5hTikudG8uYmUuTmFOO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLk5hTmAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdFxuICAgKiB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZSwgcmF0aGVyIHRoYW4gbm90XG4gICAqIGVxdWFsIHRvIGBOYU5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5lcXVhbCgnZm9vJyk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoJ2ZvbycpLnRvLm5vdC5iZS5OYU47IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDQyLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUuTmFOO1xuICAgKlxuICAgKiBAbmFtZSBOYU5cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdOYU4nLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIF8uaXNOYU4oZmxhZyh0aGlzLCAnb2JqZWN0JykpXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgTmFOJ1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IG5vdCB0byBiZSBOYU4nXG4gICAgKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAuZXhpc3RcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgbm90IHN0cmljdGx5IChgPT09YCkgZXF1YWwgdG8gZWl0aGVyIGBudWxsYCBvclxuICAgKiBgdW5kZWZpbmVkYC4gSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQgaXMgZXF1YWwgdG9cbiAgICogaXRzIGV4cGVjdGVkIHZhbHVlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmV4aXN0OyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdCgwKS50by5lcXVhbCgwKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgwKS50by5leGlzdDsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuZXhpc3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KG51bGwpLnRvLmJlLm51bGw7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QobnVsbCkudG8ubm90LmV4aXN0OyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdCh1bmRlZmluZWQpLnRvLmJlLnVuZGVmaW5lZDsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh1bmRlZmluZWQpLnRvLm5vdC5leGlzdDsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgY2FuIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QobnVsbCwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmV4aXN0O1xuICAgKlxuICAgKiBUaGUgYWxpYXMgYC5leGlzdHNgIGNhbiBiZSB1c2VkIGludGVyY2hhbmdlYWJseSB3aXRoIGAuZXhpc3RgLlxuICAgKlxuICAgKiBAbmFtZSBleGlzdFxuICAgKiBAYWxpYXMgZXhpc3RzXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydEV4aXN0ICgpIHtcbiAgICB2YXIgdmFsID0gZmxhZyh0aGlzLCAnb2JqZWN0Jyk7XG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIHZhbCAhPT0gbnVsbCAmJiB2YWwgIT09IHVuZGVmaW5lZFxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBleGlzdCdcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGV4aXN0J1xuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2V4aXN0JywgYXNzZXJ0RXhpc3QpO1xuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2V4aXN0cycsIGFzc2VydEV4aXN0KTtcblxuICAvKipcbiAgICogIyMjIC5lbXB0eVxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgYSBzdHJpbmcgb3IgYXJyYXksIGAuZW1wdHlgIGFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0J3NcbiAgICogYGxlbmd0aGAgcHJvcGVydHkgaXMgc3RyaWN0bHkgKGA9PT1gKSBlcXVhbCB0byBgMGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoW10pLnRvLmJlLmVtcHR5O1xuICAgKiAgICAgZXhwZWN0KCcnKS50by5iZS5lbXB0eTtcbiAgICpcbiAgICogV2hlbiB0aGUgdGFyZ2V0IGlzIGEgbWFwIG9yIHNldCwgYC5lbXB0eWAgYXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQncyBgc2l6ZWBcbiAgICogcHJvcGVydHkgaXMgc3RyaWN0bHkgZXF1YWwgdG8gYDBgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KG5ldyBTZXQoKSkudG8uYmUuZW1wdHk7XG4gICAqICAgICBleHBlY3QobmV3IE1hcCgpKS50by5iZS5lbXB0eTtcbiAgICpcbiAgICogV2hlbiB0aGUgdGFyZ2V0IGlzIGEgbm9uLWZ1bmN0aW9uIG9iamVjdCwgYC5lbXB0eWAgYXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXRcbiAgICogZG9lc24ndCBoYXZlIGFueSBvd24gZW51bWVyYWJsZSBwcm9wZXJ0aWVzLiBQcm9wZXJ0aWVzIHdpdGggU3ltYm9sLWJhc2VkXG4gICAqIGtleXMgYXJlIGV4Y2x1ZGVkIGZyb20gdGhlIGNvdW50LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHt9KS50by5iZS5lbXB0eTtcbiAgICpcbiAgICogQmVjYXVzZSBgLmVtcHR5YCBkb2VzIGRpZmZlcmVudCB0aGluZ3MgYmFzZWQgb24gdGhlIHRhcmdldCdzIHR5cGUsIGl0J3NcbiAgICogaW1wb3J0YW50IHRvIGNoZWNrIHRoZSB0YXJnZXQncyB0eXBlIGJlZm9yZSB1c2luZyBgLmVtcHR5YC4gU2VlIHRoZSBgLmFgXG4gICAqIGRvYyBmb3IgaW5mbyBvbiB0ZXN0aW5nIGEgdGFyZ2V0J3MgdHlwZS5cbiAgICpcbiAgICogICAgIGV4cGVjdChbXSkudG8uYmUuYW4oJ2FycmF5JykudGhhdC5pcy5lbXB0eTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5lbXB0eWAuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW5cbiAgICogYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGNvbnRhaW5zIGl0cyBleHBlY3RlZCBudW1iZXIgb2YgdmFsdWVzLFxuICAgKiByYXRoZXIgdGhhbiBhc3NlcnRpbmcgdGhhdCBpdCdzIG5vdCBlbXB0eS5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoT2YoMyk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5ub3QuYmUuZW1wdHk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiAgICAgZXhwZWN0KG5ldyBTZXQoWzEsIDIsIDNdKSkudG8uaGF2ZS5wcm9wZXJ0eSgnc2l6ZScsIDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KG5ldyBTZXQoWzEsIDIsIDNdKSkudG8ubm90LmJlLmVtcHR5OyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdChPYmplY3Qua2V5cyh7YTogMX0pKS50by5oYXZlLmxlbmd0aE9mKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ubm90LmJlLmVtcHR5OyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQSBjdXN0b20gZXJyb3IgbWVzc2FnZSBjYW4gYmUgZ2l2ZW4gYXMgdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10sICdub29vIHdoeSBmYWlsPz8nKS50by5iZS5lbXB0eTtcbiAgICpcbiAgICogQG5hbWUgZW1wdHlcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdlbXB0eScsIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmFsID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKVxuICAgICAgLCBmbGFnTXNnID0gZmxhZyh0aGlzLCAnbWVzc2FnZScpXG4gICAgICAsIGl0ZW1zQ291bnQ7XG5cbiAgICBmbGFnTXNnID0gZmxhZ01zZyA/IGZsYWdNc2cgKyAnOiAnIDogJyc7XG5cbiAgICBzd2l0Y2ggKF8udHlwZSh2YWwpLnRvTG93ZXJDYXNlKCkpIHtcbiAgICAgIGNhc2UgJ2FycmF5JzpcbiAgICAgIGNhc2UgJ3N0cmluZyc6XG4gICAgICAgIGl0ZW1zQ291bnQgPSB2YWwubGVuZ3RoO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJ21hcCc6XG4gICAgICBjYXNlICdzZXQnOlxuICAgICAgICBpdGVtc0NvdW50ID0gdmFsLnNpemU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnd2Vha21hcCc6XG4gICAgICBjYXNlICd3ZWFrc2V0JzpcbiAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICAgIGZsYWdNc2cgKyAnLmVtcHR5IHdhcyBwYXNzZWQgYSB3ZWFrIGNvbGxlY3Rpb24nLFxuICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICBzc2ZpXG4gICAgICAgICk7XG4gICAgICBjYXNlICdmdW5jdGlvbic6XG4gICAgICAgIHZhciBtc2cgPSBmbGFnTXNnICsgJy5lbXB0eSB3YXMgcGFzc2VkIGEgZnVuY3Rpb24gJyArIF8uZ2V0TmFtZSh2YWwpO1xuICAgICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IobXNnLnRyaW0oKSwgdW5kZWZpbmVkLCBzc2ZpKTtcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlmICh2YWwgIT09IE9iamVjdCh2YWwpKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICAgICAgZmxhZ01zZyArICcuZW1wdHkgd2FzIHBhc3NlZCBub24tc3RyaW5nIHByaW1pdGl2ZSAnICsgXy5pbnNwZWN0KHZhbCksXG4gICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICBzc2ZpXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICBpdGVtc0NvdW50ID0gT2JqZWN0LmtleXModmFsKS5sZW5ndGg7XG4gICAgfVxuXG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIDAgPT09IGl0ZW1zQ291bnRcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgZW1wdHknXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IG5vdCB0byBiZSBlbXB0eSdcbiAgICApO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5hcmd1bWVudHNcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYW4gYGFyZ3VtZW50c2Agb2JqZWN0LlxuICAgKlxuICAgKiAgICAgZnVuY3Rpb24gdGVzdCAoKSB7XG4gICAqICAgICAgIGV4cGVjdChhcmd1bWVudHMpLnRvLmJlLmFyZ3VtZW50cztcbiAgICogICAgIH1cbiAgICpcbiAgICogICAgIHRlc3QoKTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5hcmd1bWVudHNgLiBIb3dldmVyLCBpdCdzIG9mdGVuXG4gICAqIGJlc3QgdG8gYXNzZXJ0IHdoaWNoIHR5cGUgdGhlIHRhcmdldCBpcyBleHBlY3RlZCB0byBiZSwgcmF0aGVyIHRoYW5cbiAgICogYXNzZXJ0aW5nIHRoYXQgaXTigJlzIG5vdCBhbiBgYXJndW1lbnRzYCBvYmplY3QuXG4gICAqXG4gICAqICAgICBleHBlY3QoJ2ZvbycpLnRvLmJlLmEoJ3N0cmluZycpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5ub3QuYmUuYXJndW1lbnRzOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQSBjdXN0b20gZXJyb3IgbWVzc2FnZSBjYW4gYmUgZ2l2ZW4gYXMgdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7fSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmFyZ3VtZW50cztcbiAgICpcbiAgICogVGhlIGFsaWFzIGAuQXJndW1lbnRzYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLmFyZ3VtZW50c2AuXG4gICAqXG4gICAqIEBuYW1lIGFyZ3VtZW50c1xuICAgKiBAYWxpYXMgQXJndW1lbnRzXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGNoZWNrQXJndW1lbnRzICgpIHtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgdHlwZSA9IF8udHlwZShvYmopO1xuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAnQXJndW1lbnRzJyA9PT0gdHlwZVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSBhcmd1bWVudHMgYnV0IGdvdCAnICsgdHlwZVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgYmUgYXJndW1lbnRzJ1xuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2FyZ3VtZW50cycsIGNoZWNrQXJndW1lbnRzKTtcbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdBcmd1bWVudHMnLCBjaGVja0FyZ3VtZW50cyk7XG5cbiAgLyoqXG4gICAqICMjIyAuZXF1YWwodmFsWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgc3RyaWN0bHkgKGA9PT1gKSBlcXVhbCB0byB0aGUgZ2l2ZW4gYHZhbGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMSkudG8uZXF1YWwoMSk7XG4gICAqICAgICBleHBlY3QoJ2ZvbycpLnRvLmVxdWFsKCdmb28nKTtcbiAgICpcbiAgICogQWRkIGAuZGVlcGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gdXNlIGRlZXAgZXF1YWxpdHkgaW5zdGVhZC4gU2VlIHRoZVxuICAgKiBgZGVlcC1lcWxgIHByb2plY3QgcGFnZSBmb3IgaW5mbyBvbiB0aGUgZGVlcCBlcXVhbGl0eSBhbGdvcml0aG06XG4gICAqIGh0dHBzOi8vZ2l0aHViLmNvbS9jaGFpanMvZGVlcC1lcWwuXG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgb2JqZWN0IGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgZXF1YWxzIGB7YTogMX1gXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5kZWVwLmVxdWFsKHthOiAxfSk7XG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5ub3QuZXF1YWwoe2E6IDF9KTtcbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBhcnJheSBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGVxdWFscyBgWzEsIDJdYFxuICAgKiAgICAgZXhwZWN0KFsxLCAyXSkudG8uZGVlcC5lcXVhbChbMSwgMl0pO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyXSkudG8ubm90LmVxdWFsKFsxLCAyXSk7XG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuZXF1YWxgLiBIb3dldmVyLCBpdCdzIG9mdGVuXG4gICAqIGJlc3QgdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCBpcyBlcXVhbCB0byBpdHMgZXhwZWN0ZWQgdmFsdWUsIHJhdGhlciB0aGFuXG4gICAqIG5vdCBlcXVhbCB0byBvbmUgb2YgY291bnRsZXNzIHVuZXhwZWN0ZWQgdmFsdWVzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5lcXVhbCgyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAuZXF1YWxgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5lcXVhbCgyLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoMSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmVxdWFsKDIpO1xuICAgKlxuICAgKiBUaGUgYWxpYXNlcyBgLmVxdWFsc2AgYW5kIGBlcWAgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGggYC5lcXVhbGAuXG4gICAqXG4gICAqIEBuYW1lIGVxdWFsXG4gICAqIEBhbGlhcyBlcXVhbHNcbiAgICogQGFsaWFzIGVxXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gYXNzZXJ0RXF1YWwgKHZhbCwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpO1xuICAgIGlmIChmbGFnKHRoaXMsICdkZWVwJykpIHtcbiAgICAgIHZhciBwcmV2TG9ja1NzZmkgPSBmbGFnKHRoaXMsICdsb2NrU3NmaScpO1xuICAgICAgZmxhZyh0aGlzLCAnbG9ja1NzZmknLCB0cnVlKTtcbiAgICAgIHRoaXMuZXFsKHZhbCk7XG4gICAgICBmbGFnKHRoaXMsICdsb2NrU3NmaScsIHByZXZMb2NrU3NmaSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIHZhbCA9PT0gb2JqXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gZXF1YWwgI3tleHB9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBlcXVhbCAje2V4cH0nXG4gICAgICAgICwgdmFsXG4gICAgICAgICwgdGhpcy5fb2JqXG4gICAgICAgICwgdHJ1ZVxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdlcXVhbCcsIGFzc2VydEVxdWFsKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnZXF1YWxzJywgYXNzZXJ0RXF1YWwpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdlcScsIGFzc2VydEVxdWFsKTtcblxuICAvKipcbiAgICogIyMjIC5lcWwob2JqWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgZGVlcGx5IGVxdWFsIHRvIHRoZSBnaXZlbiBgb2JqYC4gU2VlIHRoZVxuICAgKiBgZGVlcC1lcWxgIHByb2plY3QgcGFnZSBmb3IgaW5mbyBvbiB0aGUgZGVlcCBlcXVhbGl0eSBhbGdvcml0aG06XG4gICAqIGh0dHBzOi8vZ2l0aHViLmNvbS9jaGFpanMvZGVlcC1lcWwuXG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgb2JqZWN0IGlzIGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgZXF1YWwgdG8ge2E6IDF9XG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5lcWwoe2E6IDF9KS5idXQubm90LmVxdWFsKHthOiAxfSk7XG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgYXJyYXkgaXMgZGVlcGx5IChidXQgbm90IHN0cmljdGx5KSBlcXVhbCB0byBbMSwgMl1cbiAgICogICAgIGV4cGVjdChbMSwgMl0pLnRvLmVxbChbMSwgMl0pLmJ1dC5ub3QuZXF1YWwoWzEsIDJdKTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5lcWxgLiBIb3dldmVyLCBpdCdzIG9mdGVuIGJlc3RcbiAgICogdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCBpcyBkZWVwbHkgZXF1YWwgdG8gaXRzIGV4cGVjdGVkIHZhbHVlLCByYXRoZXJcbiAgICogdGhhbiBub3QgZGVlcGx5IGVxdWFsIHRvIG9uZSBvZiBjb3VudGxlc3MgdW5leHBlY3RlZCB2YWx1ZXMuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5lcWwoe2E6IDF9KTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLm5vdC5lcWwoe2I6IDJ9KTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAuZXFsYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yIG1lc3NhZ2VcbiAgICogdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzIHRoZVxuICAgKiBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5lcWwoe2I6IDJ9LCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3Qoe2E6IDF9LCAnbm9vbyB3aHkgZmFpbD8/JykudG8uZXFsKHtiOiAyfSk7XG4gICAqXG4gICAqIFRoZSBhbGlhcyBgLmVxbHNgIGNhbiBiZSB1c2VkIGludGVyY2hhbmdlYWJseSB3aXRoIGAuZXFsYC5cbiAgICpcbiAgICogVGhlIGAuZGVlcC5lcXVhbGAgYXNzZXJ0aW9uIGlzIGFsbW9zdCBpZGVudGljYWwgdG8gYC5lcWxgIGJ1dCB3aXRoIG9uZVxuICAgKiBkaWZmZXJlbmNlOiBgLmRlZXAuZXF1YWxgIGNhdXNlcyBkZWVwIGVxdWFsaXR5IGNvbXBhcmlzb25zIHRvIGFsc28gYmUgdXNlZFxuICAgKiBmb3IgYW55IG90aGVyIGFzc2VydGlvbnMgdGhhdCBmb2xsb3cgaW4gdGhlIGNoYWluLlxuICAgKlxuICAgKiBAbmFtZSBlcWxcbiAgICogQGFsaWFzIGVxbHNcbiAgICogQHBhcmFtIHtNaXhlZH0gb2JqXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRFcWwob2JqLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgXy5lcWwob2JqLCBmbGFnKHRoaXMsICdvYmplY3QnKSlcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gZGVlcGx5IGVxdWFsICN7ZXhwfSdcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGRlZXBseSBlcXVhbCAje2V4cH0nXG4gICAgICAsIG9ialxuICAgICAgLCB0aGlzLl9vYmpcbiAgICAgICwgdHJ1ZVxuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdlcWwnLCBhc3NlcnRFcWwpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdlcWxzJywgYXNzZXJ0RXFsKTtcblxuICAvKipcbiAgICogIyMjIC5hYm92ZShuWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIgb3IgYSBkYXRlIGdyZWF0ZXIgdGhhbiB0aGUgZ2l2ZW4gbnVtYmVyIG9yIGRhdGUgYG5gIHJlc3BlY3RpdmVseS5cbiAgICogSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQgaXMgZXF1YWwgdG8gaXRzIGV4cGVjdGVkXG4gICAqIHZhbHVlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmVxdWFsKDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmJlLmFib3ZlKDEpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubGVuZ3RoT2ZgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQncyBgbGVuZ3RoYFxuICAgKiBvciBgc2l6ZWAgaXMgZ3JlYXRlciB0aGFuIHRoZSBnaXZlbiBudW1iZXIgYG5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mKDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mLmFib3ZlKDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoT2YoMyk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLmxlbmd0aE9mLmFib3ZlKDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5hYm92ZWAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMikudG8uZXF1YWwoMik7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMSkudG8ubm90LmJlLmFib3ZlKDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogYC5hYm92ZWAgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvciBtZXNzYWdlXG4gICAqIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhcyB0aGVcbiAgICogc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmJlLmFib3ZlKDIsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdCgxLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUuYWJvdmUoMik7XG4gICAqXG4gICAqIFRoZSBhbGlhc2VzIGAuZ3RgIGFuZCBgLmdyZWF0ZXJUaGFuYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aFxuICAgKiBgLmFib3ZlYC5cbiAgICpcbiAgICogQG5hbWUgYWJvdmVcbiAgICogQGFsaWFzIGd0XG4gICAqIEBhbGlhcyBncmVhdGVyVGhhblxuICAgKiBAcGFyYW0ge051bWJlcn0gblxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gYXNzZXJ0QWJvdmUgKG4sIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBkb0xlbmd0aCA9IGZsYWcodGhpcywgJ2RvTGVuZ3RoJylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBtc2dQcmVmaXggPSAoKGZsYWdNc2cpID8gZmxhZ01zZyArICc6ICcgOiAnJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKVxuICAgICAgLCBvYmpUeXBlID0gXy50eXBlKG9iaikudG9Mb3dlckNhc2UoKVxuICAgICAgLCBuVHlwZSA9IF8udHlwZShuKS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIGVycm9yTWVzc2FnZVxuICAgICAgLCBzaG91bGRUaHJvdyA9IHRydWU7XG5cbiAgICBpZiAoZG9MZW5ndGggJiYgb2JqVHlwZSAhPT0gJ21hcCcgJiYgb2JqVHlwZSAhPT0gJ3NldCcpIHtcbiAgICAgIG5ldyBBc3NlcnRpb24ob2JqLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS50by5oYXZlLnByb3BlcnR5KCdsZW5ndGgnKTtcbiAgICB9XG5cbiAgICBpZiAoIWRvTGVuZ3RoICYmIChvYmpUeXBlID09PSAnZGF0ZScgJiYgblR5cGUgIT09ICdkYXRlJykpIHtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICd0aGUgYXJndW1lbnQgdG8gYWJvdmUgbXVzdCBiZSBhIGRhdGUnO1xuICAgIH0gZWxzZSBpZiAoblR5cGUgIT09ICdudW1iZXInICYmIChkb0xlbmd0aCB8fCBvYmpUeXBlID09PSAnbnVtYmVyJykpIHtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICd0aGUgYXJndW1lbnQgdG8gYWJvdmUgbXVzdCBiZSBhIG51bWJlcic7XG4gICAgfSBlbHNlIGlmICghZG9MZW5ndGggJiYgKG9ialR5cGUgIT09ICdkYXRlJyAmJiBvYmpUeXBlICE9PSAnbnVtYmVyJykpIHtcbiAgICAgIHZhciBwcmludE9iaiA9IChvYmpUeXBlID09PSAnc3RyaW5nJykgPyBcIidcIiArIG9iaiArIFwiJ1wiIDogb2JqO1xuICAgICAgZXJyb3JNZXNzYWdlID0gbXNnUHJlZml4ICsgJ2V4cGVjdGVkICcgKyBwcmludE9iaiArICcgdG8gYmUgYSBudW1iZXIgb3IgYSBkYXRlJztcbiAgICB9IGVsc2Uge1xuICAgICAgc2hvdWxkVGhyb3cgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBpZiAoc2hvdWxkVGhyb3cpIHtcbiAgICAgIHRocm93IG5ldyBBc3NlcnRpb25FcnJvcihlcnJvck1lc3NhZ2UsIHVuZGVmaW5lZCwgc3NmaSk7XG4gICAgfVxuXG4gICAgaWYgKGRvTGVuZ3RoKSB7XG4gICAgICB2YXIgZGVzY3JpcHRvciA9ICdsZW5ndGgnXG4gICAgICAgICwgaXRlbXNDb3VudDtcbiAgICAgIGlmIChvYmpUeXBlID09PSAnbWFwJyB8fCBvYmpUeXBlID09PSAnc2V0Jykge1xuICAgICAgICBkZXNjcmlwdG9yID0gJ3NpemUnO1xuICAgICAgICBpdGVtc0NvdW50ID0gb2JqLnNpemU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpdGVtc0NvdW50ID0gb2JqLmxlbmd0aDtcbiAgICAgIH1cbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIGl0ZW1zQ291bnQgPiBuXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSBhICcgKyBkZXNjcmlwdG9yICsgJyBhYm92ZSAje2V4cH0gYnV0IGdvdCAje2FjdH0nXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGhhdmUgYSAnICsgZGVzY3JpcHRvciArICcgYWJvdmUgI3tleHB9J1xuICAgICAgICAsIG5cbiAgICAgICAgLCBpdGVtc0NvdW50XG4gICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBvYmogPiBuXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgYWJvdmUgI3tleHB9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGF0IG1vc3QgI3tleHB9J1xuICAgICAgICAsIG5cbiAgICAgICk7XG4gICAgfVxuICB9XG5cbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnYWJvdmUnLCBhc3NlcnRBYm92ZSk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2d0JywgYXNzZXJ0QWJvdmUpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdncmVhdGVyVGhhbicsIGFzc2VydEFib3ZlKTtcblxuICAvKipcbiAgICogIyMjIC5sZWFzdChuWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIgb3IgYSBkYXRlIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byB0aGUgZ2l2ZW5cbiAgICogbnVtYmVyIG9yIGRhdGUgYG5gIHJlc3BlY3RpdmVseS4gSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQgaXMgZXF1YWwgdG9cbiAgICogaXRzIGV4cGVjdGVkIHZhbHVlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmVxdWFsKDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmJlLmF0LmxlYXN0KDEpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgyKS50by5iZS5hdC5sZWFzdCgyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLmxlbmd0aE9mYCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0J3MgYGxlbmd0aGBcbiAgICogb3IgYHNpemVgIGlzIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byB0aGUgZ2l2ZW4gbnVtYmVyIGBuYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZigzKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZi5hdC5sZWFzdCgyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLmxlbmd0aE9mKDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZi5hdC5sZWFzdCgyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAubGVhc3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5iZS5hdC5sZWFzdCgyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAubGVhc3RgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5iZS5hdC5sZWFzdCgyLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoMSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmF0LmxlYXN0KDIpO1xuICAgKlxuICAgKiBUaGUgYWxpYXNlcyBgLmd0ZWAgYW5kIGAuZ3JlYXRlclRoYW5PckVxdWFsYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aFxuICAgKiBgLmxlYXN0YC5cbiAgICpcbiAgICogQG5hbWUgbGVhc3RcbiAgICogQGFsaWFzIGd0ZVxuICAgKiBAYWxpYXMgZ3JlYXRlclRoYW5PckVxdWFsXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBuXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRMZWFzdCAobiwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIGRvTGVuZ3RoID0gZmxhZyh0aGlzLCAnZG9MZW5ndGgnKVxuICAgICAgLCBmbGFnTXNnID0gZmxhZyh0aGlzLCAnbWVzc2FnZScpXG4gICAgICAsIG1zZ1ByZWZpeCA9ICgoZmxhZ01zZykgPyBmbGFnTXNnICsgJzogJyA6ICcnKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpXG4gICAgICAsIG9ialR5cGUgPSBfLnR5cGUob2JqKS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIG5UeXBlID0gXy50eXBlKG4pLnRvTG93ZXJDYXNlKClcbiAgICAgICwgZXJyb3JNZXNzYWdlXG4gICAgICAsIHNob3VsZFRocm93ID0gdHJ1ZTtcblxuICAgIGlmIChkb0xlbmd0aCAmJiBvYmpUeXBlICE9PSAnbWFwJyAmJiBvYmpUeXBlICE9PSAnc2V0Jykge1xuICAgICAgbmV3IEFzc2VydGlvbihvYmosIGZsYWdNc2csIHNzZmksIHRydWUpLnRvLmhhdmUucHJvcGVydHkoJ2xlbmd0aCcpO1xuICAgIH1cblxuICAgIGlmICghZG9MZW5ndGggJiYgKG9ialR5cGUgPT09ICdkYXRlJyAmJiBuVHlwZSAhPT0gJ2RhdGUnKSkge1xuICAgICAgZXJyb3JNZXNzYWdlID0gbXNnUHJlZml4ICsgJ3RoZSBhcmd1bWVudCB0byBsZWFzdCBtdXN0IGJlIGEgZGF0ZSc7XG4gICAgfSBlbHNlIGlmIChuVHlwZSAhPT0gJ251bWJlcicgJiYgKGRvTGVuZ3RoIHx8IG9ialR5cGUgPT09ICdudW1iZXInKSkge1xuICAgICAgZXJyb3JNZXNzYWdlID0gbXNnUHJlZml4ICsgJ3RoZSBhcmd1bWVudCB0byBsZWFzdCBtdXN0IGJlIGEgbnVtYmVyJztcbiAgICB9IGVsc2UgaWYgKCFkb0xlbmd0aCAmJiAob2JqVHlwZSAhPT0gJ2RhdGUnICYmIG9ialR5cGUgIT09ICdudW1iZXInKSkge1xuICAgICAgdmFyIHByaW50T2JqID0gKG9ialR5cGUgPT09ICdzdHJpbmcnKSA/IFwiJ1wiICsgb2JqICsgXCInXCIgOiBvYmo7XG4gICAgICBlcnJvck1lc3NhZ2UgPSBtc2dQcmVmaXggKyAnZXhwZWN0ZWQgJyArIHByaW50T2JqICsgJyB0byBiZSBhIG51bWJlciBvciBhIGRhdGUnO1xuICAgIH0gZWxzZSB7XG4gICAgICBzaG91bGRUaHJvdyA9IGZhbHNlO1xuICAgIH1cblxuICAgIGlmIChzaG91bGRUaHJvdykge1xuICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKGVycm9yTWVzc2FnZSwgdW5kZWZpbmVkLCBzc2ZpKTtcbiAgICB9XG5cbiAgICBpZiAoZG9MZW5ndGgpIHtcbiAgICAgIHZhciBkZXNjcmlwdG9yID0gJ2xlbmd0aCdcbiAgICAgICAgLCBpdGVtc0NvdW50O1xuICAgICAgaWYgKG9ialR5cGUgPT09ICdtYXAnIHx8IG9ialR5cGUgPT09ICdzZXQnKSB7XG4gICAgICAgIGRlc2NyaXB0b3IgPSAnc2l6ZSc7XG4gICAgICAgIGl0ZW1zQ291bnQgPSBvYmouc2l6ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGl0ZW1zQ291bnQgPSBvYmoubGVuZ3RoO1xuICAgICAgfVxuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgaXRlbXNDb3VudCA+PSBuXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSBhICcgKyBkZXNjcmlwdG9yICsgJyBhdCBsZWFzdCAje2V4cH0gYnV0IGdvdCAje2FjdH0nXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSBhICcgKyBkZXNjcmlwdG9yICsgJyBiZWxvdyAje2V4cH0nXG4gICAgICAgICwgblxuICAgICAgICAsIGl0ZW1zQ291bnRcbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIG9iaiA+PSBuXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgYXQgbGVhc3QgI3tleHB9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGJlbG93ICN7ZXhwfSdcbiAgICAgICAgLCBuXG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2xlYXN0JywgYXNzZXJ0TGVhc3QpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdndGUnLCBhc3NlcnRMZWFzdCk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2dyZWF0ZXJUaGFuT3JFcXVhbCcsIGFzc2VydExlYXN0KTtcblxuICAvKipcbiAgICogIyMjIC5iZWxvdyhuWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIgb3IgYSBkYXRlIGxlc3MgdGhhbiB0aGUgZ2l2ZW4gbnVtYmVyIG9yIGRhdGUgYG5gIHJlc3BlY3RpdmVseS5cbiAgICogSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQgaXMgZXF1YWwgdG8gaXRzIGV4cGVjdGVkXG4gICAqIHZhbHVlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmJlLmJlbG93KDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubGVuZ3RoT2ZgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQncyBgbGVuZ3RoYFxuICAgKiBvciBgc2l6ZWAgaXMgbGVzcyB0aGFuIHRoZSBnaXZlbiBudW1iZXIgYG5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mKDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mLmJlbG93KDQpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoKDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZi5iZWxvdyg0KTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuYmVsb3dgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmVxdWFsKDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDIpLnRvLm5vdC5iZS5iZWxvdygxKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAuYmVsb3dgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgyKS50by5iZS5iZWxvdygxLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoMiwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmJlbG93KDEpO1xuICAgKlxuICAgKiBUaGUgYWxpYXNlcyBgLmx0YCBhbmQgYC5sZXNzVGhhbmAgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGhcbiAgICogYC5iZWxvd2AuXG4gICAqXG4gICAqIEBuYW1lIGJlbG93XG4gICAqIEBhbGlhcyBsdFxuICAgKiBAYWxpYXMgbGVzc1RoYW5cbiAgICogQHBhcmFtIHtOdW1iZXJ9IG5cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydEJlbG93IChuLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgZG9MZW5ndGggPSBmbGFnKHRoaXMsICdkb0xlbmd0aCcpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgbXNnUHJlZml4ID0gKChmbGFnTXNnKSA/IGZsYWdNc2cgKyAnOiAnIDogJycpXG4gICAgICAsIHNzZmkgPSBmbGFnKHRoaXMsICdzc2ZpJylcbiAgICAgICwgb2JqVHlwZSA9IF8udHlwZShvYmopLnRvTG93ZXJDYXNlKClcbiAgICAgICwgblR5cGUgPSBfLnR5cGUobikudG9Mb3dlckNhc2UoKVxuICAgICAgLCBlcnJvck1lc3NhZ2VcbiAgICAgICwgc2hvdWxkVGhyb3cgPSB0cnVlO1xuXG4gICAgaWYgKGRvTGVuZ3RoICYmIG9ialR5cGUgIT09ICdtYXAnICYmIG9ialR5cGUgIT09ICdzZXQnKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkudG8uaGF2ZS5wcm9wZXJ0eSgnbGVuZ3RoJyk7XG4gICAgfVxuXG4gICAgaWYgKCFkb0xlbmd0aCAmJiAob2JqVHlwZSA9PT0gJ2RhdGUnICYmIG5UeXBlICE9PSAnZGF0ZScpKSB7XG4gICAgICBlcnJvck1lc3NhZ2UgPSBtc2dQcmVmaXggKyAndGhlIGFyZ3VtZW50IHRvIGJlbG93IG11c3QgYmUgYSBkYXRlJztcbiAgICB9IGVsc2UgaWYgKG5UeXBlICE9PSAnbnVtYmVyJyAmJiAoZG9MZW5ndGggfHwgb2JqVHlwZSA9PT0gJ251bWJlcicpKSB7XG4gICAgICBlcnJvck1lc3NhZ2UgPSBtc2dQcmVmaXggKyAndGhlIGFyZ3VtZW50IHRvIGJlbG93IG11c3QgYmUgYSBudW1iZXInO1xuICAgIH0gZWxzZSBpZiAoIWRvTGVuZ3RoICYmIChvYmpUeXBlICE9PSAnZGF0ZScgJiYgb2JqVHlwZSAhPT0gJ251bWJlcicpKSB7XG4gICAgICB2YXIgcHJpbnRPYmogPSAob2JqVHlwZSA9PT0gJ3N0cmluZycpID8gXCInXCIgKyBvYmogKyBcIidcIiA6IG9iajtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICdleHBlY3RlZCAnICsgcHJpbnRPYmogKyAnIHRvIGJlIGEgbnVtYmVyIG9yIGEgZGF0ZSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNob3VsZFRocm93ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKHNob3VsZFRocm93KSB7XG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoZXJyb3JNZXNzYWdlLCB1bmRlZmluZWQsIHNzZmkpO1xuICAgIH1cblxuICAgIGlmIChkb0xlbmd0aCkge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSAnbGVuZ3RoJ1xuICAgICAgICAsIGl0ZW1zQ291bnQ7XG4gICAgICBpZiAob2JqVHlwZSA9PT0gJ21hcCcgfHwgb2JqVHlwZSA9PT0gJ3NldCcpIHtcbiAgICAgICAgZGVzY3JpcHRvciA9ICdzaXplJztcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5zaXplO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5sZW5ndGg7XG4gICAgICB9XG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBpdGVtc0NvdW50IDwgblxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGhhdmUgYSAnICsgZGVzY3JpcHRvciArICcgYmVsb3cgI3tleHB9IGJ1dCBnb3QgI3thY3R9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBoYXZlIGEgJyArIGRlc2NyaXB0b3IgKyAnIGJlbG93ICN7ZXhwfSdcbiAgICAgICAgLCBuXG4gICAgICAgICwgaXRlbXNDb3VudFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgb2JqIDwgblxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGJlbG93ICN7ZXhwfSdcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSBhdCBsZWFzdCAje2V4cH0nXG4gICAgICAgICwgblxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdiZWxvdycsIGFzc2VydEJlbG93KTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnbHQnLCBhc3NlcnRCZWxvdyk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2xlc3NUaGFuJywgYXNzZXJ0QmVsb3cpO1xuXG4gIC8qKlxuICAgKiAjIyMgLm1vc3QoblssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIGEgbnVtYmVyIG9yIGEgZGF0ZSBsZXNzIHRoYW4gb3IgZXF1YWwgdG8gdGhlIGdpdmVuIG51bWJlclxuICAgKiBvciBkYXRlIGBuYCByZXNwZWN0aXZlbHkuIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0c1xuICAgKiBleHBlY3RlZCB2YWx1ZS5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5lcXVhbCgxKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxKS50by5iZS5hdC5tb3N0KDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxKS50by5iZS5hdC5tb3N0KDEpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubGVuZ3RoT2ZgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQncyBgbGVuZ3RoYFxuICAgKiBvciBgc2l6ZWAgaXMgbGVzcyB0aGFuIG9yIGVxdWFsIHRvIHRoZSBnaXZlbiBudW1iZXIgYG5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mKDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5oYXZlLmxlbmd0aE9mLmF0Lm1vc3QoNCk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZigzKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoT2YuYXQubW9zdCg0KTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAubW9zdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMikudG8uZXF1YWwoMik7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMikudG8ubm90LmJlLmF0Lm1vc3QoMSk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBgLm1vc3RgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgyKS50by5iZS5hdC5tb3N0KDEsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdCgyLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUuYXQubW9zdCgxKTtcbiAgICpcbiAgICogVGhlIGFsaWFzZXMgYC5sdGVgIGFuZCBgLmxlc3NUaGFuT3JFcXVhbGAgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGhcbiAgICogYC5tb3N0YC5cbiAgICpcbiAgICogQG5hbWUgbW9zdFxuICAgKiBAYWxpYXMgbHRlXG4gICAqIEBhbGlhcyBsZXNzVGhhbk9yRXF1YWxcbiAgICogQHBhcmFtIHtOdW1iZXJ9IG5cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydE1vc3QgKG4sIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBkb0xlbmd0aCA9IGZsYWcodGhpcywgJ2RvTGVuZ3RoJylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBtc2dQcmVmaXggPSAoKGZsYWdNc2cpID8gZmxhZ01zZyArICc6ICcgOiAnJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKVxuICAgICAgLCBvYmpUeXBlID0gXy50eXBlKG9iaikudG9Mb3dlckNhc2UoKVxuICAgICAgLCBuVHlwZSA9IF8udHlwZShuKS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIGVycm9yTWVzc2FnZVxuICAgICAgLCBzaG91bGRUaHJvdyA9IHRydWU7XG5cbiAgICBpZiAoZG9MZW5ndGggJiYgb2JqVHlwZSAhPT0gJ21hcCcgJiYgb2JqVHlwZSAhPT0gJ3NldCcpIHtcbiAgICAgIG5ldyBBc3NlcnRpb24ob2JqLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS50by5oYXZlLnByb3BlcnR5KCdsZW5ndGgnKTtcbiAgICB9XG5cbiAgICBpZiAoIWRvTGVuZ3RoICYmIChvYmpUeXBlID09PSAnZGF0ZScgJiYgblR5cGUgIT09ICdkYXRlJykpIHtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICd0aGUgYXJndW1lbnQgdG8gbW9zdCBtdXN0IGJlIGEgZGF0ZSc7XG4gICAgfSBlbHNlIGlmIChuVHlwZSAhPT0gJ251bWJlcicgJiYgKGRvTGVuZ3RoIHx8IG9ialR5cGUgPT09ICdudW1iZXInKSkge1xuICAgICAgZXJyb3JNZXNzYWdlID0gbXNnUHJlZml4ICsgJ3RoZSBhcmd1bWVudCB0byBtb3N0IG11c3QgYmUgYSBudW1iZXInO1xuICAgIH0gZWxzZSBpZiAoIWRvTGVuZ3RoICYmIChvYmpUeXBlICE9PSAnZGF0ZScgJiYgb2JqVHlwZSAhPT0gJ251bWJlcicpKSB7XG4gICAgICB2YXIgcHJpbnRPYmogPSAob2JqVHlwZSA9PT0gJ3N0cmluZycpID8gXCInXCIgKyBvYmogKyBcIidcIiA6IG9iajtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICdleHBlY3RlZCAnICsgcHJpbnRPYmogKyAnIHRvIGJlIGEgbnVtYmVyIG9yIGEgZGF0ZSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNob3VsZFRocm93ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKHNob3VsZFRocm93KSB7XG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoZXJyb3JNZXNzYWdlLCB1bmRlZmluZWQsIHNzZmkpO1xuICAgIH1cblxuICAgIGlmIChkb0xlbmd0aCkge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSAnbGVuZ3RoJ1xuICAgICAgICAsIGl0ZW1zQ291bnQ7XG4gICAgICBpZiAob2JqVHlwZSA9PT0gJ21hcCcgfHwgb2JqVHlwZSA9PT0gJ3NldCcpIHtcbiAgICAgICAgZGVzY3JpcHRvciA9ICdzaXplJztcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5zaXplO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5sZW5ndGg7XG4gICAgICB9XG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBpdGVtc0NvdW50IDw9IG5cbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBoYXZlIGEgJyArIGRlc2NyaXB0b3IgKyAnIGF0IG1vc3QgI3tleHB9IGJ1dCBnb3QgI3thY3R9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGhhdmUgYSAnICsgZGVzY3JpcHRvciArICcgYWJvdmUgI3tleHB9J1xuICAgICAgICAsIG5cbiAgICAgICAgLCBpdGVtc0NvdW50XG4gICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBvYmogPD0gblxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGF0IG1vc3QgI3tleHB9J1xuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGFib3ZlICN7ZXhwfSdcbiAgICAgICAgLCBuXG4gICAgICApO1xuICAgIH1cbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ21vc3QnLCBhc3NlcnRNb3N0KTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnbHRlJywgYXNzZXJ0TW9zdCk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2xlc3NUaGFuT3JFcXVhbCcsIGFzc2VydE1vc3QpO1xuXG4gIC8qKlxuICAgKiAjIyMgLndpdGhpbihzdGFydCwgZmluaXNoWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIgb3IgYSBkYXRlIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byB0aGUgZ2l2ZW5cbiAgICogbnVtYmVyIG9yIGRhdGUgYHN0YXJ0YCwgYW5kIGxlc3MgdGhhbiBvciBlcXVhbCB0byB0aGUgZ2l2ZW4gbnVtYmVyIG9yIGRhdGUgYGZpbmlzaGAgcmVzcGVjdGl2ZWx5LlxuICAgKiBIb3dldmVyLCBpdCdzIG9mdGVuIGJlc3QgdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCBpcyBlcXVhbCB0byBpdHMgZXhwZWN0ZWRcbiAgICogdmFsdWUuXG4gICAqXG4gICAqICAgICBleHBlY3QoMikudG8uZXF1YWwoMik7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMikudG8uYmUud2l0aGluKDEsIDMpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgyKS50by5iZS53aXRoaW4oMiwgMyk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDIpLnRvLmJlLndpdGhpbigxLCAyKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLmxlbmd0aE9mYCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0J3MgYGxlbmd0aGBcbiAgICogb3IgYHNpemVgIGlzIGdyZWF0ZXIgdGhhbiBvciBlcXVhbCB0byB0aGUgZ2l2ZW4gbnVtYmVyIGBzdGFydGAsIGFuZCBsZXNzXG4gICAqIHRoYW4gb3IgZXF1YWwgdG8gdGhlIGdpdmVuIG51bWJlciBgZmluaXNoYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZigzKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZi53aXRoaW4oMiwgNCk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZigzKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoT2Yud2l0aGluKDIsIDQpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC53aXRoaW5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmVxdWFsKDEpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5iZS53aXRoaW4oMiwgNCk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBgLndpdGhpbmAgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvclxuICAgKiBtZXNzYWdlIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhc1xuICAgKiB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDQpLnRvLmJlLndpdGhpbigxLCAzLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoNCwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLndpdGhpbigxLCAzKTtcbiAgICpcbiAgICogQG5hbWUgd2l0aGluXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBzdGFydCBsb3dlciBib3VuZCBpbmNsdXNpdmVcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGZpbmlzaCB1cHBlciBib3VuZCBpbmNsdXNpdmVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ3dpdGhpbicsIGZ1bmN0aW9uIChzdGFydCwgZmluaXNoLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgZG9MZW5ndGggPSBmbGFnKHRoaXMsICdkb0xlbmd0aCcpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgbXNnUHJlZml4ID0gKChmbGFnTXNnKSA/IGZsYWdNc2cgKyAnOiAnIDogJycpXG4gICAgICAsIHNzZmkgPSBmbGFnKHRoaXMsICdzc2ZpJylcbiAgICAgICwgb2JqVHlwZSA9IF8udHlwZShvYmopLnRvTG93ZXJDYXNlKClcbiAgICAgICwgc3RhcnRUeXBlID0gXy50eXBlKHN0YXJ0KS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIGZpbmlzaFR5cGUgPSBfLnR5cGUoZmluaXNoKS50b0xvd2VyQ2FzZSgpXG4gICAgICAsIGVycm9yTWVzc2FnZVxuICAgICAgLCBzaG91bGRUaHJvdyA9IHRydWVcbiAgICAgICwgcmFuZ2UgPSAoc3RhcnRUeXBlID09PSAnZGF0ZScgJiYgZmluaXNoVHlwZSA9PT0gJ2RhdGUnKVxuICAgICAgICAgID8gc3RhcnQudG9VVENTdHJpbmcoKSArICcuLicgKyBmaW5pc2gudG9VVENTdHJpbmcoKVxuICAgICAgICAgIDogc3RhcnQgKyAnLi4nICsgZmluaXNoO1xuXG4gICAgaWYgKGRvTGVuZ3RoICYmIG9ialR5cGUgIT09ICdtYXAnICYmIG9ialR5cGUgIT09ICdzZXQnKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkudG8uaGF2ZS5wcm9wZXJ0eSgnbGVuZ3RoJyk7XG4gICAgfVxuXG4gICAgaWYgKCFkb0xlbmd0aCAmJiAob2JqVHlwZSA9PT0gJ2RhdGUnICYmIChzdGFydFR5cGUgIT09ICdkYXRlJyB8fCBmaW5pc2hUeXBlICE9PSAnZGF0ZScpKSkge1xuICAgICAgZXJyb3JNZXNzYWdlID0gbXNnUHJlZml4ICsgJ3RoZSBhcmd1bWVudHMgdG8gd2l0aGluIG11c3QgYmUgZGF0ZXMnO1xuICAgIH0gZWxzZSBpZiAoKHN0YXJ0VHlwZSAhPT0gJ251bWJlcicgfHwgZmluaXNoVHlwZSAhPT0gJ251bWJlcicpICYmIChkb0xlbmd0aCB8fCBvYmpUeXBlID09PSAnbnVtYmVyJykpIHtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICd0aGUgYXJndW1lbnRzIHRvIHdpdGhpbiBtdXN0IGJlIG51bWJlcnMnO1xuICAgIH0gZWxzZSBpZiAoIWRvTGVuZ3RoICYmIChvYmpUeXBlICE9PSAnZGF0ZScgJiYgb2JqVHlwZSAhPT0gJ251bWJlcicpKSB7XG4gICAgICB2YXIgcHJpbnRPYmogPSAob2JqVHlwZSA9PT0gJ3N0cmluZycpID8gXCInXCIgKyBvYmogKyBcIidcIiA6IG9iajtcbiAgICAgIGVycm9yTWVzc2FnZSA9IG1zZ1ByZWZpeCArICdleHBlY3RlZCAnICsgcHJpbnRPYmogKyAnIHRvIGJlIGEgbnVtYmVyIG9yIGEgZGF0ZSc7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNob3VsZFRocm93ID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKHNob3VsZFRocm93KSB7XG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoZXJyb3JNZXNzYWdlLCB1bmRlZmluZWQsIHNzZmkpO1xuICAgIH1cblxuICAgIGlmIChkb0xlbmd0aCkge1xuICAgICAgdmFyIGRlc2NyaXB0b3IgPSAnbGVuZ3RoJ1xuICAgICAgICAsIGl0ZW1zQ291bnQ7XG4gICAgICBpZiAob2JqVHlwZSA9PT0gJ21hcCcgfHwgb2JqVHlwZSA9PT0gJ3NldCcpIHtcbiAgICAgICAgZGVzY3JpcHRvciA9ICdzaXplJztcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5zaXplO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaXRlbXNDb3VudCA9IG9iai5sZW5ndGg7XG4gICAgICB9XG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBpdGVtc0NvdW50ID49IHN0YXJ0ICYmIGl0ZW1zQ291bnQgPD0gZmluaXNoXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSBhICcgKyBkZXNjcmlwdG9yICsgJyB3aXRoaW4gJyArIHJhbmdlXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGhhdmUgYSAnICsgZGVzY3JpcHRvciArICcgd2l0aGluICcgKyByYW5nZVxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgb2JqID49IHN0YXJ0ICYmIG9iaiA8PSBmaW5pc2hcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSB3aXRoaW4gJyArIHJhbmdlXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIHdpdGhpbiAnICsgcmFuZ2VcbiAgICAgICk7XG4gICAgfVxuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5pbnN0YW5jZW9mKGNvbnN0cnVjdG9yWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYW4gaW5zdGFuY2Ugb2YgdGhlIGdpdmVuIGBjb25zdHJ1Y3RvcmAuXG4gICAqXG4gICAqICAgICBmdW5jdGlvbiBDYXQgKCkgeyB9XG4gICAqXG4gICAqICAgICBleHBlY3QobmV3IENhdCgpKS50by5iZS5hbi5pbnN0YW5jZW9mKENhdCk7XG4gICAqICAgICBleHBlY3QoWzEsIDJdKS50by5iZS5hbi5pbnN0YW5jZW9mKEFycmF5KTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5pbnN0YW5jZW9mYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLm5vdC5iZS5hbi5pbnN0YW5jZW9mKEFycmF5KTtcbiAgICpcbiAgICogYC5pbnN0YW5jZW9mYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yXG4gICAqIG1lc3NhZ2UgdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzXG4gICAqIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMSkudG8uYmUuYW4uaW5zdGFuY2VvZihBcnJheSwgJ25vb28gd2h5IGZhaWw/PycpO1xuICAgKiAgICAgZXhwZWN0KDEsICdub29vIHdoeSBmYWlsPz8nKS50by5iZS5hbi5pbnN0YW5jZW9mKEFycmF5KTtcbiAgICpcbiAgICogRHVlIHRvIGxpbWl0YXRpb25zIGluIEVTNSwgYC5pbnN0YW5jZW9mYCBtYXkgbm90IGFsd2F5cyB3b3JrIGFzIGV4cGVjdGVkXG4gICAqIHdoZW4gdXNpbmcgYSB0cmFuc3BpbGVyIHN1Y2ggYXMgQmFiZWwgb3IgVHlwZVNjcmlwdC4gSW4gcGFydGljdWxhciwgaXQgbWF5XG4gICAqIHByb2R1Y2UgdW5leHBlY3RlZCByZXN1bHRzIHdoZW4gc3ViY2xhc3NpbmcgYnVpbHQtaW4gb2JqZWN0IHN1Y2ggYXNcbiAgICogYEFycmF5YCwgYEVycm9yYCwgYW5kIGBNYXBgLiBTZWUgeW91ciB0cmFuc3BpbGVyJ3MgZG9jcyBmb3IgZGV0YWlsczpcbiAgICpcbiAgICogLSAoW0JhYmVsXShodHRwczovL2JhYmVsanMuaW8vZG9jcy91c2FnZS9jYXZlYXRzLyNjbGFzc2VzKSlcbiAgICogLSAoW1R5cGVTY3JpcHRdKGh0dHBzOi8vZ2l0aHViLmNvbS9NaWNyb3NvZnQvVHlwZVNjcmlwdC93aWtpL0JyZWFraW5nLUNoYW5nZXMjZXh0ZW5kaW5nLWJ1aWx0LWlucy1saWtlLWVycm9yLWFycmF5LWFuZC1tYXAtbWF5LW5vLWxvbmdlci13b3JrKSlcbiAgICpcbiAgICogVGhlIGFsaWFzIGAuaW5zdGFuY2VPZmAgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGggYC5pbnN0YW5jZW9mYC5cbiAgICpcbiAgICogQG5hbWUgaW5zdGFuY2VvZlxuICAgKiBAcGFyYW0ge0NvbnN0cnVjdG9yfSBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQGFsaWFzIGluc3RhbmNlT2ZcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gYXNzZXJ0SW5zdGFuY2VPZiAoY29uc3RydWN0b3IsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuXG4gICAgdmFyIHRhcmdldCA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgdmFyIHNzZmkgPSBmbGFnKHRoaXMsICdzc2ZpJyk7XG4gICAgdmFyIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJyk7XG5cbiAgICB0cnkge1xuICAgICAgdmFyIGlzSW5zdGFuY2VPZiA9IHRhcmdldCBpbnN0YW5jZW9mIGNvbnN0cnVjdG9yO1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgaWYgKGVyciBpbnN0YW5jZW9mIFR5cGVFcnJvcikge1xuICAgICAgICBmbGFnTXNnID0gZmxhZ01zZyA/IGZsYWdNc2cgKyAnOiAnIDogJyc7XG4gICAgICAgIHRocm93IG5ldyBBc3NlcnRpb25FcnJvcihcbiAgICAgICAgICBmbGFnTXNnICsgJ1RoZSBpbnN0YW5jZW9mIGFzc2VydGlvbiBuZWVkcyBhIGNvbnN0cnVjdG9yIGJ1dCAnXG4gICAgICAgICAgICArIF8udHlwZShjb25zdHJ1Y3RvcikgKyAnIHdhcyBnaXZlbi4nLFxuICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICBzc2ZpXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgICB0aHJvdyBlcnI7XG4gICAgfVxuXG4gICAgdmFyIG5hbWUgPSBfLmdldE5hbWUoY29uc3RydWN0b3IpO1xuICAgIGlmIChuYW1lID09PSBudWxsKSB7XG4gICAgICBuYW1lID0gJ2FuIHVubmFtZWQgY29uc3RydWN0b3InO1xuICAgIH1cblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICBpc0luc3RhbmNlT2ZcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgYW4gaW5zdGFuY2Ugb2YgJyArIG5hbWVcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIGFuIGluc3RhbmNlIG9mICcgKyBuYW1lXG4gICAgKTtcbiAgfTtcblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdpbnN0YW5jZW9mJywgYXNzZXJ0SW5zdGFuY2VPZik7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2luc3RhbmNlT2YnLCBhc3NlcnRJbnN0YW5jZU9mKTtcblxuICAvKipcbiAgICogIyMjIC5wcm9wZXJ0eShuYW1lWywgdmFsWywgbXNnXV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGhhcyBhIHByb3BlcnR5IHdpdGggdGhlIGdpdmVuIGtleSBgbmFtZWAuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLnByb3BlcnR5KCdhJyk7XG4gICAqXG4gICAqIFdoZW4gYHZhbGAgaXMgcHJvdmlkZWQsIGAucHJvcGVydHlgIGFsc28gYXNzZXJ0cyB0aGF0IHRoZSBwcm9wZXJ0eSdzIHZhbHVlXG4gICAqIGlzIGVxdWFsIHRvIHRoZSBnaXZlbiBgdmFsYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUucHJvcGVydHkoJ2EnLCAxKTtcbiAgICpcbiAgICogQnkgZGVmYXVsdCwgc3RyaWN0IChgPT09YCkgZXF1YWxpdHkgaXMgdXNlZC4gQWRkIGAuZGVlcGAgZWFybGllciBpbiB0aGVcbiAgICogY2hhaW4gdG8gdXNlIGRlZXAgZXF1YWxpdHkgaW5zdGVhZC4gU2VlIHRoZSBgZGVlcC1lcWxgIHByb2plY3QgcGFnZSBmb3JcbiAgICogaW5mbyBvbiB0aGUgZGVlcCBlcXVhbGl0eSBhbGdvcml0aG06IGh0dHBzOi8vZ2l0aHViLmNvbS9jaGFpanMvZGVlcC1lcWwuXG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgb2JqZWN0IGRlZXBseSAoYnV0IG5vdCBzdHJpY3RseSkgaGFzIHByb3BlcnR5IGB4OiB7YTogMX1gXG4gICAqICAgICBleHBlY3Qoe3g6IHthOiAxfX0pLnRvLmhhdmUuZGVlcC5wcm9wZXJ0eSgneCcsIHthOiAxfSk7XG4gICAqICAgICBleHBlY3Qoe3g6IHthOiAxfX0pLnRvLm5vdC5oYXZlLnByb3BlcnR5KCd4Jywge2E6IDF9KTtcbiAgICpcbiAgICogVGhlIHRhcmdldCdzIGVudW1lcmFibGUgYW5kIG5vbi1lbnVtZXJhYmxlIHByb3BlcnRpZXMgYXJlIGFsd2F5cyBpbmNsdWRlZFxuICAgKiBpbiB0aGUgc2VhcmNoLiBCeSBkZWZhdWx0LCBib3RoIG93biBhbmQgaW5oZXJpdGVkIHByb3BlcnRpZXMgYXJlIGluY2x1ZGVkLlxuICAgKiBBZGQgYC5vd25gIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIGV4Y2x1ZGUgaW5oZXJpdGVkIHByb3BlcnRpZXMgZnJvbSB0aGVcbiAgICogc2VhcmNoLlxuICAgKlxuICAgKiAgICAgT2JqZWN0LnByb3RvdHlwZS5iID0gMjtcbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUub3duLnByb3BlcnR5KCdhJyk7XG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLm93bi5wcm9wZXJ0eSgnYScsIDEpO1xuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8uaGF2ZS5wcm9wZXJ0eSgnYicpO1xuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ubm90LmhhdmUub3duLnByb3BlcnR5KCdiJyk7XG4gICAqXG4gICAqIGAuZGVlcGAgYW5kIGAub3duYCBjYW4gYmUgY29tYmluZWQuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe3g6IHthOiAxfX0pLnRvLmhhdmUuZGVlcC5vd24ucHJvcGVydHkoJ3gnLCB7YTogMX0pO1xuICAgKlxuICAgKiBBZGQgYC5uZXN0ZWRgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIGVuYWJsZSBkb3QtIGFuZCBicmFja2V0LW5vdGF0aW9uIHdoZW5cbiAgICogcmVmZXJlbmNpbmcgbmVzdGVkIHByb3BlcnRpZXMuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IHtiOiBbJ3gnLCAneSddfX0pLnRvLmhhdmUubmVzdGVkLnByb3BlcnR5KCdhLmJbMV0nKTtcbiAgICogICAgIGV4cGVjdCh7YToge2I6IFsneCcsICd5J119fSkudG8uaGF2ZS5uZXN0ZWQucHJvcGVydHkoJ2EuYlsxXScsICd5Jyk7XG4gICAqXG4gICAqIElmIGAuYCBvciBgW11gIGFyZSBwYXJ0IG9mIGFuIGFjdHVhbCBwcm9wZXJ0eSBuYW1lLCB0aGV5IGNhbiBiZSBlc2NhcGVkIGJ5XG4gICAqIGFkZGluZyB0d28gYmFja3NsYXNoZXMgYmVmb3JlIHRoZW0uXG4gICAqXG4gICAqICAgICBleHBlY3QoeycuYSc6IHsnW2JdJzogJ3gnfX0pLnRvLmhhdmUubmVzdGVkLnByb3BlcnR5KCdcXFxcLmEuXFxcXFtiXFxcXF0nKTtcbiAgICpcbiAgICogYC5kZWVwYCBhbmQgYC5uZXN0ZWRgIGNhbiBiZSBjb21iaW5lZC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YToge2I6IFt7YzogM31dfX0pXG4gICAqICAgICAgIC50by5oYXZlLmRlZXAubmVzdGVkLnByb3BlcnR5KCdhLmJbMF0nLCB7YzogM30pO1xuICAgKlxuICAgKiBgLm93bmAgYW5kIGAubmVzdGVkYCBjYW5ub3QgYmUgY29tYmluZWQuXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAucHJvcGVydHlgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ubm90LmhhdmUucHJvcGVydHkoJ2InKTtcbiAgICpcbiAgICogSG93ZXZlciwgaXQncyBkYW5nZXJvdXMgdG8gbmVnYXRlIGAucHJvcGVydHlgIHdoZW4gcHJvdmlkaW5nIGB2YWxgLiBUaGVcbiAgICogcHJvYmxlbSBpcyB0aGF0IGl0IGNyZWF0ZXMgdW5jZXJ0YWluIGV4cGVjdGF0aW9ucyBieSBhc3NlcnRpbmcgdGhhdCB0aGVcbiAgICogdGFyZ2V0IGVpdGhlciBkb2Vzbid0IGhhdmUgYSBwcm9wZXJ0eSB3aXRoIHRoZSBnaXZlbiBrZXkgYG5hbWVgLCBvciB0aGF0IGl0XG4gICAqIGRvZXMgaGF2ZSBhIHByb3BlcnR5IHdpdGggdGhlIGdpdmVuIGtleSBgbmFtZWAgYnV0IGl0cyB2YWx1ZSBpc24ndCBlcXVhbCB0b1xuICAgKiB0aGUgZ2l2ZW4gYHZhbGAuIEl0J3Mgb2Z0ZW4gYmVzdCB0byBpZGVudGlmeSB0aGUgZXhhY3Qgb3V0cHV0IHRoYXQnc1xuICAgKiBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW4gYXNzZXJ0aW9uIHRoYXQgb25seSBhY2NlcHRzIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXNuJ3QgZXhwZWN0ZWQgdG8gaGF2ZSBhIHByb3BlcnR5IHdpdGggdGhlIGdpdmVuIGtleVxuICAgKiBgbmFtZWAsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgZXhhY3RseSB0aGF0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHtiOiAyfSkudG8ubm90LmhhdmUucHJvcGVydHkoJ2EnKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh7YjogMn0pLnRvLm5vdC5oYXZlLnByb3BlcnR5KCdhJywgMSk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgZXhwZWN0ZWQgdG8gaGF2ZSBhIHByb3BlcnR5IHdpdGggdGhlIGdpdmVuIGtleSBgbmFtZWAsXG4gICAqIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgcHJvcGVydHkgaGFzIGl0cyBleHBlY3RlZCB2YWx1ZSwgcmF0aGVyXG4gICAqIHRoYW4gYXNzZXJ0aW5nIHRoYXQgaXQgZG9lc24ndCBoYXZlIG9uZSBvZiBtYW55IHVuZXhwZWN0ZWQgdmFsdWVzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAzfSkudG8uaGF2ZS5wcm9wZXJ0eSgnYScsIDMpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAzfSkudG8ubm90LmhhdmUucHJvcGVydHkoJ2EnLCAxKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAucHJvcGVydHlgIGNoYW5nZXMgdGhlIHRhcmdldCBvZiBhbnkgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW5cbiAgICogdG8gYmUgdGhlIHZhbHVlIG9mIHRoZSBwcm9wZXJ0eSBmcm9tIHRoZSBvcmlnaW5hbCB0YXJnZXQgb2JqZWN0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8uaGF2ZS5wcm9wZXJ0eSgnYScpLnRoYXQuaXMuYSgnbnVtYmVyJyk7XG4gICAqXG4gICAqIGAucHJvcGVydHlgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3JcbiAgICogbWVzc2FnZSB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXNcbiAgICogdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC4gV2hlbiBub3QgcHJvdmlkaW5nIGB2YWxgLCBvbmx5IHVzZSB0aGVcbiAgICogc2Vjb25kIGZvcm0uXG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8uaGF2ZS5wcm9wZXJ0eSgnYScsIDIsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0sICdub29vIHdoeSBmYWlsPz8nKS50by5oYXZlLnByb3BlcnR5KCdhJywgMik7XG4gICAqICAgICBleHBlY3Qoe2E6IDF9LCAnbm9vbyB3aHkgZmFpbD8/JykudG8uaGF2ZS5wcm9wZXJ0eSgnYicpO1xuICAgKlxuICAgKiAgICAgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLnByb3BlcnR5KCdiJywgdW5kZWZpbmVkLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqXG4gICAqIFRoZSBhYm92ZSBhc3NlcnRpb24gaXNuJ3QgdGhlIHNhbWUgdGhpbmcgYXMgbm90IHByb3ZpZGluZyBgdmFsYC4gSW5zdGVhZCxcbiAgICogaXQncyBhc3NlcnRpbmcgdGhhdCB0aGUgdGFyZ2V0IG9iamVjdCBoYXMgYSBgYmAgcHJvcGVydHkgdGhhdCdzIGVxdWFsIHRvXG4gICAqIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiBUaGUgYXNzZXJ0aW9ucyBgLm93blByb3BlcnR5YCBhbmQgYC5oYXZlT3duUHJvcGVydHlgIGNhbiBiZSB1c2VkXG4gICAqIGludGVyY2hhbmdlYWJseSB3aXRoIGAub3duLnByb3BlcnR5YC5cbiAgICpcbiAgICogQG5hbWUgcHJvcGVydHlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsIChvcHRpb25hbClcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEByZXR1cm5zIHZhbHVlIG9mIHByb3BlcnR5IGZvciBjaGFpbmluZ1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRQcm9wZXJ0eSAobmFtZSwgdmFsLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcblxuICAgIHZhciBpc05lc3RlZCA9IGZsYWcodGhpcywgJ25lc3RlZCcpXG4gICAgICAsIGlzT3duID0gZmxhZyh0aGlzLCAnb3duJylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpXG4gICAgICAsIG5hbWVUeXBlID0gdHlwZW9mIG5hbWU7XG5cbiAgICBmbGFnTXNnID0gZmxhZ01zZyA/IGZsYWdNc2cgKyAnOiAnIDogJyc7XG5cbiAgICBpZiAoaXNOZXN0ZWQpIHtcbiAgICAgIGlmIChuYW1lVHlwZSAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICAgIGZsYWdNc2cgKyAndGhlIGFyZ3VtZW50IHRvIHByb3BlcnR5IG11c3QgYmUgYSBzdHJpbmcgd2hlbiB1c2luZyBuZXN0ZWQgc3ludGF4JyxcbiAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgc3NmaVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAobmFtZVR5cGUgIT09ICdzdHJpbmcnICYmIG5hbWVUeXBlICE9PSAnbnVtYmVyJyAmJiBuYW1lVHlwZSAhPT0gJ3N5bWJvbCcpIHtcbiAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICAgIGZsYWdNc2cgKyAndGhlIGFyZ3VtZW50IHRvIHByb3BlcnR5IG11c3QgYmUgYSBzdHJpbmcsIG51bWJlciwgb3Igc3ltYm9sJyxcbiAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgc3NmaVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChpc05lc3RlZCAmJiBpc093bikge1xuICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICBmbGFnTXNnICsgJ1RoZSBcIm5lc3RlZFwiIGFuZCBcIm93blwiIGZsYWdzIGNhbm5vdCBiZSBjb21iaW5lZC4nLFxuICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgIHNzZmlcbiAgICAgICk7XG4gICAgfVxuXG4gICAgaWYgKG9iaiA9PT0gbnVsbCB8fCBvYmogPT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgICBmbGFnTXNnICsgJ1RhcmdldCBjYW5ub3QgYmUgbnVsbCBvciB1bmRlZmluZWQuJyxcbiAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICBzc2ZpXG4gICAgICApO1xuICAgIH1cblxuICAgIHZhciBpc0RlZXAgPSBmbGFnKHRoaXMsICdkZWVwJylcbiAgICAgICwgbmVnYXRlID0gZmxhZyh0aGlzLCAnbmVnYXRlJylcbiAgICAgICwgcGF0aEluZm8gPSBpc05lc3RlZCA/IF8uZ2V0UGF0aEluZm8ob2JqLCBuYW1lKSA6IG51bGxcbiAgICAgICwgdmFsdWUgPSBpc05lc3RlZCA/IHBhdGhJbmZvLnZhbHVlIDogb2JqW25hbWVdO1xuXG4gICAgdmFyIGRlc2NyaXB0b3IgPSAnJztcbiAgICBpZiAoaXNEZWVwKSBkZXNjcmlwdG9yICs9ICdkZWVwICc7XG4gICAgaWYgKGlzT3duKSBkZXNjcmlwdG9yICs9ICdvd24gJztcbiAgICBpZiAoaXNOZXN0ZWQpIGRlc2NyaXB0b3IgKz0gJ25lc3RlZCAnO1xuICAgIGRlc2NyaXB0b3IgKz0gJ3Byb3BlcnR5ICc7XG5cbiAgICB2YXIgaGFzUHJvcGVydHk7XG4gICAgaWYgKGlzT3duKSBoYXNQcm9wZXJ0eSA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIG5hbWUpO1xuICAgIGVsc2UgaWYgKGlzTmVzdGVkKSBoYXNQcm9wZXJ0eSA9IHBhdGhJbmZvLmV4aXN0cztcbiAgICBlbHNlIGhhc1Byb3BlcnR5ID0gXy5oYXNQcm9wZXJ0eShvYmosIG5hbWUpO1xuXG4gICAgLy8gV2hlbiBwZXJmb3JtaW5nIGEgbmVnYXRlZCBhc3NlcnRpb24gZm9yIGJvdGggbmFtZSBhbmQgdmFsLCBtZXJlbHkgaGF2aW5nXG4gICAgLy8gYSBwcm9wZXJ0eSB3aXRoIHRoZSBnaXZlbiBuYW1lIGlzbid0IGVub3VnaCB0byBjYXVzZSB0aGUgYXNzZXJ0aW9uIHRvXG4gICAgLy8gZmFpbC4gSXQgbXVzdCBib3RoIGhhdmUgYSBwcm9wZXJ0eSB3aXRoIHRoZSBnaXZlbiBuYW1lLCBhbmQgdGhlIHZhbHVlIG9mXG4gICAgLy8gdGhhdCBwcm9wZXJ0eSBtdXN0IGVxdWFsIHRoZSBnaXZlbiB2YWwuIFRoZXJlZm9yZSwgc2tpcCB0aGlzIGFzc2VydGlvbiBpblxuICAgIC8vIGZhdm9yIG9mIHRoZSBuZXh0LlxuICAgIGlmICghbmVnYXRlIHx8IGFyZ3VtZW50cy5sZW5ndGggPT09IDEpIHtcbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIGhhc1Byb3BlcnR5XG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSAnICsgZGVzY3JpcHRvciArIF8uaW5zcGVjdChuYW1lKVxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBoYXZlICcgKyBkZXNjcmlwdG9yICsgXy5pbnNwZWN0KG5hbWUpKTtcbiAgICB9XG5cbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIGhhc1Byb3BlcnR5ICYmIChpc0RlZXAgPyBfLmVxbCh2YWwsIHZhbHVlKSA6IHZhbCA9PT0gdmFsdWUpXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gaGF2ZSAnICsgZGVzY3JpcHRvciArIF8uaW5zcGVjdChuYW1lKSArICcgb2YgI3tleHB9LCBidXQgZ290ICN7YWN0fSdcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgaGF2ZSAnICsgZGVzY3JpcHRvciArIF8uaW5zcGVjdChuYW1lKSArICcgb2YgI3thY3R9J1xuICAgICAgICAsIHZhbFxuICAgICAgICAsIHZhbHVlXG4gICAgICApO1xuICAgIH1cblxuICAgIGZsYWcodGhpcywgJ29iamVjdCcsIHZhbHVlKTtcbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ3Byb3BlcnR5JywgYXNzZXJ0UHJvcGVydHkpO1xuXG4gIGZ1bmN0aW9uIGFzc2VydE93blByb3BlcnR5IChuYW1lLCB2YWx1ZSwgbXNnKSB7XG4gICAgZmxhZyh0aGlzLCAnb3duJywgdHJ1ZSk7XG4gICAgYXNzZXJ0UHJvcGVydHkuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ293blByb3BlcnR5JywgYXNzZXJ0T3duUHJvcGVydHkpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdoYXZlT3duUHJvcGVydHknLCBhc3NlcnRPd25Qcm9wZXJ0eSk7XG5cbiAgLyoqXG4gICAqICMjIyAub3duUHJvcGVydHlEZXNjcmlwdG9yKG5hbWVbLCBkZXNjcmlwdG9yWywgbXNnXV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGhhcyBpdHMgb3duIHByb3BlcnR5IGRlc2NyaXB0b3Igd2l0aCB0aGUgZ2l2ZW4ga2V5XG4gICAqIGBuYW1lYC4gRW51bWVyYWJsZSBhbmQgbm9uLWVudW1lcmFibGUgcHJvcGVydGllcyBhcmUgaW5jbHVkZWQgaW4gdGhlXG4gICAqIHNlYXJjaC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdhJyk7XG4gICAqXG4gICAqIFdoZW4gYGRlc2NyaXB0b3JgIGlzIHByb3ZpZGVkLCBgLm93blByb3BlcnR5RGVzY3JpcHRvcmAgYWxzbyBhc3NlcnRzIHRoYXRcbiAgICogdGhlIHByb3BlcnR5J3MgZGVzY3JpcHRvciBpcyBkZWVwbHkgZXF1YWwgdG8gdGhlIGdpdmVuIGBkZXNjcmlwdG9yYC4gU2VlXG4gICAqIHRoZSBgZGVlcC1lcWxgIHByb2plY3QgcGFnZSBmb3IgaW5mbyBvbiB0aGUgZGVlcCBlcXVhbGl0eSBhbGdvcml0aG06XG4gICAqIGh0dHBzOi8vZ2l0aHViLmNvbS9jaGFpanMvZGVlcC1lcWwuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLm93blByb3BlcnR5RGVzY3JpcHRvcignYScsIHtcbiAgICogICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICogICAgICAgdmFsdWU6IDEsXG4gICAqICAgICB9KTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5vd25Qcm9wZXJ0eURlc2NyaXB0b3JgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8ubm90LmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdiJyk7XG4gICAqXG4gICAqIEhvd2V2ZXIsIGl0J3MgZGFuZ2Vyb3VzIHRvIG5lZ2F0ZSBgLm93blByb3BlcnR5RGVzY3JpcHRvcmAgd2hlbiBwcm92aWRpbmdcbiAgICogYSBgZGVzY3JpcHRvcmAuIFRoZSBwcm9ibGVtIGlzIHRoYXQgaXQgY3JlYXRlcyB1bmNlcnRhaW4gZXhwZWN0YXRpb25zIGJ5XG4gICAqIGFzc2VydGluZyB0aGF0IHRoZSB0YXJnZXQgZWl0aGVyIGRvZXNuJ3QgaGF2ZSBhIHByb3BlcnR5IGRlc2NyaXB0b3Igd2l0aFxuICAgKiB0aGUgZ2l2ZW4ga2V5IGBuYW1lYCwgb3IgdGhhdCBpdCBkb2VzIGhhdmUgYSBwcm9wZXJ0eSBkZXNjcmlwdG9yIHdpdGggdGhlXG4gICAqIGdpdmVuIGtleSBgbmFtZWAgYnV0IGl04oCZcyBub3QgZGVlcGx5IGVxdWFsIHRvIHRoZSBnaXZlbiBgZGVzY3JpcHRvcmAuIEl0J3NcbiAgICogb2Z0ZW4gYmVzdCB0byBpZGVudGlmeSB0aGUgZXhhY3Qgb3V0cHV0IHRoYXQncyBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW5cbiAgICogYXNzZXJ0aW9uIHRoYXQgb25seSBhY2NlcHRzIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXNuJ3QgZXhwZWN0ZWQgdG8gaGF2ZSBhIHByb3BlcnR5IGRlc2NyaXB0b3Igd2l0aCB0aGUgZ2l2ZW5cbiAgICoga2V5IGBuYW1lYCwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCBleGFjdGx5IHRoYXQuXG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHtiOiAyfSkudG8ubm90LmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdhJyk7XG4gICAqXG4gICAqICAgICAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh7YjogMn0pLnRvLm5vdC5oYXZlLm93blByb3BlcnR5RGVzY3JpcHRvcignYScsIHtcbiAgICogICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICogICAgICAgdmFsdWU6IDEsXG4gICAqICAgICB9KTtcbiAgICpcbiAgICogV2hlbiB0aGUgdGFyZ2V0IGlzIGV4cGVjdGVkIHRvIGhhdmUgYSBwcm9wZXJ0eSBkZXNjcmlwdG9yIHdpdGggdGhlIGdpdmVuXG4gICAqIGtleSBgbmFtZWAsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgcHJvcGVydHkgaGFzIGl0cyBleHBlY3RlZFxuICAgKiBkZXNjcmlwdG9yLCByYXRoZXIgdGhhbiBhc3NlcnRpbmcgdGhhdCBpdCBkb2Vzbid0IGhhdmUgb25lIG9mIG1hbnlcbiAgICogdW5leHBlY3RlZCBkZXNjcmlwdG9ycy5cbiAgICpcbiAgICogICAgIC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3Qoe2E6IDN9KS50by5oYXZlLm93blByb3BlcnR5RGVzY3JpcHRvcignYScsIHtcbiAgICogICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICogICAgICAgdmFsdWU6IDMsXG4gICAqICAgICB9KTtcbiAgICpcbiAgICogICAgIC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAzfSkudG8ubm90LmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdhJywge1xuICAgKiAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAqICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAqICAgICAgIHdyaXRhYmxlOiB0cnVlLFxuICAgKiAgICAgICB2YWx1ZTogMSxcbiAgICogICAgIH0pO1xuICAgKlxuICAgKiBgLm93blByb3BlcnR5RGVzY3JpcHRvcmAgY2hhbmdlcyB0aGUgdGFyZ2V0IG9mIGFueSBhc3NlcnRpb25zIHRoYXQgZm9sbG93XG4gICAqIGluIHRoZSBjaGFpbiB0byBiZSB0aGUgdmFsdWUgb2YgdGhlIHByb3BlcnR5IGRlc2NyaXB0b3IgZnJvbSB0aGUgb3JpZ2luYWxcbiAgICogdGFyZ2V0IG9iamVjdC5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdhJylcbiAgICogICAgICAgLnRoYXQuaGFzLnByb3BlcnR5KCdlbnVtZXJhYmxlJywgdHJ1ZSk7XG4gICAqXG4gICAqIGAub3duUHJvcGVydHlEZXNjcmlwdG9yYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGFcbiAgICogY3VzdG9tIGVycm9yIG1lc3NhZ2UgdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvXG4gICAqIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuIFdoZW4gbm90IHByb3ZpZGluZ1xuICAgKiBgZGVzY3JpcHRvcmAsIG9ubHkgdXNlIHRoZSBzZWNvbmQgZm9ybS5cbiAgICpcbiAgICogICAgIC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLm93blByb3BlcnR5RGVzY3JpcHRvcignYScsIHtcbiAgICogICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgKiAgICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICogICAgICAgdmFsdWU6IDIsXG4gICAqICAgICB9LCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAxfSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmhhdmUub3duUHJvcGVydHlEZXNjcmlwdG9yKCdhJywge1xuICAgKiAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAqICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAqICAgICAgIHdyaXRhYmxlOiB0cnVlLFxuICAgKiAgICAgICB2YWx1ZTogMixcbiAgICogICAgIH0pO1xuICAgKlxuICAgKiAgICAgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCh7YTogMX0sICdub29vIHdoeSBmYWlsPz8nKS50by5oYXZlLm93blByb3BlcnR5RGVzY3JpcHRvcignYicpO1xuICAgKlxuICAgKiAgICAgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KVxuICAgKiAgICAgICAudG8uaGF2ZS5vd25Qcm9wZXJ0eURlc2NyaXB0b3IoJ2InLCB1bmRlZmluZWQsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICpcbiAgICogVGhlIGFib3ZlIGFzc2VydGlvbiBpc24ndCB0aGUgc2FtZSB0aGluZyBhcyBub3QgcHJvdmlkaW5nIGBkZXNjcmlwdG9yYC5cbiAgICogSW5zdGVhZCwgaXQncyBhc3NlcnRpbmcgdGhhdCB0aGUgdGFyZ2V0IG9iamVjdCBoYXMgYSBgYmAgcHJvcGVydHlcbiAgICogZGVzY3JpcHRvciB0aGF0J3MgZGVlcGx5IGVxdWFsIHRvIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiBUaGUgYWxpYXMgYC5oYXZlT3duUHJvcGVydHlEZXNjcmlwdG9yYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aFxuICAgKiBgLm93blByb3BlcnR5RGVzY3JpcHRvcmAuXG4gICAqXG4gICAqIEBuYW1lIG93blByb3BlcnR5RGVzY3JpcHRvclxuICAgKiBAYWxpYXMgaGF2ZU93blByb3BlcnR5RGVzY3JpcHRvclxuICAgKiBAcGFyYW0ge1N0cmluZ30gbmFtZVxuICAgKiBAcGFyYW0ge09iamVjdH0gZGVzY3JpcHRvciBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRPd25Qcm9wZXJ0eURlc2NyaXB0b3IgKG5hbWUsIGRlc2NyaXB0b3IsIG1zZykge1xuICAgIGlmICh0eXBlb2YgZGVzY3JpcHRvciA9PT0gJ3N0cmluZycpIHtcbiAgICAgIG1zZyA9IGRlc2NyaXB0b3I7XG4gICAgICBkZXNjcmlwdG9yID0gbnVsbDtcbiAgICB9XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpO1xuICAgIHZhciBhY3R1YWxEZXNjcmlwdG9yID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihPYmplY3Qob2JqKSwgbmFtZSk7XG4gICAgaWYgKGFjdHVhbERlc2NyaXB0b3IgJiYgZGVzY3JpcHRvcikge1xuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgXy5lcWwoZGVzY3JpcHRvciwgYWN0dWFsRGVzY3JpcHRvcilcbiAgICAgICAgLCAnZXhwZWN0ZWQgdGhlIG93biBwcm9wZXJ0eSBkZXNjcmlwdG9yIGZvciAnICsgXy5pbnNwZWN0KG5hbWUpICsgJyBvbiAje3RoaXN9IHRvIG1hdGNoICcgKyBfLmluc3BlY3QoZGVzY3JpcHRvcikgKyAnLCBnb3QgJyArIF8uaW5zcGVjdChhY3R1YWxEZXNjcmlwdG9yKVxuICAgICAgICAsICdleHBlY3RlZCB0aGUgb3duIHByb3BlcnR5IGRlc2NyaXB0b3IgZm9yICcgKyBfLmluc3BlY3QobmFtZSkgKyAnIG9uICN7dGhpc30gdG8gbm90IG1hdGNoICcgKyBfLmluc3BlY3QoZGVzY3JpcHRvcilcbiAgICAgICAgLCBkZXNjcmlwdG9yXG4gICAgICAgICwgYWN0dWFsRGVzY3JpcHRvclxuICAgICAgICAsIHRydWVcbiAgICAgICk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgIGFjdHVhbERlc2NyaXB0b3JcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBoYXZlIGFuIG93biBwcm9wZXJ0eSBkZXNjcmlwdG9yIGZvciAnICsgXy5pbnNwZWN0KG5hbWUpXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGhhdmUgYW4gb3duIHByb3BlcnR5IGRlc2NyaXB0b3IgZm9yICcgKyBfLmluc3BlY3QobmFtZSlcbiAgICAgICk7XG4gICAgfVxuICAgIGZsYWcodGhpcywgJ29iamVjdCcsIGFjdHVhbERlc2NyaXB0b3IpO1xuICB9XG5cbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnb3duUHJvcGVydHlEZXNjcmlwdG9yJywgYXNzZXJ0T3duUHJvcGVydHlEZXNjcmlwdG9yKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnaGF2ZU93blByb3BlcnR5RGVzY3JpcHRvcicsIGFzc2VydE93blByb3BlcnR5RGVzY3JpcHRvcik7XG5cbiAgLyoqXG4gICAqICMjIyAubGVuZ3RoT2YoblssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0J3MgYGxlbmd0aGAgb3IgYHNpemVgIGlzIGVxdWFsIHRvIHRoZSBnaXZlbiBudW1iZXJcbiAgICogYG5gLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZigzKTtcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZigzKTtcbiAgICogICAgIGV4cGVjdChuZXcgU2V0KFsxLCAyLCAzXSkpLnRvLmhhdmUubGVuZ3RoT2YoMyk7XG4gICAqICAgICBleHBlY3QobmV3IE1hcChbWydhJywgMV0sIFsnYicsIDJdLCBbJ2MnLCAzXV0pKS50by5oYXZlLmxlbmd0aE9mKDMpO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLmxlbmd0aE9mYC4gSG93ZXZlciwgaXQncyBvZnRlblxuICAgKiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZSB0YXJnZXQncyBgbGVuZ3RoYCBwcm9wZXJ0eSBpcyBlcXVhbCB0byBpdHMgZXhwZWN0ZWRcbiAgICogdmFsdWUsIHJhdGhlciB0aGFuIG5vdCBlcXVhbCB0byBvbmUgb2YgbWFueSB1bmV4cGVjdGVkIHZhbHVlcy5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8uaGF2ZS5sZW5ndGhPZigzKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgnZm9vJykudG8ubm90LmhhdmUubGVuZ3RoT2YoNCk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBgLmxlbmd0aE9mYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yXG4gICAqIG1lc3NhZ2UgdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzXG4gICAqIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLmxlbmd0aE9mKDIsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10sICdub29vIHdoeSBmYWlsPz8nKS50by5oYXZlLmxlbmd0aE9mKDIpO1xuICAgKlxuICAgKiBgLmxlbmd0aE9mYCBjYW4gYWxzbyBiZSB1c2VkIGFzIGEgbGFuZ3VhZ2UgY2hhaW4sIGNhdXNpbmcgYWxsIGAuYWJvdmVgLFxuICAgKiBgLmJlbG93YCwgYC5sZWFzdGAsIGAubW9zdGAsIGFuZCBgLndpdGhpbmAgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGVcbiAgICogY2hhaW4gdG8gdXNlIHRoZSB0YXJnZXQncyBgbGVuZ3RoYCBwcm9wZXJ0eSBhcyB0aGUgdGFyZ2V0LiBIb3dldmVyLCBpdCdzXG4gICAqIG9mdGVuIGJlc3QgdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCdzIGBsZW5ndGhgIHByb3BlcnR5IGlzIGVxdWFsIHRvIGl0c1xuICAgKiBleHBlY3RlZCBsZW5ndGgsIHJhdGhlciB0aGFuIGFzc2VydGluZyB0aGF0IGl0cyBgbGVuZ3RoYCBwcm9wZXJ0eSBmYWxsc1xuICAgKiB3aXRoaW4gc29tZSByYW5nZSBvZiB2YWx1ZXMuXG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZigzKTtcbiAgICpcbiAgICogICAgIC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZi5hYm92ZSgyKTtcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubGVuZ3RoT2YuYmVsb3coNCk7XG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLmxlbmd0aE9mLmF0LmxlYXN0KDMpO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZi5hdC5tb3N0KDMpO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8uaGF2ZS5sZW5ndGhPZi53aXRoaW4oMiw0KTtcbiAgICpcbiAgICogRHVlIHRvIGEgY29tcGF0aWJpbGl0eSBpc3N1ZSwgdGhlIGFsaWFzIGAubGVuZ3RoYCBjYW4ndCBiZSBjaGFpbmVkIGRpcmVjdGx5XG4gICAqIG9mZiBvZiBhbiB1bmludm9rZWQgbWV0aG9kIHN1Y2ggYXMgYC5hYC4gVGhlcmVmb3JlLCBgLmxlbmd0aGAgY2FuJ3QgYmUgdXNlZFxuICAgKiBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLmxlbmd0aE9mYCBpbiBldmVyeSBzaXR1YXRpb24uIEl0J3MgcmVjb21tZW5kZWQgdG9cbiAgICogYWx3YXlzIHVzZSBgLmxlbmd0aE9mYCBpbnN0ZWFkIG9mIGAubGVuZ3RoYC5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUuYS5sZW5ndGgoMyk7IC8vIGluY29tcGF0aWJsZTsgdGhyb3dzIGVycm9yXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLmEubGVuZ3RoT2YoMyk7ICAvLyBwYXNzZXMgYXMgZXhwZWN0ZWRcbiAgICpcbiAgICogQG5hbWUgbGVuZ3RoT2ZcbiAgICogQGFsaWFzIGxlbmd0aFxuICAgKiBAcGFyYW0ge051bWJlcn0gblxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gYXNzZXJ0TGVuZ3RoQ2hhaW4gKCkge1xuICAgIGZsYWcodGhpcywgJ2RvTGVuZ3RoJywgdHJ1ZSk7XG4gIH1cblxuICBmdW5jdGlvbiBhc3NlcnRMZW5ndGggKG4sIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBvYmpUeXBlID0gXy50eXBlKG9iaikudG9Mb3dlckNhc2UoKVxuICAgICAgLCBmbGFnTXNnID0gZmxhZyh0aGlzLCAnbWVzc2FnZScpXG4gICAgICAsIHNzZmkgPSBmbGFnKHRoaXMsICdzc2ZpJylcbiAgICAgICwgZGVzY3JpcHRvciA9ICdsZW5ndGgnXG4gICAgICAsIGl0ZW1zQ291bnQ7XG5cbiAgICBzd2l0Y2ggKG9ialR5cGUpIHtcbiAgICAgIGNhc2UgJ21hcCc6XG4gICAgICBjYXNlICdzZXQnOlxuICAgICAgICBkZXNjcmlwdG9yID0gJ3NpemUnO1xuICAgICAgICBpdGVtc0NvdW50ID0gb2JqLnNpemU7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgbmV3IEFzc2VydGlvbihvYmosIGZsYWdNc2csIHNzZmksIHRydWUpLnRvLmhhdmUucHJvcGVydHkoJ2xlbmd0aCcpO1xuICAgICAgICBpdGVtc0NvdW50ID0gb2JqLmxlbmd0aDtcbiAgICB9XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgaXRlbXNDb3VudCA9PSBuXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGhhdmUgYSAnICsgZGVzY3JpcHRvciArICcgb2YgI3tleHB9IGJ1dCBnb3QgI3thY3R9J1xuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgaGF2ZSBhICcgKyBkZXNjcmlwdG9yICsgJyBvZiAje2FjdH0nXG4gICAgICAsIG5cbiAgICAgICwgaXRlbXNDb3VudFxuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkQ2hhaW5hYmxlTWV0aG9kKCdsZW5ndGgnLCBhc3NlcnRMZW5ndGgsIGFzc2VydExlbmd0aENoYWluKTtcbiAgQXNzZXJ0aW9uLmFkZENoYWluYWJsZU1ldGhvZCgnbGVuZ3RoT2YnLCBhc3NlcnRMZW5ndGgsIGFzc2VydExlbmd0aENoYWluKTtcblxuICAvKipcbiAgICogIyMjIC5tYXRjaChyZVssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IG1hdGNoZXMgdGhlIGdpdmVuIHJlZ3VsYXIgZXhwcmVzc2lvbiBgcmVgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb29iYXInKS50by5tYXRjaCgvXmZvby8pO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLm1hdGNoYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vYmFyJykudG8ubm90Lm1hdGNoKC90YWNvLyk7XG4gICAqXG4gICAqIGAubWF0Y2hgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vYmFyJykudG8ubWF0Y2goL3RhY28vLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoJ2Zvb2JhcicsICdub29vIHdoeSBmYWlsPz8nKS50by5tYXRjaCgvdGFjby8pO1xuICAgKlxuICAgKiBUaGUgYWxpYXMgYC5tYXRjaGVzYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLm1hdGNoYC5cbiAgICpcbiAgICogQG5hbWUgbWF0Y2hcbiAgICogQGFsaWFzIG1hdGNoZXNcbiAgICogQHBhcmFtIHtSZWdFeHB9IHJlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cbiAgZnVuY3Rpb24gYXNzZXJ0TWF0Y2gocmUsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKTtcbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgcmUuZXhlYyhvYmopXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG1hdGNoICcgKyByZVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSBub3QgdG8gbWF0Y2ggJyArIHJlXG4gICAgKTtcbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ21hdGNoJywgYXNzZXJ0TWF0Y2gpO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdtYXRjaGVzJywgYXNzZXJ0TWF0Y2gpO1xuXG4gIC8qKlxuICAgKiAjIyMgLnN0cmluZyhzdHJbLCBtc2ddKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgdGhlIHRhcmdldCBzdHJpbmcgY29udGFpbnMgdGhlIGdpdmVuIHN1YnN0cmluZyBgc3RyYC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgnZm9vYmFyJykudG8uaGF2ZS5zdHJpbmcoJ2JhcicpO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLnN0cmluZ2AuXG4gICAqXG4gICAqICAgICBleHBlY3QoJ2Zvb2JhcicpLnRvLm5vdC5oYXZlLnN0cmluZygndGFjbycpO1xuICAgKlxuICAgKiBgLnN0cmluZ2AgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvclxuICAgKiBtZXNzYWdlIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhc1xuICAgKiB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb29iYXInKS50by5oYXZlLnN0cmluZygndGFjbycsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdCgnZm9vYmFyJywgJ25vb28gd2h5IGZhaWw/PycpLnRvLmhhdmUuc3RyaW5nKCd0YWNvJyk7XG4gICAqXG4gICAqIEBuYW1lIHN0cmluZ1xuICAgKiBAcGFyYW0ge1N0cmluZ30gc3RyXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdzdHJpbmcnLCBmdW5jdGlvbiAoc3RyLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpO1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS5pcy5hKCdzdHJpbmcnKTtcblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICB+b2JqLmluZGV4T2Yoc3RyKVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBjb250YWluICcgKyBfLmluc3BlY3Qoc3RyKVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgY29udGFpbiAnICsgXy5pbnNwZWN0KHN0cilcbiAgICApO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5rZXlzKGtleTFbLCBrZXkyWywgLi4uXV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IG9iamVjdCwgYXJyYXksIG1hcCwgb3Igc2V0IGhhcyB0aGUgZ2l2ZW4ga2V5cy4gT25seVxuICAgKiB0aGUgdGFyZ2V0J3Mgb3duIGluaGVyaXRlZCBwcm9wZXJ0aWVzIGFyZSBpbmNsdWRlZCBpbiB0aGUgc2VhcmNoLlxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgYW4gb2JqZWN0IG9yIGFycmF5LCBrZXlzIGNhbiBiZSBwcm92aWRlZCBhcyBvbmUgb3IgbW9yZVxuICAgKiBzdHJpbmcgYXJndW1lbnRzLCBhIHNpbmdsZSBhcnJheSBhcmd1bWVudCwgb3IgYSBzaW5nbGUgb2JqZWN0IGFyZ3VtZW50LiBJblxuICAgKiB0aGUgbGF0dGVyIGNhc2UsIG9ubHkgdGhlIGtleXMgaW4gdGhlIGdpdmVuIG9iamVjdCBtYXR0ZXI7IHRoZSB2YWx1ZXMgYXJlXG4gICAqIGlnbm9yZWQuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDJ9KS50by5oYXZlLmFsbC5rZXlzKCdhJywgJ2InKTtcbiAgICogICAgIGV4cGVjdChbJ3gnLCAneSddKS50by5oYXZlLmFsbC5rZXlzKDAsIDEpO1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxLCBiOiAyfSkudG8uaGF2ZS5hbGwua2V5cyhbJ2EnLCAnYiddKTtcbiAgICogICAgIGV4cGVjdChbJ3gnLCAneSddKS50by5oYXZlLmFsbC5rZXlzKFswLCAxXSk7XG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDEsIGI6IDJ9KS50by5oYXZlLmFsbC5rZXlzKHthOiA0LCBiOiA1fSk7IC8vIGlnbm9yZSA0IGFuZCA1XG4gICAqICAgICBleHBlY3QoWyd4JywgJ3knXSkudG8uaGF2ZS5hbGwua2V5cyh7MDogNCwgMTogNX0pOyAvLyBpZ25vcmUgNCBhbmQgNVxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgYSBtYXAgb3Igc2V0LCBlYWNoIGtleSBtdXN0IGJlIHByb3ZpZGVkIGFzIGEgc2VwYXJhdGVcbiAgICogYXJndW1lbnQuXG4gICAqXG4gICAqICAgICBleHBlY3QobmV3IE1hcChbWydhJywgMV0sIFsnYicsIDJdXSkpLnRvLmhhdmUuYWxsLmtleXMoJ2EnLCAnYicpO1xuICAgKiAgICAgZXhwZWN0KG5ldyBTZXQoWydhJywgJ2InXSkpLnRvLmhhdmUuYWxsLmtleXMoJ2EnLCAnYicpO1xuICAgKlxuICAgKiBCZWNhdXNlIGAua2V5c2AgZG9lcyBkaWZmZXJlbnQgdGhpbmdzIGJhc2VkIG9uIHRoZSB0YXJnZXQncyB0eXBlLCBpdCdzXG4gICAqIGltcG9ydGFudCB0byBjaGVjayB0aGUgdGFyZ2V0J3MgdHlwZSBiZWZvcmUgdXNpbmcgYC5rZXlzYC4gU2VlIHRoZSBgLmFgIGRvY1xuICAgKiBmb3IgaW5mbyBvbiB0ZXN0aW5nIGEgdGFyZ2V0J3MgdHlwZS5cbiAgICpcbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMn0pLnRvLmJlLmFuKCdvYmplY3QnKS50aGF0Lmhhcy5hbGwua2V5cygnYScsICdiJyk7XG4gICAqXG4gICAqIEJ5IGRlZmF1bHQsIHN0cmljdCAoYD09PWApIGVxdWFsaXR5IGlzIHVzZWQgdG8gY29tcGFyZSBrZXlzIG9mIG1hcHMgYW5kXG4gICAqIHNldHMuIEFkZCBgLmRlZXBgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIHVzZSBkZWVwIGVxdWFsaXR5IGluc3RlYWQuIFNlZVxuICAgKiB0aGUgYGRlZXAtZXFsYCBwcm9qZWN0IHBhZ2UgZm9yIGluZm8gb24gdGhlIGRlZXAgZXF1YWxpdHkgYWxnb3JpdGhtOlxuICAgKiBodHRwczovL2dpdGh1Yi5jb20vY2hhaWpzL2RlZXAtZXFsLlxuICAgKlxuICAgKiAgICAgLy8gVGFyZ2V0IHNldCBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGhhcyBrZXkgYHthOiAxfWBcbiAgICogICAgIGV4cGVjdChuZXcgU2V0KFt7YTogMX1dKSkudG8uaGF2ZS5hbGwuZGVlcC5rZXlzKFt7YTogMX1dKTtcbiAgICogICAgIGV4cGVjdChuZXcgU2V0KFt7YTogMX1dKSkudG8ubm90LmhhdmUuYWxsLmtleXMoW3thOiAxfV0pO1xuICAgKlxuICAgKiBCeSBkZWZhdWx0LCB0aGUgdGFyZ2V0IG11c3QgaGF2ZSBhbGwgb2YgdGhlIGdpdmVuIGtleXMgYW5kIG5vIG1vcmUuIEFkZFxuICAgKiBgLmFueWAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gb25seSByZXF1aXJlIHRoYXQgdGhlIHRhcmdldCBoYXZlIGF0IGxlYXN0XG4gICAqIG9uZSBvZiB0aGUgZ2l2ZW4ga2V5cy4gQWxzbywgYWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGVcbiAgICogYC5rZXlzYC4gSXQncyBvZnRlbiBiZXN0IHRvIGFkZCBgLmFueWAgd2hlbiBuZWdhdGluZyBgLmtleXNgLCBhbmQgdG8gdXNlXG4gICAqIGAuYWxsYCB3aGVuIGFzc2VydGluZyBgLmtleXNgIHdpdGhvdXQgbmVnYXRpb24uXG4gICAqXG4gICAqIFdoZW4gbmVnYXRpbmcgYC5rZXlzYCwgYC5hbnlgIGlzIHByZWZlcnJlZCBiZWNhdXNlIGAubm90LmFueS5rZXlzYCBhc3NlcnRzXG4gICAqIGV4YWN0bHkgd2hhdCdzIGV4cGVjdGVkIG9mIHRoZSBvdXRwdXQsIHdoZXJlYXMgYC5ub3QuYWxsLmtleXNgIGNyZWF0ZXNcbiAgICogdW5jZXJ0YWluIGV4cGVjdGF0aW9ucy5cbiAgICpcbiAgICogICAgIC8vIFJlY29tbWVuZGVkOyBhc3NlcnRzIHRoYXQgdGFyZ2V0IGRvZXNuJ3QgaGF2ZSBhbnkgb2YgdGhlIGdpdmVuIGtleXNcbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMn0pLnRvLm5vdC5oYXZlLmFueS5rZXlzKCdjJywgJ2QnKTtcbiAgICpcbiAgICogICAgIC8vIE5vdCByZWNvbW1lbmRlZDsgYXNzZXJ0cyB0aGF0IHRhcmdldCBkb2Vzbid0IGhhdmUgYWxsIG9mIHRoZSBnaXZlblxuICAgKiAgICAgLy8ga2V5cyBidXQgbWF5IG9yIG1heSBub3QgaGF2ZSBzb21lIG9mIHRoZW1cbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMn0pLnRvLm5vdC5oYXZlLmFsbC5rZXlzKCdjJywgJ2QnKTtcbiAgICpcbiAgICogV2hlbiBhc3NlcnRpbmcgYC5rZXlzYCB3aXRob3V0IG5lZ2F0aW9uLCBgLmFsbGAgaXMgcHJlZmVycmVkIGJlY2F1c2VcbiAgICogYC5hbGwua2V5c2AgYXNzZXJ0cyBleGFjdGx5IHdoYXQncyBleHBlY3RlZCBvZiB0aGUgb3V0cHV0LCB3aGVyZWFzXG4gICAqIGAuYW55LmtleXNgIGNyZWF0ZXMgdW5jZXJ0YWluIGV4cGVjdGF0aW9ucy5cbiAgICpcbiAgICogICAgIC8vIFJlY29tbWVuZGVkOyBhc3NlcnRzIHRoYXQgdGFyZ2V0IGhhcyBhbGwgdGhlIGdpdmVuIGtleXNcbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMn0pLnRvLmhhdmUuYWxsLmtleXMoJ2EnLCAnYicpO1xuICAgKlxuICAgKiAgICAgLy8gTm90IHJlY29tbWVuZGVkOyBhc3NlcnRzIHRoYXQgdGFyZ2V0IGhhcyBhdCBsZWFzdCBvbmUgb2YgdGhlIGdpdmVuXG4gICAqICAgICAvLyBrZXlzIGJ1dCBtYXkgb3IgbWF5IG5vdCBoYXZlIG1vcmUgb2YgdGhlbVxuICAgKiAgICAgZXhwZWN0KHthOiAxLCBiOiAyfSkudG8uaGF2ZS5hbnkua2V5cygnYScsICdiJyk7XG4gICAqXG4gICAqIE5vdGUgdGhhdCBgLmFsbGAgaXMgdXNlZCBieSBkZWZhdWx0IHdoZW4gbmVpdGhlciBgLmFsbGAgbm9yIGAuYW55YCBhcHBlYXJcbiAgICogZWFybGllciBpbiB0aGUgY2hhaW4uIEhvd2V2ZXIsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhZGQgYC5hbGxgIGFueXdheSBiZWNhdXNlXG4gICAqIGl0IGltcHJvdmVzIHJlYWRhYmlsaXR5LlxuICAgKlxuICAgKiAgICAgLy8gQm90aCBhc3NlcnRpb25zIGFyZSBpZGVudGljYWxcbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMn0pLnRvLmhhdmUuYWxsLmtleXMoJ2EnLCAnYicpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHthOiAxLCBiOiAyfSkudG8uaGF2ZS5rZXlzKCdhJywgJ2InKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLmluY2x1ZGVgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIHJlcXVpcmUgdGhhdCB0aGUgdGFyZ2V0J3Mga2V5cyBiZSBhXG4gICAqIHN1cGVyc2V0IG9mIHRoZSBleHBlY3RlZCBrZXlzLCByYXRoZXIgdGhhbiBpZGVudGljYWwgc2V0cy5cbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBvYmplY3QncyBrZXlzIGFyZSBhIHN1cGVyc2V0IG9mIFsnYScsICdiJ10gYnV0IG5vdCBpZGVudGljYWxcbiAgICogICAgIGV4cGVjdCh7YTogMSwgYjogMiwgYzogM30pLnRvLmluY2x1ZGUuYWxsLmtleXMoJ2EnLCAnYicpO1xuICAgKiAgICAgZXhwZWN0KHthOiAxLCBiOiAyLCBjOiAzfSkudG8ubm90LmhhdmUuYWxsLmtleXMoJ2EnLCAnYicpO1xuICAgKlxuICAgKiBIb3dldmVyLCBpZiBgLmFueWAgYW5kIGAuaW5jbHVkZWAgYXJlIGNvbWJpbmVkLCBvbmx5IHRoZSBgLmFueWAgdGFrZXNcbiAgICogZWZmZWN0LiBUaGUgYC5pbmNsdWRlYCBpcyBpZ25vcmVkIGluIHRoaXMgY2FzZS5cbiAgICpcbiAgICogICAgIC8vIEJvdGggYXNzZXJ0aW9ucyBhcmUgaWRlbnRpY2FsXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5oYXZlLmFueS5rZXlzKCdhJywgJ2InKTtcbiAgICogICAgIGV4cGVjdCh7YTogMX0pLnRvLmluY2x1ZGUuYW55LmtleXMoJ2EnLCAnYicpO1xuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmhhdmUua2V5KCdiJyk7XG4gICAqXG4gICAqIFRoZSBhbGlhcyBgLmtleWAgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGggYC5rZXlzYC5cbiAgICpcbiAgICogQG5hbWUga2V5c1xuICAgKiBAYWxpYXMga2V5XG4gICAqIEBwYXJhbSB7Li4uU3RyaW5nfEFycmF5fE9iamVjdH0ga2V5c1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRLZXlzIChrZXlzKSB7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIG9ialR5cGUgPSBfLnR5cGUob2JqKVxuICAgICAgLCBrZXlzVHlwZSA9IF8udHlwZShrZXlzKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpXG4gICAgICAsIGlzRGVlcCA9IGZsYWcodGhpcywgJ2RlZXAnKVxuICAgICAgLCBzdHJcbiAgICAgICwgZGVlcFN0ciA9ICcnXG4gICAgICAsIGFjdHVhbFxuICAgICAgLCBvayA9IHRydWVcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKTtcblxuICAgIGZsYWdNc2cgPSBmbGFnTXNnID8gZmxhZ01zZyArICc6ICcgOiAnJztcbiAgICB2YXIgbWl4ZWRBcmdzTXNnID0gZmxhZ01zZyArICd3aGVuIHRlc3Rpbmcga2V5cyBhZ2FpbnN0IGFuIG9iamVjdCBvciBhbiBhcnJheSB5b3UgbXVzdCBnaXZlIGEgc2luZ2xlIEFycmF5fE9iamVjdHxTdHJpbmcgYXJndW1lbnQgb3IgbXVsdGlwbGUgU3RyaW5nIGFyZ3VtZW50cyc7XG5cbiAgICBpZiAob2JqVHlwZSA9PT0gJ01hcCcgfHwgb2JqVHlwZSA9PT0gJ1NldCcpIHtcbiAgICAgIGRlZXBTdHIgPSBpc0RlZXAgPyAnZGVlcGx5ICcgOiAnJztcbiAgICAgIGFjdHVhbCA9IFtdO1xuXG4gICAgICAvLyBNYXAgYW5kIFNldCAnLmtleXMnIGFyZW4ndCBzdXBwb3J0ZWQgaW4gSUUgMTEuIFRoZXJlZm9yZSwgdXNlIC5mb3JFYWNoLlxuICAgICAgb2JqLmZvckVhY2goZnVuY3Rpb24gKHZhbCwga2V5KSB7IGFjdHVhbC5wdXNoKGtleSkgfSk7XG5cbiAgICAgIGlmIChrZXlzVHlwZSAhPT0gJ0FycmF5Jykge1xuICAgICAgICBrZXlzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgYWN0dWFsID0gXy5nZXRPd25FbnVtZXJhYmxlUHJvcGVydGllcyhvYmopO1xuXG4gICAgICBzd2l0Y2ggKGtleXNUeXBlKSB7XG4gICAgICAgIGNhc2UgJ0FycmF5JzpcbiAgICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBBc3NlcnRpb25FcnJvcihtaXhlZEFyZ3NNc2csIHVuZGVmaW5lZCwgc3NmaSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdPYmplY3QnOlxuICAgICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKG1peGVkQXJnc01zZywgdW5kZWZpbmVkLCBzc2ZpKTtcbiAgICAgICAgICB9XG4gICAgICAgICAga2V5cyA9IE9iamVjdC5rZXlzKGtleXMpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIGtleXMgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMpO1xuICAgICAgfVxuXG4gICAgICAvLyBPbmx5IHN0cmluZ2lmeSBub24tU3ltYm9scyBiZWNhdXNlIFN5bWJvbHMgd291bGQgYmVjb21lIFwiU3ltYm9sKClcIlxuICAgICAga2V5cyA9IGtleXMubWFwKGZ1bmN0aW9uICh2YWwpIHtcbiAgICAgICAgcmV0dXJuIHR5cGVvZiB2YWwgPT09ICdzeW1ib2wnID8gdmFsIDogU3RyaW5nKHZhbCk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAoIWtleXMubGVuZ3RoKSB7XG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoZmxhZ01zZyArICdrZXlzIHJlcXVpcmVkJywgdW5kZWZpbmVkLCBzc2ZpKTtcbiAgICB9XG5cbiAgICB2YXIgbGVuID0ga2V5cy5sZW5ndGhcbiAgICAgICwgYW55ID0gZmxhZyh0aGlzLCAnYW55JylcbiAgICAgICwgYWxsID0gZmxhZyh0aGlzLCAnYWxsJylcbiAgICAgICwgZXhwZWN0ZWQgPSBrZXlzO1xuXG4gICAgaWYgKCFhbnkgJiYgIWFsbCkge1xuICAgICAgYWxsID0gdHJ1ZTtcbiAgICB9XG5cbiAgICAvLyBIYXMgYW55XG4gICAgaWYgKGFueSkge1xuICAgICAgb2sgPSBleHBlY3RlZC5zb21lKGZ1bmN0aW9uKGV4cGVjdGVkS2V5KSB7XG4gICAgICAgIHJldHVybiBhY3R1YWwuc29tZShmdW5jdGlvbihhY3R1YWxLZXkpIHtcbiAgICAgICAgICBpZiAoaXNEZWVwKSB7XG4gICAgICAgICAgICByZXR1cm4gXy5lcWwoZXhwZWN0ZWRLZXksIGFjdHVhbEtleSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBleHBlY3RlZEtleSA9PT0gYWN0dWFsS2V5O1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvLyBIYXMgYWxsXG4gICAgaWYgKGFsbCkge1xuICAgICAgb2sgPSBleHBlY3RlZC5ldmVyeShmdW5jdGlvbihleHBlY3RlZEtleSkge1xuICAgICAgICByZXR1cm4gYWN0dWFsLnNvbWUoZnVuY3Rpb24oYWN0dWFsS2V5KSB7XG4gICAgICAgICAgaWYgKGlzRGVlcCkge1xuICAgICAgICAgICAgcmV0dXJuIF8uZXFsKGV4cGVjdGVkS2V5LCBhY3R1YWxLZXkpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gZXhwZWN0ZWRLZXkgPT09IGFjdHVhbEtleTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG5cbiAgICAgIGlmICghZmxhZyh0aGlzLCAnY29udGFpbnMnKSkge1xuICAgICAgICBvayA9IG9rICYmIGtleXMubGVuZ3RoID09IGFjdHVhbC5sZW5ndGg7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLy8gS2V5IHN0cmluZ1xuICAgIGlmIChsZW4gPiAxKSB7XG4gICAgICBrZXlzID0ga2V5cy5tYXAoZnVuY3Rpb24oa2V5KSB7XG4gICAgICAgIHJldHVybiBfLmluc3BlY3Qoa2V5KTtcbiAgICAgIH0pO1xuICAgICAgdmFyIGxhc3QgPSBrZXlzLnBvcCgpO1xuICAgICAgaWYgKGFsbCkge1xuICAgICAgICBzdHIgPSBrZXlzLmpvaW4oJywgJykgKyAnLCBhbmQgJyArIGxhc3Q7XG4gICAgICB9XG4gICAgICBpZiAoYW55KSB7XG4gICAgICAgIHN0ciA9IGtleXMuam9pbignLCAnKSArICcsIG9yICcgKyBsYXN0O1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHIgPSBfLmluc3BlY3Qoa2V5c1swXSk7XG4gICAgfVxuXG4gICAgLy8gRm9ybVxuICAgIHN0ciA9IChsZW4gPiAxID8gJ2tleXMgJyA6ICdrZXkgJykgKyBzdHI7XG5cbiAgICAvLyBIYXZlIC8gaW5jbHVkZVxuICAgIHN0ciA9IChmbGFnKHRoaXMsICdjb250YWlucycpID8gJ2NvbnRhaW4gJyA6ICdoYXZlICcpICsgc3RyO1xuXG4gICAgLy8gQXNzZXJ0aW9uXG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIG9rXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvICcgKyBkZWVwU3RyICsgc3RyXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCAnICsgZGVlcFN0ciArIHN0clxuICAgICAgLCBleHBlY3RlZC5zbGljZSgwKS5zb3J0KF8uY29tcGFyZUJ5SW5zcGVjdClcbiAgICAgICwgYWN0dWFsLnNvcnQoXy5jb21wYXJlQnlJbnNwZWN0KVxuICAgICAgLCB0cnVlXG4gICAgKTtcbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2tleXMnLCBhc3NlcnRLZXlzKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgna2V5JywgYXNzZXJ0S2V5cyk7XG5cbiAgLyoqXG4gICAqICMjIyAudGhyb3coW2Vycm9yTGlrZV0sIFtlcnJNc2dNYXRjaGVyXSwgW21zZ10pXG4gICAqXG4gICAqIFdoZW4gbm8gYXJndW1lbnRzIGFyZSBwcm92aWRlZCwgYC50aHJvd2AgaW52b2tlcyB0aGUgdGFyZ2V0IGZ1bmN0aW9uIGFuZFxuICAgKiBhc3NlcnRzIHRoYXQgYW4gZXJyb3IgaXMgdGhyb3duLlxuICAgKlxuICAgKiAgICAgdmFyIGJhZEZuID0gZnVuY3Rpb24gKCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKCdJbGxlZ2FsIHNhbG1vbiEnKTsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChiYWRGbikudG8udGhyb3coKTtcbiAgICpcbiAgICogV2hlbiBvbmUgYXJndW1lbnQgaXMgcHJvdmlkZWQsIGFuZCBpdCdzIGFuIGVycm9yIGNvbnN0cnVjdG9yLCBgLnRocm93YFxuICAgKiBpbnZva2VzIHRoZSB0YXJnZXQgZnVuY3Rpb24gYW5kIGFzc2VydHMgdGhhdCBhbiBlcnJvciBpcyB0aHJvd24gdGhhdCdzIGFuXG4gICAqIGluc3RhbmNlIG9mIHRoYXQgZXJyb3IgY29uc3RydWN0b3IuXG4gICAqXG4gICAqICAgICB2YXIgYmFkRm4gPSBmdW5jdGlvbiAoKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoJ0lsbGVnYWwgc2FsbW9uIScpOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGJhZEZuKS50by50aHJvdyhUeXBlRXJyb3IpO1xuICAgKlxuICAgKiBXaGVuIG9uZSBhcmd1bWVudCBpcyBwcm92aWRlZCwgYW5kIGl0J3MgYW4gZXJyb3IgaW5zdGFuY2UsIGAudGhyb3dgIGludm9rZXNcbiAgICogdGhlIHRhcmdldCBmdW5jdGlvbiBhbmQgYXNzZXJ0cyB0aGF0IGFuIGVycm9yIGlzIHRocm93biB0aGF0J3Mgc3RyaWN0bHlcbiAgICogKGA9PT1gKSBlcXVhbCB0byB0aGF0IGVycm9yIGluc3RhbmNlLlxuICAgKlxuICAgKiAgICAgdmFyIGVyciA9IG5ldyBUeXBlRXJyb3IoJ0lsbGVnYWwgc2FsbW9uIScpO1xuICAgKiAgICAgdmFyIGJhZEZuID0gZnVuY3Rpb24gKCkgeyB0aHJvdyBlcnI7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYmFkRm4pLnRvLnRocm93KGVycik7XG4gICAqXG4gICAqIFdoZW4gb25lIGFyZ3VtZW50IGlzIHByb3ZpZGVkLCBhbmQgaXQncyBhIHN0cmluZywgYC50aHJvd2AgaW52b2tlcyB0aGVcbiAgICogdGFyZ2V0IGZ1bmN0aW9uIGFuZCBhc3NlcnRzIHRoYXQgYW4gZXJyb3IgaXMgdGhyb3duIHdpdGggYSBtZXNzYWdlIHRoYXRcbiAgICogY29udGFpbnMgdGhhdCBzdHJpbmcuXG4gICAqXG4gICAqICAgICB2YXIgYmFkRm4gPSBmdW5jdGlvbiAoKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoJ0lsbGVnYWwgc2FsbW9uIScpOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGJhZEZuKS50by50aHJvdygnc2FsbW9uJyk7XG4gICAqXG4gICAqIFdoZW4gb25lIGFyZ3VtZW50IGlzIHByb3ZpZGVkLCBhbmQgaXQncyBhIHJlZ3VsYXIgZXhwcmVzc2lvbiwgYC50aHJvd2BcbiAgICogaW52b2tlcyB0aGUgdGFyZ2V0IGZ1bmN0aW9uIGFuZCBhc3NlcnRzIHRoYXQgYW4gZXJyb3IgaXMgdGhyb3duIHdpdGggYVxuICAgKiBtZXNzYWdlIHRoYXQgbWF0Y2hlcyB0aGF0IHJlZ3VsYXIgZXhwcmVzc2lvbi5cbiAgICpcbiAgICogICAgIHZhciBiYWRGbiA9IGZ1bmN0aW9uICgpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcignSWxsZWdhbCBzYWxtb24hJyk7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYmFkRm4pLnRvLnRocm93KC9zYWxtb24vKTtcbiAgICpcbiAgICogV2hlbiB0d28gYXJndW1lbnRzIGFyZSBwcm92aWRlZCwgYW5kIHRoZSBmaXJzdCBpcyBhbiBlcnJvciBpbnN0YW5jZSBvclxuICAgKiBjb25zdHJ1Y3RvciwgYW5kIHRoZSBzZWNvbmQgaXMgYSBzdHJpbmcgb3IgcmVndWxhciBleHByZXNzaW9uLCBgLnRocm93YFxuICAgKiBpbnZva2VzIHRoZSBmdW5jdGlvbiBhbmQgYXNzZXJ0cyB0aGF0IGFuIGVycm9yIGlzIHRocm93biB0aGF0IGZ1bGZpbGxzIGJvdGhcbiAgICogY29uZGl0aW9ucyBhcyBkZXNjcmliZWQgYWJvdmUuXG4gICAqXG4gICAqICAgICB2YXIgZXJyID0gbmV3IFR5cGVFcnJvcignSWxsZWdhbCBzYWxtb24hJyk7XG4gICAqICAgICB2YXIgYmFkRm4gPSBmdW5jdGlvbiAoKSB7IHRocm93IGVycjsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChiYWRGbikudG8udGhyb3coVHlwZUVycm9yLCAnc2FsbW9uJyk7XG4gICAqICAgICBleHBlY3QoYmFkRm4pLnRvLnRocm93KFR5cGVFcnJvciwgL3NhbG1vbi8pO1xuICAgKiAgICAgZXhwZWN0KGJhZEZuKS50by50aHJvdyhlcnIsICdzYWxtb24nKTtcbiAgICogICAgIGV4cGVjdChiYWRGbikudG8udGhyb3coZXJyLCAvc2FsbW9uLyk7XG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAudGhyb3dgLlxuICAgKlxuICAgKiAgICAgdmFyIGdvb2RGbiA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGdvb2RGbikudG8ubm90LnRocm93KCk7XG4gICAqXG4gICAqIEhvd2V2ZXIsIGl0J3MgZGFuZ2Vyb3VzIHRvIG5lZ2F0ZSBgLnRocm93YCB3aGVuIHByb3ZpZGluZyBhbnkgYXJndW1lbnRzLlxuICAgKiBUaGUgcHJvYmxlbSBpcyB0aGF0IGl0IGNyZWF0ZXMgdW5jZXJ0YWluIGV4cGVjdGF0aW9ucyBieSBhc3NlcnRpbmcgdGhhdCB0aGVcbiAgICogdGFyZ2V0IGVpdGhlciBkb2Vzbid0IHRocm93IGFuIGVycm9yLCBvciB0aGF0IGl0IHRocm93cyBhbiBlcnJvciBidXQgb2YgYVxuICAgKiBkaWZmZXJlbnQgdHlwZSB0aGFuIHRoZSBnaXZlbiB0eXBlLCBvciB0aGF0IGl0IHRocm93cyBhbiBlcnJvciBvZiB0aGUgZ2l2ZW5cbiAgICogdHlwZSBidXQgd2l0aCBhIG1lc3NhZ2UgdGhhdCBkb2Vzbid0IGluY2x1ZGUgdGhlIGdpdmVuIHN0cmluZy4gSXQncyBvZnRlblxuICAgKiBiZXN0IHRvIGlkZW50aWZ5IHRoZSBleGFjdCBvdXRwdXQgdGhhdCdzIGV4cGVjdGVkLCBhbmQgdGhlbiB3cml0ZSBhblxuICAgKiBhc3NlcnRpb24gdGhhdCBvbmx5IGFjY2VwdHMgdGhhdCBleGFjdCBvdXRwdXQuXG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpc24ndCBleHBlY3RlZCB0byB0aHJvdyBhbiBlcnJvciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydFxuICAgKiBleGFjdGx5IHRoYXQuXG4gICAqXG4gICAqICAgICB2YXIgZ29vZEZuID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3QoZ29vZEZuKS50by5ub3QudGhyb3coKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChnb29kRm4pLnRvLm5vdC50aHJvdyhSZWZlcmVuY2VFcnJvciwgJ3gnKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpcyBleHBlY3RlZCB0byB0aHJvdyBhbiBlcnJvciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydFxuICAgKiB0aGF0IHRoZSBlcnJvciBpcyBvZiBpdHMgZXhwZWN0ZWQgdHlwZSwgYW5kIGhhcyBhIG1lc3NhZ2UgdGhhdCBpbmNsdWRlcyBhblxuICAgKiBleHBlY3RlZCBzdHJpbmcsIHJhdGhlciB0aGFuIGFzc2VydGluZyB0aGF0IGl0IGRvZXNuJ3QgaGF2ZSBvbmUgb2YgbWFueVxuICAgKiB1bmV4cGVjdGVkIHR5cGVzLCBhbmQgZG9lc24ndCBoYXZlIGEgbWVzc2FnZSB0aGF0IGluY2x1ZGVzIHNvbWUgc3RyaW5nLlxuICAgKlxuICAgKiAgICAgdmFyIGJhZEZuID0gZnVuY3Rpb24gKCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKCdJbGxlZ2FsIHNhbG1vbiEnKTsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChiYWRGbikudG8udGhyb3coVHlwZUVycm9yLCAnc2FsbW9uJyk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoYmFkRm4pLnRvLm5vdC50aHJvdyhSZWZlcmVuY2VFcnJvciwgJ3gnKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAudGhyb3dgIGNoYW5nZXMgdGhlIHRhcmdldCBvZiBhbnkgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG9cbiAgICogYmUgdGhlIGVycm9yIG9iamVjdCB0aGF0J3MgdGhyb3duLlxuICAgKlxuICAgKiAgICAgdmFyIGVyciA9IG5ldyBUeXBlRXJyb3IoJ0lsbGVnYWwgc2FsbW9uIScpO1xuICAgKiAgICAgZXJyLmNvZGUgPSA0MjtcbiAgICogICAgIHZhciBiYWRGbiA9IGZ1bmN0aW9uICgpIHsgdGhyb3cgZXJyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGJhZEZuKS50by50aHJvdyhUeXBlRXJyb3IpLndpdGgucHJvcGVydHkoJ2NvZGUnLCA0Mik7XG4gICAqXG4gICAqIGAudGhyb3dgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3IgbWVzc2FnZVxuICAgKiB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXMgdGhlXG4gICAqIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC4gV2hlbiBub3QgcHJvdmlkaW5nIHR3byBhcmd1bWVudHMsIGFsd2F5cyB1c2VcbiAgICogdGhlIHNlY29uZCBmb3JtLlxuICAgKlxuICAgKiAgICAgdmFyIGdvb2RGbiA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGdvb2RGbikudG8udGhyb3coVHlwZUVycm9yLCAneCcsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdChnb29kRm4sICdub29vIHdoeSBmYWlsPz8nKS50by50aHJvdygpO1xuICAgKlxuICAgKiBEdWUgdG8gbGltaXRhdGlvbnMgaW4gRVM1LCBgLnRocm93YCBtYXkgbm90IGFsd2F5cyB3b3JrIGFzIGV4cGVjdGVkIHdoZW5cbiAgICogdXNpbmcgYSB0cmFuc3BpbGVyIHN1Y2ggYXMgQmFiZWwgb3IgVHlwZVNjcmlwdC4gSW4gcGFydGljdWxhciwgaXQgbWF5XG4gICAqIHByb2R1Y2UgdW5leHBlY3RlZCByZXN1bHRzIHdoZW4gc3ViY2xhc3NpbmcgdGhlIGJ1aWx0LWluIGBFcnJvcmAgb2JqZWN0IGFuZFxuICAgKiB0aGVuIHBhc3NpbmcgdGhlIHN1YmNsYXNzZWQgY29uc3RydWN0b3IgdG8gYC50aHJvd2AuIFNlZSB5b3VyIHRyYW5zcGlsZXInc1xuICAgKiBkb2NzIGZvciBkZXRhaWxzOlxuICAgKlxuICAgKiAtIChbQmFiZWxdKGh0dHBzOi8vYmFiZWxqcy5pby9kb2NzL3VzYWdlL2NhdmVhdHMvI2NsYXNzZXMpKVxuICAgKiAtIChbVHlwZVNjcmlwdF0oaHR0cHM6Ly9naXRodWIuY29tL01pY3Jvc29mdC9UeXBlU2NyaXB0L3dpa2kvQnJlYWtpbmctQ2hhbmdlcyNleHRlbmRpbmctYnVpbHQtaW5zLWxpa2UtZXJyb3ItYXJyYXktYW5kLW1hcC1tYXktbm8tbG9uZ2VyLXdvcmspKVxuICAgKlxuICAgKiBCZXdhcmUgb2Ygc29tZSBjb21tb24gbWlzdGFrZXMgd2hlbiB1c2luZyB0aGUgYHRocm93YCBhc3NlcnRpb24uIE9uZSBjb21tb25cbiAgICogbWlzdGFrZSBpcyB0byBhY2NpZGVudGFsbHkgaW52b2tlIHRoZSBmdW5jdGlvbiB5b3Vyc2VsZiBpbnN0ZWFkIG9mIGxldHRpbmdcbiAgICogdGhlIGB0aHJvd2AgYXNzZXJ0aW9uIGludm9rZSB0aGUgZnVuY3Rpb24gZm9yIHlvdS4gRm9yIGV4YW1wbGUsIHdoZW5cbiAgICogdGVzdGluZyBpZiBhIGZ1bmN0aW9uIG5hbWVkIGBmbmAgdGhyb3dzLCBwcm92aWRlIGBmbmAgaW5zdGVhZCBvZiBgZm4oKWAgYXNcbiAgICogdGhlIHRhcmdldCBmb3IgdGhlIGFzc2VydGlvbi5cbiAgICpcbiAgICogICAgIGV4cGVjdChmbikudG8udGhyb3coKTsgICAgIC8vIEdvb2QhIFRlc3RzIGBmbmAgYXMgZGVzaXJlZFxuICAgKiAgICAgZXhwZWN0KGZuKCkpLnRvLnRocm93KCk7ICAgLy8gQmFkISBUZXN0cyByZXN1bHQgb2YgYGZuKClgLCBub3QgYGZuYFxuICAgKlxuICAgKiBJZiB5b3UgbmVlZCB0byBhc3NlcnQgdGhhdCB5b3VyIGZ1bmN0aW9uIGBmbmAgdGhyb3dzIHdoZW4gcGFzc2VkIGNlcnRhaW5cbiAgICogYXJndW1lbnRzLCB0aGVuIHdyYXAgYSBjYWxsIHRvIGBmbmAgaW5zaWRlIG9mIGFub3RoZXIgZnVuY3Rpb24uXG4gICAqXG4gICAqICAgICBleHBlY3QoZnVuY3Rpb24gKCkgeyBmbig0Mik7IH0pLnRvLnRocm93KCk7ICAvLyBGdW5jdGlvbiBleHByZXNzaW9uXG4gICAqICAgICBleHBlY3QoKCkgPT4gZm4oNDIpKS50by50aHJvdygpOyAgICAgICAgICAgICAvLyBFUzYgYXJyb3cgZnVuY3Rpb25cbiAgICpcbiAgICogQW5vdGhlciBjb21tb24gbWlzdGFrZSBpcyB0byBwcm92aWRlIGFuIG9iamVjdCBtZXRob2QgKG9yIGFueSBzdGFuZC1hbG9uZVxuICAgKiBmdW5jdGlvbiB0aGF0IHJlbGllcyBvbiBgdGhpc2ApIGFzIHRoZSB0YXJnZXQgb2YgdGhlIGFzc2VydGlvbi4gRG9pbmcgc28gaXNcbiAgICogcHJvYmxlbWF0aWMgYmVjYXVzZSB0aGUgYHRoaXNgIGNvbnRleHQgd2lsbCBiZSBsb3N0IHdoZW4gdGhlIGZ1bmN0aW9uIGlzXG4gICAqIGludm9rZWQgYnkgYC50aHJvd2A7IHRoZXJlJ3Mgbm8gd2F5IGZvciBpdCB0byBrbm93IHdoYXQgYHRoaXNgIGlzIHN1cHBvc2VkXG4gICAqIHRvIGJlLiBUaGVyZSBhcmUgdHdvIHdheXMgYXJvdW5kIHRoaXMgcHJvYmxlbS4gT25lIHNvbHV0aW9uIGlzIHRvIHdyYXAgdGhlXG4gICAqIG1ldGhvZCBvciBmdW5jdGlvbiBjYWxsIGluc2lkZSBvZiBhbm90aGVyIGZ1bmN0aW9uLiBBbm90aGVyIHNvbHV0aW9uIGlzIHRvXG4gICAqIHVzZSBgYmluZGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoZnVuY3Rpb24gKCkgeyBjYXQubWVvdygpOyB9KS50by50aHJvdygpOyAgLy8gRnVuY3Rpb24gZXhwcmVzc2lvblxuICAgKiAgICAgZXhwZWN0KCgpID0+IGNhdC5tZW93KCkpLnRvLnRocm93KCk7ICAgICAgICAgICAgIC8vIEVTNiBhcnJvdyBmdW5jdGlvblxuICAgKiAgICAgZXhwZWN0KGNhdC5tZW93LmJpbmQoY2F0KSkudG8udGhyb3coKTsgICAgICAgICAgIC8vIEJpbmRcbiAgICpcbiAgICogRmluYWxseSwgaXQncyB3b3J0aCBtZW50aW9uaW5nIHRoYXQgaXQncyBhIGJlc3QgcHJhY3RpY2UgaW4gSmF2YVNjcmlwdCB0b1xuICAgKiBvbmx5IHRocm93IGBFcnJvcmAgYW5kIGRlcml2YXRpdmVzIG9mIGBFcnJvcmAgc3VjaCBhcyBgUmVmZXJlbmNlRXJyb3JgLFxuICAgKiBgVHlwZUVycm9yYCwgYW5kIHVzZXItZGVmaW5lZCBvYmplY3RzIHRoYXQgZXh0ZW5kIGBFcnJvcmAuIE5vIG90aGVyIHR5cGUgb2ZcbiAgICogdmFsdWUgd2lsbCBnZW5lcmF0ZSBhIHN0YWNrIHRyYWNlIHdoZW4gaW5pdGlhbGl6ZWQuIFdpdGggdGhhdCBzYWlkLCB0aGVcbiAgICogYHRocm93YCBhc3NlcnRpb24gZG9lcyB0ZWNobmljYWxseSBzdXBwb3J0IGFueSB0eXBlIG9mIHZhbHVlIGJlaW5nIHRocm93bixcbiAgICogbm90IGp1c3QgYEVycm9yYCBhbmQgaXRzIGRlcml2YXRpdmVzLlxuICAgKlxuICAgKiBUaGUgYWxpYXNlcyBgLnRocm93c2AgYW5kIGAuVGhyb3dgIGNhbiBiZSB1c2VkIGludGVyY2hhbmdlYWJseSB3aXRoXG4gICAqIGAudGhyb3dgLlxuICAgKlxuICAgKiBAbmFtZSB0aHJvd1xuICAgKiBAYWxpYXMgdGhyb3dzXG4gICAqIEBhbGlhcyBUaHJvd1xuICAgKiBAcGFyYW0ge0Vycm9yfEVycm9yQ29uc3RydWN0b3J9IGVycm9yTGlrZVxuICAgKiBAcGFyYW0ge1N0cmluZ3xSZWdFeHB9IGVyck1zZ01hdGNoZXIgZXJyb3IgbWVzc2FnZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQHNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9FcnJvciNFcnJvcl90eXBlc1xuICAgKiBAcmV0dXJucyBlcnJvciBmb3IgY2hhaW5pbmcgKG51bGwgaWYgbm8gZXJyb3IpXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydFRocm93cyAoZXJyb3JMaWtlLCBlcnJNc2dNYXRjaGVyLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKVxuICAgICAgLCBmbGFnTXNnID0gZmxhZyh0aGlzLCAnbWVzc2FnZScpXG4gICAgICAsIG5lZ2F0ZSA9IGZsYWcodGhpcywgJ25lZ2F0ZScpIHx8IGZhbHNlO1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS5pcy5hKCdmdW5jdGlvbicpO1xuXG4gICAgaWYgKGVycm9yTGlrZSBpbnN0YW5jZW9mIFJlZ0V4cCB8fCB0eXBlb2YgZXJyb3JMaWtlID09PSAnc3RyaW5nJykge1xuICAgICAgZXJyTXNnTWF0Y2hlciA9IGVycm9yTGlrZTtcbiAgICAgIGVycm9yTGlrZSA9IG51bGw7XG4gICAgfVxuXG4gICAgdmFyIGNhdWdodEVycjtcbiAgICB0cnkge1xuICAgICAgb2JqKCk7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICBjYXVnaHRFcnIgPSBlcnI7XG4gICAgfVxuXG4gICAgLy8gSWYgd2UgaGF2ZSB0aGUgbmVnYXRlIGZsYWcgZW5hYmxlZCBhbmQgYXQgbGVhc3Qgb25lIHZhbGlkIGFyZ3VtZW50IGl0IG1lYW5zIHdlIGRvIGV4cGVjdCBhbiBlcnJvclxuICAgIC8vIGJ1dCB3ZSB3YW50IGl0IHRvIG1hdGNoIGEgZ2l2ZW4gc2V0IG9mIGNyaXRlcmlhXG4gICAgdmFyIGV2ZXJ5QXJnSXNVbmRlZmluZWQgPSBlcnJvckxpa2UgPT09IHVuZGVmaW5lZCAmJiBlcnJNc2dNYXRjaGVyID09PSB1bmRlZmluZWQ7XG5cbiAgICAvLyBJZiB3ZSd2ZSBnb3QgdGhlIG5lZ2F0ZSBmbGFnIGVuYWJsZWQgYW5kIGJvdGggYXJncywgd2Ugc2hvdWxkIG9ubHkgZmFpbCBpZiBib3RoIGFyZW4ndCBjb21wYXRpYmxlXG4gICAgLy8gU2VlIElzc3VlICM1NTEgYW5kIFBSICM2ODNAR2l0SHViXG4gICAgdmFyIGV2ZXJ5QXJnSXNEZWZpbmVkID0gQm9vbGVhbihlcnJvckxpa2UgJiYgZXJyTXNnTWF0Y2hlcik7XG4gICAgdmFyIGVycm9yTGlrZUZhaWwgPSBmYWxzZTtcbiAgICB2YXIgZXJyTXNnTWF0Y2hlckZhaWwgPSBmYWxzZTtcblxuICAgIC8vIENoZWNraW5nIGlmIGVycm9yIHdhcyB0aHJvd25cbiAgICBpZiAoZXZlcnlBcmdJc1VuZGVmaW5lZCB8fCAhZXZlcnlBcmdJc1VuZGVmaW5lZCAmJiAhbmVnYXRlKSB7XG4gICAgICAvLyBXZSBuZWVkIHRoaXMgdG8gZGlzcGxheSByZXN1bHRzIGNvcnJlY3RseSBhY2NvcmRpbmcgdG8gdGhlaXIgdHlwZXNcbiAgICAgIHZhciBlcnJvckxpa2VTdHJpbmcgPSAnYW4gZXJyb3InO1xuICAgICAgaWYgKGVycm9yTGlrZSBpbnN0YW5jZW9mIEVycm9yKSB7XG4gICAgICAgIGVycm9yTGlrZVN0cmluZyA9ICcje2V4cH0nO1xuICAgICAgfSBlbHNlIGlmIChlcnJvckxpa2UpIHtcbiAgICAgICAgZXJyb3JMaWtlU3RyaW5nID0gXy5jaGVja0Vycm9yLmdldENvbnN0cnVjdG9yTmFtZShlcnJvckxpa2UpO1xuICAgICAgfVxuXG4gICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBjYXVnaHRFcnJcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byB0aHJvdyAnICsgZXJyb3JMaWtlU3RyaW5nXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IHRocm93IGFuIGVycm9yIGJ1dCAje2FjdH0gd2FzIHRocm93bidcbiAgICAgICAgLCBlcnJvckxpa2UgJiYgZXJyb3JMaWtlLnRvU3RyaW5nKClcbiAgICAgICAgLCAoY2F1Z2h0RXJyIGluc3RhbmNlb2YgRXJyb3IgP1xuICAgICAgICAgICAgY2F1Z2h0RXJyLnRvU3RyaW5nKCkgOiAodHlwZW9mIGNhdWdodEVyciA9PT0gJ3N0cmluZycgPyBjYXVnaHRFcnIgOiBjYXVnaHRFcnIgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF8uY2hlY2tFcnJvci5nZXRDb25zdHJ1Y3Rvck5hbWUoY2F1Z2h0RXJyKSkpXG4gICAgICApO1xuICAgIH1cblxuICAgIGlmIChlcnJvckxpa2UgJiYgY2F1Z2h0RXJyKSB7XG4gICAgICAvLyBXZSBzaG91bGQgY29tcGFyZSBpbnN0YW5jZXMgb25seSBpZiBgZXJyb3JMaWtlYCBpcyBhbiBpbnN0YW5jZSBvZiBgRXJyb3JgXG4gICAgICBpZiAoZXJyb3JMaWtlIGluc3RhbmNlb2YgRXJyb3IpIHtcbiAgICAgICAgdmFyIGlzQ29tcGF0aWJsZUluc3RhbmNlID0gXy5jaGVja0Vycm9yLmNvbXBhdGlibGVJbnN0YW5jZShjYXVnaHRFcnIsIGVycm9yTGlrZSk7XG5cbiAgICAgICAgaWYgKGlzQ29tcGF0aWJsZUluc3RhbmNlID09PSBuZWdhdGUpIHtcbiAgICAgICAgICAvLyBUaGVzZSBjaGVja3Mgd2VyZSBjcmVhdGVkIHRvIGVuc3VyZSB3ZSB3b24ndCBmYWlsIHRvbyBzb29uIHdoZW4gd2UndmUgZ290IGJvdGggYXJncyBhbmQgYSBuZWdhdGVcbiAgICAgICAgICAvLyBTZWUgSXNzdWUgIzU1MSBhbmQgUFIgIzY4M0BHaXRIdWJcbiAgICAgICAgICBpZiAoZXZlcnlBcmdJc0RlZmluZWQgJiYgbmVnYXRlKSB7XG4gICAgICAgICAgICBlcnJvckxpa2VGYWlsID0gdHJ1ZTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgICAgICAgbmVnYXRlXG4gICAgICAgICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gdGhyb3cgI3tleHB9IGJ1dCAje2FjdH0gd2FzIHRocm93bidcbiAgICAgICAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgdGhyb3cgI3tleHB9JyArIChjYXVnaHRFcnIgJiYgIW5lZ2F0ZSA/ICcgYnV0ICN7YWN0fSB3YXMgdGhyb3duJyA6ICcnKVxuICAgICAgICAgICAgICAsIGVycm9yTGlrZS50b1N0cmluZygpXG4gICAgICAgICAgICAgICwgY2F1Z2h0RXJyLnRvU3RyaW5nKClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHZhciBpc0NvbXBhdGlibGVDb25zdHJ1Y3RvciA9IF8uY2hlY2tFcnJvci5jb21wYXRpYmxlQ29uc3RydWN0b3IoY2F1Z2h0RXJyLCBlcnJvckxpa2UpO1xuICAgICAgaWYgKGlzQ29tcGF0aWJsZUNvbnN0cnVjdG9yID09PSBuZWdhdGUpIHtcbiAgICAgICAgaWYgKGV2ZXJ5QXJnSXNEZWZpbmVkICYmIG5lZ2F0ZSkge1xuICAgICAgICAgICAgZXJyb3JMaWtlRmFpbCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgICAgIG5lZ2F0ZVxuICAgICAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byB0aHJvdyAje2V4cH0gYnV0ICN7YWN0fSB3YXMgdGhyb3duJ1xuICAgICAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgdGhyb3cgI3tleHB9JyArIChjYXVnaHRFcnIgPyAnIGJ1dCAje2FjdH0gd2FzIHRocm93bicgOiAnJylcbiAgICAgICAgICAgICwgKGVycm9yTGlrZSBpbnN0YW5jZW9mIEVycm9yID8gZXJyb3JMaWtlLnRvU3RyaW5nKCkgOiBlcnJvckxpa2UgJiYgXy5jaGVja0Vycm9yLmdldENvbnN0cnVjdG9yTmFtZShlcnJvckxpa2UpKVxuICAgICAgICAgICAgLCAoY2F1Z2h0RXJyIGluc3RhbmNlb2YgRXJyb3IgPyBjYXVnaHRFcnIudG9TdHJpbmcoKSA6IGNhdWdodEVyciAmJiBfLmNoZWNrRXJyb3IuZ2V0Q29uc3RydWN0b3JOYW1lKGNhdWdodEVycikpXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChjYXVnaHRFcnIgJiYgZXJyTXNnTWF0Y2hlciAhPT0gdW5kZWZpbmVkICYmIGVyck1zZ01hdGNoZXIgIT09IG51bGwpIHtcbiAgICAgIC8vIEhlcmUgd2UgY2hlY2sgY29tcGF0aWJsZSBtZXNzYWdlc1xuICAgICAgdmFyIHBsYWNlaG9sZGVyID0gJ2luY2x1ZGluZyc7XG4gICAgICBpZiAoZXJyTXNnTWF0Y2hlciBpbnN0YW5jZW9mIFJlZ0V4cCkge1xuICAgICAgICBwbGFjZWhvbGRlciA9ICdtYXRjaGluZydcbiAgICAgIH1cblxuICAgICAgdmFyIGlzQ29tcGF0aWJsZU1lc3NhZ2UgPSBfLmNoZWNrRXJyb3IuY29tcGF0aWJsZU1lc3NhZ2UoY2F1Z2h0RXJyLCBlcnJNc2dNYXRjaGVyKTtcbiAgICAgIGlmIChpc0NvbXBhdGlibGVNZXNzYWdlID09PSBuZWdhdGUpIHtcbiAgICAgICAgaWYgKGV2ZXJ5QXJnSXNEZWZpbmVkICYmIG5lZ2F0ZSkge1xuICAgICAgICAgICAgZXJyTXNnTWF0Y2hlckZhaWwgPSB0cnVlO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAgICAgbmVnYXRlXG4gICAgICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIHRocm93IGVycm9yICcgKyBwbGFjZWhvbGRlciArICcgI3tleHB9IGJ1dCBnb3QgI3thY3R9J1xuICAgICAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byB0aHJvdyBlcnJvciBub3QgJyArIHBsYWNlaG9sZGVyICsgJyAje2V4cH0nXG4gICAgICAgICAgICAsICBlcnJNc2dNYXRjaGVyXG4gICAgICAgICAgICAsICBfLmNoZWNrRXJyb3IuZ2V0TWVzc2FnZShjYXVnaHRFcnIpXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIC8vIElmIGJvdGggYXNzZXJ0aW9ucyBmYWlsZWQgYW5kIGJvdGggc2hvdWxkJ3ZlIG1hdGNoZWQgd2UgdGhyb3cgYW4gZXJyb3JcbiAgICBpZiAoZXJyb3JMaWtlRmFpbCAmJiBlcnJNc2dNYXRjaGVyRmFpbCkge1xuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIG5lZ2F0ZVxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIHRocm93ICN7ZXhwfSBidXQgI3thY3R9IHdhcyB0aHJvd24nXG4gICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IHRocm93ICN7ZXhwfScgKyAoY2F1Z2h0RXJyID8gJyBidXQgI3thY3R9IHdhcyB0aHJvd24nIDogJycpXG4gICAgICAgICwgKGVycm9yTGlrZSBpbnN0YW5jZW9mIEVycm9yID8gZXJyb3JMaWtlLnRvU3RyaW5nKCkgOiBlcnJvckxpa2UgJiYgXy5jaGVja0Vycm9yLmdldENvbnN0cnVjdG9yTmFtZShlcnJvckxpa2UpKVxuICAgICAgICAsIChjYXVnaHRFcnIgaW5zdGFuY2VvZiBFcnJvciA/IGNhdWdodEVyci50b1N0cmluZygpIDogY2F1Z2h0RXJyICYmIF8uY2hlY2tFcnJvci5nZXRDb25zdHJ1Y3Rvck5hbWUoY2F1Z2h0RXJyKSlcbiAgICAgICk7XG4gICAgfVxuXG4gICAgZmxhZyh0aGlzLCAnb2JqZWN0JywgY2F1Z2h0RXJyKTtcbiAgfTtcblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCd0aHJvdycsIGFzc2VydFRocm93cyk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ3Rocm93cycsIGFzc2VydFRocm93cyk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ1Rocm93JywgYXNzZXJ0VGhyb3dzKTtcblxuICAvKipcbiAgICogIyMjIC5yZXNwb25kVG8obWV0aG9kWywgbXNnXSlcbiAgICpcbiAgICogV2hlbiB0aGUgdGFyZ2V0IGlzIGEgbm9uLWZ1bmN0aW9uIG9iamVjdCwgYC5yZXNwb25kVG9gIGFzc2VydHMgdGhhdCB0aGVcbiAgICogdGFyZ2V0IGhhcyBhIG1ldGhvZCB3aXRoIHRoZSBnaXZlbiBuYW1lIGBtZXRob2RgLiBUaGUgbWV0aG9kIGNhbiBiZSBvd24gb3JcbiAgICogaW5oZXJpdGVkLCBhbmQgaXQgY2FuIGJlIGVudW1lcmFibGUgb3Igbm9uLWVudW1lcmFibGUuXG4gICAqXG4gICAqICAgICBmdW5jdGlvbiBDYXQgKCkge31cbiAgICogICAgIENhdC5wcm90b3R5cGUubWVvdyA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5ldyBDYXQoKSkudG8ucmVzcG9uZFRvKCdtZW93Jyk7XG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpcyBhIGZ1bmN0aW9uLCBgLnJlc3BvbmRUb2AgYXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQnc1xuICAgKiBgcHJvdG90eXBlYCBwcm9wZXJ0eSBoYXMgYSBtZXRob2Qgd2l0aCB0aGUgZ2l2ZW4gbmFtZSBgbWV0aG9kYC4gQWdhaW4sIHRoZVxuICAgKiBtZXRob2QgY2FuIGJlIG93biBvciBpbmhlcml0ZWQsIGFuZCBpdCBjYW4gYmUgZW51bWVyYWJsZSBvciBub24tZW51bWVyYWJsZS5cbiAgICpcbiAgICogICAgIGZ1bmN0aW9uIENhdCAoKSB7fVxuICAgKiAgICAgQ2F0LnByb3RvdHlwZS5tZW93ID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3QoQ2F0KS50by5yZXNwb25kVG8oJ21lb3cnKTtcbiAgICpcbiAgICogQWRkIGAuaXRzZWxmYCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBmb3JjZSBgLnJlc3BvbmRUb2AgdG8gdHJlYXQgdGhlXG4gICAqIHRhcmdldCBhcyBhIG5vbi1mdW5jdGlvbiBvYmplY3QsIGV2ZW4gaWYgaXQncyBhIGZ1bmN0aW9uLiBUaHVzLCBpdCBhc3NlcnRzXG4gICAqIHRoYXQgdGhlIHRhcmdldCBoYXMgYSBtZXRob2Qgd2l0aCB0aGUgZ2l2ZW4gbmFtZSBgbWV0aG9kYCwgcmF0aGVyIHRoYW5cbiAgICogYXNzZXJ0aW5nIHRoYXQgdGhlIHRhcmdldCdzIGBwcm90b3R5cGVgIHByb3BlcnR5IGhhcyBhIG1ldGhvZCB3aXRoIHRoZVxuICAgKiBnaXZlbiBuYW1lIGBtZXRob2RgLlxuICAgKlxuICAgKiAgICAgZnVuY3Rpb24gQ2F0ICgpIHt9XG4gICAqICAgICBDYXQucHJvdG90eXBlLm1lb3cgPSBmdW5jdGlvbiAoKSB7fTtcbiAgICogICAgIENhdC5oaXNzID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3QoQ2F0KS5pdHNlbGYudG8ucmVzcG9uZFRvKCdoaXNzJykuYnV0Lm5vdC5yZXNwb25kVG8oJ21lb3cnKTtcbiAgICpcbiAgICogV2hlbiBub3QgYWRkaW5nIGAuaXRzZWxmYCwgaXQncyBpbXBvcnRhbnQgdG8gY2hlY2sgdGhlIHRhcmdldCdzIHR5cGUgYmVmb3JlXG4gICAqIHVzaW5nIGAucmVzcG9uZFRvYC4gU2VlIHRoZSBgLmFgIGRvYyBmb3IgaW5mbyBvbiBjaGVja2luZyBhIHRhcmdldCdzIHR5cGUuXG4gICAqXG4gICAqICAgICBmdW5jdGlvbiBDYXQgKCkge31cbiAgICogICAgIENhdC5wcm90b3R5cGUubWVvdyA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5ldyBDYXQoKSkudG8uYmUuYW4oJ29iamVjdCcpLnRoYXQucmVzcG9uZHNUbygnbWVvdycpO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLnJlc3BvbmRUb2AuXG4gICAqXG4gICAqICAgICBmdW5jdGlvbiBEb2cgKCkge31cbiAgICogICAgIERvZy5wcm90b3R5cGUuYmFyayA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5ldyBEb2coKSkudG8ubm90LnJlc3BvbmRUbygnbWVvdycpO1xuICAgKlxuICAgKiBgLnJlc3BvbmRUb2AgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvclxuICAgKiBtZXNzYWdlIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhc1xuICAgKiB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHt9KS50by5yZXNwb25kVG8oJ21lb3cnLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3Qoe30sICdub29vIHdoeSBmYWlsPz8nKS50by5yZXNwb25kVG8oJ21lb3cnKTtcbiAgICpcbiAgICogVGhlIGFsaWFzIGAucmVzcG9uZHNUb2AgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGggYC5yZXNwb25kVG9gLlxuICAgKlxuICAgKiBAbmFtZSByZXNwb25kVG9cbiAgICogQGFsaWFzIHJlc3BvbmRzVG9cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1ldGhvZFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gcmVzcG9uZFRvIChtZXRob2QsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBpdHNlbGYgPSBmbGFnKHRoaXMsICdpdHNlbGYnKVxuICAgICAgLCBjb250ZXh0ID0gKCdmdW5jdGlvbicgPT09IHR5cGVvZiBvYmogJiYgIWl0c2VsZilcbiAgICAgICAgPyBvYmoucHJvdG90eXBlW21ldGhvZF1cbiAgICAgICAgOiBvYmpbbWV0aG9kXTtcblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICAnZnVuY3Rpb24nID09PSB0eXBlb2YgY29udGV4dFxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byByZXNwb25kIHRvICcgKyBfLmluc3BlY3QobWV0aG9kKVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgcmVzcG9uZCB0byAnICsgXy5pbnNwZWN0KG1ldGhvZClcbiAgICApO1xuICB9XG5cbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgncmVzcG9uZFRvJywgcmVzcG9uZFRvKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgncmVzcG9uZHNUbycsIHJlc3BvbmRUbyk7XG5cbiAgLyoqXG4gICAqICMjIyAuaXRzZWxmXG4gICAqXG4gICAqIEZvcmNlcyBhbGwgYC5yZXNwb25kVG9gIGFzc2VydGlvbnMgdGhhdCBmb2xsb3cgaW4gdGhlIGNoYWluIHRvIGJlaGF2ZSBhcyBpZlxuICAgKiB0aGUgdGFyZ2V0IGlzIGEgbm9uLWZ1bmN0aW9uIG9iamVjdCwgZXZlbiBpZiBpdCdzIGEgZnVuY3Rpb24uIFRodXMsIGl0XG4gICAqIGNhdXNlcyBgLnJlc3BvbmRUb2AgdG8gYXNzZXJ0IHRoYXQgdGhlIHRhcmdldCBoYXMgYSBtZXRob2Qgd2l0aCB0aGUgZ2l2ZW5cbiAgICogbmFtZSwgcmF0aGVyIHRoYW4gYXNzZXJ0aW5nIHRoYXQgdGhlIHRhcmdldCdzIGBwcm90b3R5cGVgIHByb3BlcnR5IGhhcyBhXG4gICAqIG1ldGhvZCB3aXRoIHRoZSBnaXZlbiBuYW1lLlxuICAgKlxuICAgKiAgICAgZnVuY3Rpb24gQ2F0ICgpIHt9XG4gICAqICAgICBDYXQucHJvdG90eXBlLm1lb3cgPSBmdW5jdGlvbiAoKSB7fTtcbiAgICogICAgIENhdC5oaXNzID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3QoQ2F0KS5pdHNlbGYudG8ucmVzcG9uZFRvKCdoaXNzJykuYnV0Lm5vdC5yZXNwb25kVG8oJ21lb3cnKTtcbiAgICpcbiAgICogQG5hbWUgaXRzZWxmXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnaXRzZWxmJywgZnVuY3Rpb24gKCkge1xuICAgIGZsYWcodGhpcywgJ2l0c2VsZicsIHRydWUpO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5zYXRpc2Z5KG1hdGNoZXJbLCBtc2ddKVxuICAgKlxuICAgKiBJbnZva2VzIHRoZSBnaXZlbiBgbWF0Y2hlcmAgZnVuY3Rpb24gd2l0aCB0aGUgdGFyZ2V0IGJlaW5nIHBhc3NlZCBhcyB0aGVcbiAgICogZmlyc3QgYXJndW1lbnQsIGFuZCBhc3NlcnRzIHRoYXQgdGhlIHZhbHVlIHJldHVybmVkIGlzIHRydXRoeS5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5zYXRpc2Z5KGZ1bmN0aW9uKG51bSkge1xuICAgKiAgICAgICByZXR1cm4gbnVtID4gMDtcbiAgICogICAgIH0pO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLnNhdGlzZnlgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLm5vdC5zYXRpc2Z5KGZ1bmN0aW9uKG51bSkge1xuICAgKiAgICAgICByZXR1cm4gbnVtID4gMjtcbiAgICogICAgIH0pO1xuICAgKlxuICAgKiBgLnNhdGlzZnlgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3JcbiAgICogbWVzc2FnZSB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXNcbiAgICogdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5zYXRpc2Z5KGZ1bmN0aW9uKG51bSkge1xuICAgKiAgICAgICByZXR1cm4gbnVtID4gMjtcbiAgICogICAgIH0sICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICpcbiAgICogICAgIGV4cGVjdCgxLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uc2F0aXNmeShmdW5jdGlvbihudW0pIHtcbiAgICogICAgICAgcmV0dXJuIG51bSA+IDI7XG4gICAqICAgICB9KTtcbiAgICpcbiAgICogVGhlIGFsaWFzIGAuc2F0aXNmaWVzYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLnNhdGlzZnlgLlxuICAgKlxuICAgKiBAbmFtZSBzYXRpc2Z5XG4gICAqIEBhbGlhcyBzYXRpc2ZpZXNcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbWF0Y2hlclxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gc2F0aXNmeSAobWF0Y2hlciwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpO1xuICAgIHZhciByZXN1bHQgPSBtYXRjaGVyKG9iaik7XG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIHJlc3VsdFxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBzYXRpc2Z5ICcgKyBfLm9iakRpc3BsYXkobWF0Y2hlcilcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IHNhdGlzZnknICsgXy5vYmpEaXNwbGF5KG1hdGNoZXIpXG4gICAgICAsIGZsYWcodGhpcywgJ25lZ2F0ZScpID8gZmFsc2UgOiB0cnVlXG4gICAgICAsIHJlc3VsdFxuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdzYXRpc2Z5Jywgc2F0aXNmeSk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ3NhdGlzZmllcycsIHNhdGlzZnkpO1xuXG4gIC8qKlxuICAgKiAjIyMgLmNsb3NlVG8oZXhwZWN0ZWQsIGRlbHRhWywgbXNnXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIgdGhhdCdzIHdpdGhpbiBhIGdpdmVuICsvLSBgZGVsdGFgIHJhbmdlXG4gICAqIG9mIHRoZSBnaXZlbiBudW1iZXIgYGV4cGVjdGVkYC4gSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IHRoZVxuICAgKiB0YXJnZXQgaXMgZXF1YWwgdG8gaXRzIGV4cGVjdGVkIHZhbHVlLlxuICAgKlxuICAgKiAgICAgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxLjUpLnRvLmVxdWFsKDEuNSk7XG4gICAqXG4gICAqICAgICAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxLjUpLnRvLmJlLmNsb3NlVG8oMSwgMC41KTtcbiAgICogICAgIGV4cGVjdCgxLjUpLnRvLmJlLmNsb3NlVG8oMiwgMC41KTtcbiAgICogICAgIGV4cGVjdCgxLjUpLnRvLmJlLmNsb3NlVG8oMSwgMSk7XG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuY2xvc2VUb2AuXG4gICAqXG4gICAqICAgICBleHBlY3QoMS41KS50by5lcXVhbCgxLjUpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KDEuNSkudG8ubm90LmJlLmNsb3NlVG8oMywgMSk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBgLmNsb3NlVG9gIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3JcbiAgICogbWVzc2FnZSB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXNcbiAgICogdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxLjUpLnRvLmJlLmNsb3NlVG8oMywgMSwgJ25vb28gd2h5IGZhaWw/PycpO1xuICAgKiAgICAgZXhwZWN0KDEuNSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmNsb3NlVG8oMywgMSk7XG4gICAqXG4gICAqIFRoZSBhbGlhcyBgLmFwcHJveGltYXRlbHlgIGNhbiBiZSB1c2VkIGludGVyY2hhbmdlYWJseSB3aXRoIGAuY2xvc2VUb2AuXG4gICAqXG4gICAqIEBuYW1lIGNsb3NlVG9cbiAgICogQGFsaWFzIGFwcHJveGltYXRlbHlcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGV4cGVjdGVkXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkZWx0YVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbXNnIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgZnVuY3Rpb24gY2xvc2VUbyhleHBlY3RlZCwgZGVsdGEsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBvYmogPSBmbGFnKHRoaXMsICdvYmplY3QnKVxuICAgICAgLCBmbGFnTXNnID0gZmxhZyh0aGlzLCAnbWVzc2FnZScpXG4gICAgICAsIHNzZmkgPSBmbGFnKHRoaXMsICdzc2ZpJyk7XG5cbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkuaXMuYSgnbnVtYmVyJyk7XG4gICAgaWYgKHR5cGVvZiBleHBlY3RlZCAhPT0gJ251bWJlcicgfHwgdHlwZW9mIGRlbHRhICE9PSAnbnVtYmVyJykge1xuICAgICAgZmxhZ01zZyA9IGZsYWdNc2cgPyBmbGFnTXNnICsgJzogJyA6ICcnO1xuICAgICAgdmFyIGRlbHRhTWVzc2FnZSA9IGRlbHRhID09PSB1bmRlZmluZWQgPyBcIiwgYW5kIGEgZGVsdGEgaXMgcmVxdWlyZWRcIiA6IFwiXCI7XG4gICAgICB0aHJvdyBuZXcgQXNzZXJ0aW9uRXJyb3IoXG4gICAgICAgICAgZmxhZ01zZyArICd0aGUgYXJndW1lbnRzIHRvIGNsb3NlVG8gb3IgYXBwcm94aW1hdGVseSBtdXN0IGJlIG51bWJlcnMnICsgZGVsdGFNZXNzYWdlLFxuICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICBzc2ZpXG4gICAgICApO1xuICAgIH1cblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICBNYXRoLmFicyhvYmogLSBleHBlY3RlZCkgPD0gZGVsdGFcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgY2xvc2UgdG8gJyArIGV4cGVjdGVkICsgJyArLy0gJyArIGRlbHRhXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IG5vdCB0byBiZSBjbG9zZSB0byAnICsgZXhwZWN0ZWQgKyAnICsvLSAnICsgZGVsdGFcbiAgICApO1xuICB9XG5cbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnY2xvc2VUbycsIGNsb3NlVG8pO1xuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdhcHByb3hpbWF0ZWx5JywgY2xvc2VUbyk7XG5cbiAgLy8gTm90ZTogRHVwbGljYXRlcyBhcmUgaWdub3JlZCBpZiB0ZXN0aW5nIGZvciBpbmNsdXNpb24gaW5zdGVhZCBvZiBzYW1lbmVzcy5cbiAgZnVuY3Rpb24gaXNTdWJzZXRPZihzdWJzZXQsIHN1cGVyc2V0LCBjbXAsIGNvbnRhaW5zLCBvcmRlcmVkKSB7XG4gICAgaWYgKCFjb250YWlucykge1xuICAgICAgaWYgKHN1YnNldC5sZW5ndGggIT09IHN1cGVyc2V0Lmxlbmd0aCkgcmV0dXJuIGZhbHNlO1xuICAgICAgc3VwZXJzZXQgPSBzdXBlcnNldC5zbGljZSgpO1xuICAgIH1cblxuICAgIHJldHVybiBzdWJzZXQuZXZlcnkoZnVuY3Rpb24oZWxlbSwgaWR4KSB7XG4gICAgICBpZiAob3JkZXJlZCkgcmV0dXJuIGNtcCA/IGNtcChlbGVtLCBzdXBlcnNldFtpZHhdKSA6IGVsZW0gPT09IHN1cGVyc2V0W2lkeF07XG5cbiAgICAgIGlmICghY21wKSB7XG4gICAgICAgIHZhciBtYXRjaElkeCA9IHN1cGVyc2V0LmluZGV4T2YoZWxlbSk7XG4gICAgICAgIGlmIChtYXRjaElkeCA9PT0gLTEpIHJldHVybiBmYWxzZTtcblxuICAgICAgICAvLyBSZW1vdmUgbWF0Y2ggZnJvbSBzdXBlcnNldCBzbyBub3QgY291bnRlZCB0d2ljZSBpZiBkdXBsaWNhdGUgaW4gc3Vic2V0LlxuICAgICAgICBpZiAoIWNvbnRhaW5zKSBzdXBlcnNldC5zcGxpY2UobWF0Y2hJZHgsIDEpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHN1cGVyc2V0LnNvbWUoZnVuY3Rpb24oZWxlbTIsIG1hdGNoSWR4KSB7XG4gICAgICAgIGlmICghY21wKGVsZW0sIGVsZW0yKSkgcmV0dXJuIGZhbHNlO1xuXG4gICAgICAgIC8vIFJlbW92ZSBtYXRjaCBmcm9tIHN1cGVyc2V0IHNvIG5vdCBjb3VudGVkIHR3aWNlIGlmIGR1cGxpY2F0ZSBpbiBzdWJzZXQuXG4gICAgICAgIGlmICghY29udGFpbnMpIHN1cGVyc2V0LnNwbGljZShtYXRjaElkeCwgMSk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5tZW1iZXJzKHNldFssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGFycmF5IGhhcyB0aGUgc2FtZSBtZW1iZXJzIGFzIHRoZSBnaXZlbiBhcnJheVxuICAgKiBgc2V0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUubWVtYmVycyhbMiwgMSwgM10pO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAyXSkudG8uaGF2ZS5tZW1iZXJzKFsyLCAxLCAyXSk7XG4gICAqXG4gICAqIEJ5IGRlZmF1bHQsIG1lbWJlcnMgYXJlIGNvbXBhcmVkIHVzaW5nIHN0cmljdCAoYD09PWApIGVxdWFsaXR5LiBBZGQgYC5kZWVwYFxuICAgKiBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byB1c2UgZGVlcCBlcXVhbGl0eSBpbnN0ZWFkLiBTZWUgdGhlIGBkZWVwLWVxbGBcbiAgICogcHJvamVjdCBwYWdlIGZvciBpbmZvIG9uIHRoZSBkZWVwIGVxdWFsaXR5IGFsZ29yaXRobTpcbiAgICogaHR0cHM6Ly9naXRodWIuY29tL2NoYWlqcy9kZWVwLWVxbC5cbiAgICpcbiAgICogICAgIC8vIFRhcmdldCBhcnJheSBkZWVwbHkgKGJ1dCBub3Qgc3RyaWN0bHkpIGhhcyBtZW1iZXIgYHthOiAxfWBcbiAgICogICAgIGV4cGVjdChbe2E6IDF9XSkudG8uaGF2ZS5kZWVwLm1lbWJlcnMoW3thOiAxfV0pO1xuICAgKiAgICAgZXhwZWN0KFt7YTogMX1dKS50by5ub3QuaGF2ZS5tZW1iZXJzKFt7YTogMX1dKTtcbiAgICpcbiAgICogQnkgZGVmYXVsdCwgb3JkZXIgZG9lc24ndCBtYXR0ZXIuIEFkZCBgLm9yZGVyZWRgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvXG4gICAqIHJlcXVpcmUgdGhhdCBtZW1iZXJzIGFwcGVhciBpbiB0aGUgc2FtZSBvcmRlci5cbiAgICpcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmhhdmUub3JkZXJlZC5tZW1iZXJzKFsxLCAyLCAzXSk7XG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5oYXZlLm1lbWJlcnMoWzIsIDEsIDNdKVxuICAgKiAgICAgICAuYnV0Lm5vdC5vcmRlcmVkLm1lbWJlcnMoWzIsIDEsIDNdKTtcbiAgICpcbiAgICogQnkgZGVmYXVsdCwgYm90aCBhcnJheXMgbXVzdCBiZSB0aGUgc2FtZSBzaXplLiBBZGQgYC5pbmNsdWRlYCBlYXJsaWVyIGluXG4gICAqIHRoZSBjaGFpbiB0byByZXF1aXJlIHRoYXQgdGhlIHRhcmdldCdzIG1lbWJlcnMgYmUgYSBzdXBlcnNldCBvZiB0aGVcbiAgICogZXhwZWN0ZWQgbWVtYmVycy4gTm90ZSB0aGF0IGR1cGxpY2F0ZXMgYXJlIGlnbm9yZWQgaW4gdGhlIHN1YnNldCB3aGVuXG4gICAqIGAuaW5jbHVkZWAgaXMgYWRkZWQuXG4gICAqXG4gICAqICAgICAvLyBUYXJnZXQgYXJyYXkgaXMgYSBzdXBlcnNldCBvZiBbMSwgMl0gYnV0IG5vdCBpZGVudGljYWxcbiAgICogICAgIGV4cGVjdChbMSwgMiwgM10pLnRvLmluY2x1ZGUubWVtYmVycyhbMSwgMl0pO1xuICAgKiAgICAgZXhwZWN0KFsxLCAyLCAzXSkudG8ubm90LmhhdmUubWVtYmVycyhbMSwgMl0pO1xuICAgKlxuICAgKiAgICAgLy8gRHVwbGljYXRlcyBpbiB0aGUgc3Vic2V0IGFyZSBpZ25vcmVkXG4gICAqICAgICBleHBlY3QoWzEsIDIsIDNdKS50by5pbmNsdWRlLm1lbWJlcnMoWzEsIDIsIDIsIDJdKTtcbiAgICpcbiAgICogYC5kZWVwYCwgYC5vcmRlcmVkYCwgYW5kIGAuaW5jbHVkZWAgY2FuIGFsbCBiZSBjb21iaW5lZC4gSG93ZXZlciwgaWZcbiAgICogYC5pbmNsdWRlYCBhbmQgYC5vcmRlcmVkYCBhcmUgY29tYmluZWQsIHRoZSBvcmRlcmluZyBiZWdpbnMgYXQgdGhlIHN0YXJ0IG9mXG4gICAqIGJvdGggYXJyYXlzLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFt7YTogMX0sIHtiOiAyfSwge2M6IDN9XSlcbiAgICogICAgICAgLnRvLmluY2x1ZGUuZGVlcC5vcmRlcmVkLm1lbWJlcnMoW3thOiAxfSwge2I6IDJ9XSlcbiAgICogICAgICAgLmJ1dC5ub3QuaW5jbHVkZS5kZWVwLm9yZGVyZWQubWVtYmVycyhbe2I6IDJ9LCB7YzogM31dKTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5tZW1iZXJzYC4gSG93ZXZlciwgaXQnc1xuICAgKiBkYW5nZXJvdXMgdG8gZG8gc28uIFRoZSBwcm9ibGVtIGlzIHRoYXQgaXQgY3JlYXRlcyB1bmNlcnRhaW4gZXhwZWN0YXRpb25zXG4gICAqIGJ5IGFzc2VydGluZyB0aGF0IHRoZSB0YXJnZXQgYXJyYXkgZG9lc24ndCBoYXZlIGFsbCBvZiB0aGUgc2FtZSBtZW1iZXJzIGFzXG4gICAqIHRoZSBnaXZlbiBhcnJheSBgc2V0YCBidXQgbWF5IG9yIG1heSBub3QgaGF2ZSBzb21lIG9mIHRoZW0uIEl0J3Mgb2Z0ZW4gYmVzdFxuICAgKiB0byBpZGVudGlmeSB0aGUgZXhhY3Qgb3V0cHV0IHRoYXQncyBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW4gYXNzZXJ0aW9uXG4gICAqIHRoYXQgb25seSBhY2NlcHRzIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KFsxLCAyXSkudG8ubm90LmluY2x1ZGUoMykuYW5kLm5vdC5pbmNsdWRlKDQpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KFsxLCAyXSkudG8ubm90LmhhdmUubWVtYmVycyhbMywgNF0pOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogYC5tZW1iZXJzYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yXG4gICAqIG1lc3NhZ2UgdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzXG4gICAqIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICBleHBlY3QoWzEsIDJdKS50by5oYXZlLm1lbWJlcnMoWzEsIDIsIDNdLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqICAgICBleHBlY3QoWzEsIDJdLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uaGF2ZS5tZW1iZXJzKFsxLCAyLCAzXSk7XG4gICAqXG4gICAqIEBuYW1lIG1lbWJlcnNcbiAgICogQHBhcmFtIHtBcnJheX0gc2V0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdtZW1iZXJzJywgZnVuY3Rpb24gKHN1YnNldCwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKTtcblxuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS50by5iZS5hbignYXJyYXknKTtcbiAgICBuZXcgQXNzZXJ0aW9uKHN1YnNldCwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkudG8uYmUuYW4oJ2FycmF5Jyk7XG5cbiAgICB2YXIgY29udGFpbnMgPSBmbGFnKHRoaXMsICdjb250YWlucycpO1xuICAgIHZhciBvcmRlcmVkID0gZmxhZyh0aGlzLCAnb3JkZXJlZCcpO1xuXG4gICAgdmFyIHN1YmplY3QsIGZhaWxNc2csIGZhaWxOZWdhdGVNc2c7XG5cbiAgICBpZiAoY29udGFpbnMpIHtcbiAgICAgIHN1YmplY3QgPSBvcmRlcmVkID8gJ2FuIG9yZGVyZWQgc3VwZXJzZXQnIDogJ2Egc3VwZXJzZXQnO1xuICAgICAgZmFpbE1zZyA9ICdleHBlY3RlZCAje3RoaXN9IHRvIGJlICcgKyBzdWJqZWN0ICsgJyBvZiAje2V4cH0nO1xuICAgICAgZmFpbE5lZ2F0ZU1zZyA9ICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBiZSAnICsgc3ViamVjdCArICcgb2YgI3tleHB9JztcbiAgICB9IGVsc2Uge1xuICAgICAgc3ViamVjdCA9IG9yZGVyZWQgPyAnb3JkZXJlZCBtZW1iZXJzJyA6ICdtZW1iZXJzJztcbiAgICAgIGZhaWxNc2cgPSAnZXhwZWN0ZWQgI3t0aGlzfSB0byBoYXZlIHRoZSBzYW1lICcgKyBzdWJqZWN0ICsgJyBhcyAje2V4cH0nO1xuICAgICAgZmFpbE5lZ2F0ZU1zZyA9ICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBoYXZlIHRoZSBzYW1lICcgKyBzdWJqZWN0ICsgJyBhcyAje2V4cH0nO1xuICAgIH1cblxuICAgIHZhciBjbXAgPSBmbGFnKHRoaXMsICdkZWVwJykgPyBfLmVxbCA6IHVuZGVmaW5lZDtcblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgICBpc1N1YnNldE9mKHN1YnNldCwgb2JqLCBjbXAsIGNvbnRhaW5zLCBvcmRlcmVkKVxuICAgICAgLCBmYWlsTXNnXG4gICAgICAsIGZhaWxOZWdhdGVNc2dcbiAgICAgICwgc3Vic2V0XG4gICAgICAsIG9ialxuICAgICAgLCB0cnVlXG4gICAgKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAub25lT2YobGlzdFssIG1zZ10pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIGEgbWVtYmVyIG9mIHRoZSBnaXZlbiBhcnJheSBgbGlzdGAuIEhvd2V2ZXIsXG4gICAqIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZS5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxKS50by5lcXVhbCgxKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgxKS50by5iZS5vbmVPZihbMSwgMiwgM10pOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQ29tcGFyaXNvbnMgYXJlIHBlcmZvcm1lZCB1c2luZyBzdHJpY3QgKGA9PT1gKSBlcXVhbGl0eS5cbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5vbmVPZmAuXG4gICAqXG4gICAqICAgICBleHBlY3QoMSkudG8uZXF1YWwoMSk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoMSkudG8ubm90LmJlLm9uZU9mKFsyLCAzLCA0XSk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBJdCBjYW4gYWxzbyBiZSBjaGFpbmVkIHdpdGggYC5jb250YWluYCBvciBgLmluY2x1ZGVgLCB3aGljaCB3aWxsIHdvcmsgd2l0aFxuICAgKiBib3RoIGFycmF5cyBhbmQgc3RyaW5nczpcbiAgICpcbiAgICogICAgIGV4cGVjdCgnVG9kYXkgaXMgc3VubnknKS50by5jb250YWluLm9uZU9mKFsnc3VubnknLCAnY2xvdWR5J10pXG4gICAqICAgICBleHBlY3QoJ1RvZGF5IGlzIHJhaW55JykudG8ubm90LmNvbnRhaW4ub25lT2YoWydzdW5ueScsICdjbG91ZHknXSlcbiAgICogICAgIGV4cGVjdChbMSwyLDNdKS50by5jb250YWluLm9uZU9mKFszLDQsNV0pXG4gICAqICAgICBleHBlY3QoWzEsMiwzXSkudG8ubm90LmNvbnRhaW4ub25lT2YoWzQsNSw2XSlcbiAgICpcbiAgICogYC5vbmVPZmAgYWNjZXB0cyBhbiBvcHRpb25hbCBgbXNnYCBhcmd1bWVudCB3aGljaCBpcyBhIGN1c3RvbSBlcnJvciBtZXNzYWdlXG4gICAqIHRvIHNob3cgd2hlbiB0aGUgYXNzZXJ0aW9uIGZhaWxzLiBUaGUgbWVzc2FnZSBjYW4gYWxzbyBiZSBnaXZlbiBhcyB0aGVcbiAgICogc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmJlLm9uZU9mKFsyLCAzLCA0XSwgJ25vb28gd2h5IGZhaWw/PycpO1xuICAgKiAgICAgZXhwZWN0KDEsICdub29vIHdoeSBmYWlsPz8nKS50by5iZS5vbmVPZihbMiwgMywgNF0pO1xuICAgKlxuICAgKiBAbmFtZSBvbmVPZlxuICAgKiBAcGFyYW0ge0FycmF5PCo+fSBsaXN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBvbmVPZiAobGlzdCwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIGV4cGVjdGVkID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpXG4gICAgICAsIGNvbnRhaW5zID0gZmxhZyh0aGlzLCAnY29udGFpbnMnKVxuICAgICAgLCBpc0RlZXAgPSBmbGFnKHRoaXMsICdkZWVwJyk7XG4gICAgbmV3IEFzc2VydGlvbihsaXN0LCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS50by5iZS5hbignYXJyYXknKTtcblxuICAgIGlmIChjb250YWlucykge1xuICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgIGxpc3Quc29tZShmdW5jdGlvbihwb3NzaWJpbGl0eSkgeyByZXR1cm4gZXhwZWN0ZWQuaW5kZXhPZihwb3NzaWJpbGl0eSkgPiAtMSB9KVxuICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGNvbnRhaW4gb25lIG9mICN7ZXhwfSdcbiAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBub3QgY29udGFpbiBvbmUgb2YgI3tleHB9J1xuICAgICAgICAsIGxpc3RcbiAgICAgICAgLCBleHBlY3RlZFxuICAgICAgKTtcbiAgICB9IGVsc2Uge1xuICAgICAgaWYgKGlzRGVlcCkge1xuICAgICAgICB0aGlzLmFzc2VydChcbiAgICAgICAgICBsaXN0LnNvbWUoZnVuY3Rpb24ocG9zc2liaWxpdHkpIHsgcmV0dXJuIF8uZXFsKGV4cGVjdGVkLCBwb3NzaWJpbGl0eSkgfSlcbiAgICAgICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGRlZXBseSBlcXVhbCBvbmUgb2YgI3tleHB9J1xuICAgICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gZGVlcGx5IGVxdWFsIG9uZSBvZiAje2V4cH0nXG4gICAgICAgICAgLCBsaXN0XG4gICAgICAgICAgLCBleHBlY3RlZFxuICAgICAgICApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5hc3NlcnQoXG4gICAgICAgICAgbGlzdC5pbmRleE9mKGV4cGVjdGVkKSA+IC0xXG4gICAgICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSBvbmUgb2YgI3tleHB9J1xuICAgICAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIG9uZSBvZiAje2V4cH0nXG4gICAgICAgICAgLCBsaXN0XG4gICAgICAgICAgLCBleHBlY3RlZFxuICAgICAgICApO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ29uZU9mJywgb25lT2YpO1xuXG4gIC8qKlxuICAgKiAjIyMgLmNoYW5nZShzdWJqZWN0WywgcHJvcFssIG1zZ11dKVxuICAgKlxuICAgKiBXaGVuIG9uZSBhcmd1bWVudCBpcyBwcm92aWRlZCwgYC5jaGFuZ2VgIGFzc2VydHMgdGhhdCB0aGUgZ2l2ZW4gZnVuY3Rpb25cbiAgICogYHN1YmplY3RgIHJldHVybnMgYSBkaWZmZXJlbnQgdmFsdWUgd2hlbiBpdCdzIGludm9rZWQgYmVmb3JlIHRoZSB0YXJnZXRcbiAgICogZnVuY3Rpb24gY29tcGFyZWQgdG8gd2hlbiBpdCdzIGludm9rZWQgYWZ0ZXJ3YXJkLiBIb3dldmVyLCBpdCdzIG9mdGVuIGJlc3RcbiAgICogdG8gYXNzZXJ0IHRoYXQgYHN1YmplY3RgIGlzIGVxdWFsIHRvIGl0cyBleHBlY3RlZCB2YWx1ZS5cbiAgICpcbiAgICogICAgIHZhciBkb3RzID0gJydcbiAgICogICAgICAgLCBhZGREb3QgPSBmdW5jdGlvbiAoKSB7IGRvdHMgKz0gJy4nOyB9XG4gICAqICAgICAgICwgZ2V0RG90cyA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGRvdHM7IH07XG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGdldERvdHMoKSkudG8uZXF1YWwoJycpO1xuICAgKiAgICAgYWRkRG90KCk7XG4gICAqICAgICBleHBlY3QoZ2V0RG90cygpKS50by5lcXVhbCgnLicpO1xuICAgKlxuICAgKiAgICAgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoYWRkRG90KS50by5jaGFuZ2UoZ2V0RG90cyk7XG4gICAqXG4gICAqIFdoZW4gdHdvIGFyZ3VtZW50cyBhcmUgcHJvdmlkZWQsIGAuY2hhbmdlYCBhc3NlcnRzIHRoYXQgdGhlIHZhbHVlIG9mIHRoZVxuICAgKiBnaXZlbiBvYmplY3QgYHN1YmplY3RgJ3MgYHByb3BgIHByb3BlcnR5IGlzIGRpZmZlcmVudCBiZWZvcmUgaW52b2tpbmcgdGhlXG4gICAqIHRhcmdldCBmdW5jdGlvbiBjb21wYXJlZCB0byBhZnRlcndhcmQuXG4gICAqXG4gICAqICAgICB2YXIgbXlPYmogPSB7ZG90czogJyd9XG4gICAqICAgICAgICwgYWRkRG90ID0gZnVuY3Rpb24gKCkgeyBteU9iai5kb3RzICs9ICcuJzsgfTtcbiAgICpcbiAgICogICAgIC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QobXlPYmopLnRvLmhhdmUucHJvcGVydHkoJ2RvdHMnLCAnJyk7XG4gICAqICAgICBhZGREb3QoKTtcbiAgICogICAgIGV4cGVjdChteU9iaikudG8uaGF2ZS5wcm9wZXJ0eSgnZG90cycsICcuJyk7XG4gICAqXG4gICAqICAgICAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChhZGREb3QpLnRvLmNoYW5nZShteU9iaiwgJ2RvdHMnKTtcbiAgICpcbiAgICogU3RyaWN0IChgPT09YCkgZXF1YWxpdHkgaXMgdXNlZCB0byBjb21wYXJlIGJlZm9yZSBhbmQgYWZ0ZXIgdmFsdWVzLlxuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLmNoYW5nZWAuXG4gICAqXG4gICAqICAgICB2YXIgZG90cyA9ICcnXG4gICAqICAgICAgICwgbm9vcCA9IGZ1bmN0aW9uICgpIHt9XG4gICAqICAgICAgICwgZ2V0RG90cyA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGRvdHM7IH07XG4gICAqXG4gICAqICAgICBleHBlY3Qobm9vcCkudG8ubm90LmNoYW5nZShnZXREb3RzKTtcbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHtkb3RzOiAnJ31cbiAgICogICAgICAgLCBub29wID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3Qobm9vcCkudG8ubm90LmNoYW5nZShteU9iaiwgJ2RvdHMnKTtcbiAgICpcbiAgICogYC5jaGFuZ2VgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3JcbiAgICogbWVzc2FnZSB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXNcbiAgICogdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC4gV2hlbiBub3QgcHJvdmlkaW5nIHR3byBhcmd1bWVudHMsIGFsd2F5c1xuICAgKiB1c2UgdGhlIHNlY29uZCBmb3JtLlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge2RvdHM6ICcnfVxuICAgKiAgICAgICAsIGFkZERvdCA9IGZ1bmN0aW9uICgpIHsgbXlPYmouZG90cyArPSAnLic7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYWRkRG90KS50by5ub3QuY2hhbmdlKG15T2JqLCAnZG90cycsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICpcbiAgICogICAgIHZhciBkb3RzID0gJydcbiAgICogICAgICAgLCBhZGREb3QgPSBmdW5jdGlvbiAoKSB7IGRvdHMgKz0gJy4nOyB9XG4gICAqICAgICAgICwgZ2V0RG90cyA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIGRvdHM7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYWRkRG90LCAnbm9vbyB3aHkgZmFpbD8/JykudG8ubm90LmNoYW5nZShnZXREb3RzKTtcbiAgICpcbiAgICogYC5jaGFuZ2VgIGFsc28gY2F1c2VzIGFsbCBgLmJ5YCBhc3NlcnRpb25zIHRoYXQgZm9sbG93IGluIHRoZSBjaGFpbiB0b1xuICAgKiBhc3NlcnQgaG93IG11Y2ggYSBudW1lcmljIHN1YmplY3Qgd2FzIGluY3JlYXNlZCBvciBkZWNyZWFzZWQgYnkuIEhvd2V2ZXIsXG4gICAqIGl0J3MgZGFuZ2Vyb3VzIHRvIHVzZSBgLmNoYW5nZS5ieWAuIFRoZSBwcm9ibGVtIGlzIHRoYXQgaXQgY3JlYXRlc1xuICAgKiB1bmNlcnRhaW4gZXhwZWN0YXRpb25zIGJ5IGFzc2VydGluZyB0aGF0IHRoZSBzdWJqZWN0IGVpdGhlciBpbmNyZWFzZXMgYnlcbiAgICogdGhlIGdpdmVuIGRlbHRhLCBvciB0aGF0IGl0IGRlY3JlYXNlcyBieSB0aGUgZ2l2ZW4gZGVsdGEuIEl0J3Mgb2Z0ZW4gYmVzdFxuICAgKiB0byBpZGVudGlmeSB0aGUgZXhhY3Qgb3V0cHV0IHRoYXQncyBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW4gYXNzZXJ0aW9uXG4gICAqIHRoYXQgb25seSBhY2NlcHRzIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge3ZhbDogMX1cbiAgICogICAgICAgLCBhZGRUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCArPSAyOyB9XG4gICAqICAgICAgICwgc3VidHJhY3RUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCAtPSAyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChhZGRUd28pLnRvLmNoYW5nZShteU9iaiwgJ3ZhbCcpLmJ5KDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uZGVjcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uY2hhbmdlKG15T2JqLCAndmFsJykuYnkoMik7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBUaGUgYWxpYXMgYC5jaGFuZ2VzYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLmNoYW5nZWAuXG4gICAqXG4gICAqIEBuYW1lIGNoYW5nZVxuICAgKiBAYWxpYXMgY2hhbmdlc1xuICAgKiBAcGFyYW0ge1N0cmluZ30gc3ViamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcCBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydENoYW5nZXMgKHN1YmplY3QsIHByb3AsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBmbiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKTtcbiAgICBuZXcgQXNzZXJ0aW9uKGZuLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS5pcy5hKCdmdW5jdGlvbicpO1xuXG4gICAgdmFyIGluaXRpYWw7XG4gICAgaWYgKCFwcm9wKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHN1YmplY3QsIGZsYWdNc2csIHNzZmksIHRydWUpLmlzLmEoJ2Z1bmN0aW9uJyk7XG4gICAgICBpbml0aWFsID0gc3ViamVjdCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHN1YmplY3QsIGZsYWdNc2csIHNzZmksIHRydWUpLnRvLmhhdmUucHJvcGVydHkocHJvcCk7XG4gICAgICBpbml0aWFsID0gc3ViamVjdFtwcm9wXTtcbiAgICB9XG5cbiAgICBmbigpO1xuXG4gICAgdmFyIGZpbmFsID0gcHJvcCA9PT0gdW5kZWZpbmVkIHx8IHByb3AgPT09IG51bGwgPyBzdWJqZWN0KCkgOiBzdWJqZWN0W3Byb3BdO1xuICAgIHZhciBtc2dPYmogPSBwcm9wID09PSB1bmRlZmluZWQgfHwgcHJvcCA9PT0gbnVsbCA/IGluaXRpYWwgOiAnLicgKyBwcm9wO1xuXG4gICAgLy8gVGhpcyBnZXRzIGZsYWdnZWQgYmVjYXVzZSBvZiB0aGUgLmJ5KGRlbHRhKSBhc3NlcnRpb25cbiAgICBmbGFnKHRoaXMsICdkZWx0YU1zZ09iaicsIG1zZ09iaik7XG4gICAgZmxhZyh0aGlzLCAnaW5pdGlhbERlbHRhVmFsdWUnLCBpbml0aWFsKTtcbiAgICBmbGFnKHRoaXMsICdmaW5hbERlbHRhVmFsdWUnLCBmaW5hbCk7XG4gICAgZmxhZyh0aGlzLCAnZGVsdGFCZWhhdmlvcicsICdjaGFuZ2UnKTtcbiAgICBmbGFnKHRoaXMsICdyZWFsRGVsdGEnLCBmaW5hbCAhPT0gaW5pdGlhbCk7XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgIGluaXRpYWwgIT09IGZpbmFsXG4gICAgICAsICdleHBlY3RlZCAnICsgbXNnT2JqICsgJyB0byBjaGFuZ2UnXG4gICAgICAsICdleHBlY3RlZCAnICsgbXNnT2JqICsgJyB0byBub3QgY2hhbmdlJ1xuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdjaGFuZ2UnLCBhc3NlcnRDaGFuZ2VzKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnY2hhbmdlcycsIGFzc2VydENoYW5nZXMpO1xuXG4gIC8qKlxuICAgKiAjIyMgLmluY3JlYXNlKHN1YmplY3RbLCBwcm9wWywgbXNnXV0pXG4gICAqXG4gICAqIFdoZW4gb25lIGFyZ3VtZW50IGlzIHByb3ZpZGVkLCBgLmluY3JlYXNlYCBhc3NlcnRzIHRoYXQgdGhlIGdpdmVuIGZ1bmN0aW9uXG4gICAqIGBzdWJqZWN0YCByZXR1cm5zIGEgZ3JlYXRlciBudW1iZXIgd2hlbiBpdCdzIGludm9rZWQgYWZ0ZXIgaW52b2tpbmcgdGhlXG4gICAqIHRhcmdldCBmdW5jdGlvbiBjb21wYXJlZCB0byB3aGVuIGl0J3MgaW52b2tlZCBiZWZvcmVoYW5kLiBgLmluY3JlYXNlYCBhbHNvXG4gICAqIGNhdXNlcyBhbGwgYC5ieWAgYXNzZXJ0aW9ucyB0aGF0IGZvbGxvdyBpbiB0aGUgY2hhaW4gdG8gYXNzZXJ0IGhvdyBtdWNoXG4gICAqIGdyZWF0ZXIgb2YgYSBudW1iZXIgaXMgcmV0dXJuZWQuIEl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgcmV0dXJuXG4gICAqIHZhbHVlIGluY3JlYXNlZCBieSB0aGUgZXhwZWN0ZWQgYW1vdW50LCByYXRoZXIgdGhhbiBhc3NlcnRpbmcgaXQgaW5jcmVhc2VkXG4gICAqIGJ5IGFueSBhbW91bnQuXG4gICAqXG4gICAqICAgICB2YXIgdmFsID0gMVxuICAgKiAgICAgICAsIGFkZFR3byA9IGZ1bmN0aW9uICgpIHsgdmFsICs9IDI7IH1cbiAgICogICAgICAgLCBnZXRWYWwgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB2YWw7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYWRkVHdvKS50by5pbmNyZWFzZShnZXRWYWwpLmJ5KDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UoZ2V0VmFsKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIFdoZW4gdHdvIGFyZ3VtZW50cyBhcmUgcHJvdmlkZWQsIGAuaW5jcmVhc2VgIGFzc2VydHMgdGhhdCB0aGUgdmFsdWUgb2YgdGhlXG4gICAqIGdpdmVuIG9iamVjdCBgc3ViamVjdGAncyBgcHJvcGAgcHJvcGVydHkgaXMgZ3JlYXRlciBhZnRlciBpbnZva2luZyB0aGVcbiAgICogdGFyZ2V0IGZ1bmN0aW9uIGNvbXBhcmVkIHRvIGJlZm9yZWhhbmQuXG4gICAqXG4gICAqICAgICB2YXIgbXlPYmogPSB7dmFsOiAxfVxuICAgKiAgICAgICAsIGFkZFR3byA9IGZ1bmN0aW9uICgpIHsgbXlPYmoudmFsICs9IDI7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYWRkVHdvKS50by5pbmNyZWFzZShteU9iaiwgJ3ZhbCcpLmJ5KDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIEFkZCBgLm5vdGAgZWFybGllciBpbiB0aGUgY2hhaW4gdG8gbmVnYXRlIGAuaW5jcmVhc2VgLiBIb3dldmVyLCBpdCdzXG4gICAqIGRhbmdlcm91cyB0byBkbyBzby4gVGhlIHByb2JsZW0gaXMgdGhhdCBpdCBjcmVhdGVzIHVuY2VydGFpbiBleHBlY3RhdGlvbnNcbiAgICogYnkgYXNzZXJ0aW5nIHRoYXQgdGhlIHN1YmplY3QgZWl0aGVyIGRlY3JlYXNlcywgb3IgdGhhdCBpdCBzdGF5cyB0aGUgc2FtZS5cbiAgICogSXQncyBvZnRlbiBiZXN0IHRvIGlkZW50aWZ5IHRoZSBleGFjdCBvdXRwdXQgdGhhdCdzIGV4cGVjdGVkLCBhbmQgdGhlblxuICAgKiB3cml0ZSBhbiBhc3NlcnRpb24gdGhhdCBvbmx5IGFjY2VwdHMgdGhhdCBleGFjdCBvdXRwdXQuXG4gICAqXG4gICAqIFdoZW4gdGhlIHN1YmplY3QgaXMgZXhwZWN0ZWQgdG8gZGVjcmVhc2UsIGl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCBpdFxuICAgKiBkZWNyZWFzZWQgYnkgdGhlIGV4cGVjdGVkIGFtb3VudC5cbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHt2YWw6IDF9XG4gICAqICAgICAgICwgc3VidHJhY3RUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCAtPSAyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHN1YnRyYWN0VHdvKS50by5kZWNyZWFzZShteU9iaiwgJ3ZhbCcpLmJ5KDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHN1YnRyYWN0VHdvKS50by5ub3QuaW5jcmVhc2UobXlPYmosICd2YWwnKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIFdoZW4gdGhlIHN1YmplY3QgaXMgZXhwZWN0ZWQgdG8gc3RheSB0aGUgc2FtZSwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydFxuICAgKiBleGFjdGx5IHRoYXQuXG4gICAqXG4gICAqICAgICB2YXIgbXlPYmogPSB7dmFsOiAxfVxuICAgKiAgICAgICAsIG5vb3AgPSBmdW5jdGlvbiAoKSB7fTtcbiAgICpcbiAgICogICAgIGV4cGVjdChub29wKS50by5ub3QuY2hhbmdlKG15T2JqLCAndmFsJyk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3Qobm9vcCkudG8ubm90LmluY3JlYXNlKG15T2JqLCAndmFsJyk7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBgLmluY3JlYXNlYCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yXG4gICAqIG1lc3NhZ2UgdG8gc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzXG4gICAqIHRoZSBzZWNvbmQgYXJndW1lbnQgdG8gYGV4cGVjdGAuIFdoZW4gbm90IHByb3ZpZGluZyB0d28gYXJndW1lbnRzLCBhbHdheXNcbiAgICogdXNlIHRoZSBzZWNvbmQgZm9ybS5cbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHt2YWw6IDF9XG4gICAqICAgICAgICwgbm9vcCA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5vb3ApLnRvLmluY3JlYXNlKG15T2JqLCAndmFsJywgJ25vb28gd2h5IGZhaWw/PycpO1xuICAgKlxuICAgKiAgICAgdmFyIHZhbCA9IDFcbiAgICogICAgICAgLCBub29wID0gZnVuY3Rpb24gKCkge31cbiAgICogICAgICAgLCBnZXRWYWwgPSBmdW5jdGlvbiAoKSB7IHJldHVybiB2YWw7IH07XG4gICAqXG4gICAqICAgICBleHBlY3Qobm9vcCwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmluY3JlYXNlKGdldFZhbCk7XG4gICAqXG4gICAqIFRoZSBhbGlhcyBgLmluY3JlYXNlc2AgY2FuIGJlIHVzZWQgaW50ZXJjaGFuZ2VhYmx5IHdpdGggYC5pbmNyZWFzZWAuXG4gICAqXG4gICAqIEBuYW1lIGluY3JlYXNlXG4gICAqIEBhbGlhcyBpbmNyZWFzZXNcbiAgICogQHBhcmFtIHtTdHJpbmd8RnVuY3Rpb259IHN1YmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3AgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtc2cgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBmdW5jdGlvbiBhc3NlcnRJbmNyZWFzZXMgKHN1YmplY3QsIHByb3AsIG1zZykge1xuICAgIGlmIChtc2cpIGZsYWcodGhpcywgJ21lc3NhZ2UnLCBtc2cpO1xuICAgIHZhciBmbiA9IGZsYWcodGhpcywgJ29iamVjdCcpXG4gICAgICAsIGZsYWdNc2cgPSBmbGFnKHRoaXMsICdtZXNzYWdlJylcbiAgICAgICwgc3NmaSA9IGZsYWcodGhpcywgJ3NzZmknKTtcbiAgICBuZXcgQXNzZXJ0aW9uKGZuLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS5pcy5hKCdmdW5jdGlvbicpO1xuXG4gICAgdmFyIGluaXRpYWw7XG4gICAgaWYgKCFwcm9wKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHN1YmplY3QsIGZsYWdNc2csIHNzZmksIHRydWUpLmlzLmEoJ2Z1bmN0aW9uJyk7XG4gICAgICBpbml0aWFsID0gc3ViamVjdCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHN1YmplY3QsIGZsYWdNc2csIHNzZmksIHRydWUpLnRvLmhhdmUucHJvcGVydHkocHJvcCk7XG4gICAgICBpbml0aWFsID0gc3ViamVjdFtwcm9wXTtcbiAgICB9XG5cbiAgICAvLyBNYWtlIHN1cmUgdGhhdCB0aGUgdGFyZ2V0IGlzIGEgbnVtYmVyXG4gICAgbmV3IEFzc2VydGlvbihpbml0aWFsLCBmbGFnTXNnLCBzc2ZpLCB0cnVlKS5pcy5hKCdudW1iZXInKTtcblxuICAgIGZuKCk7XG5cbiAgICB2YXIgZmluYWwgPSBwcm9wID09PSB1bmRlZmluZWQgfHwgcHJvcCA9PT0gbnVsbCA/IHN1YmplY3QoKSA6IHN1YmplY3RbcHJvcF07XG4gICAgdmFyIG1zZ09iaiA9IHByb3AgPT09IHVuZGVmaW5lZCB8fCBwcm9wID09PSBudWxsID8gaW5pdGlhbCA6ICcuJyArIHByb3A7XG5cbiAgICBmbGFnKHRoaXMsICdkZWx0YU1zZ09iaicsIG1zZ09iaik7XG4gICAgZmxhZyh0aGlzLCAnaW5pdGlhbERlbHRhVmFsdWUnLCBpbml0aWFsKTtcbiAgICBmbGFnKHRoaXMsICdmaW5hbERlbHRhVmFsdWUnLCBmaW5hbCk7XG4gICAgZmxhZyh0aGlzLCAnZGVsdGFCZWhhdmlvcicsICdpbmNyZWFzZScpO1xuICAgIGZsYWcodGhpcywgJ3JlYWxEZWx0YScsIGZpbmFsIC0gaW5pdGlhbCk7XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgIGZpbmFsIC0gaW5pdGlhbCA+IDBcbiAgICAgICwgJ2V4cGVjdGVkICcgKyBtc2dPYmogKyAnIHRvIGluY3JlYXNlJ1xuICAgICAgLCAnZXhwZWN0ZWQgJyArIG1zZ09iaiArICcgdG8gbm90IGluY3JlYXNlJ1xuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdpbmNyZWFzZScsIGFzc2VydEluY3JlYXNlcyk7XG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2luY3JlYXNlcycsIGFzc2VydEluY3JlYXNlcyk7XG5cbiAgLyoqXG4gICAqICMjIyAuZGVjcmVhc2Uoc3ViamVjdFssIHByb3BbLCBtc2ddXSlcbiAgICpcbiAgICogV2hlbiBvbmUgYXJndW1lbnQgaXMgcHJvdmlkZWQsIGAuZGVjcmVhc2VgIGFzc2VydHMgdGhhdCB0aGUgZ2l2ZW4gZnVuY3Rpb25cbiAgICogYHN1YmplY3RgIHJldHVybnMgYSBsZXNzZXIgbnVtYmVyIHdoZW4gaXQncyBpbnZva2VkIGFmdGVyIGludm9raW5nIHRoZVxuICAgKiB0YXJnZXQgZnVuY3Rpb24gY29tcGFyZWQgdG8gd2hlbiBpdCdzIGludm9rZWQgYmVmb3JlaGFuZC4gYC5kZWNyZWFzZWAgYWxzb1xuICAgKiBjYXVzZXMgYWxsIGAuYnlgIGFzc2VydGlvbnMgdGhhdCBmb2xsb3cgaW4gdGhlIGNoYWluIHRvIGFzc2VydCBob3cgbXVjaFxuICAgKiBsZXNzZXIgb2YgYSBudW1iZXIgaXMgcmV0dXJuZWQuIEl0J3Mgb2Z0ZW4gYmVzdCB0byBhc3NlcnQgdGhhdCB0aGUgcmV0dXJuXG4gICAqIHZhbHVlIGRlY3JlYXNlZCBieSB0aGUgZXhwZWN0ZWQgYW1vdW50LCByYXRoZXIgdGhhbiBhc3NlcnRpbmcgaXQgZGVjcmVhc2VkXG4gICAqIGJ5IGFueSBhbW91bnQuXG4gICAqXG4gICAqICAgICB2YXIgdmFsID0gMVxuICAgKiAgICAgICAsIHN1YnRyYWN0VHdvID0gZnVuY3Rpb24gKCkgeyB2YWwgLT0gMjsgfVxuICAgKiAgICAgICAsIGdldFZhbCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHZhbDsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uZGVjcmVhc2UoZ2V0VmFsKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uZGVjcmVhc2UoZ2V0VmFsKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIFdoZW4gdHdvIGFyZ3VtZW50cyBhcmUgcHJvdmlkZWQsIGAuZGVjcmVhc2VgIGFzc2VydHMgdGhhdCB0aGUgdmFsdWUgb2YgdGhlXG4gICAqIGdpdmVuIG9iamVjdCBgc3ViamVjdGAncyBgcHJvcGAgcHJvcGVydHkgaXMgbGVzc2VyIGFmdGVyIGludm9raW5nIHRoZVxuICAgKiB0YXJnZXQgZnVuY3Rpb24gY29tcGFyZWQgdG8gYmVmb3JlaGFuZC5cbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHt2YWw6IDF9XG4gICAqICAgICAgICwgc3VidHJhY3RUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCAtPSAyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KHN1YnRyYWN0VHdvKS50by5kZWNyZWFzZShteU9iaiwgJ3ZhbCcpLmJ5KDIpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KHN1YnRyYWN0VHdvKS50by5kZWNyZWFzZShteU9iaiwgJ3ZhbCcpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5kZWNyZWFzZWAuIEhvd2V2ZXIsIGl0J3NcbiAgICogZGFuZ2Vyb3VzIHRvIGRvIHNvLiBUaGUgcHJvYmxlbSBpcyB0aGF0IGl0IGNyZWF0ZXMgdW5jZXJ0YWluIGV4cGVjdGF0aW9uc1xuICAgKiBieSBhc3NlcnRpbmcgdGhhdCB0aGUgc3ViamVjdCBlaXRoZXIgaW5jcmVhc2VzLCBvciB0aGF0IGl0IHN0YXlzIHRoZSBzYW1lLlxuICAgKiBJdCdzIG9mdGVuIGJlc3QgdG8gaWRlbnRpZnkgdGhlIGV4YWN0IG91dHB1dCB0aGF0J3MgZXhwZWN0ZWQsIGFuZCB0aGVuXG4gICAqIHdyaXRlIGFuIGFzc2VydGlvbiB0aGF0IG9ubHkgYWNjZXB0cyB0aGF0IGV4YWN0IG91dHB1dC5cbiAgICpcbiAgICogV2hlbiB0aGUgc3ViamVjdCBpcyBleHBlY3RlZCB0byBpbmNyZWFzZSwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydCB0aGF0IGl0XG4gICAqIGluY3JlYXNlZCBieSB0aGUgZXhwZWN0ZWQgYW1vdW50LlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge3ZhbDogMX1cbiAgICogICAgICAgLCBhZGRUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCArPSAyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChhZGRUd28pLnRvLm5vdC5kZWNyZWFzZShteU9iaiwgJ3ZhbCcpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogV2hlbiB0aGUgc3ViamVjdCBpcyBleHBlY3RlZCB0byBzdGF5IHRoZSBzYW1lLCBpdCdzIG9mdGVuIGJlc3QgdG8gYXNzZXJ0XG4gICAqIGV4YWN0bHkgdGhhdC5cbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHt2YWw6IDF9XG4gICAqICAgICAgICwgbm9vcCA9IGZ1bmN0aW9uICgpIHt9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5vb3ApLnRvLm5vdC5jaGFuZ2UobXlPYmosICd2YWwnKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChub29wKS50by5ub3QuZGVjcmVhc2UobXlPYmosICd2YWwnKTsgLy8gTm90IHJlY29tbWVuZGVkXG4gICAqXG4gICAqIGAuZGVjcmVhc2VgIGFjY2VwdHMgYW4gb3B0aW9uYWwgYG1zZ2AgYXJndW1lbnQgd2hpY2ggaXMgYSBjdXN0b20gZXJyb3JcbiAgICogbWVzc2FnZSB0byBzaG93IHdoZW4gdGhlIGFzc2VydGlvbiBmYWlscy4gVGhlIG1lc3NhZ2UgY2FuIGFsc28gYmUgZ2l2ZW4gYXNcbiAgICogdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC4gV2hlbiBub3QgcHJvdmlkaW5nIHR3byBhcmd1bWVudHMsIGFsd2F5c1xuICAgKiB1c2UgdGhlIHNlY29uZCBmb3JtLlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge3ZhbDogMX1cbiAgICogICAgICAgLCBub29wID0gZnVuY3Rpb24gKCkge307XG4gICAqXG4gICAqICAgICBleHBlY3Qobm9vcCkudG8uZGVjcmVhc2UobXlPYmosICd2YWwnLCAnbm9vbyB3aHkgZmFpbD8/Jyk7XG4gICAqXG4gICAqICAgICB2YXIgdmFsID0gMVxuICAgKiAgICAgICAsIG5vb3AgPSBmdW5jdGlvbiAoKSB7fVxuICAgKiAgICAgICAsIGdldFZhbCA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHZhbDsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChub29wLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uZGVjcmVhc2UoZ2V0VmFsKTtcbiAgICpcbiAgICogVGhlIGFsaWFzIGAuZGVjcmVhc2VzYCBjYW4gYmUgdXNlZCBpbnRlcmNoYW5nZWFibHkgd2l0aCBgLmRlY3JlYXNlYC5cbiAgICpcbiAgICogQG5hbWUgZGVjcmVhc2VcbiAgICogQGFsaWFzIGRlY3JlYXNlc1xuICAgKiBAcGFyYW0ge1N0cmluZ3xGdW5jdGlvbn0gc3ViamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcCBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydERlY3JlYXNlcyAoc3ViamVjdCwgcHJvcCwgbXNnKSB7XG4gICAgaWYgKG1zZykgZmxhZyh0aGlzLCAnbWVzc2FnZScsIG1zZyk7XG4gICAgdmFyIGZuID0gZmxhZyh0aGlzLCAnb2JqZWN0JylcbiAgICAgICwgZmxhZ01zZyA9IGZsYWcodGhpcywgJ21lc3NhZ2UnKVxuICAgICAgLCBzc2ZpID0gZmxhZyh0aGlzLCAnc3NmaScpO1xuICAgIG5ldyBBc3NlcnRpb24oZm4sIGZsYWdNc2csIHNzZmksIHRydWUpLmlzLmEoJ2Z1bmN0aW9uJyk7XG5cbiAgICB2YXIgaW5pdGlhbDtcbiAgICBpZiAoIXByb3ApIHtcbiAgICAgIG5ldyBBc3NlcnRpb24oc3ViamVjdCwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkuaXMuYSgnZnVuY3Rpb24nKTtcbiAgICAgIGluaXRpYWwgPSBzdWJqZWN0KCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG5ldyBBc3NlcnRpb24oc3ViamVjdCwgZmxhZ01zZywgc3NmaSwgdHJ1ZSkudG8uaGF2ZS5wcm9wZXJ0eShwcm9wKTtcbiAgICAgIGluaXRpYWwgPSBzdWJqZWN0W3Byb3BdO1xuICAgIH1cblxuICAgIC8vIE1ha2Ugc3VyZSB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXJcbiAgICBuZXcgQXNzZXJ0aW9uKGluaXRpYWwsIGZsYWdNc2csIHNzZmksIHRydWUpLmlzLmEoJ251bWJlcicpO1xuXG4gICAgZm4oKTtcblxuICAgIHZhciBmaW5hbCA9IHByb3AgPT09IHVuZGVmaW5lZCB8fCBwcm9wID09PSBudWxsID8gc3ViamVjdCgpIDogc3ViamVjdFtwcm9wXTtcbiAgICB2YXIgbXNnT2JqID0gcHJvcCA9PT0gdW5kZWZpbmVkIHx8IHByb3AgPT09IG51bGwgPyBpbml0aWFsIDogJy4nICsgcHJvcDtcblxuICAgIGZsYWcodGhpcywgJ2RlbHRhTXNnT2JqJywgbXNnT2JqKTtcbiAgICBmbGFnKHRoaXMsICdpbml0aWFsRGVsdGFWYWx1ZScsIGluaXRpYWwpO1xuICAgIGZsYWcodGhpcywgJ2ZpbmFsRGVsdGFWYWx1ZScsIGZpbmFsKTtcbiAgICBmbGFnKHRoaXMsICdkZWx0YUJlaGF2aW9yJywgJ2RlY3JlYXNlJyk7XG4gICAgZmxhZyh0aGlzLCAncmVhbERlbHRhJywgaW5pdGlhbCAtIGZpbmFsKTtcblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgZmluYWwgLSBpbml0aWFsIDwgMFxuICAgICAgLCAnZXhwZWN0ZWQgJyArIG1zZ09iaiArICcgdG8gZGVjcmVhc2UnXG4gICAgICAsICdleHBlY3RlZCAnICsgbXNnT2JqICsgJyB0byBub3QgZGVjcmVhc2UnXG4gICAgKTtcbiAgfVxuXG4gIEFzc2VydGlvbi5hZGRNZXRob2QoJ2RlY3JlYXNlJywgYXNzZXJ0RGVjcmVhc2VzKTtcbiAgQXNzZXJ0aW9uLmFkZE1ldGhvZCgnZGVjcmVhc2VzJywgYXNzZXJ0RGVjcmVhc2VzKTtcblxuICAvKipcbiAgICogIyMjIC5ieShkZWx0YVssIG1zZ10pXG4gICAqXG4gICAqIFdoZW4gZm9sbG93aW5nIGFuIGAuaW5jcmVhc2VgIGFzc2VydGlvbiBpbiB0aGUgY2hhaW4sIGAuYnlgIGFzc2VydHMgdGhhdFxuICAgKiB0aGUgc3ViamVjdCBvZiB0aGUgYC5pbmNyZWFzZWAgYXNzZXJ0aW9uIGluY3JlYXNlZCBieSB0aGUgZ2l2ZW4gYGRlbHRhYC5cbiAgICpcbiAgICogICAgIHZhciBteU9iaiA9IHt2YWw6IDF9XG4gICAqICAgICAgICwgYWRkVHdvID0gZnVuY3Rpb24gKCkgeyBteU9iai52YWwgKz0gMjsgfTtcbiAgICpcbiAgICogICAgIGV4cGVjdChhZGRUd28pLnRvLmluY3JlYXNlKG15T2JqLCAndmFsJykuYnkoMik7XG4gICAqXG4gICAqIFdoZW4gZm9sbG93aW5nIGEgYC5kZWNyZWFzZWAgYXNzZXJ0aW9uIGluIHRoZSBjaGFpbiwgYC5ieWAgYXNzZXJ0cyB0aGF0IHRoZVxuICAgKiBzdWJqZWN0IG9mIHRoZSBgLmRlY3JlYXNlYCBhc3NlcnRpb24gZGVjcmVhc2VkIGJ5IHRoZSBnaXZlbiBgZGVsdGFgLlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge3ZhbDogMX1cbiAgICogICAgICAgLCBzdWJ0cmFjdFR3byA9IGZ1bmN0aW9uICgpIHsgbXlPYmoudmFsIC09IDI7IH07XG4gICAqXG4gICAqICAgICBleHBlY3Qoc3VidHJhY3RUd28pLnRvLmRlY3JlYXNlKG15T2JqLCAndmFsJykuYnkoMik7XG4gICAqXG4gICAqIFdoZW4gZm9sbG93aW5nIGEgYC5jaGFuZ2VgIGFzc2VydGlvbiBpbiB0aGUgY2hhaW4sIGAuYnlgIGFzc2VydHMgdGhhdCB0aGVcbiAgICogc3ViamVjdCBvZiB0aGUgYC5jaGFuZ2VgIGFzc2VydGlvbiBlaXRoZXIgaW5jcmVhc2VkIG9yIGRlY3JlYXNlZCBieSB0aGVcbiAgICogZ2l2ZW4gYGRlbHRhYC4gSG93ZXZlciwgaXQncyBkYW5nZXJvdXMgdG8gdXNlIGAuY2hhbmdlLmJ5YC4gVGhlIHByb2JsZW0gaXNcbiAgICogdGhhdCBpdCBjcmVhdGVzIHVuY2VydGFpbiBleHBlY3RhdGlvbnMuIEl0J3Mgb2Z0ZW4gYmVzdCB0byBpZGVudGlmeSB0aGVcbiAgICogZXhhY3Qgb3V0cHV0IHRoYXQncyBleHBlY3RlZCwgYW5kIHRoZW4gd3JpdGUgYW4gYXNzZXJ0aW9uIHRoYXQgb25seSBhY2NlcHRzXG4gICAqIHRoYXQgZXhhY3Qgb3V0cHV0LlxuICAgKlxuICAgKiAgICAgdmFyIG15T2JqID0ge3ZhbDogMX1cbiAgICogICAgICAgLCBhZGRUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCArPSAyOyB9XG4gICAqICAgICAgICwgc3VidHJhY3RUd28gPSBmdW5jdGlvbiAoKSB7IG15T2JqLnZhbCAtPSAyOyB9O1xuICAgKlxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChhZGRUd28pLnRvLmNoYW5nZShteU9iaiwgJ3ZhbCcpLmJ5KDIpOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uZGVjcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChzdWJ0cmFjdFR3bykudG8uY2hhbmdlKG15T2JqLCAndmFsJykuYnkoMik7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLmJ5YC4gSG93ZXZlciwgaXQncyBvZnRlbiBiZXN0XG4gICAqIHRvIGFzc2VydCB0aGF0IHRoZSBzdWJqZWN0IGNoYW5nZWQgYnkgaXRzIGV4cGVjdGVkIGRlbHRhLCByYXRoZXIgdGhhblxuICAgKiBhc3NlcnRpbmcgdGhhdCBpdCBkaWRuJ3QgY2hhbmdlIGJ5IG9uZSBvZiBjb3VudGxlc3MgdW5leHBlY3RlZCBkZWx0YXMuXG4gICAqXG4gICAqICAgICB2YXIgbXlPYmogPSB7dmFsOiAxfVxuICAgKiAgICAgICAsIGFkZFR3byA9IGZ1bmN0aW9uICgpIHsgbXlPYmoudmFsICs9IDI7IH07XG4gICAqXG4gICAqICAgICAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKS5ieSgyKTtcbiAgICpcbiAgICogICAgIC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KGFkZFR3bykudG8uaW5jcmVhc2UobXlPYmosICd2YWwnKS5idXQubm90LmJ5KDMpO1xuICAgKlxuICAgKiBgLmJ5YCBhY2NlcHRzIGFuIG9wdGlvbmFsIGBtc2dgIGFyZ3VtZW50IHdoaWNoIGlzIGEgY3VzdG9tIGVycm9yIG1lc3NhZ2UgdG9cbiAgICogc2hvdyB3aGVuIHRoZSBhc3NlcnRpb24gZmFpbHMuIFRoZSBtZXNzYWdlIGNhbiBhbHNvIGJlIGdpdmVuIGFzIHRoZSBzZWNvbmRcbiAgICogYXJndW1lbnQgdG8gYGV4cGVjdGAuXG4gICAqXG4gICAqICAgICB2YXIgbXlPYmogPSB7dmFsOiAxfVxuICAgKiAgICAgICAsIGFkZFR3byA9IGZ1bmN0aW9uICgpIHsgbXlPYmoudmFsICs9IDI7IH07XG4gICAqXG4gICAqICAgICBleHBlY3QoYWRkVHdvKS50by5pbmNyZWFzZShteU9iaiwgJ3ZhbCcpLmJ5KDMsICdub29vIHdoeSBmYWlsPz8nKTtcbiAgICogICAgIGV4cGVjdChhZGRUd28sICdub29vIHdoeSBmYWlsPz8nKS50by5pbmNyZWFzZShteU9iaiwgJ3ZhbCcpLmJ5KDMpO1xuICAgKlxuICAgKiBAbmFtZSBieVxuICAgKiBAcGFyYW0ge051bWJlcn0gZGVsdGFcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1zZyBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGZ1bmN0aW9uIGFzc2VydERlbHRhKGRlbHRhLCBtc2cpIHtcbiAgICBpZiAobXNnKSBmbGFnKHRoaXMsICdtZXNzYWdlJywgbXNnKTtcblxuICAgIHZhciBtc2dPYmogPSBmbGFnKHRoaXMsICdkZWx0YU1zZ09iaicpO1xuICAgIHZhciBpbml0aWFsID0gZmxhZyh0aGlzLCAnaW5pdGlhbERlbHRhVmFsdWUnKTtcbiAgICB2YXIgZmluYWwgPSBmbGFnKHRoaXMsICdmaW5hbERlbHRhVmFsdWUnKTtcbiAgICB2YXIgYmVoYXZpb3IgPSBmbGFnKHRoaXMsICdkZWx0YUJlaGF2aW9yJyk7XG4gICAgdmFyIHJlYWxEZWx0YSA9IGZsYWcodGhpcywgJ3JlYWxEZWx0YScpO1xuXG4gICAgdmFyIGV4cHJlc3Npb247XG4gICAgaWYgKGJlaGF2aW9yID09PSAnY2hhbmdlJykge1xuICAgICAgZXhwcmVzc2lvbiA9IE1hdGguYWJzKGZpbmFsIC0gaW5pdGlhbCkgPT09IE1hdGguYWJzKGRlbHRhKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZXhwcmVzc2lvbiA9IHJlYWxEZWx0YSA9PT0gTWF0aC5hYnMoZGVsdGEpO1xuICAgIH1cblxuICAgIHRoaXMuYXNzZXJ0KFxuICAgICAgZXhwcmVzc2lvblxuICAgICAgLCAnZXhwZWN0ZWQgJyArIG1zZ09iaiArICcgdG8gJyArIGJlaGF2aW9yICsgJyBieSAnICsgZGVsdGFcbiAgICAgICwgJ2V4cGVjdGVkICcgKyBtc2dPYmogKyAnIHRvIG5vdCAnICsgYmVoYXZpb3IgKyAnIGJ5ICcgKyBkZWx0YVxuICAgICk7XG4gIH1cblxuICBBc3NlcnRpb24uYWRkTWV0aG9kKCdieScsIGFzc2VydERlbHRhKTtcblxuICAvKipcbiAgICogIyMjIC5leHRlbnNpYmxlXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIGV4dGVuc2libGUsIHdoaWNoIG1lYW5zIHRoYXQgbmV3IHByb3BlcnRpZXMgY2FuXG4gICAqIGJlIGFkZGVkIHRvIGl0LiBQcmltaXRpdmVzIGFyZSBuZXZlciBleHRlbnNpYmxlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSkudG8uYmUuZXh0ZW5zaWJsZTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5leHRlbnNpYmxlYC5cbiAgICpcbiAgICogICAgIHZhciBub25FeHRlbnNpYmxlT2JqZWN0ID0gT2JqZWN0LnByZXZlbnRFeHRlbnNpb25zKHt9KVxuICAgKiAgICAgICAsIHNlYWxlZE9iamVjdCA9IE9iamVjdC5zZWFsKHt9KVxuICAgKiAgICAgICAsIGZyb3plbk9iamVjdCA9IE9iamVjdC5mcmVlemUoe30pO1xuICAgKlxuICAgKiAgICAgZXhwZWN0KG5vbkV4dGVuc2libGVPYmplY3QpLnRvLm5vdC5iZS5leHRlbnNpYmxlO1xuICAgKiAgICAgZXhwZWN0KHNlYWxlZE9iamVjdCkudG8ubm90LmJlLmV4dGVuc2libGU7XG4gICAqICAgICBleHBlY3QoZnJvemVuT2JqZWN0KS50by5ub3QuYmUuZXh0ZW5zaWJsZTtcbiAgICogICAgIGV4cGVjdCgxKS50by5ub3QuYmUuZXh0ZW5zaWJsZTtcbiAgICpcbiAgICogQSBjdXN0b20gZXJyb3IgbWVzc2FnZSBjYW4gYmUgZ2l2ZW4gYXMgdGhlIHNlY29uZCBhcmd1bWVudCB0byBgZXhwZWN0YC5cbiAgICpcbiAgICogICAgIGV4cGVjdCgxLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUuZXh0ZW5zaWJsZTtcbiAgICpcbiAgICogQG5hbWUgZXh0ZW5zaWJsZVxuICAgKiBAbmFtZXNwYWNlIEJERFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBBc3NlcnRpb24uYWRkUHJvcGVydHkoJ2V4dGVuc2libGUnLCBmdW5jdGlvbigpIHtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0Jyk7XG5cbiAgICAvLyBJbiBFUzUsIGlmIHRoZSBhcmd1bWVudCB0byB0aGlzIG1ldGhvZCBpcyBhIHByaW1pdGl2ZSwgdGhlbiBpdCB3aWxsIGNhdXNlIGEgVHlwZUVycm9yLlxuICAgIC8vIEluIEVTNiwgYSBub24tb2JqZWN0IGFyZ3VtZW50IHdpbGwgYmUgdHJlYXRlZCBhcyBpZiBpdCB3YXMgYSBub24tZXh0ZW5zaWJsZSBvcmRpbmFyeSBvYmplY3QsIHNpbXBseSByZXR1cm4gZmFsc2UuXG4gICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvT2JqZWN0L2lzRXh0ZW5zaWJsZVxuICAgIC8vIFRoZSBmb2xsb3dpbmcgcHJvdmlkZXMgRVM2IGJlaGF2aW9yIGZvciBFUzUgZW52aXJvbm1lbnRzLlxuXG4gICAgdmFyIGlzRXh0ZW5zaWJsZSA9IG9iaiA9PT0gT2JqZWN0KG9iaikgJiYgT2JqZWN0LmlzRXh0ZW5zaWJsZShvYmopO1xuXG4gICAgdGhpcy5hc3NlcnQoXG4gICAgICBpc0V4dGVuc2libGVcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gYmUgZXh0ZW5zaWJsZSdcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIGV4dGVuc2libGUnXG4gICAgKTtcbiAgfSk7XG5cbiAgLyoqXG4gICAqICMjIyAuc2VhbGVkXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIHNlYWxlZCwgd2hpY2ggbWVhbnMgdGhhdCBuZXcgcHJvcGVydGllcyBjYW4ndCBiZVxuICAgKiBhZGRlZCB0byBpdCwgYW5kIGl0cyBleGlzdGluZyBwcm9wZXJ0aWVzIGNhbid0IGJlIHJlY29uZmlndXJlZCBvciBkZWxldGVkLlxuICAgKiBIb3dldmVyLCBpdCdzIHBvc3NpYmxlIHRoYXQgaXRzIGV4aXN0aW5nIHByb3BlcnRpZXMgY2FuIHN0aWxsIGJlIHJlYXNzaWduZWRcbiAgICogdG8gZGlmZmVyZW50IHZhbHVlcy4gUHJpbWl0aXZlcyBhcmUgYWx3YXlzIHNlYWxlZC5cbiAgICpcbiAgICogICAgIHZhciBzZWFsZWRPYmplY3QgPSBPYmplY3Quc2VhbCh7fSk7XG4gICAqICAgICB2YXIgZnJvemVuT2JqZWN0ID0gT2JqZWN0LmZyZWV6ZSh7fSk7XG4gICAqXG4gICAqICAgICBleHBlY3Qoc2VhbGVkT2JqZWN0KS50by5iZS5zZWFsZWQ7XG4gICAqICAgICBleHBlY3QoZnJvemVuT2JqZWN0KS50by5iZS5zZWFsZWQ7XG4gICAqICAgICBleHBlY3QoMSkudG8uYmUuc2VhbGVkO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLnNlYWxlZGAuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5ub3QuYmUuc2VhbGVkO1xuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLnNlYWxlZDtcbiAgICpcbiAgICogQG5hbWUgc2VhbGVkXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnc2VhbGVkJywgZnVuY3Rpb24oKSB7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpO1xuXG4gICAgLy8gSW4gRVM1LCBpZiB0aGUgYXJndW1lbnQgdG8gdGhpcyBtZXRob2QgaXMgYSBwcmltaXRpdmUsIHRoZW4gaXQgd2lsbCBjYXVzZSBhIFR5cGVFcnJvci5cbiAgICAvLyBJbiBFUzYsIGEgbm9uLW9iamVjdCBhcmd1bWVudCB3aWxsIGJlIHRyZWF0ZWQgYXMgaWYgaXQgd2FzIGEgc2VhbGVkIG9yZGluYXJ5IG9iamVjdCwgc2ltcGx5IHJldHVybiB0cnVlLlxuICAgIC8vIFNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9PYmplY3QvaXNTZWFsZWRcbiAgICAvLyBUaGUgZm9sbG93aW5nIHByb3ZpZGVzIEVTNiBiZWhhdmlvciBmb3IgRVM1IGVudmlyb25tZW50cy5cblxuICAgIHZhciBpc1NlYWxlZCA9IG9iaiA9PT0gT2JqZWN0KG9iaikgPyBPYmplY3QuaXNTZWFsZWQob2JqKSA6IHRydWU7XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgIGlzU2VhbGVkXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIHNlYWxlZCdcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIHNlYWxlZCdcbiAgICApO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5mcm96ZW5cbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgZnJvemVuLCB3aGljaCBtZWFucyB0aGF0IG5ldyBwcm9wZXJ0aWVzIGNhbid0IGJlXG4gICAqIGFkZGVkIHRvIGl0LCBhbmQgaXRzIGV4aXN0aW5nIHByb3BlcnRpZXMgY2FuJ3QgYmUgcmVhc3NpZ25lZCB0byBkaWZmZXJlbnRcbiAgICogdmFsdWVzLCByZWNvbmZpZ3VyZWQsIG9yIGRlbGV0ZWQuIFByaW1pdGl2ZXMgYXJlIGFsd2F5cyBmcm96ZW4uXG4gICAqXG4gICAqICAgICB2YXIgZnJvemVuT2JqZWN0ID0gT2JqZWN0LmZyZWV6ZSh7fSk7XG4gICAqXG4gICAqICAgICBleHBlY3QoZnJvemVuT2JqZWN0KS50by5iZS5mcm96ZW47XG4gICAqICAgICBleHBlY3QoMSkudG8uYmUuZnJvemVuO1xuICAgKlxuICAgKiBBZGQgYC5ub3RgIGVhcmxpZXIgaW4gdGhlIGNoYWluIHRvIG5lZ2F0ZSBgLmZyb3plbmAuXG4gICAqXG4gICAqICAgICBleHBlY3Qoe2E6IDF9KS50by5ub3QuYmUuZnJvemVuO1xuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KHthOiAxfSwgJ25vb28gd2h5IGZhaWw/PycpLnRvLmJlLmZyb3plbjtcbiAgICpcbiAgICogQG5hbWUgZnJvemVuXG4gICAqIEBuYW1lc3BhY2UgQkREXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIEFzc2VydGlvbi5hZGRQcm9wZXJ0eSgnZnJvemVuJywgZnVuY3Rpb24oKSB7XG4gICAgdmFyIG9iaiA9IGZsYWcodGhpcywgJ29iamVjdCcpO1xuXG4gICAgLy8gSW4gRVM1LCBpZiB0aGUgYXJndW1lbnQgdG8gdGhpcyBtZXRob2QgaXMgYSBwcmltaXRpdmUsIHRoZW4gaXQgd2lsbCBjYXVzZSBhIFR5cGVFcnJvci5cbiAgICAvLyBJbiBFUzYsIGEgbm9uLW9iamVjdCBhcmd1bWVudCB3aWxsIGJlIHRyZWF0ZWQgYXMgaWYgaXQgd2FzIGEgZnJvemVuIG9yZGluYXJ5IG9iamVjdCwgc2ltcGx5IHJldHVybiB0cnVlLlxuICAgIC8vIFNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9PYmplY3QvaXNGcm96ZW5cbiAgICAvLyBUaGUgZm9sbG93aW5nIHByb3ZpZGVzIEVTNiBiZWhhdmlvciBmb3IgRVM1IGVudmlyb25tZW50cy5cblxuICAgIHZhciBpc0Zyb3plbiA9IG9iaiA9PT0gT2JqZWN0KG9iaikgPyBPYmplY3QuaXNGcm96ZW4ob2JqKSA6IHRydWU7XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgIGlzRnJvemVuXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGJlIGZyb3plbidcbiAgICAgICwgJ2V4cGVjdGVkICN7dGhpc30gdG8gbm90IGJlIGZyb3plbidcbiAgICApO1xuICB9KTtcblxuICAvKipcbiAgICogIyMjIC5maW5pdGVcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgYSBudW1iZXIsIGFuZCBpc24ndCBgTmFOYCBvciBwb3NpdGl2ZS9uZWdhdGl2ZVxuICAgKiBgSW5maW5pdHlgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KDEpLnRvLmJlLmZpbml0ZTtcbiAgICpcbiAgICogQWRkIGAubm90YCBlYXJsaWVyIGluIHRoZSBjaGFpbiB0byBuZWdhdGUgYC5maW5pdGVgLiBIb3dldmVyLCBpdCdzXG4gICAqIGRhbmdlcm91cyB0byBkbyBzby4gVGhlIHByb2JsZW0gaXMgdGhhdCBpdCBjcmVhdGVzIHVuY2VydGFpbiBleHBlY3RhdGlvbnNcbiAgICogYnkgYXNzZXJ0aW5nIHRoYXQgdGhlIHN1YmplY3QgZWl0aGVyIGlzbid0IGEgbnVtYmVyLCBvciB0aGF0IGl0J3MgYE5hTmAsIG9yXG4gICAqIHRoYXQgaXQncyBwb3NpdGl2ZSBgSW5maW5pdHlgLCBvciB0aGF0IGl0J3MgbmVnYXRpdmUgYEluZmluaXR5YC4gSXQncyBvZnRlblxuICAgKiBiZXN0IHRvIGlkZW50aWZ5IHRoZSBleGFjdCBvdXRwdXQgdGhhdCdzIGV4cGVjdGVkLCBhbmQgdGhlbiB3cml0ZSBhblxuICAgKiBhc3NlcnRpb24gdGhhdCBvbmx5IGFjY2VwdHMgdGhhdCBleGFjdCBvdXRwdXQuXG4gICAqXG4gICAqIFdoZW4gdGhlIHRhcmdldCBpc24ndCBleHBlY3RlZCB0byBiZSBhIG51bWJlciwgaXQncyBvZnRlbiBiZXN0IHRvIGFzc2VydFxuICAgKiB0aGF0IGl0J3MgdGhlIGV4cGVjdGVkIHR5cGUsIHJhdGhlciB0aGFuIGFzc2VydGluZyB0aGF0IGl0IGlzbid0IG9uZSBvZlxuICAgKiBtYW55IHVuZXhwZWN0ZWQgdHlwZXMuXG4gICAqXG4gICAqICAgICBleHBlY3QoJ2ZvbycpLnRvLmJlLmEoJ3N0cmluZycpOyAvLyBSZWNvbW1lbmRlZFxuICAgKiAgICAgZXhwZWN0KCdmb28nKS50by5ub3QuYmUuZmluaXRlOyAvLyBOb3QgcmVjb21tZW5kZWRcbiAgICpcbiAgICogV2hlbiB0aGUgdGFyZ2V0IGlzIGV4cGVjdGVkIHRvIGJlIGBOYU5gLCBpdCdzIG9mdGVuIGJlc3QgdG8gYXNzZXJ0IGV4YWN0bHlcbiAgICogdGhhdC5cbiAgICpcbiAgICogICAgIGV4cGVjdChOYU4pLnRvLmJlLk5hTjsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdChOYU4pLnRvLm5vdC5iZS5maW5pdGU7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgZXhwZWN0ZWQgdG8gYmUgcG9zaXRpdmUgaW5maW5pdHksIGl0J3Mgb2Z0ZW4gYmVzdCB0b1xuICAgKiBhc3NlcnQgZXhhY3RseSB0aGF0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KEluZmluaXR5KS50by5lcXVhbChJbmZpbml0eSk7IC8vIFJlY29tbWVuZGVkXG4gICAqICAgICBleHBlY3QoSW5maW5pdHkpLnRvLm5vdC5iZS5maW5pdGU7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBXaGVuIHRoZSB0YXJnZXQgaXMgZXhwZWN0ZWQgdG8gYmUgbmVnYXRpdmUgaW5maW5pdHksIGl0J3Mgb2Z0ZW4gYmVzdCB0b1xuICAgKiBhc3NlcnQgZXhhY3RseSB0aGF0LlxuICAgKlxuICAgKiAgICAgZXhwZWN0KC1JbmZpbml0eSkudG8uZXF1YWwoLUluZmluaXR5KTsgLy8gUmVjb21tZW5kZWRcbiAgICogICAgIGV4cGVjdCgtSW5maW5pdHkpLnRvLm5vdC5iZS5maW5pdGU7IC8vIE5vdCByZWNvbW1lbmRlZFxuICAgKlxuICAgKiBBIGN1c3RvbSBlcnJvciBtZXNzYWdlIGNhbiBiZSBnaXZlbiBhcyB0aGUgc2Vjb25kIGFyZ3VtZW50IHRvIGBleHBlY3RgLlxuICAgKlxuICAgKiAgICAgZXhwZWN0KCdmb28nLCAnbm9vbyB3aHkgZmFpbD8/JykudG8uYmUuZmluaXRlO1xuICAgKlxuICAgKiBAbmFtZSBmaW5pdGVcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdmaW5pdGUnLCBmdW5jdGlvbihtc2cpIHtcbiAgICB2YXIgb2JqID0gZmxhZyh0aGlzLCAnb2JqZWN0Jyk7XG5cbiAgICB0aGlzLmFzc2VydChcbiAgICAgICAgdHlwZW9mIG9iaiA9PT0gJ251bWJlcicgJiYgaXNGaW5pdGUob2JqKVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBiZSBhIGZpbml0ZSBudW1iZXInXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBiZSBhIGZpbml0ZSBudW1iZXInXG4gICAgKTtcbiAgfSk7XG59O1xuIiwiLyohXG4gKiBjaGFpXG4gKiBDb3B5cmlnaHQoYykgMjAxMS0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY2hhaSwgdXRpbCkge1xuICAvKiFcbiAgICogQ2hhaSBkZXBlbmRlbmNpZXMuXG4gICAqL1xuXG4gIHZhciBBc3NlcnRpb24gPSBjaGFpLkFzc2VydGlvblxuICAgICwgZmxhZyA9IHV0aWwuZmxhZztcblxuICAvKiFcbiAgICogTW9kdWxlIGV4cG9ydC5cbiAgICovXG5cbiAgLyoqXG4gICAqICMjIyBhc3NlcnQoZXhwcmVzc2lvbiwgbWVzc2FnZSlcbiAgICpcbiAgICogV3JpdGUgeW91ciBvd24gdGVzdCBleHByZXNzaW9ucy5cbiAgICpcbiAgICogICAgIGFzc2VydCgnZm9vJyAhPT0gJ2JhcicsICdmb28gaXMgbm90IGJhcicpO1xuICAgKiAgICAgYXNzZXJ0KEFycmF5LmlzQXJyYXkoW10pLCAnZW1wdHkgYXJyYXlzIGFyZSBhcnJheXMnKTtcbiAgICpcbiAgICogQHBhcmFtIHtNaXhlZH0gZXhwcmVzc2lvbiB0byB0ZXN0IGZvciB0cnV0aGluZXNzXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIHRvIGRpc3BsYXkgb24gZXJyb3JcbiAgICogQG5hbWUgYXNzZXJ0XG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIHZhciBhc3NlcnQgPSBjaGFpLmFzc2VydCA9IGZ1bmN0aW9uIChleHByZXNzLCBlcnJtc2cpIHtcbiAgICB2YXIgdGVzdCA9IG5ldyBBc3NlcnRpb24obnVsbCwgbnVsbCwgY2hhaS5hc3NlcnQsIHRydWUpO1xuICAgIHRlc3QuYXNzZXJ0KFxuICAgICAgICBleHByZXNzXG4gICAgICAsIGVycm1zZ1xuICAgICAgLCAnWyBuZWdhdGlvbiBtZXNzYWdlIHVuYXZhaWxhYmxlIF0nXG4gICAgKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5mYWlsKFttZXNzYWdlXSlcbiAgICogIyMjIC5mYWlsKGFjdHVhbCwgZXhwZWN0ZWQsIFttZXNzYWdlXSwgW29wZXJhdG9yXSlcbiAgICpcbiAgICogVGhyb3cgYSBmYWlsdXJlLiBOb2RlLmpzIGBhc3NlcnRgIG1vZHVsZS1jb21wYXRpYmxlLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmZhaWwoKTtcbiAgICogICAgIGFzc2VydC5mYWlsKFwiY3VzdG9tIGVycm9yIG1lc3NhZ2VcIik7XG4gICAqICAgICBhc3NlcnQuZmFpbCgxLCAyKTtcbiAgICogICAgIGFzc2VydC5mYWlsKDEsIDIsIFwiY3VzdG9tIGVycm9yIG1lc3NhZ2VcIik7XG4gICAqICAgICBhc3NlcnQuZmFpbCgxLCAyLCBcImN1c3RvbSBlcnJvciBtZXNzYWdlXCIsIFwiPlwiKTtcbiAgICogICAgIGFzc2VydC5mYWlsKDEsIDIsIHVuZGVmaW5lZCwgXCI+XCIpO1xuICAgKlxuICAgKiBAbmFtZSBmYWlsXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGFjdHVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBleHBlY3RlZFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gb3BlcmF0b3JcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmZhaWwgPSBmdW5jdGlvbiAoYWN0dWFsLCBleHBlY3RlZCwgbWVzc2FnZSwgb3BlcmF0b3IpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA8IDIpIHtcbiAgICAgICAgLy8gQ29tcGx5IHdpdGggTm9kZSdzIGZhaWwoW21lc3NhZ2VdKSBpbnRlcmZhY2VcblxuICAgICAgICBtZXNzYWdlID0gYWN0dWFsO1xuICAgICAgICBhY3R1YWwgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgbWVzc2FnZSA9IG1lc3NhZ2UgfHwgJ2Fzc2VydC5mYWlsKCknO1xuICAgIHRocm93IG5ldyBjaGFpLkFzc2VydGlvbkVycm9yKG1lc3NhZ2UsIHtcbiAgICAgICAgYWN0dWFsOiBhY3R1YWxcbiAgICAgICwgZXhwZWN0ZWQ6IGV4cGVjdGVkXG4gICAgICAsIG9wZXJhdG9yOiBvcGVyYXRvclxuICAgIH0sIGFzc2VydC5mYWlsKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc09rKG9iamVjdCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgdHJ1dGh5LlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzT2soJ2V2ZXJ5dGhpbmcnLCAnZXZlcnl0aGluZyBpcyBvaycpO1xuICAgKiAgICAgYXNzZXJ0LmlzT2soZmFsc2UsICd0aGlzIHdpbGwgZmFpbCcpO1xuICAgKlxuICAgKiBAbmFtZSBpc09rXG4gICAqIEBhbGlhcyBva1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3QgdG8gdGVzdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNPayA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc09rLCB0cnVlKS5pcy5vaztcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc05vdE9rKG9iamVjdCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgZmFsc3kuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaXNOb3RPaygnZXZlcnl0aGluZycsICd0aGlzIHdpbGwgZmFpbCcpO1xuICAgKiAgICAgYXNzZXJ0LmlzTm90T2soZmFsc2UsICd0aGlzIHdpbGwgcGFzcycpO1xuICAgKlxuICAgKiBAbmFtZSBpc05vdE9rXG4gICAqIEBhbGlhcyBub3RPa1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3QgdG8gdGVzdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOb3RPayA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc05vdE9rLCB0cnVlKS5pcy5ub3Qub2s7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuZXF1YWwoYWN0dWFsLCBleHBlY3RlZCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIG5vbi1zdHJpY3QgZXF1YWxpdHkgKGA9PWApIG9mIGBhY3R1YWxgIGFuZCBgZXhwZWN0ZWRgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmVxdWFsKDMsICczJywgJz09IGNvZXJjZXMgdmFsdWVzIHRvIHN0cmluZ3MnKTtcbiAgICpcbiAgICogQG5hbWUgZXF1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gYWN0dWFsXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGV4cGVjdGVkXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5lcXVhbCA9IGZ1bmN0aW9uIChhY3QsIGV4cCwgbXNnKSB7XG4gICAgdmFyIHRlc3QgPSBuZXcgQXNzZXJ0aW9uKGFjdCwgbXNnLCBhc3NlcnQuZXF1YWwsIHRydWUpO1xuXG4gICAgdGVzdC5hc3NlcnQoXG4gICAgICAgIGV4cCA9PSBmbGFnKHRlc3QsICdvYmplY3QnKVxuICAgICAgLCAnZXhwZWN0ZWQgI3t0aGlzfSB0byBlcXVhbCAje2V4cH0nXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBlcXVhbCAje2FjdH0nXG4gICAgICAsIGV4cFxuICAgICAgLCBhY3RcbiAgICAgICwgdHJ1ZVxuICAgICk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90RXF1YWwoYWN0dWFsLCBleHBlY3RlZCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIG5vbi1zdHJpY3QgaW5lcXVhbGl0eSAoYCE9YCkgb2YgYGFjdHVhbGAgYW5kIGBleHBlY3RlZGAuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90RXF1YWwoMywgNCwgJ3RoZXNlIG51bWJlcnMgYXJlIG5vdCBlcXVhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBub3RFcXVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBhY3R1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gZXhwZWN0ZWRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdEVxdWFsID0gZnVuY3Rpb24gKGFjdCwgZXhwLCBtc2cpIHtcbiAgICB2YXIgdGVzdCA9IG5ldyBBc3NlcnRpb24oYWN0LCBtc2csIGFzc2VydC5ub3RFcXVhbCwgdHJ1ZSk7XG5cbiAgICB0ZXN0LmFzc2VydChcbiAgICAgICAgZXhwICE9IGZsYWcodGVzdCwgJ29iamVjdCcpXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIG5vdCBlcXVhbCAje2V4cH0nXG4gICAgICAsICdleHBlY3RlZCAje3RoaXN9IHRvIGVxdWFsICN7YWN0fSdcbiAgICAgICwgZXhwXG4gICAgICAsIGFjdFxuICAgICAgLCB0cnVlXG4gICAgKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5zdHJpY3RFcXVhbChhY3R1YWwsIGV4cGVjdGVkLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgc3RyaWN0IGVxdWFsaXR5IChgPT09YCkgb2YgYGFjdHVhbGAgYW5kIGBleHBlY3RlZGAuXG4gICAqXG4gICAqICAgICBhc3NlcnQuc3RyaWN0RXF1YWwodHJ1ZSwgdHJ1ZSwgJ3RoZXNlIGJvb2xlYW5zIGFyZSBzdHJpY3RseSBlcXVhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBzdHJpY3RFcXVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBhY3R1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gZXhwZWN0ZWRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnN0cmljdEVxdWFsID0gZnVuY3Rpb24gKGFjdCwgZXhwLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGFjdCwgbXNnLCBhc3NlcnQuc3RyaWN0RXF1YWwsIHRydWUpLnRvLmVxdWFsKGV4cCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90U3RyaWN0RXF1YWwoYWN0dWFsLCBleHBlY3RlZCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHN0cmljdCBpbmVxdWFsaXR5IChgIT09YCkgb2YgYGFjdHVhbGAgYW5kIGBleHBlY3RlZGAuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90U3RyaWN0RXF1YWwoMywgJzMnLCAnbm8gY29lcmNpb24gZm9yIHN0cmljdCBlcXVhbGl0eScpO1xuICAgKlxuICAgKiBAbmFtZSBub3RTdHJpY3RFcXVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBhY3R1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gZXhwZWN0ZWRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdFN0cmljdEVxdWFsID0gZnVuY3Rpb24gKGFjdCwgZXhwLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGFjdCwgbXNnLCBhc3NlcnQubm90U3RyaWN0RXF1YWwsIHRydWUpLnRvLm5vdC5lcXVhbChleHApO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmRlZXBFcXVhbChhY3R1YWwsIGV4cGVjdGVkLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgYWN0dWFsYCBpcyBkZWVwbHkgZXF1YWwgdG8gYGV4cGVjdGVkYC5cbiAgICpcbiAgICogICAgIGFzc2VydC5kZWVwRXF1YWwoeyB0ZWE6ICdncmVlbicgfSwgeyB0ZWE6ICdncmVlbicgfSk7XG4gICAqXG4gICAqIEBuYW1lIGRlZXBFcXVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBhY3R1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gZXhwZWN0ZWRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQGFsaWFzIGRlZXBTdHJpY3RFcXVhbFxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuZGVlcEVxdWFsID0gYXNzZXJ0LmRlZXBTdHJpY3RFcXVhbCA9IGZ1bmN0aW9uIChhY3QsIGV4cCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihhY3QsIG1zZywgYXNzZXJ0LmRlZXBFcXVhbCwgdHJ1ZSkudG8uZXFsKGV4cCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90RGVlcEVxdWFsKGFjdHVhbCwgZXhwZWN0ZWQsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0IHRoYXQgYGFjdHVhbGAgaXMgbm90IGRlZXBseSBlcXVhbCB0byBgZXhwZWN0ZWRgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBFcXVhbCh7IHRlYTogJ2dyZWVuJyB9LCB7IHRlYTogJ2phc21pbmUnIH0pO1xuICAgKlxuICAgKiBAbmFtZSBub3REZWVwRXF1YWxcbiAgICogQHBhcmFtIHtNaXhlZH0gYWN0dWFsXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGV4cGVjdGVkXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3REZWVwRXF1YWwgPSBmdW5jdGlvbiAoYWN0LCBleHAsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oYWN0LCBtc2csIGFzc2VydC5ub3REZWVwRXF1YWwsIHRydWUpLnRvLm5vdC5lcWwoZXhwKTtcbiAgfTtcblxuICAgLyoqXG4gICAqICMjIyAuaXNBYm92ZSh2YWx1ZVRvQ2hlY2ssIHZhbHVlVG9CZUFib3ZlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgYHZhbHVlVG9DaGVja2AgaXMgc3RyaWN0bHkgZ3JlYXRlciB0aGFuICg+KSBgdmFsdWVUb0JlQWJvdmVgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzQWJvdmUoNSwgMiwgJzUgaXMgc3RyaWN0bHkgZ3JlYXRlciB0aGFuIDInKTtcbiAgICpcbiAgICogQG5hbWUgaXNBYm92ZVxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVRvQ2hlY2tcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVUb0JlQWJvdmVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzQWJvdmUgPSBmdW5jdGlvbiAodmFsLCBhYnYsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0Fib3ZlLCB0cnVlKS50by5iZS5hYm92ZShhYnYpO1xuICB9O1xuXG4gICAvKipcbiAgICogIyMjIC5pc0F0TGVhc3QodmFsdWVUb0NoZWNrLCB2YWx1ZVRvQmVBdExlYXN0LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgYHZhbHVlVG9DaGVja2AgaXMgZ3JlYXRlciB0aGFuIG9yIGVxdWFsIHRvICg+PSkgYHZhbHVlVG9CZUF0TGVhc3RgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzQXRMZWFzdCg1LCAyLCAnNSBpcyBncmVhdGVyIG9yIGVxdWFsIHRvIDInKTtcbiAgICogICAgIGFzc2VydC5pc0F0TGVhc3QoMywgMywgJzMgaXMgZ3JlYXRlciBvciBlcXVhbCB0byAzJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzQXRMZWFzdFxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVRvQ2hlY2tcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVUb0JlQXRMZWFzdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNBdExlYXN0ID0gZnVuY3Rpb24gKHZhbCwgYXRsc3QsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0F0TGVhc3QsIHRydWUpLnRvLmJlLmxlYXN0KGF0bHN0KTtcbiAgfTtcblxuICAgLyoqXG4gICAqICMjIyAuaXNCZWxvdyh2YWx1ZVRvQ2hlY2ssIHZhbHVlVG9CZUJlbG93LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgYHZhbHVlVG9DaGVja2AgaXMgc3RyaWN0bHkgbGVzcyB0aGFuICg8KSBgdmFsdWVUb0JlQmVsb3dgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzQmVsb3coMywgNiwgJzMgaXMgc3RyaWN0bHkgbGVzcyB0aGFuIDYnKTtcbiAgICpcbiAgICogQG5hbWUgaXNCZWxvd1xuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVRvQ2hlY2tcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVUb0JlQmVsb3dcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzQmVsb3cgPSBmdW5jdGlvbiAodmFsLCBibHcsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0JlbG93LCB0cnVlKS50by5iZS5iZWxvdyhibHcpO1xuICB9O1xuXG4gICAvKipcbiAgICogIyMjIC5pc0F0TW9zdCh2YWx1ZVRvQ2hlY2ssIHZhbHVlVG9CZUF0TW9zdCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIGB2YWx1ZVRvQ2hlY2tgIGlzIGxlc3MgdGhhbiBvciBlcXVhbCB0byAoPD0pIGB2YWx1ZVRvQmVBdE1vc3RgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzQXRNb3N0KDMsIDYsICczIGlzIGxlc3MgdGhhbiBvciBlcXVhbCB0byA2Jyk7XG4gICAqICAgICBhc3NlcnQuaXNBdE1vc3QoNCwgNCwgJzQgaXMgbGVzcyB0aGFuIG9yIGVxdWFsIHRvIDQnKTtcbiAgICpcbiAgICogQG5hbWUgaXNBdE1vc3RcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVUb0NoZWNrXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlVG9CZUF0TW9zdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNBdE1vc3QgPSBmdW5jdGlvbiAodmFsLCBhdG1zdCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzQXRNb3N0LCB0cnVlKS50by5iZS5tb3N0KGF0bXN0KTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc1RydWUodmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgdHJ1ZS5cbiAgICpcbiAgICogICAgIHZhciB0ZWFTZXJ2ZWQgPSB0cnVlO1xuICAgKiAgICAgYXNzZXJ0LmlzVHJ1ZSh0ZWFTZXJ2ZWQsICd0aGUgdGVhIGhhcyBiZWVuIHNlcnZlZCcpO1xuICAgKlxuICAgKiBAbmFtZSBpc1RydWVcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzVHJ1ZSA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc1RydWUsIHRydWUpLmlzWyd0cnVlJ107XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RUcnVlKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIG5vdCB0cnVlLlxuICAgKlxuICAgKiAgICAgdmFyIHRlYSA9ICd0YXN0eSBjaGFpJztcbiAgICogICAgIGFzc2VydC5pc05vdFRydWUodGVhLCAnZ3JlYXQsIHRpbWUgZm9yIHRlYSEnKTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3RUcnVlXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05vdFRydWUgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3RUcnVlLCB0cnVlKS50by5ub3QuZXF1YWwodHJ1ZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNGYWxzZSh2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBpcyBmYWxzZS5cbiAgICpcbiAgICogICAgIHZhciB0ZWFTZXJ2ZWQgPSBmYWxzZTtcbiAgICogICAgIGFzc2VydC5pc0ZhbHNlKHRlYVNlcnZlZCwgJ25vIHRlYSB5ZXQ/IGhtbS4uLicpO1xuICAgKlxuICAgKiBAbmFtZSBpc0ZhbHNlXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc0ZhbHNlID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzRmFsc2UsIHRydWUpLmlzWydmYWxzZSddO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzTm90RmFsc2UodmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgbm90IGZhbHNlLlxuICAgKlxuICAgKiAgICAgdmFyIHRlYSA9ICd0YXN0eSBjaGFpJztcbiAgICogICAgIGFzc2VydC5pc05vdEZhbHNlKHRlYSwgJ2dyZWF0LCB0aW1lIGZvciB0ZWEhJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTm90RmFsc2VcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzTm90RmFsc2UgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3RGYWxzZSwgdHJ1ZSkudG8ubm90LmVxdWFsKGZhbHNlKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc051bGwodmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgbnVsbC5cbiAgICpcbiAgICogICAgIGFzc2VydC5pc051bGwoZXJyLCAndGhlcmUgd2FzIG5vIGVycm9yJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTnVsbFxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOdWxsID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzTnVsbCwgdHJ1ZSkudG8uZXF1YWwobnVsbCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3ROdWxsKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIG5vdCBudWxsLlxuICAgKlxuICAgKiAgICAgdmFyIHRlYSA9ICd0YXN0eSBjaGFpJztcbiAgICogICAgIGFzc2VydC5pc05vdE51bGwodGVhLCAnZ3JlYXQsIHRpbWUgZm9yIHRlYSEnKTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3ROdWxsXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05vdE51bGwgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3ROdWxsLCB0cnVlKS50by5ub3QuZXF1YWwobnVsbCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOYU5cbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHZhbHVlIGlzIE5hTi5cbiAgICpcbiAgICogICAgIGFzc2VydC5pc05hTihOYU4sICdOYU4gaXMgTmFOJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTmFOXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05hTiA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc05hTiwgdHJ1ZSkudG8uYmUuTmFOO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzTm90TmFOXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB2YWx1ZSBpcyBub3QgTmFOLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzTm90TmFOKDQsICc0IGlzIG5vdCBOYU4nKTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3ROYU5cbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG4gIGFzc2VydC5pc05vdE5hTiA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc05vdE5hTiwgdHJ1ZSkubm90LnRvLmJlLk5hTjtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5leGlzdHNcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgbmVpdGhlciBgbnVsbGAgbm9yIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiAgICAgdmFyIGZvbyA9ICdoaSc7XG4gICAqXG4gICAqICAgICBhc3NlcnQuZXhpc3RzKGZvbywgJ2ZvbyBpcyBuZWl0aGVyIGBudWxsYCBub3IgYHVuZGVmaW5lZGAnKTtcbiAgICpcbiAgICogQG5hbWUgZXhpc3RzXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5leGlzdHMgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuZXhpc3RzLCB0cnVlKS50by5leGlzdDtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3RFeGlzdHNcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgZWl0aGVyIGBudWxsYCBvciBgdW5kZWZpbmVkYC5cbiAgICpcbiAgICogICAgIHZhciBiYXIgPSBudWxsXG4gICAqICAgICAgICwgYmF6O1xuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdEV4aXN0cyhiYXIpO1xuICAgKiAgICAgYXNzZXJ0Lm5vdEV4aXN0cyhiYXosICdiYXogaXMgZWl0aGVyIG51bGwgb3IgdW5kZWZpbmVkJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdEV4aXN0c1xuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90RXhpc3RzID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0Lm5vdEV4aXN0cywgdHJ1ZSkudG8ubm90LmV4aXN0O1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzVW5kZWZpbmVkKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGB1bmRlZmluZWRgLlxuICAgKlxuICAgKiAgICAgdmFyIHRlYTtcbiAgICogICAgIGFzc2VydC5pc1VuZGVmaW5lZCh0ZWEsICdubyB0ZWEgZGVmaW5lZCcpO1xuICAgKlxuICAgKiBAbmFtZSBpc1VuZGVmaW5lZFxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNVbmRlZmluZWQgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNVbmRlZmluZWQsIHRydWUpLnRvLmVxdWFsKHVuZGVmaW5lZCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNEZWZpbmVkKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIG5vdCBgdW5kZWZpbmVkYC5cbiAgICpcbiAgICogICAgIHZhciB0ZWEgPSAnY3VwIG9mIGNoYWknO1xuICAgKiAgICAgYXNzZXJ0LmlzRGVmaW5lZCh0ZWEsICd0ZWEgaGFzIGJlZW4gZGVmaW5lZCcpO1xuICAgKlxuICAgKiBAbmFtZSBpc0RlZmluZWRcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzRGVmaW5lZCA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0RlZmluZWQsIHRydWUpLnRvLm5vdC5lcXVhbCh1bmRlZmluZWQpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzRnVuY3Rpb24odmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgYSBmdW5jdGlvbi5cbiAgICpcbiAgICogICAgIGZ1bmN0aW9uIHNlcnZlVGVhKCkgeyByZXR1cm4gJ2N1cCBvZiB0ZWEnOyB9O1xuICAgKiAgICAgYXNzZXJ0LmlzRnVuY3Rpb24oc2VydmVUZWEsICdncmVhdCwgd2UgY2FuIGhhdmUgdGVhIG5vdycpO1xuICAgKlxuICAgKiBAbmFtZSBpc0Z1bmN0aW9uXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc0Z1bmN0aW9uID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzRnVuY3Rpb24sIHRydWUpLnRvLmJlLmEoJ2Z1bmN0aW9uJyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RGdW5jdGlvbih2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBpcyBfbm90XyBhIGZ1bmN0aW9uLlxuICAgKlxuICAgKiAgICAgdmFyIHNlcnZlVGVhID0gWyAnaGVhdCcsICdwb3VyJywgJ3NpcCcgXTtcbiAgICogICAgIGFzc2VydC5pc05vdEZ1bmN0aW9uKHNlcnZlVGVhLCAnZ3JlYXQsIHdlIGhhdmUgbGlzdGVkIHRoZSBzdGVwcycpO1xuICAgKlxuICAgKiBAbmFtZSBpc05vdEZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05vdEZ1bmN0aW9uID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzTm90RnVuY3Rpb24sIHRydWUpLnRvLm5vdC5iZS5hKCdmdW5jdGlvbicpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzT2JqZWN0KHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGFuIG9iamVjdCBvZiB0eXBlICdPYmplY3QnIChhcyByZXZlYWxlZCBieSBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZ2ApLlxuICAgKiBfVGhlIGFzc2VydGlvbiBkb2VzIG5vdCBtYXRjaCBzdWJjbGFzc2VkIG9iamVjdHMuX1xuICAgKlxuICAgKiAgICAgdmFyIHNlbGVjdGlvbiA9IHsgbmFtZTogJ0NoYWknLCBzZXJ2ZTogJ3dpdGggc3BpY2VzJyB9O1xuICAgKiAgICAgYXNzZXJ0LmlzT2JqZWN0KHNlbGVjdGlvbiwgJ3RlYSBzZWxlY3Rpb24gaXMgYW4gb2JqZWN0Jyk7XG4gICAqXG4gICAqIEBuYW1lIGlzT2JqZWN0XG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc09iamVjdCA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc09iamVjdCwgdHJ1ZSkudG8uYmUuYSgnb2JqZWN0Jyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RPYmplY3QodmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgX25vdF8gYW4gb2JqZWN0IG9mIHR5cGUgJ09iamVjdCcgKGFzIHJldmVhbGVkIGJ5IGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nYCkuXG4gICAqXG4gICAqICAgICB2YXIgc2VsZWN0aW9uID0gJ2NoYWknXG4gICAqICAgICBhc3NlcnQuaXNOb3RPYmplY3Qoc2VsZWN0aW9uLCAndGVhIHNlbGVjdGlvbiBpcyBub3QgYW4gb2JqZWN0Jyk7XG4gICAqICAgICBhc3NlcnQuaXNOb3RPYmplY3QobnVsbCwgJ251bGwgaXMgbm90IGFuIG9iamVjdCcpO1xuICAgKlxuICAgKiBAbmFtZSBpc05vdE9iamVjdFxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOb3RPYmplY3QgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3RPYmplY3QsIHRydWUpLnRvLm5vdC5iZS5hKCdvYmplY3QnKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc0FycmF5KHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGFuIGFycmF5LlxuICAgKlxuICAgKiAgICAgdmFyIG1lbnUgPSBbICdncmVlbicsICdjaGFpJywgJ29vbG9uZycgXTtcbiAgICogICAgIGFzc2VydC5pc0FycmF5KG1lbnUsICd3aGF0IGtpbmQgb2YgdGVhIGRvIHdlIHdhbnQ/Jyk7XG4gICAqXG4gICAqIEBuYW1lIGlzQXJyYXlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzQXJyYXkgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNBcnJheSwgdHJ1ZSkudG8uYmUuYW4oJ2FycmF5Jyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RBcnJheSh2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBpcyBfbm90XyBhbiBhcnJheS5cbiAgICpcbiAgICogICAgIHZhciBtZW51ID0gJ2dyZWVufGNoYWl8b29sb25nJztcbiAgICogICAgIGFzc2VydC5pc05vdEFycmF5KG1lbnUsICd3aGF0IGtpbmQgb2YgdGVhIGRvIHdlIHdhbnQ/Jyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTm90QXJyYXlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzTm90QXJyYXkgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3RBcnJheSwgdHJ1ZSkudG8ubm90LmJlLmFuKCdhcnJheScpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzU3RyaW5nKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGEgc3RyaW5nLlxuICAgKlxuICAgKiAgICAgdmFyIHRlYU9yZGVyID0gJ2NoYWknO1xuICAgKiAgICAgYXNzZXJ0LmlzU3RyaW5nKHRlYU9yZGVyLCAnb3JkZXIgcGxhY2VkJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzU3RyaW5nXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc1N0cmluZyA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc1N0cmluZywgdHJ1ZSkudG8uYmUuYSgnc3RyaW5nJyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RTdHJpbmcodmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgaXMgX25vdF8gYSBzdHJpbmcuXG4gICAqXG4gICAqICAgICB2YXIgdGVhT3JkZXIgPSA0O1xuICAgKiAgICAgYXNzZXJ0LmlzTm90U3RyaW5nKHRlYU9yZGVyLCAnb3JkZXIgcGxhY2VkJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTm90U3RyaW5nXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05vdFN0cmluZyA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc05vdFN0cmluZywgdHJ1ZSkudG8ubm90LmJlLmEoJ3N0cmluZycpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzTnVtYmVyKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGEgbnVtYmVyLlxuICAgKlxuICAgKiAgICAgdmFyIGN1cHMgPSAyO1xuICAgKiAgICAgYXNzZXJ0LmlzTnVtYmVyKGN1cHMsICdob3cgbWFueSBjdXBzJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzTnVtYmVyXG4gICAqIEBwYXJhbSB7TnVtYmVyfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOdW1iZXIgPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOdW1iZXIsIHRydWUpLnRvLmJlLmEoJ251bWJlcicpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzTm90TnVtYmVyKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIF9ub3RfIGEgbnVtYmVyLlxuICAgKlxuICAgKiAgICAgdmFyIGN1cHMgPSAnMiBjdXBzIHBsZWFzZSc7XG4gICAqICAgICBhc3NlcnQuaXNOb3ROdW1iZXIoY3VwcywgJ2hvdyBtYW55IGN1cHMnKTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3ROdW1iZXJcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzTm90TnVtYmVyID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzTm90TnVtYmVyLCB0cnVlKS50by5ub3QuYmUuYSgnbnVtYmVyJyk7XG4gIH07XG5cbiAgIC8qKlxuICAgKiAjIyMgLmlzRmluaXRlKHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGEgZmluaXRlIG51bWJlci4gVW5saWtlIGAuaXNOdW1iZXJgLCB0aGlzIHdpbGwgZmFpbCBmb3IgYE5hTmAgYW5kIGBJbmZpbml0eWAuXG4gICAqXG4gICAqICAgICB2YXIgY3VwcyA9IDI7XG4gICAqICAgICBhc3NlcnQuaXNGaW5pdGUoY3VwcywgJ2hvdyBtYW55IGN1cHMnKTtcbiAgICpcbiAgICogICAgIGFzc2VydC5pc0Zpbml0ZShOYU4pOyAvLyB0aHJvd3NcbiAgICpcbiAgICogQG5hbWUgaXNGaW5pdGVcbiAgICogQHBhcmFtIHtOdW1iZXJ9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc0Zpbml0ZSA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0Zpbml0ZSwgdHJ1ZSkudG8uYmUuZmluaXRlO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzQm9vbGVhbih2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBpcyBhIGJvb2xlYW4uXG4gICAqXG4gICAqICAgICB2YXIgdGVhUmVhZHkgPSB0cnVlXG4gICAqICAgICAgICwgdGVhU2VydmVkID0gZmFsc2U7XG4gICAqXG4gICAqICAgICBhc3NlcnQuaXNCb29sZWFuKHRlYVJlYWR5LCAnaXMgdGhlIHRlYSByZWFkeScpO1xuICAgKiAgICAgYXNzZXJ0LmlzQm9vbGVhbih0ZWFTZXJ2ZWQsICdoYXMgdGVhIGJlZW4gc2VydmVkJyk7XG4gICAqXG4gICAqIEBuYW1lIGlzQm9vbGVhblxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNCb29sZWFuID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzQm9vbGVhbiwgdHJ1ZSkudG8uYmUuYSgnYm9vbGVhbicpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzTm90Qm9vbGVhbih2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBpcyBfbm90XyBhIGJvb2xlYW4uXG4gICAqXG4gICAqICAgICB2YXIgdGVhUmVhZHkgPSAneWVwJ1xuICAgKiAgICAgICAsIHRlYVNlcnZlZCA9ICdub3BlJztcbiAgICpcbiAgICogICAgIGFzc2VydC5pc05vdEJvb2xlYW4odGVhUmVhZHksICdpcyB0aGUgdGVhIHJlYWR5Jyk7XG4gICAqICAgICBhc3NlcnQuaXNOb3RCb29sZWFuKHRlYVNlcnZlZCwgJ2hhcyB0ZWEgYmVlbiBzZXJ2ZWQnKTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3RCb29sZWFuXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pc05vdEJvb2xlYW4gPSBmdW5jdGlvbiAodmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaXNOb3RCb29sZWFuLCB0cnVlKS50by5ub3QuYmUuYSgnYm9vbGVhbicpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLnR5cGVPZih2YWx1ZSwgbmFtZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCdzIHR5cGUgaXMgYG5hbWVgLCBhcyBkZXRlcm1pbmVkIGJ5XG4gICAqIGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nYC5cbiAgICpcbiAgICogICAgIGFzc2VydC50eXBlT2YoeyB0ZWE6ICdjaGFpJyB9LCAnb2JqZWN0JywgJ3dlIGhhdmUgYW4gb2JqZWN0Jyk7XG4gICAqICAgICBhc3NlcnQudHlwZU9mKFsnY2hhaScsICdqYXNtaW5lJ10sICdhcnJheScsICd3ZSBoYXZlIGFuIGFycmF5Jyk7XG4gICAqICAgICBhc3NlcnQudHlwZU9mKCd0ZWEnLCAnc3RyaW5nJywgJ3dlIGhhdmUgYSBzdHJpbmcnKTtcbiAgICogICAgIGFzc2VydC50eXBlT2YoL3RlYS8sICdyZWdleHAnLCAnd2UgaGF2ZSBhIHJlZ3VsYXIgZXhwcmVzc2lvbicpO1xuICAgKiAgICAgYXNzZXJ0LnR5cGVPZihudWxsLCAnbnVsbCcsICd3ZSBoYXZlIGEgbnVsbCcpO1xuICAgKiAgICAgYXNzZXJ0LnR5cGVPZih1bmRlZmluZWQsICd1bmRlZmluZWQnLCAnd2UgaGF2ZSBhbiB1bmRlZmluZWQnKTtcbiAgICpcbiAgICogQG5hbWUgdHlwZU9mXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC50eXBlT2YgPSBmdW5jdGlvbiAodmFsLCB0eXBlLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQudHlwZU9mLCB0cnVlKS50by5iZS5hKHR5cGUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdFR5cGVPZih2YWx1ZSwgbmFtZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCdzIHR5cGUgaXMgX25vdF8gYG5hbWVgLCBhcyBkZXRlcm1pbmVkIGJ5XG4gICAqIGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nYC5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RUeXBlT2YoJ3RlYScsICdudW1iZXInLCAnc3RyaW5ncyBhcmUgbm90IG51bWJlcnMnKTtcbiAgICpcbiAgICogQG5hbWUgbm90VHlwZU9mXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSB0eXBlb2YgbmFtZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90VHlwZU9mID0gZnVuY3Rpb24gKHZhbCwgdHlwZSwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0Lm5vdFR5cGVPZiwgdHJ1ZSkudG8ubm90LmJlLmEodHlwZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaW5zdGFuY2VPZihvYmplY3QsIGNvbnN0cnVjdG9yLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgdmFsdWVgIGlzIGFuIGluc3RhbmNlIG9mIGBjb25zdHJ1Y3RvcmAuXG4gICAqXG4gICAqICAgICB2YXIgVGVhID0gZnVuY3Rpb24gKG5hbWUpIHsgdGhpcy5uYW1lID0gbmFtZTsgfVxuICAgKiAgICAgICAsIGNoYWkgPSBuZXcgVGVhKCdjaGFpJyk7XG4gICAqXG4gICAqICAgICBhc3NlcnQuaW5zdGFuY2VPZihjaGFpLCBUZWEsICdjaGFpIGlzIGFuIGluc3RhbmNlIG9mIHRlYScpO1xuICAgKlxuICAgKiBAbmFtZSBpbnN0YW5jZU9mXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtDb25zdHJ1Y3Rvcn0gY29uc3RydWN0b3JcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lmluc3RhbmNlT2YgPSBmdW5jdGlvbiAodmFsLCB0eXBlLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnLCBhc3NlcnQuaW5zdGFuY2VPZiwgdHJ1ZSkudG8uYmUuaW5zdGFuY2VPZih0eXBlKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3RJbnN0YW5jZU9mKG9iamVjdCwgY29uc3RydWN0b3IsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyBgdmFsdWVgIGlzIG5vdCBhbiBpbnN0YW5jZSBvZiBgY29uc3RydWN0b3JgLlxuICAgKlxuICAgKiAgICAgdmFyIFRlYSA9IGZ1bmN0aW9uIChuYW1lKSB7IHRoaXMubmFtZSA9IG5hbWU7IH1cbiAgICogICAgICAgLCBjaGFpID0gbmV3IFN0cmluZygnY2hhaScpO1xuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdEluc3RhbmNlT2YoY2hhaSwgVGVhLCAnY2hhaSBpcyBub3QgYW4gaW5zdGFuY2Ugb2YgdGVhJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdEluc3RhbmNlT2ZcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge0NvbnN0cnVjdG9yfSBjb25zdHJ1Y3RvclxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90SW5zdGFuY2VPZiA9IGZ1bmN0aW9uICh2YWwsIHR5cGUsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5ub3RJbnN0YW5jZU9mLCB0cnVlKVxuICAgICAgLnRvLm5vdC5iZS5pbnN0YW5jZU9mKHR5cGUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmluY2x1ZGUoaGF5c3RhY2ssIG5lZWRsZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYGhheXN0YWNrYCBpbmNsdWRlcyBgbmVlZGxlYC4gQ2FuIGJlIHVzZWQgdG8gYXNzZXJ0IHRoZVxuICAgKiBpbmNsdXNpb24gb2YgYSB2YWx1ZSBpbiBhbiBhcnJheSwgYSBzdWJzdHJpbmcgaW4gYSBzdHJpbmcsIG9yIGEgc3Vic2V0IG9mXG4gICAqIHByb3BlcnRpZXMgaW4gYW4gb2JqZWN0LlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmluY2x1ZGUoWzEsMiwzXSwgMiwgJ2FycmF5IGNvbnRhaW5zIHZhbHVlJyk7XG4gICAqICAgICBhc3NlcnQuaW5jbHVkZSgnZm9vYmFyJywgJ2ZvbycsICdzdHJpbmcgY29udGFpbnMgc3Vic3RyaW5nJyk7XG4gICAqICAgICBhc3NlcnQuaW5jbHVkZSh7IGZvbzogJ2JhcicsIGhlbGxvOiAndW5pdmVyc2UnIH0sIHsgZm9vOiAnYmFyJyB9LCAnb2JqZWN0IGNvbnRhaW5zIHByb3BlcnR5Jyk7XG4gICAqXG4gICAqIFN0cmljdCBlcXVhbGl0eSAoPT09KSBpcyB1c2VkLiBXaGVuIGFzc2VydGluZyB0aGUgaW5jbHVzaW9uIG9mIGEgdmFsdWUgaW5cbiAgICogYW4gYXJyYXksIHRoZSBhcnJheSBpcyBzZWFyY2hlZCBmb3IgYW4gZWxlbWVudCB0aGF0J3Mgc3RyaWN0bHkgZXF1YWwgdG8gdGhlXG4gICAqIGdpdmVuIHZhbHVlLiBXaGVuIGFzc2VydGluZyBhIHN1YnNldCBvZiBwcm9wZXJ0aWVzIGluIGFuIG9iamVjdCwgdGhlIG9iamVjdFxuICAgKiBpcyBzZWFyY2hlZCBmb3IgdGhlIGdpdmVuIHByb3BlcnR5IGtleXMsIGNoZWNraW5nIHRoYXQgZWFjaCBvbmUgaXMgcHJlc2VudFxuICAgKiBhbmQgc3RyaWN0bHkgZXF1YWwgdG8gdGhlIGdpdmVuIHByb3BlcnR5IHZhbHVlLiBGb3IgaW5zdGFuY2U6XG4gICAqXG4gICAqICAgICB2YXIgb2JqMSA9IHthOiAxfVxuICAgKiAgICAgICAsIG9iajIgPSB7YjogMn07XG4gICAqICAgICBhc3NlcnQuaW5jbHVkZShbb2JqMSwgb2JqMl0sIG9iajEpO1xuICAgKiAgICAgYXNzZXJ0LmluY2x1ZGUoe2Zvbzogb2JqMSwgYmFyOiBvYmoyfSwge2Zvbzogb2JqMX0pO1xuICAgKiAgICAgYXNzZXJ0LmluY2x1ZGUoe2Zvbzogb2JqMSwgYmFyOiBvYmoyfSwge2Zvbzogb2JqMSwgYmFyOiBvYmoyfSk7XG4gICAqXG4gICAqIEBuYW1lIGluY2x1ZGVcbiAgICogQHBhcmFtIHtBcnJheXxTdHJpbmd9IGhheXN0YWNrXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaW5jbHVkZSA9IGZ1bmN0aW9uIChleHAsIGluYywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihleHAsIG1zZywgYXNzZXJ0LmluY2x1ZGUsIHRydWUpLmluY2x1ZGUoaW5jKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3RJbmNsdWRlKGhheXN0YWNrLCBuZWVkbGUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBoYXlzdGFja2AgZG9lcyBub3QgaW5jbHVkZSBgbmVlZGxlYC4gQ2FuIGJlIHVzZWQgdG8gYXNzZXJ0XG4gICAqIHRoZSBhYnNlbmNlIG9mIGEgdmFsdWUgaW4gYW4gYXJyYXksIGEgc3Vic3RyaW5nIGluIGEgc3RyaW5nLCBvciBhIHN1YnNldCBvZlxuICAgKiBwcm9wZXJ0aWVzIGluIGFuIG9iamVjdC5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RJbmNsdWRlKFsxLDIsM10sIDQsIFwiYXJyYXkgZG9lc24ndCBjb250YWluIHZhbHVlXCIpO1xuICAgKiAgICAgYXNzZXJ0Lm5vdEluY2x1ZGUoJ2Zvb2JhcicsICdiYXonLCBcInN0cmluZyBkb2Vzbid0IGNvbnRhaW4gc3Vic3RyaW5nXCIpO1xuICAgKiAgICAgYXNzZXJ0Lm5vdEluY2x1ZGUoeyBmb286ICdiYXInLCBoZWxsbzogJ3VuaXZlcnNlJyB9LCB7IGZvbzogJ2JheicgfSwgJ29iamVjdCBkb2Vzbid0IGNvbnRhaW4gcHJvcGVydHknKTtcbiAgICpcbiAgICogU3RyaWN0IGVxdWFsaXR5ICg9PT0pIGlzIHVzZWQuIFdoZW4gYXNzZXJ0aW5nIHRoZSBhYnNlbmNlIG9mIGEgdmFsdWUgaW4gYW5cbiAgICogYXJyYXksIHRoZSBhcnJheSBpcyBzZWFyY2hlZCB0byBjb25maXJtIHRoZSBhYnNlbmNlIG9mIGFuIGVsZW1lbnQgdGhhdCdzXG4gICAqIHN0cmljdGx5IGVxdWFsIHRvIHRoZSBnaXZlbiB2YWx1ZS4gV2hlbiBhc3NlcnRpbmcgYSBzdWJzZXQgb2YgcHJvcGVydGllcyBpblxuICAgKiBhbiBvYmplY3QsIHRoZSBvYmplY3QgaXMgc2VhcmNoZWQgdG8gY29uZmlybSB0aGF0IGF0IGxlYXN0IG9uZSBvZiB0aGUgZ2l2ZW5cbiAgICogcHJvcGVydHkga2V5cyBpcyBlaXRoZXIgbm90IHByZXNlbnQgb3Igbm90IHN0cmljdGx5IGVxdWFsIHRvIHRoZSBnaXZlblxuICAgKiBwcm9wZXJ0eSB2YWx1ZS4gRm9yIGluc3RhbmNlOlxuICAgKlxuICAgKiAgICAgdmFyIG9iajEgPSB7YTogMX1cbiAgICogICAgICAgLCBvYmoyID0ge2I6IDJ9O1xuICAgKiAgICAgYXNzZXJ0Lm5vdEluY2x1ZGUoW29iajEsIG9iajJdLCB7YTogMX0pO1xuICAgKiAgICAgYXNzZXJ0Lm5vdEluY2x1ZGUoe2Zvbzogb2JqMSwgYmFyOiBvYmoyfSwge2Zvbzoge2E6IDF9fSk7XG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZSh7Zm9vOiBvYmoxLCBiYXI6IG9iajJ9LCB7Zm9vOiBvYmoxLCBiYXI6IHtiOiAyfX0pO1xuICAgKlxuICAgKiBAbmFtZSBub3RJbmNsdWRlXG4gICAqIEBwYXJhbSB7QXJyYXl8U3RyaW5nfSBoYXlzdGFja1xuICAgKiBAcGFyYW0ge01peGVkfSBuZWVkbGVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdEluY2x1ZGUgPSBmdW5jdGlvbiAoZXhwLCBpbmMsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5ub3RJbmNsdWRlLCB0cnVlKS5ub3QuaW5jbHVkZShpbmMpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmRlZXBJbmNsdWRlKGhheXN0YWNrLCBuZWVkbGUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBoYXlzdGFja2AgaW5jbHVkZXMgYG5lZWRsZWAuIENhbiBiZSB1c2VkIHRvIGFzc2VydCB0aGVcbiAgICogaW5jbHVzaW9uIG9mIGEgdmFsdWUgaW4gYW4gYXJyYXkgb3IgYSBzdWJzZXQgb2YgcHJvcGVydGllcyBpbiBhbiBvYmplY3QuXG4gICAqIERlZXAgZXF1YWxpdHkgaXMgdXNlZC5cbiAgICpcbiAgICogICAgIHZhciBvYmoxID0ge2E6IDF9XG4gICAqICAgICAgICwgb2JqMiA9IHtiOiAyfTtcbiAgICogICAgIGFzc2VydC5kZWVwSW5jbHVkZShbb2JqMSwgb2JqMl0sIHthOiAxfSk7XG4gICAqICAgICBhc3NlcnQuZGVlcEluY2x1ZGUoe2Zvbzogb2JqMSwgYmFyOiBvYmoyfSwge2Zvbzoge2E6IDF9fSk7XG4gICAqICAgICBhc3NlcnQuZGVlcEluY2x1ZGUoe2Zvbzogb2JqMSwgYmFyOiBvYmoyfSwge2Zvbzoge2E6IDF9LCBiYXI6IHtiOiAyfX0pO1xuICAgKlxuICAgKiBAbmFtZSBkZWVwSW5jbHVkZVxuICAgKiBAcGFyYW0ge0FycmF5fFN0cmluZ30gaGF5c3RhY2tcbiAgICogQHBhcmFtIHtNaXhlZH0gbmVlZGxlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kZWVwSW5jbHVkZSA9IGZ1bmN0aW9uIChleHAsIGluYywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihleHAsIG1zZywgYXNzZXJ0LmRlZXBJbmNsdWRlLCB0cnVlKS5kZWVwLmluY2x1ZGUoaW5jKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3REZWVwSW5jbHVkZShoYXlzdGFjaywgbmVlZGxlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgaGF5c3RhY2tgIGRvZXMgbm90IGluY2x1ZGUgYG5lZWRsZWAuIENhbiBiZSB1c2VkIHRvIGFzc2VydFxuICAgKiB0aGUgYWJzZW5jZSBvZiBhIHZhbHVlIGluIGFuIGFycmF5IG9yIGEgc3Vic2V0IG9mIHByb3BlcnRpZXMgaW4gYW4gb2JqZWN0LlxuICAgKiBEZWVwIGVxdWFsaXR5IGlzIHVzZWQuXG4gICAqXG4gICAqICAgICB2YXIgb2JqMSA9IHthOiAxfVxuICAgKiAgICAgICAsIG9iajIgPSB7YjogMn07XG4gICAqICAgICBhc3NlcnQubm90RGVlcEluY2x1ZGUoW29iajEsIG9iajJdLCB7YTogOX0pO1xuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBJbmNsdWRlKHtmb286IG9iajEsIGJhcjogb2JqMn0sIHtmb286IHthOiA5fX0pO1xuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBJbmNsdWRlKHtmb286IG9iajEsIGJhcjogb2JqMn0sIHtmb286IHthOiAxfSwgYmFyOiB7YjogOX19KTtcbiAgICpcbiAgICogQG5hbWUgbm90RGVlcEluY2x1ZGVcbiAgICogQHBhcmFtIHtBcnJheXxTdHJpbmd9IGhheXN0YWNrXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90RGVlcEluY2x1ZGUgPSBmdW5jdGlvbiAoZXhwLCBpbmMsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5ub3REZWVwSW5jbHVkZSwgdHJ1ZSkubm90LmRlZXAuaW5jbHVkZShpbmMpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5lc3RlZEluY2x1ZGUoaGF5c3RhY2ssIG5lZWRsZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgJ2hheXN0YWNrJyBpbmNsdWRlcyAnbmVlZGxlJy5cbiAgICogQ2FuIGJlIHVzZWQgdG8gYXNzZXJ0IHRoZSBpbmNsdXNpb24gb2YgYSBzdWJzZXQgb2YgcHJvcGVydGllcyBpbiBhblxuICAgKiBvYmplY3QuXG4gICAqIEVuYWJsZXMgdGhlIHVzZSBvZiBkb3QtIGFuZCBicmFja2V0LW5vdGF0aW9uIGZvciByZWZlcmVuY2luZyBuZXN0ZWRcbiAgICogcHJvcGVydGllcy5cbiAgICogJ1tdJyBhbmQgJy4nIGluIHByb3BlcnR5IG5hbWVzIGNhbiBiZSBlc2NhcGVkIHVzaW5nIGRvdWJsZSBiYWNrc2xhc2hlcy5cbiAgICpcbiAgICogICAgIGFzc2VydC5uZXN0ZWRJbmNsdWRlKHsnLmEnOiB7J2InOiAneCd9fSwgeydcXFxcLmEuW2JdJzogJ3gnfSk7XG4gICAqICAgICBhc3NlcnQubmVzdGVkSW5jbHVkZSh7J2EnOiB7J1tiXSc6ICd4J319LCB7J2EuXFxcXFtiXFxcXF0nOiAneCd9KTtcbiAgICpcbiAgICogQG5hbWUgbmVzdGVkSW5jbHVkZVxuICAgKiBAcGFyYW0ge09iamVjdH0gaGF5c3RhY2tcbiAgICogQHBhcmFtIHtPYmplY3R9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubmVzdGVkSW5jbHVkZSA9IGZ1bmN0aW9uIChleHAsIGluYywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihleHAsIG1zZywgYXNzZXJ0Lm5lc3RlZEluY2x1ZGUsIHRydWUpLm5lc3RlZC5pbmNsdWRlKGluYyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90TmVzdGVkSW5jbHVkZShoYXlzdGFjaywgbmVlZGxlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCAnaGF5c3RhY2snIGRvZXMgbm90IGluY2x1ZGUgJ25lZWRsZScuXG4gICAqIENhbiBiZSB1c2VkIHRvIGFzc2VydCB0aGUgYWJzZW5jZSBvZiBhIHN1YnNldCBvZiBwcm9wZXJ0aWVzIGluIGFuXG4gICAqIG9iamVjdC5cbiAgICogRW5hYmxlcyB0aGUgdXNlIG9mIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gZm9yIHJlZmVyZW5jaW5nIG5lc3RlZFxuICAgKiBwcm9wZXJ0aWVzLlxuICAgKiAnW10nIGFuZCAnLicgaW4gcHJvcGVydHkgbmFtZXMgY2FuIGJlIGVzY2FwZWQgdXNpbmcgZG91YmxlIGJhY2tzbGFzaGVzLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdE5lc3RlZEluY2x1ZGUoeycuYSc6IHsnYic6ICd4J319LCB7J1xcXFwuYS5iJzogJ3knfSk7XG4gICAqICAgICBhc3NlcnQubm90TmVzdGVkSW5jbHVkZSh7J2EnOiB7J1tiXSc6ICd4J319LCB7J2EuXFxcXFtiXFxcXF0nOiAneSd9KTtcbiAgICpcbiAgICogQG5hbWUgbm90TmVzdGVkSW5jbHVkZVxuICAgKiBAcGFyYW0ge09iamVjdH0gaGF5c3RhY2tcbiAgICogQHBhcmFtIHtPYmplY3R9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90TmVzdGVkSW5jbHVkZSA9IGZ1bmN0aW9uIChleHAsIGluYywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihleHAsIG1zZywgYXNzZXJ0Lm5vdE5lc3RlZEluY2x1ZGUsIHRydWUpXG4gICAgICAubm90Lm5lc3RlZC5pbmNsdWRlKGluYyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuZGVlcE5lc3RlZEluY2x1ZGUoaGF5c3RhY2ssIG5lZWRsZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgJ2hheXN0YWNrJyBpbmNsdWRlcyAnbmVlZGxlJy5cbiAgICogQ2FuIGJlIHVzZWQgdG8gYXNzZXJ0IHRoZSBpbmNsdXNpb24gb2YgYSBzdWJzZXQgb2YgcHJvcGVydGllcyBpbiBhblxuICAgKiBvYmplY3Qgd2hpbGUgY2hlY2tpbmcgZm9yIGRlZXAgZXF1YWxpdHkuXG4gICAqIEVuYWJsZXMgdGhlIHVzZSBvZiBkb3QtIGFuZCBicmFja2V0LW5vdGF0aW9uIGZvciByZWZlcmVuY2luZyBuZXN0ZWRcbiAgICogcHJvcGVydGllcy5cbiAgICogJ1tdJyBhbmQgJy4nIGluIHByb3BlcnR5IG5hbWVzIGNhbiBiZSBlc2NhcGVkIHVzaW5nIGRvdWJsZSBiYWNrc2xhc2hlcy5cbiAgICpcbiAgICogICAgIGFzc2VydC5kZWVwTmVzdGVkSW5jbHVkZSh7YToge2I6IFt7eDogMX1dfX0sIHsnYS5iWzBdJzoge3g6IDF9fSk7XG4gICAqICAgICBhc3NlcnQuZGVlcE5lc3RlZEluY2x1ZGUoeycuYSc6IHsnW2JdJzoge3g6IDF9fX0sIHsnXFxcXC5hLlxcXFxbYlxcXFxdJzoge3g6IDF9fSk7XG4gICAqXG4gICAqIEBuYW1lIGRlZXBOZXN0ZWRJbmNsdWRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBoYXlzdGFja1xuICAgKiBAcGFyYW0ge09iamVjdH0gbmVlZGxlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kZWVwTmVzdGVkSW5jbHVkZSA9IGZ1bmN0aW9uKGV4cCwgaW5jLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGV4cCwgbXNnLCBhc3NlcnQuZGVlcE5lc3RlZEluY2x1ZGUsIHRydWUpXG4gICAgICAuZGVlcC5uZXN0ZWQuaW5jbHVkZShpbmMpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdERlZXBOZXN0ZWRJbmNsdWRlKGhheXN0YWNrLCBuZWVkbGUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0ICdoYXlzdGFjaycgZG9lcyBub3QgaW5jbHVkZSAnbmVlZGxlJy5cbiAgICogQ2FuIGJlIHVzZWQgdG8gYXNzZXJ0IHRoZSBhYnNlbmNlIG9mIGEgc3Vic2V0IG9mIHByb3BlcnRpZXMgaW4gYW5cbiAgICogb2JqZWN0IHdoaWxlIGNoZWNraW5nIGZvciBkZWVwIGVxdWFsaXR5LlxuICAgKiBFbmFibGVzIHRoZSB1c2Ugb2YgZG90LSBhbmQgYnJhY2tldC1ub3RhdGlvbiBmb3IgcmVmZXJlbmNpbmcgbmVzdGVkXG4gICAqIHByb3BlcnRpZXMuXG4gICAqICdbXScgYW5kICcuJyBpbiBwcm9wZXJ0eSBuYW1lcyBjYW4gYmUgZXNjYXBlZCB1c2luZyBkb3VibGUgYmFja3NsYXNoZXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90RGVlcE5lc3RlZEluY2x1ZGUoe2E6IHtiOiBbe3g6IDF9XX19LCB7J2EuYlswXSc6IHt5OiAxfX0pXG4gICAqICAgICBhc3NlcnQubm90RGVlcE5lc3RlZEluY2x1ZGUoeycuYSc6IHsnW2JdJzoge3g6IDF9fX0sIHsnXFxcXC5hLlxcXFxbYlxcXFxdJzoge3k6IDJ9fSk7XG4gICAqXG4gICAqIEBuYW1lIG5vdERlZXBOZXN0ZWRJbmNsdWRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBoYXlzdGFja1xuICAgKiBAcGFyYW0ge09iamVjdH0gbmVlZGxlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3REZWVwTmVzdGVkSW5jbHVkZSA9IGZ1bmN0aW9uKGV4cCwgaW5jLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGV4cCwgbXNnLCBhc3NlcnQubm90RGVlcE5lc3RlZEluY2x1ZGUsIHRydWUpXG4gICAgICAubm90LmRlZXAubmVzdGVkLmluY2x1ZGUoaW5jKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5vd25JbmNsdWRlKGhheXN0YWNrLCBuZWVkbGUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0ICdoYXlzdGFjaycgaW5jbHVkZXMgJ25lZWRsZScuXG4gICAqIENhbiBiZSB1c2VkIHRvIGFzc2VydCB0aGUgaW5jbHVzaW9uIG9mIGEgc3Vic2V0IG9mIHByb3BlcnRpZXMgaW4gYW5cbiAgICogb2JqZWN0IHdoaWxlIGlnbm9yaW5nIGluaGVyaXRlZCBwcm9wZXJ0aWVzLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm93bkluY2x1ZGUoeyBhOiAxIH0sIHsgYTogMSB9KTtcbiAgICpcbiAgICogQG5hbWUgb3duSW5jbHVkZVxuICAgKiBAcGFyYW0ge09iamVjdH0gaGF5c3RhY2tcbiAgICogQHBhcmFtIHtPYmplY3R9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQub3duSW5jbHVkZSA9IGZ1bmN0aW9uKGV4cCwgaW5jLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGV4cCwgbXNnLCBhc3NlcnQub3duSW5jbHVkZSwgdHJ1ZSkub3duLmluY2x1ZGUoaW5jKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3RPd25JbmNsdWRlKGhheXN0YWNrLCBuZWVkbGUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0ICdoYXlzdGFjaycgaW5jbHVkZXMgJ25lZWRsZScuXG4gICAqIENhbiBiZSB1c2VkIHRvIGFzc2VydCB0aGUgYWJzZW5jZSBvZiBhIHN1YnNldCBvZiBwcm9wZXJ0aWVzIGluIGFuXG4gICAqIG9iamVjdCB3aGlsZSBpZ25vcmluZyBpbmhlcml0ZWQgcHJvcGVydGllcy5cbiAgICpcbiAgICogICAgIE9iamVjdC5wcm90b3R5cGUuYiA9IDI7XG4gICAqXG4gICAqICAgICBhc3NlcnQubm90T3duSW5jbHVkZSh7IGE6IDEgfSwgeyBiOiAyIH0pO1xuICAgKlxuICAgKiBAbmFtZSBub3RPd25JbmNsdWRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBoYXlzdGFja1xuICAgKiBAcGFyYW0ge09iamVjdH0gbmVlZGxlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3RPd25JbmNsdWRlID0gZnVuY3Rpb24oZXhwLCBpbmMsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5ub3RPd25JbmNsdWRlLCB0cnVlKS5ub3Qub3duLmluY2x1ZGUoaW5jKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5kZWVwT3duSW5jbHVkZShoYXlzdGFjaywgbmVlZGxlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCAnaGF5c3RhY2snIGluY2x1ZGVzICduZWVkbGUnLlxuICAgKiBDYW4gYmUgdXNlZCB0byBhc3NlcnQgdGhlIGluY2x1c2lvbiBvZiBhIHN1YnNldCBvZiBwcm9wZXJ0aWVzIGluIGFuXG4gICAqIG9iamVjdCB3aGlsZSBpZ25vcmluZyBpbmhlcml0ZWQgcHJvcGVydGllcyBhbmQgY2hlY2tpbmcgZm9yIGRlZXAgZXF1YWxpdHkuXG4gICAqXG4gICAqICAgICAgYXNzZXJ0LmRlZXBPd25JbmNsdWRlKHthOiB7YjogMn19LCB7YToge2I6IDJ9fSk7XG4gICAqXG4gICAqIEBuYW1lIGRlZXBPd25JbmNsdWRlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBoYXlzdGFja1xuICAgKiBAcGFyYW0ge09iamVjdH0gbmVlZGxlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kZWVwT3duSW5jbHVkZSA9IGZ1bmN0aW9uKGV4cCwgaW5jLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGV4cCwgbXNnLCBhc3NlcnQuZGVlcE93bkluY2x1ZGUsIHRydWUpXG4gICAgICAuZGVlcC5vd24uaW5jbHVkZShpbmMpO1xuICB9O1xuXG4gICAvKipcbiAgICogIyMjIC5ub3REZWVwT3duSW5jbHVkZShoYXlzdGFjaywgbmVlZGxlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCAnaGF5c3RhY2snIGluY2x1ZGVzICduZWVkbGUnLlxuICAgKiBDYW4gYmUgdXNlZCB0byBhc3NlcnQgdGhlIGFic2VuY2Ugb2YgYSBzdWJzZXQgb2YgcHJvcGVydGllcyBpbiBhblxuICAgKiBvYmplY3Qgd2hpbGUgaWdub3JpbmcgaW5oZXJpdGVkIHByb3BlcnRpZXMgYW5kIGNoZWNraW5nIGZvciBkZWVwIGVxdWFsaXR5LlxuICAgKlxuICAgKiAgICAgIGFzc2VydC5ub3REZWVwT3duSW5jbHVkZSh7YToge2I6IDJ9fSwge2E6IHtjOiAzfX0pO1xuICAgKlxuICAgKiBAbmFtZSBub3REZWVwT3duSW5jbHVkZVxuICAgKiBAcGFyYW0ge09iamVjdH0gaGF5c3RhY2tcbiAgICogQHBhcmFtIHtPYmplY3R9IG5lZWRsZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90RGVlcE93bkluY2x1ZGUgPSBmdW5jdGlvbihleHAsIGluYywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihleHAsIG1zZywgYXNzZXJ0Lm5vdERlZXBPd25JbmNsdWRlLCB0cnVlKVxuICAgICAgLm5vdC5kZWVwLm93bi5pbmNsdWRlKGluYyk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubWF0Y2godmFsdWUsIHJlZ2V4cCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHZhbHVlYCBtYXRjaGVzIHRoZSByZWd1bGFyIGV4cHJlc3Npb24gYHJlZ2V4cGAuXG4gICAqXG4gICAqICAgICBhc3NlcnQubWF0Y2goJ2Zvb2JhcicsIC9eZm9vLywgJ3JlZ2V4cCBtYXRjaGVzJyk7XG4gICAqXG4gICAqIEBuYW1lIG1hdGNoXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7UmVnRXhwfSByZWdleHBcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm1hdGNoID0gZnVuY3Rpb24gKGV4cCwgcmUsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5tYXRjaCwgdHJ1ZSkudG8ubWF0Y2gocmUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdE1hdGNoKHZhbHVlLCByZWdleHAsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGB2YWx1ZWAgZG9lcyBub3QgbWF0Y2ggdGhlIHJlZ3VsYXIgZXhwcmVzc2lvbiBgcmVnZXhwYC5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RNYXRjaCgnZm9vYmFyJywgL15mb28vLCAncmVnZXhwIGRvZXMgbm90IG1hdGNoJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdE1hdGNoXG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7UmVnRXhwfSByZWdleHBcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdE1hdGNoID0gZnVuY3Rpb24gKGV4cCwgcmUsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5ub3RNYXRjaCwgdHJ1ZSkudG8ubm90Lm1hdGNoKHJlKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5wcm9wZXJ0eShvYmplY3QsIHByb3BlcnR5LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYSBkaXJlY3Qgb3IgaW5oZXJpdGVkIHByb3BlcnR5IG5hbWVkIGJ5XG4gICAqIGBwcm9wZXJ0eWAuXG4gICAqXG4gICAqICAgICBhc3NlcnQucHJvcGVydHkoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH19LCAndGVhJyk7XG4gICAqICAgICBhc3NlcnQucHJvcGVydHkoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH19LCAndG9TdHJpbmcnKTtcbiAgICpcbiAgICogQG5hbWUgcHJvcGVydHlcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnByb3BlcnR5ID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LnByb3BlcnR5LCB0cnVlKS50by5oYXZlLnByb3BlcnR5KHByb3ApO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdFByb3BlcnR5KG9iamVjdCwgcHJvcGVydHksIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGRvZXMgX25vdF8gaGF2ZSBhIGRpcmVjdCBvciBpbmhlcml0ZWQgcHJvcGVydHkgbmFtZWRcbiAgICogYnkgYHByb3BlcnR5YC5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RQcm9wZXJ0eSh7IHRlYTogeyBncmVlbjogJ21hdGNoYScgfX0sICdjb2ZmZWUnKTtcbiAgICpcbiAgICogQG5hbWUgbm90UHJvcGVydHlcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdFByb3BlcnR5ID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5vdFByb3BlcnR5LCB0cnVlKVxuICAgICAgLnRvLm5vdC5oYXZlLnByb3BlcnR5KHByb3ApO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLnByb3BlcnR5VmFsKG9iamVjdCwgcHJvcGVydHksIHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYSBkaXJlY3Qgb3IgaW5oZXJpdGVkIHByb3BlcnR5IG5hbWVkIGJ5XG4gICAqIGBwcm9wZXJ0eWAgd2l0aCBhIHZhbHVlIGdpdmVuIGJ5IGB2YWx1ZWAuIFVzZXMgYSBzdHJpY3QgZXF1YWxpdHkgY2hlY2tcbiAgICogKD09PSkuXG4gICAqXG4gICAqICAgICBhc3NlcnQucHJvcGVydHlWYWwoeyB0ZWE6ICdpcyBnb29kJyB9LCAndGVhJywgJ2lzIGdvb2QnKTtcbiAgICpcbiAgICogQG5hbWUgcHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnByb3BlcnR5VmFsID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgdmFsLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQucHJvcGVydHlWYWwsIHRydWUpXG4gICAgICAudG8uaGF2ZS5wcm9wZXJ0eShwcm9wLCB2YWwpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdFByb3BlcnR5VmFsKG9iamVjdCwgcHJvcGVydHksIHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBkb2VzIF9ub3RfIGhhdmUgYSBkaXJlY3Qgb3IgaW5oZXJpdGVkIHByb3BlcnR5IG5hbWVkXG4gICAqIGJ5IGBwcm9wZXJ0eWAgd2l0aCB2YWx1ZSBnaXZlbiBieSBgdmFsdWVgLiBVc2VzIGEgc3RyaWN0IGVxdWFsaXR5IGNoZWNrXG4gICAqICg9PT0pLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdFByb3BlcnR5VmFsKHsgdGVhOiAnaXMgZ29vZCcgfSwgJ3RlYScsICdpcyBiYWQnKTtcbiAgICogICAgIGFzc2VydC5ub3RQcm9wZXJ0eVZhbCh7IHRlYTogJ2lzIGdvb2QnIH0sICdjb2ZmZWUnLCAnaXMgZ29vZCcpO1xuICAgKlxuICAgKiBAbmFtZSBub3RQcm9wZXJ0eVZhbFxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90UHJvcGVydHlWYWwgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCB2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5ub3RQcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5wcm9wZXJ0eShwcm9wLCB2YWwpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmRlZXBQcm9wZXJ0eVZhbChvYmplY3QsIHByb3BlcnR5LCB2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgZGlyZWN0IG9yIGluaGVyaXRlZCBwcm9wZXJ0eSBuYW1lZCBieVxuICAgKiBgcHJvcGVydHlgIHdpdGggYSB2YWx1ZSBnaXZlbiBieSBgdmFsdWVgLiBVc2VzIGEgZGVlcCBlcXVhbGl0eSBjaGVjay5cbiAgICpcbiAgICogICAgIGFzc2VydC5kZWVwUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH0gfSwgJ3RlYScsIHsgZ3JlZW46ICdtYXRjaGEnIH0pO1xuICAgKlxuICAgKiBAbmFtZSBkZWVwUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRlZXBQcm9wZXJ0eVZhbCA9IGZ1bmN0aW9uIChvYmosIHByb3AsIHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmRlZXBQcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5oYXZlLmRlZXAucHJvcGVydHkocHJvcCwgdmFsKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5ub3REZWVwUHJvcGVydHlWYWwob2JqZWN0LCBwcm9wZXJ0eSwgdmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGRvZXMgX25vdF8gaGF2ZSBhIGRpcmVjdCBvciBpbmhlcml0ZWQgcHJvcGVydHkgbmFtZWRcbiAgICogYnkgYHByb3BlcnR5YCB3aXRoIHZhbHVlIGdpdmVuIGJ5IGB2YWx1ZWAuIFVzZXMgYSBkZWVwIGVxdWFsaXR5IGNoZWNrLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBQcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogJ21hdGNoYScgfSB9LCAndGVhJywgeyBibGFjazogJ21hdGNoYScgfSk7XG4gICAqICAgICBhc3NlcnQubm90RGVlcFByb3BlcnR5VmFsKHsgdGVhOiB7IGdyZWVuOiAnbWF0Y2hhJyB9IH0sICd0ZWEnLCB7IGdyZWVuOiAnb29sb25nJyB9KTtcbiAgICogICAgIGFzc2VydC5ub3REZWVwUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH0gfSwgJ2NvZmZlZScsIHsgZ3JlZW46ICdtYXRjaGEnIH0pO1xuICAgKlxuICAgKiBAbmFtZSBub3REZWVwUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdERlZXBQcm9wZXJ0eVZhbCA9IGZ1bmN0aW9uIChvYmosIHByb3AsIHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5vdERlZXBQcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5kZWVwLnByb3BlcnR5KHByb3AsIHZhbCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAub3duUHJvcGVydHkob2JqZWN0LCBwcm9wZXJ0eSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgZGlyZWN0IHByb3BlcnR5IG5hbWVkIGJ5IGBwcm9wZXJ0eWAuIEluaGVyaXRlZFxuICAgKiBwcm9wZXJ0aWVzIGFyZW4ndCBjaGVja2VkLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm93blByb3BlcnR5KHsgdGVhOiB7IGdyZWVuOiAnbWF0Y2hhJyB9fSwgJ3RlYScpO1xuICAgKlxuICAgKiBAbmFtZSBvd25Qcm9wZXJ0eVxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQub3duUHJvcGVydHkgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQub3duUHJvcGVydHksIHRydWUpXG4gICAgICAudG8uaGF2ZS5vd24ucHJvcGVydHkocHJvcCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90T3duUHJvcGVydHkob2JqZWN0LCBwcm9wZXJ0eSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgZG9lcyBfbm90XyBoYXZlIGEgZGlyZWN0IHByb3BlcnR5IG5hbWVkIGJ5XG4gICAqIGBwcm9wZXJ0eWAuIEluaGVyaXRlZCBwcm9wZXJ0aWVzIGFyZW4ndCBjaGVja2VkLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdE93blByb3BlcnR5KHsgdGVhOiB7IGdyZWVuOiAnbWF0Y2hhJyB9fSwgJ2NvZmZlZScpO1xuICAgKiAgICAgYXNzZXJ0Lm5vdE93blByb3BlcnR5KHt9LCAndG9TdHJpbmcnKTtcbiAgICpcbiAgICogQG5hbWUgbm90T3duUHJvcGVydHlcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdE93blByb3BlcnR5ID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5vdE93blByb3BlcnR5LCB0cnVlKVxuICAgICAgLnRvLm5vdC5oYXZlLm93bi5wcm9wZXJ0eShwcm9wKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5vd25Qcm9wZXJ0eVZhbChvYmplY3QsIHByb3BlcnR5LCB2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgZGlyZWN0IHByb3BlcnR5IG5hbWVkIGJ5IGBwcm9wZXJ0eWAgYW5kIGEgdmFsdWVcbiAgICogZXF1YWwgdG8gdGhlIHByb3ZpZGVkIGB2YWx1ZWAuIFVzZXMgYSBzdHJpY3QgZXF1YWxpdHkgY2hlY2sgKD09PSkuXG4gICAqIEluaGVyaXRlZCBwcm9wZXJ0aWVzIGFyZW4ndCBjaGVja2VkLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm93blByb3BlcnR5VmFsKHsgY29mZmVlOiAnaXMgZ29vZCd9LCAnY29mZmVlJywgJ2lzIGdvb2QnKTtcbiAgICpcbiAgICogQG5hbWUgb3duUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm93blByb3BlcnR5VmFsID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgdmFsdWUsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5vd25Qcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5oYXZlLm93bi5wcm9wZXJ0eShwcm9wLCB2YWx1ZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90T3duUHJvcGVydHlWYWwob2JqZWN0LCBwcm9wZXJ0eSwgdmFsdWUsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGRvZXMgX25vdF8gaGF2ZSBhIGRpcmVjdCBwcm9wZXJ0eSBuYW1lZCBieSBgcHJvcGVydHlgXG4gICAqIHdpdGggYSB2YWx1ZSBlcXVhbCB0byB0aGUgcHJvdmlkZWQgYHZhbHVlYC4gVXNlcyBhIHN0cmljdCBlcXVhbGl0eSBjaGVja1xuICAgKiAoPT09KS4gSW5oZXJpdGVkIHByb3BlcnRpZXMgYXJlbid0IGNoZWNrZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90T3duUHJvcGVydHlWYWwoeyB0ZWE6ICdpcyBiZXR0ZXInfSwgJ3RlYScsICdpcyB3b3JzZScpO1xuICAgKiAgICAgYXNzZXJ0Lm5vdE93blByb3BlcnR5VmFsKHt9LCAndG9TdHJpbmcnLCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nKTtcbiAgICpcbiAgICogQG5hbWUgbm90T3duUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdE93blByb3BlcnR5VmFsID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgdmFsdWUsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5ub3RPd25Qcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5vd24ucHJvcGVydHkocHJvcCwgdmFsdWUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmRlZXBPd25Qcm9wZXJ0eVZhbChvYmplY3QsIHByb3BlcnR5LCB2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgZGlyZWN0IHByb3BlcnR5IG5hbWVkIGJ5IGBwcm9wZXJ0eWAgYW5kIGEgdmFsdWVcbiAgICogZXF1YWwgdG8gdGhlIHByb3ZpZGVkIGB2YWx1ZWAuIFVzZXMgYSBkZWVwIGVxdWFsaXR5IGNoZWNrLiBJbmhlcml0ZWRcbiAgICogcHJvcGVydGllcyBhcmVuJ3QgY2hlY2tlZC5cbiAgICpcbiAgICogICAgIGFzc2VydC5kZWVwT3duUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH0gfSwgJ3RlYScsIHsgZ3JlZW46ICdtYXRjaGEnIH0pO1xuICAgKlxuICAgKiBAbmFtZSBkZWVwT3duUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRlZXBPd25Qcm9wZXJ0eVZhbCA9IGZ1bmN0aW9uIChvYmosIHByb3AsIHZhbHVlLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuZGVlcE93blByb3BlcnR5VmFsLCB0cnVlKVxuICAgICAgLnRvLmhhdmUuZGVlcC5vd24ucHJvcGVydHkocHJvcCwgdmFsdWUpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdERlZXBPd25Qcm9wZXJ0eVZhbChvYmplY3QsIHByb3BlcnR5LCB2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgZG9lcyBfbm90XyBoYXZlIGEgZGlyZWN0IHByb3BlcnR5IG5hbWVkIGJ5IGBwcm9wZXJ0eWBcbiAgICogd2l0aCBhIHZhbHVlIGVxdWFsIHRvIHRoZSBwcm92aWRlZCBgdmFsdWVgLiBVc2VzIGEgZGVlcCBlcXVhbGl0eSBjaGVjay5cbiAgICogSW5oZXJpdGVkIHByb3BlcnRpZXMgYXJlbid0IGNoZWNrZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90RGVlcE93blByb3BlcnR5VmFsKHsgdGVhOiB7IGdyZWVuOiAnbWF0Y2hhJyB9IH0sICd0ZWEnLCB7IGJsYWNrOiAnbWF0Y2hhJyB9KTtcbiAgICogICAgIGFzc2VydC5ub3REZWVwT3duUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH0gfSwgJ3RlYScsIHsgZ3JlZW46ICdvb2xvbmcnIH0pO1xuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBPd25Qcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogJ21hdGNoYScgfSB9LCAnY29mZmVlJywgeyBncmVlbjogJ21hdGNoYScgfSk7XG4gICAqICAgICBhc3NlcnQubm90RGVlcE93blByb3BlcnR5VmFsKHt9LCAndG9TdHJpbmcnLCBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nKTtcbiAgICpcbiAgICogQG5hbWUgbm90RGVlcE93blByb3BlcnR5VmFsXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5XG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3REZWVwT3duUHJvcGVydHlWYWwgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCB2YWx1ZSwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5vdERlZXBPd25Qcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5kZWVwLm93bi5wcm9wZXJ0eShwcm9wLCB2YWx1ZSk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubmVzdGVkUHJvcGVydHkob2JqZWN0LCBwcm9wZXJ0eSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgZGlyZWN0IG9yIGluaGVyaXRlZCBwcm9wZXJ0eSBuYW1lZCBieVxuICAgKiBgcHJvcGVydHlgLCB3aGljaCBjYW4gYmUgYSBzdHJpbmcgdXNpbmcgZG90LSBhbmQgYnJhY2tldC1ub3RhdGlvbiBmb3JcbiAgICogbmVzdGVkIHJlZmVyZW5jZS5cbiAgICpcbiAgICogICAgIGFzc2VydC5uZXN0ZWRQcm9wZXJ0eSh7IHRlYTogeyBncmVlbjogJ21hdGNoYScgfX0sICd0ZWEuZ3JlZW4nKTtcbiAgICpcbiAgICogQG5hbWUgbmVzdGVkUHJvcGVydHlcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5lc3RlZFByb3BlcnR5ID0gZnVuY3Rpb24gKG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5lc3RlZFByb3BlcnR5LCB0cnVlKVxuICAgICAgLnRvLmhhdmUubmVzdGVkLnByb3BlcnR5KHByb3ApO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdE5lc3RlZFByb3BlcnR5KG9iamVjdCwgcHJvcGVydHksIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGRvZXMgX25vdF8gaGF2ZSBhIHByb3BlcnR5IG5hbWVkIGJ5IGBwcm9wZXJ0eWAsIHdoaWNoXG4gICAqIGNhbiBiZSBhIHN0cmluZyB1c2luZyBkb3QtIGFuZCBicmFja2V0LW5vdGF0aW9uIGZvciBuZXN0ZWQgcmVmZXJlbmNlLiBUaGVcbiAgICogcHJvcGVydHkgY2Fubm90IGV4aXN0IG9uIHRoZSBvYmplY3Qgbm9yIGFueXdoZXJlIGluIGl0cyBwcm90b3R5cGUgY2hhaW4uXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90TmVzdGVkUHJvcGVydHkoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH19LCAndGVhLm9vbG9uZycpO1xuICAgKlxuICAgKiBAbmFtZSBub3ROZXN0ZWRQcm9wZXJ0eVxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90TmVzdGVkUHJvcGVydHkgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQubm90TmVzdGVkUHJvcGVydHksIHRydWUpXG4gICAgICAudG8ubm90LmhhdmUubmVzdGVkLnByb3BlcnR5KHByb3ApO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5lc3RlZFByb3BlcnR5VmFsKG9iamVjdCwgcHJvcGVydHksIHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYSBwcm9wZXJ0eSBuYW1lZCBieSBgcHJvcGVydHlgIHdpdGggdmFsdWUgZ2l2ZW5cbiAgICogYnkgYHZhbHVlYC4gYHByb3BlcnR5YCBjYW4gdXNlIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gZm9yIG5lc3RlZFxuICAgKiByZWZlcmVuY2UuIFVzZXMgYSBzdHJpY3QgZXF1YWxpdHkgY2hlY2sgKD09PSkuXG4gICAqXG4gICAqICAgICBhc3NlcnQubmVzdGVkUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH19LCAndGVhLmdyZWVuJywgJ21hdGNoYScpO1xuICAgKlxuICAgKiBAbmFtZSBuZXN0ZWRQcm9wZXJ0eVZhbFxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eVxuICAgKiBAcGFyYW0ge01peGVkfSB2YWx1ZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubmVzdGVkUHJvcGVydHlWYWwgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCB2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5uZXN0ZWRQcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5oYXZlLm5lc3RlZC5wcm9wZXJ0eShwcm9wLCB2YWwpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm5vdE5lc3RlZFByb3BlcnR5VmFsKG9iamVjdCwgcHJvcGVydHksIHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBkb2VzIF9ub3RfIGhhdmUgYSBwcm9wZXJ0eSBuYW1lZCBieSBgcHJvcGVydHlgIHdpdGhcbiAgICogdmFsdWUgZ2l2ZW4gYnkgYHZhbHVlYC4gYHByb3BlcnR5YCBjYW4gdXNlIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gZm9yXG4gICAqIG5lc3RlZCByZWZlcmVuY2UuIFVzZXMgYSBzdHJpY3QgZXF1YWxpdHkgY2hlY2sgKD09PSkuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90TmVzdGVkUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46ICdtYXRjaGEnIH19LCAndGVhLmdyZWVuJywgJ2tvbmFjaGEnKTtcbiAgICogICAgIGFzc2VydC5ub3ROZXN0ZWRQcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogJ21hdGNoYScgfX0sICdjb2ZmZWUuZ3JlZW4nLCAnbWF0Y2hhJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdE5lc3RlZFByb3BlcnR5VmFsXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5XG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3ROZXN0ZWRQcm9wZXJ0eVZhbCA9IGZ1bmN0aW9uIChvYmosIHByb3AsIHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lm5vdE5lc3RlZFByb3BlcnR5VmFsLCB0cnVlKVxuICAgICAgLnRvLm5vdC5oYXZlLm5lc3RlZC5wcm9wZXJ0eShwcm9wLCB2YWwpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmRlZXBOZXN0ZWRQcm9wZXJ0eVZhbChvYmplY3QsIHByb3BlcnR5LCB2YWx1ZSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIGEgcHJvcGVydHkgbmFtZWQgYnkgYHByb3BlcnR5YCB3aXRoIGEgdmFsdWUgZ2l2ZW5cbiAgICogYnkgYHZhbHVlYC4gYHByb3BlcnR5YCBjYW4gdXNlIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gZm9yIG5lc3RlZFxuICAgKiByZWZlcmVuY2UuIFVzZXMgYSBkZWVwIGVxdWFsaXR5IGNoZWNrLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmRlZXBOZXN0ZWRQcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogeyBtYXRjaGE6ICd5dW0nIH0gfSB9LCAndGVhLmdyZWVuJywgeyBtYXRjaGE6ICd5dW0nIH0pO1xuICAgKlxuICAgKiBAbmFtZSBkZWVwTmVzdGVkUHJvcGVydHlWYWxcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHlcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRlZXBOZXN0ZWRQcm9wZXJ0eVZhbCA9IGZ1bmN0aW9uIChvYmosIHByb3AsIHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmRlZXBOZXN0ZWRQcm9wZXJ0eVZhbCwgdHJ1ZSlcbiAgICAgIC50by5oYXZlLmRlZXAubmVzdGVkLnByb3BlcnR5KHByb3AsIHZhbCk7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAubm90RGVlcE5lc3RlZFByb3BlcnR5VmFsKG9iamVjdCwgcHJvcGVydHksIHZhbHVlLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBkb2VzIF9ub3RfIGhhdmUgYSBwcm9wZXJ0eSBuYW1lZCBieSBgcHJvcGVydHlgIHdpdGhcbiAgICogdmFsdWUgZ2l2ZW4gYnkgYHZhbHVlYC4gYHByb3BlcnR5YCBjYW4gdXNlIGRvdC0gYW5kIGJyYWNrZXQtbm90YXRpb24gZm9yXG4gICAqIG5lc3RlZCByZWZlcmVuY2UuIFVzZXMgYSBkZWVwIGVxdWFsaXR5IGNoZWNrLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBOZXN0ZWRQcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogeyBtYXRjaGE6ICd5dW0nIH0gfSB9LCAndGVhLmdyZWVuJywgeyBvb2xvbmc6ICd5dW0nIH0pO1xuICAgKiAgICAgYXNzZXJ0Lm5vdERlZXBOZXN0ZWRQcm9wZXJ0eVZhbCh7IHRlYTogeyBncmVlbjogeyBtYXRjaGE6ICd5dW0nIH0gfSB9LCAndGVhLmdyZWVuJywgeyBtYXRjaGE6ICd5dWNrJyB9KTtcbiAgICogICAgIGFzc2VydC5ub3REZWVwTmVzdGVkUHJvcGVydHlWYWwoeyB0ZWE6IHsgZ3JlZW46IHsgbWF0Y2hhOiAneXVtJyB9IH0gfSwgJ3RlYS5ibGFjaycsIHsgbWF0Y2hhOiAneXVtJyB9KTtcbiAgICpcbiAgICogQG5hbWUgbm90RGVlcE5lc3RlZFByb3BlcnR5VmFsXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5XG4gICAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3REZWVwTmVzdGVkUHJvcGVydHlWYWwgPSBmdW5jdGlvbiAob2JqLCBwcm9wLCB2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5ub3REZWVwTmVzdGVkUHJvcGVydHlWYWwsIHRydWUpXG4gICAgICAudG8ubm90LmhhdmUuZGVlcC5uZXN0ZWQucHJvcGVydHkocHJvcCwgdmFsKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmxlbmd0aE9mKG9iamVjdCwgbGVuZ3RoLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYSBgbGVuZ3RoYCBvciBgc2l6ZWAgd2l0aCB0aGUgZXhwZWN0ZWQgdmFsdWUuXG4gICAqXG4gICAqICAgICBhc3NlcnQubGVuZ3RoT2YoWzEsMiwzXSwgMywgJ2FycmF5IGhhcyBsZW5ndGggb2YgMycpO1xuICAgKiAgICAgYXNzZXJ0Lmxlbmd0aE9mKCdmb29iYXInLCA2LCAnc3RyaW5nIGhhcyBsZW5ndGggb2YgNicpO1xuICAgKiAgICAgYXNzZXJ0Lmxlbmd0aE9mKG5ldyBTZXQoWzEsMiwzXSksIDMsICdzZXQgaGFzIHNpemUgb2YgMycpO1xuICAgKiAgICAgYXNzZXJ0Lmxlbmd0aE9mKG5ldyBNYXAoW1snYScsMV0sWydiJywyXSxbJ2MnLDNdXSksIDMsICdtYXAgaGFzIHNpemUgb2YgMycpO1xuICAgKlxuICAgKiBAbmFtZSBsZW5ndGhPZlxuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGxlbmd0aFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubGVuZ3RoT2YgPSBmdW5jdGlvbiAoZXhwLCBsZW4sIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oZXhwLCBtc2csIGFzc2VydC5sZW5ndGhPZiwgdHJ1ZSkudG8uaGF2ZS5sZW5ndGhPZihsZW4pO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmhhc0FueUtleXMob2JqZWN0LCBba2V5c10sIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGhhcyBhdCBsZWFzdCBvbmUgb2YgdGhlIGBrZXlzYCBwcm92aWRlZC5cbiAgICogWW91IGNhbiBhbHNvIHByb3ZpZGUgYSBzaW5nbGUgb2JqZWN0IGluc3RlYWQgb2YgYSBga2V5c2AgYXJyYXkgYW5kIGl0cyBrZXlzXG4gICAqIHdpbGwgYmUgdXNlZCBhcyB0aGUgZXhwZWN0ZWQgc2V0IG9mIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaGFzQW55S2V5cyh7Zm9vOiAxLCBiYXI6IDIsIGJhejogM30sIFsnZm9vJywgJ2lEb250RXhpc3QnLCAnYmF6J10pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueUtleXMoe2ZvbzogMSwgYmFyOiAyLCBiYXo6IDN9LCB7Zm9vOiAzMCwgaURvbnRFeGlzdDogOTksIGJhejogMTMzN30pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueUtleXMobmV3IE1hcChbW3tmb286IDF9LCAnYmFyJ10sIFsna2V5JywgJ3ZhbHVlJ11dKSwgW3tmb286IDF9LCAna2V5J10pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueUtleXMobmV3IFNldChbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddKSwgW3tmb286ICdiYXInfSwgJ2Fub3RoZXJLZXknXSk7XG4gICAqXG4gICAqIEBuYW1lIGhhc0FueUtleXNcbiAgICogQHBhcmFtIHtNaXhlZH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7QXJyYXl8T2JqZWN0fSBrZXlzXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5oYXNBbnlLZXlzID0gZnVuY3Rpb24gKG9iaiwga2V5cywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lmhhc0FueUtleXMsIHRydWUpLnRvLmhhdmUuYW55LmtleXMoa2V5cyk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5oYXNBbGxLZXlzKG9iamVjdCwgW2tleXNdLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYWxsIGFuZCBvbmx5IGFsbCBvZiB0aGUgYGtleXNgIHByb3ZpZGVkLlxuICAgKiBZb3UgY2FuIGFsc28gcHJvdmlkZSBhIHNpbmdsZSBvYmplY3QgaW5zdGVhZCBvZiBhIGBrZXlzYCBhcnJheSBhbmQgaXRzIGtleXNcbiAgICogd2lsbCBiZSB1c2VkIGFzIHRoZSBleHBlY3RlZCBzZXQgb2Yga2V5cy5cbiAgICpcbiAgICogICAgIGFzc2VydC5oYXNBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwgWydmb28nLCAnYmFyJywgJ2JheiddKTtcbiAgICogICAgIGFzc2VydC5oYXNBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwge2ZvbzogMzAsIGJhcjogOTksIGJhejogMTMzN10pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FsbEtleXMobmV3IE1hcChbW3tmb286IDF9LCAnYmFyJ10sIFsna2V5JywgJ3ZhbHVlJ11dKSwgW3tmb286IDF9LCAna2V5J10pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FsbEtleXMobmV3IFNldChbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddLCBbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddKTtcbiAgICpcbiAgICogQG5hbWUgaGFzQWxsS2V5c1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmdbXX0ga2V5c1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaGFzQWxsS2V5cyA9IGZ1bmN0aW9uIChvYmosIGtleXMsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5oYXNBbGxLZXlzLCB0cnVlKS50by5oYXZlLmFsbC5rZXlzKGtleXMpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAuY29udGFpbnNBbGxLZXlzKG9iamVjdCwgW2tleXNdLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBoYXMgYWxsIG9mIHRoZSBga2V5c2AgcHJvdmlkZWQgYnV0IG1heSBoYXZlIG1vcmUga2V5cyBub3QgbGlzdGVkLlxuICAgKiBZb3UgY2FuIGFsc28gcHJvdmlkZSBhIHNpbmdsZSBvYmplY3QgaW5zdGVhZCBvZiBhIGBrZXlzYCBhcnJheSBhbmQgaXRzIGtleXNcbiAgICogd2lsbCBiZSB1c2VkIGFzIHRoZSBleHBlY3RlZCBzZXQgb2Yga2V5cy5cbiAgICpcbiAgICogICAgIGFzc2VydC5jb250YWluc0FsbEtleXMoe2ZvbzogMSwgYmFyOiAyLCBiYXo6IDN9LCBbJ2ZvbycsICdiYXonXSk7XG4gICAqICAgICBhc3NlcnQuY29udGFpbnNBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwgWydmb28nLCAnYmFyJywgJ2JheiddKTtcbiAgICogICAgIGFzc2VydC5jb250YWluc0FsbEtleXMoe2ZvbzogMSwgYmFyOiAyLCBiYXo6IDN9LCB7Zm9vOiAzMCwgYmF6OiAxMzM3fSk7XG4gICAqICAgICBhc3NlcnQuY29udGFpbnNBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwge2ZvbzogMzAsIGJhcjogOTksIGJhejogMTMzN30pO1xuICAgKiAgICAgYXNzZXJ0LmNvbnRhaW5zQWxsS2V5cyhuZXcgTWFwKFtbe2ZvbzogMX0sICdiYXInXSwgWydrZXknLCAndmFsdWUnXV0pLCBbe2ZvbzogMX1dKTtcbiAgICogICAgIGFzc2VydC5jb250YWluc0FsbEtleXMobmV3IE1hcChbW3tmb286IDF9LCAnYmFyJ10sIFsna2V5JywgJ3ZhbHVlJ11dKSwgW3tmb286IDF9LCAna2V5J10pO1xuICAgKiAgICAgYXNzZXJ0LmNvbnRhaW5zQWxsS2V5cyhuZXcgU2V0KFt7Zm9vOiAnYmFyJ30sICdhbm90aGVyS2V5J10sIFt7Zm9vOiAnYmFyJ31dKTtcbiAgICogICAgIGFzc2VydC5jb250YWluc0FsbEtleXMobmV3IFNldChbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddLCBbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddKTtcbiAgICpcbiAgICogQG5hbWUgY29udGFpbnNBbGxLZXlzXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ1tdfSBrZXlzXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5jb250YWluc0FsbEtleXMgPSBmdW5jdGlvbiAob2JqLCBrZXlzLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuY29udGFpbnNBbGxLZXlzLCB0cnVlKVxuICAgICAgLnRvLmNvbnRhaW4uYWxsLmtleXMoa2V5cyk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5kb2VzTm90SGF2ZUFueUtleXMob2JqZWN0LCBba2V5c10sIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGhhcyBub25lIG9mIHRoZSBga2V5c2AgcHJvdmlkZWQuXG4gICAqIFlvdSBjYW4gYWxzbyBwcm92aWRlIGEgc2luZ2xlIG9iamVjdCBpbnN0ZWFkIG9mIGEgYGtleXNgIGFycmF5IGFuZCBpdHMga2V5c1xuICAgKiB3aWxsIGJlIHVzZWQgYXMgdGhlIGV4cGVjdGVkIHNldCBvZiBrZXlzLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQW55S2V5cyh7Zm9vOiAxLCBiYXI6IDIsIGJhejogM30sIFsnb25lJywgJ3R3bycsICdleGFtcGxlJ10pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQW55S2V5cyh7Zm9vOiAxLCBiYXI6IDIsIGJhejogM30sIHtvbmU6IDEsIHR3bzogMiwgZXhhbXBsZTogJ2Zvbyd9KTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90SGF2ZUFueUtleXMobmV3IE1hcChbW3tmb286IDF9LCAnYmFyJ10sIFsna2V5JywgJ3ZhbHVlJ11dKSwgW3tvbmU6ICd0d28nfSwgJ2V4YW1wbGUnXSk7XG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbnlLZXlzKG5ldyBTZXQoW3tmb286ICdiYXInfSwgJ2Fub3RoZXJLZXknXSwgW3tvbmU6ICd0d28nfSwgJ2V4YW1wbGUnXSk7XG4gICAqXG4gICAqIEBuYW1lIGRvZXNOb3RIYXZlQW55S2V5c1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmdbXX0ga2V5c1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuZG9lc05vdEhhdmVBbnlLZXlzID0gZnVuY3Rpb24gKG9iaiwga2V5cywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmRvZXNOb3RIYXZlQW55S2V5cywgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5hbnkua2V5cyhrZXlzKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmRvZXNOb3RIYXZlQWxsS2V5cyhvYmplY3QsIFtrZXlzXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgZG9lcyBub3QgaGF2ZSBhdCBsZWFzdCBvbmUgb2YgdGhlIGBrZXlzYCBwcm92aWRlZC5cbiAgICogWW91IGNhbiBhbHNvIHByb3ZpZGUgYSBzaW5nbGUgb2JqZWN0IGluc3RlYWQgb2YgYSBga2V5c2AgYXJyYXkgYW5kIGl0cyBrZXlzXG4gICAqIHdpbGwgYmUgdXNlZCBhcyB0aGUgZXhwZWN0ZWQgc2V0IG9mIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwgWydvbmUnLCAndHdvJywgJ2V4YW1wbGUnXSk7XG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbGxLZXlzKHtmb286IDEsIGJhcjogMiwgYmF6OiAzfSwge29uZTogMSwgdHdvOiAyLCBleGFtcGxlOiAnZm9vJ30pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQWxsS2V5cyhuZXcgTWFwKFtbe2ZvbzogMX0sICdiYXInXSwgWydrZXknLCAndmFsdWUnXV0pLCBbe29uZTogJ3R3byd9LCAnZXhhbXBsZSddKTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90SGF2ZUFsbEtleXMobmV3IFNldChbe2ZvbzogJ2Jhcid9LCAnYW5vdGhlcktleSddLCBbe29uZTogJ3R3byd9LCAnZXhhbXBsZSddKTtcbiAgICpcbiAgICogQG5hbWUgZG9lc05vdEhhdmVBbGxLZXlzXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG9iamVjdFxuICAgKiBAcGFyYW0ge1N0cmluZ1tdfSBrZXlzXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kb2VzTm90SGF2ZUFsbEtleXMgPSBmdW5jdGlvbiAob2JqLCBrZXlzLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuZG9lc05vdEhhdmVBbGxLZXlzLCB0cnVlKVxuICAgICAgLnRvLm5vdC5oYXZlLmFsbC5rZXlzKGtleXMpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAuaGFzQW55RGVlcEtleXMob2JqZWN0LCBba2V5c10sIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGhhcyBhdCBsZWFzdCBvbmUgb2YgdGhlIGBrZXlzYCBwcm92aWRlZC5cbiAgICogU2luY2UgU2V0cyBhbmQgTWFwcyBjYW4gaGF2ZSBvYmplY3RzIGFzIGtleXMgeW91IGNhbiB1c2UgdGhpcyBhc3NlcnRpb24gdG8gcGVyZm9ybVxuICAgKiBhIGRlZXAgY29tcGFyaXNvbi5cbiAgICogWW91IGNhbiBhbHNvIHByb3ZpZGUgYSBzaW5nbGUgb2JqZWN0IGluc3RlYWQgb2YgYSBga2V5c2AgYXJyYXkgYW5kIGl0cyBrZXlzXG4gICAqIHdpbGwgYmUgdXNlZCBhcyB0aGUgZXhwZWN0ZWQgc2V0IG9mIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaGFzQW55RGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFsxLCAyXV0pLCB7b25lOiAnb25lJ30pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueURlZXBLZXlzKG5ldyBNYXAoW1t7b25lOiAnb25lJ30sICd2YWx1ZU9uZSddLCBbMSwgMl1dKSwgW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSk7XG4gICAqICAgICBhc3NlcnQuaGFzQW55RGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFt7dHdvOiAndHdvJ30sICd2YWx1ZVR3byddXSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueURlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIHtvbmU6ICdvbmUnfSk7XG4gICAqICAgICBhc3NlcnQuaGFzQW55RGVlcEtleXMobmV3IFNldChbe29uZTogJ29uZSd9LCB7dHdvOiAndHdvJ31dKSwgW3tvbmU6ICdvbmUnfSwge3RocmVlOiAndGhyZWUnfV0pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FueURlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKlxuICAgKiBAbmFtZSBoYXNBbnlEZWVwS2V5c1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtBcnJheXxPYmplY3R9IGtleXNcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lmhhc0FueURlZXBLZXlzID0gZnVuY3Rpb24gKG9iaiwga2V5cywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lmhhc0FueURlZXBLZXlzLCB0cnVlKVxuICAgICAgLnRvLmhhdmUuYW55LmRlZXAua2V5cyhrZXlzKTtcbiAgfVxuXG4gLyoqXG4gICAqICMjIyAuaGFzQWxsRGVlcEtleXMob2JqZWN0LCBba2V5c10sIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGhhcyBhbGwgYW5kIG9ubHkgYWxsIG9mIHRoZSBga2V5c2AgcHJvdmlkZWQuXG4gICAqIFNpbmNlIFNldHMgYW5kIE1hcHMgY2FuIGhhdmUgb2JqZWN0cyBhcyBrZXlzIHlvdSBjYW4gdXNlIHRoaXMgYXNzZXJ0aW9uIHRvIHBlcmZvcm1cbiAgICogYSBkZWVwIGNvbXBhcmlzb24uXG4gICAqIFlvdSBjYW4gYWxzbyBwcm92aWRlIGEgc2luZ2xlIG9iamVjdCBpbnN0ZWFkIG9mIGEgYGtleXNgIGFycmF5IGFuZCBpdHMga2V5c1xuICAgKiB3aWxsIGJlIHVzZWQgYXMgdGhlIGV4cGVjdGVkIHNldCBvZiBrZXlzLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lmhhc0FsbERlZXBLZXlzKG5ldyBNYXAoW1t7b25lOiAnb25lJ30sICd2YWx1ZU9uZSddXSksIHtvbmU6ICdvbmUnfSk7XG4gICAqICAgICBhc3NlcnQuaGFzQWxsRGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFt7dHdvOiAndHdvJ30sICd2YWx1ZVR3byddXSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FsbERlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfV0pLCB7b25lOiAnb25lJ30pO1xuICAgKiAgICAgYXNzZXJ0Lmhhc0FsbERlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKlxuICAgKiBAbmFtZSBoYXNBbGxEZWVwS2V5c1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtBcnJheXxPYmplY3R9IGtleXNcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lmhhc0FsbERlZXBLZXlzID0gZnVuY3Rpb24gKG9iaiwga2V5cywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0Lmhhc0FsbERlZXBLZXlzLCB0cnVlKVxuICAgICAgLnRvLmhhdmUuYWxsLmRlZXAua2V5cyhrZXlzKTtcbiAgfVxuXG4gLyoqXG4gICAqICMjIyAuY29udGFpbnNBbGxEZWVwS2V5cyhvYmplY3QsIFtrZXlzXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgY29udGFpbnMgYWxsIG9mIHRoZSBga2V5c2AgcHJvdmlkZWQuXG4gICAqIFNpbmNlIFNldHMgYW5kIE1hcHMgY2FuIGhhdmUgb2JqZWN0cyBhcyBrZXlzIHlvdSBjYW4gdXNlIHRoaXMgYXNzZXJ0aW9uIHRvIHBlcmZvcm1cbiAgICogYSBkZWVwIGNvbXBhcmlzb24uXG4gICAqIFlvdSBjYW4gYWxzbyBwcm92aWRlIGEgc2luZ2xlIG9iamVjdCBpbnN0ZWFkIG9mIGEgYGtleXNgIGFycmF5IGFuZCBpdHMga2V5c1xuICAgKiB3aWxsIGJlIHVzZWQgYXMgdGhlIGV4cGVjdGVkIHNldCBvZiBrZXlzLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmNvbnRhaW5zQWxsRGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFsxLCAyXV0pLCB7b25lOiAnb25lJ30pO1xuICAgKiAgICAgYXNzZXJ0LmNvbnRhaW5zQWxsRGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFt7dHdvOiAndHdvJ30sICd2YWx1ZVR3byddXSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKiAgICAgYXNzZXJ0LmNvbnRhaW5zQWxsRGVlcEtleXMobmV3IFNldChbe29uZTogJ29uZSd9LCB7dHdvOiAndHdvJ31dKSwge29uZTogJ29uZSd9KTtcbiAgICogICAgIGFzc2VydC5jb250YWluc0FsbERlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pO1xuICAgKlxuICAgKiBAbmFtZSBjb250YWluc0FsbERlZXBLZXlzXG4gICAqIEBwYXJhbSB7TWl4ZWR9IG9iamVjdFxuICAgKiBAcGFyYW0ge0FycmF5fE9iamVjdH0ga2V5c1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuY29udGFpbnNBbGxEZWVwS2V5cyA9IGZ1bmN0aW9uIChvYmosIGtleXMsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5jb250YWluc0FsbERlZXBLZXlzLCB0cnVlKVxuICAgICAgLnRvLmNvbnRhaW4uYWxsLmRlZXAua2V5cyhrZXlzKTtcbiAgfVxuXG4gLyoqXG4gICAqICMjIyAuZG9lc05vdEhhdmVBbnlEZWVwS2V5cyhvYmplY3QsIFtrZXlzXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaGFzIG5vbmUgb2YgdGhlIGBrZXlzYCBwcm92aWRlZC5cbiAgICogU2luY2UgU2V0cyBhbmQgTWFwcyBjYW4gaGF2ZSBvYmplY3RzIGFzIGtleXMgeW91IGNhbiB1c2UgdGhpcyBhc3NlcnRpb24gdG8gcGVyZm9ybVxuICAgKiBhIGRlZXAgY29tcGFyaXNvbi5cbiAgICogWW91IGNhbiBhbHNvIHByb3ZpZGUgYSBzaW5nbGUgb2JqZWN0IGluc3RlYWQgb2YgYSBga2V5c2AgYXJyYXkgYW5kIGl0cyBrZXlzXG4gICAqIHdpbGwgYmUgdXNlZCBhcyB0aGUgZXhwZWN0ZWQgc2V0IG9mIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbnlEZWVwS2V5cyhuZXcgTWFwKFtbe29uZTogJ29uZSd9LCAndmFsdWVPbmUnXSwgWzEsIDJdXSksIHt0aGlzRG9lc05vdDogJ2V4aXN0J30pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQW55RGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFt7dHdvOiAndHdvJ30sICd2YWx1ZVR3byddXSksIFt7dHdlbnR5OiAndHdlbnR5J30sIHtmaWZ0eTogJ2ZpZnR5J31dKTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90SGF2ZUFueURlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIHt0d2VudHk6ICd0d2VudHknfSk7XG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbnlEZWVwS2V5cyhuZXcgU2V0KFt7b25lOiAnb25lJ30sIHt0d286ICd0d28nfV0pLCBbe3R3ZW50eTogJ3R3ZW50eSd9LCB7ZmlmdHk6ICdmaWZ0eSd9XSk7XG4gICAqXG4gICAqIEBuYW1lIGRvZXNOb3RIYXZlQW55RGVlcEtleXNcbiAgICogQHBhcmFtIHtNaXhlZH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7QXJyYXl8T2JqZWN0fSBrZXlzXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kb2VzTm90SGF2ZUFueURlZXBLZXlzID0gZnVuY3Rpb24gKG9iaiwga2V5cywgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmRvZXNOb3RIYXZlQW55RGVlcEtleXMsIHRydWUpXG4gICAgICAudG8ubm90LmhhdmUuYW55LmRlZXAua2V5cyhrZXlzKTtcbiAgfVxuXG4gLyoqXG4gICAqICMjIyAuZG9lc05vdEhhdmVBbGxEZWVwS2V5cyhvYmplY3QsIFtrZXlzXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgZG9lcyBub3QgaGF2ZSBhdCBsZWFzdCBvbmUgb2YgdGhlIGBrZXlzYCBwcm92aWRlZC5cbiAgICogU2luY2UgU2V0cyBhbmQgTWFwcyBjYW4gaGF2ZSBvYmplY3RzIGFzIGtleXMgeW91IGNhbiB1c2UgdGhpcyBhc3NlcnRpb24gdG8gcGVyZm9ybVxuICAgKiBhIGRlZXAgY29tcGFyaXNvbi5cbiAgICogWW91IGNhbiBhbHNvIHByb3ZpZGUgYSBzaW5nbGUgb2JqZWN0IGluc3RlYWQgb2YgYSBga2V5c2AgYXJyYXkgYW5kIGl0cyBrZXlzXG4gICAqIHdpbGwgYmUgdXNlZCBhcyB0aGUgZXhwZWN0ZWQgc2V0IG9mIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuZG9lc05vdEhhdmVBbGxEZWVwS2V5cyhuZXcgTWFwKFtbe29uZTogJ29uZSd9LCAndmFsdWVPbmUnXSwgWzEsIDJdXSksIHt0aGlzRG9lc05vdDogJ2V4aXN0J30pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQWxsRGVlcEtleXMobmV3IE1hcChbW3tvbmU6ICdvbmUnfSwgJ3ZhbHVlT25lJ10sIFt7dHdvOiAndHdvJ30sICd2YWx1ZVR3byddXSksIFt7dHdlbnR5OiAndHdlbnR5J30sIHtvbmU6ICdvbmUnfV0pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RIYXZlQWxsRGVlcEtleXMobmV3IFNldChbe29uZTogJ29uZSd9LCB7dHdvOiAndHdvJ31dKSwge3R3ZW50eTogJ3R3ZW50eSd9KTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90SGF2ZUFsbERlZXBLZXlzKG5ldyBTZXQoW3tvbmU6ICdvbmUnfSwge3R3bzogJ3R3byd9XSksIFt7b25lOiAnb25lJ30sIHtmaWZ0eTogJ2ZpZnR5J31dKTtcbiAgICpcbiAgICogQG5hbWUgZG9lc05vdEhhdmVBbGxEZWVwS2V5c1xuICAgKiBAcGFyYW0ge01peGVkfSBvYmplY3RcbiAgICogQHBhcmFtIHtBcnJheXxPYmplY3R9IGtleXNcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRvZXNOb3RIYXZlQWxsRGVlcEtleXMgPSBmdW5jdGlvbiAob2JqLCBrZXlzLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuZG9lc05vdEhhdmVBbGxEZWVwS2V5cywgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5hbGwuZGVlcC5rZXlzKGtleXMpO1xuICB9XG5cbiAvKipcbiAgICogIyMjIC50aHJvd3MoZm4sIFtlcnJvckxpa2Uvc3RyaW5nL3JlZ2V4cF0sIFtzdHJpbmcvcmVnZXhwXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBJZiBgZXJyb3JMaWtlYCBpcyBhbiBgRXJyb3JgIGNvbnN0cnVjdG9yLCBhc3NlcnRzIHRoYXQgYGZuYCB3aWxsIHRocm93IGFuIGVycm9yIHRoYXQgaXMgYW5cbiAgICogaW5zdGFuY2Ugb2YgYGVycm9yTGlrZWAuXG4gICAqIElmIGBlcnJvckxpa2VgIGlzIGFuIGBFcnJvcmAgaW5zdGFuY2UsIGFzc2VydHMgdGhhdCB0aGUgZXJyb3IgdGhyb3duIGlzIHRoZSBzYW1lXG4gICAqIGluc3RhbmNlIGFzIGBlcnJvckxpa2VgLlxuICAgKiBJZiBgZXJyTXNnTWF0Y2hlcmAgaXMgcHJvdmlkZWQsIGl0IGFsc28gYXNzZXJ0cyB0aGF0IHRoZSBlcnJvciB0aHJvd24gd2lsbCBoYXZlIGFcbiAgICogbWVzc2FnZSBtYXRjaGluZyBgZXJyTXNnTWF0Y2hlcmAuXG4gICAqXG4gICAqICAgICBhc3NlcnQudGhyb3dzKGZuLCAnRXJyb3IgdGhyb3duIG11c3QgaGF2ZSB0aGlzIG1zZycpO1xuICAgKiAgICAgYXNzZXJ0LnRocm93cyhmbiwgL0Vycm9yIHRocm93biBtdXN0IGhhdmUgYSBtc2cgdGhhdCBtYXRjaGVzIHRoaXMvKTtcbiAgICogICAgIGFzc2VydC50aHJvd3MoZm4sIFJlZmVyZW5jZUVycm9yKTtcbiAgICogICAgIGFzc2VydC50aHJvd3MoZm4sIGVycm9ySW5zdGFuY2UpO1xuICAgKiAgICAgYXNzZXJ0LnRocm93cyhmbiwgUmVmZXJlbmNlRXJyb3IsICdFcnJvciB0aHJvd24gbXVzdCBiZSBhIFJlZmVyZW5jZUVycm9yIGFuZCBoYXZlIHRoaXMgbXNnJyk7XG4gICAqICAgICBhc3NlcnQudGhyb3dzKGZuLCBlcnJvckluc3RhbmNlLCAnRXJyb3IgdGhyb3duIG11c3QgYmUgdGhlIHNhbWUgZXJyb3JJbnN0YW5jZSBhbmQgaGF2ZSB0aGlzIG1zZycpO1xuICAgKiAgICAgYXNzZXJ0LnRocm93cyhmbiwgUmVmZXJlbmNlRXJyb3IsIC9FcnJvciB0aHJvd24gbXVzdCBiZSBhIFJlZmVyZW5jZUVycm9yIGFuZCBtYXRjaCB0aGlzLyk7XG4gICAqICAgICBhc3NlcnQudGhyb3dzKGZuLCBlcnJvckluc3RhbmNlLCAvRXJyb3IgdGhyb3duIG11c3QgYmUgdGhlIHNhbWUgZXJyb3JJbnN0YW5jZSBhbmQgbWF0Y2ggdGhpcy8pO1xuICAgKlxuICAgKiBAbmFtZSB0aHJvd3NcbiAgICogQGFsaWFzIHRocm93XG4gICAqIEBhbGlhcyBUaHJvd1xuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxuICAgKiBAcGFyYW0ge0Vycm9yQ29uc3RydWN0b3J8RXJyb3J9IGVycm9yTGlrZVxuICAgKiBAcGFyYW0ge1JlZ0V4cHxTdHJpbmd9IGVyck1zZ01hdGNoZXJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQHNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9FcnJvciNFcnJvcl90eXBlc1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQudGhyb3dzID0gZnVuY3Rpb24gKGZuLCBlcnJvckxpa2UsIGVyck1zZ01hdGNoZXIsIG1zZykge1xuICAgIGlmICgnc3RyaW5nJyA9PT0gdHlwZW9mIGVycm9yTGlrZSB8fCBlcnJvckxpa2UgaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICAgIGVyck1zZ01hdGNoZXIgPSBlcnJvckxpa2U7XG4gICAgICBlcnJvckxpa2UgPSBudWxsO1xuICAgIH1cblxuICAgIHZhciBhc3NlcnRFcnIgPSBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC50aHJvd3MsIHRydWUpXG4gICAgICAudG8udGhyb3coZXJyb3JMaWtlLCBlcnJNc2dNYXRjaGVyKTtcbiAgICByZXR1cm4gZmxhZyhhc3NlcnRFcnIsICdvYmplY3QnKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5kb2VzTm90VGhyb3coZm4sIFtlcnJvckxpa2Uvc3RyaW5nL3JlZ2V4cF0sIFtzdHJpbmcvcmVnZXhwXSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBJZiBgZXJyb3JMaWtlYCBpcyBhbiBgRXJyb3JgIGNvbnN0cnVjdG9yLCBhc3NlcnRzIHRoYXQgYGZuYCB3aWxsIF9ub3RfIHRocm93IGFuIGVycm9yIHRoYXQgaXMgYW5cbiAgICogaW5zdGFuY2Ugb2YgYGVycm9yTGlrZWAuXG4gICAqIElmIGBlcnJvckxpa2VgIGlzIGFuIGBFcnJvcmAgaW5zdGFuY2UsIGFzc2VydHMgdGhhdCB0aGUgZXJyb3IgdGhyb3duIGlzIF9ub3RfIHRoZSBzYW1lXG4gICAqIGluc3RhbmNlIGFzIGBlcnJvckxpa2VgLlxuICAgKiBJZiBgZXJyTXNnTWF0Y2hlcmAgaXMgcHJvdmlkZWQsIGl0IGFsc28gYXNzZXJ0cyB0aGF0IHRoZSBlcnJvciB0aHJvd24gd2lsbCBfbm90XyBoYXZlIGFcbiAgICogbWVzc2FnZSBtYXRjaGluZyBgZXJyTXNnTWF0Y2hlcmAuXG4gICAqXG4gICAqICAgICBhc3NlcnQuZG9lc05vdFRocm93KGZuLCAnQW55IEVycm9yIHRocm93biBtdXN0IG5vdCBoYXZlIHRoaXMgbWVzc2FnZScpO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RUaHJvdyhmbiwgL0FueSBFcnJvciB0aHJvd24gbXVzdCBub3QgbWF0Y2ggdGhpcy8pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RUaHJvdyhmbiwgRXJyb3IpO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RUaHJvdyhmbiwgZXJyb3JJbnN0YW5jZSk7XG4gICAqICAgICBhc3NlcnQuZG9lc05vdFRocm93KGZuLCBFcnJvciwgJ0Vycm9yIG11c3Qgbm90IGhhdmUgdGhpcyBtZXNzYWdlJyk7XG4gICAqICAgICBhc3NlcnQuZG9lc05vdFRocm93KGZuLCBlcnJvckluc3RhbmNlLCAnRXJyb3IgbXVzdCBub3QgaGF2ZSB0aGlzIG1lc3NhZ2UnKTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90VGhyb3coZm4sIEVycm9yLCAvRXJyb3IgbXVzdCBub3QgbWF0Y2ggdGhpcy8pO1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3RUaHJvdyhmbiwgZXJyb3JJbnN0YW5jZSwgL0Vycm9yIG11c3Qgbm90IG1hdGNoIHRoaXMvKTtcbiAgICpcbiAgICogQG5hbWUgZG9lc05vdFRocm93XG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IGZuXG4gICAqIEBwYXJhbSB7RXJyb3JDb25zdHJ1Y3Rvcn0gZXJyb3JMaWtlXG4gICAqIEBwYXJhbSB7UmVnRXhwfFN0cmluZ30gZXJyTXNnTWF0Y2hlclxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAc2VlIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuL0phdmFTY3JpcHQvUmVmZXJlbmNlL0dsb2JhbF9PYmplY3RzL0Vycm9yI0Vycm9yX3R5cGVzXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kb2VzTm90VGhyb3cgPSBmdW5jdGlvbiAoZm4sIGVycm9yTGlrZSwgZXJyTXNnTWF0Y2hlciwgbXNnKSB7XG4gICAgaWYgKCdzdHJpbmcnID09PSB0eXBlb2YgZXJyb3JMaWtlIHx8IGVycm9yTGlrZSBpbnN0YW5jZW9mIFJlZ0V4cCkge1xuICAgICAgZXJyTXNnTWF0Y2hlciA9IGVycm9yTGlrZTtcbiAgICAgIGVycm9yTGlrZSA9IG51bGw7XG4gICAgfVxuXG4gICAgbmV3IEFzc2VydGlvbihmbiwgbXNnLCBhc3NlcnQuZG9lc05vdFRocm93LCB0cnVlKVxuICAgICAgLnRvLm5vdC50aHJvdyhlcnJvckxpa2UsIGVyck1zZ01hdGNoZXIpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLm9wZXJhdG9yKHZhbDEsIG9wZXJhdG9yLCB2YWwyLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIENvbXBhcmVzIHR3byB2YWx1ZXMgdXNpbmcgYG9wZXJhdG9yYC5cbiAgICpcbiAgICogICAgIGFzc2VydC5vcGVyYXRvcigxLCAnPCcsIDIsICdldmVyeXRoaW5nIGlzIG9rJyk7XG4gICAqICAgICBhc3NlcnQub3BlcmF0b3IoMSwgJz4nLCAyLCAndGhpcyB3aWxsIGZhaWwnKTtcbiAgICpcbiAgICogQG5hbWUgb3BlcmF0b3JcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsMVxuICAgKiBAcGFyYW0ge1N0cmluZ30gb3BlcmF0b3JcbiAgICogQHBhcmFtIHtNaXhlZH0gdmFsMlxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQub3BlcmF0b3IgPSBmdW5jdGlvbiAodmFsLCBvcGVyYXRvciwgdmFsMiwgbXNnKSB7XG4gICAgdmFyIG9rO1xuICAgIHN3aXRjaChvcGVyYXRvcikge1xuICAgICAgY2FzZSAnPT0nOlxuICAgICAgICBvayA9IHZhbCA9PSB2YWwyO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJz09PSc6XG4gICAgICAgIG9rID0gdmFsID09PSB2YWwyO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJz4nOlxuICAgICAgICBvayA9IHZhbCA+IHZhbDI7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnPj0nOlxuICAgICAgICBvayA9IHZhbCA+PSB2YWwyO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJzwnOlxuICAgICAgICBvayA9IHZhbCA8IHZhbDI7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAnPD0nOlxuICAgICAgICBvayA9IHZhbCA8PSB2YWwyO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgJyE9JzpcbiAgICAgICAgb2sgPSB2YWwgIT0gdmFsMjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlICchPT0nOlxuICAgICAgICBvayA9IHZhbCAhPT0gdmFsMjtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICBtc2cgPSBtc2cgPyBtc2cgKyAnOiAnIDogbXNnO1xuICAgICAgICB0aHJvdyBuZXcgY2hhaS5Bc3NlcnRpb25FcnJvcihcbiAgICAgICAgICBtc2cgKyAnSW52YWxpZCBvcGVyYXRvciBcIicgKyBvcGVyYXRvciArICdcIicsXG4gICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgIGFzc2VydC5vcGVyYXRvclxuICAgICAgICApO1xuICAgIH1cbiAgICB2YXIgdGVzdCA9IG5ldyBBc3NlcnRpb24ob2ssIG1zZywgYXNzZXJ0Lm9wZXJhdG9yLCB0cnVlKTtcbiAgICB0ZXN0LmFzc2VydChcbiAgICAgICAgdHJ1ZSA9PT0gZmxhZyh0ZXN0LCAnb2JqZWN0JylcbiAgICAgICwgJ2V4cGVjdGVkICcgKyB1dGlsLmluc3BlY3QodmFsKSArICcgdG8gYmUgJyArIG9wZXJhdG9yICsgJyAnICsgdXRpbC5pbnNwZWN0KHZhbDIpXG4gICAgICAsICdleHBlY3RlZCAnICsgdXRpbC5pbnNwZWN0KHZhbCkgKyAnIHRvIG5vdCBiZSAnICsgb3BlcmF0b3IgKyAnICcgKyB1dGlsLmluc3BlY3QodmFsMikgKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5jbG9zZVRvKGFjdHVhbCwgZXhwZWN0ZWQsIGRlbHRhLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCB0aGUgdGFyZ2V0IGlzIGVxdWFsIGBleHBlY3RlZGAsIHRvIHdpdGhpbiBhICsvLSBgZGVsdGFgIHJhbmdlLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmNsb3NlVG8oMS41LCAxLCAwLjUsICdudW1iZXJzIGFyZSBjbG9zZScpO1xuICAgKlxuICAgKiBAbmFtZSBjbG9zZVRvXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBhY3R1YWxcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGV4cGVjdGVkXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkZWx0YVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuY2xvc2VUbyA9IGZ1bmN0aW9uIChhY3QsIGV4cCwgZGVsdGEsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oYWN0LCBtc2csIGFzc2VydC5jbG9zZVRvLCB0cnVlKS50by5iZS5jbG9zZVRvKGV4cCwgZGVsdGEpO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmFwcHJveGltYXRlbHkoYWN0dWFsLCBleHBlY3RlZCwgZGVsdGEsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgZXF1YWwgYGV4cGVjdGVkYCwgdG8gd2l0aGluIGEgKy8tIGBkZWx0YWAgcmFuZ2UuXG4gICAqXG4gICAqICAgICBhc3NlcnQuYXBwcm94aW1hdGVseSgxLjUsIDEsIDAuNSwgJ251bWJlcnMgYXJlIGNsb3NlJyk7XG4gICAqXG4gICAqIEBuYW1lIGFwcHJveGltYXRlbHlcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGFjdHVhbFxuICAgKiBAcGFyYW0ge051bWJlcn0gZXhwZWN0ZWRcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGRlbHRhXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5hcHByb3hpbWF0ZWx5ID0gZnVuY3Rpb24gKGFjdCwgZXhwLCBkZWx0YSwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihhY3QsIG1zZywgYXNzZXJ0LmFwcHJveGltYXRlbHksIHRydWUpXG4gICAgICAudG8uYmUuYXBwcm94aW1hdGVseShleHAsIGRlbHRhKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5zYW1lTWVtYmVycyhzZXQxLCBzZXQyLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc2V0MWAgYW5kIGBzZXQyYCBoYXZlIHRoZSBzYW1lIG1lbWJlcnMgaW4gYW55IG9yZGVyLiBVc2VzIGFcbiAgICogc3RyaWN0IGVxdWFsaXR5IGNoZWNrICg9PT0pLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LnNhbWVNZW1iZXJzKFsgMSwgMiwgMyBdLCBbIDIsIDEsIDMgXSwgJ3NhbWUgbWVtYmVycycpO1xuICAgKlxuICAgKiBAbmFtZSBzYW1lTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnNhbWVNZW1iZXJzID0gZnVuY3Rpb24gKHNldDEsIHNldDIsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oc2V0MSwgbXNnLCBhc3NlcnQuc2FtZU1lbWJlcnMsIHRydWUpXG4gICAgICAudG8uaGF2ZS5zYW1lLm1lbWJlcnMoc2V0Mik7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5ub3RTYW1lTWVtYmVycyhzZXQxLCBzZXQyLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc2V0MWAgYW5kIGBzZXQyYCBkb24ndCBoYXZlIHRoZSBzYW1lIG1lbWJlcnMgaW4gYW55IG9yZGVyLlxuICAgKiBVc2VzIGEgc3RyaWN0IGVxdWFsaXR5IGNoZWNrICg9PT0pLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdFNhbWVNZW1iZXJzKFsgMSwgMiwgMyBdLCBbIDUsIDEsIDMgXSwgJ25vdCBzYW1lIG1lbWJlcnMnKTtcbiAgICpcbiAgICogQG5hbWUgbm90U2FtZU1lbWJlcnNcbiAgICogQHBhcmFtIHtBcnJheX0gc2V0MVxuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQyXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3RTYW1lTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0Lm5vdFNhbWVNZW1iZXJzLCB0cnVlKVxuICAgICAgLnRvLm5vdC5oYXZlLnNhbWUubWVtYmVycyhzZXQyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLnNhbWVEZWVwTWVtYmVycyhzZXQxLCBzZXQyLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc2V0MWAgYW5kIGBzZXQyYCBoYXZlIHRoZSBzYW1lIG1lbWJlcnMgaW4gYW55IG9yZGVyLiBVc2VzIGFcbiAgICogZGVlcCBlcXVhbGl0eSBjaGVjay5cbiAgICpcbiAgICogICAgIGFzc2VydC5zYW1lRGVlcE1lbWJlcnMoWyB7IGE6IDEgfSwgeyBiOiAyIH0sIHsgYzogMyB9IF0sIFt7IGI6IDIgfSwgeyBhOiAxIH0sIHsgYzogMyB9XSwgJ3NhbWUgZGVlcCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIHNhbWVEZWVwTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnNhbWVEZWVwTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0LnNhbWVEZWVwTWVtYmVycywgdHJ1ZSlcbiAgICAgIC50by5oYXZlLnNhbWUuZGVlcC5tZW1iZXJzKHNldDIpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAubm90U2FtZURlZXBNZW1iZXJzKHNldDEsIHNldDIsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBzZXQxYCBhbmQgYHNldDJgIGRvbid0IGhhdmUgdGhlIHNhbWUgbWVtYmVycyBpbiBhbnkgb3JkZXIuXG4gICAqIFVzZXMgYSBkZWVwIGVxdWFsaXR5IGNoZWNrLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdFNhbWVEZWVwTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgW3sgYjogMiB9LCB7IGE6IDEgfSwgeyBmOiA1IH1dLCAnbm90IHNhbWUgZGVlcCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdFNhbWVEZWVwTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdFNhbWVEZWVwTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0Lm5vdFNhbWVEZWVwTWVtYmVycywgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5zYW1lLmRlZXAubWVtYmVycyhzZXQyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLnNhbWVPcmRlcmVkTWVtYmVycyhzZXQxLCBzZXQyLCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc2V0MWAgYW5kIGBzZXQyYCBoYXZlIHRoZSBzYW1lIG1lbWJlcnMgaW4gdGhlIHNhbWUgb3JkZXIuXG4gICAqIFVzZXMgYSBzdHJpY3QgZXF1YWxpdHkgY2hlY2sgKD09PSkuXG4gICAqXG4gICAqICAgICBhc3NlcnQuc2FtZU9yZGVyZWRNZW1iZXJzKFsgMSwgMiwgMyBdLCBbIDEsIDIsIDMgXSwgJ3NhbWUgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIHNhbWVPcmRlcmVkTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnNhbWVPcmRlcmVkTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0LnNhbWVPcmRlcmVkTWVtYmVycywgdHJ1ZSlcbiAgICAgIC50by5oYXZlLnNhbWUub3JkZXJlZC5tZW1iZXJzKHNldDIpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAubm90U2FtZU9yZGVyZWRNZW1iZXJzKHNldDEsIHNldDIsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBzZXQxYCBhbmQgYHNldDJgIGRvbid0IGhhdmUgdGhlIHNhbWUgbWVtYmVycyBpbiB0aGUgc2FtZVxuICAgKiBvcmRlci4gVXNlcyBhIHN0cmljdCBlcXVhbGl0eSBjaGVjayAoPT09KS5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RTYW1lT3JkZXJlZE1lbWJlcnMoWyAxLCAyLCAzIF0sIFsgMiwgMSwgMyBdLCAnbm90IHNhbWUgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdFNhbWVPcmRlcmVkTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdFNhbWVPcmRlcmVkTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0Lm5vdFNhbWVPcmRlcmVkTWVtYmVycywgdHJ1ZSlcbiAgICAgIC50by5ub3QuaGF2ZS5zYW1lLm9yZGVyZWQubWVtYmVycyhzZXQyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLnNhbWVEZWVwT3JkZXJlZE1lbWJlcnMoc2V0MSwgc2V0MiwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHNldDFgIGFuZCBgc2V0MmAgaGF2ZSB0aGUgc2FtZSBtZW1iZXJzIGluIHRoZSBzYW1lIG9yZGVyLlxuICAgKiBVc2VzIGEgZGVlcCBlcXVhbGl0eSBjaGVjay5cbiAgICpcbiAgICogICAgIGFzc2VydC5zYW1lRGVlcE9yZGVyZWRNZW1iZXJzKFsgeyBhOiAxIH0sIHsgYjogMiB9LCB7IGM6IDMgfSBdLCBbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgJ3NhbWUgZGVlcCBvcmRlcmVkIG1lbWJlcnMnKTtcbiAgICpcbiAgICogQG5hbWUgc2FtZURlZXBPcmRlcmVkTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzZXQxXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDJcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LnNhbWVEZWVwT3JkZXJlZE1lbWJlcnMgPSBmdW5jdGlvbiAoc2V0MSwgc2V0MiwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihzZXQxLCBtc2csIGFzc2VydC5zYW1lRGVlcE9yZGVyZWRNZW1iZXJzLCB0cnVlKVxuICAgICAgLnRvLmhhdmUuc2FtZS5kZWVwLm9yZGVyZWQubWVtYmVycyhzZXQyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLm5vdFNhbWVEZWVwT3JkZXJlZE1lbWJlcnMoc2V0MSwgc2V0MiwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHNldDFgIGFuZCBgc2V0MmAgZG9uJ3QgaGF2ZSB0aGUgc2FtZSBtZW1iZXJzIGluIHRoZSBzYW1lXG4gICAqIG9yZGVyLiBVc2VzIGEgZGVlcCBlcXVhbGl0eSBjaGVjay5cbiAgICpcbiAgICogICAgIGFzc2VydC5ub3RTYW1lRGVlcE9yZGVyZWRNZW1iZXJzKFsgeyBhOiAxIH0sIHsgYjogMiB9LCB7IGM6IDMgfSBdLCBbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyB6OiA1IH0gXSwgJ25vdCBzYW1lIGRlZXAgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqICAgICBhc3NlcnQubm90U2FtZURlZXBPcmRlcmVkTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgWyB7IGI6IDIgfSwgeyBhOiAxIH0sIHsgYzogMyB9IF0sICdub3Qgc2FtZSBkZWVwIG9yZGVyZWQgbWVtYmVycycpO1xuICAgKlxuICAgKiBAbmFtZSBub3RTYW1lRGVlcE9yZGVyZWRNZW1iZXJzXG4gICAqIEBwYXJhbSB7QXJyYXl9IHNldDFcbiAgICogQHBhcmFtIHtBcnJheX0gc2V0MlxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQubm90U2FtZURlZXBPcmRlcmVkTWVtYmVycyA9IGZ1bmN0aW9uIChzZXQxLCBzZXQyLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHNldDEsIG1zZywgYXNzZXJ0Lm5vdFNhbWVEZWVwT3JkZXJlZE1lbWJlcnMsIHRydWUpXG4gICAgICAudG8ubm90LmhhdmUuc2FtZS5kZWVwLm9yZGVyZWQubWVtYmVycyhzZXQyKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmluY2x1ZGVNZW1iZXJzKHN1cGVyc2V0LCBzdWJzZXQsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBzdWJzZXRgIGlzIGluY2x1ZGVkIGluIGBzdXBlcnNldGAgaW4gYW55IG9yZGVyLiBVc2VzIGFcbiAgICogc3RyaWN0IGVxdWFsaXR5IGNoZWNrICg9PT0pLiBEdXBsaWNhdGVzIGFyZSBpZ25vcmVkLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmluY2x1ZGVNZW1iZXJzKFsgMSwgMiwgMyBdLCBbIDIsIDEsIDIgXSwgJ2luY2x1ZGUgbWVtYmVycycpO1xuICAgKlxuICAgKiBAbmFtZSBpbmNsdWRlTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmluY2x1ZGVNZW1iZXJzID0gZnVuY3Rpb24gKHN1cGVyc2V0LCBzdWJzZXQsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oc3VwZXJzZXQsIG1zZywgYXNzZXJ0LmluY2x1ZGVNZW1iZXJzLCB0cnVlKVxuICAgICAgLnRvLmluY2x1ZGUubWVtYmVycyhzdWJzZXQpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAubm90SW5jbHVkZU1lbWJlcnMoc3VwZXJzZXQsIHN1YnNldCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHN1YnNldGAgaXNuJ3QgaW5jbHVkZWQgaW4gYHN1cGVyc2V0YCBpbiBhbnkgb3JkZXIuIFVzZXMgYVxuICAgKiBzdHJpY3QgZXF1YWxpdHkgY2hlY2sgKD09PSkuIER1cGxpY2F0ZXMgYXJlIGlnbm9yZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZU1lbWJlcnMoWyAxLCAyLCAzIF0sIFsgNSwgMSBdLCAnbm90IGluY2x1ZGUgbWVtYmVycycpO1xuICAgKlxuICAgKiBAbmFtZSBub3RJbmNsdWRlTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdEluY2x1ZGVNZW1iZXJzID0gZnVuY3Rpb24gKHN1cGVyc2V0LCBzdWJzZXQsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oc3VwZXJzZXQsIG1zZywgYXNzZXJ0Lm5vdEluY2x1ZGVNZW1iZXJzLCB0cnVlKVxuICAgICAgLnRvLm5vdC5pbmNsdWRlLm1lbWJlcnMoc3Vic2V0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmluY2x1ZGVEZWVwTWVtYmVycyhzdXBlcnNldCwgc3Vic2V0LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc3Vic2V0YCBpcyBpbmNsdWRlZCBpbiBgc3VwZXJzZXRgIGluIGFueSBvcmRlci4gVXNlcyBhIGRlZXBcbiAgICogZXF1YWxpdHkgY2hlY2suIER1cGxpY2F0ZXMgYXJlIGlnbm9yZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaW5jbHVkZURlZXBNZW1iZXJzKFsgeyBhOiAxIH0sIHsgYjogMiB9LCB7IGM6IDMgfSBdLCBbIHsgYjogMiB9LCB7IGE6IDEgfSwgeyBiOiAyIH0gXSwgJ2luY2x1ZGUgZGVlcCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIGluY2x1ZGVEZWVwTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmluY2x1ZGVEZWVwTWVtYmVycyA9IGZ1bmN0aW9uIChzdXBlcnNldCwgc3Vic2V0LCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHN1cGVyc2V0LCBtc2csIGFzc2VydC5pbmNsdWRlRGVlcE1lbWJlcnMsIHRydWUpXG4gICAgICAudG8uaW5jbHVkZS5kZWVwLm1lbWJlcnMoc3Vic2V0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLm5vdEluY2x1ZGVEZWVwTWVtYmVycyhzdXBlcnNldCwgc3Vic2V0LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgc3Vic2V0YCBpc24ndCBpbmNsdWRlZCBpbiBgc3VwZXJzZXRgIGluIGFueSBvcmRlci4gVXNlcyBhXG4gICAqIGRlZXAgZXF1YWxpdHkgY2hlY2suIER1cGxpY2F0ZXMgYXJlIGlnbm9yZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZURlZXBNZW1iZXJzKFsgeyBhOiAxIH0sIHsgYjogMiB9LCB7IGM6IDMgfSBdLCBbIHsgYjogMiB9LCB7IGY6IDUgfSBdLCAnbm90IGluY2x1ZGUgZGVlcCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdEluY2x1ZGVEZWVwTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdEluY2x1ZGVEZWVwTWVtYmVycyA9IGZ1bmN0aW9uIChzdXBlcnNldCwgc3Vic2V0LCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHN1cGVyc2V0LCBtc2csIGFzc2VydC5ub3RJbmNsdWRlRGVlcE1lbWJlcnMsIHRydWUpXG4gICAgICAudG8ubm90LmluY2x1ZGUuZGVlcC5tZW1iZXJzKHN1YnNldCk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5pbmNsdWRlT3JkZXJlZE1lbWJlcnMoc3VwZXJzZXQsIHN1YnNldCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHN1YnNldGAgaXMgaW5jbHVkZWQgaW4gYHN1cGVyc2V0YCBpbiB0aGUgc2FtZSBvcmRlclxuICAgKiBiZWdpbm5pbmcgd2l0aCB0aGUgZmlyc3QgZWxlbWVudCBpbiBgc3VwZXJzZXRgLiBVc2VzIGEgc3RyaWN0IGVxdWFsaXR5XG4gICAqIGNoZWNrICg9PT0pLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmluY2x1ZGVPcmRlcmVkTWVtYmVycyhbIDEsIDIsIDMgXSwgWyAxLCAyIF0sICdpbmNsdWRlIG9yZGVyZWQgbWVtYmVycycpO1xuICAgKlxuICAgKiBAbmFtZSBpbmNsdWRlT3JkZXJlZE1lbWJlcnNcbiAgICogQHBhcmFtIHtBcnJheX0gc3VwZXJzZXRcbiAgICogQHBhcmFtIHtBcnJheX0gc3Vic2V0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pbmNsdWRlT3JkZXJlZE1lbWJlcnMgPSBmdW5jdGlvbiAoc3VwZXJzZXQsIHN1YnNldCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihzdXBlcnNldCwgbXNnLCBhc3NlcnQuaW5jbHVkZU9yZGVyZWRNZW1iZXJzLCB0cnVlKVxuICAgICAgLnRvLmluY2x1ZGUub3JkZXJlZC5tZW1iZXJzKHN1YnNldCk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5ub3RJbmNsdWRlT3JkZXJlZE1lbWJlcnMoc3VwZXJzZXQsIHN1YnNldCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYHN1YnNldGAgaXNuJ3QgaW5jbHVkZWQgaW4gYHN1cGVyc2V0YCBpbiB0aGUgc2FtZSBvcmRlclxuICAgKiBiZWdpbm5pbmcgd2l0aCB0aGUgZmlyc3QgZWxlbWVudCBpbiBgc3VwZXJzZXRgLiBVc2VzIGEgc3RyaWN0IGVxdWFsaXR5XG4gICAqIGNoZWNrICg9PT0pLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm5vdEluY2x1ZGVPcmRlcmVkTWVtYmVycyhbIDEsIDIsIDMgXSwgWyAyLCAxIF0sICdub3QgaW5jbHVkZSBvcmRlcmVkIG1lbWJlcnMnKTtcbiAgICogICAgIGFzc2VydC5ub3RJbmNsdWRlT3JkZXJlZE1lbWJlcnMoWyAxLCAyLCAzIF0sIFsgMiwgMyBdLCAnbm90IGluY2x1ZGUgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdEluY2x1ZGVPcmRlcmVkTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0Lm5vdEluY2x1ZGVPcmRlcmVkTWVtYmVycyA9IGZ1bmN0aW9uIChzdXBlcnNldCwgc3Vic2V0LCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKHN1cGVyc2V0LCBtc2csIGFzc2VydC5ub3RJbmNsdWRlT3JkZXJlZE1lbWJlcnMsIHRydWUpXG4gICAgICAudG8ubm90LmluY2x1ZGUub3JkZXJlZC5tZW1iZXJzKHN1YnNldCk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5pbmNsdWRlRGVlcE9yZGVyZWRNZW1iZXJzKHN1cGVyc2V0LCBzdWJzZXQsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBzdWJzZXRgIGlzIGluY2x1ZGVkIGluIGBzdXBlcnNldGAgaW4gdGhlIHNhbWUgb3JkZXJcbiAgICogYmVnaW5uaW5nIHdpdGggdGhlIGZpcnN0IGVsZW1lbnQgaW4gYHN1cGVyc2V0YC4gVXNlcyBhIGRlZXAgZXF1YWxpdHlcbiAgICogY2hlY2suXG4gICAqXG4gICAqICAgICBhc3NlcnQuaW5jbHVkZURlZXBPcmRlcmVkTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgWyB7IGE6IDEgfSwgeyBiOiAyIH0gXSwgJ2luY2x1ZGUgZGVlcCBvcmRlcmVkIG1lbWJlcnMnKTtcbiAgICpcbiAgICogQG5hbWUgaW5jbHVkZURlZXBPcmRlcmVkTWVtYmVyc1xuICAgKiBAcGFyYW0ge0FycmF5fSBzdXBlcnNldFxuICAgKiBAcGFyYW0ge0FycmF5fSBzdWJzZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmluY2x1ZGVEZWVwT3JkZXJlZE1lbWJlcnMgPSBmdW5jdGlvbiAoc3VwZXJzZXQsIHN1YnNldCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihzdXBlcnNldCwgbXNnLCBhc3NlcnQuaW5jbHVkZURlZXBPcmRlcmVkTWVtYmVycywgdHJ1ZSlcbiAgICAgIC50by5pbmNsdWRlLmRlZXAub3JkZXJlZC5tZW1iZXJzKHN1YnNldCk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5ub3RJbmNsdWRlRGVlcE9yZGVyZWRNZW1iZXJzKHN1cGVyc2V0LCBzdWJzZXQsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBzdWJzZXRgIGlzbid0IGluY2x1ZGVkIGluIGBzdXBlcnNldGAgaW4gdGhlIHNhbWUgb3JkZXJcbiAgICogYmVnaW5uaW5nIHdpdGggdGhlIGZpcnN0IGVsZW1lbnQgaW4gYHN1cGVyc2V0YC4gVXNlcyBhIGRlZXAgZXF1YWxpdHlcbiAgICogY2hlY2suXG4gICAqXG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZURlZXBPcmRlcmVkTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgWyB7IGE6IDEgfSwgeyBmOiA1IH0gXSwgJ25vdCBpbmNsdWRlIGRlZXAgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZURlZXBPcmRlcmVkTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgWyB7IGI6IDIgfSwgeyBhOiAxIH0gXSwgJ25vdCBpbmNsdWRlIGRlZXAgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqICAgICBhc3NlcnQubm90SW5jbHVkZURlZXBPcmRlcmVkTWVtYmVycyhbIHsgYTogMSB9LCB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgWyB7IGI6IDIgfSwgeyBjOiAzIH0gXSwgJ25vdCBpbmNsdWRlIGRlZXAgb3JkZXJlZCBtZW1iZXJzJyk7XG4gICAqXG4gICAqIEBuYW1lIG5vdEluY2x1ZGVEZWVwT3JkZXJlZE1lbWJlcnNcbiAgICogQHBhcmFtIHtBcnJheX0gc3VwZXJzZXRcbiAgICogQHBhcmFtIHtBcnJheX0gc3Vic2V0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5ub3RJbmNsdWRlRGVlcE9yZGVyZWRNZW1iZXJzID0gZnVuY3Rpb24gKHN1cGVyc2V0LCBzdWJzZXQsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24oc3VwZXJzZXQsIG1zZywgYXNzZXJ0Lm5vdEluY2x1ZGVEZWVwT3JkZXJlZE1lbWJlcnMsIHRydWUpXG4gICAgICAudG8ubm90LmluY2x1ZGUuZGVlcC5vcmRlcmVkLm1lbWJlcnMoc3Vic2V0KTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLm9uZU9mKGluTGlzdCwgbGlzdCwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgbm9uLW9iamVjdCwgbm9uLWFycmF5IHZhbHVlIGBpbkxpc3RgIGFwcGVhcnMgaW4gdGhlIGZsYXQgYXJyYXkgYGxpc3RgLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0Lm9uZU9mKDEsIFsgMiwgMSBdLCAnTm90IGZvdW5kIGluIGxpc3QnKTtcbiAgICpcbiAgICogQG5hbWUgb25lT2ZcbiAgICogQHBhcmFtIHsqfSBpbkxpc3RcbiAgICogQHBhcmFtIHtBcnJheTwqPn0gbGlzdFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQub25lT2YgPSBmdW5jdGlvbiAoaW5MaXN0LCBsaXN0LCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKGluTGlzdCwgbXNnLCBhc3NlcnQub25lT2YsIHRydWUpLnRvLmJlLm9uZU9mKGxpc3QpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAuY2hhbmdlcyhmdW5jdGlvbiwgb2JqZWN0LCBwcm9wZXJ0eSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBjaGFuZ2VzIHRoZSB2YWx1ZSBvZiBhIHByb3BlcnR5LlxuICAgKlxuICAgKiAgICAgdmFyIG9iaiA9IHsgdmFsOiAxMCB9O1xuICAgKiAgICAgdmFyIGZuID0gZnVuY3Rpb24oKSB7IG9iai52YWwgPSAyMiB9O1xuICAgKiAgICAgYXNzZXJ0LmNoYW5nZXMoZm4sIG9iaiwgJ3ZhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBjaGFuZ2VzXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG1vZGlmaWVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3Qgb3IgZ2V0dGVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eSBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuY2hhbmdlcyA9IGZ1bmN0aW9uIChmbiwgb2JqLCBwcm9wLCBtc2cpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMyAmJiB0eXBlb2Ygb2JqID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBtc2cgPSBwcm9wO1xuICAgICAgcHJvcCA9IG51bGw7XG4gICAgfVxuXG4gICAgbmV3IEFzc2VydGlvbihmbiwgbXNnLCBhc3NlcnQuY2hhbmdlcywgdHJ1ZSkudG8uY2hhbmdlKG9iaiwgcHJvcCk7XG4gIH1cblxuICAgLyoqXG4gICAqICMjIyAuY2hhbmdlc0J5KGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBkZWx0YSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBjaGFuZ2VzIHRoZSB2YWx1ZSBvZiBhIHByb3BlcnR5IGJ5IGFuIGFtb3VudCAoZGVsdGEpLlxuICAgKlxuICAgKiAgICAgdmFyIG9iaiA9IHsgdmFsOiAxMCB9O1xuICAgKiAgICAgdmFyIGZuID0gZnVuY3Rpb24oKSB7IG9iai52YWwgKz0gMiB9O1xuICAgKiAgICAgYXNzZXJ0LmNoYW5nZXNCeShmbiwgb2JqLCAndmFsJywgMik7XG4gICAqXG4gICAqIEBuYW1lIGNoYW5nZXNCeVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBjaGFuZ2UgYW1vdW50IChkZWx0YSlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuY2hhbmdlc0J5ID0gZnVuY3Rpb24gKGZuLCBvYmosIHByb3AsIGRlbHRhLCBtc2cpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gNCAmJiB0eXBlb2Ygb2JqID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICB2YXIgdG1wTXNnID0gZGVsdGE7XG4gICAgICBkZWx0YSA9IHByb3A7XG4gICAgICBtc2cgPSB0bXBNc2c7XG4gICAgfSBlbHNlIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAzKSB7XG4gICAgICBkZWx0YSA9IHByb3A7XG4gICAgICBwcm9wID0gbnVsbDtcbiAgICB9XG5cbiAgICBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC5jaGFuZ2VzQnksIHRydWUpXG4gICAgICAudG8uY2hhbmdlKG9iaiwgcHJvcCkuYnkoZGVsdGEpO1xuICB9XG5cbiAgIC8qKlxuICAgKiAjIyMgLmRvZXNOb3RDaGFuZ2UoZnVuY3Rpb24sIG9iamVjdCwgcHJvcGVydHksIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGEgZnVuY3Rpb24gZG9lcyBub3QgY2hhbmdlIHRoZSB2YWx1ZSBvZiBhIHByb3BlcnR5LlxuICAgKlxuICAgKiAgICAgdmFyIG9iaiA9IHsgdmFsOiAxMCB9O1xuICAgKiAgICAgdmFyIGZuID0gZnVuY3Rpb24oKSB7IGNvbnNvbGUubG9nKCdmb28nKTsgfTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90Q2hhbmdlKGZuLCBvYmosICd2YWwnKTtcbiAgICpcbiAgICogQG5hbWUgZG9lc05vdENoYW5nZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRvZXNOb3RDaGFuZ2UgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgbXNnID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC5kb2VzTm90Q2hhbmdlLCB0cnVlKVxuICAgICAgLnRvLm5vdC5jaGFuZ2Uob2JqLCBwcm9wKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmNoYW5nZXNCdXROb3RCeShmdW5jdGlvbiwgb2JqZWN0LCBwcm9wZXJ0eSwgZGVsdGEsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGEgZnVuY3Rpb24gZG9lcyBub3QgY2hhbmdlIHRoZSB2YWx1ZSBvZiBhIHByb3BlcnR5IG9yIG9mIGEgZnVuY3Rpb24ncyByZXR1cm4gdmFsdWUgYnkgYW4gYW1vdW50IChkZWx0YSlcbiAgICpcbiAgICogICAgIHZhciBvYmogPSB7IHZhbDogMTAgfTtcbiAgICogICAgIHZhciBmbiA9IGZ1bmN0aW9uKCkgeyBvYmoudmFsICs9IDEwIH07XG4gICAqICAgICBhc3NlcnQuY2hhbmdlc0J1dE5vdEJ5KGZuLCBvYmosICd2YWwnLCA1KTtcbiAgICpcbiAgICogQG5hbWUgY2hhbmdlc0J1dE5vdEJ5XG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG1vZGlmaWVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3Qgb3IgZ2V0dGVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eSBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtOdW1iZXJ9IGNoYW5nZSBhbW91bnQgKGRlbHRhKVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5jaGFuZ2VzQnV0Tm90QnkgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgZGVsdGEsIG1zZykge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSA0ICYmIHR5cGVvZiBvYmogPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHZhciB0bXBNc2cgPSBkZWx0YTtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIG1zZyA9IHRtcE1zZztcbiAgICB9IGVsc2UgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMpIHtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmNoYW5nZXNCdXROb3RCeSwgdHJ1ZSlcbiAgICAgIC50by5jaGFuZ2Uob2JqLCBwcm9wKS5idXQubm90LmJ5KGRlbHRhKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmluY3JlYXNlcyhmdW5jdGlvbiwgb2JqZWN0LCBwcm9wZXJ0eSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBpbmNyZWFzZXMgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eS5cbiAgICpcbiAgICogICAgIHZhciBvYmogPSB7IHZhbDogMTAgfTtcbiAgICogICAgIHZhciBmbiA9IGZ1bmN0aW9uKCkgeyBvYmoudmFsID0gMTMgfTtcbiAgICogICAgIGFzc2VydC5pbmNyZWFzZXMoZm4sIG9iaiwgJ3ZhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBpbmNyZWFzZXNcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbW9kaWZpZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBvciBnZXR0ZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5IG5hbWUgX29wdGlvbmFsX1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5pbmNyZWFzZXMgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgbXNnID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC5pbmNyZWFzZXMsIHRydWUpXG4gICAgICAudG8uaW5jcmVhc2Uob2JqLCBwcm9wKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmluY3JlYXNlc0J5KGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBkZWx0YSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBpbmNyZWFzZXMgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eSBvciBhIGZ1bmN0aW9uJ3MgcmV0dXJuIHZhbHVlIGJ5IGFuIGFtb3VudCAoZGVsdGEpLlxuICAgKlxuICAgKiAgICAgdmFyIG9iaiA9IHsgdmFsOiAxMCB9O1xuICAgKiAgICAgdmFyIGZuID0gZnVuY3Rpb24oKSB7IG9iai52YWwgKz0gMTAgfTtcbiAgICogICAgIGFzc2VydC5pbmNyZWFzZXNCeShmbiwgb2JqLCAndmFsJywgMTApO1xuICAgKlxuICAgKiBAbmFtZSBpbmNyZWFzZXNCeVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBjaGFuZ2UgYW1vdW50IChkZWx0YSlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaW5jcmVhc2VzQnkgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgZGVsdGEsIG1zZykge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSA0ICYmIHR5cGVvZiBvYmogPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHZhciB0bXBNc2cgPSBkZWx0YTtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIG1zZyA9IHRtcE1zZztcbiAgICB9IGVsc2UgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMpIHtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmluY3JlYXNlc0J5LCB0cnVlKVxuICAgICAgLnRvLmluY3JlYXNlKG9iaiwgcHJvcCkuYnkoZGVsdGEpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAuZG9lc05vdEluY3JlYXNlKGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBhIGZ1bmN0aW9uIGRvZXMgbm90IGluY3JlYXNlIGEgbnVtZXJpYyBvYmplY3QgcHJvcGVydHkuXG4gICAqXG4gICAqICAgICB2YXIgb2JqID0geyB2YWw6IDEwIH07XG4gICAqICAgICB2YXIgZm4gPSBmdW5jdGlvbigpIHsgb2JqLnZhbCA9IDggfTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90SW5jcmVhc2UoZm4sIG9iaiwgJ3ZhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBkb2VzTm90SW5jcmVhc2VcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbW9kaWZpZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBvciBnZXR0ZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5IG5hbWUgX29wdGlvbmFsX1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kb2VzTm90SW5jcmVhc2UgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgbXNnID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC5kb2VzTm90SW5jcmVhc2UsIHRydWUpXG4gICAgICAudG8ubm90LmluY3JlYXNlKG9iaiwgcHJvcCk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5pbmNyZWFzZXNCdXROb3RCeShmdW5jdGlvbiwgb2JqZWN0LCBwcm9wZXJ0eSwgZGVsdGEsIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGEgZnVuY3Rpb24gZG9lcyBub3QgaW5jcmVhc2UgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eSBvciBmdW5jdGlvbidzIHJldHVybiB2YWx1ZSBieSBhbiBhbW91bnQgKGRlbHRhKS5cbiAgICpcbiAgICogICAgIHZhciBvYmogPSB7IHZhbDogMTAgfTtcbiAgICogICAgIHZhciBmbiA9IGZ1bmN0aW9uKCkgeyBvYmoudmFsID0gMTUgfTtcbiAgICogICAgIGFzc2VydC5pbmNyZWFzZXNCdXROb3RCeShmbiwgb2JqLCAndmFsJywgMTApO1xuICAgKlxuICAgKiBAbmFtZSBpbmNyZWFzZXNCdXROb3RCeVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBjaGFuZ2UgYW1vdW50IChkZWx0YSlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaW5jcmVhc2VzQnV0Tm90QnkgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgZGVsdGEsIG1zZykge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSA0ICYmIHR5cGVvZiBvYmogPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHZhciB0bXBNc2cgPSBkZWx0YTtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIG1zZyA9IHRtcE1zZztcbiAgICB9IGVsc2UgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMpIHtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmluY3JlYXNlc0J1dE5vdEJ5LCB0cnVlKVxuICAgICAgLnRvLmluY3JlYXNlKG9iaiwgcHJvcCkuYnV0Lm5vdC5ieShkZWx0YSk7XG4gIH1cblxuICAvKipcbiAgICogIyMjIC5kZWNyZWFzZXMoZnVuY3Rpb24sIG9iamVjdCwgcHJvcGVydHksIFttZXNzYWdlXSlcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGEgZnVuY3Rpb24gZGVjcmVhc2VzIGEgbnVtZXJpYyBvYmplY3QgcHJvcGVydHkuXG4gICAqXG4gICAqICAgICB2YXIgb2JqID0geyB2YWw6IDEwIH07XG4gICAqICAgICB2YXIgZm4gPSBmdW5jdGlvbigpIHsgb2JqLnZhbCA9IDUgfTtcbiAgICogICAgIGFzc2VydC5kZWNyZWFzZXMoZm4sIG9iaiwgJ3ZhbCcpO1xuICAgKlxuICAgKiBAbmFtZSBkZWNyZWFzZXNcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gbW9kaWZpZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCBvciBnZXR0ZXIgZnVuY3Rpb25cbiAgICogQHBhcmFtIHtTdHJpbmd9IHByb3BlcnR5IG5hbWUgX29wdGlvbmFsX1xuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kZWNyZWFzZXMgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgbXNnID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIHJldHVybiBuZXcgQXNzZXJ0aW9uKGZuLCBtc2csIGFzc2VydC5kZWNyZWFzZXMsIHRydWUpXG4gICAgICAudG8uZGVjcmVhc2Uob2JqLCBwcm9wKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmRlY3JlYXNlc0J5KGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBkZWx0YSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBkZWNyZWFzZXMgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eSBvciBhIGZ1bmN0aW9uJ3MgcmV0dXJuIHZhbHVlIGJ5IGFuIGFtb3VudCAoZGVsdGEpXG4gICAqXG4gICAqICAgICB2YXIgb2JqID0geyB2YWw6IDEwIH07XG4gICAqICAgICB2YXIgZm4gPSBmdW5jdGlvbigpIHsgb2JqLnZhbCAtPSA1IH07XG4gICAqICAgICBhc3NlcnQuZGVjcmVhc2VzQnkoZm4sIG9iaiwgJ3ZhbCcsIDUpO1xuICAgKlxuICAgKiBAbmFtZSBkZWNyZWFzZXNCeVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBjaGFuZ2UgYW1vdW50IChkZWx0YSlcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuZGVjcmVhc2VzQnkgPSBmdW5jdGlvbiAoZm4sIG9iaiwgcHJvcCwgZGVsdGEsIG1zZykge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSA0ICYmIHR5cGVvZiBvYmogPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHZhciB0bXBNc2cgPSBkZWx0YTtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIG1zZyA9IHRtcE1zZztcbiAgICB9IGVsc2UgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDMpIHtcbiAgICAgIGRlbHRhID0gcHJvcDtcbiAgICAgIHByb3AgPSBudWxsO1xuICAgIH1cblxuICAgIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmRlY3JlYXNlc0J5LCB0cnVlKVxuICAgICAgLnRvLmRlY3JlYXNlKG9iaiwgcHJvcCkuYnkoZGVsdGEpO1xuICB9XG5cbiAgLyoqXG4gICAqICMjIyAuZG9lc05vdERlY3JlYXNlKGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBbbWVzc2FnZV0pXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBhIGZ1bmN0aW9uIGRvZXMgbm90IGRlY3JlYXNlcyBhIG51bWVyaWMgb2JqZWN0IHByb3BlcnR5LlxuICAgKlxuICAgKiAgICAgdmFyIG9iaiA9IHsgdmFsOiAxMCB9O1xuICAgKiAgICAgdmFyIGZuID0gZnVuY3Rpb24oKSB7IG9iai52YWwgPSAxNSB9O1xuICAgKiAgICAgYXNzZXJ0LmRvZXNOb3REZWNyZWFzZShmbiwgb2JqLCAndmFsJyk7XG4gICAqXG4gICAqIEBuYW1lIGRvZXNOb3REZWNyZWFzZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBtb2RpZmllciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IG9yIGdldHRlciBmdW5jdGlvblxuICAgKiBAcGFyYW0ge1N0cmluZ30gcHJvcGVydHkgbmFtZSBfb3B0aW9uYWxfXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmRvZXNOb3REZWNyZWFzZSA9IGZ1bmN0aW9uIChmbiwgb2JqLCBwcm9wLCBtc2cpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMyAmJiB0eXBlb2Ygb2JqID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBtc2cgPSBwcm9wO1xuICAgICAgcHJvcCA9IG51bGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmRvZXNOb3REZWNyZWFzZSwgdHJ1ZSlcbiAgICAgIC50by5ub3QuZGVjcmVhc2Uob2JqLCBwcm9wKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmRvZXNOb3REZWNyZWFzZUJ5KGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBkZWx0YSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBkb2VzIG5vdCBkZWNyZWFzZXMgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eSBvciBhIGZ1bmN0aW9uJ3MgcmV0dXJuIHZhbHVlIGJ5IGFuIGFtb3VudCAoZGVsdGEpXG4gICAqXG4gICAqICAgICB2YXIgb2JqID0geyB2YWw6IDEwIH07XG4gICAqICAgICB2YXIgZm4gPSBmdW5jdGlvbigpIHsgb2JqLnZhbCA9IDUgfTtcbiAgICogICAgIGFzc2VydC5kb2VzTm90RGVjcmVhc2VCeShmbiwgb2JqLCAndmFsJywgMSk7XG4gICAqXG4gICAqIEBuYW1lIGRvZXNOb3REZWNyZWFzZUJ5XG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG1vZGlmaWVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3Qgb3IgZ2V0dGVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eSBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtOdW1iZXJ9IGNoYW5nZSBhbW91bnQgKGRlbHRhKVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kb2VzTm90RGVjcmVhc2VCeSA9IGZ1bmN0aW9uIChmbiwgb2JqLCBwcm9wLCBkZWx0YSwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDQgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdmFyIHRtcE1zZyA9IGRlbHRhO1xuICAgICAgZGVsdGEgPSBwcm9wO1xuICAgICAgbXNnID0gdG1wTXNnO1xuICAgIH0gZWxzZSBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMykge1xuICAgICAgZGVsdGEgPSBwcm9wO1xuICAgICAgcHJvcCA9IG51bGw7XG4gICAgfVxuXG4gICAgcmV0dXJuIG5ldyBBc3NlcnRpb24oZm4sIG1zZywgYXNzZXJ0LmRvZXNOb3REZWNyZWFzZUJ5LCB0cnVlKVxuICAgICAgLnRvLm5vdC5kZWNyZWFzZShvYmosIHByb3ApLmJ5KGRlbHRhKTtcbiAgfVxuXG4gIC8qKlxuICAgKiAjIyMgLmRlY3JlYXNlc0J1dE5vdEJ5KGZ1bmN0aW9uLCBvYmplY3QsIHByb3BlcnR5LCBkZWx0YSwgW21lc3NhZ2VdKVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYSBmdW5jdGlvbiBkb2VzIG5vdCBkZWNyZWFzZXMgYSBudW1lcmljIG9iamVjdCBwcm9wZXJ0eSBvciBhIGZ1bmN0aW9uJ3MgcmV0dXJuIHZhbHVlIGJ5IGFuIGFtb3VudCAoZGVsdGEpXG4gICAqXG4gICAqICAgICB2YXIgb2JqID0geyB2YWw6IDEwIH07XG4gICAqICAgICB2YXIgZm4gPSBmdW5jdGlvbigpIHsgb2JqLnZhbCA9IDUgfTtcbiAgICogICAgIGFzc2VydC5kZWNyZWFzZXNCdXROb3RCeShmbiwgb2JqLCAndmFsJywgMSk7XG4gICAqXG4gICAqIEBuYW1lIGRlY3JlYXNlc0J1dE5vdEJ5XG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG1vZGlmaWVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3Qgb3IgZ2V0dGVyIGZ1bmN0aW9uXG4gICAqIEBwYXJhbSB7U3RyaW5nfSBwcm9wZXJ0eSBuYW1lIF9vcHRpb25hbF9cbiAgICogQHBhcmFtIHtOdW1iZXJ9IGNoYW5nZSBhbW91bnQgKGRlbHRhKVxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBfb3B0aW9uYWxfXG4gICAqIEBuYW1lc3BhY2UgQXNzZXJ0XG4gICAqIEBhcGkgcHVibGljXG4gICAqL1xuXG4gIGFzc2VydC5kZWNyZWFzZXNCdXROb3RCeSA9IGZ1bmN0aW9uIChmbiwgb2JqLCBwcm9wLCBkZWx0YSwgbXNnKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDQgJiYgdHlwZW9mIG9iaiA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdmFyIHRtcE1zZyA9IGRlbHRhO1xuICAgICAgZGVsdGEgPSBwcm9wO1xuICAgICAgbXNnID0gdG1wTXNnO1xuICAgIH0gZWxzZSBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMykge1xuICAgICAgZGVsdGEgPSBwcm9wO1xuICAgICAgcHJvcCA9IG51bGw7XG4gICAgfVxuXG4gICAgbmV3IEFzc2VydGlvbihmbiwgbXNnLCBhc3NlcnQuZGVjcmVhc2VzQnV0Tm90QnksIHRydWUpXG4gICAgICAudG8uZGVjcmVhc2Uob2JqLCBwcm9wKS5idXQubm90LmJ5KGRlbHRhKTtcbiAgfVxuXG4gIC8qIVxuICAgKiAjIyMgLmlmRXJyb3Iob2JqZWN0KVxuICAgKlxuICAgKiBBc3NlcnRzIGlmIHZhbHVlIGlzIG5vdCBhIGZhbHNlIHZhbHVlLCBhbmQgdGhyb3dzIGlmIGl0IGlzIGEgdHJ1ZSB2YWx1ZS5cbiAgICogVGhpcyBpcyBhZGRlZCB0byBhbGxvdyBmb3IgY2hhaSB0byBiZSBhIGRyb3AtaW4gcmVwbGFjZW1lbnQgZm9yIE5vZGUnc1xuICAgKiBhc3NlcnQgY2xhc3MuXG4gICAqXG4gICAqICAgICB2YXIgZXJyID0gbmV3IEVycm9yKCdJIGFtIGEgY3VzdG9tIGVycm9yJyk7XG4gICAqICAgICBhc3NlcnQuaWZFcnJvcihlcnIpOyAvLyBSZXRocm93cyBlcnIhXG4gICAqXG4gICAqIEBuYW1lIGlmRXJyb3JcbiAgICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaWZFcnJvciA9IGZ1bmN0aW9uICh2YWwpIHtcbiAgICBpZiAodmFsKSB7XG4gICAgICB0aHJvdyh2YWwpO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc0V4dGVuc2libGUob2JqZWN0KVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgZXh0ZW5zaWJsZSAoY2FuIGhhdmUgbmV3IHByb3BlcnRpZXMgYWRkZWQgdG8gaXQpLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzRXh0ZW5zaWJsZSh7fSk7XG4gICAqXG4gICAqIEBuYW1lIGlzRXh0ZW5zaWJsZVxuICAgKiBAYWxpYXMgZXh0ZW5zaWJsZVxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzRXh0ZW5zaWJsZSA9IGZ1bmN0aW9uIChvYmosIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24ob2JqLCBtc2csIGFzc2VydC5pc0V4dGVuc2libGUsIHRydWUpLnRvLmJlLmV4dGVuc2libGU7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RFeHRlbnNpYmxlKG9iamVjdClcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IGBvYmplY3RgIGlzIF9ub3RfIGV4dGVuc2libGUuXG4gICAqXG4gICAqICAgICB2YXIgbm9uRXh0ZW5zaWJsZU9iamVjdCA9IE9iamVjdC5wcmV2ZW50RXh0ZW5zaW9ucyh7fSk7XG4gICAqICAgICB2YXIgc2VhbGVkT2JqZWN0ID0gT2JqZWN0LnNlYWwoe30pO1xuICAgKiAgICAgdmFyIGZyb3plbk9iamVjdCA9IE9iamVjdC5mcmVlemUoe30pO1xuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzTm90RXh0ZW5zaWJsZShub25FeHRlbnNpYmxlT2JqZWN0KTtcbiAgICogICAgIGFzc2VydC5pc05vdEV4dGVuc2libGUoc2VhbGVkT2JqZWN0KTtcbiAgICogICAgIGFzc2VydC5pc05vdEV4dGVuc2libGUoZnJvemVuT2JqZWN0KTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3RFeHRlbnNpYmxlXG4gICAqIEBhbGlhcyBub3RFeHRlbnNpYmxlXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOb3RFeHRlbnNpYmxlID0gZnVuY3Rpb24gKG9iaiwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmlzTm90RXh0ZW5zaWJsZSwgdHJ1ZSkudG8ubm90LmJlLmV4dGVuc2libGU7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNTZWFsZWQob2JqZWN0KVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgc2VhbGVkIChjYW5ub3QgaGF2ZSBuZXcgcHJvcGVydGllcyBhZGRlZCB0byBpdFxuICAgKiBhbmQgaXRzIGV4aXN0aW5nIHByb3BlcnRpZXMgY2Fubm90IGJlIHJlbW92ZWQpLlxuICAgKlxuICAgKiAgICAgdmFyIHNlYWxlZE9iamVjdCA9IE9iamVjdC5zZWFsKHt9KTtcbiAgICogICAgIHZhciBmcm96ZW5PYmplY3QgPSBPYmplY3Quc2VhbCh7fSk7XG4gICAqXG4gICAqICAgICBhc3NlcnQuaXNTZWFsZWQoc2VhbGVkT2JqZWN0KTtcbiAgICogICAgIGFzc2VydC5pc1NlYWxlZChmcm96ZW5PYmplY3QpO1xuICAgKlxuICAgKiBAbmFtZSBpc1NlYWxlZFxuICAgKiBAYWxpYXMgc2VhbGVkXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNTZWFsZWQgPSBmdW5jdGlvbiAob2JqLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuaXNTZWFsZWQsIHRydWUpLnRvLmJlLnNlYWxlZDtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc05vdFNlYWxlZChvYmplY3QpXG4gICAqXG4gICAqIEFzc2VydHMgdGhhdCBgb2JqZWN0YCBpcyBfbm90XyBzZWFsZWQuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaXNOb3RTZWFsZWQoe30pO1xuICAgKlxuICAgKiBAbmFtZSBpc05vdFNlYWxlZFxuICAgKiBAYWxpYXMgbm90U2VhbGVkXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3RcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOb3RTZWFsZWQgPSBmdW5jdGlvbiAob2JqLCBtc2cpIHtcbiAgICBuZXcgQXNzZXJ0aW9uKG9iaiwgbXNnLCBhc3NlcnQuaXNOb3RTZWFsZWQsIHRydWUpLnRvLm5vdC5iZS5zZWFsZWQ7XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNGcm96ZW4ob2JqZWN0KVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgZnJvemVuIChjYW5ub3QgaGF2ZSBuZXcgcHJvcGVydGllcyBhZGRlZCB0byBpdFxuICAgKiBhbmQgaXRzIGV4aXN0aW5nIHByb3BlcnRpZXMgY2Fubm90IGJlIG1vZGlmaWVkKS5cbiAgICpcbiAgICogICAgIHZhciBmcm96ZW5PYmplY3QgPSBPYmplY3QuZnJlZXplKHt9KTtcbiAgICogICAgIGFzc2VydC5mcm96ZW4oZnJvemVuT2JqZWN0KTtcbiAgICpcbiAgICogQG5hbWUgaXNGcm96ZW5cbiAgICogQGFsaWFzIGZyb3plblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzRnJvemVuID0gZnVuY3Rpb24gKG9iaiwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmlzRnJvemVuLCB0cnVlKS50by5iZS5mcm96ZW47XG4gIH07XG5cbiAgLyoqXG4gICAqICMjIyAuaXNOb3RGcm96ZW4ob2JqZWN0KVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgYG9iamVjdGAgaXMgX25vdF8gZnJvemVuLlxuICAgKlxuICAgKiAgICAgYXNzZXJ0LmlzTm90RnJvemVuKHt9KTtcbiAgICpcbiAgICogQG5hbWUgaXNOb3RGcm96ZW5cbiAgICogQGFsaWFzIG5vdEZyb3plblxuICAgKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzTm90RnJvemVuID0gZnVuY3Rpb24gKG9iaiwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbihvYmosIG1zZywgYXNzZXJ0LmlzTm90RnJvemVuLCB0cnVlKS50by5ub3QuYmUuZnJvemVuO1xuICB9O1xuXG4gIC8qKlxuICAgKiAjIyMgLmlzRW1wdHkodGFyZ2V0KVxuICAgKlxuICAgKiBBc3NlcnRzIHRoYXQgdGhlIHRhcmdldCBkb2VzIG5vdCBjb250YWluIGFueSB2YWx1ZXMuXG4gICAqIEZvciBhcnJheXMgYW5kIHN0cmluZ3MsIGl0IGNoZWNrcyB0aGUgYGxlbmd0aGAgcHJvcGVydHkuXG4gICAqIEZvciBgTWFwYCBhbmQgYFNldGAgaW5zdGFuY2VzLCBpdCBjaGVja3MgdGhlIGBzaXplYCBwcm9wZXJ0eS5cbiAgICogRm9yIG5vbi1mdW5jdGlvbiBvYmplY3RzLCBpdCBnZXRzIHRoZSBjb3VudCBvZiBvd25cbiAgICogZW51bWVyYWJsZSBzdHJpbmcga2V5cy5cbiAgICpcbiAgICogICAgIGFzc2VydC5pc0VtcHR5KFtdKTtcbiAgICogICAgIGFzc2VydC5pc0VtcHR5KCcnKTtcbiAgICogICAgIGFzc2VydC5pc0VtcHR5KG5ldyBNYXApO1xuICAgKiAgICAgYXNzZXJ0LmlzRW1wdHkoe30pO1xuICAgKlxuICAgKiBAbmFtZSBpc0VtcHR5XG4gICAqIEBhbGlhcyBlbXB0eVxuICAgKiBAcGFyYW0ge09iamVjdHxBcnJheXxTdHJpbmd8TWFwfFNldH0gdGFyZ2V0XG4gICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlIF9vcHRpb25hbF9cbiAgICogQG5hbWVzcGFjZSBBc3NlcnRcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgYXNzZXJ0LmlzRW1wdHkgPSBmdW5jdGlvbih2YWwsIG1zZykge1xuICAgIG5ldyBBc3NlcnRpb24odmFsLCBtc2csIGFzc2VydC5pc0VtcHR5LCB0cnVlKS50by5iZS5lbXB0eTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5pc05vdEVtcHR5KHRhcmdldClcbiAgICpcbiAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgY29udGFpbnMgdmFsdWVzLlxuICAgKiBGb3IgYXJyYXlzIGFuZCBzdHJpbmdzLCBpdCBjaGVja3MgdGhlIGBsZW5ndGhgIHByb3BlcnR5LlxuICAgKiBGb3IgYE1hcGAgYW5kIGBTZXRgIGluc3RhbmNlcywgaXQgY2hlY2tzIHRoZSBgc2l6ZWAgcHJvcGVydHkuXG4gICAqIEZvciBub24tZnVuY3Rpb24gb2JqZWN0cywgaXQgZ2V0cyB0aGUgY291bnQgb2Ygb3duXG4gICAqIGVudW1lcmFibGUgc3RyaW5nIGtleXMuXG4gICAqXG4gICAqICAgICBhc3NlcnQuaXNOb3RFbXB0eShbMSwgMl0pO1xuICAgKiAgICAgYXNzZXJ0LmlzTm90RW1wdHkoJzM0Jyk7XG4gICAqICAgICBhc3NlcnQuaXNOb3RFbXB0eShuZXcgU2V0KFs1LCA2XSkpO1xuICAgKiAgICAgYXNzZXJ0LmlzTm90RW1wdHkoeyBrZXk6IDcgfSk7XG4gICAqXG4gICAqIEBuYW1lIGlzTm90RW1wdHlcbiAgICogQGFsaWFzIG5vdEVtcHR5XG4gICAqIEBwYXJhbSB7T2JqZWN0fEFycmF5fFN0cmluZ3xNYXB8U2V0fSB0YXJnZXRcbiAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2UgX29wdGlvbmFsX1xuICAgKiBAbmFtZXNwYWNlIEFzc2VydFxuICAgKiBAYXBpIHB1YmxpY1xuICAgKi9cblxuICBhc3NlcnQuaXNOb3RFbXB0eSA9IGZ1bmN0aW9uKHZhbCwgbXNnKSB7XG4gICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZywgYXNzZXJ0LmlzTm90RW1wdHksIHRydWUpLnRvLm5vdC5iZS5lbXB0eTtcbiAgfTtcblxuICAvKiFcbiAgICogQWxpYXNlcy5cbiAgICovXG5cbiAgKGZ1bmN0aW9uIGFsaWFzKG5hbWUsIGFzKXtcbiAgICBhc3NlcnRbYXNdID0gYXNzZXJ0W25hbWVdO1xuICAgIHJldHVybiBhbGlhcztcbiAgfSlcbiAgKCdpc09rJywgJ29rJylcbiAgKCdpc05vdE9rJywgJ25vdE9rJylcbiAgKCd0aHJvd3MnLCAndGhyb3cnKVxuICAoJ3Rocm93cycsICdUaHJvdycpXG4gICgnaXNFeHRlbnNpYmxlJywgJ2V4dGVuc2libGUnKVxuICAoJ2lzTm90RXh0ZW5zaWJsZScsICdub3RFeHRlbnNpYmxlJylcbiAgKCdpc1NlYWxlZCcsICdzZWFsZWQnKVxuICAoJ2lzTm90U2VhbGVkJywgJ25vdFNlYWxlZCcpXG4gICgnaXNGcm96ZW4nLCAnZnJvemVuJylcbiAgKCdpc05vdEZyb3plbicsICdub3RGcm96ZW4nKVxuICAoJ2lzRW1wdHknLCAnZW1wdHknKVxuICAoJ2lzTm90RW1wdHknLCAnbm90RW1wdHknKTtcbn07XG4iLCIvKiFcbiAqIGNoYWlcbiAqIENvcHlyaWdodChjKSAyMDExLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChjaGFpLCB1dGlsKSB7XG4gIGNoYWkuZXhwZWN0ID0gZnVuY3Rpb24gKHZhbCwgbWVzc2FnZSkge1xuICAgIHJldHVybiBuZXcgY2hhaS5Bc3NlcnRpb24odmFsLCBtZXNzYWdlKTtcbiAgfTtcblxuICAvKipcbiAgICogIyMjIC5mYWlsKFttZXNzYWdlXSlcbiAgICogIyMjIC5mYWlsKGFjdHVhbCwgZXhwZWN0ZWQsIFttZXNzYWdlXSwgW29wZXJhdG9yXSlcbiAgICpcbiAgICogVGhyb3cgYSBmYWlsdXJlLlxuICAgKlxuICAgKiAgICAgZXhwZWN0LmZhaWwoKTtcbiAgICogICAgIGV4cGVjdC5mYWlsKFwiY3VzdG9tIGVycm9yIG1lc3NhZ2VcIik7XG4gICAqICAgICBleHBlY3QuZmFpbCgxLCAyKTtcbiAgICogICAgIGV4cGVjdC5mYWlsKDEsIDIsIFwiY3VzdG9tIGVycm9yIG1lc3NhZ2VcIik7XG4gICAqICAgICBleHBlY3QuZmFpbCgxLCAyLCBcImN1c3RvbSBlcnJvciBtZXNzYWdlXCIsIFwiPlwiKTtcbiAgICogICAgIGV4cGVjdC5mYWlsKDEsIDIsIHVuZGVmaW5lZCwgXCI+XCIpO1xuICAgKlxuICAgKiBAbmFtZSBmYWlsXG4gICAqIEBwYXJhbSB7TWl4ZWR9IGFjdHVhbFxuICAgKiBAcGFyYW0ge01peGVkfSBleHBlY3RlZFxuICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gb3BlcmF0b3JcbiAgICogQG5hbWVzcGFjZSBCRERcbiAgICogQGFwaSBwdWJsaWNcbiAgICovXG5cbiAgY2hhaS5leHBlY3QuZmFpbCA9IGZ1bmN0aW9uIChhY3R1YWwsIGV4cGVjdGVkLCBtZXNzYWdlLCBvcGVyYXRvcikge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoIDwgMikge1xuICAgICAgICBtZXNzYWdlID0gYWN0dWFsO1xuICAgICAgICBhY3R1YWwgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgbWVzc2FnZSA9IG1lc3NhZ2UgfHwgJ2V4cGVjdC5mYWlsKCknO1xuICAgIHRocm93IG5ldyBjaGFpLkFzc2VydGlvbkVycm9yKG1lc3NhZ2UsIHtcbiAgICAgICAgYWN0dWFsOiBhY3R1YWxcbiAgICAgICwgZXhwZWN0ZWQ6IGV4cGVjdGVkXG4gICAgICAsIG9wZXJhdG9yOiBvcGVyYXRvclxuICAgIH0sIGNoYWkuZXhwZWN0LmZhaWwpO1xuICB9O1xufTtcbiIsIi8qIVxuICogY2hhaVxuICogQ29weXJpZ2h0KGMpIDIwMTEtMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNoYWksIHV0aWwpIHtcbiAgdmFyIEFzc2VydGlvbiA9IGNoYWkuQXNzZXJ0aW9uO1xuXG4gIGZ1bmN0aW9uIGxvYWRTaG91bGQgKCkge1xuICAgIC8vIGV4cGxpY2l0bHkgZGVmaW5lIHRoaXMgbWV0aG9kIGFzIGZ1bmN0aW9uIGFzIHRvIGhhdmUgaXQncyBuYW1lIHRvIGluY2x1ZGUgYXMgYHNzZmlgXG4gICAgZnVuY3Rpb24gc2hvdWxkR2V0dGVyKCkge1xuICAgICAgaWYgKHRoaXMgaW5zdGFuY2VvZiBTdHJpbmdcbiAgICAgICAgICB8fCB0aGlzIGluc3RhbmNlb2YgTnVtYmVyXG4gICAgICAgICAgfHwgdGhpcyBpbnN0YW5jZW9mIEJvb2xlYW5cbiAgICAgICAgICB8fCB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmIHRoaXMgaW5zdGFuY2VvZiBTeW1ib2xcbiAgICAgICAgICB8fCB0eXBlb2YgQmlnSW50ID09PSAnZnVuY3Rpb24nICYmIHRoaXMgaW5zdGFuY2VvZiBCaWdJbnQpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBBc3NlcnRpb24odGhpcy52YWx1ZU9mKCksIG51bGwsIHNob3VsZEdldHRlcik7XG4gICAgICB9XG4gICAgICByZXR1cm4gbmV3IEFzc2VydGlvbih0aGlzLCBudWxsLCBzaG91bGRHZXR0ZXIpO1xuICAgIH1cbiAgICBmdW5jdGlvbiBzaG91bGRTZXR0ZXIodmFsdWUpIHtcbiAgICAgIC8vIFNlZSBodHRwczovL2dpdGh1Yi5jb20vY2hhaWpzL2NoYWkvaXNzdWVzLzg2OiB0aGlzIG1ha2VzXG4gICAgICAvLyBgd2hhdGV2ZXIuc2hvdWxkID0gc29tZVZhbHVlYCBhY3R1YWxseSBzZXQgYHNvbWVWYWx1ZWAsIHdoaWNoIGlzXG4gICAgICAvLyBlc3BlY2lhbGx5IHVzZWZ1bCBmb3IgYGdsb2JhbC5zaG91bGQgPSByZXF1aXJlKCdjaGFpJykuc2hvdWxkKClgLlxuICAgICAgLy9cbiAgICAgIC8vIE5vdGUgdGhhdCB3ZSBoYXZlIHRvIHVzZSBbW0RlZmluZVByb3BlcnR5XV0gaW5zdGVhZCBvZiBbW1B1dF1dXG4gICAgICAvLyBzaW5jZSBvdGhlcndpc2Ugd2Ugd291bGQgdHJpZ2dlciB0aGlzIHZlcnkgc2V0dGVyIVxuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRoaXMsICdzaG91bGQnLCB7XG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgICB3cml0YWJsZTogdHJ1ZVxuICAgICAgfSk7XG4gICAgfVxuICAgIC8vIG1vZGlmeSBPYmplY3QucHJvdG90eXBlIHRvIGhhdmUgYHNob3VsZGBcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoT2JqZWN0LnByb3RvdHlwZSwgJ3Nob3VsZCcsIHtcbiAgICAgIHNldDogc2hvdWxkU2V0dGVyXG4gICAgICAsIGdldDogc2hvdWxkR2V0dGVyXG4gICAgICAsIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pO1xuXG4gICAgdmFyIHNob3VsZCA9IHt9O1xuXG4gICAgLyoqXG4gICAgICogIyMjIC5mYWlsKFttZXNzYWdlXSlcbiAgICAgKiAjIyMgLmZhaWwoYWN0dWFsLCBleHBlY3RlZCwgW21lc3NhZ2VdLCBbb3BlcmF0b3JdKVxuICAgICAqXG4gICAgICogVGhyb3cgYSBmYWlsdXJlLlxuICAgICAqXG4gICAgICogICAgIHNob3VsZC5mYWlsKCk7XG4gICAgICogICAgIHNob3VsZC5mYWlsKFwiY3VzdG9tIGVycm9yIG1lc3NhZ2VcIik7XG4gICAgICogICAgIHNob3VsZC5mYWlsKDEsIDIpO1xuICAgICAqICAgICBzaG91bGQuZmFpbCgxLCAyLCBcImN1c3RvbSBlcnJvciBtZXNzYWdlXCIpO1xuICAgICAqICAgICBzaG91bGQuZmFpbCgxLCAyLCBcImN1c3RvbSBlcnJvciBtZXNzYWdlXCIsIFwiPlwiKTtcbiAgICAgKiAgICAgc2hvdWxkLmZhaWwoMSwgMiwgdW5kZWZpbmVkLCBcIj5cIik7XG4gICAgICpcbiAgICAgKlxuICAgICAqIEBuYW1lIGZhaWxcbiAgICAgKiBAcGFyYW0ge01peGVkfSBhY3R1YWxcbiAgICAgKiBAcGFyYW0ge01peGVkfSBleHBlY3RlZFxuICAgICAqIEBwYXJhbSB7U3RyaW5nfSBtZXNzYWdlXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IG9wZXJhdG9yXG4gICAgICogQG5hbWVzcGFjZSBCRERcbiAgICAgKiBAYXBpIHB1YmxpY1xuICAgICAqL1xuXG4gICAgc2hvdWxkLmZhaWwgPSBmdW5jdGlvbiAoYWN0dWFsLCBleHBlY3RlZCwgbWVzc2FnZSwgb3BlcmF0b3IpIHtcbiAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoIDwgMikge1xuICAgICAgICAgIG1lc3NhZ2UgPSBhY3R1YWw7XG4gICAgICAgICAgYWN0dWFsID0gdW5kZWZpbmVkO1xuICAgICAgfVxuXG4gICAgICBtZXNzYWdlID0gbWVzc2FnZSB8fCAnc2hvdWxkLmZhaWwoKSc7XG4gICAgICB0aHJvdyBuZXcgY2hhaS5Bc3NlcnRpb25FcnJvcihtZXNzYWdlLCB7XG4gICAgICAgICAgYWN0dWFsOiBhY3R1YWxcbiAgICAgICAgLCBleHBlY3RlZDogZXhwZWN0ZWRcbiAgICAgICAgLCBvcGVyYXRvcjogb3BlcmF0b3JcbiAgICAgIH0sIHNob3VsZC5mYWlsKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogIyMjIC5lcXVhbChhY3R1YWwsIGV4cGVjdGVkLCBbbWVzc2FnZV0pXG4gICAgICpcbiAgICAgKiBBc3NlcnRzIG5vbi1zdHJpY3QgZXF1YWxpdHkgKGA9PWApIG9mIGBhY3R1YWxgIGFuZCBgZXhwZWN0ZWRgLlxuICAgICAqXG4gICAgICogICAgIHNob3VsZC5lcXVhbCgzLCAnMycsICc9PSBjb2VyY2VzIHZhbHVlcyB0byBzdHJpbmdzJyk7XG4gICAgICpcbiAgICAgKiBAbmFtZSBlcXVhbFxuICAgICAqIEBwYXJhbSB7TWl4ZWR9IGFjdHVhbFxuICAgICAqIEBwYXJhbSB7TWl4ZWR9IGV4cGVjdGVkXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICAgKiBAbmFtZXNwYWNlIFNob3VsZFxuICAgICAqIEBhcGkgcHVibGljXG4gICAgICovXG5cbiAgICBzaG91bGQuZXF1YWwgPSBmdW5jdGlvbiAodmFsMSwgdmFsMiwgbXNnKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHZhbDEsIG1zZykudG8uZXF1YWwodmFsMik7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqICMjIyAudGhyb3coZnVuY3Rpb24sIFtjb25zdHJ1Y3Rvci9zdHJpbmcvcmVnZXhwXSwgW3N0cmluZy9yZWdleHBdLCBbbWVzc2FnZV0pXG4gICAgICpcbiAgICAgKiBBc3NlcnRzIHRoYXQgYGZ1bmN0aW9uYCB3aWxsIHRocm93IGFuIGVycm9yIHRoYXQgaXMgYW4gaW5zdGFuY2Ugb2ZcbiAgICAgKiBgY29uc3RydWN0b3JgLCBvciBhbHRlcm5hdGVseSB0aGF0IGl0IHdpbGwgdGhyb3cgYW4gZXJyb3Igd2l0aCBtZXNzYWdlXG4gICAgICogbWF0Y2hpbmcgYHJlZ2V4cGAuXG4gICAgICpcbiAgICAgKiAgICAgc2hvdWxkLnRocm93KGZuLCAnZnVuY3Rpb24gdGhyb3dzIGEgcmVmZXJlbmNlIGVycm9yJyk7XG4gICAgICogICAgIHNob3VsZC50aHJvdyhmbiwgL2Z1bmN0aW9uIHRocm93cyBhIHJlZmVyZW5jZSBlcnJvci8pO1xuICAgICAqICAgICBzaG91bGQudGhyb3coZm4sIFJlZmVyZW5jZUVycm9yKTtcbiAgICAgKiAgICAgc2hvdWxkLnRocm93KGZuLCBSZWZlcmVuY2VFcnJvciwgJ2Z1bmN0aW9uIHRocm93cyBhIHJlZmVyZW5jZSBlcnJvcicpO1xuICAgICAqICAgICBzaG91bGQudGhyb3coZm4sIFJlZmVyZW5jZUVycm9yLCAvZnVuY3Rpb24gdGhyb3dzIGEgcmVmZXJlbmNlIGVycm9yLyk7XG4gICAgICpcbiAgICAgKiBAbmFtZSB0aHJvd1xuICAgICAqIEBhbGlhcyBUaHJvd1xuICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmN0aW9uXG4gICAgICogQHBhcmFtIHtFcnJvckNvbnN0cnVjdG9yfSBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7UmVnRXhwfSByZWdleHBcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgICAqIEBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4vSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvRXJyb3IjRXJyb3JfdHlwZXNcbiAgICAgKiBAbmFtZXNwYWNlIFNob3VsZFxuICAgICAqIEBhcGkgcHVibGljXG4gICAgICovXG5cbiAgICBzaG91bGQuVGhyb3cgPSBmdW5jdGlvbiAoZm4sIGVycnQsIGVycnMsIG1zZykge1xuICAgICAgbmV3IEFzc2VydGlvbihmbiwgbXNnKS50by5UaHJvdyhlcnJ0LCBlcnJzKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogIyMjIC5leGlzdFxuICAgICAqXG4gICAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgbmVpdGhlciBgbnVsbGAgbm9yIGB1bmRlZmluZWRgLlxuICAgICAqXG4gICAgICogICAgIHZhciBmb28gPSAnaGknO1xuICAgICAqXG4gICAgICogICAgIHNob3VsZC5leGlzdChmb28sICdmb28gZXhpc3RzJyk7XG4gICAgICpcbiAgICAgKiBAbmFtZSBleGlzdFxuICAgICAqIEBuYW1lc3BhY2UgU2hvdWxkXG4gICAgICogQGFwaSBwdWJsaWNcbiAgICAgKi9cblxuICAgIHNob3VsZC5leGlzdCA9IGZ1bmN0aW9uICh2YWwsIG1zZykge1xuICAgICAgbmV3IEFzc2VydGlvbih2YWwsIG1zZykudG8uZXhpc3Q7XG4gICAgfVxuXG4gICAgLy8gbmVnYXRpb25cbiAgICBzaG91bGQubm90ID0ge31cblxuICAgIC8qKlxuICAgICAqICMjIyAubm90LmVxdWFsKGFjdHVhbCwgZXhwZWN0ZWQsIFttZXNzYWdlXSlcbiAgICAgKlxuICAgICAqIEFzc2VydHMgbm9uLXN0cmljdCBpbmVxdWFsaXR5IChgIT1gKSBvZiBgYWN0dWFsYCBhbmQgYGV4cGVjdGVkYC5cbiAgICAgKlxuICAgICAqICAgICBzaG91bGQubm90LmVxdWFsKDMsIDQsICd0aGVzZSBudW1iZXJzIGFyZSBub3QgZXF1YWwnKTtcbiAgICAgKlxuICAgICAqIEBuYW1lIG5vdC5lcXVhbFxuICAgICAqIEBwYXJhbSB7TWl4ZWR9IGFjdHVhbFxuICAgICAqIEBwYXJhbSB7TWl4ZWR9IGV4cGVjdGVkXG4gICAgICogQHBhcmFtIHtTdHJpbmd9IG1lc3NhZ2VcbiAgICAgKiBAbmFtZXNwYWNlIFNob3VsZFxuICAgICAqIEBhcGkgcHVibGljXG4gICAgICovXG5cbiAgICBzaG91bGQubm90LmVxdWFsID0gZnVuY3Rpb24gKHZhbDEsIHZhbDIsIG1zZykge1xuICAgICAgbmV3IEFzc2VydGlvbih2YWwxLCBtc2cpLnRvLm5vdC5lcXVhbCh2YWwyKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogIyMjIC50aHJvdyhmdW5jdGlvbiwgW2NvbnN0cnVjdG9yL3JlZ2V4cF0sIFttZXNzYWdlXSlcbiAgICAgKlxuICAgICAqIEFzc2VydHMgdGhhdCBgZnVuY3Rpb25gIHdpbGwgX25vdF8gdGhyb3cgYW4gZXJyb3IgdGhhdCBpcyBhbiBpbnN0YW5jZSBvZlxuICAgICAqIGBjb25zdHJ1Y3RvcmAsIG9yIGFsdGVybmF0ZWx5IHRoYXQgaXQgd2lsbCBub3QgdGhyb3cgYW4gZXJyb3Igd2l0aCBtZXNzYWdlXG4gICAgICogbWF0Y2hpbmcgYHJlZ2V4cGAuXG4gICAgICpcbiAgICAgKiAgICAgc2hvdWxkLm5vdC50aHJvdyhmbiwgRXJyb3IsICdmdW5jdGlvbiBkb2VzIG5vdCB0aHJvdycpO1xuICAgICAqXG4gICAgICogQG5hbWUgbm90LnRocm93XG4gICAgICogQGFsaWFzIG5vdC5UaHJvd1xuICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmN0aW9uXG4gICAgICogQHBhcmFtIHtFcnJvckNvbnN0cnVjdG9yfSBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7UmVnRXhwfSByZWdleHBcbiAgICAgKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZVxuICAgICAqIEBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4vSmF2YVNjcmlwdC9SZWZlcmVuY2UvR2xvYmFsX09iamVjdHMvRXJyb3IjRXJyb3JfdHlwZXNcbiAgICAgKiBAbmFtZXNwYWNlIFNob3VsZFxuICAgICAqIEBhcGkgcHVibGljXG4gICAgICovXG5cbiAgICBzaG91bGQubm90LlRocm93ID0gZnVuY3Rpb24gKGZuLCBlcnJ0LCBlcnJzLCBtc2cpIHtcbiAgICAgIG5ldyBBc3NlcnRpb24oZm4sIG1zZykudG8ubm90LlRocm93KGVycnQsIGVycnMpO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiAjIyMgLm5vdC5leGlzdFxuICAgICAqXG4gICAgICogQXNzZXJ0cyB0aGF0IHRoZSB0YXJnZXQgaXMgbmVpdGhlciBgbnVsbGAgbm9yIGB1bmRlZmluZWRgLlxuICAgICAqXG4gICAgICogICAgIHZhciBiYXIgPSBudWxsO1xuICAgICAqXG4gICAgICogICAgIHNob3VsZC5ub3QuZXhpc3QoYmFyLCAnYmFyIGRvZXMgbm90IGV4aXN0Jyk7XG4gICAgICpcbiAgICAgKiBAbmFtZSBub3QuZXhpc3RcbiAgICAgKiBAbmFtZXNwYWNlIFNob3VsZFxuICAgICAqIEBhcGkgcHVibGljXG4gICAgICovXG5cbiAgICBzaG91bGQubm90LmV4aXN0ID0gZnVuY3Rpb24gKHZhbCwgbXNnKSB7XG4gICAgICBuZXcgQXNzZXJ0aW9uKHZhbCwgbXNnKS50by5ub3QuZXhpc3Q7XG4gICAgfVxuXG4gICAgc2hvdWxkWyd0aHJvdyddID0gc2hvdWxkWydUaHJvdyddO1xuICAgIHNob3VsZC5ub3RbJ3Rocm93J10gPSBzaG91bGQubm90WydUaHJvdyddO1xuXG4gICAgcmV0dXJuIHNob3VsZDtcbiAgfTtcblxuICBjaGFpLnNob3VsZCA9IGxvYWRTaG91bGQ7XG4gIGNoYWkuU2hvdWxkID0gbG9hZFNob3VsZDtcbn07XG4iLCIvKiFcbiAqIENoYWkgLSBhZGRDaGFpbmluZ01ldGhvZCB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyohXG4gKiBNb2R1bGUgZGVwZW5kZW5jaWVzXG4gKi9cblxudmFyIGFkZExlbmd0aEd1YXJkID0gcmVxdWlyZSgnLi9hZGRMZW5ndGhHdWFyZCcpO1xudmFyIGNoYWkgPSByZXF1aXJlKCcuLi8uLi9jaGFpJyk7XG52YXIgZmxhZyA9IHJlcXVpcmUoJy4vZmxhZycpO1xudmFyIHByb3hpZnkgPSByZXF1aXJlKCcuL3Byb3hpZnknKTtcbnZhciB0cmFuc2ZlckZsYWdzID0gcmVxdWlyZSgnLi90cmFuc2ZlckZsYWdzJyk7XG5cbi8qIVxuICogTW9kdWxlIHZhcmlhYmxlc1xuICovXG5cbi8vIENoZWNrIHdoZXRoZXIgYE9iamVjdC5zZXRQcm90b3R5cGVPZmAgaXMgc3VwcG9ydGVkXG52YXIgY2FuU2V0UHJvdG90eXBlID0gdHlwZW9mIE9iamVjdC5zZXRQcm90b3R5cGVPZiA9PT0gJ2Z1bmN0aW9uJztcblxuLy8gV2l0aG91dCBgT2JqZWN0LnNldFByb3RvdHlwZU9mYCBzdXBwb3J0LCB0aGlzIG1vZHVsZSB3aWxsIG5lZWQgdG8gYWRkIHByb3BlcnRpZXMgdG8gYSBmdW5jdGlvbi5cbi8vIEhvd2V2ZXIsIHNvbWUgb2YgZnVuY3Rpb25zJyBvd24gcHJvcHMgYXJlIG5vdCBjb25maWd1cmFibGUgYW5kIHNob3VsZCBiZSBza2lwcGVkLlxudmFyIHRlc3RGbiA9IGZ1bmN0aW9uKCkge307XG52YXIgZXhjbHVkZU5hbWVzID0gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGVzdEZuKS5maWx0ZXIoZnVuY3Rpb24obmFtZSkge1xuICB2YXIgcHJvcERlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRlc3RGbiwgbmFtZSk7XG5cbiAgLy8gTm90ZTogUGhhbnRvbUpTIDEueCBpbmNsdWRlcyBgY2FsbGVlYCBhcyBvbmUgb2YgYHRlc3RGbmAncyBvd24gcHJvcGVydGllcyxcbiAgLy8gYnV0IHRoZW4gcmV0dXJucyBgdW5kZWZpbmVkYCBhcyB0aGUgcHJvcGVydHkgZGVzY3JpcHRvciBmb3IgYGNhbGxlZWAuIEFzIGFcbiAgLy8gd29ya2Fyb3VuZCwgd2UgcGVyZm9ybSBhbiBvdGhlcndpc2UgdW5uZWNlc3NhcnkgdHlwZS1jaGVjayBmb3IgYHByb3BEZXNjYCxcbiAgLy8gYW5kIHRoZW4gZmlsdGVyIGl0IG91dCBpZiBpdCdzIG5vdCBhbiBvYmplY3QgYXMgaXQgc2hvdWxkIGJlLlxuICBpZiAodHlwZW9mIHByb3BEZXNjICE9PSAnb2JqZWN0JylcbiAgICByZXR1cm4gdHJ1ZTtcblxuICByZXR1cm4gIXByb3BEZXNjLmNvbmZpZ3VyYWJsZTtcbn0pO1xuXG4vLyBDYWNoZSBgRnVuY3Rpb25gIHByb3BlcnRpZXNcbnZhciBjYWxsICA9IEZ1bmN0aW9uLnByb3RvdHlwZS5jYWxsLFxuICAgIGFwcGx5ID0gRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5O1xuXG4vKipcbiAqICMjIyAuYWRkQ2hhaW5hYmxlTWV0aG9kKGN0eCwgbmFtZSwgbWV0aG9kLCBjaGFpbmluZ0JlaGF2aW9yKVxuICpcbiAqIEFkZHMgYSBtZXRob2QgdG8gYW4gb2JqZWN0LCBzdWNoIHRoYXQgdGhlIG1ldGhvZCBjYW4gYWxzbyBiZSBjaGFpbmVkLlxuICpcbiAqICAgICB1dGlscy5hZGRDaGFpbmFibGVNZXRob2QoY2hhaS5Bc3NlcnRpb24ucHJvdG90eXBlLCAnZm9vJywgZnVuY3Rpb24gKHN0cikge1xuICogICAgICAgdmFyIG9iaiA9IHV0aWxzLmZsYWcodGhpcywgJ29iamVjdCcpO1xuICogICAgICAgbmV3IGNoYWkuQXNzZXJ0aW9uKG9iaikudG8uYmUuZXF1YWwoc3RyKTtcbiAqICAgICB9KTtcbiAqXG4gKiBDYW4gYWxzbyBiZSBhY2Nlc3NlZCBkaXJlY3RseSBmcm9tIGBjaGFpLkFzc2VydGlvbmAuXG4gKlxuICogICAgIGNoYWkuQXNzZXJ0aW9uLmFkZENoYWluYWJsZU1ldGhvZCgnZm9vJywgZm4sIGNoYWluaW5nQmVoYXZpb3IpO1xuICpcbiAqIFRoZSByZXN1bHQgY2FuIHRoZW4gYmUgdXNlZCBhcyBib3RoIGEgbWV0aG9kIGFzc2VydGlvbiwgZXhlY3V0aW5nIGJvdGggYG1ldGhvZGAgYW5kXG4gKiBgY2hhaW5pbmdCZWhhdmlvcmAsIG9yIGFzIGEgbGFuZ3VhZ2UgY2hhaW4sIHdoaWNoIG9ubHkgZXhlY3V0ZXMgYGNoYWluaW5nQmVoYXZpb3JgLlxuICpcbiAqICAgICBleHBlY3QoZm9vU3RyKS50by5iZS5mb28oJ2JhcicpO1xuICogICAgIGV4cGVjdChmb29TdHIpLnRvLmJlLmZvby5lcXVhbCgnZm9vJyk7XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGN0eCBvYmplY3QgdG8gd2hpY2ggdGhlIG1ldGhvZCBpcyBhZGRlZFxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgb2YgbWV0aG9kIHRvIGFkZFxuICogQHBhcmFtIHtGdW5jdGlvbn0gbWV0aG9kIGZ1bmN0aW9uIHRvIGJlIHVzZWQgZm9yIGBuYW1lYCwgd2hlbiBjYWxsZWRcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNoYWluaW5nQmVoYXZpb3IgZnVuY3Rpb24gdG8gYmUgY2FsbGVkIGV2ZXJ5IHRpbWUgdGhlIHByb3BlcnR5IGlzIGFjY2Vzc2VkXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBhZGRDaGFpbmFibGVNZXRob2RcbiAqIEBhcGkgcHVibGljXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBhZGRDaGFpbmFibGVNZXRob2QoY3R4LCBuYW1lLCBtZXRob2QsIGNoYWluaW5nQmVoYXZpb3IpIHtcbiAgaWYgKHR5cGVvZiBjaGFpbmluZ0JlaGF2aW9yICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgY2hhaW5pbmdCZWhhdmlvciA9IGZ1bmN0aW9uICgpIHsgfTtcbiAgfVxuXG4gIHZhciBjaGFpbmFibGVCZWhhdmlvciA9IHtcbiAgICAgIG1ldGhvZDogbWV0aG9kXG4gICAgLCBjaGFpbmluZ0JlaGF2aW9yOiBjaGFpbmluZ0JlaGF2aW9yXG4gIH07XG5cbiAgLy8gc2F2ZSB0aGUgbWV0aG9kcyBzbyB3ZSBjYW4gb3ZlcndyaXRlIHRoZW0gbGF0ZXIsIGlmIHdlIG5lZWQgdG8uXG4gIGlmICghY3R4Ll9fbWV0aG9kcykge1xuICAgIGN0eC5fX21ldGhvZHMgPSB7fTtcbiAgfVxuICBjdHguX19tZXRob2RzW25hbWVdID0gY2hhaW5hYmxlQmVoYXZpb3I7XG5cbiAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGN0eCwgbmFtZSxcbiAgICB7IGdldDogZnVuY3Rpb24gY2hhaW5hYmxlTWV0aG9kR2V0dGVyKCkge1xuICAgICAgICBjaGFpbmFibGVCZWhhdmlvci5jaGFpbmluZ0JlaGF2aW9yLmNhbGwodGhpcyk7XG5cbiAgICAgICAgdmFyIGNoYWluYWJsZU1ldGhvZFdyYXBwZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgLy8gU2V0dGluZyB0aGUgYHNzZmlgIGZsYWcgdG8gYGNoYWluYWJsZU1ldGhvZFdyYXBwZXJgIGNhdXNlcyB0aGlzXG4gICAgICAgICAgLy8gZnVuY3Rpb24gdG8gYmUgdGhlIHN0YXJ0aW5nIHBvaW50IGZvciByZW1vdmluZyBpbXBsZW1lbnRhdGlvblxuICAgICAgICAgIC8vIGZyYW1lcyBmcm9tIHRoZSBzdGFjayB0cmFjZSBvZiBhIGZhaWxlZCBhc3NlcnRpb24uXG4gICAgICAgICAgLy9cbiAgICAgICAgICAvLyBIb3dldmVyLCB3ZSBvbmx5IHdhbnQgdG8gdXNlIHRoaXMgZnVuY3Rpb24gYXMgdGhlIHN0YXJ0aW5nIHBvaW50IGlmXG4gICAgICAgICAgLy8gdGhlIGBsb2NrU3NmaWAgZmxhZyBpc24ndCBzZXQuXG4gICAgICAgICAgLy9cbiAgICAgICAgICAvLyBJZiB0aGUgYGxvY2tTc2ZpYCBmbGFnIGlzIHNldCwgdGhlbiB0aGlzIGFzc2VydGlvbiBpcyBiZWluZ1xuICAgICAgICAgIC8vIGludm9rZWQgZnJvbSBpbnNpZGUgb2YgYW5vdGhlciBhc3NlcnRpb24uIEluIHRoaXMgY2FzZSwgdGhlIGBzc2ZpYFxuICAgICAgICAgIC8vIGZsYWcgaGFzIGFscmVhZHkgYmVlbiBzZXQgYnkgdGhlIG91dGVyIGFzc2VydGlvbi5cbiAgICAgICAgICAvL1xuICAgICAgICAgIC8vIE5vdGUgdGhhdCBvdmVyd3JpdGluZyBhIGNoYWluYWJsZSBtZXRob2QgbWVyZWx5IHJlcGxhY2VzIHRoZSBzYXZlZFxuICAgICAgICAgIC8vIG1ldGhvZHMgaW4gYGN0eC5fX21ldGhvZHNgIGluc3RlYWQgb2YgY29tcGxldGVseSByZXBsYWNpbmcgdGhlXG4gICAgICAgICAgLy8gb3ZlcndyaXR0ZW4gYXNzZXJ0aW9uLiBUaGVyZWZvcmUsIGFuIG92ZXJ3cml0aW5nIGFzc2VydGlvbiB3b24ndFxuICAgICAgICAgIC8vIHNldCB0aGUgYHNzZmlgIG9yIGBsb2NrU3NmaWAgZmxhZ3MuXG4gICAgICAgICAgaWYgKCFmbGFnKHRoaXMsICdsb2NrU3NmaScpKSB7XG4gICAgICAgICAgICBmbGFnKHRoaXMsICdzc2ZpJywgY2hhaW5hYmxlTWV0aG9kV3JhcHBlcik7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdmFyIHJlc3VsdCA9IGNoYWluYWJsZUJlaGF2aW9yLm1ldGhvZC5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICAgIGlmIChyZXN1bHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YXIgbmV3QXNzZXJ0aW9uID0gbmV3IGNoYWkuQXNzZXJ0aW9uKCk7XG4gICAgICAgICAgdHJhbnNmZXJGbGFncyh0aGlzLCBuZXdBc3NlcnRpb24pO1xuICAgICAgICAgIHJldHVybiBuZXdBc3NlcnRpb247XG4gICAgICAgIH07XG5cbiAgICAgICAgYWRkTGVuZ3RoR3VhcmQoY2hhaW5hYmxlTWV0aG9kV3JhcHBlciwgbmFtZSwgdHJ1ZSk7XG5cbiAgICAgICAgLy8gVXNlIGBPYmplY3Quc2V0UHJvdG90eXBlT2ZgIGlmIGF2YWlsYWJsZVxuICAgICAgICBpZiAoY2FuU2V0UHJvdG90eXBlKSB7XG4gICAgICAgICAgLy8gSW5oZXJpdCBhbGwgcHJvcGVydGllcyBmcm9tIHRoZSBvYmplY3QgYnkgcmVwbGFjaW5nIHRoZSBgRnVuY3Rpb25gIHByb3RvdHlwZVxuICAgICAgICAgIHZhciBwcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHRoaXMpO1xuICAgICAgICAgIC8vIFJlc3RvcmUgdGhlIGBjYWxsYCBhbmQgYGFwcGx5YCBtZXRob2RzIGZyb20gYEZ1bmN0aW9uYFxuICAgICAgICAgIHByb3RvdHlwZS5jYWxsID0gY2FsbDtcbiAgICAgICAgICBwcm90b3R5cGUuYXBwbHkgPSBhcHBseTtcbiAgICAgICAgICBPYmplY3Quc2V0UHJvdG90eXBlT2YoY2hhaW5hYmxlTWV0aG9kV3JhcHBlciwgcHJvdG90eXBlKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBPdGhlcndpc2UsIHJlZGVmaW5lIGFsbCBwcm9wZXJ0aWVzIChzbG93ISlcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgdmFyIGFzc2VydGVyTmFtZXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhjdHgpO1xuICAgICAgICAgIGFzc2VydGVyTmFtZXMuZm9yRWFjaChmdW5jdGlvbiAoYXNzZXJ0ZXJOYW1lKSB7XG4gICAgICAgICAgICBpZiAoZXhjbHVkZU5hbWVzLmluZGV4T2YoYXNzZXJ0ZXJOYW1lKSAhPT0gLTEpIHtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgcGQgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGN0eCwgYXNzZXJ0ZXJOYW1lKTtcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjaGFpbmFibGVNZXRob2RXcmFwcGVyLCBhc3NlcnRlck5hbWUsIHBkKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRyYW5zZmVyRmxhZ3ModGhpcywgY2hhaW5hYmxlTWV0aG9kV3JhcHBlcik7XG4gICAgICAgIHJldHVybiBwcm94aWZ5KGNoYWluYWJsZU1ldGhvZFdyYXBwZXIpO1xuICAgICAgfVxuICAgICwgY29uZmlndXJhYmxlOiB0cnVlXG4gIH0pO1xufTtcbiIsInZhciBmbkxlbmd0aERlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGZ1bmN0aW9uICgpIHt9LCAnbGVuZ3RoJyk7XG5cbi8qIVxuICogQ2hhaSAtIGFkZExlbmd0aEd1YXJkIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuYWRkTGVuZ3RoR3VhcmQoZm4sIGFzc2VydGlvbk5hbWUsIGlzQ2hhaW5hYmxlKVxuICpcbiAqIERlZmluZSBgbGVuZ3RoYCBhcyBhIGdldHRlciBvbiB0aGUgZ2l2ZW4gdW5pbnZva2VkIG1ldGhvZCBhc3NlcnRpb24uIFRoZVxuICogZ2V0dGVyIGFjdHMgYXMgYSBndWFyZCBhZ2FpbnN0IGNoYWluaW5nIGBsZW5ndGhgIGRpcmVjdGx5IG9mZiBvZiBhbiB1bmludm9rZWRcbiAqIG1ldGhvZCBhc3NlcnRpb24sIHdoaWNoIGlzIGEgcHJvYmxlbSBiZWNhdXNlIGl0IHJlZmVyZW5jZXMgYGZ1bmN0aW9uYCdzXG4gKiBidWlsdC1pbiBgbGVuZ3RoYCBwcm9wZXJ0eSBpbnN0ZWFkIG9mIENoYWkncyBgbGVuZ3RoYCBhc3NlcnRpb24uIFdoZW4gdGhlXG4gKiBnZXR0ZXIgY2F0Y2hlcyB0aGUgdXNlciBtYWtpbmcgdGhpcyBtaXN0YWtlLCBpdCB0aHJvd3MgYW4gZXJyb3Igd2l0aCBhXG4gKiBoZWxwZnVsIG1lc3NhZ2UuXG4gKlxuICogVGhlcmUgYXJlIHR3byB3YXlzIGluIHdoaWNoIHRoaXMgbWlzdGFrZSBjYW4gYmUgbWFkZS4gVGhlIGZpcnN0IHdheSBpcyBieVxuICogY2hhaW5pbmcgdGhlIGBsZW5ndGhgIGFzc2VydGlvbiBkaXJlY3RseSBvZmYgb2YgYW4gdW5pbnZva2VkIGNoYWluYWJsZVxuICogbWV0aG9kLiBJbiB0aGlzIGNhc2UsIENoYWkgc3VnZ2VzdHMgdGhhdCB0aGUgdXNlciB1c2UgYGxlbmd0aE9mYCBpbnN0ZWFkLiBUaGVcbiAqIHNlY29uZCB3YXkgaXMgYnkgY2hhaW5pbmcgdGhlIGBsZW5ndGhgIGFzc2VydGlvbiBkaXJlY3RseSBvZmYgb2YgYW4gdW5pbnZva2VkXG4gKiBub24tY2hhaW5hYmxlIG1ldGhvZC4gTm9uLWNoYWluYWJsZSBtZXRob2RzIG11c3QgYmUgaW52b2tlZCBwcmlvciB0b1xuICogY2hhaW5pbmcuIEluIHRoaXMgY2FzZSwgQ2hhaSBzdWdnZXN0cyB0aGF0IHRoZSB1c2VyIGNvbnN1bHQgdGhlIGRvY3MgZm9yIHRoZVxuICogZ2l2ZW4gYXNzZXJ0aW9uLlxuICpcbiAqIElmIHRoZSBgbGVuZ3RoYCBwcm9wZXJ0eSBvZiBmdW5jdGlvbnMgaXMgdW5jb25maWd1cmFibGUsIHRoZW4gcmV0dXJuIGBmbmBcbiAqIHdpdGhvdXQgbW9kaWZpY2F0aW9uLlxuICpcbiAqIE5vdGUgdGhhdCBpbiBFUzYsIHRoZSBmdW5jdGlvbidzIGBsZW5ndGhgIHByb3BlcnR5IGlzIGNvbmZpZ3VyYWJsZSwgc28gb25jZVxuICogc3VwcG9ydCBmb3IgbGVnYWN5IGVudmlyb25tZW50cyBpcyBkcm9wcGVkLCBDaGFpJ3MgYGxlbmd0aGAgcHJvcGVydHkgY2FuXG4gKiByZXBsYWNlIHRoZSBidWlsdC1pbiBmdW5jdGlvbidzIGBsZW5ndGhgIHByb3BlcnR5LCBhbmQgdGhpcyBsZW5ndGggZ3VhcmQgd2lsbFxuICogbm8gbG9uZ2VyIGJlIG5lY2Vzc2FyeS4gSW4gdGhlIG1lYW4gdGltZSwgbWFpbnRhaW5pbmcgY29uc2lzdGVuY3kgYWNyb3NzIGFsbFxuICogZW52aXJvbm1lbnRzIGlzIHRoZSBwcmlvcml0eS5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxuICogQHBhcmFtIHtTdHJpbmd9IGFzc2VydGlvbk5hbWVcbiAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNDaGFpbmFibGVcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIGFkZExlbmd0aEd1YXJkXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBhZGRMZW5ndGhHdWFyZCAoZm4sIGFzc2VydGlvbk5hbWUsIGlzQ2hhaW5hYmxlKSB7XG4gIGlmICghZm5MZW5ndGhEZXNjLmNvbmZpZ3VyYWJsZSkgcmV0dXJuIGZuO1xuXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShmbiwgJ2xlbmd0aCcsIHtcbiAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChpc0NoYWluYWJsZSkge1xuICAgICAgICB0aHJvdyBFcnJvcignSW52YWxpZCBDaGFpIHByb3BlcnR5OiAnICsgYXNzZXJ0aW9uTmFtZSArICcubGVuZ3RoLiBEdWUnICtcbiAgICAgICAgICAnIHRvIGEgY29tcGF0aWJpbGl0eSBpc3N1ZSwgXCJsZW5ndGhcIiBjYW5ub3QgZGlyZWN0bHkgZm9sbG93IFwiJyArXG4gICAgICAgICAgYXNzZXJ0aW9uTmFtZSArICdcIi4gVXNlIFwiJyArIGFzc2VydGlvbk5hbWUgKyAnLmxlbmd0aE9mXCIgaW5zdGVhZC4nKTtcbiAgICAgIH1cblxuICAgICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgQ2hhaSBwcm9wZXJ0eTogJyArIGFzc2VydGlvbk5hbWUgKyAnLmxlbmd0aC4gU2VlJyArXG4gICAgICAgICcgZG9jcyBmb3IgcHJvcGVyIHVzYWdlIG9mIFwiJyArIGFzc2VydGlvbk5hbWUgKyAnXCIuJyk7XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gZm47XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gYWRkTWV0aG9kIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG52YXIgYWRkTGVuZ3RoR3VhcmQgPSByZXF1aXJlKCcuL2FkZExlbmd0aEd1YXJkJyk7XG52YXIgY2hhaSA9IHJlcXVpcmUoJy4uLy4uL2NoYWknKTtcbnZhciBmbGFnID0gcmVxdWlyZSgnLi9mbGFnJyk7XG52YXIgcHJveGlmeSA9IHJlcXVpcmUoJy4vcHJveGlmeScpO1xudmFyIHRyYW5zZmVyRmxhZ3MgPSByZXF1aXJlKCcuL3RyYW5zZmVyRmxhZ3MnKTtcblxuLyoqXG4gKiAjIyMgLmFkZE1ldGhvZChjdHgsIG5hbWUsIG1ldGhvZClcbiAqXG4gKiBBZGRzIGEgbWV0aG9kIHRvIHRoZSBwcm90b3R5cGUgb2YgYW4gb2JqZWN0LlxuICpcbiAqICAgICB1dGlscy5hZGRNZXRob2QoY2hhaS5Bc3NlcnRpb24ucHJvdG90eXBlLCAnZm9vJywgZnVuY3Rpb24gKHN0cikge1xuICogICAgICAgdmFyIG9iaiA9IHV0aWxzLmZsYWcodGhpcywgJ29iamVjdCcpO1xuICogICAgICAgbmV3IGNoYWkuQXNzZXJ0aW9uKG9iaikudG8uYmUuZXF1YWwoc3RyKTtcbiAqICAgICB9KTtcbiAqXG4gKiBDYW4gYWxzbyBiZSBhY2Nlc3NlZCBkaXJlY3RseSBmcm9tIGBjaGFpLkFzc2VydGlvbmAuXG4gKlxuICogICAgIGNoYWkuQXNzZXJ0aW9uLmFkZE1ldGhvZCgnZm9vJywgZm4pO1xuICpcbiAqIFRoZW4gY2FuIGJlIHVzZWQgYXMgYW55IG90aGVyIGFzc2VydGlvbi5cbiAqXG4gKiAgICAgZXhwZWN0KGZvb1N0cikudG8uYmUuZm9vKCdiYXInKTtcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gY3R4IG9iamVjdCB0byB3aGljaCB0aGUgbWV0aG9kIGlzIGFkZGVkXG4gKiBAcGFyYW0ge1N0cmluZ30gbmFtZSBvZiBtZXRob2QgdG8gYWRkXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBtZXRob2QgZnVuY3Rpb24gdG8gYmUgdXNlZCBmb3IgbmFtZVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgYWRkTWV0aG9kXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYWRkTWV0aG9kKGN0eCwgbmFtZSwgbWV0aG9kKSB7XG4gIHZhciBtZXRob2RXcmFwcGVyID0gZnVuY3Rpb24gKCkge1xuICAgIC8vIFNldHRpbmcgdGhlIGBzc2ZpYCBmbGFnIHRvIGBtZXRob2RXcmFwcGVyYCBjYXVzZXMgdGhpcyBmdW5jdGlvbiB0byBiZSB0aGVcbiAgICAvLyBzdGFydGluZyBwb2ludCBmb3IgcmVtb3ZpbmcgaW1wbGVtZW50YXRpb24gZnJhbWVzIGZyb20gdGhlIHN0YWNrIHRyYWNlIG9mXG4gICAgLy8gYSBmYWlsZWQgYXNzZXJ0aW9uLlxuICAgIC8vXG4gICAgLy8gSG93ZXZlciwgd2Ugb25seSB3YW50IHRvIHVzZSB0aGlzIGZ1bmN0aW9uIGFzIHRoZSBzdGFydGluZyBwb2ludCBpZiB0aGVcbiAgICAvLyBgbG9ja1NzZmlgIGZsYWcgaXNuJ3Qgc2V0LlxuICAgIC8vXG4gICAgLy8gSWYgdGhlIGBsb2NrU3NmaWAgZmxhZyBpcyBzZXQsIHRoZW4gZWl0aGVyIHRoaXMgYXNzZXJ0aW9uIGhhcyBiZWVuXG4gICAgLy8gb3ZlcndyaXR0ZW4gYnkgYW5vdGhlciBhc3NlcnRpb24sIG9yIHRoaXMgYXNzZXJ0aW9uIGlzIGJlaW5nIGludm9rZWQgZnJvbVxuICAgIC8vIGluc2lkZSBvZiBhbm90aGVyIGFzc2VydGlvbi4gSW4gdGhlIGZpcnN0IGNhc2UsIHRoZSBgc3NmaWAgZmxhZyBoYXNcbiAgICAvLyBhbHJlYWR5IGJlZW4gc2V0IGJ5IHRoZSBvdmVyd3JpdGluZyBhc3NlcnRpb24uIEluIHRoZSBzZWNvbmQgY2FzZSwgdGhlXG4gICAgLy8gYHNzZmlgIGZsYWcgaGFzIGFscmVhZHkgYmVlbiBzZXQgYnkgdGhlIG91dGVyIGFzc2VydGlvbi5cbiAgICBpZiAoIWZsYWcodGhpcywgJ2xvY2tTc2ZpJykpIHtcbiAgICAgIGZsYWcodGhpcywgJ3NzZmknLCBtZXRob2RXcmFwcGVyKTtcbiAgICB9XG5cbiAgICB2YXIgcmVzdWx0ID0gbWV0aG9kLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgaWYgKHJlc3VsdCAhPT0gdW5kZWZpbmVkKVxuICAgICAgcmV0dXJuIHJlc3VsdDtcblxuICAgIHZhciBuZXdBc3NlcnRpb24gPSBuZXcgY2hhaS5Bc3NlcnRpb24oKTtcbiAgICB0cmFuc2ZlckZsYWdzKHRoaXMsIG5ld0Fzc2VydGlvbik7XG4gICAgcmV0dXJuIG5ld0Fzc2VydGlvbjtcbiAgfTtcblxuICBhZGRMZW5ndGhHdWFyZChtZXRob2RXcmFwcGVyLCBuYW1lLCBmYWxzZSk7XG4gIGN0eFtuYW1lXSA9IHByb3hpZnkobWV0aG9kV3JhcHBlciwgbmFtZSk7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gYWRkUHJvcGVydHkgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTItMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbnZhciBjaGFpID0gcmVxdWlyZSgnLi4vLi4vY2hhaScpO1xudmFyIGZsYWcgPSByZXF1aXJlKCcuL2ZsYWcnKTtcbnZhciBpc1Byb3h5RW5hYmxlZCA9IHJlcXVpcmUoJy4vaXNQcm94eUVuYWJsZWQnKTtcbnZhciB0cmFuc2ZlckZsYWdzID0gcmVxdWlyZSgnLi90cmFuc2ZlckZsYWdzJyk7XG5cbi8qKlxuICogIyMjIC5hZGRQcm9wZXJ0eShjdHgsIG5hbWUsIGdldHRlcilcbiAqXG4gKiBBZGRzIGEgcHJvcGVydHkgdG8gdGhlIHByb3RvdHlwZSBvZiBhbiBvYmplY3QuXG4gKlxuICogICAgIHV0aWxzLmFkZFByb3BlcnR5KGNoYWkuQXNzZXJ0aW9uLnByb3RvdHlwZSwgJ2ZvbycsIGZ1bmN0aW9uICgpIHtcbiAqICAgICAgIHZhciBvYmogPSB1dGlscy5mbGFnKHRoaXMsICdvYmplY3QnKTtcbiAqICAgICAgIG5ldyBjaGFpLkFzc2VydGlvbihvYmopLnRvLmJlLmluc3RhbmNlb2YoRm9vKTtcbiAqICAgICB9KTtcbiAqXG4gKiBDYW4gYWxzbyBiZSBhY2Nlc3NlZCBkaXJlY3RseSBmcm9tIGBjaGFpLkFzc2VydGlvbmAuXG4gKlxuICogICAgIGNoYWkuQXNzZXJ0aW9uLmFkZFByb3BlcnR5KCdmb28nLCBmbik7XG4gKlxuICogVGhlbiBjYW4gYmUgdXNlZCBhcyBhbnkgb3RoZXIgYXNzZXJ0aW9uLlxuICpcbiAqICAgICBleHBlY3QobXlGb28pLnRvLmJlLmZvbztcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gY3R4IG9iamVjdCB0byB3aGljaCB0aGUgcHJvcGVydHkgaXMgYWRkZWRcbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIG9mIHByb3BlcnR5IHRvIGFkZFxuICogQHBhcmFtIHtGdW5jdGlvbn0gZ2V0dGVyIGZ1bmN0aW9uIHRvIGJlIHVzZWQgZm9yIG5hbWVcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIGFkZFByb3BlcnR5XG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gYWRkUHJvcGVydHkoY3R4LCBuYW1lLCBnZXR0ZXIpIHtcbiAgZ2V0dGVyID0gZ2V0dGVyID09PSB1bmRlZmluZWQgPyBmdW5jdGlvbiAoKSB7fSA6IGdldHRlcjtcblxuICBPYmplY3QuZGVmaW5lUHJvcGVydHkoY3R4LCBuYW1lLFxuICAgIHsgZ2V0OiBmdW5jdGlvbiBwcm9wZXJ0eUdldHRlcigpIHtcbiAgICAgICAgLy8gU2V0dGluZyB0aGUgYHNzZmlgIGZsYWcgdG8gYHByb3BlcnR5R2V0dGVyYCBjYXVzZXMgdGhpcyBmdW5jdGlvbiB0b1xuICAgICAgICAvLyBiZSB0aGUgc3RhcnRpbmcgcG9pbnQgZm9yIHJlbW92aW5nIGltcGxlbWVudGF0aW9uIGZyYW1lcyBmcm9tIHRoZVxuICAgICAgICAvLyBzdGFjayB0cmFjZSBvZiBhIGZhaWxlZCBhc3NlcnRpb24uXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEhvd2V2ZXIsIHdlIG9ubHkgd2FudCB0byB1c2UgdGhpcyBmdW5jdGlvbiBhcyB0aGUgc3RhcnRpbmcgcG9pbnQgaWZcbiAgICAgICAgLy8gdGhlIGBsb2NrU3NmaWAgZmxhZyBpc24ndCBzZXQgYW5kIHByb3h5IHByb3RlY3Rpb24gaXMgZGlzYWJsZWQuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIElmIHRoZSBgbG9ja1NzZmlgIGZsYWcgaXMgc2V0LCB0aGVuIGVpdGhlciB0aGlzIGFzc2VydGlvbiBoYXMgYmVlblxuICAgICAgICAvLyBvdmVyd3JpdHRlbiBieSBhbm90aGVyIGFzc2VydGlvbiwgb3IgdGhpcyBhc3NlcnRpb24gaXMgYmVpbmcgaW52b2tlZFxuICAgICAgICAvLyBmcm9tIGluc2lkZSBvZiBhbm90aGVyIGFzc2VydGlvbi4gSW4gdGhlIGZpcnN0IGNhc2UsIHRoZSBgc3NmaWAgZmxhZ1xuICAgICAgICAvLyBoYXMgYWxyZWFkeSBiZWVuIHNldCBieSB0aGUgb3ZlcndyaXRpbmcgYXNzZXJ0aW9uLiBJbiB0aGUgc2Vjb25kXG4gICAgICAgIC8vIGNhc2UsIHRoZSBgc3NmaWAgZmxhZyBoYXMgYWxyZWFkeSBiZWVuIHNldCBieSB0aGUgb3V0ZXIgYXNzZXJ0aW9uLlxuICAgICAgICAvL1xuICAgICAgICAvLyBJZiBwcm94eSBwcm90ZWN0aW9uIGlzIGVuYWJsZWQsIHRoZW4gdGhlIGBzc2ZpYCBmbGFnIGhhcyBhbHJlYWR5IGJlZW5cbiAgICAgICAgLy8gc2V0IGJ5IHRoZSBwcm94eSBnZXR0ZXIuXG4gICAgICAgIGlmICghaXNQcm94eUVuYWJsZWQoKSAmJiAhZmxhZyh0aGlzLCAnbG9ja1NzZmknKSkge1xuICAgICAgICAgIGZsYWcodGhpcywgJ3NzZmknLCBwcm9wZXJ0eUdldHRlcik7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgcmVzdWx0ID0gZ2V0dGVyLmNhbGwodGhpcyk7XG4gICAgICAgIGlmIChyZXN1bHQgIT09IHVuZGVmaW5lZClcbiAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuXG4gICAgICAgIHZhciBuZXdBc3NlcnRpb24gPSBuZXcgY2hhaS5Bc3NlcnRpb24oKTtcbiAgICAgICAgdHJhbnNmZXJGbGFncyh0aGlzLCBuZXdBc3NlcnRpb24pO1xuICAgICAgICByZXR1cm4gbmV3QXNzZXJ0aW9uO1xuICAgICAgfVxuICAgICwgY29uZmlndXJhYmxlOiB0cnVlXG4gIH0pO1xufTtcbiIsIi8qIVxuICogQ2hhaSAtIGNvbXBhcmVCeUluc3BlY3QgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTEtMjAxNiBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbi8qIVxuICogTW9kdWxlIGRlcGVuZGVuY2llc1xuICovXG5cbnZhciBpbnNwZWN0ID0gcmVxdWlyZSgnLi9pbnNwZWN0Jyk7XG5cbi8qKlxuICogIyMjIC5jb21wYXJlQnlJbnNwZWN0KG1peGVkLCBtaXhlZClcbiAqXG4gKiBUbyBiZSB1c2VkIGFzIGEgY29tcGFyZUZ1bmN0aW9uIHdpdGggQXJyYXkucHJvdG90eXBlLnNvcnQuIENvbXBhcmVzIGVsZW1lbnRzXG4gKiB1c2luZyBpbnNwZWN0IGluc3RlYWQgb2YgZGVmYXVsdCBiZWhhdmlvciBvZiB1c2luZyB0b1N0cmluZyBzbyB0aGF0IFN5bWJvbHNcbiAqIGFuZCBvYmplY3RzIHdpdGggaXJyZWd1bGFyL21pc3NpbmcgdG9TdHJpbmcgY2FuIHN0aWxsIGJlIHNvcnRlZCB3aXRob3V0IGFcbiAqIFR5cGVFcnJvci5cbiAqXG4gKiBAcGFyYW0ge01peGVkfSBmaXJzdCBlbGVtZW50IHRvIGNvbXBhcmVcbiAqIEBwYXJhbSB7TWl4ZWR9IHNlY29uZCBlbGVtZW50IHRvIGNvbXBhcmVcbiAqIEByZXR1cm5zIHtOdW1iZXJ9IC0xIGlmICdhJyBzaG91bGQgY29tZSBiZWZvcmUgJ2InOyBvdGhlcndpc2UgMVxuICogQG5hbWUgY29tcGFyZUJ5SW5zcGVjdFxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGNvbXBhcmVCeUluc3BlY3QoYSwgYikge1xuICByZXR1cm4gaW5zcGVjdChhKSA8IGluc3BlY3QoYikgPyAtMSA6IDE7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gZXhwZWN0VHlwZXMgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTItMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbi8qKlxuICogIyMjIC5leHBlY3RUeXBlcyhvYmosIHR5cGVzKVxuICpcbiAqIEVuc3VyZXMgdGhhdCB0aGUgb2JqZWN0IGJlaW5nIHRlc3RlZCBhZ2FpbnN0IGlzIG9mIGEgdmFsaWQgdHlwZS5cbiAqXG4gKiAgICAgdXRpbHMuZXhwZWN0VHlwZXModGhpcywgWydhcnJheScsICdvYmplY3QnLCAnc3RyaW5nJ10pO1xuICpcbiAqIEBwYXJhbSB7TWl4ZWR9IG9iaiBjb25zdHJ1Y3RlZCBBc3NlcnRpb25cbiAqIEBwYXJhbSB7QXJyYXl9IHR5cGUgQSBsaXN0IG9mIGFsbG93ZWQgdHlwZXMgZm9yIHRoaXMgYXNzZXJ0aW9uXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBleHBlY3RUeXBlc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG52YXIgQXNzZXJ0aW9uRXJyb3IgPSByZXF1aXJlKCdhc3NlcnRpb24tZXJyb3InKTtcbnZhciBmbGFnID0gcmVxdWlyZSgnLi9mbGFnJyk7XG52YXIgdHlwZSA9IHJlcXVpcmUoJ3R5cGUtZGV0ZWN0Jyk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZXhwZWN0VHlwZXMob2JqLCB0eXBlcykge1xuICB2YXIgZmxhZ01zZyA9IGZsYWcob2JqLCAnbWVzc2FnZScpO1xuICB2YXIgc3NmaSA9IGZsYWcob2JqLCAnc3NmaScpO1xuXG4gIGZsYWdNc2cgPSBmbGFnTXNnID8gZmxhZ01zZyArICc6ICcgOiAnJztcblxuICBvYmogPSBmbGFnKG9iaiwgJ29iamVjdCcpO1xuICB0eXBlcyA9IHR5cGVzLm1hcChmdW5jdGlvbiAodCkgeyByZXR1cm4gdC50b0xvd2VyQ2FzZSgpOyB9KTtcbiAgdHlwZXMuc29ydCgpO1xuXG4gIC8vIFRyYW5zZm9ybXMgWydsb3JlbScsICdpcHN1bSddIGludG8gJ2EgbG9yZW0sIG9yIGFuIGlwc3VtJ1xuICB2YXIgc3RyID0gdHlwZXMubWFwKGZ1bmN0aW9uICh0LCBpbmRleCkge1xuICAgIHZhciBhcnQgPSB+WyAnYScsICdlJywgJ2knLCAnbycsICd1JyBdLmluZGV4T2YodC5jaGFyQXQoMCkpID8gJ2FuJyA6ICdhJztcbiAgICB2YXIgb3IgPSB0eXBlcy5sZW5ndGggPiAxICYmIGluZGV4ID09PSB0eXBlcy5sZW5ndGggLSAxID8gJ29yICcgOiAnJztcbiAgICByZXR1cm4gb3IgKyBhcnQgKyAnICcgKyB0O1xuICB9KS5qb2luKCcsICcpO1xuXG4gIHZhciBvYmpUeXBlID0gdHlwZShvYmopLnRvTG93ZXJDYXNlKCk7XG5cbiAgaWYgKCF0eXBlcy5zb21lKGZ1bmN0aW9uIChleHBlY3RlZCkgeyByZXR1cm4gb2JqVHlwZSA9PT0gZXhwZWN0ZWQ7IH0pKSB7XG4gICAgdGhyb3cgbmV3IEFzc2VydGlvbkVycm9yKFxuICAgICAgZmxhZ01zZyArICdvYmplY3QgdGVzdGVkIG11c3QgYmUgJyArIHN0ciArICcsIGJ1dCAnICsgb2JqVHlwZSArICcgZ2l2ZW4nLFxuICAgICAgdW5kZWZpbmVkLFxuICAgICAgc3NmaVxuICAgICk7XG4gIH1cbn07XG4iLCIvKiFcbiAqIENoYWkgLSBmbGFnIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuZmxhZyhvYmplY3QsIGtleSwgW3ZhbHVlXSlcbiAqXG4gKiBHZXQgb3Igc2V0IGEgZmxhZyB2YWx1ZSBvbiBhbiBvYmplY3QuIElmIGFcbiAqIHZhbHVlIGlzIHByb3ZpZGVkIGl0IHdpbGwgYmUgc2V0LCBlbHNlIGl0IHdpbGxcbiAqIHJldHVybiB0aGUgY3VycmVudGx5IHNldCB2YWx1ZSBvciBgdW5kZWZpbmVkYCBpZlxuICogdGhlIHZhbHVlIGlzIG5vdCBzZXQuXG4gKlxuICogICAgIHV0aWxzLmZsYWcodGhpcywgJ2ZvbycsICdiYXInKTsgLy8gc2V0dGVyXG4gKiAgICAgdXRpbHMuZmxhZyh0aGlzLCAnZm9vJyk7IC8vIGdldHRlciwgcmV0dXJucyBgYmFyYFxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmplY3QgY29uc3RydWN0ZWQgQXNzZXJ0aW9uXG4gKiBAcGFyYW0ge1N0cmluZ30ga2V5XG4gKiBAcGFyYW0ge01peGVkfSB2YWx1ZSAob3B0aW9uYWwpXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBmbGFnXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGZsYWcob2JqLCBrZXksIHZhbHVlKSB7XG4gIHZhciBmbGFncyA9IG9iai5fX2ZsYWdzIHx8IChvYmouX19mbGFncyA9IE9iamVjdC5jcmVhdGUobnVsbCkpO1xuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMykge1xuICAgIGZsYWdzW2tleV0gPSB2YWx1ZTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZmxhZ3Nba2V5XTtcbiAgfVxufTtcbiIsIi8qIVxuICogQ2hhaSAtIGdldEFjdHVhbCB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyoqXG4gKiAjIyMgLmdldEFjdHVhbChvYmplY3QsIFthY3R1YWxdKVxuICpcbiAqIFJldHVybnMgdGhlIGBhY3R1YWxgIHZhbHVlIGZvciBhbiBBc3NlcnRpb24uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCAoY29uc3RydWN0ZWQgQXNzZXJ0aW9uKVxuICogQHBhcmFtIHtBcmd1bWVudHN9IGNoYWkuQXNzZXJ0aW9uLnByb3RvdHlwZS5hc3NlcnQgYXJndW1lbnRzXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBnZXRBY3R1YWxcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldEFjdHVhbChvYmosIGFyZ3MpIHtcbiAgcmV0dXJuIGFyZ3MubGVuZ3RoID4gNCA/IGFyZ3NbNF0gOiBvYmouX29iajtcbn07XG4iLCIvKiFcbiAqIENoYWkgLSBnZXRFbnVtZXJhYmxlUHJvcGVydGllcyB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyoqXG4gKiAjIyMgLmdldEVudW1lcmFibGVQcm9wZXJ0aWVzKG9iamVjdClcbiAqXG4gKiBUaGlzIGFsbG93cyB0aGUgcmV0cmlldmFsIG9mIGVudW1lcmFibGUgcHJvcGVydHkgbmFtZXMgb2YgYW4gb2JqZWN0LFxuICogaW5oZXJpdGVkIG9yIG5vdC5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gKiBAcmV0dXJucyB7QXJyYXl9XG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBnZXRFbnVtZXJhYmxlUHJvcGVydGllc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldEVudW1lcmFibGVQcm9wZXJ0aWVzKG9iamVjdCkge1xuICB2YXIgcmVzdWx0ID0gW107XG4gIGZvciAodmFyIG5hbWUgaW4gb2JqZWN0KSB7XG4gICAgcmVzdWx0LnB1c2gobmFtZSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG4iLCIvKiFcbiAqIENoYWkgLSBtZXNzYWdlIGNvbXBvc2l0aW9uIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKiFcbiAqIE1vZHVsZSBkZXBlbmRlbmNpZXNcbiAqL1xuXG52YXIgZmxhZyA9IHJlcXVpcmUoJy4vZmxhZycpXG4gICwgZ2V0QWN0dWFsID0gcmVxdWlyZSgnLi9nZXRBY3R1YWwnKVxuICAsIG9iakRpc3BsYXkgPSByZXF1aXJlKCcuL29iakRpc3BsYXknKTtcblxuLyoqXG4gKiAjIyMgLmdldE1lc3NhZ2Uob2JqZWN0LCBtZXNzYWdlLCBuZWdhdGVNZXNzYWdlKVxuICpcbiAqIENvbnN0cnVjdCB0aGUgZXJyb3IgbWVzc2FnZSBiYXNlZCBvbiBmbGFnc1xuICogYW5kIHRlbXBsYXRlIHRhZ3MuIFRlbXBsYXRlIHRhZ3Mgd2lsbCByZXR1cm5cbiAqIGEgc3RyaW5naWZpZWQgaW5zcGVjdGlvbiBvZiB0aGUgb2JqZWN0IHJlZmVyZW5jZWQuXG4gKlxuICogTWVzc2FnZSB0ZW1wbGF0ZSB0YWdzOlxuICogLSBgI3t0aGlzfWAgY3VycmVudCBhc3NlcnRlZCBvYmplY3RcbiAqIC0gYCN7YWN0fWAgYWN0dWFsIHZhbHVlXG4gKiAtIGAje2V4cH1gIGV4cGVjdGVkIHZhbHVlXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCAoY29uc3RydWN0ZWQgQXNzZXJ0aW9uKVxuICogQHBhcmFtIHtBcmd1bWVudHN9IGNoYWkuQXNzZXJ0aW9uLnByb3RvdHlwZS5hc3NlcnQgYXJndW1lbnRzXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBnZXRNZXNzYWdlXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gZ2V0TWVzc2FnZShvYmosIGFyZ3MpIHtcbiAgdmFyIG5lZ2F0ZSA9IGZsYWcob2JqLCAnbmVnYXRlJylcbiAgICAsIHZhbCA9IGZsYWcob2JqLCAnb2JqZWN0JylcbiAgICAsIGV4cGVjdGVkID0gYXJnc1szXVxuICAgICwgYWN0dWFsID0gZ2V0QWN0dWFsKG9iaiwgYXJncylcbiAgICAsIG1zZyA9IG5lZ2F0ZSA/IGFyZ3NbMl0gOiBhcmdzWzFdXG4gICAgLCBmbGFnTXNnID0gZmxhZyhvYmosICdtZXNzYWdlJyk7XG5cbiAgaWYodHlwZW9mIG1zZyA9PT0gXCJmdW5jdGlvblwiKSBtc2cgPSBtc2coKTtcbiAgbXNnID0gbXNnIHx8ICcnO1xuICBtc2cgPSBtc2dcbiAgICAucmVwbGFjZSgvI1xce3RoaXNcXH0vZywgZnVuY3Rpb24gKCkgeyByZXR1cm4gb2JqRGlzcGxheSh2YWwpOyB9KVxuICAgIC5yZXBsYWNlKC8jXFx7YWN0XFx9L2csIGZ1bmN0aW9uICgpIHsgcmV0dXJuIG9iakRpc3BsYXkoYWN0dWFsKTsgfSlcbiAgICAucmVwbGFjZSgvI1xce2V4cFxcfS9nLCBmdW5jdGlvbiAoKSB7IHJldHVybiBvYmpEaXNwbGF5KGV4cGVjdGVkKTsgfSk7XG5cbiAgcmV0dXJuIGZsYWdNc2cgPyBmbGFnTXNnICsgJzogJyArIG1zZyA6IG1zZztcbn07XG4iLCJ2YXIgdHlwZSA9IHJlcXVpcmUoJ3R5cGUtZGV0ZWN0Jyk7XG5cbnZhciBmbGFnID0gcmVxdWlyZSgnLi9mbGFnJyk7XG5cbmZ1bmN0aW9uIGlzT2JqZWN0VHlwZShvYmopIHtcbiAgdmFyIG9iamVjdFR5cGUgPSB0eXBlKG9iaik7XG4gIHZhciBvYmplY3RUeXBlcyA9IFsnQXJyYXknLCAnT2JqZWN0JywgJ2Z1bmN0aW9uJ107XG5cbiAgcmV0dXJuIG9iamVjdFR5cGVzLmluZGV4T2Yob2JqZWN0VHlwZSkgIT09IC0xO1xufVxuXG4vKipcbiAqICMjIyAuZ2V0T3BlcmF0b3IobWVzc2FnZSlcbiAqXG4gKiBFeHRyYWN0IHRoZSBvcGVyYXRvciBmcm9tIGVycm9yIG1lc3NhZ2UuXG4gKiBPcGVyYXRvciBkZWZpbmVkIGlzIGJhc2VkIG9uIGJlbG93IGxpbmtcbiAqIGh0dHBzOi8vbm9kZWpzLm9yZy9hcGkvYXNzZXJ0Lmh0bWwjYXNzZXJ0X2Fzc2VydC5cbiAqXG4gKiBSZXR1cm5zIHRoZSBgb3BlcmF0b3JgIG9yIGB1bmRlZmluZWRgIHZhbHVlIGZvciBhbiBBc3NlcnRpb24uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCAoY29uc3RydWN0ZWQgQXNzZXJ0aW9uKVxuICogQHBhcmFtIHtBcmd1bWVudHN9IGNoYWkuQXNzZXJ0aW9uLnByb3RvdHlwZS5hc3NlcnQgYXJndW1lbnRzXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBnZXRPcGVyYXRvclxuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldE9wZXJhdG9yKG9iaiwgYXJncykge1xuICB2YXIgb3BlcmF0b3IgPSBmbGFnKG9iaiwgJ29wZXJhdG9yJyk7XG4gIHZhciBuZWdhdGUgPSBmbGFnKG9iaiwgJ25lZ2F0ZScpO1xuICB2YXIgZXhwZWN0ZWQgPSBhcmdzWzNdO1xuICB2YXIgbXNnID0gbmVnYXRlID8gYXJnc1syXSA6IGFyZ3NbMV07XG5cbiAgaWYgKG9wZXJhdG9yKSB7XG4gICAgcmV0dXJuIG9wZXJhdG9yO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBtc2cgPT09ICdmdW5jdGlvbicpIG1zZyA9IG1zZygpO1xuXG4gIG1zZyA9IG1zZyB8fCAnJztcbiAgaWYgKCFtc2cpIHtcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgaWYgKC9cXHNoYXZlXFxzLy50ZXN0KG1zZykpIHtcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgdmFyIGlzT2JqZWN0ID0gaXNPYmplY3RUeXBlKGV4cGVjdGVkKTtcbiAgaWYgKC9cXHNub3RcXHMvLnRlc3QobXNnKSkge1xuICAgIHJldHVybiBpc09iamVjdCA/ICdub3REZWVwU3RyaWN0RXF1YWwnIDogJ25vdFN0cmljdEVxdWFsJztcbiAgfVxuXG4gIHJldHVybiBpc09iamVjdCA/ICdkZWVwU3RyaWN0RXF1YWwnIDogJ3N0cmljdEVxdWFsJztcbn07XG4iLCIvKiFcbiAqIENoYWkgLSBnZXRPd25FbnVtZXJhYmxlUHJvcGVydGllcyB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMS0yMDE2IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyohXG4gKiBNb2R1bGUgZGVwZW5kZW5jaWVzXG4gKi9cblxudmFyIGdldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMgPSByZXF1aXJlKCcuL2dldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMnKTtcblxuLyoqXG4gKiAjIyMgLmdldE93bkVudW1lcmFibGVQcm9wZXJ0aWVzKG9iamVjdClcbiAqXG4gKiBUaGlzIGFsbG93cyB0aGUgcmV0cmlldmFsIG9mIGRpcmVjdGx5LW93bmVkIGVudW1lcmFibGUgcHJvcGVydHkgbmFtZXMgYW5kXG4gKiBzeW1ib2xzIG9mIGFuIG9iamVjdC4gVGhpcyBmdW5jdGlvbiBpcyBuZWNlc3NhcnkgYmVjYXVzZSBPYmplY3Qua2V5cyBvbmx5XG4gKiByZXR1cm5zIGVudW1lcmFibGUgcHJvcGVydHkgbmFtZXMsIG5vdCBlbnVtZXJhYmxlIHByb3BlcnR5IHN5bWJvbHMuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHJldHVybnMge0FycmF5fVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgZ2V0T3duRW51bWVyYWJsZVByb3BlcnRpZXNcbiAqIEBhcGkgcHVibGljXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBnZXRPd25FbnVtZXJhYmxlUHJvcGVydGllcyhvYmopIHtcbiAgcmV0dXJuIE9iamVjdC5rZXlzKG9iaikuY29uY2F0KGdldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMob2JqKSk7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gZ2V0T3duRW51bWVyYWJsZVByb3BlcnR5U3ltYm9scyB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMS0yMDE2IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyoqXG4gKiAjIyMgLmdldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMob2JqZWN0KVxuICpcbiAqIFRoaXMgYWxsb3dzIHRoZSByZXRyaWV2YWwgb2YgZGlyZWN0bHktb3duZWQgZW51bWVyYWJsZSBwcm9wZXJ0eSBzeW1ib2xzIG9mIGFuXG4gKiBvYmplY3QuIFRoaXMgZnVuY3Rpb24gaXMgbmVjZXNzYXJ5IGJlY2F1c2UgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9sc1xuICogcmV0dXJucyBib3RoIGVudW1lcmFibGUgYW5kIG5vbi1lbnVtZXJhYmxlIHByb3BlcnR5IHN5bWJvbHMuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHJldHVybnMge0FycmF5fVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgZ2V0T3duRW51bWVyYWJsZVByb3BlcnR5U3ltYm9sc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMob2JqKSB7XG4gIGlmICh0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyAhPT0gJ2Z1bmN0aW9uJykgcmV0dXJuIFtdO1xuXG4gIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKG9iaikuZmlsdGVyKGZ1bmN0aW9uIChzeW0pIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmosIHN5bSkuZW51bWVyYWJsZTtcbiAgfSk7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gZ2V0UHJvcGVydGllcyB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyoqXG4gKiAjIyMgLmdldFByb3BlcnRpZXMob2JqZWN0KVxuICpcbiAqIFRoaXMgYWxsb3dzIHRoZSByZXRyaWV2YWwgb2YgcHJvcGVydHkgbmFtZXMgb2YgYW4gb2JqZWN0LCBlbnVtZXJhYmxlIG9yIG5vdCxcbiAqIGluaGVyaXRlZCBvciBub3QuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHJldHVybnMge0FycmF5fVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgZ2V0UHJvcGVydGllc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGdldFByb3BlcnRpZXMob2JqZWN0KSB7XG4gIHZhciByZXN1bHQgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhvYmplY3QpO1xuXG4gIGZ1bmN0aW9uIGFkZFByb3BlcnR5KHByb3BlcnR5KSB7XG4gICAgaWYgKHJlc3VsdC5pbmRleE9mKHByb3BlcnR5KSA9PT0gLTEpIHtcbiAgICAgIHJlc3VsdC5wdXNoKHByb3BlcnR5KTtcbiAgICB9XG4gIH1cblxuICB2YXIgcHJvdG8gPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqZWN0KTtcbiAgd2hpbGUgKHByb3RvICE9PSBudWxsKSB7XG4gICAgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMocHJvdG8pLmZvckVhY2goYWRkUHJvcGVydHkpO1xuICAgIHByb3RvID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKHByb3RvKTtcbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59O1xuIiwiLyohXG4gKiBjaGFpXG4gKiBDb3B5cmlnaHQoYykgMjAxMSBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbi8qIVxuICogRGVwZW5kZW5jaWVzIHRoYXQgYXJlIHVzZWQgZm9yIG11bHRpcGxlIGV4cG9ydHMgYXJlIHJlcXVpcmVkIGhlcmUgb25seSBvbmNlXG4gKi9cblxudmFyIHBhdGh2YWwgPSByZXF1aXJlKCdwYXRodmFsJyk7XG5cbi8qIVxuICogdGVzdCB1dGlsaXR5XG4gKi9cblxuZXhwb3J0cy50ZXN0ID0gcmVxdWlyZSgnLi90ZXN0Jyk7XG5cbi8qIVxuICogdHlwZSB1dGlsaXR5XG4gKi9cblxuZXhwb3J0cy50eXBlID0gcmVxdWlyZSgndHlwZS1kZXRlY3QnKTtcblxuLyohXG4gKiBleHBlY3RUeXBlcyB1dGlsaXR5XG4gKi9cbmV4cG9ydHMuZXhwZWN0VHlwZXMgPSByZXF1aXJlKCcuL2V4cGVjdFR5cGVzJyk7XG5cbi8qIVxuICogbWVzc2FnZSB1dGlsaXR5XG4gKi9cblxuZXhwb3J0cy5nZXRNZXNzYWdlID0gcmVxdWlyZSgnLi9nZXRNZXNzYWdlJyk7XG5cbi8qIVxuICogYWN0dWFsIHV0aWxpdHlcbiAqL1xuXG5leHBvcnRzLmdldEFjdHVhbCA9IHJlcXVpcmUoJy4vZ2V0QWN0dWFsJyk7XG5cbi8qIVxuICogSW5zcGVjdCB1dGlsXG4gKi9cblxuZXhwb3J0cy5pbnNwZWN0ID0gcmVxdWlyZSgnLi9pbnNwZWN0Jyk7XG5cbi8qIVxuICogT2JqZWN0IERpc3BsYXkgdXRpbFxuICovXG5cbmV4cG9ydHMub2JqRGlzcGxheSA9IHJlcXVpcmUoJy4vb2JqRGlzcGxheScpO1xuXG4vKiFcbiAqIEZsYWcgdXRpbGl0eVxuICovXG5cbmV4cG9ydHMuZmxhZyA9IHJlcXVpcmUoJy4vZmxhZycpO1xuXG4vKiFcbiAqIEZsYWcgdHJhbnNmZXJyaW5nIHV0aWxpdHlcbiAqL1xuXG5leHBvcnRzLnRyYW5zZmVyRmxhZ3MgPSByZXF1aXJlKCcuL3RyYW5zZmVyRmxhZ3MnKTtcblxuLyohXG4gKiBEZWVwIGVxdWFsIHV0aWxpdHlcbiAqL1xuXG5leHBvcnRzLmVxbCA9IHJlcXVpcmUoJ2RlZXAtZXFsJyk7XG5cbi8qIVxuICogRGVlcCBwYXRoIGluZm9cbiAqL1xuXG5leHBvcnRzLmdldFBhdGhJbmZvID0gcGF0aHZhbC5nZXRQYXRoSW5mbztcblxuLyohXG4gKiBDaGVjayBpZiBhIHByb3BlcnR5IGV4aXN0c1xuICovXG5cbmV4cG9ydHMuaGFzUHJvcGVydHkgPSBwYXRodmFsLmhhc1Byb3BlcnR5O1xuXG4vKiFcbiAqIEZ1bmN0aW9uIG5hbWVcbiAqL1xuXG5leHBvcnRzLmdldE5hbWUgPSByZXF1aXJlKCdnZXQtZnVuYy1uYW1lJyk7XG5cbi8qIVxuICogYWRkIFByb3BlcnR5XG4gKi9cblxuZXhwb3J0cy5hZGRQcm9wZXJ0eSA9IHJlcXVpcmUoJy4vYWRkUHJvcGVydHknKTtcblxuLyohXG4gKiBhZGQgTWV0aG9kXG4gKi9cblxuZXhwb3J0cy5hZGRNZXRob2QgPSByZXF1aXJlKCcuL2FkZE1ldGhvZCcpO1xuXG4vKiFcbiAqIG92ZXJ3cml0ZSBQcm9wZXJ0eVxuICovXG5cbmV4cG9ydHMub3ZlcndyaXRlUHJvcGVydHkgPSByZXF1aXJlKCcuL292ZXJ3cml0ZVByb3BlcnR5Jyk7XG5cbi8qIVxuICogb3ZlcndyaXRlIE1ldGhvZFxuICovXG5cbmV4cG9ydHMub3ZlcndyaXRlTWV0aG9kID0gcmVxdWlyZSgnLi9vdmVyd3JpdGVNZXRob2QnKTtcblxuLyohXG4gKiBBZGQgYSBjaGFpbmFibGUgbWV0aG9kXG4gKi9cblxuZXhwb3J0cy5hZGRDaGFpbmFibGVNZXRob2QgPSByZXF1aXJlKCcuL2FkZENoYWluYWJsZU1ldGhvZCcpO1xuXG4vKiFcbiAqIE92ZXJ3cml0ZSBjaGFpbmFibGUgbWV0aG9kXG4gKi9cblxuZXhwb3J0cy5vdmVyd3JpdGVDaGFpbmFibGVNZXRob2QgPSByZXF1aXJlKCcuL292ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZCcpO1xuXG4vKiFcbiAqIENvbXBhcmUgYnkgaW5zcGVjdCBtZXRob2RcbiAqL1xuXG5leHBvcnRzLmNvbXBhcmVCeUluc3BlY3QgPSByZXF1aXJlKCcuL2NvbXBhcmVCeUluc3BlY3QnKTtcblxuLyohXG4gKiBHZXQgb3duIGVudW1lcmFibGUgcHJvcGVydHkgc3ltYm9scyBtZXRob2RcbiAqL1xuXG5leHBvcnRzLmdldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMgPSByZXF1aXJlKCcuL2dldE93bkVudW1lcmFibGVQcm9wZXJ0eVN5bWJvbHMnKTtcblxuLyohXG4gKiBHZXQgb3duIGVudW1lcmFibGUgcHJvcGVydGllcyBtZXRob2RcbiAqL1xuXG5leHBvcnRzLmdldE93bkVudW1lcmFibGVQcm9wZXJ0aWVzID0gcmVxdWlyZSgnLi9nZXRPd25FbnVtZXJhYmxlUHJvcGVydGllcycpO1xuXG4vKiFcbiAqIENoZWNrcyBlcnJvciBhZ2FpbnN0IGEgZ2l2ZW4gc2V0IG9mIGNyaXRlcmlhXG4gKi9cblxuZXhwb3J0cy5jaGVja0Vycm9yID0gcmVxdWlyZSgnY2hlY2stZXJyb3InKTtcblxuLyohXG4gKiBQcm94aWZ5IHV0aWxcbiAqL1xuXG5leHBvcnRzLnByb3hpZnkgPSByZXF1aXJlKCcuL3Byb3hpZnknKTtcblxuLyohXG4gKiBhZGRMZW5ndGhHdWFyZCB1dGlsXG4gKi9cblxuZXhwb3J0cy5hZGRMZW5ndGhHdWFyZCA9IHJlcXVpcmUoJy4vYWRkTGVuZ3RoR3VhcmQnKTtcblxuLyohXG4gKiBpc1Byb3h5RW5hYmxlZCBoZWxwZXJcbiAqL1xuXG5leHBvcnRzLmlzUHJveHlFbmFibGVkID0gcmVxdWlyZSgnLi9pc1Byb3h5RW5hYmxlZCcpO1xuXG4vKiFcbiAqIGlzTmFOIG1ldGhvZFxuICovXG5cbmV4cG9ydHMuaXNOYU4gPSByZXF1aXJlKCcuL2lzTmFOJyk7XG5cbi8qIVxuICogZ2V0T3BlcmF0b3IgbWV0aG9kXG4gKi9cblxuZXhwb3J0cy5nZXRPcGVyYXRvciA9IHJlcXVpcmUoJy4vZ2V0T3BlcmF0b3InKTsiLCIvLyBUaGlzIGlzIChhbG1vc3QpIGRpcmVjdGx5IGZyb20gTm9kZS5qcyB1dGlsc1xuLy8gaHR0cHM6Ly9naXRodWIuY29tL2pveWVudC9ub2RlL2Jsb2IvZjhjMzM1ZDBjYWY0N2YxNmQzMTQxM2Y4OWFhMjhlZGEzODc4ZTNhYS9saWIvdXRpbC5qc1xuXG52YXIgZ2V0TmFtZSA9IHJlcXVpcmUoJ2dldC1mdW5jLW5hbWUnKTtcbnZhciBnZXRQcm9wZXJ0aWVzID0gcmVxdWlyZSgnLi9nZXRQcm9wZXJ0aWVzJyk7XG52YXIgZ2V0RW51bWVyYWJsZVByb3BlcnRpZXMgPSByZXF1aXJlKCcuL2dldEVudW1lcmFibGVQcm9wZXJ0aWVzJyk7XG52YXIgY29uZmlnID0gcmVxdWlyZSgnLi4vY29uZmlnJyk7XG5cbm1vZHVsZS5leHBvcnRzID0gaW5zcGVjdDtcblxuLyoqXG4gKiAjIyMgLmluc3BlY3Qob2JqLCBbc2hvd0hpZGRlbl0sIFtkZXB0aF0sIFtjb2xvcnNdKVxuICpcbiAqIEVjaG9lcyB0aGUgdmFsdWUgb2YgYSB2YWx1ZS4gVHJpZXMgdG8gcHJpbnQgdGhlIHZhbHVlIG91dFxuICogaW4gdGhlIGJlc3Qgd2F5IHBvc3NpYmxlIGdpdmVuIHRoZSBkaWZmZXJlbnQgdHlwZXMuXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iaiBUaGUgb2JqZWN0IHRvIHByaW50IG91dC5cbiAqIEBwYXJhbSB7Qm9vbGVhbn0gc2hvd0hpZGRlbiBGbGFnIHRoYXQgc2hvd3MgaGlkZGVuIChub3QgZW51bWVyYWJsZSlcbiAqICAgIHByb3BlcnRpZXMgb2Ygb2JqZWN0cy4gRGVmYXVsdCBpcyBmYWxzZS5cbiAqIEBwYXJhbSB7TnVtYmVyfSBkZXB0aCBEZXB0aCBpbiB3aGljaCB0byBkZXNjZW5kIGluIG9iamVjdC4gRGVmYXVsdCBpcyAyLlxuICogQHBhcmFtIHtCb29sZWFufSBjb2xvcnMgRmxhZyB0byB0dXJuIG9uIEFOU0kgZXNjYXBlIGNvZGVzIHRvIGNvbG9yIHRoZVxuICogICAgb3V0cHV0LiBEZWZhdWx0IGlzIGZhbHNlIChubyBjb2xvcmluZykuXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBpbnNwZWN0XG4gKi9cbmZ1bmN0aW9uIGluc3BlY3Qob2JqLCBzaG93SGlkZGVuLCBkZXB0aCwgY29sb3JzKSB7XG4gIHZhciBjdHggPSB7XG4gICAgc2hvd0hpZGRlbjogc2hvd0hpZGRlbixcbiAgICBzZWVuOiBbXSxcbiAgICBzdHlsaXplOiBmdW5jdGlvbiAoc3RyKSB7IHJldHVybiBzdHI7IH1cbiAgfTtcbiAgcmV0dXJuIGZvcm1hdFZhbHVlKGN0eCwgb2JqLCAodHlwZW9mIGRlcHRoID09PSAndW5kZWZpbmVkJyA/IDIgOiBkZXB0aCkpO1xufVxuXG4vLyBSZXR1cm5zIHRydWUgaWYgb2JqZWN0IGlzIGEgRE9NIGVsZW1lbnQuXG52YXIgaXNET01FbGVtZW50ID0gZnVuY3Rpb24gKG9iamVjdCkge1xuICBpZiAodHlwZW9mIEhUTUxFbGVtZW50ID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBvYmplY3QgaW5zdGFuY2VvZiBIVE1MRWxlbWVudDtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gb2JqZWN0ICYmXG4gICAgICB0eXBlb2Ygb2JqZWN0ID09PSAnb2JqZWN0JyAmJlxuICAgICAgJ25vZGVUeXBlJyBpbiBvYmplY3QgJiZcbiAgICAgIG9iamVjdC5ub2RlVHlwZSA9PT0gMSAmJlxuICAgICAgdHlwZW9mIG9iamVjdC5ub2RlTmFtZSA9PT0gJ3N0cmluZyc7XG4gIH1cbn07XG5cbmZ1bmN0aW9uIGZvcm1hdFZhbHVlKGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcykge1xuICAvLyBQcm92aWRlIGEgaG9vayBmb3IgdXNlci1zcGVjaWZpZWQgaW5zcGVjdCBmdW5jdGlvbnMuXG4gIC8vIENoZWNrIHRoYXQgdmFsdWUgaXMgYW4gb2JqZWN0IHdpdGggYW4gaW5zcGVjdCBmdW5jdGlvbiBvbiBpdFxuICBpZiAodmFsdWUgJiYgdHlwZW9mIHZhbHVlLmluc3BlY3QgPT09ICdmdW5jdGlvbicgJiZcbiAgICAgIC8vIEZpbHRlciBvdXQgdGhlIHV0aWwgbW9kdWxlLCBpdCdzIGluc3BlY3QgZnVuY3Rpb24gaXMgc3BlY2lhbFxuICAgICAgdmFsdWUuaW5zcGVjdCAhPT0gZXhwb3J0cy5pbnNwZWN0ICYmXG4gICAgICAvLyBBbHNvIGZpbHRlciBvdXQgYW55IHByb3RvdHlwZSBvYmplY3RzIHVzaW5nIHRoZSBjaXJjdWxhciBjaGVjay5cbiAgICAgICEodmFsdWUuY29uc3RydWN0b3IgJiYgdmFsdWUuY29uc3RydWN0b3IucHJvdG90eXBlID09PSB2YWx1ZSkpIHtcbiAgICB2YXIgcmV0ID0gdmFsdWUuaW5zcGVjdChyZWN1cnNlVGltZXMsIGN0eCk7XG4gICAgaWYgKHR5cGVvZiByZXQgIT09ICdzdHJpbmcnKSB7XG4gICAgICByZXQgPSBmb3JtYXRWYWx1ZShjdHgsIHJldCwgcmVjdXJzZVRpbWVzKTtcbiAgICB9XG4gICAgcmV0dXJuIHJldDtcbiAgfVxuXG4gIC8vIFByaW1pdGl2ZSB0eXBlcyBjYW5ub3QgaGF2ZSBwcm9wZXJ0aWVzXG4gIHZhciBwcmltaXRpdmUgPSBmb3JtYXRQcmltaXRpdmUoY3R4LCB2YWx1ZSk7XG4gIGlmIChwcmltaXRpdmUpIHtcbiAgICByZXR1cm4gcHJpbWl0aXZlO1xuICB9XG5cbiAgLy8gSWYgdGhpcyBpcyBhIERPTSBlbGVtZW50LCB0cnkgdG8gZ2V0IHRoZSBvdXRlciBIVE1MLlxuICBpZiAoaXNET01FbGVtZW50KHZhbHVlKSkge1xuICAgIGlmICgnb3V0ZXJIVE1MJyBpbiB2YWx1ZSkge1xuICAgICAgcmV0dXJuIHZhbHVlLm91dGVySFRNTDtcbiAgICAgIC8vIFRoaXMgdmFsdWUgZG9lcyBub3QgaGF2ZSBhbiBvdXRlckhUTUwgYXR0cmlidXRlLFxuICAgICAgLy8gICBpdCBjb3VsZCBzdGlsbCBiZSBhbiBYTUwgZWxlbWVudFxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBBdHRlbXB0IHRvIHNlcmlhbGl6ZSBpdFxuICAgICAgdHJ5IHtcbiAgICAgICAgaWYgKGRvY3VtZW50LnhtbFZlcnNpb24pIHtcbiAgICAgICAgICB2YXIgeG1sU2VyaWFsaXplciA9IG5ldyBYTUxTZXJpYWxpemVyKCk7XG4gICAgICAgICAgcmV0dXJuIHhtbFNlcmlhbGl6ZXIuc2VyaWFsaXplVG9TdHJpbmcodmFsdWUpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIEZpcmVmb3ggMTEtIGRvIG5vdCBzdXBwb3J0IG91dGVySFRNTFxuICAgICAgICAgIC8vICAgSXQgZG9lcywgaG93ZXZlciwgc3VwcG9ydCBpbm5lckhUTUxcbiAgICAgICAgICAvLyAgIFVzZSB0aGUgZm9sbG93aW5nIHRvIHJlbmRlciB0aGUgZWxlbWVudFxuICAgICAgICAgIHZhciBucyA9IFwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbFwiO1xuICAgICAgICAgIHZhciBjb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50TlMobnMsICdfJyk7XG5cbiAgICAgICAgICBjb250YWluZXIuYXBwZW5kQ2hpbGQodmFsdWUuY2xvbmVOb2RlKGZhbHNlKSk7XG4gICAgICAgICAgdmFyIGh0bWwgPSBjb250YWluZXIuaW5uZXJIVE1MXG4gICAgICAgICAgICAucmVwbGFjZSgnPjwnLCAnPicgKyB2YWx1ZS5pbm5lckhUTUwgKyAnPCcpO1xuICAgICAgICAgIGNvbnRhaW5lci5pbm5lckhUTUwgPSAnJztcbiAgICAgICAgICByZXR1cm4gaHRtbDtcbiAgICAgICAgfVxuICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgIC8vIFRoaXMgY291bGQgYmUgYSBub24tbmF0aXZlIERPTSBpbXBsZW1lbnRhdGlvbixcbiAgICAgICAgLy8gICBjb250aW51ZSB3aXRoIHRoZSBub3JtYWwgZmxvdzpcbiAgICAgICAgLy8gICBwcmludGluZyB0aGUgZWxlbWVudCBhcyBpZiBpdCBpcyBhbiBvYmplY3QuXG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy8gTG9vayB1cCB0aGUga2V5cyBvZiB0aGUgb2JqZWN0LlxuICB2YXIgdmlzaWJsZUtleXMgPSBnZXRFbnVtZXJhYmxlUHJvcGVydGllcyh2YWx1ZSk7XG4gIHZhciBrZXlzID0gY3R4LnNob3dIaWRkZW4gPyBnZXRQcm9wZXJ0aWVzKHZhbHVlKSA6IHZpc2libGVLZXlzO1xuXG4gIHZhciBuYW1lLCBuYW1lU3VmZml4O1xuXG4gIC8vIFNvbWUgdHlwZSBvZiBvYmplY3Qgd2l0aG91dCBwcm9wZXJ0aWVzIGNhbiBiZSBzaG9ydGN1dC5cbiAgLy8gSW4gSUUsIGVycm9ycyBoYXZlIGEgc2luZ2xlIGBzdGFja2AgcHJvcGVydHksIG9yIGlmIHRoZXkgYXJlIHZhbmlsbGEgYEVycm9yYCxcbiAgLy8gYSBgc3RhY2tgIHBsdXMgYGRlc2NyaXB0aW9uYCBwcm9wZXJ0eTsgaWdub3JlIHRob3NlIGZvciBjb25zaXN0ZW5jeS5cbiAgaWYgKGtleXMubGVuZ3RoID09PSAwIHx8IChpc0Vycm9yKHZhbHVlKSAmJiAoXG4gICAgICAoa2V5cy5sZW5ndGggPT09IDEgJiYga2V5c1swXSA9PT0gJ3N0YWNrJykgfHxcbiAgICAgIChrZXlzLmxlbmd0aCA9PT0gMiAmJiBrZXlzWzBdID09PSAnZGVzY3JpcHRpb24nICYmIGtleXNbMV0gPT09ICdzdGFjaycpXG4gICAgICkpKSB7XG4gICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgbmFtZSA9IGdldE5hbWUodmFsdWUpO1xuICAgICAgbmFtZVN1ZmZpeCA9IG5hbWUgPyAnOiAnICsgbmFtZSA6ICcnO1xuICAgICAgcmV0dXJuIGN0eC5zdHlsaXplKCdbRnVuY3Rpb24nICsgbmFtZVN1ZmZpeCArICddJywgJ3NwZWNpYWwnKTtcbiAgICB9XG4gICAgaWYgKGlzUmVnRXhwKHZhbHVlKSkge1xuICAgICAgcmV0dXJuIGN0eC5zdHlsaXplKFJlZ0V4cC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSksICdyZWdleHAnKTtcbiAgICB9XG4gICAgaWYgKGlzRGF0ZSh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZShEYXRlLnByb3RvdHlwZS50b1VUQ1N0cmluZy5jYWxsKHZhbHVlKSwgJ2RhdGUnKTtcbiAgICB9XG4gICAgaWYgKGlzRXJyb3IodmFsdWUpKSB7XG4gICAgICByZXR1cm4gZm9ybWF0RXJyb3IodmFsdWUpO1xuICAgIH1cbiAgfVxuXG4gIHZhciBiYXNlID0gJydcbiAgICAsIGFycmF5ID0gZmFsc2VcbiAgICAsIHR5cGVkQXJyYXkgPSBmYWxzZVxuICAgICwgYnJhY2VzID0gWyd7JywgJ30nXTtcblxuICBpZiAoaXNUeXBlZEFycmF5KHZhbHVlKSkge1xuICAgIHR5cGVkQXJyYXkgPSB0cnVlO1xuICAgIGJyYWNlcyA9IFsnWycsICddJ107XG4gIH1cblxuICAvLyBNYWtlIEFycmF5IHNheSB0aGF0IHRoZXkgYXJlIEFycmF5XG4gIGlmIChpc0FycmF5KHZhbHVlKSkge1xuICAgIGFycmF5ID0gdHJ1ZTtcbiAgICBicmFjZXMgPSBbJ1snLCAnXSddO1xuICB9XG5cbiAgLy8gTWFrZSBmdW5jdGlvbnMgc2F5IHRoYXQgdGhleSBhcmUgZnVuY3Rpb25zXG4gIGlmICh0eXBlb2YgdmFsdWUgPT09ICdmdW5jdGlvbicpIHtcbiAgICBuYW1lID0gZ2V0TmFtZSh2YWx1ZSk7XG4gICAgbmFtZVN1ZmZpeCA9IG5hbWUgPyAnOiAnICsgbmFtZSA6ICcnO1xuICAgIGJhc2UgPSAnIFtGdW5jdGlvbicgKyBuYW1lU3VmZml4ICsgJ10nO1xuICB9XG5cbiAgLy8gTWFrZSBSZWdFeHBzIHNheSB0aGF0IHRoZXkgYXJlIFJlZ0V4cHNcbiAgaWYgKGlzUmVnRXhwKHZhbHVlKSkge1xuICAgIGJhc2UgPSAnICcgKyBSZWdFeHAucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpO1xuICB9XG5cbiAgLy8gTWFrZSBkYXRlcyB3aXRoIHByb3BlcnRpZXMgZmlyc3Qgc2F5IHRoZSBkYXRlXG4gIGlmIChpc0RhdGUodmFsdWUpKSB7XG4gICAgYmFzZSA9ICcgJyArIERhdGUucHJvdG90eXBlLnRvVVRDU3RyaW5nLmNhbGwodmFsdWUpO1xuICB9XG5cbiAgLy8gTWFrZSBlcnJvciB3aXRoIG1lc3NhZ2UgZmlyc3Qgc2F5IHRoZSBlcnJvclxuICBpZiAoaXNFcnJvcih2YWx1ZSkpIHtcbiAgICByZXR1cm4gZm9ybWF0RXJyb3IodmFsdWUpO1xuICB9XG5cbiAgaWYgKGtleXMubGVuZ3RoID09PSAwICYmICghYXJyYXkgfHwgdmFsdWUubGVuZ3RoID09IDApKSB7XG4gICAgcmV0dXJuIGJyYWNlc1swXSArIGJhc2UgKyBicmFjZXNbMV07XG4gIH1cblxuICBpZiAocmVjdXJzZVRpbWVzIDwgMCkge1xuICAgIGlmIChpc1JlZ0V4cCh2YWx1ZSkpIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZShSZWdFeHAucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpLCAncmVnZXhwJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZSgnW09iamVjdF0nLCAnc3BlY2lhbCcpO1xuICAgIH1cbiAgfVxuXG4gIGN0eC5zZWVuLnB1c2godmFsdWUpO1xuXG4gIHZhciBvdXRwdXQ7XG4gIGlmIChhcnJheSkge1xuICAgIG91dHB1dCA9IGZvcm1hdEFycmF5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsIGtleXMpO1xuICB9IGVsc2UgaWYgKHR5cGVkQXJyYXkpIHtcbiAgICByZXR1cm4gZm9ybWF0VHlwZWRBcnJheSh2YWx1ZSk7XG4gIH0gZWxzZSB7XG4gICAgb3V0cHV0ID0ga2V5cy5tYXAoZnVuY3Rpb24oa2V5KSB7XG4gICAgICByZXR1cm4gZm9ybWF0UHJvcGVydHkoY3R4LCB2YWx1ZSwgcmVjdXJzZVRpbWVzLCB2aXNpYmxlS2V5cywga2V5LCBhcnJheSk7XG4gICAgfSk7XG4gIH1cblxuICBjdHguc2Vlbi5wb3AoKTtcblxuICByZXR1cm4gcmVkdWNlVG9TaW5nbGVTdHJpbmcob3V0cHV0LCBiYXNlLCBicmFjZXMpO1xufVxuXG5mdW5jdGlvbiBmb3JtYXRQcmltaXRpdmUoY3R4LCB2YWx1ZSkge1xuICBzd2l0Y2ggKHR5cGVvZiB2YWx1ZSkge1xuICAgIGNhc2UgJ3VuZGVmaW5lZCc6XG4gICAgICByZXR1cm4gY3R4LnN0eWxpemUoJ3VuZGVmaW5lZCcsICd1bmRlZmluZWQnKTtcblxuICAgIGNhc2UgJ3N0cmluZyc6XG4gICAgICB2YXIgc2ltcGxlID0gJ1xcJycgKyBKU09OLnN0cmluZ2lmeSh2YWx1ZSkucmVwbGFjZSgvXlwifFwiJC9nLCAnJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoLycvZywgXCJcXFxcJ1wiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxcXFwiL2csICdcIicpICsgJ1xcJyc7XG4gICAgICByZXR1cm4gY3R4LnN0eWxpemUoc2ltcGxlLCAnc3RyaW5nJyk7XG5cbiAgICBjYXNlICdudW1iZXInOlxuICAgICAgaWYgKHZhbHVlID09PSAwICYmICgxL3ZhbHVlKSA9PT0gLUluZmluaXR5KSB7XG4gICAgICAgIHJldHVybiBjdHguc3R5bGl6ZSgnLTAnLCAnbnVtYmVyJyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gY3R4LnN0eWxpemUoJycgKyB2YWx1ZSwgJ251bWJlcicpO1xuXG4gICAgY2FzZSAnYm9vbGVhbic6XG4gICAgICByZXR1cm4gY3R4LnN0eWxpemUoJycgKyB2YWx1ZSwgJ2Jvb2xlYW4nKTtcblxuICAgIGNhc2UgJ3N5bWJvbCc6XG4gICAgICByZXR1cm4gY3R4LnN0eWxpemUodmFsdWUudG9TdHJpbmcoKSwgJ3N5bWJvbCcpO1xuXG4gICAgY2FzZSAnYmlnaW50JzpcbiAgICAgIHJldHVybiBjdHguc3R5bGl6ZSh2YWx1ZS50b1N0cmluZygpICsgJ24nLCAnYmlnaW50Jyk7XG4gIH1cbiAgLy8gRm9yIHNvbWUgcmVhc29uIHR5cGVvZiBudWxsIGlzIFwib2JqZWN0XCIsIHNvIHNwZWNpYWwgY2FzZSBoZXJlLlxuICBpZiAodmFsdWUgPT09IG51bGwpIHtcbiAgICByZXR1cm4gY3R4LnN0eWxpemUoJ251bGwnLCAnbnVsbCcpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGZvcm1hdEVycm9yKHZhbHVlKSB7XG4gIHJldHVybiAnWycgKyBFcnJvci5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSkgKyAnXSc7XG59XG5cbmZ1bmN0aW9uIGZvcm1hdEFycmF5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsIGtleXMpIHtcbiAgdmFyIG91dHB1dCA9IFtdO1xuICBmb3IgKHZhciBpID0gMCwgbCA9IHZhbHVlLmxlbmd0aDsgaSA8IGw7ICsraSkge1xuICAgIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwodmFsdWUsIFN0cmluZyhpKSkpIHtcbiAgICAgIG91dHB1dC5wdXNoKGZvcm1hdFByb3BlcnR5KGN0eCwgdmFsdWUsIHJlY3Vyc2VUaW1lcywgdmlzaWJsZUtleXMsXG4gICAgICAgICAgU3RyaW5nKGkpLCB0cnVlKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG91dHB1dC5wdXNoKCcnKTtcbiAgICB9XG4gIH1cblxuICBrZXlzLmZvckVhY2goZnVuY3Rpb24oa2V5KSB7XG4gICAgaWYgKCFrZXkubWF0Y2goL15cXGQrJC8pKSB7XG4gICAgICBvdXRwdXQucHVzaChmb3JtYXRQcm9wZXJ0eShjdHgsIHZhbHVlLCByZWN1cnNlVGltZXMsIHZpc2libGVLZXlzLFxuICAgICAgICAgIGtleSwgdHJ1ZSkpO1xuICAgIH1cbiAgfSk7XG4gIHJldHVybiBvdXRwdXQ7XG59XG5cbmZ1bmN0aW9uIGZvcm1hdFR5cGVkQXJyYXkodmFsdWUpIHtcbiAgdmFyIHN0ciA9ICdbICc7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCB2YWx1ZS5sZW5ndGg7ICsraSkge1xuICAgIGlmIChzdHIubGVuZ3RoID49IGNvbmZpZy50cnVuY2F0ZVRocmVzaG9sZCAtIDcpIHtcbiAgICAgIHN0ciArPSAnLi4uJztcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgICBzdHIgKz0gdmFsdWVbaV0gKyAnLCAnO1xuICB9XG4gIHN0ciArPSAnIF0nO1xuXG4gIC8vIFJlbW92aW5nIHRyYWlsaW5nIGAsIGAgaWYgdGhlIGFycmF5IHdhcyBub3QgdHJ1bmNhdGVkXG4gIGlmIChzdHIuaW5kZXhPZignLCAgXScpICE9PSAtMSkge1xuICAgIHN0ciA9IHN0ci5yZXBsYWNlKCcsICBdJywgJyBdJyk7XG4gIH1cblxuICByZXR1cm4gc3RyO1xufVxuXG5mdW5jdGlvbiBmb3JtYXRQcm9wZXJ0eShjdHgsIHZhbHVlLCByZWN1cnNlVGltZXMsIHZpc2libGVLZXlzLCBrZXksIGFycmF5KSB7XG4gIHZhciBuYW1lO1xuICB2YXIgcHJvcERlc2NyaXB0b3IgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHZhbHVlLCBrZXkpO1xuICB2YXIgc3RyO1xuXG4gIGlmIChwcm9wRGVzY3JpcHRvcikge1xuICAgIGlmIChwcm9wRGVzY3JpcHRvci5nZXQpIHtcbiAgICAgIGlmIChwcm9wRGVzY3JpcHRvci5zZXQpIHtcbiAgICAgICAgc3RyID0gY3R4LnN0eWxpemUoJ1tHZXR0ZXIvU2V0dGVyXScsICdzcGVjaWFsJyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzdHIgPSBjdHguc3R5bGl6ZSgnW0dldHRlcl0nLCAnc3BlY2lhbCcpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBpZiAocHJvcERlc2NyaXB0b3Iuc2V0KSB7XG4gICAgICAgIHN0ciA9IGN0eC5zdHlsaXplKCdbU2V0dGVyXScsICdzcGVjaWFsJyk7XG4gICAgICB9XG4gICAgfVxuICB9XG4gIGlmICh2aXNpYmxlS2V5cy5pbmRleE9mKGtleSkgPCAwKSB7XG4gICAgbmFtZSA9ICdbJyArIGtleSArICddJztcbiAgfVxuICBpZiAoIXN0cikge1xuICAgIGlmIChjdHguc2Vlbi5pbmRleE9mKHZhbHVlW2tleV0pIDwgMCkge1xuICAgICAgaWYgKHJlY3Vyc2VUaW1lcyA9PT0gbnVsbCkge1xuICAgICAgICBzdHIgPSBmb3JtYXRWYWx1ZShjdHgsIHZhbHVlW2tleV0sIG51bGwpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc3RyID0gZm9ybWF0VmFsdWUoY3R4LCB2YWx1ZVtrZXldLCByZWN1cnNlVGltZXMgLSAxKTtcbiAgICAgIH1cbiAgICAgIGlmIChzdHIuaW5kZXhPZignXFxuJykgPiAtMSkge1xuICAgICAgICBpZiAoYXJyYXkpIHtcbiAgICAgICAgICBzdHIgPSBzdHIuc3BsaXQoJ1xcbicpLm1hcChmdW5jdGlvbihsaW5lKSB7XG4gICAgICAgICAgICByZXR1cm4gJyAgJyArIGxpbmU7XG4gICAgICAgICAgfSkuam9pbignXFxuJykuc3Vic3RyKDIpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHN0ciA9ICdcXG4nICsgc3RyLnNwbGl0KCdcXG4nKS5tYXAoZnVuY3Rpb24obGluZSkge1xuICAgICAgICAgICAgcmV0dXJuICcgICAnICsgbGluZTtcbiAgICAgICAgICB9KS5qb2luKCdcXG4nKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBzdHIgPSBjdHguc3R5bGl6ZSgnW0NpcmN1bGFyXScsICdzcGVjaWFsJyk7XG4gICAgfVxuICB9XG4gIGlmICh0eXBlb2YgbmFtZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBpZiAoYXJyYXkgJiYga2V5Lm1hdGNoKC9eXFxkKyQvKSkge1xuICAgICAgcmV0dXJuIHN0cjtcbiAgICB9XG4gICAgbmFtZSA9IEpTT04uc3RyaW5naWZ5KCcnICsga2V5KTtcbiAgICBpZiAobmFtZS5tYXRjaCgvXlwiKFthLXpBLVpfXVthLXpBLVpfMC05XSopXCIkLykpIHtcbiAgICAgIG5hbWUgPSBuYW1lLnN1YnN0cigxLCBuYW1lLmxlbmd0aCAtIDIpO1xuICAgICAgbmFtZSA9IGN0eC5zdHlsaXplKG5hbWUsICduYW1lJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIG5hbWUgPSBuYW1lLnJlcGxhY2UoLycvZywgXCJcXFxcJ1wiKVxuICAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxcXFwiL2csICdcIicpXG4gICAgICAgICAgICAgICAgIC5yZXBsYWNlKC8oXlwifFwiJCkvZywgXCInXCIpO1xuICAgICAgbmFtZSA9IGN0eC5zdHlsaXplKG5hbWUsICdzdHJpbmcnKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbmFtZSArICc6ICcgKyBzdHI7XG59XG5cbmZ1bmN0aW9uIHJlZHVjZVRvU2luZ2xlU3RyaW5nKG91dHB1dCwgYmFzZSwgYnJhY2VzKSB7XG4gIHZhciBsZW5ndGggPSBvdXRwdXQucmVkdWNlKGZ1bmN0aW9uKHByZXYsIGN1cikge1xuICAgIHJldHVybiBwcmV2ICsgY3VyLmxlbmd0aCArIDE7XG4gIH0sIDApO1xuXG4gIGlmIChsZW5ndGggPiA2MCkge1xuICAgIHJldHVybiBicmFjZXNbMF0gK1xuICAgICAgICAgICAoYmFzZSA9PT0gJycgPyAnJyA6IGJhc2UgKyAnXFxuICcpICtcbiAgICAgICAgICAgJyAnICtcbiAgICAgICAgICAgb3V0cHV0LmpvaW4oJyxcXG4gICcpICtcbiAgICAgICAgICAgJyAnICtcbiAgICAgICAgICAgYnJhY2VzWzFdO1xuICB9XG5cbiAgcmV0dXJuIGJyYWNlc1swXSArIGJhc2UgKyAnICcgKyBvdXRwdXQuam9pbignLCAnKSArICcgJyArIGJyYWNlc1sxXTtcbn1cblxuZnVuY3Rpb24gaXNUeXBlZEFycmF5KGFyKSB7XG4gIC8vIFVuZm9ydHVuYXRlbHkgdGhlcmUncyBubyB3YXkgdG8gY2hlY2sgaWYgYW4gb2JqZWN0IGlzIGEgVHlwZWRBcnJheVxuICAvLyBXZSBoYXZlIHRvIGNoZWNrIGlmIGl0J3Mgb25lIG9mIHRoZXNlIHR5cGVzXG4gIHJldHVybiAodHlwZW9mIGFyID09PSAnb2JqZWN0JyAmJiAvXFx3K0FycmF5XSQvLnRlc3Qob2JqZWN0VG9TdHJpbmcoYXIpKSk7XG59XG5cbmZ1bmN0aW9uIGlzQXJyYXkoYXIpIHtcbiAgcmV0dXJuIEFycmF5LmlzQXJyYXkoYXIpIHx8XG4gICAgICAgICAodHlwZW9mIGFyID09PSAnb2JqZWN0JyAmJiBvYmplY3RUb1N0cmluZyhhcikgPT09ICdbb2JqZWN0IEFycmF5XScpO1xufVxuXG5mdW5jdGlvbiBpc1JlZ0V4cChyZSkge1xuICByZXR1cm4gdHlwZW9mIHJlID09PSAnb2JqZWN0JyAmJiBvYmplY3RUb1N0cmluZyhyZSkgPT09ICdbb2JqZWN0IFJlZ0V4cF0nO1xufVxuXG5mdW5jdGlvbiBpc0RhdGUoZCkge1xuICByZXR1cm4gdHlwZW9mIGQgPT09ICdvYmplY3QnICYmIG9iamVjdFRvU3RyaW5nKGQpID09PSAnW29iamVjdCBEYXRlXSc7XG59XG5cbmZ1bmN0aW9uIGlzRXJyb3IoZSkge1xuICByZXR1cm4gdHlwZW9mIGUgPT09ICdvYmplY3QnICYmIG9iamVjdFRvU3RyaW5nKGUpID09PSAnW29iamVjdCBFcnJvcl0nO1xufVxuXG5mdW5jdGlvbiBvYmplY3RUb1N0cmluZyhvKSB7XG4gIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobyk7XG59XG4iLCIvKiFcbiAqIENoYWkgLSBpc05hTiB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE1IFNha3RoaXByaXlhbiBWYWlyYW1hbmkgPHRoZWNoYXJnaW5ndm9sY2Fub0BnbWFpbC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuaXNOYU4odmFsdWUpXG4gKlxuICogQ2hlY2tzIGlmIHRoZSBnaXZlbiB2YWx1ZSBpcyBOYU4gb3Igbm90LlxuICpcbiAqICAgICB1dGlscy5pc05hTihOYU4pOyAvLyB0cnVlXG4gKlxuICogQHBhcmFtIHtWYWx1ZX0gVGhlIHZhbHVlIHdoaWNoIGhhcyB0byBiZSBjaGVja2VkIGlmIGl0IGlzIE5hTlxuICogQG5hbWUgaXNOYU5cbiAqIEBhcGkgcHJpdmF0ZVxuICovXG5cbmZ1bmN0aW9uIGlzTmFOKHZhbHVlKSB7XG4gIC8vIFJlZmVyIGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvI3NlYy1pc25hbi1udW1iZXJcbiAgLy8gc2VjdGlvbidzIE5PVEUuXG4gIHJldHVybiB2YWx1ZSAhPT0gdmFsdWU7XG59XG5cbi8vIElmIEVDTUFTY3JpcHQgNidzIE51bWJlci5pc05hTiBpcyBwcmVzZW50LCBwcmVmZXIgdGhhdC5cbm1vZHVsZS5leHBvcnRzID0gTnVtYmVyLmlzTmFOIHx8IGlzTmFOO1xuIiwidmFyIGNvbmZpZyA9IHJlcXVpcmUoJy4uL2NvbmZpZycpO1xuXG4vKiFcbiAqIENoYWkgLSBpc1Byb3h5RW5hYmxlZCBoZWxwZXJcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuaXNQcm94eUVuYWJsZWQoKVxuICpcbiAqIEhlbHBlciBmdW5jdGlvbiB0byBjaGVjayBpZiBDaGFpJ3MgcHJveHkgcHJvdGVjdGlvbiBmZWF0dXJlIGlzIGVuYWJsZWQuIElmXG4gKiBwcm94aWVzIGFyZSB1bnN1cHBvcnRlZCBvciBkaXNhYmxlZCB2aWEgdGhlIHVzZXIncyBDaGFpIGNvbmZpZywgdGhlbiByZXR1cm5cbiAqIGZhbHNlLiBPdGhlcndpc2UsIHJldHVybiB0cnVlLlxuICpcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIGlzUHJveHlFbmFibGVkXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpc1Byb3h5RW5hYmxlZCgpIHtcbiAgcmV0dXJuIGNvbmZpZy51c2VQcm94eSAmJlxuICAgIHR5cGVvZiBQcm94eSAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICB0eXBlb2YgUmVmbGVjdCAhPT0gJ3VuZGVmaW5lZCc7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gZmxhZyB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyohXG4gKiBNb2R1bGUgZGVwZW5kZW5jaWVzXG4gKi9cblxudmFyIGluc3BlY3QgPSByZXF1aXJlKCcuL2luc3BlY3QnKTtcbnZhciBjb25maWcgPSByZXF1aXJlKCcuLi9jb25maWcnKTtcblxuLyoqXG4gKiAjIyMgLm9iakRpc3BsYXkob2JqZWN0KVxuICpcbiAqIERldGVybWluZXMgaWYgYW4gb2JqZWN0IG9yIGFuIGFycmF5IG1hdGNoZXNcbiAqIGNyaXRlcmlhIHRvIGJlIGluc3BlY3RlZCBpbi1saW5lIGZvciBlcnJvclxuICogbWVzc2FnZXMgb3Igc2hvdWxkIGJlIHRydW5jYXRlZC5cbiAqXG4gKiBAcGFyYW0ge01peGVkfSBqYXZhc2NyaXB0IG9iamVjdCB0byBpbnNwZWN0XG4gKiBAbmFtZSBvYmpEaXNwbGF5XG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gb2JqRGlzcGxheShvYmopIHtcbiAgdmFyIHN0ciA9IGluc3BlY3Qob2JqKVxuICAgICwgdHlwZSA9IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChvYmopO1xuXG4gIGlmIChjb25maWcudHJ1bmNhdGVUaHJlc2hvbGQgJiYgc3RyLmxlbmd0aCA+PSBjb25maWcudHJ1bmNhdGVUaHJlc2hvbGQpIHtcbiAgICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgRnVuY3Rpb25dJykge1xuICAgICAgcmV0dXJuICFvYmoubmFtZSB8fCBvYmoubmFtZSA9PT0gJydcbiAgICAgICAgPyAnW0Z1bmN0aW9uXSdcbiAgICAgICAgOiAnW0Z1bmN0aW9uOiAnICsgb2JqLm5hbWUgKyAnXSc7XG4gICAgfSBlbHNlIGlmICh0eXBlID09PSAnW29iamVjdCBBcnJheV0nKSB7XG4gICAgICByZXR1cm4gJ1sgQXJyYXkoJyArIG9iai5sZW5ndGggKyAnKSBdJztcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdbb2JqZWN0IE9iamVjdF0nKSB7XG4gICAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKG9iailcbiAgICAgICAgLCBrc3RyID0ga2V5cy5sZW5ndGggPiAyXG4gICAgICAgICAgPyBrZXlzLnNwbGljZSgwLCAyKS5qb2luKCcsICcpICsgJywgLi4uJ1xuICAgICAgICAgIDoga2V5cy5qb2luKCcsICcpO1xuICAgICAgcmV0dXJuICd7IE9iamVjdCAoJyArIGtzdHIgKyAnKSB9JztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHN0cjtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgcmV0dXJuIHN0cjtcbiAgfVxufTtcbiIsIi8qIVxuICogQ2hhaSAtIG92ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZCB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxudmFyIGNoYWkgPSByZXF1aXJlKCcuLi8uLi9jaGFpJyk7XG52YXIgdHJhbnNmZXJGbGFncyA9IHJlcXVpcmUoJy4vdHJhbnNmZXJGbGFncycpO1xuXG4vKipcbiAqICMjIyAub3ZlcndyaXRlQ2hhaW5hYmxlTWV0aG9kKGN0eCwgbmFtZSwgbWV0aG9kLCBjaGFpbmluZ0JlaGF2aW9yKVxuICpcbiAqIE92ZXJ3cml0ZXMgYW4gYWxyZWFkeSBleGlzdGluZyBjaGFpbmFibGUgbWV0aG9kXG4gKiBhbmQgcHJvdmlkZXMgYWNjZXNzIHRvIHRoZSBwcmV2aW91cyBmdW5jdGlvbiBvclxuICogcHJvcGVydHkuICBNdXN0IHJldHVybiBmdW5jdGlvbnMgdG8gYmUgdXNlZCBmb3JcbiAqIG5hbWUuXG4gKlxuICogICAgIHV0aWxzLm92ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZChjaGFpLkFzc2VydGlvbi5wcm90b3R5cGUsICdsZW5ndGhPZicsXG4gKiAgICAgICBmdW5jdGlvbiAoX3N1cGVyKSB7XG4gKiAgICAgICB9XG4gKiAgICAgLCBmdW5jdGlvbiAoX3N1cGVyKSB7XG4gKiAgICAgICB9XG4gKiAgICAgKTtcbiAqXG4gKiBDYW4gYWxzbyBiZSBhY2Nlc3NlZCBkaXJlY3RseSBmcm9tIGBjaGFpLkFzc2VydGlvbmAuXG4gKlxuICogICAgIGNoYWkuQXNzZXJ0aW9uLm92ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZCgnZm9vJywgZm4sIGZuKTtcbiAqXG4gKiBUaGVuIGNhbiBiZSB1c2VkIGFzIGFueSBvdGhlciBhc3NlcnRpb24uXG4gKlxuICogICAgIGV4cGVjdChteUZvbykudG8uaGF2ZS5sZW5ndGhPZigzKTtcbiAqICAgICBleHBlY3QobXlGb28pLnRvLmhhdmUubGVuZ3RoT2YuYWJvdmUoMyk7XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IGN0eCBvYmplY3Qgd2hvc2UgbWV0aG9kIC8gcHJvcGVydHkgaXMgdG8gYmUgb3ZlcndyaXR0ZW5cbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIG9mIG1ldGhvZCAvIHByb3BlcnR5IHRvIG92ZXJ3cml0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gbWV0aG9kIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIGZ1bmN0aW9uIHRvIGJlIHVzZWQgZm9yIG5hbWVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGNoYWluaW5nQmVoYXZpb3IgZnVuY3Rpb24gdGhhdCByZXR1cm5zIGEgZnVuY3Rpb24gdG8gYmUgdXNlZCBmb3IgcHJvcGVydHlcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIG92ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZFxuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG92ZXJ3cml0ZUNoYWluYWJsZU1ldGhvZChjdHgsIG5hbWUsIG1ldGhvZCwgY2hhaW5pbmdCZWhhdmlvcikge1xuICB2YXIgY2hhaW5hYmxlQmVoYXZpb3IgPSBjdHguX19tZXRob2RzW25hbWVdO1xuXG4gIHZhciBfY2hhaW5pbmdCZWhhdmlvciA9IGNoYWluYWJsZUJlaGF2aW9yLmNoYWluaW5nQmVoYXZpb3I7XG4gIGNoYWluYWJsZUJlaGF2aW9yLmNoYWluaW5nQmVoYXZpb3IgPSBmdW5jdGlvbiBvdmVyd3JpdGluZ0NoYWluYWJsZU1ldGhvZEdldHRlcigpIHtcbiAgICB2YXIgcmVzdWx0ID0gY2hhaW5pbmdCZWhhdmlvcihfY2hhaW5pbmdCZWhhdmlvcikuY2FsbCh0aGlzKTtcbiAgICBpZiAocmVzdWx0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuXG4gICAgdmFyIG5ld0Fzc2VydGlvbiA9IG5ldyBjaGFpLkFzc2VydGlvbigpO1xuICAgIHRyYW5zZmVyRmxhZ3ModGhpcywgbmV3QXNzZXJ0aW9uKTtcbiAgICByZXR1cm4gbmV3QXNzZXJ0aW9uO1xuICB9O1xuXG4gIHZhciBfbWV0aG9kID0gY2hhaW5hYmxlQmVoYXZpb3IubWV0aG9kO1xuICBjaGFpbmFibGVCZWhhdmlvci5tZXRob2QgPSBmdW5jdGlvbiBvdmVyd3JpdGluZ0NoYWluYWJsZU1ldGhvZFdyYXBwZXIoKSB7XG4gICAgdmFyIHJlc3VsdCA9IG1ldGhvZChfbWV0aG9kKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgIGlmIChyZXN1bHQgIT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9XG5cbiAgICB2YXIgbmV3QXNzZXJ0aW9uID0gbmV3IGNoYWkuQXNzZXJ0aW9uKCk7XG4gICAgdHJhbnNmZXJGbGFncyh0aGlzLCBuZXdBc3NlcnRpb24pO1xuICAgIHJldHVybiBuZXdBc3NlcnRpb247XG4gIH07XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gb3ZlcndyaXRlTWV0aG9kIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG52YXIgYWRkTGVuZ3RoR3VhcmQgPSByZXF1aXJlKCcuL2FkZExlbmd0aEd1YXJkJyk7XG52YXIgY2hhaSA9IHJlcXVpcmUoJy4uLy4uL2NoYWknKTtcbnZhciBmbGFnID0gcmVxdWlyZSgnLi9mbGFnJyk7XG52YXIgcHJveGlmeSA9IHJlcXVpcmUoJy4vcHJveGlmeScpO1xudmFyIHRyYW5zZmVyRmxhZ3MgPSByZXF1aXJlKCcuL3RyYW5zZmVyRmxhZ3MnKTtcblxuLyoqXG4gKiAjIyMgLm92ZXJ3cml0ZU1ldGhvZChjdHgsIG5hbWUsIGZuKVxuICpcbiAqIE92ZXJ3cml0ZXMgYW4gYWxyZWFkeSBleGlzdGluZyBtZXRob2QgYW5kIHByb3ZpZGVzXG4gKiBhY2Nlc3MgdG8gcHJldmlvdXMgZnVuY3Rpb24uIE11c3QgcmV0dXJuIGZ1bmN0aW9uXG4gKiB0byBiZSB1c2VkIGZvciBuYW1lLlxuICpcbiAqICAgICB1dGlscy5vdmVyd3JpdGVNZXRob2QoY2hhaS5Bc3NlcnRpb24ucHJvdG90eXBlLCAnZXF1YWwnLCBmdW5jdGlvbiAoX3N1cGVyKSB7XG4gKiAgICAgICByZXR1cm4gZnVuY3Rpb24gKHN0cikge1xuICogICAgICAgICB2YXIgb2JqID0gdXRpbHMuZmxhZyh0aGlzLCAnb2JqZWN0Jyk7XG4gKiAgICAgICAgIGlmIChvYmogaW5zdGFuY2VvZiBGb28pIHtcbiAqICAgICAgICAgICBuZXcgY2hhaS5Bc3NlcnRpb24ob2JqLnZhbHVlKS50by5lcXVhbChzdHIpO1xuICogICAgICAgICB9IGVsc2Uge1xuICogICAgICAgICAgIF9zdXBlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICogICAgICAgICB9XG4gKiAgICAgICB9XG4gKiAgICAgfSk7XG4gKlxuICogQ2FuIGFsc28gYmUgYWNjZXNzZWQgZGlyZWN0bHkgZnJvbSBgY2hhaS5Bc3NlcnRpb25gLlxuICpcbiAqICAgICBjaGFpLkFzc2VydGlvbi5vdmVyd3JpdGVNZXRob2QoJ2ZvbycsIGZuKTtcbiAqXG4gKiBUaGVuIGNhbiBiZSB1c2VkIGFzIGFueSBvdGhlciBhc3NlcnRpb24uXG4gKlxuICogICAgIGV4cGVjdChteUZvbykudG8uZXF1YWwoJ2JhcicpO1xuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBjdHggb2JqZWN0IHdob3NlIG1ldGhvZCBpcyB0byBiZSBvdmVyd3JpdHRlblxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgb2YgbWV0aG9kIHRvIG92ZXJ3cml0ZVxuICogQHBhcmFtIHtGdW5jdGlvbn0gbWV0aG9kIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIGZ1bmN0aW9uIHRvIGJlIHVzZWQgZm9yIG5hbWVcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIG92ZXJ3cml0ZU1ldGhvZFxuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIG92ZXJ3cml0ZU1ldGhvZChjdHgsIG5hbWUsIG1ldGhvZCkge1xuICB2YXIgX21ldGhvZCA9IGN0eFtuYW1lXVxuICAgICwgX3N1cGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKG5hbWUgKyAnIGlzIG5vdCBhIGZ1bmN0aW9uJyk7XG4gICAgfTtcblxuICBpZiAoX21ldGhvZCAmJiAnZnVuY3Rpb24nID09PSB0eXBlb2YgX21ldGhvZClcbiAgICBfc3VwZXIgPSBfbWV0aG9kO1xuXG4gIHZhciBvdmVyd3JpdGluZ01ldGhvZFdyYXBwZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgLy8gU2V0dGluZyB0aGUgYHNzZmlgIGZsYWcgdG8gYG92ZXJ3cml0aW5nTWV0aG9kV3JhcHBlcmAgY2F1c2VzIHRoaXNcbiAgICAvLyBmdW5jdGlvbiB0byBiZSB0aGUgc3RhcnRpbmcgcG9pbnQgZm9yIHJlbW92aW5nIGltcGxlbWVudGF0aW9uIGZyYW1lcyBmcm9tXG4gICAgLy8gdGhlIHN0YWNrIHRyYWNlIG9mIGEgZmFpbGVkIGFzc2VydGlvbi5cbiAgICAvL1xuICAgIC8vIEhvd2V2ZXIsIHdlIG9ubHkgd2FudCB0byB1c2UgdGhpcyBmdW5jdGlvbiBhcyB0aGUgc3RhcnRpbmcgcG9pbnQgaWYgdGhlXG4gICAgLy8gYGxvY2tTc2ZpYCBmbGFnIGlzbid0IHNldC5cbiAgICAvL1xuICAgIC8vIElmIHRoZSBgbG9ja1NzZmlgIGZsYWcgaXMgc2V0LCB0aGVuIGVpdGhlciB0aGlzIGFzc2VydGlvbiBoYXMgYmVlblxuICAgIC8vIG92ZXJ3cml0dGVuIGJ5IGFub3RoZXIgYXNzZXJ0aW9uLCBvciB0aGlzIGFzc2VydGlvbiBpcyBiZWluZyBpbnZva2VkIGZyb21cbiAgICAvLyBpbnNpZGUgb2YgYW5vdGhlciBhc3NlcnRpb24uIEluIHRoZSBmaXJzdCBjYXNlLCB0aGUgYHNzZmlgIGZsYWcgaGFzXG4gICAgLy8gYWxyZWFkeSBiZWVuIHNldCBieSB0aGUgb3ZlcndyaXRpbmcgYXNzZXJ0aW9uLiBJbiB0aGUgc2Vjb25kIGNhc2UsIHRoZVxuICAgIC8vIGBzc2ZpYCBmbGFnIGhhcyBhbHJlYWR5IGJlZW4gc2V0IGJ5IHRoZSBvdXRlciBhc3NlcnRpb24uXG4gICAgaWYgKCFmbGFnKHRoaXMsICdsb2NrU3NmaScpKSB7XG4gICAgICBmbGFnKHRoaXMsICdzc2ZpJywgb3ZlcndyaXRpbmdNZXRob2RXcmFwcGVyKTtcbiAgICB9XG5cbiAgICAvLyBTZXR0aW5nIHRoZSBgbG9ja1NzZmlgIGZsYWcgdG8gYHRydWVgIHByZXZlbnRzIHRoZSBvdmVyd3JpdHRlbiBhc3NlcnRpb25cbiAgICAvLyBmcm9tIGNoYW5naW5nIHRoZSBgc3NmaWAgZmxhZy4gQnkgdGhpcyBwb2ludCwgdGhlIGBzc2ZpYCBmbGFnIGlzIGFscmVhZHlcbiAgICAvLyBzZXQgdG8gdGhlIGNvcnJlY3Qgc3RhcnRpbmcgcG9pbnQgZm9yIHRoaXMgYXNzZXJ0aW9uLlxuICAgIHZhciBvcmlnTG9ja1NzZmkgPSBmbGFnKHRoaXMsICdsb2NrU3NmaScpO1xuICAgIGZsYWcodGhpcywgJ2xvY2tTc2ZpJywgdHJ1ZSk7XG4gICAgdmFyIHJlc3VsdCA9IG1ldGhvZChfc3VwZXIpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgZmxhZyh0aGlzLCAnbG9ja1NzZmknLCBvcmlnTG9ja1NzZmkpO1xuXG4gICAgaWYgKHJlc3VsdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gcmVzdWx0O1xuICAgIH1cblxuICAgIHZhciBuZXdBc3NlcnRpb24gPSBuZXcgY2hhaS5Bc3NlcnRpb24oKTtcbiAgICB0cmFuc2ZlckZsYWdzKHRoaXMsIG5ld0Fzc2VydGlvbik7XG4gICAgcmV0dXJuIG5ld0Fzc2VydGlvbjtcbiAgfVxuXG4gIGFkZExlbmd0aEd1YXJkKG92ZXJ3cml0aW5nTWV0aG9kV3JhcHBlciwgbmFtZSwgZmFsc2UpO1xuICBjdHhbbmFtZV0gPSBwcm94aWZ5KG92ZXJ3cml0aW5nTWV0aG9kV3JhcHBlciwgbmFtZSk7XG59O1xuIiwiLyohXG4gKiBDaGFpIC0gb3ZlcndyaXRlUHJvcGVydHkgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTItMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbnZhciBjaGFpID0gcmVxdWlyZSgnLi4vLi4vY2hhaScpO1xudmFyIGZsYWcgPSByZXF1aXJlKCcuL2ZsYWcnKTtcbnZhciBpc1Byb3h5RW5hYmxlZCA9IHJlcXVpcmUoJy4vaXNQcm94eUVuYWJsZWQnKTtcbnZhciB0cmFuc2ZlckZsYWdzID0gcmVxdWlyZSgnLi90cmFuc2ZlckZsYWdzJyk7XG5cbi8qKlxuICogIyMjIC5vdmVyd3JpdGVQcm9wZXJ0eShjdHgsIG5hbWUsIGZuKVxuICpcbiAqIE92ZXJ3cml0ZXMgYW4gYWxyZWFkeSBleGlzdGluZyBwcm9wZXJ0eSBnZXR0ZXIgYW5kIHByb3ZpZGVzXG4gKiBhY2Nlc3MgdG8gcHJldmlvdXMgdmFsdWUuIE11c3QgcmV0dXJuIGZ1bmN0aW9uIHRvIHVzZSBhcyBnZXR0ZXIuXG4gKlxuICogICAgIHV0aWxzLm92ZXJ3cml0ZVByb3BlcnR5KGNoYWkuQXNzZXJ0aW9uLnByb3RvdHlwZSwgJ29rJywgZnVuY3Rpb24gKF9zdXBlcikge1xuICogICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAqICAgICAgICAgdmFyIG9iaiA9IHV0aWxzLmZsYWcodGhpcywgJ29iamVjdCcpO1xuICogICAgICAgICBpZiAob2JqIGluc3RhbmNlb2YgRm9vKSB7XG4gKiAgICAgICAgICAgbmV3IGNoYWkuQXNzZXJ0aW9uKG9iai5uYW1lKS50by5lcXVhbCgnYmFyJyk7XG4gKiAgICAgICAgIH0gZWxzZSB7XG4gKiAgICAgICAgICAgX3N1cGVyLmNhbGwodGhpcyk7XG4gKiAgICAgICAgIH1cbiAqICAgICAgIH1cbiAqICAgICB9KTtcbiAqXG4gKlxuICogQ2FuIGFsc28gYmUgYWNjZXNzZWQgZGlyZWN0bHkgZnJvbSBgY2hhaS5Bc3NlcnRpb25gLlxuICpcbiAqICAgICBjaGFpLkFzc2VydGlvbi5vdmVyd3JpdGVQcm9wZXJ0eSgnZm9vJywgZm4pO1xuICpcbiAqIFRoZW4gY2FuIGJlIHVzZWQgYXMgYW55IG90aGVyIGFzc2VydGlvbi5cbiAqXG4gKiAgICAgZXhwZWN0KG15Rm9vKS50by5iZS5vaztcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gY3R4IG9iamVjdCB3aG9zZSBwcm9wZXJ0eSBpcyB0byBiZSBvdmVyd3JpdHRlblxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWUgb2YgcHJvcGVydHkgdG8gb3ZlcndyaXRlXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBnZXR0ZXIgZnVuY3Rpb24gdGhhdCByZXR1cm5zIGEgZ2V0dGVyIGZ1bmN0aW9uIHRvIGJlIHVzZWQgZm9yIG5hbWVcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIG92ZXJ3cml0ZVByb3BlcnR5XG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gb3ZlcndyaXRlUHJvcGVydHkoY3R4LCBuYW1lLCBnZXR0ZXIpIHtcbiAgdmFyIF9nZXQgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGN0eCwgbmFtZSlcbiAgICAsIF9zdXBlciA9IGZ1bmN0aW9uICgpIHt9O1xuXG4gIGlmIChfZ2V0ICYmICdmdW5jdGlvbicgPT09IHR5cGVvZiBfZ2V0LmdldClcbiAgICBfc3VwZXIgPSBfZ2V0LmdldFxuXG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjdHgsIG5hbWUsXG4gICAgeyBnZXQ6IGZ1bmN0aW9uIG92ZXJ3cml0aW5nUHJvcGVydHlHZXR0ZXIoKSB7XG4gICAgICAgIC8vIFNldHRpbmcgdGhlIGBzc2ZpYCBmbGFnIHRvIGBvdmVyd3JpdGluZ1Byb3BlcnR5R2V0dGVyYCBjYXVzZXMgdGhpc1xuICAgICAgICAvLyBmdW5jdGlvbiB0byBiZSB0aGUgc3RhcnRpbmcgcG9pbnQgZm9yIHJlbW92aW5nIGltcGxlbWVudGF0aW9uIGZyYW1lc1xuICAgICAgICAvLyBmcm9tIHRoZSBzdGFjayB0cmFjZSBvZiBhIGZhaWxlZCBhc3NlcnRpb24uXG4gICAgICAgIC8vXG4gICAgICAgIC8vIEhvd2V2ZXIsIHdlIG9ubHkgd2FudCB0byB1c2UgdGhpcyBmdW5jdGlvbiBhcyB0aGUgc3RhcnRpbmcgcG9pbnQgaWZcbiAgICAgICAgLy8gdGhlIGBsb2NrU3NmaWAgZmxhZyBpc24ndCBzZXQgYW5kIHByb3h5IHByb3RlY3Rpb24gaXMgZGlzYWJsZWQuXG4gICAgICAgIC8vXG4gICAgICAgIC8vIElmIHRoZSBgbG9ja1NzZmlgIGZsYWcgaXMgc2V0LCB0aGVuIGVpdGhlciB0aGlzIGFzc2VydGlvbiBoYXMgYmVlblxuICAgICAgICAvLyBvdmVyd3JpdHRlbiBieSBhbm90aGVyIGFzc2VydGlvbiwgb3IgdGhpcyBhc3NlcnRpb24gaXMgYmVpbmcgaW52b2tlZFxuICAgICAgICAvLyBmcm9tIGluc2lkZSBvZiBhbm90aGVyIGFzc2VydGlvbi4gSW4gdGhlIGZpcnN0IGNhc2UsIHRoZSBgc3NmaWAgZmxhZ1xuICAgICAgICAvLyBoYXMgYWxyZWFkeSBiZWVuIHNldCBieSB0aGUgb3ZlcndyaXRpbmcgYXNzZXJ0aW9uLiBJbiB0aGUgc2Vjb25kXG4gICAgICAgIC8vIGNhc2UsIHRoZSBgc3NmaWAgZmxhZyBoYXMgYWxyZWFkeSBiZWVuIHNldCBieSB0aGUgb3V0ZXIgYXNzZXJ0aW9uLlxuICAgICAgICAvL1xuICAgICAgICAvLyBJZiBwcm94eSBwcm90ZWN0aW9uIGlzIGVuYWJsZWQsIHRoZW4gdGhlIGBzc2ZpYCBmbGFnIGhhcyBhbHJlYWR5IGJlZW5cbiAgICAgICAgLy8gc2V0IGJ5IHRoZSBwcm94eSBnZXR0ZXIuXG4gICAgICAgIGlmICghaXNQcm94eUVuYWJsZWQoKSAmJiAhZmxhZyh0aGlzLCAnbG9ja1NzZmknKSkge1xuICAgICAgICAgIGZsYWcodGhpcywgJ3NzZmknLCBvdmVyd3JpdGluZ1Byb3BlcnR5R2V0dGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFNldHRpbmcgdGhlIGBsb2NrU3NmaWAgZmxhZyB0byBgdHJ1ZWAgcHJldmVudHMgdGhlIG92ZXJ3cml0dGVuXG4gICAgICAgIC8vIGFzc2VydGlvbiBmcm9tIGNoYW5naW5nIHRoZSBgc3NmaWAgZmxhZy4gQnkgdGhpcyBwb2ludCwgdGhlIGBzc2ZpYFxuICAgICAgICAvLyBmbGFnIGlzIGFscmVhZHkgc2V0IHRvIHRoZSBjb3JyZWN0IHN0YXJ0aW5nIHBvaW50IGZvciB0aGlzIGFzc2VydGlvbi5cbiAgICAgICAgdmFyIG9yaWdMb2NrU3NmaSA9IGZsYWcodGhpcywgJ2xvY2tTc2ZpJyk7XG4gICAgICAgIGZsYWcodGhpcywgJ2xvY2tTc2ZpJywgdHJ1ZSk7XG4gICAgICAgIHZhciByZXN1bHQgPSBnZXR0ZXIoX3N1cGVyKS5jYWxsKHRoaXMpO1xuICAgICAgICBmbGFnKHRoaXMsICdsb2NrU3NmaScsIG9yaWdMb2NrU3NmaSk7XG5cbiAgICAgICAgaWYgKHJlc3VsdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBuZXdBc3NlcnRpb24gPSBuZXcgY2hhaS5Bc3NlcnRpb24oKTtcbiAgICAgICAgdHJhbnNmZXJGbGFncyh0aGlzLCBuZXdBc3NlcnRpb24pO1xuICAgICAgICByZXR1cm4gbmV3QXNzZXJ0aW9uO1xuICAgICAgfVxuICAgICwgY29uZmlndXJhYmxlOiB0cnVlXG4gIH0pO1xufTtcbiIsInZhciBjb25maWcgPSByZXF1aXJlKCcuLi9jb25maWcnKTtcbnZhciBmbGFnID0gcmVxdWlyZSgnLi9mbGFnJyk7XG52YXIgZ2V0UHJvcGVydGllcyA9IHJlcXVpcmUoJy4vZ2V0UHJvcGVydGllcycpO1xudmFyIGlzUHJveHlFbmFibGVkID0gcmVxdWlyZSgnLi9pc1Byb3h5RW5hYmxlZCcpO1xuXG4vKiFcbiAqIENoYWkgLSBwcm94aWZ5IHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAucHJveGlmeShvYmplY3QpXG4gKlxuICogUmV0dXJuIGEgcHJveHkgb2YgZ2l2ZW4gb2JqZWN0IHRoYXQgdGhyb3dzIGFuIGVycm9yIHdoZW4gYSBub24tZXhpc3RlbnRcbiAqIHByb3BlcnR5IGlzIHJlYWQuIEJ5IGRlZmF1bHQsIHRoZSByb290IGNhdXNlIGlzIGFzc3VtZWQgdG8gYmUgYSBtaXNzcGVsbGVkXG4gKiBwcm9wZXJ0eSwgYW5kIHRodXMgYW4gYXR0ZW1wdCBpcyBtYWRlIHRvIG9mZmVyIGEgcmVhc29uYWJsZSBzdWdnZXN0aW9uIGZyb21cbiAqIHRoZSBsaXN0IG9mIGV4aXN0aW5nIHByb3BlcnRpZXMuIEhvd2V2ZXIsIGlmIGEgbm9uQ2hhaW5hYmxlTWV0aG9kTmFtZSBpc1xuICogcHJvdmlkZWQsIHRoZW4gdGhlIHJvb3QgY2F1c2UgaXMgaW5zdGVhZCBhIGZhaWx1cmUgdG8gaW52b2tlIGEgbm9uLWNoYWluYWJsZVxuICogbWV0aG9kIHByaW9yIHRvIHJlYWRpbmcgdGhlIG5vbi1leGlzdGVudCBwcm9wZXJ0eS5cbiAqXG4gKiBJZiBwcm94aWVzIGFyZSB1bnN1cHBvcnRlZCBvciBkaXNhYmxlZCB2aWEgdGhlIHVzZXIncyBDaGFpIGNvbmZpZywgdGhlblxuICogcmV0dXJuIG9iamVjdCB3aXRob3V0IG1vZGlmaWNhdGlvbi5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqXG4gKiBAcGFyYW0ge1N0cmluZ30gbm9uQ2hhaW5hYmxlTWV0aG9kTmFtZVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgcHJveGlmeVxuICovXG5cbnZhciBidWlsdGlucyA9IFsnX19mbGFncycsICdfX21ldGhvZHMnLCAnX29iaicsICdhc3NlcnQnXTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBwcm94aWZ5KG9iaiwgbm9uQ2hhaW5hYmxlTWV0aG9kTmFtZSkge1xuICBpZiAoIWlzUHJveHlFbmFibGVkKCkpIHJldHVybiBvYmo7XG5cbiAgcmV0dXJuIG5ldyBQcm94eShvYmosIHtcbiAgICBnZXQ6IGZ1bmN0aW9uIHByb3h5R2V0dGVyKHRhcmdldCwgcHJvcGVydHkpIHtcbiAgICAgIC8vIFRoaXMgY2hlY2sgaXMgaGVyZSBiZWNhdXNlIHdlIHNob3VsZCBub3QgdGhyb3cgZXJyb3JzIG9uIFN5bWJvbCBwcm9wZXJ0aWVzXG4gICAgICAvLyBzdWNoIGFzIGBTeW1ib2wudG9TdHJpbmdUYWdgLlxuICAgICAgLy8gVGhlIHZhbHVlcyBmb3Igd2hpY2ggYW4gZXJyb3Igc2hvdWxkIGJlIHRocm93biBjYW4gYmUgY29uZmlndXJlZCB1c2luZ1xuICAgICAgLy8gdGhlIGBjb25maWcucHJveHlFeGNsdWRlZEtleXNgIHNldHRpbmcuXG4gICAgICBpZiAodHlwZW9mIHByb3BlcnR5ID09PSAnc3RyaW5nJyAmJlxuICAgICAgICAgIGNvbmZpZy5wcm94eUV4Y2x1ZGVkS2V5cy5pbmRleE9mKHByb3BlcnR5KSA9PT0gLTEgJiZcbiAgICAgICAgICAhUmVmbGVjdC5oYXModGFyZ2V0LCBwcm9wZXJ0eSkpIHtcbiAgICAgICAgLy8gU3BlY2lhbCBtZXNzYWdlIGZvciBpbnZhbGlkIHByb3BlcnR5IGFjY2VzcyBvZiBub24tY2hhaW5hYmxlIG1ldGhvZHMuXG4gICAgICAgIGlmIChub25DaGFpbmFibGVNZXRob2ROYW1lKSB7XG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgQ2hhaSBwcm9wZXJ0eTogJyArIG5vbkNoYWluYWJsZU1ldGhvZE5hbWUgKyAnLicgK1xuICAgICAgICAgICAgcHJvcGVydHkgKyAnLiBTZWUgZG9jcyBmb3IgcHJvcGVyIHVzYWdlIG9mIFwiJyArXG4gICAgICAgICAgICBub25DaGFpbmFibGVNZXRob2ROYW1lICsgJ1wiLicpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gSWYgdGhlIHByb3BlcnR5IGlzIHJlYXNvbmFibHkgY2xvc2UgdG8gYW4gZXhpc3RpbmcgQ2hhaSBwcm9wZXJ0eSxcbiAgICAgICAgLy8gc3VnZ2VzdCB0aGF0IHByb3BlcnR5IHRvIHRoZSB1c2VyLiBPbmx5IHN1Z2dlc3QgcHJvcGVydGllcyB3aXRoIGFcbiAgICAgICAgLy8gZGlzdGFuY2UgbGVzcyB0aGFuIDQuXG4gICAgICAgIHZhciBzdWdnZXN0aW9uID0gbnVsbDtcbiAgICAgICAgdmFyIHN1Z2dlc3Rpb25EaXN0YW5jZSA9IDQ7XG4gICAgICAgIGdldFByb3BlcnRpZXModGFyZ2V0KS5mb3JFYWNoKGZ1bmN0aW9uKHByb3ApIHtcbiAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAhT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eShwcm9wKSAmJlxuICAgICAgICAgICAgYnVpbHRpbnMuaW5kZXhPZihwcm9wKSA9PT0gLTFcbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIHZhciBkaXN0ID0gc3RyaW5nRGlzdGFuY2VDYXBwZWQoXG4gICAgICAgICAgICAgIHByb3BlcnR5LFxuICAgICAgICAgICAgICBwcm9wLFxuICAgICAgICAgICAgICBzdWdnZXN0aW9uRGlzdGFuY2VcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAoZGlzdCA8IHN1Z2dlc3Rpb25EaXN0YW5jZSkge1xuICAgICAgICAgICAgICBzdWdnZXN0aW9uID0gcHJvcDtcbiAgICAgICAgICAgICAgc3VnZ2VzdGlvbkRpc3RhbmNlID0gZGlzdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChzdWdnZXN0aW9uICE9PSBudWxsKSB7XG4gICAgICAgICAgdGhyb3cgRXJyb3IoJ0ludmFsaWQgQ2hhaSBwcm9wZXJ0eTogJyArIHByb3BlcnR5ICtcbiAgICAgICAgICAgICcuIERpZCB5b3UgbWVhbiBcIicgKyBzdWdnZXN0aW9uICsgJ1wiPycpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRocm93IEVycm9yKCdJbnZhbGlkIENoYWkgcHJvcGVydHk6ICcgKyBwcm9wZXJ0eSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gVXNlIHRoaXMgcHJveHkgZ2V0dGVyIGFzIHRoZSBzdGFydGluZyBwb2ludCBmb3IgcmVtb3ZpbmcgaW1wbGVtZW50YXRpb25cbiAgICAgIC8vIGZyYW1lcyBmcm9tIHRoZSBzdGFjayB0cmFjZSBvZiBhIGZhaWxlZCBhc3NlcnRpb24uIEZvciBwcm9wZXJ0eVxuICAgICAgLy8gYXNzZXJ0aW9ucywgdGhpcyBwcmV2ZW50cyB0aGUgcHJveHkgZ2V0dGVyIGZyb20gc2hvd2luZyB1cCBpbiB0aGUgc3RhY2tcbiAgICAgIC8vIHRyYWNlIHNpbmNlIGl0J3MgaW52b2tlZCBiZWZvcmUgdGhlIHByb3BlcnR5IGdldHRlci4gRm9yIG1ldGhvZCBhbmRcbiAgICAgIC8vIGNoYWluYWJsZSBtZXRob2QgYXNzZXJ0aW9ucywgdGhpcyBmbGFnIHdpbGwgZW5kIHVwIGdldHRpbmcgY2hhbmdlZCB0b1xuICAgICAgLy8gdGhlIG1ldGhvZCB3cmFwcGVyLCB3aGljaCBpcyBnb29kIHNpbmNlIHRoaXMgZnJhbWUgd2lsbCBubyBsb25nZXIgYmUgaW5cbiAgICAgIC8vIHRoZSBzdGFjayBvbmNlIHRoZSBtZXRob2QgaXMgaW52b2tlZC4gTm90ZSB0aGF0IENoYWkgYnVpbHRpbiBhc3NlcnRpb25cbiAgICAgIC8vIHByb3BlcnRpZXMgc3VjaCBhcyBgX19mbGFnc2AgYXJlIHNraXBwZWQgc2luY2UgdGhpcyBpcyBvbmx5IG1lYW50IHRvXG4gICAgICAvLyBjYXB0dXJlIHRoZSBzdGFydGluZyBwb2ludCBvZiBhbiBhc3NlcnRpb24uIFRoaXMgc3RlcCBpcyBhbHNvIHNraXBwZWRcbiAgICAgIC8vIGlmIHRoZSBgbG9ja1NzZmlgIGZsYWcgaXMgc2V0LCB0aHVzIGluZGljYXRpbmcgdGhhdCB0aGlzIGFzc2VydGlvbiBpc1xuICAgICAgLy8gYmVpbmcgY2FsbGVkIGZyb20gd2l0aGluIGFub3RoZXIgYXNzZXJ0aW9uLiBJbiB0aGF0IGNhc2UsIHRoZSBgc3NmaWBcbiAgICAgIC8vIGZsYWcgaXMgYWxyZWFkeSBzZXQgdG8gdGhlIG91dGVyIGFzc2VydGlvbidzIHN0YXJ0aW5nIHBvaW50LlxuICAgICAgaWYgKGJ1aWx0aW5zLmluZGV4T2YocHJvcGVydHkpID09PSAtMSAmJiAhZmxhZyh0YXJnZXQsICdsb2NrU3NmaScpKSB7XG4gICAgICAgIGZsYWcodGFyZ2V0LCAnc3NmaScsIHByb3h5R2V0dGVyKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIFJlZmxlY3QuZ2V0KHRhcmdldCwgcHJvcGVydHkpO1xuICAgIH1cbiAgfSk7XG59O1xuXG4vKipcbiAqICMgc3RyaW5nRGlzdGFuY2VDYXBwZWQoc3RyQSwgc3RyQiwgY2FwKVxuICogUmV0dXJuIHRoZSBMZXZlbnNodGVpbiBkaXN0YW5jZSBiZXR3ZWVuIHR3byBzdHJpbmdzLCBidXQgbm8gbW9yZSB0aGFuIGNhcC5cbiAqIEBwYXJhbSB7c3RyaW5nfSBzdHJBXG4gKiBAcGFyYW0ge3N0cmluZ30gc3RyQlxuICogQHBhcmFtIHtudW1iZXJ9IG51bWJlclxuICogQHJldHVybiB7bnVtYmVyfSBtaW4oc3RyaW5nIGRpc3RhbmNlIGJldHdlZW4gc3RyQSBhbmQgc3RyQiwgY2FwKVxuICogQGFwaSBwcml2YXRlXG4gKi9cblxuZnVuY3Rpb24gc3RyaW5nRGlzdGFuY2VDYXBwZWQoc3RyQSwgc3RyQiwgY2FwKSB7XG4gIGlmIChNYXRoLmFicyhzdHJBLmxlbmd0aCAtIHN0ckIubGVuZ3RoKSA+PSBjYXApIHtcbiAgICByZXR1cm4gY2FwO1xuICB9XG5cbiAgdmFyIG1lbW8gPSBbXTtcbiAgLy8gYG1lbW9gIGlzIGEgdHdvLWRpbWVuc2lvbmFsIGFycmF5IGNvbnRhaW5pbmcgZGlzdGFuY2VzLlxuICAvLyBtZW1vW2ldW2pdIGlzIHRoZSBkaXN0YW5jZSBiZXR3ZWVuIHN0ckEuc2xpY2UoMCwgaSkgYW5kXG4gIC8vIHN0ckIuc2xpY2UoMCwgaikuXG4gIGZvciAodmFyIGkgPSAwOyBpIDw9IHN0ckEubGVuZ3RoOyBpKyspIHtcbiAgICBtZW1vW2ldID0gQXJyYXkoc3RyQi5sZW5ndGggKyAxKS5maWxsKDApO1xuICAgIG1lbW9baV1bMF0gPSBpO1xuICB9XG4gIGZvciAodmFyIGogPSAwOyBqIDwgc3RyQi5sZW5ndGg7IGorKykge1xuICAgIG1lbW9bMF1bal0gPSBqO1xuICB9XG5cbiAgZm9yICh2YXIgaSA9IDE7IGkgPD0gc3RyQS5sZW5ndGg7IGkrKykge1xuICAgIHZhciBjaCA9IHN0ckEuY2hhckNvZGVBdChpIC0gMSk7XG4gICAgZm9yICh2YXIgaiA9IDE7IGogPD0gc3RyQi5sZW5ndGg7IGorKykge1xuICAgICAgaWYgKE1hdGguYWJzKGkgLSBqKSA+PSBjYXApIHtcbiAgICAgICAgbWVtb1tpXVtqXSA9IGNhcDtcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG4gICAgICBtZW1vW2ldW2pdID0gTWF0aC5taW4oXG4gICAgICAgIG1lbW9baSAtIDFdW2pdICsgMSxcbiAgICAgICAgbWVtb1tpXVtqIC0gMV0gKyAxLFxuICAgICAgICBtZW1vW2kgLSAxXVtqIC0gMV0gK1xuICAgICAgICAgIChjaCA9PT0gc3RyQi5jaGFyQ29kZUF0KGogLSAxKSA/IDAgOiAxKVxuICAgICAgKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWVtb1tzdHJBLmxlbmd0aF1bc3RyQi5sZW5ndGhdO1xufVxuIiwiLyohXG4gKiBDaGFpIC0gdGVzdCB1dGlsaXR5XG4gKiBDb3B5cmlnaHQoYykgMjAxMi0yMDE0IEpha2UgTHVlciA8amFrZUBhbG9naWNhbHBhcmFkb3guY29tPlxuICogTUlUIExpY2Vuc2VkXG4gKi9cblxuLyohXG4gKiBNb2R1bGUgZGVwZW5kZW5jaWVzXG4gKi9cblxudmFyIGZsYWcgPSByZXF1aXJlKCcuL2ZsYWcnKTtcblxuLyoqXG4gKiAjIyMgLnRlc3Qob2JqZWN0LCBleHByZXNzaW9uKVxuICpcbiAqIFRlc3QgYW5kIG9iamVjdCBmb3IgZXhwcmVzc2lvbi5cbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IChjb25zdHJ1Y3RlZCBBc3NlcnRpb24pXG4gKiBAcGFyYW0ge0FyZ3VtZW50c30gY2hhaS5Bc3NlcnRpb24ucHJvdG90eXBlLmFzc2VydCBhcmd1bWVudHNcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIHRlc3RcbiAqL1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRlc3Qob2JqLCBhcmdzKSB7XG4gIHZhciBuZWdhdGUgPSBmbGFnKG9iaiwgJ25lZ2F0ZScpXG4gICAgLCBleHByID0gYXJnc1swXTtcbiAgcmV0dXJuIG5lZ2F0ZSA/ICFleHByIDogZXhwcjtcbn07XG4iLCIvKiFcbiAqIENoYWkgLSB0cmFuc2ZlckZsYWdzIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTQgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAudHJhbnNmZXJGbGFncyhhc3NlcnRpb24sIG9iamVjdCwgaW5jbHVkZUFsbCA9IHRydWUpXG4gKlxuICogVHJhbnNmZXIgYWxsIHRoZSBmbGFncyBmb3IgYGFzc2VydGlvbmAgdG8gYG9iamVjdGAuIElmXG4gKiBgaW5jbHVkZUFsbGAgaXMgc2V0IHRvIGBmYWxzZWAsIHRoZW4gdGhlIGJhc2UgQ2hhaVxuICogYXNzZXJ0aW9uIGZsYWdzIChuYW1lbHkgYG9iamVjdGAsIGBzc2ZpYCwgYGxvY2tTc2ZpYCxcbiAqIGFuZCBgbWVzc2FnZWApIHdpbGwgbm90IGJlIHRyYW5zZmVycmVkLlxuICpcbiAqXG4gKiAgICAgdmFyIG5ld0Fzc2VydGlvbiA9IG5ldyBBc3NlcnRpb24oKTtcbiAqICAgICB1dGlscy50cmFuc2ZlckZsYWdzKGFzc2VydGlvbiwgbmV3QXNzZXJ0aW9uKTtcbiAqXG4gKiAgICAgdmFyIGFub3RoZXJBc3NlcnRpb24gPSBuZXcgQXNzZXJ0aW9uKG15T2JqKTtcbiAqICAgICB1dGlscy50cmFuc2ZlckZsYWdzKGFzc2VydGlvbiwgYW5vdGhlckFzc2VydGlvbiwgZmFsc2UpO1xuICpcbiAqIEBwYXJhbSB7QXNzZXJ0aW9ufSBhc3NlcnRpb24gdGhlIGFzc2VydGlvbiB0byB0cmFuc2ZlciB0aGUgZmxhZ3MgZnJvbVxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCB0aGUgb2JqZWN0IHRvIHRyYW5zZmVyIHRoZSBmbGFncyB0bzsgdXN1YWxseSBhIG5ldyBhc3NlcnRpb25cbiAqIEBwYXJhbSB7Qm9vbGVhbn0gaW5jbHVkZUFsbFxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgdHJhbnNmZXJGbGFnc1xuICogQGFwaSBwcml2YXRlXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0cmFuc2ZlckZsYWdzKGFzc2VydGlvbiwgb2JqZWN0LCBpbmNsdWRlQWxsKSB7XG4gIHZhciBmbGFncyA9IGFzc2VydGlvbi5fX2ZsYWdzIHx8IChhc3NlcnRpb24uX19mbGFncyA9IE9iamVjdC5jcmVhdGUobnVsbCkpO1xuXG4gIGlmICghb2JqZWN0Ll9fZmxhZ3MpIHtcbiAgICBvYmplY3QuX19mbGFncyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gIH1cblxuICBpbmNsdWRlQWxsID0gYXJndW1lbnRzLmxlbmd0aCA9PT0gMyA/IGluY2x1ZGVBbGwgOiB0cnVlO1xuXG4gIGZvciAodmFyIGZsYWcgaW4gZmxhZ3MpIHtcbiAgICBpZiAoaW5jbHVkZUFsbCB8fFxuICAgICAgICAoZmxhZyAhPT0gJ29iamVjdCcgJiYgZmxhZyAhPT0gJ3NzZmknICYmIGZsYWcgIT09ICdsb2NrU3NmaScgJiYgZmxhZyAhPSAnbWVzc2FnZScpKSB7XG4gICAgICBvYmplY3QuX19mbGFnc1tmbGFnXSA9IGZsYWdzW2ZsYWddO1xuICAgIH1cbiAgfVxufTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogIVxuICogQ2hhaSAtIGNoZWNrRXJyb3IgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTItMjAxNiBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIE1JVCBMaWNlbnNlZFxuICovXG5cbi8qKlxuICogIyMjIC5jaGVja0Vycm9yXG4gKlxuICogQ2hlY2tzIHRoYXQgYW4gZXJyb3IgY29uZm9ybXMgdG8gYSBnaXZlbiBzZXQgb2YgY3JpdGVyaWEgYW5kL29yIHJldHJpZXZlcyBpbmZvcm1hdGlvbiBhYm91dCBpdC5cbiAqXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbi8qKlxuICogIyMjIC5jb21wYXRpYmxlSW5zdGFuY2UodGhyb3duLCBlcnJvckxpa2UpXG4gKlxuICogQ2hlY2tzIGlmIHR3byBpbnN0YW5jZXMgYXJlIGNvbXBhdGlibGUgKHN0cmljdCBlcXVhbCkuXG4gKiBSZXR1cm5zIGZhbHNlIGlmIGVycm9yTGlrZSBpcyBub3QgYW4gaW5zdGFuY2Ugb2YgRXJyb3IsIGJlY2F1c2UgaW5zdGFuY2VzXG4gKiBjYW4gb25seSBiZSBjb21wYXRpYmxlIGlmIHRoZXkncmUgYm90aCBlcnJvciBpbnN0YW5jZXMuXG4gKlxuICogQG5hbWUgY29tcGF0aWJsZUluc3RhbmNlXG4gKiBAcGFyYW0ge0Vycm9yfSB0aHJvd24gZXJyb3JcbiAqIEBwYXJhbSB7RXJyb3J8RXJyb3JDb25zdHJ1Y3Rvcn0gZXJyb3JMaWtlIG9iamVjdCB0byBjb21wYXJlIGFnYWluc3RcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBhcGkgcHVibGljXG4gKi9cblxuZnVuY3Rpb24gY29tcGF0aWJsZUluc3RhbmNlKHRocm93biwgZXJyb3JMaWtlKSB7XG4gIHJldHVybiBlcnJvckxpa2UgaW5zdGFuY2VvZiBFcnJvciAmJiB0aHJvd24gPT09IGVycm9yTGlrZTtcbn1cblxuLyoqXG4gKiAjIyMgLmNvbXBhdGlibGVDb25zdHJ1Y3Rvcih0aHJvd24sIGVycm9yTGlrZSlcbiAqXG4gKiBDaGVja3MgaWYgdHdvIGNvbnN0cnVjdG9ycyBhcmUgY29tcGF0aWJsZS5cbiAqIFRoaXMgZnVuY3Rpb24gY2FuIHJlY2VpdmUgZWl0aGVyIGFuIGVycm9yIGNvbnN0cnVjdG9yIG9yXG4gKiBhbiBlcnJvciBpbnN0YW5jZSBhcyB0aGUgYGVycm9yTGlrZWAgYXJndW1lbnQuXG4gKiBDb25zdHJ1Y3RvcnMgYXJlIGNvbXBhdGlibGUgaWYgdGhleSdyZSB0aGUgc2FtZSBvciBpZiBvbmUgaXNcbiAqIGFuIGluc3RhbmNlIG9mIGFub3RoZXIuXG4gKlxuICogQG5hbWUgY29tcGF0aWJsZUNvbnN0cnVjdG9yXG4gKiBAcGFyYW0ge0Vycm9yfSB0aHJvd24gZXJyb3JcbiAqIEBwYXJhbSB7RXJyb3J8RXJyb3JDb25zdHJ1Y3Rvcn0gZXJyb3JMaWtlIG9iamVjdCB0byBjb21wYXJlIGFnYWluc3RcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBhcGkgcHVibGljXG4gKi9cblxuZnVuY3Rpb24gY29tcGF0aWJsZUNvbnN0cnVjdG9yKHRocm93biwgZXJyb3JMaWtlKSB7XG4gIGlmIChlcnJvckxpa2UgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgIC8vIElmIGBlcnJvckxpa2VgIGlzIGFuIGluc3RhbmNlIG9mIGFueSBlcnJvciB3ZSBjb21wYXJlIHRoZWlyIGNvbnN0cnVjdG9yc1xuICAgIHJldHVybiB0aHJvd24uY29uc3RydWN0b3IgPT09IGVycm9yTGlrZS5jb25zdHJ1Y3RvciB8fCB0aHJvd24gaW5zdGFuY2VvZiBlcnJvckxpa2UuY29uc3RydWN0b3I7XG4gIH0gZWxzZSBpZiAoZXJyb3JMaWtlLnByb3RvdHlwZSBpbnN0YW5jZW9mIEVycm9yIHx8IGVycm9yTGlrZSA9PT0gRXJyb3IpIHtcbiAgICAvLyBJZiBgZXJyb3JMaWtlYCBpcyBhIGNvbnN0cnVjdG9yIHRoYXQgaW5oZXJpdHMgZnJvbSBFcnJvciwgd2UgY29tcGFyZSBgdGhyb3duYCB0byBgZXJyb3JMaWtlYCBkaXJlY3RseVxuICAgIHJldHVybiB0aHJvd24uY29uc3RydWN0b3IgPT09IGVycm9yTGlrZSB8fCB0aHJvd24gaW5zdGFuY2VvZiBlcnJvckxpa2U7XG4gIH1cblxuICByZXR1cm4gZmFsc2U7XG59XG5cbi8qKlxuICogIyMjIC5jb21wYXRpYmxlTWVzc2FnZSh0aHJvd24sIGVyck1hdGNoZXIpXG4gKlxuICogQ2hlY2tzIGlmIGFuIGVycm9yJ3MgbWVzc2FnZSBpcyBjb21wYXRpYmxlIHdpdGggYSBtYXRjaGVyIChTdHJpbmcgb3IgUmVnRXhwKS5cbiAqIElmIHRoZSBtZXNzYWdlIGNvbnRhaW5zIHRoZSBTdHJpbmcgb3IgcGFzc2VzIHRoZSBSZWdFeHAgdGVzdCxcbiAqIGl0IGlzIGNvbnNpZGVyZWQgY29tcGF0aWJsZS5cbiAqXG4gKiBAbmFtZSBjb21wYXRpYmxlTWVzc2FnZVxuICogQHBhcmFtIHtFcnJvcn0gdGhyb3duIGVycm9yXG4gKiBAcGFyYW0ge1N0cmluZ3xSZWdFeHB9IGVyck1hdGNoZXIgdG8gbG9vayBmb3IgaW50byB0aGUgbWVzc2FnZVxuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQGFwaSBwdWJsaWNcbiAqL1xuXG5mdW5jdGlvbiBjb21wYXRpYmxlTWVzc2FnZSh0aHJvd24sIGVyck1hdGNoZXIpIHtcbiAgdmFyIGNvbXBhcmlzb25TdHJpbmcgPSB0eXBlb2YgdGhyb3duID09PSAnc3RyaW5nJyA/IHRocm93biA6IHRocm93bi5tZXNzYWdlO1xuICBpZiAoZXJyTWF0Y2hlciBpbnN0YW5jZW9mIFJlZ0V4cCkge1xuICAgIHJldHVybiBlcnJNYXRjaGVyLnRlc3QoY29tcGFyaXNvblN0cmluZyk7XG4gIH0gZWxzZSBpZiAodHlwZW9mIGVyck1hdGNoZXIgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIGNvbXBhcmlzb25TdHJpbmcuaW5kZXhPZihlcnJNYXRjaGVyKSAhPT0gLTE7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tbWFnaWMtbnVtYmVyc1xuICB9XG5cbiAgcmV0dXJuIGZhbHNlO1xufVxuXG4vKipcbiAqICMjIyAuZ2V0RnVuY3Rpb25OYW1lKGNvbnN0cnVjdG9yRm4pXG4gKlxuICogUmV0dXJucyB0aGUgbmFtZSBvZiBhIGZ1bmN0aW9uLlxuICogVGhpcyBhbHNvIGluY2x1ZGVzIGEgcG9seWZpbGwgZnVuY3Rpb24gaWYgYGNvbnN0cnVjdG9yRm4ubmFtZWAgaXMgbm90IGRlZmluZWQuXG4gKlxuICogQG5hbWUgZ2V0RnVuY3Rpb25OYW1lXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBjb25zdHJ1Y3RvckZuXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG52YXIgZnVuY3Rpb25OYW1lTWF0Y2ggPSAvXFxzKmZ1bmN0aW9uKD86XFxzfFxccypcXC9cXCpbXig/OipcXC8pXStcXCpcXC9cXHMqKSooW15cXChcXC9dKykvO1xuZnVuY3Rpb24gZ2V0RnVuY3Rpb25OYW1lKGNvbnN0cnVjdG9yRm4pIHtcbiAgdmFyIG5hbWUgPSAnJztcbiAgaWYgKHR5cGVvZiBjb25zdHJ1Y3RvckZuLm5hbWUgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgLy8gSGVyZSB3ZSBydW4gYSBwb2x5ZmlsbCBpZiBjb25zdHJ1Y3RvckZuLm5hbWUgaXMgbm90IGRlZmluZWRcbiAgICB2YXIgbWF0Y2ggPSBTdHJpbmcoY29uc3RydWN0b3JGbikubWF0Y2goZnVuY3Rpb25OYW1lTWF0Y2gpO1xuICAgIGlmIChtYXRjaCkge1xuICAgICAgbmFtZSA9IG1hdGNoWzFdO1xuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBuYW1lID0gY29uc3RydWN0b3JGbi5uYW1lO1xuICB9XG5cbiAgcmV0dXJuIG5hbWU7XG59XG5cbi8qKlxuICogIyMjIC5nZXRDb25zdHJ1Y3Rvck5hbWUoZXJyb3JMaWtlKVxuICpcbiAqIEdldHMgdGhlIGNvbnN0cnVjdG9yIG5hbWUgZm9yIGFuIEVycm9yIGluc3RhbmNlIG9yIGNvbnN0cnVjdG9yIGl0c2VsZi5cbiAqXG4gKiBAbmFtZSBnZXRDb25zdHJ1Y3Rvck5hbWVcbiAqIEBwYXJhbSB7RXJyb3J8RXJyb3JDb25zdHJ1Y3Rvcn0gZXJyb3JMaWtlXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbmZ1bmN0aW9uIGdldENvbnN0cnVjdG9yTmFtZShlcnJvckxpa2UpIHtcbiAgdmFyIGNvbnN0cnVjdG9yTmFtZSA9IGVycm9yTGlrZTtcbiAgaWYgKGVycm9yTGlrZSBpbnN0YW5jZW9mIEVycm9yKSB7XG4gICAgY29uc3RydWN0b3JOYW1lID0gZ2V0RnVuY3Rpb25OYW1lKGVycm9yTGlrZS5jb25zdHJ1Y3Rvcik7XG4gIH0gZWxzZSBpZiAodHlwZW9mIGVycm9yTGlrZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIC8vIElmIGBlcnJgIGlzIG5vdCBhbiBpbnN0YW5jZSBvZiBFcnJvciBpdCBpcyBhbiBlcnJvciBjb25zdHJ1Y3RvciBpdHNlbGYgb3IgYW5vdGhlciBmdW5jdGlvbi5cbiAgICAvLyBJZiB3ZSd2ZSBnb3QgYSBjb21tb24gZnVuY3Rpb24gd2UgZ2V0IGl0cyBuYW1lLCBvdGhlcndpc2Ugd2UgbWF5IG5lZWQgdG8gY3JlYXRlIGEgbmV3IGluc3RhbmNlXG4gICAgLy8gb2YgdGhlIGVycm9yIGp1c3QgaW4gY2FzZSBpdCdzIGEgcG9vcmx5LWNvbnN0cnVjdGVkIGVycm9yLiBQbGVhc2Ugc2VlIGNoYWlqcy9jaGFpL2lzc3Vlcy80NSB0byBrbm93IG1vcmUuXG4gICAgY29uc3RydWN0b3JOYW1lID0gZ2V0RnVuY3Rpb25OYW1lKGVycm9yTGlrZSkudHJpbSgpIHx8XG4gICAgICAgIGdldEZ1bmN0aW9uTmFtZShuZXcgZXJyb3JMaWtlKCkpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5ldy1jYXBcbiAgfVxuXG4gIHJldHVybiBjb25zdHJ1Y3Rvck5hbWU7XG59XG5cbi8qKlxuICogIyMjIC5nZXRNZXNzYWdlKGVycm9yTGlrZSlcbiAqXG4gKiBHZXRzIHRoZSBlcnJvciBtZXNzYWdlIGZyb20gYW4gZXJyb3IuXG4gKiBJZiBgZXJyYCBpcyBhIFN0cmluZyBpdHNlbGYsIHdlIHJldHVybiBpdC5cbiAqIElmIHRoZSBlcnJvciBoYXMgbm8gbWVzc2FnZSwgd2UgcmV0dXJuIGFuIGVtcHR5IHN0cmluZy5cbiAqXG4gKiBAbmFtZSBnZXRNZXNzYWdlXG4gKiBAcGFyYW0ge0Vycm9yfFN0cmluZ30gZXJyb3JMaWtlXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbmZ1bmN0aW9uIGdldE1lc3NhZ2UoZXJyb3JMaWtlKSB7XG4gIHZhciBtc2cgPSAnJztcbiAgaWYgKGVycm9yTGlrZSAmJiBlcnJvckxpa2UubWVzc2FnZSkge1xuICAgIG1zZyA9IGVycm9yTGlrZS5tZXNzYWdlO1xuICB9IGVsc2UgaWYgKHR5cGVvZiBlcnJvckxpa2UgPT09ICdzdHJpbmcnKSB7XG4gICAgbXNnID0gZXJyb3JMaWtlO1xuICB9XG5cbiAgcmV0dXJuIG1zZztcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIGNvbXBhdGlibGVJbnN0YW5jZTogY29tcGF0aWJsZUluc3RhbmNlLFxuICBjb21wYXRpYmxlQ29uc3RydWN0b3I6IGNvbXBhdGlibGVDb25zdHJ1Y3RvcixcbiAgY29tcGF0aWJsZU1lc3NhZ2U6IGNvbXBhdGlibGVNZXNzYWdlLFxuICBnZXRNZXNzYWdlOiBnZXRNZXNzYWdlLFxuICBnZXRDb25zdHJ1Y3Rvck5hbWU6IGdldENvbnN0cnVjdG9yTmFtZSxcbn07XG4iLCIndXNlIHN0cmljdCc7XG4vKiBnbG9iYWxzIFN5bWJvbDogZmFsc2UsIFVpbnQ4QXJyYXk6IGZhbHNlLCBXZWFrTWFwOiBmYWxzZSAqL1xuLyohXG4gKiBkZWVwLWVxbFxuICogQ29weXJpZ2h0KGMpIDIwMTMgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG52YXIgdHlwZSA9IHJlcXVpcmUoJ3R5cGUtZGV0ZWN0Jyk7XG5mdW5jdGlvbiBGYWtlTWFwKCkge1xuICB0aGlzLl9rZXkgPSAnY2hhaS9kZWVwLWVxbF9fJyArIE1hdGgucmFuZG9tKCkgKyBEYXRlLm5vdygpO1xufVxuXG5GYWtlTWFwLnByb3RvdHlwZSA9IHtcbiAgZ2V0OiBmdW5jdGlvbiBnZXRNYXAoa2V5KSB7XG4gICAgcmV0dXJuIGtleVt0aGlzLl9rZXldO1xuICB9LFxuICBzZXQ6IGZ1bmN0aW9uIHNldE1hcChrZXksIHZhbHVlKSB7XG4gICAgaWYgKE9iamVjdC5pc0V4dGVuc2libGUoa2V5KSkge1xuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGtleSwgdGhpcy5fa2V5LCB7XG4gICAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgfSk7XG4gICAgfVxuICB9LFxufTtcblxudmFyIE1lbW9pemVNYXAgPSB0eXBlb2YgV2Vha01hcCA9PT0gJ2Z1bmN0aW9uJyA/IFdlYWtNYXAgOiBGYWtlTWFwO1xuLyohXG4gKiBDaGVjayB0byBzZWUgaWYgdGhlIE1lbW9pemVNYXAgaGFzIHJlY29yZGVkIGEgcmVzdWx0IG9mIHRoZSB0d28gb3BlcmFuZHNcbiAqXG4gKiBAcGFyYW0ge01peGVkfSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7TWl4ZWR9IHJpZ2h0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7TWVtb2l6ZU1hcH0gbWVtb2l6ZU1hcFxuICogQHJldHVybnMge0Jvb2xlYW58bnVsbH0gcmVzdWx0XG4qL1xuZnVuY3Rpb24gbWVtb2l6ZUNvbXBhcmUobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBtZW1vaXplTWFwKSB7XG4gIC8vIFRlY2huaWNhbGx5LCBXZWFrTWFwIGtleXMgY2FuICpvbmx5KiBiZSBvYmplY3RzLCBub3QgcHJpbWl0aXZlcy5cbiAgaWYgKCFtZW1vaXplTWFwIHx8IGlzUHJpbWl0aXZlKGxlZnRIYW5kT3BlcmFuZCkgfHwgaXNQcmltaXRpdmUocmlnaHRIYW5kT3BlcmFuZCkpIHtcbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuICB2YXIgbGVmdEhhbmRNYXAgPSBtZW1vaXplTWFwLmdldChsZWZ0SGFuZE9wZXJhbmQpO1xuICBpZiAobGVmdEhhbmRNYXApIHtcbiAgICB2YXIgcmVzdWx0ID0gbGVmdEhhbmRNYXAuZ2V0KHJpZ2h0SGFuZE9wZXJhbmQpO1xuICAgIGlmICh0eXBlb2YgcmVzdWx0ID09PSAnYm9vbGVhbicpIHtcbiAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuICB9XG4gIHJldHVybiBudWxsO1xufVxuXG4vKiFcbiAqIFNldCB0aGUgcmVzdWx0IG9mIHRoZSBlcXVhbGl0eSBpbnRvIHRoZSBNZW1vaXplTWFwXG4gKlxuICogQHBhcmFtIHtNaXhlZH0gbGVmdEhhbmRPcGVyYW5kXG4gKiBAcGFyYW0ge01peGVkfSByaWdodEhhbmRPcGVyYW5kXG4gKiBAcGFyYW0ge01lbW9pemVNYXB9IG1lbW9pemVNYXBcbiAqIEBwYXJhbSB7Qm9vbGVhbn0gcmVzdWx0XG4qL1xuZnVuY3Rpb24gbWVtb2l6ZVNldChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQsIG1lbW9pemVNYXAsIHJlc3VsdCkge1xuICAvLyBUZWNobmljYWxseSwgV2Vha01hcCBrZXlzIGNhbiAqb25seSogYmUgb2JqZWN0cywgbm90IHByaW1pdGl2ZXMuXG4gIGlmICghbWVtb2l6ZU1hcCB8fCBpc1ByaW1pdGl2ZShsZWZ0SGFuZE9wZXJhbmQpIHx8IGlzUHJpbWl0aXZlKHJpZ2h0SGFuZE9wZXJhbmQpKSB7XG4gICAgcmV0dXJuO1xuICB9XG4gIHZhciBsZWZ0SGFuZE1hcCA9IG1lbW9pemVNYXAuZ2V0KGxlZnRIYW5kT3BlcmFuZCk7XG4gIGlmIChsZWZ0SGFuZE1hcCkge1xuICAgIGxlZnRIYW5kTWFwLnNldChyaWdodEhhbmRPcGVyYW5kLCByZXN1bHQpO1xuICB9IGVsc2Uge1xuICAgIGxlZnRIYW5kTWFwID0gbmV3IE1lbW9pemVNYXAoKTtcbiAgICBsZWZ0SGFuZE1hcC5zZXQocmlnaHRIYW5kT3BlcmFuZCwgcmVzdWx0KTtcbiAgICBtZW1vaXplTWFwLnNldChsZWZ0SGFuZE9wZXJhbmQsIGxlZnRIYW5kTWFwKTtcbiAgfVxufVxuXG4vKiFcbiAqIFByaW1hcnkgRXhwb3J0XG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBkZWVwRXF1YWw7XG5tb2R1bGUuZXhwb3J0cy5NZW1vaXplTWFwID0gTWVtb2l6ZU1hcDtcblxuLyoqXG4gKiBBc3NlcnQgZGVlcGx5IG5lc3RlZCBzYW1lVmFsdWUgZXF1YWxpdHkgYmV0d2VlbiB0d28gb2JqZWN0cyBvZiBhbnkgdHlwZS5cbiAqXG4gKiBAcGFyYW0ge01peGVkfSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7TWl4ZWR9IHJpZ2h0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc10gKG9wdGlvbmFsKSBBZGRpdGlvbmFsIG9wdGlvbnNcbiAqIEBwYXJhbSB7QXJyYXl9IFtvcHRpb25zLmNvbXBhcmF0b3JdIChvcHRpb25hbCkgT3ZlcnJpZGUgZGVmYXVsdCBhbGdvcml0aG0sIGRldGVybWluaW5nIGN1c3RvbSBlcXVhbGl0eS5cbiAqIEBwYXJhbSB7QXJyYXl9IFtvcHRpb25zLm1lbW9pemVdIChvcHRpb25hbCkgUHJvdmlkZSBhIGN1c3RvbSBtZW1vaXphdGlvbiBvYmplY3Qgd2hpY2ggd2lsbCBjYWNoZSB0aGUgcmVzdWx0cyBvZlxuICAgIGNvbXBsZXggb2JqZWN0cyBmb3IgYSBzcGVlZCBib29zdC4gQnkgcGFzc2luZyBgZmFsc2VgIHlvdSBjYW4gZGlzYWJsZSBtZW1vaXphdGlvbiwgYnV0IHRoaXMgd2lsbCBjYXVzZSBjaXJjdWxhclxuICAgIHJlZmVyZW5jZXMgdG8gYmxvdyB0aGUgc3RhY2suXG4gKiBAcmV0dXJuIHtCb29sZWFufSBlcXVhbCBtYXRjaFxuICovXG5mdW5jdGlvbiBkZWVwRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKSB7XG4gIC8vIElmIHdlIGhhdmUgYSBjb21wYXJhdG9yLCB3ZSBjYW4ndCBhc3N1bWUgYW55dGhpbmc7IHNvIGJhaWwgdG8gaXRzIGNoZWNrIGZpcnN0LlxuICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLmNvbXBhcmF0b3IpIHtcbiAgICByZXR1cm4gZXh0ZW5zaXZlRGVlcEVxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucyk7XG4gIH1cblxuICB2YXIgc2ltcGxlUmVzdWx0ID0gc2ltcGxlRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kKTtcbiAgaWYgKHNpbXBsZVJlc3VsdCAhPT0gbnVsbCkge1xuICAgIHJldHVybiBzaW1wbGVSZXN1bHQ7XG4gIH1cblxuICAvLyBEZWVwZXIgY29tcGFyaXNvbnMgYXJlIHB1c2hlZCB0aHJvdWdoIHRvIGEgbGFyZ2VyIGZ1bmN0aW9uXG4gIHJldHVybiBleHRlbnNpdmVEZWVwRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKTtcbn1cblxuLyoqXG4gKiBNYW55IGNvbXBhcmlzb25zIGNhbiBiZSBjYW5jZWxlZCBvdXQgZWFybHkgdmlhIHNpbXBsZSBlcXVhbGl0eSBvciBwcmltaXRpdmUgY2hlY2tzLlxuICogQHBhcmFtIHtNaXhlZH0gbGVmdEhhbmRPcGVyYW5kXG4gKiBAcGFyYW0ge01peGVkfSByaWdodEhhbmRPcGVyYW5kXG4gKiBAcmV0dXJuIHtCb29sZWFufG51bGx9IGVxdWFsIG1hdGNoXG4gKi9cbmZ1bmN0aW9uIHNpbXBsZUVxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCkge1xuICAvLyBFcXVhbCByZWZlcmVuY2VzIChleGNlcHQgZm9yIE51bWJlcnMpIGNhbiBiZSByZXR1cm5lZCBlYXJseVxuICBpZiAobGVmdEhhbmRPcGVyYW5kID09PSByaWdodEhhbmRPcGVyYW5kKSB7XG4gICAgLy8gSGFuZGxlICstMCBjYXNlc1xuICAgIHJldHVybiBsZWZ0SGFuZE9wZXJhbmQgIT09IDAgfHwgMSAvIGxlZnRIYW5kT3BlcmFuZCA9PT0gMSAvIHJpZ2h0SGFuZE9wZXJhbmQ7XG4gIH1cblxuICAvLyBoYW5kbGUgTmFOIGNhc2VzXG4gIGlmIChcbiAgICBsZWZ0SGFuZE9wZXJhbmQgIT09IGxlZnRIYW5kT3BlcmFuZCAmJiAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXNlbGYtY29tcGFyZVxuICAgIHJpZ2h0SGFuZE9wZXJhbmQgIT09IHJpZ2h0SGFuZE9wZXJhbmQgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1zZWxmLWNvbXBhcmVcbiAgKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICAvLyBBbnl0aGluZyB0aGF0IGlzIG5vdCBhbiAnb2JqZWN0JywgaS5lLiBzeW1ib2xzLCBmdW5jdGlvbnMsIGJvb2xlYW5zLCBudW1iZXJzLFxuICAvLyBzdHJpbmdzLCBhbmQgdW5kZWZpbmVkLCBjYW4gYmUgY29tcGFyZWQgYnkgcmVmZXJlbmNlLlxuICBpZiAoaXNQcmltaXRpdmUobGVmdEhhbmRPcGVyYW5kKSB8fCBpc1ByaW1pdGl2ZShyaWdodEhhbmRPcGVyYW5kKSkge1xuICAgIC8vIEVhc3kgb3V0IGIvYyBpdCB3b3VsZCBoYXZlIHBhc3NlZCB0aGUgZmlyc3QgZXF1YWxpdHkgY2hlY2tcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59XG5cbi8qIVxuICogVGhlIG1haW4gbG9naWMgb2YgdGhlIGBkZWVwRXF1YWxgIGZ1bmN0aW9uLlxuICpcbiAqIEBwYXJhbSB7TWl4ZWR9IGxlZnRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtNaXhlZH0gcmlnaHRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zXSAob3B0aW9uYWwpIEFkZGl0aW9uYWwgb3B0aW9uc1xuICogQHBhcmFtIHtBcnJheX0gW29wdGlvbnMuY29tcGFyYXRvcl0gKG9wdGlvbmFsKSBPdmVycmlkZSBkZWZhdWx0IGFsZ29yaXRobSwgZGV0ZXJtaW5pbmcgY3VzdG9tIGVxdWFsaXR5LlxuICogQHBhcmFtIHtBcnJheX0gW29wdGlvbnMubWVtb2l6ZV0gKG9wdGlvbmFsKSBQcm92aWRlIGEgY3VzdG9tIG1lbW9pemF0aW9uIG9iamVjdCB3aGljaCB3aWxsIGNhY2hlIHRoZSByZXN1bHRzIG9mXG4gICAgY29tcGxleCBvYmplY3RzIGZvciBhIHNwZWVkIGJvb3N0LiBCeSBwYXNzaW5nIGBmYWxzZWAgeW91IGNhbiBkaXNhYmxlIG1lbW9pemF0aW9uLCBidXQgdGhpcyB3aWxsIGNhdXNlIGNpcmN1bGFyXG4gICAgcmVmZXJlbmNlcyB0byBibG93IHRoZSBzdGFjay5cbiAqIEByZXR1cm4ge0Jvb2xlYW59IGVxdWFsIG1hdGNoXG4qL1xuZnVuY3Rpb24gZXh0ZW5zaXZlRGVlcEVxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucykge1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgb3B0aW9ucy5tZW1vaXplID0gb3B0aW9ucy5tZW1vaXplID09PSBmYWxzZSA/IGZhbHNlIDogb3B0aW9ucy5tZW1vaXplIHx8IG5ldyBNZW1vaXplTWFwKCk7XG4gIHZhciBjb21wYXJhdG9yID0gb3B0aW9ucyAmJiBvcHRpb25zLmNvbXBhcmF0b3I7XG5cbiAgLy8gQ2hlY2sgaWYgYSBtZW1vaXplZCByZXN1bHQgZXhpc3RzLlxuICB2YXIgbWVtb2l6ZVJlc3VsdExlZnQgPSBtZW1vaXplQ29tcGFyZShsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQsIG9wdGlvbnMubWVtb2l6ZSk7XG4gIGlmIChtZW1vaXplUmVzdWx0TGVmdCAhPT0gbnVsbCkge1xuICAgIHJldHVybiBtZW1vaXplUmVzdWx0TGVmdDtcbiAgfVxuICB2YXIgbWVtb2l6ZVJlc3VsdFJpZ2h0ID0gbWVtb2l6ZUNvbXBhcmUocmlnaHRIYW5kT3BlcmFuZCwgbGVmdEhhbmRPcGVyYW5kLCBvcHRpb25zLm1lbW9pemUpO1xuICBpZiAobWVtb2l6ZVJlc3VsdFJpZ2h0ICE9PSBudWxsKSB7XG4gICAgcmV0dXJuIG1lbW9pemVSZXN1bHRSaWdodDtcbiAgfVxuXG4gIC8vIElmIGEgY29tcGFyYXRvciBpcyBwcmVzZW50LCB1c2UgaXQuXG4gIGlmIChjb21wYXJhdG9yKSB7XG4gICAgdmFyIGNvbXBhcmF0b3JSZXN1bHQgPSBjb21wYXJhdG9yKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCk7XG4gICAgLy8gQ29tcGFyYXRvcnMgbWF5IHJldHVybiBudWxsLCBpbiB3aGljaCBjYXNlIHdlIHdhbnQgdG8gZ28gYmFjayB0byBkZWZhdWx0IGJlaGF2aW9yLlxuICAgIGlmIChjb21wYXJhdG9yUmVzdWx0ID09PSBmYWxzZSB8fCBjb21wYXJhdG9yUmVzdWx0ID09PSB0cnVlKSB7XG4gICAgICBtZW1vaXplU2V0KGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucy5tZW1vaXplLCBjb21wYXJhdG9yUmVzdWx0KTtcbiAgICAgIHJldHVybiBjb21wYXJhdG9yUmVzdWx0O1xuICAgIH1cbiAgICAvLyBUbyBhbGxvdyBjb21wYXJhdG9ycyB0byBvdmVycmlkZSAqYW55KiBiZWhhdmlvciwgd2UgcmFuIHRoZW0gZmlyc3QuIFNpbmNlIGl0IGRpZG4ndCBkZWNpZGVcbiAgICAvLyB3aGF0IHRvIGRvLCB3ZSBuZWVkIHRvIG1ha2Ugc3VyZSB0byByZXR1cm4gdGhlIGJhc2ljIHRlc3RzIGZpcnN0IGJlZm9yZSB3ZSBtb3ZlIG9uLlxuICAgIHZhciBzaW1wbGVSZXN1bHQgPSBzaW1wbGVFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQpO1xuICAgIGlmIChzaW1wbGVSZXN1bHQgIT09IG51bGwpIHtcbiAgICAgIC8vIERvbid0IG1lbW9pemUgdGhpcywgaXQgdGFrZXMgbG9uZ2VyIHRvIHNldC9yZXRyaWV2ZSB0aGFuIHRvIGp1c3QgY29tcGFyZS5cbiAgICAgIHJldHVybiBzaW1wbGVSZXN1bHQ7XG4gICAgfVxuICB9XG5cbiAgdmFyIGxlZnRIYW5kVHlwZSA9IHR5cGUobGVmdEhhbmRPcGVyYW5kKTtcbiAgaWYgKGxlZnRIYW5kVHlwZSAhPT0gdHlwZShyaWdodEhhbmRPcGVyYW5kKSkge1xuICAgIG1lbW9pemVTZXQobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zLm1lbW9pemUsIGZhbHNlKTtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICAvLyBUZW1wb3JhcmlseSBzZXQgdGhlIG9wZXJhbmRzIGluIHRoZSBtZW1vaXplIG9iamVjdCB0byBwcmV2ZW50IGJsb3dpbmcgdGhlIHN0YWNrXG4gIG1lbW9pemVTZXQobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zLm1lbW9pemUsIHRydWUpO1xuXG4gIHZhciByZXN1bHQgPSBleHRlbnNpdmVEZWVwRXF1YWxCeVR5cGUobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBsZWZ0SGFuZFR5cGUsIG9wdGlvbnMpO1xuICBtZW1vaXplU2V0KGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucy5tZW1vaXplLCByZXN1bHQpO1xuICByZXR1cm4gcmVzdWx0O1xufVxuXG5mdW5jdGlvbiBleHRlbnNpdmVEZWVwRXF1YWxCeVR5cGUobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBsZWZ0SGFuZFR5cGUsIG9wdGlvbnMpIHtcbiAgc3dpdGNoIChsZWZ0SGFuZFR5cGUpIHtcbiAgICBjYXNlICdTdHJpbmcnOlxuICAgIGNhc2UgJ051bWJlcic6XG4gICAgY2FzZSAnQm9vbGVhbic6XG4gICAgY2FzZSAnRGF0ZSc6XG4gICAgICAvLyBJZiB0aGVzZSB0eXBlcyBhcmUgdGhlaXIgaW5zdGFuY2UgdHlwZXMgKGUuZy4gYG5ldyBOdW1iZXJgKSB0aGVuIHJlLWRlZXBFcXVhbCBhZ2FpbnN0IHRoZWlyIHZhbHVlc1xuICAgICAgcmV0dXJuIGRlZXBFcXVhbChsZWZ0SGFuZE9wZXJhbmQudmFsdWVPZigpLCByaWdodEhhbmRPcGVyYW5kLnZhbHVlT2YoKSk7XG4gICAgY2FzZSAnUHJvbWlzZSc6XG4gICAgY2FzZSAnU3ltYm9sJzpcbiAgICBjYXNlICdmdW5jdGlvbic6XG4gICAgY2FzZSAnV2Vha01hcCc6XG4gICAgY2FzZSAnV2Vha1NldCc6XG4gICAgY2FzZSAnRXJyb3InOlxuICAgICAgcmV0dXJuIGxlZnRIYW5kT3BlcmFuZCA9PT0gcmlnaHRIYW5kT3BlcmFuZDtcbiAgICBjYXNlICdBcmd1bWVudHMnOlxuICAgIGNhc2UgJ0ludDhBcnJheSc6XG4gICAgY2FzZSAnVWludDhBcnJheSc6XG4gICAgY2FzZSAnVWludDhDbGFtcGVkQXJyYXknOlxuICAgIGNhc2UgJ0ludDE2QXJyYXknOlxuICAgIGNhc2UgJ1VpbnQxNkFycmF5JzpcbiAgICBjYXNlICdJbnQzMkFycmF5JzpcbiAgICBjYXNlICdVaW50MzJBcnJheSc6XG4gICAgY2FzZSAnRmxvYXQzMkFycmF5JzpcbiAgICBjYXNlICdGbG9hdDY0QXJyYXknOlxuICAgIGNhc2UgJ0FycmF5JzpcbiAgICAgIHJldHVybiBpdGVyYWJsZUVxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucyk7XG4gICAgY2FzZSAnUmVnRXhwJzpcbiAgICAgIHJldHVybiByZWdleHBFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQpO1xuICAgIGNhc2UgJ0dlbmVyYXRvcic6XG4gICAgICByZXR1cm4gZ2VuZXJhdG9yRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKTtcbiAgICBjYXNlICdEYXRhVmlldyc6XG4gICAgICByZXR1cm4gaXRlcmFibGVFcXVhbChuZXcgVWludDhBcnJheShsZWZ0SGFuZE9wZXJhbmQuYnVmZmVyKSwgbmV3IFVpbnQ4QXJyYXkocmlnaHRIYW5kT3BlcmFuZC5idWZmZXIpLCBvcHRpb25zKTtcbiAgICBjYXNlICdBcnJheUJ1ZmZlcic6XG4gICAgICByZXR1cm4gaXRlcmFibGVFcXVhbChuZXcgVWludDhBcnJheShsZWZ0SGFuZE9wZXJhbmQpLCBuZXcgVWludDhBcnJheShyaWdodEhhbmRPcGVyYW5kKSwgb3B0aW9ucyk7XG4gICAgY2FzZSAnU2V0JzpcbiAgICAgIHJldHVybiBlbnRyaWVzRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKTtcbiAgICBjYXNlICdNYXAnOlxuICAgICAgcmV0dXJuIGVudHJpZXNFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQsIG9wdGlvbnMpO1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gb2JqZWN0RXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKTtcbiAgfVxufVxuXG4vKiFcbiAqIENvbXBhcmUgdHdvIFJlZ3VsYXIgRXhwcmVzc2lvbnMgZm9yIGVxdWFsaXR5LlxuICpcbiAqIEBwYXJhbSB7UmVnRXhwfSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7UmVnRXhwfSByaWdodEhhbmRPcGVyYW5kXG4gKiBAcmV0dXJuIHtCb29sZWFufSByZXN1bHRcbiAqL1xuXG5mdW5jdGlvbiByZWdleHBFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQpIHtcbiAgcmV0dXJuIGxlZnRIYW5kT3BlcmFuZC50b1N0cmluZygpID09PSByaWdodEhhbmRPcGVyYW5kLnRvU3RyaW5nKCk7XG59XG5cbi8qIVxuICogQ29tcGFyZSB0d28gU2V0cy9NYXBzIGZvciBlcXVhbGl0eS4gRmFzdGVyIHRoYW4gb3RoZXIgZXF1YWxpdHkgZnVuY3Rpb25zLlxuICpcbiAqIEBwYXJhbSB7U2V0fSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7U2V0fSByaWdodEhhbmRPcGVyYW5kXG4gKiBAcGFyYW0ge09iamVjdH0gW29wdGlvbnNdIChPcHRpb25hbClcbiAqIEByZXR1cm4ge0Jvb2xlYW59IHJlc3VsdFxuICovXG5cbmZ1bmN0aW9uIGVudHJpZXNFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQsIG9wdGlvbnMpIHtcbiAgLy8gSUUxMSBkb2Vzbid0IHN1cHBvcnQgU2V0I2VudHJpZXMgb3IgU2V0I0BAaXRlcmF0b3IsIHNvIHdlIG5lZWQgbWFudWFsbHkgcG9wdWxhdGUgdXNpbmcgU2V0I2ZvckVhY2hcbiAgaWYgKGxlZnRIYW5kT3BlcmFuZC5zaXplICE9PSByaWdodEhhbmRPcGVyYW5kLnNpemUpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgaWYgKGxlZnRIYW5kT3BlcmFuZC5zaXplID09PSAwKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cbiAgdmFyIGxlZnRIYW5kSXRlbXMgPSBbXTtcbiAgdmFyIHJpZ2h0SGFuZEl0ZW1zID0gW107XG4gIGxlZnRIYW5kT3BlcmFuZC5mb3JFYWNoKGZ1bmN0aW9uIGdhdGhlckVudHJpZXMoa2V5LCB2YWx1ZSkge1xuICAgIGxlZnRIYW5kSXRlbXMucHVzaChbIGtleSwgdmFsdWUgXSk7XG4gIH0pO1xuICByaWdodEhhbmRPcGVyYW5kLmZvckVhY2goZnVuY3Rpb24gZ2F0aGVyRW50cmllcyhrZXksIHZhbHVlKSB7XG4gICAgcmlnaHRIYW5kSXRlbXMucHVzaChbIGtleSwgdmFsdWUgXSk7XG4gIH0pO1xuICByZXR1cm4gaXRlcmFibGVFcXVhbChsZWZ0SGFuZEl0ZW1zLnNvcnQoKSwgcmlnaHRIYW5kSXRlbXMuc29ydCgpLCBvcHRpb25zKTtcbn1cblxuLyohXG4gKiBTaW1wbGUgZXF1YWxpdHkgZm9yIGZsYXQgaXRlcmFibGUgb2JqZWN0cyBzdWNoIGFzIEFycmF5cywgVHlwZWRBcnJheXMgb3IgTm9kZS5qcyBidWZmZXJzLlxuICpcbiAqIEBwYXJhbSB7SXRlcmFibGV9IGxlZnRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtJdGVyYWJsZX0gcmlnaHRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zXSAoT3B0aW9uYWwpXG4gKiBAcmV0dXJuIHtCb29sZWFufSByZXN1bHRcbiAqL1xuXG5mdW5jdGlvbiBpdGVyYWJsZUVxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwgb3B0aW9ucykge1xuICB2YXIgbGVuZ3RoID0gbGVmdEhhbmRPcGVyYW5kLmxlbmd0aDtcbiAgaWYgKGxlbmd0aCAhPT0gcmlnaHRIYW5kT3BlcmFuZC5sZW5ndGgpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgaWYgKGxlbmd0aCA9PT0gMCkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG4gIHZhciBpbmRleCA9IC0xO1xuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCkge1xuICAgIGlmIChkZWVwRXF1YWwobGVmdEhhbmRPcGVyYW5kW2luZGV4XSwgcmlnaHRIYW5kT3BlcmFuZFtpbmRleF0sIG9wdGlvbnMpID09PSBmYWxzZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuICByZXR1cm4gdHJ1ZTtcbn1cblxuLyohXG4gKiBTaW1wbGUgZXF1YWxpdHkgZm9yIGdlbmVyYXRvciBvYmplY3RzIHN1Y2ggYXMgdGhvc2UgcmV0dXJuZWQgYnkgZ2VuZXJhdG9yIGZ1bmN0aW9ucy5cbiAqXG4gKiBAcGFyYW0ge0l0ZXJhYmxlfSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7SXRlcmFibGV9IHJpZ2h0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc10gKE9wdGlvbmFsKVxuICogQHJldHVybiB7Qm9vbGVhbn0gcmVzdWx0XG4gKi9cblxuZnVuY3Rpb24gZ2VuZXJhdG9yRXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKSB7XG4gIHJldHVybiBpdGVyYWJsZUVxdWFsKGdldEdlbmVyYXRvckVudHJpZXMobGVmdEhhbmRPcGVyYW5kKSwgZ2V0R2VuZXJhdG9yRW50cmllcyhyaWdodEhhbmRPcGVyYW5kKSwgb3B0aW9ucyk7XG59XG5cbi8qIVxuICogRGV0ZXJtaW5lIGlmIHRoZSBnaXZlbiBvYmplY3QgaGFzIGFuIEBAaXRlcmF0b3IgZnVuY3Rpb24uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHRhcmdldFxuICogQHJldHVybiB7Qm9vbGVhbn0gYHRydWVgIGlmIHRoZSBvYmplY3QgaGFzIGFuIEBAaXRlcmF0b3IgZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIGhhc0l0ZXJhdG9yRnVuY3Rpb24odGFyZ2V0KSB7XG4gIHJldHVybiB0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJlxuICAgIHR5cGVvZiB0YXJnZXQgPT09ICdvYmplY3QnICYmXG4gICAgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICB0eXBlb2YgdGFyZ2V0W1N5bWJvbC5pdGVyYXRvcl0gPT09ICdmdW5jdGlvbic7XG59XG5cbi8qIVxuICogR2V0cyBhbGwgaXRlcmF0b3IgZW50cmllcyBmcm9tIHRoZSBnaXZlbiBPYmplY3QuIElmIHRoZSBPYmplY3QgaGFzIG5vIEBAaXRlcmF0b3IgZnVuY3Rpb24sIHJldHVybnMgYW4gZW1wdHkgYXJyYXkuXG4gKiBUaGlzIHdpbGwgY29uc3VtZSB0aGUgaXRlcmF0b3IgLSB3aGljaCBjb3VsZCBoYXZlIHNpZGUgZWZmZWN0cyBkZXBlbmRpbmcgb24gdGhlIEBAaXRlcmF0b3IgaW1wbGVtZW50YXRpb24uXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHRhcmdldFxuICogQHJldHVybnMge0FycmF5fSBhbiBhcnJheSBvZiBlbnRyaWVzIGZyb20gdGhlIEBAaXRlcmF0b3IgZnVuY3Rpb25cbiAqL1xuZnVuY3Rpb24gZ2V0SXRlcmF0b3JFbnRyaWVzKHRhcmdldCkge1xuICBpZiAoaGFzSXRlcmF0b3JGdW5jdGlvbih0YXJnZXQpKSB7XG4gICAgdHJ5IHtcbiAgICAgIHJldHVybiBnZXRHZW5lcmF0b3JFbnRyaWVzKHRhcmdldFtTeW1ib2wuaXRlcmF0b3JdKCkpO1xuICAgIH0gY2F0Y2ggKGl0ZXJhdG9yRXJyb3IpIHtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIFtdO1xufVxuXG4vKiFcbiAqIEdldHMgYWxsIGVudHJpZXMgZnJvbSBhIEdlbmVyYXRvci4gVGhpcyB3aWxsIGNvbnN1bWUgdGhlIGdlbmVyYXRvciAtIHdoaWNoIGNvdWxkIGhhdmUgc2lkZSBlZmZlY3RzLlxuICpcbiAqIEBwYXJhbSB7R2VuZXJhdG9yfSB0YXJnZXRcbiAqIEByZXR1cm5zIHtBcnJheX0gYW4gYXJyYXkgb2YgZW50cmllcyBmcm9tIHRoZSBHZW5lcmF0b3IuXG4gKi9cbmZ1bmN0aW9uIGdldEdlbmVyYXRvckVudHJpZXMoZ2VuZXJhdG9yKSB7XG4gIHZhciBnZW5lcmF0b3JSZXN1bHQgPSBnZW5lcmF0b3IubmV4dCgpO1xuICB2YXIgYWNjdW11bGF0b3IgPSBbIGdlbmVyYXRvclJlc3VsdC52YWx1ZSBdO1xuICB3aGlsZSAoZ2VuZXJhdG9yUmVzdWx0LmRvbmUgPT09IGZhbHNlKSB7XG4gICAgZ2VuZXJhdG9yUmVzdWx0ID0gZ2VuZXJhdG9yLm5leHQoKTtcbiAgICBhY2N1bXVsYXRvci5wdXNoKGdlbmVyYXRvclJlc3VsdC52YWx1ZSk7XG4gIH1cbiAgcmV0dXJuIGFjY3VtdWxhdG9yO1xufVxuXG4vKiFcbiAqIEdldHMgYWxsIG93biBhbmQgaW5oZXJpdGVkIGVudW1lcmFibGUga2V5cyBmcm9tIGEgdGFyZ2V0LlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSB0YXJnZXRcbiAqIEByZXR1cm5zIHtBcnJheX0gYW4gYXJyYXkgb2Ygb3duIGFuZCBpbmhlcml0ZWQgZW51bWVyYWJsZSBrZXlzIGZyb20gdGhlIHRhcmdldC5cbiAqL1xuZnVuY3Rpb24gZ2V0RW51bWVyYWJsZUtleXModGFyZ2V0KSB7XG4gIHZhciBrZXlzID0gW107XG4gIGZvciAodmFyIGtleSBpbiB0YXJnZXQpIHtcbiAgICBrZXlzLnB1c2goa2V5KTtcbiAgfVxuICByZXR1cm4ga2V5cztcbn1cblxuLyohXG4gKiBEZXRlcm1pbmVzIGlmIHR3byBvYmplY3RzIGhhdmUgbWF0Y2hpbmcgdmFsdWVzLCBnaXZlbiBhIHNldCBvZiBrZXlzLiBEZWZlcnMgdG8gZGVlcEVxdWFsIGZvciB0aGUgZXF1YWxpdHkgY2hlY2sgb2ZcbiAqIGVhY2gga2V5LiBJZiBhbnkgdmFsdWUgb2YgdGhlIGdpdmVuIGtleSBpcyBub3QgZXF1YWwsIHRoZSBmdW5jdGlvbiB3aWxsIHJldHVybiBmYWxzZSAoZWFybHkpLlxuICpcbiAqIEBwYXJhbSB7TWl4ZWR9IGxlZnRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtNaXhlZH0gcmlnaHRIYW5kT3BlcmFuZFxuICogQHBhcmFtIHtBcnJheX0ga2V5cyBBbiBhcnJheSBvZiBrZXlzIHRvIGNvbXBhcmUgdGhlIHZhbHVlcyBvZiBsZWZ0SGFuZE9wZXJhbmQgYW5kIHJpZ2h0SGFuZE9wZXJhbmQgYWdhaW5zdFxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zXSAoT3B0aW9uYWwpXG4gKiBAcmV0dXJuIHtCb29sZWFufSByZXN1bHRcbiAqL1xuZnVuY3Rpb24ga2V5c0VxdWFsKGxlZnRIYW5kT3BlcmFuZCwgcmlnaHRIYW5kT3BlcmFuZCwga2V5cywgb3B0aW9ucykge1xuICB2YXIgbGVuZ3RoID0ga2V5cy5sZW5ndGg7XG4gIGlmIChsZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgaSArPSAxKSB7XG4gICAgaWYgKGRlZXBFcXVhbChsZWZ0SGFuZE9wZXJhbmRba2V5c1tpXV0sIHJpZ2h0SGFuZE9wZXJhbmRba2V5c1tpXV0sIG9wdGlvbnMpID09PSBmYWxzZSkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgfVxuICByZXR1cm4gdHJ1ZTtcbn1cblxuLyohXG4gKiBSZWN1cnNpdmVseSBjaGVjayB0aGUgZXF1YWxpdHkgb2YgdHdvIE9iamVjdHMuIE9uY2UgYmFzaWMgc2FtZW5lc3MgaGFzIGJlZW4gZXN0YWJsaXNoZWQgaXQgd2lsbCBkZWZlciB0byBgZGVlcEVxdWFsYFxuICogZm9yIGVhY2ggZW51bWVyYWJsZSBrZXkgaW4gdGhlIG9iamVjdC5cbiAqXG4gKiBAcGFyYW0ge01peGVkfSBsZWZ0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7TWl4ZWR9IHJpZ2h0SGFuZE9wZXJhbmRcbiAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc10gKE9wdGlvbmFsKVxuICogQHJldHVybiB7Qm9vbGVhbn0gcmVzdWx0XG4gKi9cblxuZnVuY3Rpb24gb2JqZWN0RXF1YWwobGVmdEhhbmRPcGVyYW5kLCByaWdodEhhbmRPcGVyYW5kLCBvcHRpb25zKSB7XG4gIHZhciBsZWZ0SGFuZEtleXMgPSBnZXRFbnVtZXJhYmxlS2V5cyhsZWZ0SGFuZE9wZXJhbmQpO1xuICB2YXIgcmlnaHRIYW5kS2V5cyA9IGdldEVudW1lcmFibGVLZXlzKHJpZ2h0SGFuZE9wZXJhbmQpO1xuICBpZiAobGVmdEhhbmRLZXlzLmxlbmd0aCAmJiBsZWZ0SGFuZEtleXMubGVuZ3RoID09PSByaWdodEhhbmRLZXlzLmxlbmd0aCkge1xuICAgIGxlZnRIYW5kS2V5cy5zb3J0KCk7XG4gICAgcmlnaHRIYW5kS2V5cy5zb3J0KCk7XG4gICAgaWYgKGl0ZXJhYmxlRXF1YWwobGVmdEhhbmRLZXlzLCByaWdodEhhbmRLZXlzKSA9PT0gZmFsc2UpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcmV0dXJuIGtleXNFcXVhbChsZWZ0SGFuZE9wZXJhbmQsIHJpZ2h0SGFuZE9wZXJhbmQsIGxlZnRIYW5kS2V5cywgb3B0aW9ucyk7XG4gIH1cblxuICB2YXIgbGVmdEhhbmRFbnRyaWVzID0gZ2V0SXRlcmF0b3JFbnRyaWVzKGxlZnRIYW5kT3BlcmFuZCk7XG4gIHZhciByaWdodEhhbmRFbnRyaWVzID0gZ2V0SXRlcmF0b3JFbnRyaWVzKHJpZ2h0SGFuZE9wZXJhbmQpO1xuICBpZiAobGVmdEhhbmRFbnRyaWVzLmxlbmd0aCAmJiBsZWZ0SGFuZEVudHJpZXMubGVuZ3RoID09PSByaWdodEhhbmRFbnRyaWVzLmxlbmd0aCkge1xuICAgIGxlZnRIYW5kRW50cmllcy5zb3J0KCk7XG4gICAgcmlnaHRIYW5kRW50cmllcy5zb3J0KCk7XG4gICAgcmV0dXJuIGl0ZXJhYmxlRXF1YWwobGVmdEhhbmRFbnRyaWVzLCByaWdodEhhbmRFbnRyaWVzLCBvcHRpb25zKTtcbiAgfVxuXG4gIGlmIChsZWZ0SGFuZEtleXMubGVuZ3RoID09PSAwICYmXG4gICAgICBsZWZ0SGFuZEVudHJpZXMubGVuZ3RoID09PSAwICYmXG4gICAgICByaWdodEhhbmRLZXlzLmxlbmd0aCA9PT0gMCAmJlxuICAgICAgcmlnaHRIYW5kRW50cmllcy5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxuLyohXG4gKiBSZXR1cm5zIHRydWUgaWYgdGhlIGFyZ3VtZW50IGlzIGEgcHJpbWl0aXZlLlxuICpcbiAqIFRoaXMgaW50ZW50aW9uYWxseSByZXR1cm5zIHRydWUgZm9yIGFsbCBvYmplY3RzIHRoYXQgY2FuIGJlIGNvbXBhcmVkIGJ5IHJlZmVyZW5jZSxcbiAqIGluY2x1ZGluZyBmdW5jdGlvbnMgYW5kIHN5bWJvbHMuXG4gKlxuICogQHBhcmFtIHtNaXhlZH0gdmFsdWVcbiAqIEByZXR1cm4ge0Jvb2xlYW59IHJlc3VsdFxuICovXG5mdW5jdGlvbiBpc1ByaW1pdGl2ZSh2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUgPT09IG51bGwgfHwgdHlwZW9mIHZhbHVlICE9PSAnb2JqZWN0Jztcbn1cbiIsIid1c2Ugc3RyaWN0JztcblxuLyogIVxuICogQ2hhaSAtIGdldEZ1bmNOYW1lIHV0aWxpdHlcbiAqIENvcHlyaWdodChjKSAyMDEyLTIwMTYgSmFrZSBMdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuZ2V0RnVuY05hbWUoY29uc3RydWN0b3JGbilcbiAqXG4gKiBSZXR1cm5zIHRoZSBuYW1lIG9mIGEgZnVuY3Rpb24uXG4gKiBXaGVuIGEgbm9uLWZ1bmN0aW9uIGluc3RhbmNlIGlzIHBhc3NlZCwgcmV0dXJucyBgbnVsbGAuXG4gKiBUaGlzIGFsc28gaW5jbHVkZXMgYSBwb2x5ZmlsbCBmdW5jdGlvbiBpZiBgYUZ1bmMubmFtZWAgaXMgbm90IGRlZmluZWQuXG4gKlxuICogQG5hbWUgZ2V0RnVuY05hbWVcbiAqIEBwYXJhbSB7RnVuY3Rpb259IGZ1bmN0XG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbnZhciB0b1N0cmluZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS50b1N0cmluZztcbnZhciBmdW5jdGlvbk5hbWVNYXRjaCA9IC9cXHMqZnVuY3Rpb24oPzpcXHN8XFxzKlxcL1xcKlteKD86KlxcLyldK1xcKlxcL1xccyopKihbXlxcc1xcKFxcL10rKS87XG5mdW5jdGlvbiBnZXRGdW5jTmFtZShhRnVuYykge1xuICBpZiAodHlwZW9mIGFGdW5jICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB2YXIgbmFtZSA9ICcnO1xuICBpZiAodHlwZW9mIEZ1bmN0aW9uLnByb3RvdHlwZS5uYW1lID09PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgYUZ1bmMubmFtZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAvLyBIZXJlIHdlIHJ1biBhIHBvbHlmaWxsIGlmIEZ1bmN0aW9uIGRvZXMgbm90IHN1cHBvcnQgdGhlIGBuYW1lYCBwcm9wZXJ0eSBhbmQgaWYgYUZ1bmMubmFtZSBpcyBub3QgZGVmaW5lZFxuICAgIHZhciBtYXRjaCA9IHRvU3RyaW5nLmNhbGwoYUZ1bmMpLm1hdGNoKGZ1bmN0aW9uTmFtZU1hdGNoKTtcbiAgICBpZiAobWF0Y2gpIHtcbiAgICAgIG5hbWUgPSBtYXRjaFsxXTtcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgLy8gSWYgd2UndmUgZ290IGEgYG5hbWVgIHByb3BlcnR5IHdlIGp1c3QgdXNlIGl0XG4gICAgbmFtZSA9IGFGdW5jLm5hbWU7XG4gIH1cblxuICByZXR1cm4gbmFtZTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBnZXRGdW5jTmFtZTtcbiIsIid1c2Ugc3RyaWN0JztcblxuLyogIVxuICogQ2hhaSAtIHBhdGh2YWwgdXRpbGl0eVxuICogQ29weXJpZ2h0KGMpIDIwMTItMjAxNCBKYWtlIEx1ZXIgPGpha2VAYWxvZ2ljYWxwYXJhZG94LmNvbT5cbiAqIEBzZWUgaHR0cHM6Ly9naXRodWIuY29tL2xvZ2ljYWxwYXJhZG94L2ZpbHRyXG4gKiBNSVQgTGljZW5zZWRcbiAqL1xuXG4vKipcbiAqICMjIyAuaGFzUHJvcGVydHkob2JqZWN0LCBuYW1lKVxuICpcbiAqIFRoaXMgYWxsb3dzIGNoZWNraW5nIHdoZXRoZXIgYW4gb2JqZWN0IGhhcyBvd25cbiAqIG9yIGluaGVyaXRlZCBmcm9tIHByb3RvdHlwZSBjaGFpbiBuYW1lZCBwcm9wZXJ0eS5cbiAqXG4gKiBCYXNpY2FsbHkgZG9lcyB0aGUgc2FtZSB0aGluZyBhcyB0aGUgYGluYFxuICogb3BlcmF0b3IgYnV0IHdvcmtzIHByb3Blcmx5IHdpdGggbnVsbC91bmRlZmluZWQgdmFsdWVzXG4gKiBhbmQgb3RoZXIgcHJpbWl0aXZlcy5cbiAqXG4gKiAgICAgdmFyIG9iaiA9IHtcbiAqICAgICAgICAgYXJyOiBbJ2EnLCAnYicsICdjJ11cbiAqICAgICAgICwgc3RyOiAnSGVsbG8nXG4gKiAgICAgfVxuICpcbiAqIFRoZSBmb2xsb3dpbmcgd291bGQgYmUgdGhlIHJlc3VsdHMuXG4gKlxuICogICAgIGhhc1Byb3BlcnR5KG9iaiwgJ3N0cicpOyAgLy8gdHJ1ZVxuICogICAgIGhhc1Byb3BlcnR5KG9iaiwgJ2NvbnN0cnVjdG9yJyk7ICAvLyB0cnVlXG4gKiAgICAgaGFzUHJvcGVydHkob2JqLCAnYmFyJyk7ICAvLyBmYWxzZVxuICpcbiAqICAgICBoYXNQcm9wZXJ0eShvYmouc3RyLCAnbGVuZ3RoJyk7IC8vIHRydWVcbiAqICAgICBoYXNQcm9wZXJ0eShvYmouc3RyLCAxKTsgIC8vIHRydWVcbiAqICAgICBoYXNQcm9wZXJ0eShvYmouc3RyLCA1KTsgIC8vIGZhbHNlXG4gKlxuICogICAgIGhhc1Byb3BlcnR5KG9iai5hcnIsICdsZW5ndGgnKTsgIC8vIHRydWVcbiAqICAgICBoYXNQcm9wZXJ0eShvYmouYXJyLCAyKTsgIC8vIHRydWVcbiAqICAgICBoYXNQcm9wZXJ0eShvYmouYXJyLCAzKTsgIC8vIGZhbHNlXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHBhcmFtIHtTdHJpbmd8U3ltYm9sfSBuYW1lXG4gKiBAcmV0dXJucyB7Qm9vbGVhbn0gd2hldGhlciBpdCBleGlzdHNcbiAqIEBuYW1lc3BhY2UgVXRpbHNcbiAqIEBuYW1lIGhhc1Byb3BlcnR5XG4gKiBAYXBpIHB1YmxpY1xuICovXG5cbmZ1bmN0aW9uIGhhc1Byb3BlcnR5KG9iaiwgbmFtZSkge1xuICBpZiAodHlwZW9mIG9iaiA9PT0gJ3VuZGVmaW5lZCcgfHwgb2JqID09PSBudWxsKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLy8gVGhlIGBpbmAgb3BlcmF0b3IgZG9lcyBub3Qgd29yayB3aXRoIHByaW1pdGl2ZXMuXG4gIHJldHVybiBuYW1lIGluIE9iamVjdChvYmopO1xufVxuXG4vKiAhXG4gKiAjIyBwYXJzZVBhdGgocGF0aClcbiAqXG4gKiBIZWxwZXIgZnVuY3Rpb24gdXNlZCB0byBwYXJzZSBzdHJpbmcgb2JqZWN0XG4gKiBwYXRocy4gVXNlIGluIGNvbmp1bmN0aW9uIHdpdGggYGludGVybmFsR2V0UGF0aFZhbHVlYC5cbiAqXG4gKiAgICAgIHZhciBwYXJzZWQgPSBwYXJzZVBhdGgoJ215b2JqZWN0LnByb3BlcnR5LnN1YnByb3AnKTtcbiAqXG4gKiAjIyMgUGF0aHM6XG4gKlxuICogKiBDYW4gYmUgaW5maW5pdGVseSBkZWVwIGFuZCBuZXN0ZWQuXG4gKiAqIEFycmF5cyBhcmUgYWxzbyB2YWxpZCB1c2luZyB0aGUgZm9ybWFsIGBteW9iamVjdC5kb2N1bWVudFszXS5wcm9wZXJ0eWAuXG4gKiAqIExpdGVyYWwgZG90cyBhbmQgYnJhY2tldHMgKG5vdCBkZWxpbWl0ZXIpIG11c3QgYmUgYmFja3NsYXNoLWVzY2FwZWQuXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IHBhdGhcbiAqIEByZXR1cm5zIHtPYmplY3R9IHBhcnNlZFxuICogQGFwaSBwcml2YXRlXG4gKi9cblxuZnVuY3Rpb24gcGFyc2VQYXRoKHBhdGgpIHtcbiAgdmFyIHN0ciA9IHBhdGgucmVwbGFjZSgvKFteXFxcXF0pXFxbL2csICckMS5bJyk7XG4gIHZhciBwYXJ0cyA9IHN0ci5tYXRjaCgvKFxcXFxcXC58W14uXSs/KSsvZyk7XG4gIHJldHVybiBwYXJ0cy5tYXAoZnVuY3Rpb24gbWFwTWF0Y2hlcyh2YWx1ZSkge1xuICAgIGlmIChcbiAgICAgIHZhbHVlID09PSAnY29uc3RydWN0b3InIHx8XG4gICAgICB2YWx1ZSA9PT0gJ19fcHJvdG9fXycgfHxcbiAgICAgIHZhbHVlID09PSAncHJvdG90eXBlJ1xuICAgICkge1xuICAgICAgcmV0dXJuIHt9O1xuICAgIH1cbiAgICB2YXIgcmVnZXhwID0gL15cXFsoXFxkKylcXF0kLztcbiAgICB2YXIgbUFyciA9IHJlZ2V4cC5leGVjKHZhbHVlKTtcbiAgICB2YXIgcGFyc2VkID0gbnVsbDtcbiAgICBpZiAobUFycikge1xuICAgICAgcGFyc2VkID0geyBpOiBwYXJzZUZsb2F0KG1BcnJbMV0pIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIHBhcnNlZCA9IHsgcDogdmFsdWUucmVwbGFjZSgvXFxcXChbLltcXF1dKS9nLCAnJDEnKSB9O1xuICAgIH1cblxuICAgIHJldHVybiBwYXJzZWQ7XG4gIH0pO1xufVxuXG4vKiAhXG4gKiAjIyBpbnRlcm5hbEdldFBhdGhWYWx1ZShvYmosIHBhcnNlZFssIHBhdGhEZXB0aF0pXG4gKlxuICogSGVscGVyIGNvbXBhbmlvbiBmdW5jdGlvbiBmb3IgYC5wYXJzZVBhdGhgIHRoYXQgcmV0dXJuc1xuICogdGhlIHZhbHVlIGxvY2F0ZWQgYXQgdGhlIHBhcnNlZCBhZGRyZXNzLlxuICpcbiAqICAgICAgdmFyIHZhbHVlID0gZ2V0UGF0aFZhbHVlKG9iaiwgcGFyc2VkKTtcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0IHRvIHNlYXJjaCBhZ2FpbnN0XG4gKiBAcGFyYW0ge09iamVjdH0gcGFyc2VkIGRlZmluaXRpb24gZnJvbSBgcGFyc2VQYXRoYC5cbiAqIEBwYXJhbSB7TnVtYmVyfSBkZXB0aCAobmVzdGluZyBsZXZlbCkgb2YgdGhlIHByb3BlcnR5IHdlIHdhbnQgdG8gcmV0cmlldmVcbiAqIEByZXR1cm5zIHtPYmplY3R8VW5kZWZpbmVkfSB2YWx1ZVxuICogQGFwaSBwcml2YXRlXG4gKi9cblxuZnVuY3Rpb24gaW50ZXJuYWxHZXRQYXRoVmFsdWUob2JqLCBwYXJzZWQsIHBhdGhEZXB0aCkge1xuICB2YXIgdGVtcG9yYXJ5VmFsdWUgPSBvYmo7XG4gIHZhciByZXMgPSBudWxsO1xuICBwYXRoRGVwdGggPSB0eXBlb2YgcGF0aERlcHRoID09PSAndW5kZWZpbmVkJyA/IHBhcnNlZC5sZW5ndGggOiBwYXRoRGVwdGg7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBwYXRoRGVwdGg7IGkrKykge1xuICAgIHZhciBwYXJ0ID0gcGFyc2VkW2ldO1xuICAgIGlmICh0ZW1wb3JhcnlWYWx1ZSkge1xuICAgICAgaWYgKHR5cGVvZiBwYXJ0LnAgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRlbXBvcmFyeVZhbHVlID0gdGVtcG9yYXJ5VmFsdWVbcGFydC5pXTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRlbXBvcmFyeVZhbHVlID0gdGVtcG9yYXJ5VmFsdWVbcGFydC5wXTtcbiAgICAgIH1cblxuICAgICAgaWYgKGkgPT09IHBhdGhEZXB0aCAtIDEpIHtcbiAgICAgICAgcmVzID0gdGVtcG9yYXJ5VmFsdWU7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlcztcbn1cblxuLyogIVxuICogIyMgaW50ZXJuYWxTZXRQYXRoVmFsdWUob2JqLCB2YWx1ZSwgcGFyc2VkKVxuICpcbiAqIENvbXBhbmlvbiBmdW5jdGlvbiBmb3IgYHBhcnNlUGF0aGAgdGhhdCBzZXRzXG4gKiB0aGUgdmFsdWUgbG9jYXRlZCBhdCBhIHBhcnNlZCBhZGRyZXNzLlxuICpcbiAqICBpbnRlcm5hbFNldFBhdGhWYWx1ZShvYmosICd2YWx1ZScsIHBhcnNlZCk7XG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdCB0byBzZWFyY2ggYW5kIGRlZmluZSBvblxuICogQHBhcmFtIHsqfSB2YWx1ZSB0byB1c2UgdXBvbiBzZXRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXJzZWQgZGVmaW5pdGlvbiBmcm9tIGBwYXJzZVBhdGhgXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG5mdW5jdGlvbiBpbnRlcm5hbFNldFBhdGhWYWx1ZShvYmosIHZhbCwgcGFyc2VkKSB7XG4gIHZhciB0ZW1wT2JqID0gb2JqO1xuICB2YXIgcGF0aERlcHRoID0gcGFyc2VkLmxlbmd0aDtcbiAgdmFyIHBhcnQgPSBudWxsO1xuICAvLyBIZXJlIHdlIGl0ZXJhdGUgdGhyb3VnaCBldmVyeSBwYXJ0IG9mIHRoZSBwYXRoXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgcGF0aERlcHRoOyBpKyspIHtcbiAgICB2YXIgcHJvcE5hbWUgPSBudWxsO1xuICAgIHZhciBwcm9wVmFsID0gbnVsbDtcbiAgICBwYXJ0ID0gcGFyc2VkW2ldO1xuXG4gICAgLy8gSWYgaXQncyB0aGUgbGFzdCBwYXJ0IG9mIHRoZSBwYXRoLCB3ZSBzZXQgdGhlICdwcm9wTmFtZScgdmFsdWUgd2l0aCB0aGUgcHJvcGVydHkgbmFtZVxuICAgIGlmIChpID09PSBwYXRoRGVwdGggLSAxKSB7XG4gICAgICBwcm9wTmFtZSA9IHR5cGVvZiBwYXJ0LnAgPT09ICd1bmRlZmluZWQnID8gcGFydC5pIDogcGFydC5wO1xuICAgICAgLy8gTm93IHdlIHNldCB0aGUgcHJvcGVydHkgd2l0aCB0aGUgbmFtZSBoZWxkIGJ5ICdwcm9wTmFtZScgb24gb2JqZWN0IHdpdGggdGhlIGRlc2lyZWQgdmFsXG4gICAgICB0ZW1wT2JqW3Byb3BOYW1lXSA9IHZhbDtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBwYXJ0LnAgIT09ICd1bmRlZmluZWQnICYmIHRlbXBPYmpbcGFydC5wXSkge1xuICAgICAgdGVtcE9iaiA9IHRlbXBPYmpbcGFydC5wXTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBwYXJ0LmkgIT09ICd1bmRlZmluZWQnICYmIHRlbXBPYmpbcGFydC5pXSkge1xuICAgICAgdGVtcE9iaiA9IHRlbXBPYmpbcGFydC5pXTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gSWYgdGhlIG9iaiBkb2Vzbid0IGhhdmUgdGhlIHByb3BlcnR5IHdlIGNyZWF0ZSBvbmUgd2l0aCB0aGF0IG5hbWUgdG8gZGVmaW5lIGl0XG4gICAgICB2YXIgbmV4dCA9IHBhcnNlZFtpICsgMV07XG4gICAgICAvLyBIZXJlIHdlIHNldCB0aGUgbmFtZSBvZiB0aGUgcHJvcGVydHkgd2hpY2ggd2lsbCBiZSBkZWZpbmVkXG4gICAgICBwcm9wTmFtZSA9IHR5cGVvZiBwYXJ0LnAgPT09ICd1bmRlZmluZWQnID8gcGFydC5pIDogcGFydC5wO1xuICAgICAgLy8gSGVyZSB3ZSBkZWNpZGUgaWYgdGhpcyBwcm9wZXJ0eSB3aWxsIGJlIGFuIGFycmF5IG9yIGEgbmV3IG9iamVjdFxuICAgICAgcHJvcFZhbCA9IHR5cGVvZiBuZXh0LnAgPT09ICd1bmRlZmluZWQnID8gW10gOiB7fTtcbiAgICAgIHRlbXBPYmpbcHJvcE5hbWVdID0gcHJvcFZhbDtcbiAgICAgIHRlbXBPYmogPSB0ZW1wT2JqW3Byb3BOYW1lXTtcbiAgICB9XG4gIH1cbn1cblxuLyoqXG4gKiAjIyMgLmdldFBhdGhJbmZvKG9iamVjdCwgcGF0aClcbiAqXG4gKiBUaGlzIGFsbG93cyB0aGUgcmV0cmlldmFsIG9mIHByb3BlcnR5IGluZm8gaW4gYW5cbiAqIG9iamVjdCBnaXZlbiBhIHN0cmluZyBwYXRoLlxuICpcbiAqIFRoZSBwYXRoIGluZm8gY29uc2lzdHMgb2YgYW4gb2JqZWN0IHdpdGggdGhlXG4gKiBmb2xsb3dpbmcgcHJvcGVydGllczpcbiAqXG4gKiAqIHBhcmVudCAtIFRoZSBwYXJlbnQgb2JqZWN0IG9mIHRoZSBwcm9wZXJ0eSByZWZlcmVuY2VkIGJ5IGBwYXRoYFxuICogKiBuYW1lIC0gVGhlIG5hbWUgb2YgdGhlIGZpbmFsIHByb3BlcnR5LCBhIG51bWJlciBpZiBpdCB3YXMgYW4gYXJyYXkgaW5kZXhlclxuICogKiB2YWx1ZSAtIFRoZSB2YWx1ZSBvZiB0aGUgcHJvcGVydHksIGlmIGl0IGV4aXN0cywgb3RoZXJ3aXNlIGB1bmRlZmluZWRgXG4gKiAqIGV4aXN0cyAtIFdoZXRoZXIgdGhlIHByb3BlcnR5IGV4aXN0cyBvciBub3RcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gb2JqZWN0XG4gKiBAcGFyYW0ge1N0cmluZ30gcGF0aFxuICogQHJldHVybnMge09iamVjdH0gaW5mb1xuICogQG5hbWVzcGFjZSBVdGlsc1xuICogQG5hbWUgZ2V0UGF0aEluZm9cbiAqIEBhcGkgcHVibGljXG4gKi9cblxuZnVuY3Rpb24gZ2V0UGF0aEluZm8ob2JqLCBwYXRoKSB7XG4gIHZhciBwYXJzZWQgPSBwYXJzZVBhdGgocGF0aCk7XG4gIHZhciBsYXN0ID0gcGFyc2VkW3BhcnNlZC5sZW5ndGggLSAxXTtcbiAgdmFyIGluZm8gPSB7XG4gICAgcGFyZW50OlxuICAgICAgcGFyc2VkLmxlbmd0aCA+IDEgP1xuICAgICAgICBpbnRlcm5hbEdldFBhdGhWYWx1ZShvYmosIHBhcnNlZCwgcGFyc2VkLmxlbmd0aCAtIDEpIDpcbiAgICAgICAgb2JqLFxuICAgIG5hbWU6IGxhc3QucCB8fCBsYXN0LmksXG4gICAgdmFsdWU6IGludGVybmFsR2V0UGF0aFZhbHVlKG9iaiwgcGFyc2VkKSxcbiAgfTtcbiAgaW5mby5leGlzdHMgPSBoYXNQcm9wZXJ0eShpbmZvLnBhcmVudCwgaW5mby5uYW1lKTtcblxuICByZXR1cm4gaW5mbztcbn1cblxuLyoqXG4gKiAjIyMgLmdldFBhdGhWYWx1ZShvYmplY3QsIHBhdGgpXG4gKlxuICogVGhpcyBhbGxvd3MgdGhlIHJldHJpZXZhbCBvZiB2YWx1ZXMgaW4gYW5cbiAqIG9iamVjdCBnaXZlbiBhIHN0cmluZyBwYXRoLlxuICpcbiAqICAgICB2YXIgb2JqID0ge1xuICogICAgICAgICBwcm9wMToge1xuICogICAgICAgICAgICAgYXJyOiBbJ2EnLCAnYicsICdjJ11cbiAqICAgICAgICAgICAsIHN0cjogJ0hlbGxvJ1xuICogICAgICAgICB9XG4gKiAgICAgICAsIHByb3AyOiB7XG4gKiAgICAgICAgICAgICBhcnI6IFsgeyBuZXN0ZWQ6ICdVbml2ZXJzZScgfSBdXG4gKiAgICAgICAgICAgLCBzdHI6ICdIZWxsbyBhZ2FpbiEnXG4gKiAgICAgICAgIH1cbiAqICAgICB9XG4gKlxuICogVGhlIGZvbGxvd2luZyB3b3VsZCBiZSB0aGUgcmVzdWx0cy5cbiAqXG4gKiAgICAgZ2V0UGF0aFZhbHVlKG9iaiwgJ3Byb3AxLnN0cicpOyAvLyBIZWxsb1xuICogICAgIGdldFBhdGhWYWx1ZShvYmosICdwcm9wMS5hdHRbMl0nKTsgLy8gYlxuICogICAgIGdldFBhdGhWYWx1ZShvYmosICdwcm9wMi5hcnJbMF0ubmVzdGVkJyk7IC8vIFVuaXZlcnNlXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHBhcmFtIHtTdHJpbmd9IHBhdGhcbiAqIEByZXR1cm5zIHtPYmplY3R9IHZhbHVlIG9yIGB1bmRlZmluZWRgXG4gKiBAbmFtZXNwYWNlIFV0aWxzXG4gKiBAbmFtZSBnZXRQYXRoVmFsdWVcbiAqIEBhcGkgcHVibGljXG4gKi9cblxuZnVuY3Rpb24gZ2V0UGF0aFZhbHVlKG9iaiwgcGF0aCkge1xuICB2YXIgaW5mbyA9IGdldFBhdGhJbmZvKG9iaiwgcGF0aCk7XG4gIHJldHVybiBpbmZvLnZhbHVlO1xufVxuXG4vKipcbiAqICMjIyAuc2V0UGF0aFZhbHVlKG9iamVjdCwgcGF0aCwgdmFsdWUpXG4gKlxuICogRGVmaW5lIHRoZSB2YWx1ZSBpbiBhbiBvYmplY3QgYXQgYSBnaXZlbiBzdHJpbmcgcGF0aC5cbiAqXG4gKiBgYGBqc1xuICogdmFyIG9iaiA9IHtcbiAqICAgICBwcm9wMToge1xuICogICAgICAgICBhcnI6IFsnYScsICdiJywgJ2MnXVxuICogICAgICAgLCBzdHI6ICdIZWxsbydcbiAqICAgICB9XG4gKiAgICwgcHJvcDI6IHtcbiAqICAgICAgICAgYXJyOiBbIHsgbmVzdGVkOiAnVW5pdmVyc2UnIH0gXVxuICogICAgICAgLCBzdHI6ICdIZWxsbyBhZ2FpbiEnXG4gKiAgICAgfVxuICogfTtcbiAqIGBgYFxuICpcbiAqIFRoZSBmb2xsb3dpbmcgd291bGQgYmUgYWNjZXB0YWJsZS5cbiAqXG4gKiBgYGBqc1xuICogdmFyIHByb3BlcnRpZXMgPSByZXF1aXJlKCd0ZWEtcHJvcGVydGllcycpO1xuICogcHJvcGVydGllcy5zZXQob2JqLCAncHJvcDEuc3RyJywgJ0hlbGxvIFVuaXZlcnNlIScpO1xuICogcHJvcGVydGllcy5zZXQob2JqLCAncHJvcDEuYXJyWzJdJywgJ0InKTtcbiAqIHByb3BlcnRpZXMuc2V0KG9iaiwgJ3Byb3AyLmFyclswXS5uZXN0ZWQudmFsdWUnLCB7IGhlbGxvOiAndW5pdmVyc2UnIH0pO1xuICogYGBgXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IG9iamVjdFxuICogQHBhcmFtIHtTdHJpbmd9IHBhdGhcbiAqIEBwYXJhbSB7TWl4ZWR9IHZhbHVlXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG5mdW5jdGlvbiBzZXRQYXRoVmFsdWUob2JqLCBwYXRoLCB2YWwpIHtcbiAgdmFyIHBhcnNlZCA9IHBhcnNlUGF0aChwYXRoKTtcbiAgaW50ZXJuYWxTZXRQYXRoVmFsdWUob2JqLCB2YWwsIHBhcnNlZCk7XG4gIHJldHVybiBvYmo7XG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICBoYXNQcm9wZXJ0eTogaGFzUHJvcGVydHksXG4gIGdldFBhdGhJbmZvOiBnZXRQYXRoSW5mbyxcbiAgZ2V0UGF0aFZhbHVlOiBnZXRQYXRoVmFsdWUsXG4gIHNldFBhdGhWYWx1ZTogc2V0UGF0aFZhbHVlLFxufTtcbiIsIihmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XG5cdHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpIDpcblx0dHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKGZhY3RvcnkpIDpcblx0KGdsb2JhbC50eXBlRGV0ZWN0ID0gZmFjdG9yeSgpKTtcbn0odGhpcywgKGZ1bmN0aW9uICgpIHsgJ3VzZSBzdHJpY3QnO1xuXG4vKiAhXG4gKiB0eXBlLWRldGVjdFxuICogQ29weXJpZ2h0KGMpIDIwMTMgamFrZSBsdWVyIDxqYWtlQGFsb2dpY2FscGFyYWRveC5jb20+XG4gKiBNSVQgTGljZW5zZWRcbiAqL1xudmFyIHByb21pc2VFeGlzdHMgPSB0eXBlb2YgUHJvbWlzZSA9PT0gJ2Z1bmN0aW9uJztcblxuLyogZXNsaW50LWRpc2FibGUgbm8tdW5kZWYgKi9cbnZhciBnbG9iYWxPYmplY3QgPSB0eXBlb2Ygc2VsZiA9PT0gJ29iamVjdCcgPyBzZWxmIDogZ2xvYmFsOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIGlkLWJsYWNrbGlzdFxuXG52YXIgc3ltYm9sRXhpc3RzID0gdHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCc7XG52YXIgbWFwRXhpc3RzID0gdHlwZW9mIE1hcCAhPT0gJ3VuZGVmaW5lZCc7XG52YXIgc2V0RXhpc3RzID0gdHlwZW9mIFNldCAhPT0gJ3VuZGVmaW5lZCc7XG52YXIgd2Vha01hcEV4aXN0cyA9IHR5cGVvZiBXZWFrTWFwICE9PSAndW5kZWZpbmVkJztcbnZhciB3ZWFrU2V0RXhpc3RzID0gdHlwZW9mIFdlYWtTZXQgIT09ICd1bmRlZmluZWQnO1xudmFyIGRhdGFWaWV3RXhpc3RzID0gdHlwZW9mIERhdGFWaWV3ICE9PSAndW5kZWZpbmVkJztcbnZhciBzeW1ib2xJdGVyYXRvckV4aXN0cyA9IHN5bWJvbEV4aXN0cyAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yICE9PSAndW5kZWZpbmVkJztcbnZhciBzeW1ib2xUb1N0cmluZ1RhZ0V4aXN0cyA9IHN5bWJvbEV4aXN0cyAmJiB0eXBlb2YgU3ltYm9sLnRvU3RyaW5nVGFnICE9PSAndW5kZWZpbmVkJztcbnZhciBzZXRFbnRyaWVzRXhpc3RzID0gc2V0RXhpc3RzICYmIHR5cGVvZiBTZXQucHJvdG90eXBlLmVudHJpZXMgPT09ICdmdW5jdGlvbic7XG52YXIgbWFwRW50cmllc0V4aXN0cyA9IG1hcEV4aXN0cyAmJiB0eXBlb2YgTWFwLnByb3RvdHlwZS5lbnRyaWVzID09PSAnZnVuY3Rpb24nO1xudmFyIHNldEl0ZXJhdG9yUHJvdG90eXBlID0gc2V0RW50cmllc0V4aXN0cyAmJiBPYmplY3QuZ2V0UHJvdG90eXBlT2YobmV3IFNldCgpLmVudHJpZXMoKSk7XG52YXIgbWFwSXRlcmF0b3JQcm90b3R5cGUgPSBtYXBFbnRyaWVzRXhpc3RzICYmIE9iamVjdC5nZXRQcm90b3R5cGVPZihuZXcgTWFwKCkuZW50cmllcygpKTtcbnZhciBhcnJheUl0ZXJhdG9yRXhpc3RzID0gc3ltYm9sSXRlcmF0b3JFeGlzdHMgJiYgdHlwZW9mIEFycmF5LnByb3RvdHlwZVtTeW1ib2wuaXRlcmF0b3JdID09PSAnZnVuY3Rpb24nO1xudmFyIGFycmF5SXRlcmF0b3JQcm90b3R5cGUgPSBhcnJheUl0ZXJhdG9yRXhpc3RzICYmIE9iamVjdC5nZXRQcm90b3R5cGVPZihbXVtTeW1ib2wuaXRlcmF0b3JdKCkpO1xudmFyIHN0cmluZ0l0ZXJhdG9yRXhpc3RzID0gc3ltYm9sSXRlcmF0b3JFeGlzdHMgJiYgdHlwZW9mIFN0cmluZy5wcm90b3R5cGVbU3ltYm9sLml0ZXJhdG9yXSA9PT0gJ2Z1bmN0aW9uJztcbnZhciBzdHJpbmdJdGVyYXRvclByb3RvdHlwZSA9IHN0cmluZ0l0ZXJhdG9yRXhpc3RzICYmIE9iamVjdC5nZXRQcm90b3R5cGVPZignJ1tTeW1ib2wuaXRlcmF0b3JdKCkpO1xudmFyIHRvU3RyaW5nTGVmdFNsaWNlTGVuZ3RoID0gODtcbnZhciB0b1N0cmluZ1JpZ2h0U2xpY2VMZW5ndGggPSAtMTtcbi8qKlxuICogIyMjIHR5cGVPZiAob2JqKVxuICpcbiAqIFVzZXMgYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmdgIHRvIGRldGVybWluZSB0aGUgdHlwZSBvZiBhbiBvYmplY3QsXG4gKiBub3JtYWxpc2luZyBiZWhhdmlvdXIgYWNyb3NzIGVuZ2luZSB2ZXJzaW9ucyAmIHdlbGwgb3B0aW1pc2VkLlxuICpcbiAqIEBwYXJhbSB7TWl4ZWR9IG9iamVjdFxuICogQHJldHVybiB7U3RyaW5nfSBvYmplY3QgdHlwZVxuICogQGFwaSBwdWJsaWNcbiAqL1xuZnVuY3Rpb24gdHlwZURldGVjdChvYmopIHtcbiAgLyogISBTcGVlZCBvcHRpbWlzYXRpb25cbiAgICogUHJlOlxuICAgKiAgIHN0cmluZyBsaXRlcmFsICAgICB4IDMsMDM5LDAzNSBvcHMvc2VjIMKxMS42MiUgKDc4IHJ1bnMgc2FtcGxlZClcbiAgICogICBib29sZWFuIGxpdGVyYWwgICAgeCAxLDQyNCwxMzggb3BzL3NlYyDCsTQuNTQlICg3NSBydW5zIHNhbXBsZWQpXG4gICAqICAgbnVtYmVyIGxpdGVyYWwgICAgIHggMSw2NTMsMTUzIG9wcy9zZWMgwrExLjkxJSAoODIgcnVucyBzYW1wbGVkKVxuICAgKiAgIHVuZGVmaW5lZCAgICAgICAgICB4IDksOTc4LDY2MCBvcHMvc2VjIMKxMS45MiUgKDc1IHJ1bnMgc2FtcGxlZClcbiAgICogICBmdW5jdGlvbiAgICAgICAgICAgeCAyLDU1Niw3Njkgb3BzL3NlYyDCsTEuNzMlICg3NyBydW5zIHNhbXBsZWQpXG4gICAqIFBvc3Q6XG4gICAqICAgc3RyaW5nIGxpdGVyYWwgICAgIHggMzgsNTY0LDc5NiBvcHMvc2VjIMKxMS4xNSUgKDc5IHJ1bnMgc2FtcGxlZClcbiAgICogICBib29sZWFuIGxpdGVyYWwgICAgeCAzMSwxNDgsOTQwIG9wcy9zZWMgwrExLjEwJSAoNzkgcnVucyBzYW1wbGVkKVxuICAgKiAgIG51bWJlciBsaXRlcmFsICAgICB4IDMyLDY3OSwzMzAgb3BzL3NlYyDCsTEuOTAlICg3OCBydW5zIHNhbXBsZWQpXG4gICAqICAgdW5kZWZpbmVkICAgICAgICAgIHggMzIsMzYzLDM2OCBvcHMvc2VjIMKxMS4wNyUgKDgyIHJ1bnMgc2FtcGxlZClcbiAgICogICBmdW5jdGlvbiAgICAgICAgICAgeCAzMSwyOTYsODcwIG9wcy9zZWMgwrEwLjk2JSAoODMgcnVucyBzYW1wbGVkKVxuICAgKi9cbiAgdmFyIHR5cGVvZk9iaiA9IHR5cGVvZiBvYmo7XG4gIGlmICh0eXBlb2ZPYmogIT09ICdvYmplY3QnKSB7XG4gICAgcmV0dXJuIHR5cGVvZk9iajtcbiAgfVxuXG4gIC8qICEgU3BlZWQgb3B0aW1pc2F0aW9uXG4gICAqIFByZTpcbiAgICogICBudWxsICAgICAgICAgICAgICAgeCAyOCw2NDUsNzY1IG9wcy9zZWMgwrExLjE3JSAoODIgcnVucyBzYW1wbGVkKVxuICAgKiBQb3N0OlxuICAgKiAgIG51bGwgICAgICAgICAgICAgICB4IDM2LDQyOCw5NjIgb3BzL3NlYyDCsTEuMzclICg4NCBydW5zIHNhbXBsZWQpXG4gICAqL1xuICBpZiAob2JqID09PSBudWxsKSB7XG4gICAgcmV0dXJuICdudWxsJztcbiAgfVxuXG4gIC8qICEgU3BlYyBDb25mb3JtYW5jZVxuICAgKiBUZXN0OiBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHdpbmRvdylgYFxuICAgKiAgLSBOb2RlID09PSBcIltvYmplY3QgZ2xvYmFsXVwiXG4gICAqICAtIENocm9tZSA9PT0gXCJbb2JqZWN0IGdsb2JhbF1cIlxuICAgKiAgLSBGaXJlZm94ID09PSBcIltvYmplY3QgV2luZG93XVwiXG4gICAqICAtIFBoYW50b21KUyA9PT0gXCJbb2JqZWN0IFdpbmRvd11cIlxuICAgKiAgLSBTYWZhcmkgPT09IFwiW29iamVjdCBXaW5kb3ddXCJcbiAgICogIC0gSUUgMTEgPT09IFwiW29iamVjdCBXaW5kb3ddXCJcbiAgICogIC0gSUUgRWRnZSA9PT0gXCJbb2JqZWN0IFdpbmRvd11cIlxuICAgKiBUZXN0OiBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHRoaXMpYGBcbiAgICogIC0gQ2hyb21lIFdvcmtlciA9PT0gXCJbb2JqZWN0IGdsb2JhbF1cIlxuICAgKiAgLSBGaXJlZm94IFdvcmtlciA9PT0gXCJbb2JqZWN0IERlZGljYXRlZFdvcmtlckdsb2JhbFNjb3BlXVwiXG4gICAqICAtIFNhZmFyaSBXb3JrZXIgPT09IFwiW29iamVjdCBEZWRpY2F0ZWRXb3JrZXJHbG9iYWxTY29wZV1cIlxuICAgKiAgLSBJRSAxMSBXb3JrZXIgPT09IFwiW29iamVjdCBXb3JrZXJHbG9iYWxTY29wZV1cIlxuICAgKiAgLSBJRSBFZGdlIFdvcmtlciA9PT0gXCJbb2JqZWN0IFdvcmtlckdsb2JhbFNjb3BlXVwiXG4gICAqL1xuICBpZiAob2JqID09PSBnbG9iYWxPYmplY3QpIHtcbiAgICByZXR1cm4gJ2dsb2JhbCc7XG4gIH1cblxuICAvKiAhIFNwZWVkIG9wdGltaXNhdGlvblxuICAgKiBQcmU6XG4gICAqICAgYXJyYXkgbGl0ZXJhbCAgICAgIHggMiw4ODgsMzUyIG9wcy9zZWMgwrEwLjY3JSAoODIgcnVucyBzYW1wbGVkKVxuICAgKiBQb3N0OlxuICAgKiAgIGFycmF5IGxpdGVyYWwgICAgICB4IDIyLDQ3OSw2NTAgb3BzL3NlYyDCsTAuOTYlICg4MSBydW5zIHNhbXBsZWQpXG4gICAqL1xuICBpZiAoXG4gICAgQXJyYXkuaXNBcnJheShvYmopICYmXG4gICAgKHN5bWJvbFRvU3RyaW5nVGFnRXhpc3RzID09PSBmYWxzZSB8fCAhKFN5bWJvbC50b1N0cmluZ1RhZyBpbiBvYmopKVxuICApIHtcbiAgICByZXR1cm4gJ0FycmF5JztcbiAgfVxuXG4gIC8vIE5vdCBjYWNoaW5nIGV4aXN0ZW5jZSBvZiBgd2luZG93YCBhbmQgcmVsYXRlZCBwcm9wZXJ0aWVzIGR1ZSB0byBwb3RlbnRpYWxcbiAgLy8gZm9yIGB3aW5kb3dgIHRvIGJlIHVuc2V0IGJlZm9yZSB0ZXN0cyBpbiBxdWFzaS1icm93c2VyIGVudmlyb25tZW50cy5cbiAgaWYgKHR5cGVvZiB3aW5kb3cgPT09ICdvYmplY3QnICYmIHdpbmRvdyAhPT0gbnVsbCkge1xuICAgIC8qICEgU3BlYyBDb25mb3JtYW5jZVxuICAgICAqIChodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS9icm93c2Vycy5odG1sI2xvY2F0aW9uKVxuICAgICAqIFdoYXRXRyBIVE1MJDcuNy4zIC0gVGhlIGBMb2NhdGlvbmAgaW50ZXJmYWNlXG4gICAgICogVGVzdDogYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh3aW5kb3cubG9jYXRpb24pYGBcbiAgICAgKiAgLSBJRSA8PTExID09PSBcIltvYmplY3QgT2JqZWN0XVwiXG4gICAgICogIC0gSUUgRWRnZSA8PTEzID09PSBcIltvYmplY3QgT2JqZWN0XVwiXG4gICAgICovXG4gICAgaWYgKHR5cGVvZiB3aW5kb3cubG9jYXRpb24gPT09ICdvYmplY3QnICYmIG9iaiA9PT0gd2luZG93LmxvY2F0aW9uKSB7XG4gICAgICByZXR1cm4gJ0xvY2F0aW9uJztcbiAgICB9XG5cbiAgICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICAgKiAoaHR0cHM6Ly9odG1sLnNwZWMud2hhdHdnLm9yZy8jZG9jdW1lbnQpXG4gICAgICogV2hhdFdHIEhUTUwkMy4xLjEgLSBUaGUgYERvY3VtZW50YCBvYmplY3RcbiAgICAgKiBOb3RlOiBNb3N0IGJyb3dzZXJzIGN1cnJlbnRseSBhZGhlciB0byB0aGUgVzNDIERPTSBMZXZlbCAyIHNwZWNcbiAgICAgKiAgICAgICAoaHR0cHM6Ly93d3cudzMub3JnL1RSL0RPTS1MZXZlbC0yLUhUTUwvaHRtbC5odG1sI0lELTI2ODA5MjY4KVxuICAgICAqICAgICAgIHdoaWNoIHN1Z2dlc3RzIHRoYXQgYnJvd3NlcnMgc2hvdWxkIHVzZSBIVE1MVGFibGVDZWxsRWxlbWVudCBmb3JcbiAgICAgKiAgICAgICBib3RoIFREIGFuZCBUSCBlbGVtZW50cy4gV2hhdFdHIHNlcGFyYXRlcyB0aGVzZS5cbiAgICAgKiAgICAgICBXaGF0V0cgSFRNTCBzdGF0ZXM6XG4gICAgICogICAgICAgICA+IEZvciBoaXN0b3JpY2FsIHJlYXNvbnMsIFdpbmRvdyBvYmplY3RzIG11c3QgYWxzbyBoYXZlIGFcbiAgICAgKiAgICAgICAgID4gd3JpdGFibGUsIGNvbmZpZ3VyYWJsZSwgbm9uLWVudW1lcmFibGUgcHJvcGVydHkgbmFtZWRcbiAgICAgKiAgICAgICAgID4gSFRNTERvY3VtZW50IHdob3NlIHZhbHVlIGlzIHRoZSBEb2N1bWVudCBpbnRlcmZhY2Ugb2JqZWN0LlxuICAgICAqIFRlc3Q6IGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoZG9jdW1lbnQpYGBcbiAgICAgKiAgLSBDaHJvbWUgPT09IFwiW29iamVjdCBIVE1MRG9jdW1lbnRdXCJcbiAgICAgKiAgLSBGaXJlZm94ID09PSBcIltvYmplY3QgSFRNTERvY3VtZW50XVwiXG4gICAgICogIC0gU2FmYXJpID09PSBcIltvYmplY3QgSFRNTERvY3VtZW50XVwiXG4gICAgICogIC0gSUUgPD0xMCA9PT0gXCJbb2JqZWN0IERvY3VtZW50XVwiXG4gICAgICogIC0gSUUgMTEgPT09IFwiW29iamVjdCBIVE1MRG9jdW1lbnRdXCJcbiAgICAgKiAgLSBJRSBFZGdlIDw9MTMgPT09IFwiW29iamVjdCBIVE1MRG9jdW1lbnRdXCJcbiAgICAgKi9cbiAgICBpZiAodHlwZW9mIHdpbmRvdy5kb2N1bWVudCA9PT0gJ29iamVjdCcgJiYgb2JqID09PSB3aW5kb3cuZG9jdW1lbnQpIHtcbiAgICAgIHJldHVybiAnRG9jdW1lbnQnO1xuICAgIH1cblxuICAgIGlmICh0eXBlb2Ygd2luZG93Lm5hdmlnYXRvciA9PT0gJ29iamVjdCcpIHtcbiAgICAgIC8qICEgU3BlYyBDb25mb3JtYW5jZVxuICAgICAgICogKGh0dHBzOi8vaHRtbC5zcGVjLndoYXR3Zy5vcmcvbXVsdGlwYWdlL3dlYmFwcGFwaXMuaHRtbCNtaW1ldHlwZWFycmF5KVxuICAgICAgICogV2hhdFdHIEhUTUwkOC42LjEuNSAtIFBsdWdpbnMgLSBJbnRlcmZhY2UgTWltZVR5cGVBcnJheVxuICAgICAgICogVGVzdDogYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChuYXZpZ2F0b3IubWltZVR5cGVzKWBgXG4gICAgICAgKiAgLSBJRSA8PTEwID09PSBcIltvYmplY3QgTVNNaW1lVHlwZXNDb2xsZWN0aW9uXVwiXG4gICAgICAgKi9cbiAgICAgIGlmICh0eXBlb2Ygd2luZG93Lm5hdmlnYXRvci5taW1lVHlwZXMgPT09ICdvYmplY3QnICYmXG4gICAgICAgICAgb2JqID09PSB3aW5kb3cubmF2aWdhdG9yLm1pbWVUeXBlcykge1xuICAgICAgICByZXR1cm4gJ01pbWVUeXBlQXJyYXknO1xuICAgICAgfVxuXG4gICAgICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICAgICAqIChodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnL211bHRpcGFnZS93ZWJhcHBhcGlzLmh0bWwjcGx1Z2luYXJyYXkpXG4gICAgICAgKiBXaGF0V0cgSFRNTCQ4LjYuMS41IC0gUGx1Z2lucyAtIEludGVyZmFjZSBQbHVnaW5BcnJheVxuICAgICAgICogVGVzdDogYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChuYXZpZ2F0b3IucGx1Z2lucylgYFxuICAgICAgICogIC0gSUUgPD0xMCA9PT0gXCJbb2JqZWN0IE1TUGx1Z2luc0NvbGxlY3Rpb25dXCJcbiAgICAgICAqL1xuICAgICAgaWYgKHR5cGVvZiB3aW5kb3cubmF2aWdhdG9yLnBsdWdpbnMgPT09ICdvYmplY3QnICYmXG4gICAgICAgICAgb2JqID09PSB3aW5kb3cubmF2aWdhdG9yLnBsdWdpbnMpIHtcbiAgICAgICAgcmV0dXJuICdQbHVnaW5BcnJheSc7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCh0eXBlb2Ygd2luZG93LkhUTUxFbGVtZW50ID09PSAnZnVuY3Rpb24nIHx8XG4gICAgICAgIHR5cGVvZiB3aW5kb3cuSFRNTEVsZW1lbnQgPT09ICdvYmplY3QnKSAmJlxuICAgICAgICBvYmogaW5zdGFuY2VvZiB3aW5kb3cuSFRNTEVsZW1lbnQpIHtcbiAgICAgIC8qICEgU3BlYyBDb25mb3JtYW5jZVxuICAgICAgKiAoaHR0cHM6Ly9odG1sLnNwZWMud2hhdHdnLm9yZy9tdWx0aXBhZ2Uvd2ViYXBwYXBpcy5odG1sI3BsdWdpbmFycmF5KVxuICAgICAgKiBXaGF0V0cgSFRNTCQ0LjQuNCAtIFRoZSBgYmxvY2txdW90ZWAgZWxlbWVudCAtIEludGVyZmFjZSBgSFRNTFF1b3RlRWxlbWVudGBcbiAgICAgICogVGVzdDogYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdibG9ja3F1b3RlJykpYGBcbiAgICAgICogIC0gSUUgPD0xMCA9PT0gXCJbb2JqZWN0IEhUTUxCbG9ja0VsZW1lbnRdXCJcbiAgICAgICovXG4gICAgICBpZiAob2JqLnRhZ05hbWUgPT09ICdCTE9DS1FVT1RFJykge1xuICAgICAgICByZXR1cm4gJ0hUTUxRdW90ZUVsZW1lbnQnO1xuICAgICAgfVxuXG4gICAgICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICAgICAqIChodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnLyNodG1sdGFibGVkYXRhY2VsbGVsZW1lbnQpXG4gICAgICAgKiBXaGF0V0cgSFRNTCQ0LjkuOSAtIFRoZSBgdGRgIGVsZW1lbnQgLSBJbnRlcmZhY2UgYEhUTUxUYWJsZURhdGFDZWxsRWxlbWVudGBcbiAgICAgICAqIE5vdGU6IE1vc3QgYnJvd3NlcnMgY3VycmVudGx5IGFkaGVyIHRvIHRoZSBXM0MgRE9NIExldmVsIDIgc3BlY1xuICAgICAgICogICAgICAgKGh0dHBzOi8vd3d3LnczLm9yZy9UUi9ET00tTGV2ZWwtMi1IVE1ML2h0bWwuaHRtbCNJRC04MjkxNTA3NSlcbiAgICAgICAqICAgICAgIHdoaWNoIHN1Z2dlc3RzIHRoYXQgYnJvd3NlcnMgc2hvdWxkIHVzZSBIVE1MVGFibGVDZWxsRWxlbWVudCBmb3JcbiAgICAgICAqICAgICAgIGJvdGggVEQgYW5kIFRIIGVsZW1lbnRzLiBXaGF0V0cgc2VwYXJhdGVzIHRoZXNlLlxuICAgICAgICogVGVzdDogT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RkJykpXG4gICAgICAgKiAgLSBDaHJvbWUgPT09IFwiW29iamVjdCBIVE1MVGFibGVDZWxsRWxlbWVudF1cIlxuICAgICAgICogIC0gRmlyZWZveCA9PT0gXCJbb2JqZWN0IEhUTUxUYWJsZUNlbGxFbGVtZW50XVwiXG4gICAgICAgKiAgLSBTYWZhcmkgPT09IFwiW29iamVjdCBIVE1MVGFibGVDZWxsRWxlbWVudF1cIlxuICAgICAgICovXG4gICAgICBpZiAob2JqLnRhZ05hbWUgPT09ICdURCcpIHtcbiAgICAgICAgcmV0dXJuICdIVE1MVGFibGVEYXRhQ2VsbEVsZW1lbnQnO1xuICAgICAgfVxuXG4gICAgICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICAgICAqIChodHRwczovL2h0bWwuc3BlYy53aGF0d2cub3JnLyNodG1sdGFibGVoZWFkZXJjZWxsZWxlbWVudClcbiAgICAgICAqIFdoYXRXRyBIVE1MJDQuOS45IC0gVGhlIGB0ZGAgZWxlbWVudCAtIEludGVyZmFjZSBgSFRNTFRhYmxlSGVhZGVyQ2VsbEVsZW1lbnRgXG4gICAgICAgKiBOb3RlOiBNb3N0IGJyb3dzZXJzIGN1cnJlbnRseSBhZGhlciB0byB0aGUgVzNDIERPTSBMZXZlbCAyIHNwZWNcbiAgICAgICAqICAgICAgIChodHRwczovL3d3dy53My5vcmcvVFIvRE9NLUxldmVsLTItSFRNTC9odG1sLmh0bWwjSUQtODI5MTUwNzUpXG4gICAgICAgKiAgICAgICB3aGljaCBzdWdnZXN0cyB0aGF0IGJyb3dzZXJzIHNob3VsZCB1c2UgSFRNTFRhYmxlQ2VsbEVsZW1lbnQgZm9yXG4gICAgICAgKiAgICAgICBib3RoIFREIGFuZCBUSCBlbGVtZW50cy4gV2hhdFdHIHNlcGFyYXRlcyB0aGVzZS5cbiAgICAgICAqIFRlc3Q6IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0aCcpKVxuICAgICAgICogIC0gQ2hyb21lID09PSBcIltvYmplY3QgSFRNTFRhYmxlQ2VsbEVsZW1lbnRdXCJcbiAgICAgICAqICAtIEZpcmVmb3ggPT09IFwiW29iamVjdCBIVE1MVGFibGVDZWxsRWxlbWVudF1cIlxuICAgICAgICogIC0gU2FmYXJpID09PSBcIltvYmplY3QgSFRNTFRhYmxlQ2VsbEVsZW1lbnRdXCJcbiAgICAgICAqL1xuICAgICAgaWYgKG9iai50YWdOYW1lID09PSAnVEgnKSB7XG4gICAgICAgIHJldHVybiAnSFRNTFRhYmxlSGVhZGVyQ2VsbEVsZW1lbnQnO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qICEgU3BlZWQgb3B0aW1pc2F0aW9uXG4gICogUHJlOlxuICAqICAgRmxvYXQ2NEFycmF5ICAgICAgIHggNjI1LDY0NCBvcHMvc2VjIMKxMS41OCUgKDgwIHJ1bnMgc2FtcGxlZClcbiAgKiAgIEZsb2F0MzJBcnJheSAgICAgICB4IDEsMjc5LDg1MiBvcHMvc2VjIMKxMi45MSUgKDc3IHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQzMkFycmF5ICAgICAgICB4IDEsMTc4LDE4NSBvcHMvc2VjIMKxMS45NSUgKDgzIHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQxNkFycmF5ICAgICAgICB4IDEsMDA4LDM4MCBvcHMvc2VjIMKxMi4yNSUgKDgwIHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQ4QXJyYXkgICAgICAgICB4IDEsMTI4LDA0MCBvcHMvc2VjIMKxMi4xMSUgKDgxIHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDMyQXJyYXkgICAgICAgICB4IDEsMTcwLDExOSBvcHMvc2VjIMKxMi44OCUgKDgwIHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDE2QXJyYXkgICAgICAgICB4IDEsMTc2LDM0OCBvcHMvc2VjIMKxNS43OSUgKDg2IHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDhBcnJheSAgICAgICAgICB4IDEsMDU4LDcwNyBvcHMvc2VjIMKxNC45NCUgKDc3IHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQ4Q2xhbXBlZEFycmF5ICB4IDEsMTEwLDYzMyBvcHMvc2VjIMKxNC4yMCUgKDgwIHJ1bnMgc2FtcGxlZClcbiAgKiBQb3N0OlxuICAqICAgRmxvYXQ2NEFycmF5ICAgICAgIHggNywxMDUsNjcxIG9wcy9zZWMgwrExMy40NyUgKDY0IHJ1bnMgc2FtcGxlZClcbiAgKiAgIEZsb2F0MzJBcnJheSAgICAgICB4IDUsODg3LDkxMiBvcHMvc2VjIMKxMS40NiUgKDgyIHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQzMkFycmF5ICAgICAgICB4IDYsNDkxLDY2MSBvcHMvc2VjIMKxMS43NiUgKDc5IHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQxNkFycmF5ICAgICAgICB4IDYsNTU5LDc5NSBvcHMvc2VjIMKxMS42NyUgKDgyIHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQ4QXJyYXkgICAgICAgICB4IDYsNDYzLDk2NiBvcHMvc2VjIMKxMS40MyUgKDg1IHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDMyQXJyYXkgICAgICAgICB4IDUsNjQxLDg0MSBvcHMvc2VjIMKxMy40OSUgKDgxIHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDE2QXJyYXkgICAgICAgICB4IDYsNTgzLDUxMSBvcHMvc2VjIMKxMS45OCUgKDgwIHJ1bnMgc2FtcGxlZClcbiAgKiAgIEludDhBcnJheSAgICAgICAgICB4IDYsNjA2LDA3OCBvcHMvc2VjIMKxMS43NCUgKDgxIHJ1bnMgc2FtcGxlZClcbiAgKiAgIFVpbnQ4Q2xhbXBlZEFycmF5ICB4IDYsNjAyLDIyNCBvcHMvc2VjIMKxMS43NyUgKDgzIHJ1bnMgc2FtcGxlZClcbiAgKi9cbiAgdmFyIHN0cmluZ1RhZyA9IChzeW1ib2xUb1N0cmluZ1RhZ0V4aXN0cyAmJiBvYmpbU3ltYm9sLnRvU3RyaW5nVGFnXSk7XG4gIGlmICh0eXBlb2Ygc3RyaW5nVGFnID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBzdHJpbmdUYWc7XG4gIH1cblxuICB2YXIgb2JqUHJvdG90eXBlID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKG9iaik7XG4gIC8qICEgU3BlZWQgb3B0aW1pc2F0aW9uXG4gICogUHJlOlxuICAqICAgcmVnZXggbGl0ZXJhbCAgICAgIHggMSw3NzIsMzg1IG9wcy9zZWMgwrExLjg1JSAoNzcgcnVucyBzYW1wbGVkKVxuICAqICAgcmVnZXggY29uc3RydWN0b3IgIHggMiwxNDMsNjM0IG9wcy9zZWMgwrEyLjQ2JSAoNzggcnVucyBzYW1wbGVkKVxuICAqIFBvc3Q6XG4gICogICByZWdleCBsaXRlcmFsICAgICAgeCAzLDkyOCwwMDkgb3BzL3NlYyDCsTAuNjUlICg3OCBydW5zIHNhbXBsZWQpXG4gICogICByZWdleCBjb25zdHJ1Y3RvciAgeCAzLDkzMSwxMDggb3BzL3NlYyDCsTAuNTglICg4NCBydW5zIHNhbXBsZWQpXG4gICovXG4gIGlmIChvYmpQcm90b3R5cGUgPT09IFJlZ0V4cC5wcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ1JlZ0V4cCc7XG4gIH1cblxuICAvKiAhIFNwZWVkIG9wdGltaXNhdGlvblxuICAqIFByZTpcbiAgKiAgIGRhdGUgICAgICAgICAgICAgICB4IDIsMTMwLDA3NCBvcHMvc2VjIMKxNC40MiUgKDY4IHJ1bnMgc2FtcGxlZClcbiAgKiBQb3N0OlxuICAqICAgZGF0ZSAgICAgICAgICAgICAgIHggMyw5NTMsNzc5IG9wcy9zZWMgwrExLjM1JSAoNzcgcnVucyBzYW1wbGVkKVxuICAqL1xuICBpZiAob2JqUHJvdG90eXBlID09PSBEYXRlLnByb3RvdHlwZSkge1xuICAgIHJldHVybiAnRGF0ZSc7XG4gIH1cblxuICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICogKGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvaW5kZXguaHRtbCNzZWMtcHJvbWlzZS5wcm90b3R5cGUtQEB0b3N0cmluZ3RhZylcbiAgICogRVM2JDI1LjQuNS40IC0gUHJvbWlzZS5wcm90b3R5cGVbQEB0b1N0cmluZ1RhZ10gc2hvdWxkIGJlIFwiUHJvbWlzZVwiOlxuICAgKiBUZXN0OiBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKFByb21pc2UucmVzb2x2ZSgpKWBgXG4gICAqICAtIENocm9tZSA8PTQ3ID09PSBcIltvYmplY3QgT2JqZWN0XVwiXG4gICAqICAtIEVkZ2UgPD0yMCA9PT0gXCJbb2JqZWN0IE9iamVjdF1cIlxuICAgKiAgLSBGaXJlZm94IDI5LUxhdGVzdCA9PT0gXCJbb2JqZWN0IFByb21pc2VdXCJcbiAgICogIC0gU2FmYXJpIDcuMS1MYXRlc3QgPT09IFwiW29iamVjdCBQcm9taXNlXVwiXG4gICAqL1xuICBpZiAocHJvbWlzZUV4aXN0cyAmJiBvYmpQcm90b3R5cGUgPT09IFByb21pc2UucHJvdG90eXBlKSB7XG4gICAgcmV0dXJuICdQcm9taXNlJztcbiAgfVxuXG4gIC8qICEgU3BlZWQgb3B0aW1pc2F0aW9uXG4gICogUHJlOlxuICAqICAgc2V0ICAgICAgICAgICAgICAgIHggMiwyMjIsMTg2IG9wcy9zZWMgwrExLjMxJSAoODIgcnVucyBzYW1wbGVkKVxuICAqIFBvc3Q6XG4gICogICBzZXQgICAgICAgICAgICAgICAgeCA0LDU0NSw4Nzkgb3BzL3NlYyDCsTEuMTMlICg4MyBydW5zIHNhbXBsZWQpXG4gICovXG4gIGlmIChzZXRFeGlzdHMgJiYgb2JqUHJvdG90eXBlID09PSBTZXQucHJvdG90eXBlKSB7XG4gICAgcmV0dXJuICdTZXQnO1xuICB9XG5cbiAgLyogISBTcGVlZCBvcHRpbWlzYXRpb25cbiAgKiBQcmU6XG4gICogICBtYXAgICAgICAgICAgICAgICAgeCAyLDM5Niw4NDIgb3BzL3NlYyDCsTEuNTklICg4MSBydW5zIHNhbXBsZWQpXG4gICogUG9zdDpcbiAgKiAgIG1hcCAgICAgICAgICAgICAgICB4IDQsMTgzLDk0NSBvcHMvc2VjIMKxNi41OSUgKDgyIHJ1bnMgc2FtcGxlZClcbiAgKi9cbiAgaWYgKG1hcEV4aXN0cyAmJiBvYmpQcm90b3R5cGUgPT09IE1hcC5wcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ01hcCc7XG4gIH1cblxuICAvKiAhIFNwZWVkIG9wdGltaXNhdGlvblxuICAqIFByZTpcbiAgKiAgIHdlYWtzZXQgICAgICAgICAgICB4IDEsMzIzLDIyMCBvcHMvc2VjIMKxMi4xNyUgKDc2IHJ1bnMgc2FtcGxlZClcbiAgKiBQb3N0OlxuICAqICAgd2Vha3NldCAgICAgICAgICAgIHggNCwyMzcsNTEwIG9wcy9zZWMgwrEyLjAxJSAoNzcgcnVucyBzYW1wbGVkKVxuICAqL1xuICBpZiAod2Vha1NldEV4aXN0cyAmJiBvYmpQcm90b3R5cGUgPT09IFdlYWtTZXQucHJvdG90eXBlKSB7XG4gICAgcmV0dXJuICdXZWFrU2V0JztcbiAgfVxuXG4gIC8qICEgU3BlZWQgb3B0aW1pc2F0aW9uXG4gICogUHJlOlxuICAqICAgd2Vha21hcCAgICAgICAgICAgIHggMSw1MDAsMjYwIG9wcy9zZWMgwrEyLjAyJSAoNzggcnVucyBzYW1wbGVkKVxuICAqIFBvc3Q6XG4gICogICB3ZWFrbWFwICAgICAgICAgICAgeCAzLDg4MSwzODQgb3BzL3NlYyDCsTEuNDUlICg4MiBydW5zIHNhbXBsZWQpXG4gICovXG4gIGlmICh3ZWFrTWFwRXhpc3RzICYmIG9ialByb3RvdHlwZSA9PT0gV2Vha01hcC5wcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ1dlYWtNYXAnO1xuICB9XG5cbiAgLyogISBTcGVjIENvbmZvcm1hbmNlXG4gICAqIChodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wL2luZGV4Lmh0bWwjc2VjLWRhdGF2aWV3LnByb3RvdHlwZS1AQHRvc3RyaW5ndGFnKVxuICAgKiBFUzYkMjQuMi40LjIxIC0gRGF0YVZpZXcucHJvdG90eXBlW0BAdG9TdHJpbmdUYWddIHNob3VsZCBiZSBcIkRhdGFWaWV3XCI6XG4gICAqIFRlc3Q6IGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobmV3IERhdGFWaWV3KG5ldyBBcnJheUJ1ZmZlcigxKSkpYGBcbiAgICogIC0gRWRnZSA8PTEzID09PSBcIltvYmplY3QgT2JqZWN0XVwiXG4gICAqL1xuICBpZiAoZGF0YVZpZXdFeGlzdHMgJiYgb2JqUHJvdG90eXBlID09PSBEYXRhVmlldy5wcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ0RhdGFWaWV3JztcbiAgfVxuXG4gIC8qICEgU3BlYyBDb25mb3JtYW5jZVxuICAgKiAoaHR0cDovL3d3dy5lY21hLWludGVybmF0aW9uYWwub3JnL2VjbWEtMjYyLzYuMC9pbmRleC5odG1sI3NlYy0lbWFwaXRlcmF0b3Jwcm90b3R5cGUlLUBAdG9zdHJpbmd0YWcpXG4gICAqIEVTNiQyMy4xLjUuMi4yIC0gJU1hcEl0ZXJhdG9yUHJvdG90eXBlJVtAQHRvU3RyaW5nVGFnXSBzaG91bGQgYmUgXCJNYXAgSXRlcmF0b3JcIjpcbiAgICogVGVzdDogYE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChuZXcgTWFwKCkuZW50cmllcygpKWBgXG4gICAqICAtIEVkZ2UgPD0xMyA9PT0gXCJbb2JqZWN0IE9iamVjdF1cIlxuICAgKi9cbiAgaWYgKG1hcEV4aXN0cyAmJiBvYmpQcm90b3R5cGUgPT09IG1hcEl0ZXJhdG9yUHJvdG90eXBlKSB7XG4gICAgcmV0dXJuICdNYXAgSXRlcmF0b3InO1xuICB9XG5cbiAgLyogISBTcGVjIENvbmZvcm1hbmNlXG4gICAqIChodHRwOi8vd3d3LmVjbWEtaW50ZXJuYXRpb25hbC5vcmcvZWNtYS0yNjIvNi4wL2luZGV4Lmh0bWwjc2VjLSVzZXRpdGVyYXRvcnByb3RvdHlwZSUtQEB0b3N0cmluZ3RhZylcbiAgICogRVM2JDIzLjIuNS4yLjIgLSAlU2V0SXRlcmF0b3JQcm90b3R5cGUlW0BAdG9TdHJpbmdUYWddIHNob3VsZCBiZSBcIlNldCBJdGVyYXRvclwiOlxuICAgKiBUZXN0OiBgT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG5ldyBTZXQoKS5lbnRyaWVzKCkpYGBcbiAgICogIC0gRWRnZSA8PTEzID09PSBcIltvYmplY3QgT2JqZWN0XVwiXG4gICAqL1xuICBpZiAoc2V0RXhpc3RzICYmIG9ialByb3RvdHlwZSA9PT0gc2V0SXRlcmF0b3JQcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ1NldCBJdGVyYXRvcic7XG4gIH1cblxuICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICogKGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvaW5kZXguaHRtbCNzZWMtJWFycmF5aXRlcmF0b3Jwcm90b3R5cGUlLUBAdG9zdHJpbmd0YWcpXG4gICAqIEVTNiQyMi4xLjUuMi4yIC0gJUFycmF5SXRlcmF0b3JQcm90b3R5cGUlW0BAdG9TdHJpbmdUYWddIHNob3VsZCBiZSBcIkFycmF5IEl0ZXJhdG9yXCI6XG4gICAqIFRlc3Q6IGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoW11bU3ltYm9sLml0ZXJhdG9yXSgpKWBgXG4gICAqICAtIEVkZ2UgPD0xMyA9PT0gXCJbb2JqZWN0IE9iamVjdF1cIlxuICAgKi9cbiAgaWYgKGFycmF5SXRlcmF0b3JFeGlzdHMgJiYgb2JqUHJvdG90eXBlID09PSBhcnJheUl0ZXJhdG9yUHJvdG90eXBlKSB7XG4gICAgcmV0dXJuICdBcnJheSBJdGVyYXRvcic7XG4gIH1cblxuICAvKiAhIFNwZWMgQ29uZm9ybWFuY2VcbiAgICogKGh0dHA6Ly93d3cuZWNtYS1pbnRlcm5hdGlvbmFsLm9yZy9lY21hLTI2Mi82LjAvaW5kZXguaHRtbCNzZWMtJXN0cmluZ2l0ZXJhdG9ycHJvdG90eXBlJS1AQHRvc3RyaW5ndGFnKVxuICAgKiBFUzYkMjEuMS41LjIuMiAtICVTdHJpbmdJdGVyYXRvclByb3RvdHlwZSVbQEB0b1N0cmluZ1RhZ10gc2hvdWxkIGJlIFwiU3RyaW5nIEl0ZXJhdG9yXCI6XG4gICAqIFRlc3Q6IGBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoJydbU3ltYm9sLml0ZXJhdG9yXSgpKWBgXG4gICAqICAtIEVkZ2UgPD0xMyA9PT0gXCJbb2JqZWN0IE9iamVjdF1cIlxuICAgKi9cbiAgaWYgKHN0cmluZ0l0ZXJhdG9yRXhpc3RzICYmIG9ialByb3RvdHlwZSA9PT0gc3RyaW5nSXRlcmF0b3JQcm90b3R5cGUpIHtcbiAgICByZXR1cm4gJ1N0cmluZyBJdGVyYXRvcic7XG4gIH1cblxuICAvKiAhIFNwZWVkIG9wdGltaXNhdGlvblxuICAqIFByZTpcbiAgKiAgIG9iamVjdCBmcm9tIG51bGwgICB4IDIsNDI0LDMyMCBvcHMvc2VjIMKxMS42NyUgKDc2IHJ1bnMgc2FtcGxlZClcbiAgKiBQb3N0OlxuICAqICAgb2JqZWN0IGZyb20gbnVsbCAgIHggNSw4MzgsMDAwIG9wcy9zZWMgwrEwLjk5JSAoODQgcnVucyBzYW1wbGVkKVxuICAqL1xuICBpZiAob2JqUHJvdG90eXBlID09PSBudWxsKSB7XG4gICAgcmV0dXJuICdPYmplY3QnO1xuICB9XG5cbiAgcmV0dXJuIE9iamVjdFxuICAgIC5wcm90b3R5cGVcbiAgICAudG9TdHJpbmdcbiAgICAuY2FsbChvYmopXG4gICAgLnNsaWNlKHRvU3RyaW5nTGVmdFNsaWNlTGVuZ3RoLCB0b1N0cmluZ1JpZ2h0U2xpY2VMZW5ndGgpO1xufVxuXG5yZXR1cm4gdHlwZURldGVjdDtcblxufSkpKTtcbiIsImltcG9ydCBjaGFpIGZyb20gJy4vaW5kZXguanMnO1xuXG5leHBvcnQgY29uc3QgZXhwZWN0ID0gY2hhaS5leHBlY3Q7XG5leHBvcnQgY29uc3QgdmVyc2lvbiA9IGNoYWkudmVyc2lvbjtcbmV4cG9ydCBjb25zdCBBc3NlcnRpb24gPSBjaGFpLkFzc2VydGlvbjtcbmV4cG9ydCBjb25zdCBBc3NlcnRpb25FcnJvciA9IGNoYWkuQXNzZXJ0aW9uRXJyb3I7XG5leHBvcnQgY29uc3QgdXRpbCA9IGNoYWkudXRpbDtcbmV4cG9ydCBjb25zdCBjb25maWcgPSBjaGFpLmNvbmZpZztcbmV4cG9ydCBjb25zdCB1c2UgPSBjaGFpLnVzZTtcbmV4cG9ydCBjb25zdCBzaG91bGQgPSBjaGFpLnNob3VsZDtcbmV4cG9ydCBjb25zdCBhc3NlcnQgPSBjaGFpLmFzc2VydDtcbmV4cG9ydCBjb25zdCBjb3JlID0gY2hhaS5jb3JlO1xuXG5leHBvcnQgZGVmYXVsdCBjaGFpO1xuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9