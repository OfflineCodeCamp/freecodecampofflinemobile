/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!********************************************!*\
  !*** ./src/client/workers/sass-compile.js ***!
  \********************************************/
// work around for SASS error in Edge
// https://github.com/medialize/sass.js/issues/96#issuecomment-424386171
if (!self.crypto) {
  self.crypto = {
    getRandomValues: function getRandomValues(array) {
      for (var i = 0, l = array.length; i < l; i++) {
        array[i] = Math.floor(Math.random() * 256);
      }

      return array;
    }
  };
}

self.importScripts('/js/sass.sync.js');

self.onmessage = function (e) {
  var data = e.data;
  self.Sass.compile(data, function (result) {
    if (result.status === 0) {
      self.postMessage(result.text);
    } else {
      self.postMessage({
        type: 'error',
        data: {
          message: result.formatted
        }
      });
    }
  });
};

self.postMessage({
  type: 'contentLoaded'
});
/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Fzcy1jb21waWxlLmQyMWY4MGU5MTRkMTIzNWVlNTE5LmpzIiwibWFwcGluZ3MiOiI7Ozs7O0FBQUE7QUFDQTtBQUNBLElBQUksQ0FBQ0EsSUFBSSxDQUFDQyxNQUFWLEVBQWtCO0FBQ2hCRCxFQUFBQSxJQUFJLENBQUNDLE1BQUwsR0FBYztBQUNaQyxJQUFBQSxlQUFlLEVBQUUseUJBQVVDLEtBQVYsRUFBaUI7QUFDaEMsV0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBUixFQUFXQyxDQUFDLEdBQUdGLEtBQUssQ0FBQ0csTUFBMUIsRUFBa0NGLENBQUMsR0FBR0MsQ0FBdEMsRUFBeUNELENBQUMsRUFBMUMsRUFBOEM7QUFDNUNELFFBQUFBLEtBQUssQ0FBQ0MsQ0FBRCxDQUFMLEdBQVdHLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsS0FBZ0IsR0FBM0IsQ0FBWDtBQUNEOztBQUNELGFBQU9OLEtBQVA7QUFDRDtBQU5XLEdBQWQ7QUFRRDs7QUFFREgsSUFBSSxDQUFDVSxhQUFMLENBQW1CLGtCQUFuQjs7QUFFQVYsSUFBSSxDQUFDVyxTQUFMLEdBQWlCLFVBQUFDLENBQUMsRUFBSTtBQUNwQixNQUFNQyxJQUFJLEdBQUdELENBQUMsQ0FBQ0MsSUFBZjtBQUNBYixFQUFBQSxJQUFJLENBQUNjLElBQUwsQ0FBVUMsT0FBVixDQUFrQkYsSUFBbEIsRUFBd0IsVUFBQUcsTUFBTSxFQUFJO0FBQ2hDLFFBQUlBLE1BQU0sQ0FBQ0MsTUFBUCxLQUFrQixDQUF0QixFQUF5QjtBQUN2QmpCLE1BQUFBLElBQUksQ0FBQ2tCLFdBQUwsQ0FBaUJGLE1BQU0sQ0FBQ0csSUFBeEI7QUFDRCxLQUZELE1BRU87QUFDTG5CLE1BQUFBLElBQUksQ0FBQ2tCLFdBQUwsQ0FBaUI7QUFBRUUsUUFBQUEsSUFBSSxFQUFFLE9BQVI7QUFBaUJQLFFBQUFBLElBQUksRUFBRTtBQUFFUSxVQUFBQSxPQUFPLEVBQUVMLE1BQU0sQ0FBQ007QUFBbEI7QUFBdkIsT0FBakI7QUFDRDtBQUNGLEdBTkQ7QUFPRCxDQVREOztBQVdBdEIsSUFBSSxDQUFDa0IsV0FBTCxDQUFpQjtBQUFFRSxFQUFBQSxJQUFJLEVBQUU7QUFBUixDQUFqQixFIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vQGZyZWVjb2RlY2FtcC9jbGllbnQvLi9zcmMvY2xpZW50L3dvcmtlcnMvc2Fzcy1jb21waWxlLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIHdvcmsgYXJvdW5kIGZvciBTQVNTIGVycm9yIGluIEVkZ2Vcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tZWRpYWxpemUvc2Fzcy5qcy9pc3N1ZXMvOTYjaXNzdWVjb21tZW50LTQyNDM4NjE3MVxuaWYgKCFzZWxmLmNyeXB0bykge1xuICBzZWxmLmNyeXB0byA9IHtcbiAgICBnZXRSYW5kb21WYWx1ZXM6IGZ1bmN0aW9uIChhcnJheSkge1xuICAgICAgZm9yICh2YXIgaSA9IDAsIGwgPSBhcnJheS5sZW5ndGg7IGkgPCBsOyBpKyspIHtcbiAgICAgICAgYXJyYXlbaV0gPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAyNTYpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGFycmF5O1xuICAgIH1cbiAgfTtcbn1cblxuc2VsZi5pbXBvcnRTY3JpcHRzKCcvanMvc2Fzcy5zeW5jLmpzJyk7XG5cbnNlbGYub25tZXNzYWdlID0gZSA9PiB7XG4gIGNvbnN0IGRhdGEgPSBlLmRhdGE7XG4gIHNlbGYuU2Fzcy5jb21waWxlKGRhdGEsIHJlc3VsdCA9PiB7XG4gICAgaWYgKHJlc3VsdC5zdGF0dXMgPT09IDApIHtcbiAgICAgIHNlbGYucG9zdE1lc3NhZ2UocmVzdWx0LnRleHQpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzZWxmLnBvc3RNZXNzYWdlKHsgdHlwZTogJ2Vycm9yJywgZGF0YTogeyBtZXNzYWdlOiByZXN1bHQuZm9ybWF0dGVkIH0gfSk7XG4gICAgfVxuICB9KTtcbn07XG5cbnNlbGYucG9zdE1lc3NhZ2UoeyB0eXBlOiAnY29udGVudExvYWRlZCcgfSk7XG4iXSwibmFtZXMiOlsic2VsZiIsImNyeXB0byIsImdldFJhbmRvbVZhbHVlcyIsImFycmF5IiwiaSIsImwiLCJsZW5ndGgiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJpbXBvcnRTY3JpcHRzIiwib25tZXNzYWdlIiwiZSIsImRhdGEiLCJTYXNzIiwiY29tcGlsZSIsInJlc3VsdCIsInN0YXR1cyIsInBvc3RNZXNzYWdlIiwidGV4dCIsInR5cGUiLCJtZXNzYWdlIiwiZm9ybWF0dGVkIl0sInNvdXJjZVJvb3QiOiIifQ==