# Offline Free Code Camp Mobile

This project is an experimental fork of Free Code Camp with the goal of creating an offline optimized mobile application for users without reliable internet connections. It is not an official project from Free Code Camp and all support issues should be directed to this project.

## Introduction

Although many of us take our internet connections for granted, it may not always be the case for someone wanting to learn to code. This project seeks to make coding education more accessible for those users.

We've taking the curriculum from Free Code Camp and included it in a mobile application for Android. Users can simple download this application on their Android device and continue to use it without Internet access.

## High level goals for the beta version

- The download is a reasonable size. FCC's curriculum can be quite large, but it's text heavy and should compress well.
- The application should work well on moderately old Android devices.
- Users should have access to the bulk of the coding curriculum under FCC (some curriculum is heavily video based and therefore not a good fit)

## Download

[Find it on Google Play here](https://play.google.com/store/apps/details?id=space.atrailing.offline_code_camp)

## Development

Built with Flutter. TODO: Add development guide

## License

Licensed under the same license as Free Code Camp, [BSD 3 Clause License](./LICENSE)